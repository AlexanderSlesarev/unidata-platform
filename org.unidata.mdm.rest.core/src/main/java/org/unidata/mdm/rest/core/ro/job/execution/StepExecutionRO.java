/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 * Date: 10.03.2016
 */

package org.unidata.mdm.rest.core.ro.job.execution;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.core.ro.job.ExecutionStateRO;

/**
 * FIXDOC: add file description.
 *
 * @author amagdenko
 */
public class StepExecutionRO {

    private Long id;

    private Long jobExecutionId;

    private String stepName;

    private Instant startTime;

    private Instant endTime;

   	private ExecutionStateRO state;

   	private List<String> failures;

    /**
     * @return the state
     */
    public ExecutionStateRO getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(ExecutionStateRO state) {
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJobExecutionId() {
        return jobExecutionId;
    }

    public void setJobExecutionId(Long jobExecutionId) {
        this.jobExecutionId = jobExecutionId;
    }

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the failures
     */
    public List<String> getFailures() {
        return Objects.isNull(failures) ? Collections.emptyList() : failures;
    }

    /**
     * @param failures the failures to set
     */
    public void setFailures(List<String> failures) {
        this.failures = failures;
    }
}
