/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.core.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.context.DataImportInputContext;
import org.unidata.mdm.core.context.DataImportTemplateContext;
import org.unidata.mdm.core.dto.DataImportTemplateResult;
import org.unidata.mdm.core.service.DataImportService;
import org.unidata.mdm.core.type.load.DataImportFormat;
import org.unidata.mdm.rest.core.converter.DataImportConverter;
import org.unidata.mdm.rest.core.exception.CoreRestExceptionIds;
import org.unidata.mdm.rest.core.ro.ie.GetImportHandlersRO;
import org.unidata.mdm.rest.core.ro.ie.ImportDataResultRO;
import org.unidata.mdm.rest.core.ro.ie.TemplateParamsRequestRO;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.exception.PlatformFailureException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * The Class DataImportRestService.
 */
@Path(DataImportRestService.SERVICE_PATH)
@Consumes({"application/json"})
@Produces({"application/json"})
public class DataImportRestService extends AbstractRestService {
    /**
     * Path.
     */
    public static final String SERVICE_PATH = "import-data";
    /**
     * Tag.
     */
    public static final String SERVICE_TAG = SERVICE_PATH;
    /**
     * The data import service.
     */
    @Autowired
    private DataImportService dataImportService;
    /**
     * Info converter.
     */
    private DataImportConverter converter = new DataImportConverter();
    /**
     * List of handler onfo objects.
     * @return list of handler onfo objects
     */
    @GET
    @Operation(
        description = "Returns list of import handlers, known to the system.",
        method = HttpMethod.GET,
        tags = { SERVICE_TAG },
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetImportHandlersRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "401"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response findAll() {
        return ok(new GetImportHandlersRO(converter.to(dataImportService.handlers())));
    }
    /**
     * Import data.

     * @param params the import params
     * @param attachment   the attachment
     * @return the response
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(
        description = "Uploads data in XLSX format.",
        method = HttpMethod.POST,
        tags = { SERVICE_TAG },
        requestBody =
            @RequestBody(content =
                @Content(
                        mediaType = MediaType.MULTIPART_FORM_DATA,
                        schema = @Schema(implementation = DataUploadDescriptor.class)),
                description = "Attachment content."),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = ImportDataResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "401"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response handle(
            @Multipart("file") Attachment attachment,
            @Multipart("target") String target,
            @Multipart("format") String format,
            @Multipart("entityName") String entityName,
            @Multipart("sourceSystem") String sourceSystem,
            @Multipart("mergeWithPrevious") boolean mergeWithPrevious) {

        try {

            DataImportInputContext ctx = DataImportInputContext.builder()
                    .target(target)
                    .format(DataImportFormat.valueOf(format))
                    .entityName(entityName)
                    .fileName(attachment.getContentDisposition().getFilename())
                    .input(attachment.getDataHandler().getInputStream())
                    .mergeWithPrevious(mergeWithPrevious)
                    .sourceSystem(sourceSystem)
                    .build();

            dataImportService.handle(ctx);

        } catch (IOException ioe) {
            throw new PlatformFailureException("IO Exception, while importing data.",
                    ioe, CoreRestExceptionIds.EX_CORE_DATA_IMPORT_IO_FAILED);
        }

        return ok(new ImportDataResultRO(true));
    }
    /**
     * Creates the template.
     *
     * @param entityName the entity name
     * @return the response
     * @throws Exception the exception
     */
    @POST
    @Path("template")
    @Operation(
        description = "Create import template (temporary API).",
        method = HttpMethod.POST,
        tags = { SERVICE_TAG },
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = TemplateParamsRequestRO.class))),
        responses = {
                @ApiResponse(content = @Content(schema = @Schema(type = "string", format = "binary")), responseCode = "200"),
                @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response template(TemplateParamsRequestRO params) throws UnsupportedEncodingException {

        final DataImportFormat format = DataImportFormat.valueOf(params.getFormat());
        final DataImportTemplateResult result = dataImportService.template(DataImportTemplateContext.builder()
                .entityName(params.getEntityName())
                .format(format)
                .target(params.getTarget())
                .build());

        final String encodedFilename = URLEncoder.encode(params.getEntityName().toLowerCase()
                + "-import-template" + format.getExtension(),
                StandardCharsets.UTF_8.name());

        return Response
                .ok(result.getBytes())
                .encoding(StandardCharsets.UTF_8.name())
                .header("Content-Disposition",
                        "attachment; filename=" + params.getEntityName().toLowerCase() +  "-import-template" +  format.getExtension() + "; filename*=UTF-8''" + encodedFilename)
                .header("Content-Type", MediaType.APPLICATION_OCTET_STREAM_TYPE).build();
    }
    /**
     * @author Mikhail Mikhailov on Feb 2, 2021
     * Not really a functional class, but a OpenAPI helper.
     */
    static class DataUploadDescriptor {
        /**
         * The target handler name.
         */
        @Schema(name = "target", description =  "The target handler name as returned by handlers call.", type = "string")
        public String target;
        /**
         * Target import format
         */
        @Schema(name = "format", allowableValues = {"XLSX", "JSON"}, description = "Target import format (only XLSX is really supported for now).", type = "string")
        public String format;
        /**
         * The entity name.
         */
        @Schema(name = "entityName", description = "Target entity name, if applies.", type = "string")
        public String entityName;
        /**
         * The source system.
         */
        @Schema(name = "sourceSystem", description = "Source system name, if applies.", type = "string")
        public String sourceSystem;
        /**
         * Flag indicates merge with previous version flow.
         */
        @Schema(name = "mergeWithPrevious", description = "Merge with previous revision flag, if applies.", type = "boolean")
        public boolean mergeWithPrevious;
        /**
         * Binary payload.
         */
        @Schema(name = "file", description = "Upload content.", type = "string", format = "binary")
        public String file;
    }
}
