/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * @author Mikhail Mikhailov on Mar 4, 2021
 */
public class GetNamespacesRO extends DetailedOutputRO {
    /**
     * The NSs.
     */
    private List<NameSpaceRO> namespaces;
    /**
     * Constructor.
     */
    public GetNamespacesRO() {
        super();
    }
    /**
     * Constructor.
     */
    public GetNamespacesRO(List<NameSpaceRO> namespaces) {
        super();
        this.namespaces = namespaces;
    }
    /**
     * @return the namespaces
     */
    public List<NameSpaceRO> getNamespaces() {
        return namespaces;
    }
    /**
     * @param namespaces the namespaces to set
     */
    public void setNamespaces(List<NameSpaceRO> namespaces) {
        this.namespaces = namespaces;
    }
}
