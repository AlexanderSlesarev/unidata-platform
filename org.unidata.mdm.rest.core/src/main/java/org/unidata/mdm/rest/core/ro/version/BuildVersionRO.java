/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.rest.core.ro.version;

import java.util.Date;

/**
 * @author Sergey Murskiy on 31.05.2021
 */
public class BuildVersionRO {
    /**
     * Application version.
     */
    private String version;
    /**
     * SNV revision.
     */
    private String svnRevision;
    /**
     * Build number.
     */
    private String buildNumber;
    /**
     * Build date.
     */
    public Date buildDate;

    /**
     * @return version
     */
    public String getVersion() {
        return version;
    }
    /**
     * @param version version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }
    /**
     * @return svn revision
     */
    public String getSvnRevision() {
        return svnRevision;
    }
    /**
     * @param svnRevision svn revision to set
     */
    public void setSvnRevision(String svnRevision) {
        this.svnRevision = svnRevision;
    }
    /**
     * @return build number
     */
    public String getBuildNumber() {
        return buildNumber;
    }
    /**
     * @param buildNumber build number to set
     */
    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }
    /**
     * @return build date
     */
    public Date getBuildDate() {
        return buildDate;
    }
    /**
     * @param buildDate build date to set
     */
    public void setBuildDate(Date buildDate) {
        this.buildDate = buildDate;
    }
}
