/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.serialization.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.impl.job.JobParameterFactory;
import org.unidata.mdm.core.service.job.CustomJobRegistry;
import org.unidata.mdm.core.type.job.JobDescriptor;
import org.unidata.mdm.core.type.job.JobParameterDescriptor;
import org.unidata.mdm.rest.core.ro.job.definition.JobDefinitionRO;
import org.unidata.mdm.system.util.ContextUtils;

import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

/**
 * @author Mikhail Mikhailov on Jun 30, 2021
 * The strategy is, to read all, what can be read and validate, what has been read, later in converter.
 * Unknown parameters are skipped.
 */
public class JobDefinitionRODeserializer extends JsonDeserializer<JobDefinitionRO> {
    /**
     * The registry.
     */
    @Autowired
    private CustomJobRegistry jobRegistry;
    /**
     * The parameter factory.
     */
    @Autowired
    private JobParameterFactory parameterFactory;
    /**
     * Constructor.
     */
    public JobDefinitionRODeserializer() {
        ContextUtils.autowireOrphanBean(this);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public JobDefinitionRO deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {

        final ObjectCodec oc = p.getCodec();
        final JsonNode root = oc.readTree(p);
        final JobDefinitionRO result = new JobDefinitionRO();

        // 1. Required job name
        JsonNode n = root.get(JobDefinitionROFields.FIELD_JOB_NAME);
        if (Objects.isNull(n) || n.isNull()) {
            return result;
        }

        if (!n.isTextual()) {
            throwNodeTypeMismatch(JobDefinitionROFields.FIELD_JOB_NAME);
        }

        final String jobName = StringUtils.trim(n.asText());
        final JobDescriptor descriptor = jobRegistry.getDescriptor(jobName);

        if (Objects.isNull(descriptor)) {
            return result;
        }

        result.setJobName(jobName);

        // 2. Optional id
        readId(root, result);

        // 3. Required name
        readDisplayName(root, result);

        // 4. Optional description
        readDescription(root, result);

        // 5. Required enabled
        readEnabled(root, result);

        // 6. Cron expression
        readCronExpression(root, result);

        // 7. Tags
        readTags(root, result);

        // 8. Parameters
        readParameters(root, result, descriptor);

        return result;
    }

    private void readId(JsonNode root, JobDefinitionRO result) throws JsonProcessingException {

        JsonNode n = root.get(JobDefinitionROFields.FIELD_ID);
        if (Objects.nonNull(n) && !n.isNull()) {

            if (!n.isIntegralNumber()) {
                throwNodeTypeMismatch(JobDefinitionROFields.FIELD_ID);
            }

            result.setId(n.asLong());
        }
    }

    private void readDisplayName(JsonNode root, JobDefinitionRO result) throws JsonProcessingException {

        JsonNode n = root.get(JobDefinitionROFields.FIELD_DISPLAY_NAME);
        if (Objects.nonNull(n) && !n.isNull()) {

            if (!n.isTextual()) {
                throwNodeTypeMismatch(JobDefinitionROFields.FIELD_DISPLAY_NAME);
            }

            result.setDisplayName(StringUtils.trim(n.asText()));
        }
    }

    private void readDescription(JsonNode root, JobDefinitionRO result) throws JsonProcessingException {

        JsonNode n = root.get(JobDefinitionROFields.FIELD_DESCRIPTION);
        if (Objects.nonNull(n) && !n.isNull()) {

            if (!n.isTextual()) {
                throwNodeTypeMismatch(JobDefinitionROFields.FIELD_DESCRIPTION);
            }

            result.setDescription(StringUtils.trim(n.asText()));
        }
    }

    private void readEnabled(JsonNode root, JobDefinitionRO result) throws JsonProcessingException {

        JsonNode n = root.get(JobDefinitionROFields.FIELD_ENABLED);
        if (Objects.nonNull(n) && !n.isNull()) {

            if (!n.isBoolean()) {
                throwNodeTypeMismatch(JobDefinitionROFields.FIELD_ENABLED);
            }

            result.setEnabled(n.asBoolean());
        }
    }

    private void readCronExpression(JsonNode root, JobDefinitionRO result) throws JsonProcessingException {

        JsonNode n = root.get(JobDefinitionROFields.FIELD_CRON_EXPRESSION);
        if (Objects.nonNull(n) && !n.isNull()) {

            if (!n.isTextual()) {
                throwNodeTypeMismatch(JobDefinitionROFields.FIELD_CRON_EXPRESSION);
            }

            result.setCronExpression(n.asText());
        }
    }

    private void readTags(JsonNode root, JobDefinitionRO result) throws JsonProcessingException {

        JsonNode n = root.get(JobDefinitionROFields.FIELD_TAGS);
        if (Objects.nonNull(n) && !n.isNull()) {

            if (!n.isArray()) {
                throwNodeTypeMismatch(JobDefinitionROFields.FIELD_TAGS);
            }

            Set<String> tags = new HashSet<>();
            ArrayNode an = (ArrayNode) n;
            for (int i = 0; i < an.size(); i++) {

                JsonNode el = an.get(i);
                if (Objects.isNull(el) || el.isNull()) {
                    continue;
                }

                if (!el.isTextual()) {
                    throwNodeTypeMismatch(JobDefinitionROFields.FIELD_TAGS + " (in element(s))");
                }

                tags.add(StringUtils.trim(el.asText()));
            }

            if (CollectionUtils.isNotEmpty(tags)) {
                result.setTags(new ArrayList<>(tags));
            }
        }
    }

    private void readParameters(JsonNode root, JobDefinitionRO result, JobDescriptor descriptor) throws JsonProcessingException {

        JsonNode n = root.get(JobDefinitionROFields.FIELD_PARAMETERS);
        if (Objects.isNull(n) || n.isNull()) {
            return;
        } else if (!n.isObject()) {
            throwNodeTypeMismatch(JobDefinitionROFields.FIELD_PARAMETERS);
        }

        Map<String, JobParameterDescriptor<?>> all = descriptor.getAllParameters();
        Map<String, Object> extracted = new HashMap<>();
        for (Iterator<Entry<String, JsonNode>> i = n.fields(); i.hasNext(); ) {

            Entry<String, JsonNode> obj = i.next();

            JobParameterDescriptor<?> pd = all.get(obj.getKey());

            // Ignore unknown
            if (Objects.isNull(pd) || pd.isHidden() || pd.isReadOnly()
             || Objects.isNull(obj.getValue()) || obj.getValue().isNull()) {
                continue;
            }

            extracted.put(pd.getName(), parameterFactory.fromNode(pd, obj.getValue()));
        }

        result.setParameters(extracted);
    }

    private void throwNodeTypeMismatch(String fieldId) throws JobDefinitionDeserializingException {
        throw new JobDefinitionDeserializingException("The field [" + fieldId + "] is not of the expected type.");
    }

    public static class JobDefinitionDeserializingException extends JsonProcessingException {
        /**
         * GSVUID.
         */
        private static final long serialVersionUID = 6907032970984539082L;

        public JobDefinitionDeserializingException(String msg, JsonLocation loc, Throwable rootCause) {
            super(msg, loc, rootCause);
        }

        public JobDefinitionDeserializingException(String msg, JsonLocation loc) {
            super(msg, loc);
        }

        public JobDefinitionDeserializingException(String msg, Throwable rootCause) {
            super(msg, rootCause);
        }

        public JobDefinitionDeserializingException(String msg) {
            super(msg);
        }

        public JobDefinitionDeserializingException(Throwable rootCause) {
            super(rootCause);
        }
    }
}
