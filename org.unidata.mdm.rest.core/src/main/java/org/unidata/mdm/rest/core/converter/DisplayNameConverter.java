/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.converter;

import java.util.stream.Collectors;

import org.unidata.mdm.core.dto.DisplayNameResolutionResult;
import org.unidata.mdm.rest.core.ro.display.DisplayNameRO;
import org.unidata.mdm.rest.core.ro.display.DisplayableFieldRO;
import org.unidata.mdm.system.convert.Converter;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov on Nov 16, 2021
 */
public class DisplayNameConverter extends Converter<DisplayNameResolutionResult, DisplayNameRO> {
    /**
     * Constructor.
     */
    public DisplayNameConverter() {
        super(DisplayNameConverter::convert, null);
    }

    private static DisplayNameRO convert(DisplayNameResolutionResult source) {

        DisplayNameRO target = new DisplayNameRO();

        target.setNameSpace(source.getNameSpace().getId());
        target.setTypeName(source.getTypeName());
        target.setSubject(source.getSubject());
        target.setDeleted(source.isDeleted());
        target.setInactive(source.isInactive());
        target.setQualifier(source.getQualifier());
        target.setValidFrom(ConvertUtils.date2LocalDateTime(source.getValidFrom()));
        target.setValidTo(ConvertUtils.date2LocalDateTime(source.getValidTo()));
        target.setDisplayValue(source.getDisplayValue());
        target.setFields(source.getFields().stream()
                .map(s -> {

                    DisplayableFieldRO t = new DisplayableFieldRO();

                    t.setName(s.getName());
                    t.setDisplayName(s.getDisplayName());
                    t.setValues(s.getValues());
                    t.setDisplayValues(s.getDisplayValues());

                    return t;
                })
                .collect(Collectors.toList()));

        return target;
    }
}
