/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.core.ro.ie;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The Class ImportParams.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportParamsRO {
    /**
     * The target handler name.
     */
    @Schema(description =  "The target handler name as returned by handlers call.")
    private String target;
    /**
     * Target import format
     */
    @Schema(allowableValues = {"XLSX", "JSON"}, description = "Target import format (only XLSX is really supported for now).")
    private String format;
    /**
     * The entity name.
     */
    @Schema(description = "Target entity name, if applies.")
    private String entityName;
    /**
     * The source system.
     */
    @Schema(description = "Source system name, if applies.")
    private String sourceSystem;
    /**
     * Flag indicates merge with previous version flow.
     */
    @Schema(description = "Merge with previous revision flag, if applies.")
    private boolean mergeWithPrevious;
    /**
     * Gets the entity name.
     *
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * Sets the entity name.
     *
     * @param entityName
     *            the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * Gets the source system.
     *
     * @return the sourceSystem
     */
    public String getSourceSystem() {
        return sourceSystem;
    }

    /**
     * Sets the source system.
     *
     * @param sourceSystem
     *            the sourceSystem to set
     */
    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    /**
     * Is merge merge with previous version flow enabled.
     * @return
     */
    public boolean isMergeWithPrevious() {
        return mergeWithPrevious;
    }

    /**
     * Set flag.
     * @param mergeWithPreviousVersion true if importing records should be merged, false otherwise
     */
    public void setMergeWithPrevious(boolean mergeWithPreviousVersion) {
        this.mergeWithPrevious = mergeWithPreviousVersion;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * @return the format
     */
    public String getFormat() {
        return format;
    }

    /**
     * @param format the format to set
     */
    public void setFormat(String format) {
        this.format = format;
    }
}
