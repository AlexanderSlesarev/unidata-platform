/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 * Date: 10.03.2016
 */

package org.unidata.mdm.rest.core.ro.job.definition;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.unidata.mdm.rest.core.serialization.json.JobDefinitionRODeserializer;
import org.unidata.mdm.rest.core.serialization.json.JobDefinitionROSerializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * FIXDOC: add file description.
 *
 * @author amagdenko
 */
@JsonDeserialize(using = JobDefinitionRODeserializer.class)
@JsonSerialize(using = JobDefinitionROSerializer.class)
public class JobDefinitionRO {
    /**
     * This definition id.
     */
    private Long id;
    /**
     * The job name, this definition is created for.
     */
    private String jobName;
    /**
     * This definition name.
     */
    private String displayName;
    /**
     * This definition description.
     */
    private String description;
    /**
     * Enabled state (no run if false).
     */
    private boolean enabled;
    /**
     * Error state.
     */
    private boolean error;
    /**
     * Optional cron expression.
     */
    private String cronExpression;
    /**
     * Optional tags.
     */
    private List<String> tags;
    /**
     * Parameters.
     */
    private Map<String, Object> parameters;
    /**
     * Date, at what the last edition was created.
     */
    private Instant updateDate;
    /**
     * Date, at what the draft and first edition was created.
     */
    private Instant createDate;
    /**
     * The creator's user name.
     */
    private String createdBy;
    /**
     * The updater's user name.
     */
    private String updatedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String name) {
        this.displayName = name;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobNameReference) {
        this.jobName = jobNameReference;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<String> getTags() {
        return Objects.isNull(tags) ? Collections.emptyList() : tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
    /**
     * @return the parameters
     */
    public Map<String, Object> getParameters() {
        return Objects.isNull(parameters) ? Collections.emptyMap() : parameters;
    }
    /**
     * @param parameters the parameters to set
     */
    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }
    /**
     * @return the updateDate
     */
    public Instant getUpdateDate() {
        return updateDate;
    }
    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }
    /**
     * @return the createDate
     */
    public Instant getCreateDate() {
        return createDate;
    }
    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Instant createDate) {
        this.createDate = createDate;
    }
    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    /**
     * @return the updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }
    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
