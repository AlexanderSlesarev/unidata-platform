/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.serialization.json;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.job.CustomJobRegistry;
import org.unidata.mdm.core.type.job.JobDescriptor;
import org.unidata.mdm.core.type.job.JobParameterDescriptor;
import org.unidata.mdm.rest.core.ro.job.definition.JobDefinitionRO;
import org.unidata.mdm.system.util.ContextUtils;
import org.unidata.mdm.system.util.ConvertUtils;
import org.unidata.mdm.system.util.JsonUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * @author Mikhail Mikhailov on Jun 30, 2021
 */
public class JobDefinitionROSerializer extends JsonSerializer<JobDefinitionRO> {
    /**
     * The registry.
     */
    @Autowired
    private CustomJobRegistry jobRegistry;
    /**
     * Constructor.
     */
    public JobDefinitionROSerializer() {
        ContextUtils.autowireOrphanBean(this);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(JobDefinitionRO value, JsonGenerator jg, SerializerProvider serializers)
            throws IOException {

        jg.writeStartObject();

        jg.writeNumberField(JobDefinitionROFields.FIELD_ID, value.getId());
        jg.writeStringField(JobDefinitionROFields.FIELD_JOB_NAME, value.getJobName());
        jg.writeStringField(JobDefinitionROFields.FIELD_DISPLAY_NAME, value.getDisplayName());
        jg.writeStringField(JobDefinitionROFields.FIELD_DESCRIPTION, value.getDescription());
        jg.writeBooleanField(JobDefinitionROFields.FIELD_ENABLED, value.isEnabled());
        jg.writeBooleanField(JobDefinitionROFields.FIELD_ERROR, value.isError());
        jg.writeStringField(JobDefinitionROFields.FIELD_CRON_EXPRESSION, value.getCronExpression());
        jg.writeStringField(JobDefinitionROFields.FIELD_CREATE_DATE, ConvertUtils.instant2String(value.getCreateDate()));
        jg.writeStringField(JobDefinitionROFields.FIELD_CREATED_BY, value.getCreatedBy());
        jg.writeStringField(JobDefinitionROFields.FIELD_UPDATE_DATE, ConvertUtils.instant2String(value.getUpdateDate()));
        jg.writeStringField(JobDefinitionROFields.FIELD_UPDATED_BY, value.getUpdatedBy());

        jg.writeFieldName(JobDefinitionROFields.FIELD_TAGS);
        jg.writeStartArray();
        for (String v : value.getTags()) {
            jg.writeString(v);
        }
        jg.writeEndArray();

        final JobDescriptor descriptor = jobRegistry.getDescriptor(value.getJobName());

        Map<String, JobParameterDescriptor<?>> all = new HashMap<>();
        all.putAll(descriptor.getParametersMap());
        descriptor.getFractions().forEach(f -> all.putAll(f.getParametersMap()));

        jg.writeFieldName(JobDefinitionROFields.FIELD_PARAMETERS);
        jg.writeStartObject();
        for (Entry<String, Object> p : value.getParameters().entrySet()) {
            writeParameter(jg, all, p);
        }
        jg.writeEndObject();

        jg.writeEndObject();
    }

    private void writeParameter(JsonGenerator jg, Map<String, JobParameterDescriptor<?>> all, Entry<String, Object> paramVal)
            throws IOException {

        JobParameterDescriptor<?> pd = all.get(paramVal.getKey());
        if (Objects.isNull(pd)) {
            return;
        }

        jg.writeFieldName(pd.getName());
        jg.writeTree(JsonUtils.writeNode(paramVal.getValue()));
    }
}
