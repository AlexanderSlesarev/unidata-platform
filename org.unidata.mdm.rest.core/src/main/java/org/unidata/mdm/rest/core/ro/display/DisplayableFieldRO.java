/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.display;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Mikhail Mikhailov on Nov 19, 2021
 */
public class DisplayableFieldRO {
    /**
     * Attribute name.
     */
    private String name;
    /**
     * Attribute's display name.
     */
    private String displayName;
    /**
     * Plain values.
     */
    private List<Object> values;
    /**
     * Formatted display value(s).
     */
    private List<String> displayValues;
    /**
     * Constructor.
     */
    public DisplayableFieldRO() {
        super();
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the values
     */
    public List<Object> getValues() {
        return Objects.isNull(values) ? Collections.emptyList() : values;
    }
    /**
     * @param values the values to set
     */
    public void setValues(List<Object> values) {
        this.values = values;
    }
    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }
    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    /**
     * @return the displayValues
     */
    public List<String> getDisplayValues() {
        return Objects.isNull(displayValues) ? Collections.emptyList() : displayValues;
    }
    /**
     * @param displayValues the displayValues to set
     */
    public void setDisplayValues(List<String> displayValues) {
        this.displayValues = displayValues;
    }
}
