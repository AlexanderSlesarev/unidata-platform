/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.context.DisplayNameResolutionContext;
import org.unidata.mdm.core.dto.DisplayNameResolutionResult;
import org.unidata.mdm.core.service.DisplayNameService;
import org.unidata.mdm.rest.core.converter.DisplayNameConverter;
import org.unidata.mdm.rest.core.ro.DisplayNamesRequestRO;
import org.unidata.mdm.rest.core.ro.DisplayNamesResultRO;
import org.unidata.mdm.rest.core.ro.GetDisplayNameRO;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.system.service.NameSpaceService;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Dmitriy Bobrov on Nov 01, 2021
 */
@Path(DisplayNameRestService.SERVICE_PATH)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class DisplayNameRestService {

    public static final String SERVICE_PATH = "display-name";
    public static final String SERVICE_TAG = "display-name";

    private static final DisplayNameConverter DISPLAY_NAME_CONVERTER = new DisplayNameConverter();

    @Autowired
    private DisplayNameService displayNameService;

    @Autowired
    private NameSpaceService nameSpaceService;

    @GET
    @Operation(
            description = "Gets single display name",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            responses = {
                @ApiResponse(content = @Content(schema = @Schema(implementation = GetDisplayNameRO.class)), responseCode = "200"),
                @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")})
    public GetDisplayNameRO resolve(
            @QueryParam("nameSpace") String nameSpace,
            @QueryParam("typeName") String typeName,
            @QueryParam("subject") String subject,
            @QueryParam("validFrom") @DefaultValue(StringUtils.EMPTY) String from,
            @QueryParam("validTo") @DefaultValue(StringUtils.EMPTY) String to,
            @QueryParam("deleted") String deletedAsString,
            @QueryParam("inactive") String inactiveAsString,
            @QueryParam("qualifier") String qualifier,
            @QueryParam("fields") String fieldsAsString) {

        String[] fields = StringUtils.split(fieldsAsString, ',');
        DisplayNameResolutionResult result = displayNameService.resolve(DisplayNameResolutionContext.builder()
                .nameSpace(nameSpaceService.getById(nameSpace))
                .typeName(typeName)
                .subject(subject)
                .validFrom(ConvertUtils.string2Date(from))
                .validTo(ConvertUtils.string2Date(to))
                .deleted(BooleanUtils.toBoolean(deletedAsString))
                .inactive(BooleanUtils.toBoolean(inactiveAsString))
                .qualifier(StringUtils.defaultIfBlank(qualifier, null))
                .fields(Objects.isNull(fields) ? null : Arrays.stream(fields).map(StringUtils::trim).toArray(String[]::new))
                .build());

        return new GetDisplayNameRO(DISPLAY_NAME_CONVERTER.to(result));
    }

    @POST
    @Operation(
        description = "Get several display names at once.",
        method = HttpMethod.POST,
        tags = SERVICE_TAG,
        requestBody = @RequestBody(content =
            @Content(schema = @Schema(implementation = DisplayNamesRequestRO.class)), description = "Get display names by supplied specs."),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = DisplayNamesResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")})
    public DisplayNamesResultRO resolve(DisplayNamesRequestRO query) {

        List<DisplayNameResolutionResult> results = displayNameService.resolve(query.getSpecs().stream()
                .filter(Objects::nonNull)
                .map(s -> DisplayNameResolutionContext.builder()
                        .nameSpace(nameSpaceService.getById(s.getNameSpace()))
                        .typeName(s.getTypeName())
                        .subject((Serializable) s.getSubject())
                        .qualifier(s.getQualifier())
                        .validFrom(ConvertUtils.localDateTime2Date(s.getValidFrom()))
                        .validTo(ConvertUtils.localDateTime2Date(s.getValidTo()))
                        .deleted(s.isDeleted())
                        .inactive(s.isInactive())
                        .fields(s.getFields())
                        .build())
                .collect(Collectors.toList()));

        return new DisplayNamesResultRO(DISPLAY_NAME_CONVERTER.to(results));
    }
}
