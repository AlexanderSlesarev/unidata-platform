/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.serialization.json;

/**
 * @author Mikhail Mikhailov on Jul 1, 2021
 */
public final class JobDefinitionROFields {
    // Read-Write
    /**
     * Job name.
     */
    public static final String FIELD_JOB_NAME = "jobName";
    /**
     * Definition id.
     */
    public static final String FIELD_ID = "id";
    /**
     * Definition name.
     */
    public static final String FIELD_DISPLAY_NAME = "displayName";
    /**
     * Definition description.
     */
    public static final String FIELD_DESCRIPTION = "description";
    /**
     * 'Enabled' field.
     */
    public static final String FIELD_ENABLED = "enabled";
    /**
     * 'Error' field
     */
    public static final String FIELD_ERROR = "error";
    /**
     * Cron expression field.
     */
    public static final String FIELD_CRON_EXPRESSION = "cronExpression";
    /**
     * Tags.
     */
    public static final String FIELD_TAGS = "tags";
    /**
     * Parameters.
     */
    public static final String FIELD_PARAMETERS = "parameters";
    // Read-Only
    /**
     * Date, at what the last edition was created.
     */
    public static final String FIELD_UPDATE_DATE = "updateDate";
    /**
     * Date, at what the draft and first edition was created.
     */
    public static final String FIELD_CREATE_DATE = "createDate";
    /**
     * The creator's user name.
     */
    public static final String FIELD_CREATED_BY = "createdBy";
    /**
     * The updater's user name.
     */
    public static final String FIELD_UPDATED_BY = "updatedBy";
    /**
     * Constructor.
     */
    private JobDefinitionROFields() {
        super();
    }
}
