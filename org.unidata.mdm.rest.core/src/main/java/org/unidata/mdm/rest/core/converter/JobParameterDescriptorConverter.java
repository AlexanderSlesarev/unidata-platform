/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.converter;

import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.job.JobParameterDescriptor;
import org.unidata.mdm.rest.core.ro.job.descriptor.JobParameterDescriptorRO;
import org.unidata.mdm.system.convert.Converter;

@Component
public class JobParameterDescriptorConverter extends Converter<JobParameterDescriptor<?>, JobParameterDescriptorRO> {

    public JobParameterDescriptorConverter() {
        super(JobParameterDescriptorConverter::convert, null);
    }

    private static JobParameterDescriptorRO convert(JobParameterDescriptor<?> source) {

        JobParameterDescriptorRO target = new JobParameterDescriptorRO();

        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        target.setDescription(source.getDescription());
        target.setHidden(source.isHidden());
        target.setRequired(source.isRequired());
        target.setReadOnly(source.isReadOnly());
        target.setType(source.getType().name());
        target.setKind(source.getKind().name());
        target.setDefaultValue(source.getDefaultValue());

        if (source.hasSelector()) {
            target.setSelector(source.getSelector().get().entrySet()
                    .stream()
                    .sequential()
                    .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (a, b) -> a + b, LinkedHashMap::new)));
        }

        return target;
    }
}
