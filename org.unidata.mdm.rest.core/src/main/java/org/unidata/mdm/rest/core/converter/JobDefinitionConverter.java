/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.converter;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.exception.CoreExceptionIds;
import org.unidata.mdm.core.exception.JobException;
import org.unidata.mdm.core.service.impl.job.JobParameterFactory;
import org.unidata.mdm.core.service.job.CustomJobRegistry;
import org.unidata.mdm.core.type.job.JobDefinition;
import org.unidata.mdm.core.type.job.JobDescriptor;
import org.unidata.mdm.core.type.job.JobParameterDefinition;
import org.unidata.mdm.core.type.job.JobParameterDescriptor;
import org.unidata.mdm.rest.core.ro.job.definition.JobDefinitionRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Jun 30, 2021
 */
@Component
public class JobDefinitionConverter extends Converter<JobDefinition, JobDefinitionRO> {

    @Autowired
    private CustomJobRegistry jobRegistry;

    @Autowired
    private JobParameterFactory jobParameterFactory;

    /**
     * Constructor.
     */
    public JobDefinitionConverter() {
        super();
        this.to = this::convert;
        this.from = this::convert;
    }

    private JobDefinitionRO convert(JobDefinition source) {

        final JobDefinitionRO ro = new JobDefinitionRO();
        ro.setId(source.getId());
        ro.setDisplayName(source.getDisplayName());
        ro.setCronExpression(source.getCronExpression());
        ro.setJobName(source.getJobName());
        ro.setEnabled(source.isEnabled());
        ro.setError(source.isError());
        ro.setDescription(source.getDescription());
        ro.setTags(source.getTags());
        ro.setParameters(to(source.getParametersMap()));
        ro.setCreateDate(Objects.isNull(source.getCreateDate()) ? null : source.getCreateDate().toInstant());
        ro.setCreatedBy(source.getCreatedBy());
        ro.setUpdateDate(Objects.isNull(source.getUpdateDate()) ? null : source.getUpdateDate().toInstant());
        ro.setUpdatedBy(source.getUpdatedBy());

        return ro;
    }

    private JobDefinition convert(JobDefinitionRO source) {

        if (StringUtils.isBlank(source.getJobName())) {
            throw new JobException("Invalid job definition. Job name is missing.",
                    CoreExceptionIds.EX_JOB_DEFINITION_JOB_NAME_MISSING);
        } else if (StringUtils.isBlank(source.getDisplayName())) {
            throw new JobException("Invalid job definition. Definition's display name is missing",
                    CoreExceptionIds.EX_JOB_DEFINITION_NAME_MISSING);
        }

        JobDescriptor descriptor = jobRegistry.getDescriptor(source.getJobName());
        if (Objects.isNull(descriptor)) {
            throw new JobException("Job descriptor for job name [{}] not found!",
                    CoreExceptionIds.EX_JOB_NOT_FOUND, source.getJobName());
        } else if (descriptor.isSystem()) {
            throw new JobException("Attempt to save job definition for a system, not configurable job [{}].",
                    CoreExceptionIds.EX_JOB_SAVE_TO_SYSTEM_JOB, descriptor.getJobName());
        }

        final JobDefinition jd = new JobDefinition();

        jd.setId(source.getId());
        jd.setDisplayName(source.getDisplayName());
        jd.setCronExpression(source.getCronExpression());
        jd.setJobName(source.getJobName());
        jd.setEnabled(source.isEnabled());
        jd.setError(source.isError());
        jd.setDescription(source.getDescription());
        jd.setTags(source.getTags());
        jd.setParameters(from(descriptor, source.getParameters()));

        return jd;
    }

    private Map<String, Object> to(Map<String, JobParameterDefinition<?>> parameters) {

        if (MapUtils.isEmpty(parameters)) {
            return Collections.emptyMap();
        }

        Map<String, Object> result = new LinkedHashMap<>();
        for (Entry<String, JobParameterDefinition<?>> entry : parameters.entrySet()) {

            JobParameterDefinition<?> p = entry.getValue();
            if (p.isCollection()) {
                result.put(entry.getKey(), p.collection());
            } else if (p.isSingle()) {
                result.put(entry.getKey(), p.single());
            } else if (p.isMap()) {
                result.put(entry.getKey(), p.map());
            } else if (p.isCustom()) {
                result.put(entry.getKey(), p.asCustom());
            }
        }

        return result;
    }

    private Map<String, JobParameterDefinition<?>> from(JobDescriptor descriptor, Map<String, Object> source) {

        Map<String, JobParameterDescriptor<?>> parameters = descriptor.getAllParameters();
        Map<String, JobParameterDefinition<?>> result = new LinkedHashMap<>();
        for (JobParameterDescriptor<?> d : parameters.values()) {

            // Hidden's default value will be put to parameters
            // right before the execution if not prresent.
            if (d.isHidden() || d.isReadOnly()) {

                if (d.isReadOnly() && d.hasDefaultValue()) {
                    result.put(d.getName(), jobParameterFactory.fromValue(d, d.getDefaultValue()));
                }

                continue;
            }

            Object v = source.get(d.getName());
            v = Objects.isNull(v) ? d.getDefaultValue() : v;

            result.put(d.getName(), jobParameterFactory.fromValue(d, v));
        }

        return result;
    }

}
