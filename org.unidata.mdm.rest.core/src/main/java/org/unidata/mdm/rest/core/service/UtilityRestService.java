/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.core.util.FileUtils;
import org.unidata.mdm.rest.core.exception.CoreRestExceptionIds;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.system.util.RestConstants;
import org.unidata.mdm.system.exception.PlatformFailureException;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.FileAppender;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * @author Mikhail Mikhailov
 *
 */
@Path(UtilityRestService.SERVICE_PATH)
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class UtilityRestService extends AbstractRestService {
    /**
     * Logger.
     */
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(UtilityRestService.class);
    /**
     * Path.
     */
    public static final String SERVICE_PATH = "utility";
    /**
     * Tag.
     */
    public static final String SERVICE_TAG = SERVICE_PATH;
    /**
     * Log appender name.
     */
    private static final String BACKEND_LOG_FILE_APPENDER_NAME = "BACKEND_LOG_FILE";

    private static final Comparator<java.nio.file.Path> LOG_FILE_COMPARATOR = (o1, o2) -> {

        FileTime ft1 = null;
        try { ft1 = Files.getLastModifiedTime(o1); }
        catch (IOException e) {
            // NOSONAR
        }

        FileTime ft2 = null;
        try { ft2 = Files.getLastModifiedTime(o2); }
        catch (IOException e) {
            // NOSONAR
        }

        if (Objects.isNull(ft1) && Objects.isNull(ft2)) {
            return 0;
        }

        if (Objects.isNull(ft1)) {
            return -1;
        }

        if (Objects.isNull(ft2)) {
            return 1;
        }

        long result = ft1.toMillis() - ft2.toMillis();
        if (result < 0) {
            return -1;
        } else if (result > 0) {
            return 1;
        }

        return 0;
    };
    /**
     * Gets two last log records for this node.
     * @return response (attachment)
     */
    @GET
    @Path("/" + RestConstants.PATH_PARAM_LOGS)
    @Operation(
        description = "Returns the youngest files from this node.",
        method = HttpMethod.GET,
        tags = { SERVICE_TAG },
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(type = "string", format = "binary")), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response logs() {

        List<String> paths = collect();

        String addressTag = getHSR().getLocalAddr().replace('.', '_') + "_" + Integer.valueOf(getHSR().getLocalPort());
        String fileName = "logs-" + addressTag + "-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + ".zip";
        String encodedFilename = FileUtils.urlEncode(fileName);

        final StreamingOutput retval = so -> {

            try (ZipOutputStream zos = new ZipOutputStream(so, StandardCharsets.UTF_8)) {

                for (String path : paths) {
                    append(zos, path);
                }

                zos.finish();

            } catch (Exception exc) {
                LOGGER.warn("Exception caught while output logs (I/O).", exc);
            }
        };

        return Response.ok(retval)
                .encoding(StandardCharsets.UTF_8.name())
                .header("Content-Disposition", "attachment; filename=" + fileName + "; filename*=UTF-8''" + encodedFilename)
                .header("Content-Type", MediaType.APPLICATION_OCTET_STREAM_TYPE)
                .build();
    }

    private void append(ZipOutputStream zos, String path) {

        File f = new File(path);
        try (InputStream is = new FileInputStream(f)) {

            ZipEntry entry = new ZipEntry(f.getName());
            entry.setTime(f.lastModified());
            entry.setSize(f.length());
            entry.setMethod(ZipEntry.DEFLATED);

            zos.putNextEntry(entry);

            byte[] buf = new byte[FileUtils.DEFAULT_BUFFER_SIZE];
            int count = -1;
            while ((count = is.read(buf, 0, buf.length)) != -1) {
                zos.write(buf, 0, count);
            }

            zos.closeEntry();

        } catch (Exception e) {
            LOGGER.warn("Exception caught while reading file {}.", path, e);
        }
    }

    private List<String> collect() {

        String current = current();
        if (StringUtils.isBlank(current)) {
            throw new PlatformFailureException("System log file appender not found. Check 'logback.xml'",
                    CoreRestExceptionIds.EX_SYSTEM_LOG_FILE_APPENDER_NOT_FOUND_OR_MISCONFIGURED);
        }

        List<String> paths = new ArrayList<>(2);
        paths.add(current);

        String previous = previous(current);
        if (Objects.nonNull(previous)) {
            paths.add(previous);
        }

        return paths;
    }

    private String current() {

        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        for (Logger logger : context.getLoggerList()) {

            for (Iterator<Appender<ILoggingEvent>> index = logger.iteratorForAppenders(); index.hasNext();) {

                Appender<ILoggingEvent> appender = index.next();
                if (appender instanceof FileAppender) {

                    FileAppender<?> fileAppender = (FileAppender<?>) appender;
                    if (!BACKEND_LOG_FILE_APPENDER_NAME.equals(appender.getName())) {
                        continue;
                    }

                    return new File(fileAppender.getFile()).getAbsolutePath();
                }
            }
        }

        return null;
    }

    /**
     * Selects youngest rotated (previous) log element.
     * @param currentLogPath current path
     * @return previous log path or null
     */
    private String previous(String currentLogPath) {

        String fileName = Paths.get(currentLogPath).getFileName().toString();
        String baseName = StringUtils.substringBefore(fileName, ".");

        java.nio.file.Path closed = Paths.get(Paths.get(currentLogPath).getParent().toString(), "closed");
        if (closed.toFile().isDirectory() && Files.isReadable(closed)) {

            try (Stream<java.nio.file.Path> paths = Files.list(closed)) {

                java.nio.file.Path prev = paths
                        .filter(p -> p.getFileName().toString().startsWith(baseName))
                        .max(LOG_FILE_COMPARATOR)
                        .orElse(null);

                return prev == null ? null : prev.toFile().getAbsolutePath();
            } catch (IOException ioe) {
                LOGGER.warn("Exception caught while listing log files in 'closed'", ioe);
            }
        }

        return null;
    }
}
