/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.core.ro.display.DisplayNameSpecRO;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Nov 16, 2021
 * Display names requsts. Specs can follow for different registers, different tine intervals.
 * The only restriction - flags inactive & deleted can not have distinct state within namespace:typename.
 * The order is not preserved in response.
 */
@Schema(description = "Display names requsts.\n"
        + "Specs can be supplied for different type names, different time intervals and namespaces in a single request.\n"
        + "The only restriction - flags inactive & deleted can not have distinct state within namespace:typename.\n"
        + "The order is not preserved in response.")
public class DisplayNamesRequestRO {
    /**
     * Display name specifications.
     */
    private List<DisplayNameSpecRO> specs;
    /**
     * Constructor.
     */
    public DisplayNamesRequestRO() {
        super();
    }
    /**
     * @return the specs
     */
    public List<DisplayNameSpecRO> getSpecs() {
        return Objects.isNull(specs) ? Collections.emptyList() : specs;
    }
    /**
     * @param specs the specs to set
     */
    public void setSpecs(List<DisplayNameSpecRO> specs) {
        this.specs = specs;
    }
}
