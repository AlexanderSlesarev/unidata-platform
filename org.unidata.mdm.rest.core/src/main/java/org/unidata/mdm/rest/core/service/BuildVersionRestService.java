/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */

package org.unidata.mdm.rest.core.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.rest.core.ro.version.BuildVersionRO;
import org.unidata.mdm.rest.core.ro.version.GetBuildVersionResultRO;
import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.configuration.BuildVersion;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.text.ParseException;

/**
 * @author Sergey Murskiy on 31.05.2021
 */
@Path("/build-version")
@Consumes({"application/json"})
@Produces({"application/json"})
public class BuildVersionRestService extends AbstractRestService {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BuildVersionRestService.class);

    @GET
    @Operation(
            description = "Get build version.",
            method = "GET",
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetBuildVersionResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedOutputRO.class)), responseCode = "500")
            }
    )
    public Response getBuildVersion() {
        BuildVersionRO ro = new BuildVersionRO();

        ro.setVersion(BuildVersion.APP_VERSION);
        ro.setSvnRevision(BuildVersion.SVN_REVISION);
        ro.setBuildNumber(BuildVersion.BUILD_NUMBER);

        try {
            ro.setBuildDate(DateUtils.parseDate(BuildVersion.BUILD_DATE, "yyyy-MM-dd HH:mm:ss"));
        } catch (ParseException e) {
            final String message = "Unable to parse build date.";
            LOGGER.warn(message, e);
        }

        GetBuildVersionResultRO result = new GetBuildVersionResultRO();
        result.setBuildVersion(ro);

        return ok(result);
    }
}
