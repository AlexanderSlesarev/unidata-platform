package org.unidata.mdm.rest.core.service;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.context.DeleteLargeObjectContext;
import org.unidata.mdm.core.context.FetchLargeObjectContext;
import org.unidata.mdm.core.context.UpsertLargeObjectContext;
import org.unidata.mdm.core.dto.LargeObjectResult;
import org.unidata.mdm.core.exception.CoreExceptionIds;
import org.unidata.mdm.core.service.LargeObjectsService;
import org.unidata.mdm.core.type.lob.LargeObjectAcceptance;
import org.unidata.mdm.core.util.FileUtils;
import org.unidata.mdm.core.util.LargeObjectUtils;
import org.unidata.mdm.rest.core.converter.LargeObjectConverter;
import org.unidata.mdm.rest.core.ro.lob.DeleteLobResultRO;
import org.unidata.mdm.rest.core.ro.lob.UploadResultRO;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.system.util.RestConstants;
import org.unidata.mdm.system.exception.PlatformBusinessException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * Large object service
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
@Path(LargeObjectRestService.LOB_TAG)
public class LargeObjectRestService extends AbstractRestService {

    public static final String LOB_TAG = "lob";

    @Autowired
    private LargeObjectsService largeObjectsService;

    @Autowired
    private LargeObjectConverter largeObjectConverter;

    /**
     * Saves binary large object.
     *
     * @param blobId golden record id
     * @param tag attribute
     * @param attachment attachment object
     * @return ok/nok
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/" + RestConstants.PATH_PARAM_BLOB)
    @Operation(
        description = "Saves binary large object.",
        requestBody = @RequestBody(
                content = @Content(
                    mediaType = MediaType.MULTIPART_FORM_DATA,
                    schema = @Schema(implementation = LargeObjectUploadDescriptor.class)),
                description = "Attachment content."),
        tags = LOB_TAG,
        method = HttpMethod.POST,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = UploadResultRO.class)), responseCode = "200")
        }
    )
    public Response uploadBlob(
        @Multipart(value = RestConstants.DATA_PARAM_ID, required = false) String blobId,
        @Multipart(value = RestConstants.DATA_PARAM_FILENAME, required = false) String filename,
        @Multipart(value = RestConstants.DATA_PARAM_TAGS, required = false) String tags,
        @Multipart(value = RestConstants.DATA_PARAM_ACCEPTANCE, required = false) String acceptance,
        @Multipart(value = RestConstants.DATA_PARAM_CONTENT) Attachment attachment) {
        return ok(source(blobId, filename, StringUtils.split(tags, ','), acceptance, attachment, true));
    }
    /**
     * Saves character large object.
     *
     * @param clobId golden record id
     * @param tag attribute
     * @param attachment attachment object
     * @return ok/nok
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/" + RestConstants.PATH_PARAM_CLOB)
    @Operation(
        description = "Saves character large object.",
        requestBody = @RequestBody(
                content = @Content(
                    mediaType = MediaType.MULTIPART_FORM_DATA,
                    schema = @Schema(implementation = LargeObjectUploadDescriptor.class)),
                description = "Attachment content."),
        tags = LOB_TAG,
        method = HttpMethod.POST,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = UploadResultRO.class)), responseCode = "200")
        }
    )
    public Response uploadClob(
            @Multipart(value = RestConstants.DATA_PARAM_ID, required = false) String clobId,
            @Multipart(value = RestConstants.DATA_PARAM_FILENAME, required = false) String filename,
            @Multipart(value = RestConstants.DATA_PARAM_TAGS, required = false) String tags,
            @Multipart(value = RestConstants.DATA_PARAM_ACCEPTANCE, required = false) String acceptance,
        @Multipart(value = RestConstants.DATA_PARAM_CONTENT) Attachment attachment) {
        return ok(source(clobId, filename, StringUtils.split(tags, ','), acceptance, attachment, false));
    }
    /**
     * Gets CLOB data associated with an attribute of a golden record.
     *
     * @param clobId LOB object id.
     * @return byte stream
     */
    @GET
    @Path("/" + RestConstants.PATH_PARAM_CLOB + "/{" + RestConstants.DATA_PARAM_ID + "}")
    @Produces("text/plain")
    @Operation(
        description = "Gets CLOB data associated with the given ID.",
        method = HttpMethod.GET,
        tags = LOB_TAG,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response fetchClob(
            @Parameter(description = "CLOB ID", in = ParameterIn.PATH) @PathParam(RestConstants.DATA_PARAM_ID) String clobId) {

        final LargeObjectResult result = largeObjectsService.fetchLargeObject(FetchLargeObjectContext.builder()
                .largeObjectId(clobId)
                .binary(false)
                .build());

        return dump(result, false);
    }

    /**
     * Gets BLOB data associated with an attribute of a golden record.
     *
     * @param blobId LOB object id.
     * @return byte stream
     */
    @GET
    @Path("/" + RestConstants.PATH_PARAM_BLOB + "/{" + RestConstants.DATA_PARAM_ID + "}")
    @Operation(
        description = "Gets BLOB data associated with the given ID.",
        method = HttpMethod.GET,
        tags = LOB_TAG,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response fetchBlob(
            @Parameter(description = "BLOB ID", in = ParameterIn.PATH) @PathParam(RestConstants.DATA_PARAM_ID) String blobId) {

        final LargeObjectResult result = largeObjectsService.fetchLargeObject(FetchLargeObjectContext.builder()
                .largeObjectId(blobId)
                .binary(true)
                .build());

        return dump(result, true);
    }
    /**
     * Deletes golden blob data.
     *
     * @param blobId the id
     * @param attr the attribute
     * @return ok/nok
     */
    @DELETE
    @Path("/" + RestConstants.PATH_PARAM_BLOB + "/{" + RestConstants.DATA_PARAM_ID + "}")
    @Operation(
        description = "Deletes BLOB data, associated with the given ID.",
        method = HttpMethod.DELETE,
        tags = LOB_TAG,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DeleteLobResultRO.class)), responseCode = "200")
        }
    )
    public Response deleteBlob(
        @Parameter(description = "BLOB ID", in = ParameterIn.PATH) @PathParam(RestConstants.DATA_PARAM_ID) String blobId) {

        return ok(new DeleteLobResultRO(largeObjectsService.deleteLargeObject(DeleteLargeObjectContext.builder()
            .largeObjectId(blobId)
            .binary(true)
            .build())));
    }
    /**
     * Deletes golden CLOB data.
     *
     * @param clobId the id
     * @param attr the attribute
     * @return ok/nok
     */
    @DELETE
    @Path("/" + RestConstants.PATH_PARAM_CLOB + "/{" + RestConstants.DATA_PARAM_ID + "}")
    @Operation(
        description = "Deletes CLOB data, associated with the given ID.",
        method = HttpMethod.DELETE,
        tags = LOB_TAG,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DeleteLobResultRO.class)), responseCode = "200")
        }
    )
    public Response deleteClob(
        @Parameter(description = "CLOB ID", in = ParameterIn.PATH) @PathParam(RestConstants.DATA_PARAM_ID) String clobId) {

        return ok(new DeleteLobResultRO(largeObjectsService.deleteLargeObject(DeleteLargeObjectContext.builder()
                .largeObjectId(clobId)
                .binary(false)
                .build())));
    }

    private Response dump(final LargeObjectResult result, boolean binary) {

        String fileName = StringUtils.isBlank(result.getFileName()) ? StringUtils.EMPTY : FileUtils.urlEncode(result.getFileName());
        Response.ResponseBuilder response = Response.ok(LargeObjectUtils.createStreamingOutputForLargeObject(result))
            .encoding(StandardCharsets.UTF_8.name())
            .header(HttpHeaders.CONTENT_DISPOSITION , String.format("attachment; filename=%s; filename*=UTF-8'' %s", fileName, fileName));

        if (StringUtils.isNotBlank(result.getMimeType())) {
            response.header(HttpHeaders.CONTENT_TYPE, result.getMimeType());
        } else if (binary) {
            response.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_TYPE);
        } else {
            response.header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_TYPE);
        }

        return response.build();
    }

    private UploadResultRO source(String lobId, String filename, String[] tags, String acceptance, Attachment attachment, boolean binary) {

        MediaType mediaType = attachment.getContentType();
        if (binary || StringUtils.equals(mediaType.getType(), "text")) {

            UUID id = null;
            if (StringUtils.isNotBlank(lobId)) {
                try {
                    id = UUID.fromString(lobId);
                } catch (Exception e) {
                    throw new PlatformBusinessException("Invalid ID format [{0}]. A valid UUID is expected.",
                            CoreExceptionIds.EX_CORE_LOB_INVALID_ID_FORMAT,
                            lobId);
                }
            }

            LargeObjectResult largeObject = largeObjectsService.saveLargeObject(UpsertLargeObjectContext.builder()
                .tags(tags)
                .largeObjectId(id)
                .binary(binary)
                .acceptance(StringUtils.isBlank(acceptance) ? LargeObjectAcceptance.PENDING : LargeObjectAcceptance.valueOf(acceptance))
                .input(() -> attachment.getObject(InputStream.class))
                .filename(StringUtils.isNotBlank(filename) ? filename : attachment.getContentDisposition().getFilename())
                .mimeType(new StringBuilder()
                        .append(mediaType.getType())
                        .append('/')
                        .append(mediaType.getSubtype())
                        .toString())
                .build());

            return new UploadResultRO(largeObjectConverter.from(largeObject));
        } else {
            throw new PlatformBusinessException("Invalid character media type [{0}]. Only 'text/*' is supported.",
                    CoreExceptionIds.EX_CORE_LOB_INVALID_CLOB_MEDIATYPE,
                    attachment.getContentType().toString());
        }
    }
    /**
     * @author Mikhail Mikhailov on Sep 19, 2021
     * Upload descriptor.
     */
    static class LargeObjectUploadDescriptor {

        @Schema(name = RestConstants.DATA_PARAM_ID, description = "ID (UUID) of the LOB object. Blank for new uploads.", type = "string")
        public String id;

        @Schema(name = RestConstants.DATA_PARAM_FILENAME, description = "Name of the file.", type = "string")
        public String filename;

        @Schema(name = RestConstants.DATA_PARAM_TAGS, description = "Optional collection of tags (a comma separated string).", type = "string", example = "param1,param2,param3,...")
        public String tags;

        @Schema(name = RestConstants.DATA_PARAM_ACCEPTANCE, allowableValues = {"PENDING", "ACCEPTED"}, type = "string",
                description = "Optional acceptance flag. 'PENDING' means, that the LOB record will be saved to the DB, but will be deleted by binary junk collector job, "
                        + "if the record didn't switch the state to 'ACCEPTED' for a configured period of time. 'ACCEPTED' will mark the LOB record as persisted. "
                        + "If the field is missing in save request 'PENDING' is used.")
        public String acceptance;

        @Schema(name = RestConstants.DATA_PARAM_CONTENT, description = "Upload content.", type = "string", format = "binary")
        public String content;
    }
}

