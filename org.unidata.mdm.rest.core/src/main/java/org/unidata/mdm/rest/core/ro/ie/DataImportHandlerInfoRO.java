/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.ie;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on May 13, 2021
 * Handler info.
 */
public class DataImportHandlerInfoRO {
    /**
     * Handler's id.
     */
    @Schema(description = "Data handler's ID.")
    private String id;
    /**
     * Handler's description.
     */
    @Schema(description = "Data handler's decsription.")
    private String description;
    /**
     * Supported formats.
     */
    @Schema(description = "Data formats, supported by this handler.", allowableValues = {"XLSX", "JSON"})
    private Collection<String> formats;
    /**
     * Constructor.
     */
    public DataImportHandlerInfoRO() {
        super();
    }
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the formats
     */
    public Collection<String> getFormats() {
        return Objects.isNull(formats) ? Collections.emptyList() : formats;
    }
    /**
     * @param formats the formats to set
     */
    public void setFormats(Collection<String> formats) {
        this.formats = formats;
    }
}
