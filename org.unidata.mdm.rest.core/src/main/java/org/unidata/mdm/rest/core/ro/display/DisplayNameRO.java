/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.display;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Nov 16, 2021
 */
public class DisplayNameRO {

    @Schema(description = "Data name space.")
    private String nameSpace;

    @Schema(description = "Data type name (entity name)")
    private String typeName;

    @Schema(description = "The ID (record|relation|other).")
    private Object subject;

    @Schema(description = "Qualifier field, if was set")
    private String qualifier;

    @Schema(description = "Deleted state mark, if was set")
    private boolean deleted;

    @Schema(description = "Inactive state mark, if was set")
    private boolean inactive;

    @Schema(description = "Valid from left boundary")
    private LocalDateTime validFrom;

    @Schema(description = "Valid to right boundary")
    private LocalDateTime validTo;

    @Schema(description = "Separate fields array")
    private List<DisplayableFieldRO> fields;
    /**
     * The final joined value.
     */
    @Schema(description = "Display name for the whole record.")
    private String displayValue;
    /**
     * Constructor.
     */
    public DisplayNameRO() {
        super();
    }
    /**
     * @return the nameSpace
     */
    public String getNameSpace() {
        return nameSpace;
    }
    /**
     * @param nameSpace the nameSpace to set
     */
    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }
    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }
    /**
     * @param typeName the typeName to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    /**
     * @return the subject
     */
    public Object getSubject() {
        return subject;
    }
    /**
     * @param subject the subject to set
     */
    public void setSubject(Object subject) {
        this.subject = subject;
    }
    /**
     * @return the qualifier
     */
    public String getQualifier() {
        return qualifier;
    }
    /**
     * @param qualifier the qualifier to set
     */
    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }
    /**
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }
    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    /**
     * @return the inactive
     */
    public boolean isInactive() {
        return inactive;
    }
    /**
     * @param inactive the inactive to set
     */
    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }
    /**
     * @return the validFrom
     */
    public LocalDateTime getValidFrom() {
        return validFrom;
    }
    /**
     * @param validFrom the validFrom to set
     */
    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }
    /**
     * @return the validTo
     */
    public LocalDateTime getValidTo() {
        return validTo;
    }
    /**
     * @param validTo the validTo to set
     */
    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }
    /**
     * @return the fields
     */
    public List<DisplayableFieldRO> getFields() {
        return Objects.isNull(fields) ? Collections.emptyList() : fields;
    }
    /**
     * @param fields the fields to set
     */
    public void setFields(List<DisplayableFieldRO> fields) {
        this.fields = fields;
    }
    /**
     * @return the displayValue
     */
    public String getDisplayValue() {
        return displayValue;
    }
    /**
     * @param displayValue the displayValue to set
     */
    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }
}
