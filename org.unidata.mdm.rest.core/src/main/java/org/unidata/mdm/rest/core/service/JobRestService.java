/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.core.service;

import static org.unidata.mdm.core.util.SecurityUtils.ADMIN_SYSTEM_MANAGEMENT;
import static org.unidata.mdm.core.util.SecurityUtils.DATA_OPERATIONS_MANAGEMENT;
import static org.unidata.mdm.core.util.SecurityUtils.EXECUTE_DATA_OPERATIONS;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.unidata.mdm.core.context.JobDefinitionUpsertContext;
import org.unidata.mdm.core.context.JobDefinitionsEnableContext;
import org.unidata.mdm.core.context.JobDefinitionsQueryContext;
import org.unidata.mdm.core.context.JobDefinitionsRemoveContext;
import org.unidata.mdm.core.context.JobDescriptorsGetContext;
import org.unidata.mdm.core.context.JobExecutionQueryContext;
import org.unidata.mdm.core.context.JobExecutionStartContext;
import org.unidata.mdm.core.context.JobExecutionStatusContext;
import org.unidata.mdm.core.context.JobExecutionStopContext;
import org.unidata.mdm.core.context.StepExecutionQueryContext;
import org.unidata.mdm.core.context.UpsertLargeObjectContext;
import org.unidata.mdm.core.context.UpsertUserEventRequestContext;
import org.unidata.mdm.core.dto.UserEventDTO;
import org.unidata.mdm.core.dto.job.JobDefinitionUpsertResult;
import org.unidata.mdm.core.dto.job.JobDefinitionsQueryResult;
import org.unidata.mdm.core.dto.job.JobDescriptorsGetResult;
import org.unidata.mdm.core.dto.job.JobExecutionStartResult;
import org.unidata.mdm.core.dto.job.JobExecutionStatusResult;
import org.unidata.mdm.core.dto.job.JobExecutionsQueryResult;
import org.unidata.mdm.core.dto.job.StepExecutionQueryResult;
import org.unidata.mdm.core.dto.reports.ReportUtil;
import org.unidata.mdm.core.exception.JobException;
import org.unidata.mdm.core.service.AsyncExecutor;
import org.unidata.mdm.core.service.JobService;
import org.unidata.mdm.core.service.LargeObjectsService;
import org.unidata.mdm.core.service.SecurityService;
import org.unidata.mdm.core.service.UserService;
import org.unidata.mdm.core.type.job.JobDefinition;
import org.unidata.mdm.core.type.job.JobExecutionStatus;
import org.unidata.mdm.core.util.FileUtils;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.rest.core.converter.ExecutionStateConverter;
import org.unidata.mdm.rest.core.converter.JobDefinitionConverter;
import org.unidata.mdm.rest.core.converter.JobDescriptorConverter;
import org.unidata.mdm.rest.core.converter.JobExecutionConverter;
import org.unidata.mdm.rest.core.converter.StepExecutionConverter;
import org.unidata.mdm.rest.core.exception.CoreRestExceptionIds;
import org.unidata.mdm.rest.core.ro.job.ExportJobDefinitionsRequestRO;
import org.unidata.mdm.rest.core.ro.job.JobGeneralSuccessRO;
import org.unidata.mdm.rest.core.ro.job.definition.GetJobDefinitionResultRO;
import org.unidata.mdm.rest.core.ro.job.definition.GetJobDefinitionsWithStateResultRO;
import org.unidata.mdm.rest.core.ro.job.definition.GetTagsResultRO;
import org.unidata.mdm.rest.core.ro.job.definition.JobDefinitionWithStateRO;
import org.unidata.mdm.rest.core.ro.job.definition.JobDefinitionsRO;
import org.unidata.mdm.rest.core.ro.job.definition.SearchJobDefinitionsRO;
import org.unidata.mdm.rest.core.ro.job.definition.UpsertJobDefinitionRO;
import org.unidata.mdm.rest.core.ro.job.descriptor.GetJobDescriptorsResultRO;
import org.unidata.mdm.rest.core.ro.job.execution.GetJobExecutionResultRO;
import org.unidata.mdm.rest.core.ro.job.execution.GetJobExecutionsResultRO;
import org.unidata.mdm.rest.core.ro.job.execution.GetStepExecutionsResultRO;
import org.unidata.mdm.rest.core.ro.job.execution.StatusJobExecutionResultRO;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.util.JsonUtils;
import org.unidata.mdm.system.util.TextUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Encoding;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * Rest service for all job related activities.
 */
@Path("/jobs")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class JobRestService extends AbstractRestService {
    /**
     * Descriptors path element and tag.
     */
    private static final String DESCRIPTORS = "descriptors";
    /**
     * Definitions path element and tag.
     */
    private static final String DEFINITIONS = "definitions";
    /**
     * Executions path element and tag.
     */
    private static final String EXECUTIONS = "executions";

    private static final String JOBS_EXPORT_SUCCESS = "app.user.events.export.jobs.success";
    private static final String JOBS_EXPORT_FAIL = "app.user.events.export.jobs.fail";
    private static final String JOBS_IMPORT_FAIL = "app.user.events.import.jobs.result.failed";
    private static final String JOBS_IMPORT_SUCCESS = "app.user.events.import.jobs.result.success";
    private static final String JOBS_IMPORT = "app.user.events.import.jobs.result";

    private static final String JOBS_IMPORT_OPERATION_NAME = "JOBS_IMPORT";
    private static final String JOBS_EXPORT_OPERATION_NAME = "JOBS_EXPORT";

    /** The Constant DATA_PARAM_FILE. */
    private static final String DATA_PARAM_FILE = "file";

    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JobRestService.class);

    @Autowired
    private JobDescriptorConverter jobDescriptorConverter;

    @Autowired
    private JobDefinitionConverter jobDefinitionConverter;

    @Autowired
    private JobExecutionConverter jobExecutionConverter;

    @Autowired
    private ExecutionStateConverter executionStateConverter;

    @Autowired
    private StepExecutionConverter stepExecutionConverter;

    @Autowired
    private JobService jobService;

    @Autowired
    private AsyncExecutor asyncExecutor;

    @Autowired
    private UserService userService;

    @Autowired
    private LargeObjectsService largeObjectsService;

    @Autowired
    private SecurityService securityService;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Descriptors
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Find all job descriptors.
     *
     * @return the response
     * @throws Exception the exception
     */
    @GET
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + DESCRIPTORS)
    @Operation(
        description = "Gets job descriptors, registered by the system.",
        method = HttpMethod.GET,
        tags = DESCRIPTORS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetJobDescriptorsResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response findDescriptors() {

        JobDescriptorsGetResult result = jobService.descriptors(JobDescriptorsGetContext.builder()
                .all(true)
                .build());

        return ok(new GetJobDescriptorsResultRO(jobDescriptorConverter.to(result.getDescriptors())));
    }

    /**
     * Find all job descriptors.
     *
     * @return the response
     * @throws Exception the exception
     */
    @GET
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + DESCRIPTORS + "/{name}")
    @Operation(
        description = "Gets a specific job descriptor by name.",
        method = HttpMethod.GET,
        tags = DESCRIPTORS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetJobDescriptorsResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response findDescriptor(@Parameter(description = "Job name/id.", in = ParameterIn.PATH) @PathParam(value = "name") String name) {

        JobDescriptorsGetResult result = jobService.descriptors(JobDescriptorsGetContext.builder()
                .name(name)
                .build());

        return ok(new GetJobDescriptorsResultRO(jobDescriptorConverter.to(result.getDescriptors())));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // END OF Descriptors
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Definitions.
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Find all.
     *
     * @return the response
     * @throws Exception the exception
     */
    @GET
    @Path(value = "/" + DEFINITIONS)
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Operation(
        description = "Gets all currently available job definitions with short digest (state of the last execution etc.).",
        method = HttpMethod.GET,
        tags = DEFINITIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetJobDefinitionsWithStateResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response findAllDefinitionsWithState() {

        JobDefinitionsQueryResult retval = jobService.definitions(JobDefinitionsQueryContext.builder()
                .all(true)
                .build());

        GetJobDefinitionsWithStateResultRO result = new GetJobDefinitionsWithStateResultRO();
        result.setTotalCount(retval.getTotalCount());
        result.setDefinitions(retval.getDefinitions().entrySet().stream()
                .map(entry -> {

                    JobDefinitionWithStateRO jdwi = new JobDefinitionWithStateRO();
                    jdwi.setDefinition(jobDefinitionConverter.to(entry.getKey()));
                    jdwi.setState(executionStateConverter.to(entry.getValue()));

                    return jdwi;
                })
                .collect(Collectors.toList()));

        return ok(result);
    }
    /**
     * Find jobs page.
     *
     * @param fromInd the from ind
     * @param itemCount the item count
     * @param status the status
     * @return the response
     * @throws Exception the exception
     */
    @POST
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + DEFINITIONS + "/search")
    @Operation(
        description = "Gets job definitions in paginated fashion.",
        method = HttpMethod.POST,
        tags = DEFINITIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetJobDefinitionsWithStateResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response searchJobsDefinitions(@Parameter(description = "The request") SearchJobDefinitionsRO request) {

        JobDefinitionsQueryResult retval = jobService.definitions(JobDefinitionsQueryContext.builder()
                .activeOnly(request.isActiveOnly())
                .inactiveOnly(request.isInactiveOnly())
                .definitionName(StringUtils.defaultIfBlank(request.getName(), null))
                .jobName(StringUtils.defaultIfBlank(request.getJobName(), null))
                .createdBy(StringUtils.defaultIfBlank(request.getCreatedBy(), null))
                .tags(CollectionUtils.isNotEmpty(request.getTags()) ? request.getTags() : null)
                .lastFinishedWith(StringUtils.isBlank(request.getLastExecutionStatus()) ? null : JobExecutionStatus.fromValue(request.getLastExecutionStatus()))
                .from(request.getStart())
                .count(request.getCount())
                .sortBy(StringUtils.defaultIfBlank(request.getSortField(), null), StringUtils.defaultIfBlank(request.getSortOrder(), null))
                .build());

        GetJobDefinitionsWithStateResultRO result = new GetJobDefinitionsWithStateResultRO();
        result.setTotalCount(retval.getTotalCount());
        result.setDefinitions(retval.getDefinitions().entrySet().stream()
                .map(entry -> {

                    JobDefinitionWithStateRO jdwi = new JobDefinitionWithStateRO();
                    jdwi.setDefinition(jobDefinitionConverter.to(entry.getKey()));
                    jdwi.setState(executionStateConverter.to(entry.getValue()));

                    return jdwi;
                })
                .collect(Collectors.toList()));

        return ok(result);
    }

    /**
     * Save job.
     *
     * @param jobId the job id
     * @param job the job
     * @return the response
     * @throws Exception the exception
     */
    @PUT
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
         + " T(org.unidata.mdm.core.util.SecurityUtils).isUpdateRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + DEFINITIONS)
    @Operation(
        description = "Create or update a job definition.",
        method = HttpMethod.POST,
        tags =  DEFINITIONS,
        requestBody = @RequestBody(
            content = @Content(schema = @Schema(implementation = UpsertJobDefinitionRO.class)),
            description = "The job definition."
        ),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetJobDefinitionResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response upsertDefinition(@Parameter(description = "Upserts job definition with flags.") UpsertJobDefinitionRO request) {

        JobDefinitionUpsertResult result = jobService.upsert(JobDefinitionUpsertContext.builder()
            .definition(jobDefinitionConverter.from(request.getDefinition()))
            .skipCronWarnings(request.isSkipCronWarnings())
            .build());

        return ok(new GetJobDefinitionResultRO(jobDefinitionConverter.to(result.getDefinition())));
    }
    /**
     * Mark job.
     *
     * @param jobDefinitionId the job id
     * @param enabled the enabled
     * @return the response
     * @throws Exception the exception
     */
    @POST
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser()  or "
         + " T(org.unidata.mdm.core.util.SecurityUtils).isUpdateRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + DEFINITIONS + "/enable/{jobDefinitionId}/{enabled}")
    @Operation(
        description = "Does activate / deactivate a job definition.",
        method = HttpMethod.POST,
        tags =  DEFINITIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = JobGeneralSuccessRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response enableDefinition(
            @Parameter(description = "Идентификатор задачи", in = ParameterIn.PATH) @PathParam("jobDefinitionId") final Long jobDefinitionId,
            @Parameter(description = "Флаг состояния", in = ParameterIn.PATH) @PathParam("enabled") final boolean enabled) {

        jobService.enable(JobDefinitionsEnableContext.builder()
                .jobDefinitionId(jobDefinitionId)
                .enable(enabled)
                .build());

        return ok(new JobGeneralSuccessRO(true));
    }
    /**
     * Removes a job definition.
     *
     * @param jobDefinitionId the job id
     * @return the response
     * @throws Exception the exception
     */
    @DELETE
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser()  or"
         + " T(org.unidata.mdm.core.util.SecurityUtils).isDeleteRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + DEFINITIONS + "/{jobDefinitionId}")
    @Operation(
        description = "Removes a job definition by id.",
        method = HttpMethod.DELETE,
        tags = DEFINITIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = JobGeneralSuccessRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response removeDefinition(@Parameter(description = "Job definition ID.", in = ParameterIn.PATH) @PathParam("jobDefinitionId") final Long jobDefinitionId) {

        jobService.remove(JobDefinitionsRemoveContext.builder()
                .jobDefinitionId(jobDefinitionId)
                .stop(true)
                .build());

        return ok(new JobGeneralSuccessRO(true));
    }
    /**
     * Export jobs.
     *
     * @param jobsIds the jobs ids
     * @return the response
     * @throws Exception the exception
     */
    @POST
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + DEFINITIONS + "/export")
    @Operation(
        description = "Exports all (or just selected) job definitions to JSON.",
        method = HttpMethod.POST,
        tags = DEFINITIONS,
        requestBody = @RequestBody(
                content = @Content(schema = @Schema(implementation = ExportJobDefinitionsRequestRO.class)),
                description = "Optional list of job IDs."),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = JobGeneralSuccessRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response exportDefinitions(ExportJobDefinitionsRequestRO request) {

        final JobDefinitionsQueryResult selection = jobService.definitions(JobDefinitionsQueryContext.builder()
                .all(CollectionUtils.isEmpty(request.getJobDefinitionsIds()))
                .jobDefinitionIds(request.getJobDefinitionsIds())
                .build());

        if (MapUtils.isNotEmpty(selection.getDefinitions())) {

            final String userToken = SecurityUtils.getCurrentUserToken();
            asyncExecutor.async(() -> selection.getDefinitions().keySet())
                .thenApply(jobDefinitionConverter::to)
                .thenApply(JobDefinitionsRO::new)
                .thenApply(JsonUtils::write)
                .whenComplete((json, ex) -> {

                    securityService.authenticate(userToken, true);

                    if (exportWithoutSuccess(ex)) {
                        return;
                    }

                    exportWithSuccess(json);
            });
        }

        return ok(new JobGeneralSuccessRO(true));
    }
    /**
     * Import jobs.
     *
     * @param fileAttachment JSON file with jobs definitions.
     * @return the response
     */
    @POST
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or ("
          + "T(org.unidata.mdm.core.util.SecurityUtils).isCreateRightsForResource('"+ ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "') and "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isUpdateRightsForResource('"+ ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "') and "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isDeleteRightsForResource('"+ ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "'))")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path(value = "/" + DEFINITIONS + "/import")
    @Operation(
        description = "Import job definitions.",
        method = HttpMethod.POST,
        tags = DEFINITIONS,
        requestBody = @RequestBody(content =
                @Content(
                        mediaType = MediaType.MULTIPART_FORM_DATA,
                        schema = @Schema(implementation = JobsUploadDescriptor.class),
                        encoding = {@Encoding(name = DATA_PARAM_FILE, contentType = "application/json")}),
                description = "Attachment content."),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = JobGeneralSuccessRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response importDefinitions(
            @Parameter(description = "File, containing job definitions in the same format, as is used by the export operation.")
            @Multipart(value = DATA_PARAM_FILE) final Attachment fileAttachment) {

        if (Objects.isNull(fileAttachment)) {
            return okOrNotFound(null);
        }

        final String userToken = SecurityUtils.getCurrentUserToken();
        final List<JobDefinition> input = importAttachmentToJobsList(fileAttachment);
        asyncExecutor.async(() -> input)
            .whenComplete((jobs, ex) -> {

                securityService.authenticate(userToken, true);
                if (importWithoutSuccess(fileAttachment, ex, jobs)) {
                    return;
                }

                int failedJobs = 0;
                int savedJobs = 0;
                StringBuilder details = new StringBuilder();
                for (JobDefinition jd : jobs) {

                    boolean success = true;
                    try {

                        jobService.upsert(JobDefinitionUpsertContext.builder()
                                .definition(jd)
                                .skipCronWarnings(true)
                                .checkByName(true)
                                .build());

                        details.append(TextUtils.getText(JOBS_IMPORT_SUCCESS, jd.getDisplayName()))
                               .append(System.lineSeparator());

                    } catch (Exception e) {

                        importAppendException(jd, e, details);
                        success = false;
                    }

                    if (success) {
                        savedJobs++;
                    } else {
                        failedJobs++;
                    }
                }

                importWithSuccess(details, savedJobs, failedJobs);
            });

        return ok(new JobGeneralSuccessRO(true));
    }
    /**
     * Get all job tags.
     *
     * @return the response
     * @throws Exception the exception
     */
    @GET
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
         + " T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + DEFINITIONS + "/tags")
    @Operation(
        description = "Loads all tags, used on definition objects.",
        method = HttpMethod.GET,
        tags = DEFINITIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetTagsResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response tags() {
        return ok(new GetTagsResultRO(jobService.tags()));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // END OF Definitions.
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Start job.
     *
     * @param jobDefinitionId the job id
     * @return the response
     * @throws Exception the exception
     */
    @POST
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + EXECUTE_DATA_OPERATIONS + "')")
    @Path(value = "/" + EXECUTIONS + "/start/{jobDefinitionId}")
    @Operation(
        description = "Starts a previously defined job.",
        method = HttpMethod.POST,
        tags =  EXECUTIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetJobExecutionResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response startExecution(
            @Parameter(description = "A job definition id.", in = ParameterIn.PATH) @PathParam("jobDefinitionId") final Long jobDefinitionId) {

        JobExecutionStartResult result = jobService.start(JobExecutionStartContext.builder()
                .jobDefinitionId(jobDefinitionId)
                .build());

        return ok(new GetJobExecutionResultRO(jobExecutionConverter.to(result.getExecution())));
    }
    /**
     * Restart job.
     *
     * @param jobExecutionId the job execution id
     * @return the response
     * @throws Exception the exception
     */
    @POST
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + EXECUTE_DATA_OPERATIONS + "')")
    @Path(value = "/" + EXECUTIONS + "/restart/{jobExecutionId}")
    @Operation(
        description = "Re-starts a previous start attempts. The argument is an EXECUTION id.",
        method = HttpMethod.POST,
        tags =  EXECUTIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetJobExecutionResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response restartExecution(@Parameter(description = "Job execution ID.", in = ParameterIn.PATH) @PathParam("jobExecutionId") final Long jobExecutionId) {

        JobExecutionStartResult result = jobService.start(JobExecutionStartContext.builder()
                .jobExecutionId(jobExecutionId)
                .restart(true)
                .build());

        return ok(new GetJobExecutionResultRO(jobExecutionConverter.to(result.getExecution())));
    }
    /**
     * Stop job.
     *
     * @param jobDefinitionId the job id
     * @return the response
     * @throws Exception the exception
     */
    @POST
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
         + " T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + EXECUTE_DATA_OPERATIONS + "')")
    @Path(value =  "/" + EXECUTIONS + "/stop-latest/{jobDefinitionId}")
    @Operation(
        description = "Stops the most recent running execution of a job definition with the given ID.",
        method = HttpMethod.POST,
        tags =  EXECUTIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = JobGeneralSuccessRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response stopExecutionByDefinitionId(@Parameter(description = "Job definition ID", in = ParameterIn.PATH) @PathParam("jobDefinitionId") final Long jobDefinitionId) {

        jobService.stop(JobExecutionStopContext.builder()
                .jobDefinitionId(jobDefinitionId)
                .build());

        return ok(new JobGeneralSuccessRO(true));
    }
    /**
     * Stop job.
     *
     * @param jobExecutionId the job id
     * @return the response
     * @throws Exception the exception
     */
    @POST
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
         + " T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + EXECUTE_DATA_OPERATIONS + "')")
    @Path(value =  "/" + EXECUTIONS + "/stop/{jobExecutionId}")
    @Operation(
        description = "Stops a concrete running execution of a job with the given execution ID.",
        method = HttpMethod.POST,
        tags =  EXECUTIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = JobGeneralSuccessRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response stopExecutionByExecutionId(@Parameter(description = "Job execution ID", in = ParameterIn.PATH) @PathParam("jobExecutionId") final Long jobExecutionId) {

        jobService.stop(JobExecutionStopContext.builder()
                .jobExecutionId(jobExecutionId)
                .build());

        return ok(new JobGeneralSuccessRO(true));
    }
    /**
     * Find job executions page.
     *
     * @param jobDefinitionId the job id
     * @param fromIndex the from ind
     * @param itemsCount the item count
     * @return the response
     * @throws Exception the exception
     */
    @GET
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + EXECUTIONS + "/definition/{jobDefinitionId}/{fromIndex}/{itemsCount}")
    @Operation(
        description = "Gets a portion of executions in paginated fashion.",
        method = HttpMethod.GET,
        tags =  EXECUTIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetJobExecutionsResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response findExecutions(
            @Parameter(description = "The job definition id") @PathParam("jobDefinitionId") final Long jobDefinitionId,
            @Parameter(description = "From index") @PathParam("fromIndex") @DefaultValue("0") final long fromIndex,
            @Parameter(description = "Count") @PathParam("itemsCount") @DefaultValue("0") final int itemsCount) {

        JobExecutionsQueryResult result = jobService.executions(JobExecutionQueryContext.builder()
                .jobDefinitionId(jobDefinitionId)
                .from(fromIndex > 0 ? Long.valueOf(fromIndex) : null)
                .count(itemsCount > 0 ? Integer.valueOf(itemsCount) : null)
                .restartable(true)
                .build());

        return ok(new GetJobExecutionsResultRO(jobExecutionConverter.to(result.getPage()), result.getTotalCount()));
    }
    /**
     * Find job executions page.
     *
     * @param jobDefinitionId the job id
     * @param fromIndex the from ind
     * @param itemsCount the item count
     * @return the response
     * @throws Exception the exception
     */
    @GET
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
          + "T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + EXECUTIONS + "/name/{jobName}/{fromIndex}/{itemsCount}")
    @Operation(
        description = "Gets a portion of executions in paginated fashion.",
        method = HttpMethod.GET,
        tags =  EXECUTIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetJobExecutionsResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response findExecutions(
            @Parameter(description = "The job definition id") @PathParam("jobName") final String jobName,
            @Parameter(description = "From index") @PathParam("fromIndex") @DefaultValue("0") final long fromIndex,
            @Parameter(description = "Count") @PathParam("itemsCount") @DefaultValue("0") final int itemsCount) {

        JobExecutionsQueryResult result = jobService.executions(JobExecutionQueryContext.builder()
                .jobName(jobName)
                .from(fromIndex > 0 ? Long.valueOf(fromIndex) : null)
                .count(itemsCount > 0 ? Integer.valueOf(itemsCount) : null)
                .restartable(false)
                .build());

        return ok(new GetJobExecutionsResultRO(jobExecutionConverter.to(result.getPage()), result.getTotalCount()));
    }
    /**
     * Find step executions page.
     *
     * @param jobExecutionId the job execution id
     * @param fromIndex the from ind
     * @param itemsCount the item count
     * @return the response
     * @throws Exception the exception
     */
    @GET
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
         + " T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + EXECUTIONS + "/steps/{jobExecutionId}/{fromIndex}/{itemsCount}")
    @Operation(
        description = "Gets step executions by job EXECUTION id.",
        method = HttpMethod.GET,
        tags =  EXECUTIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetStepExecutionsResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response findSteps(
            @Parameter(description = "Job execution id") @PathParam("jobExecutionId") final Long jobExecutionId,
            @Parameter(description = "From index") @PathParam("fromIndex") final long fromIndex,
            @Parameter(description = "Count") @PathParam("itemsCount") final int itemsCount) {

        StepExecutionQueryResult result = jobService.steps(StepExecutionQueryContext.builder()
                .jobExecutionId(jobExecutionId)
                .from(fromIndex == 0 ? null : fromIndex)
                .count(itemsCount == 0 ? null : itemsCount)
                .build());

        return ok(new GetStepExecutionsResultRO(stepExecutionConverter.to(result.getPage()), result.getTotalCount()));
    }
    /**
     * Gets the job execution status.
     * @param jobDefinitionId the job id
     */
    @GET
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
         + " T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + EXECUTIONS + "/definition-status/{jobDefinitionId}")
    @Operation(
        description = "Returns the status of the last started execution by definition id.",
        method = HttpMethod.GET,
        tags =  EXECUTIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = StatusJobExecutionResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response executionStatusByDefinitionId(@PathParam("jobDefinitionId") long jobDefinitionId) {

        JobExecutionStatusResult result = jobService.status(JobExecutionStatusContext.builder()
                .jobDefinitionId(jobDefinitionId)
                .progress(true)
                .build());

        return ok(new StatusJobExecutionResultRO(jobExecutionConverter.to(result.getExecution()), result.getProgress()));
    }
    /**
     * Gets the job execution status.
     * @param jobDefinitionId the job id
     */
    @GET
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
         + " T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + EXECUTIONS + "/name-status/{jobName}")
    @Operation(
        description = "Returns the status of the last started system job execution by name.",
        method = HttpMethod.GET,
        tags =  EXECUTIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = StatusJobExecutionResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response executionStatusByName(@PathParam("jobName") String jobName) {

        JobExecutionStatusResult result = jobService.status(JobExecutionStatusContext.builder()
                .jobName(jobName)
                .progress(true)
                .build());

        return ok(new StatusJobExecutionResultRO(jobExecutionConverter.to(result.getExecution()), result.getProgress()));
    }
    /**
     * Gets the job execution status.
     * @param jobDefinitionId the job id
     */
    @GET
    @PreAuthorize(
            "T(org.unidata.mdm.core.util.SecurityUtils).isAdminUser() or "
         + " T(org.unidata.mdm.core.util.SecurityUtils).isReadRightsForResource('" + ADMIN_SYSTEM_MANAGEMENT + "," + DATA_OPERATIONS_MANAGEMENT + "')")
    @Path(value = "/" + EXECUTIONS + "/execution-status/{jobExecutionId}")
    @Operation(
        description = "Returns the status of an execution by execution ID.",
        method = HttpMethod.GET,
        tags =  EXECUTIONS,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = StatusJobExecutionResultRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response executionStatusByExecutionId(@PathParam("jobExecutionId") long jobExecutionId) {

        JobExecutionStatusResult result = jobService.status(JobExecutionStatusContext.builder()
                .jobExecutionId(jobExecutionId)
                .progress(true)
                .build());

        return ok(new StatusJobExecutionResultRO(jobExecutionConverter.to(result.getExecution()), result.getProgress()));
    }
    private void exportWithSuccess(String json) {

        try (final InputStream is = new ByteArrayInputStream(json.getBytes())) {

            final UserEventDTO event = userService.upsert(UpsertUserEventRequestContext.builder()
                    .login(SecurityUtils.getCurrentUserName())
                    .type(JOBS_EXPORT_OPERATION_NAME)
                    .content(TextUtils.getText(JOBS_EXPORT_SUCCESS))
                    .build());

            largeObjectsService.saveLargeObject(UpsertLargeObjectContext.builder()
                    .subjectId(event.getId())
                    .mimeType("application/json")
                    .binary(false)
                    .input(is)
                    .filename(exportConstructFileName())
                    .build());

        } catch (IOException e) {
            exportWithoutSuccess(e);
        }
    }

    private String exportConstructFileName() {

        try {
            String ts = DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd_HH-mm-ss");
            return URLEncoder.encode("jobs_" + ts + ".json", StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error generating jobs file name", e);
        }

        return "jobs.json";
    }

    private boolean exportWithoutSuccess(final Throwable ex) {

        if (ex != null) {

            LOGGER.error("Export jobs operation failed.", ex);
            userService.upsert(UpsertUserEventRequestContext.builder()
                    .login(SecurityUtils.getCurrentUserName())
                    .type(JOBS_EXPORT_OPERATION_NAME)
                    .content(TextUtils.getText(JOBS_EXPORT_FAIL))
                    .details(ex.getMessage())
                    .build());
            return true;
        }

        return false;
    }

    private void importAppendException(JobDefinition job, Exception ex, StringBuilder details) {

        LOGGER.error("Failed to import job with jobReferenceName [{}], jobName [{}].",
                job.getJobName(), job.getDisplayName(), ex);

        details.append(TextUtils.getText(JOBS_IMPORT_FAIL, job.getDisplayName()));

        if (ex instanceof JobException) {

            JobException je = (JobException) ex;
            details.append(ReportUtil.SPACE)
                   .append(TextUtils.getText(je.getId().code(), je.getArgs()));
        }

        details.append(System.lineSeparator());
    }

    private boolean importWithoutSuccess(Attachment fileAttachment, Throwable ex, List<JobDefinition> jobs) {

        final String user = SecurityUtils.getCurrentUserName();
        if (ex != null) {
            LOGGER.error("Failed to import jobs from [{}].", fileAttachment.getContentDisposition().getFilename(), ex);
            userService.upsert(UpsertUserEventRequestContext.builder()
                    .login(user)
                    .type(JOBS_IMPORT_OPERATION_NAME)
                    .content(TextUtils.getText(JOBS_IMPORT, "0", jobs.size(), System.lineSeparator()))
                    .details(ex.getMessage())
                    .build());

            return true;
        } else if (CollectionUtils.isEmpty(jobs)) {
            LOGGER.warn("Skip import jobs from [{}]. Zero length input.", fileAttachment.getContentDisposition().getFilename());
            userService.upsert(UpsertUserEventRequestContext.builder()
                    .login(user)
                    .type(JOBS_IMPORT_OPERATION_NAME)
                    .content(TextUtils.getText(JOBS_IMPORT, "0", jobs.size(), System.lineSeparator()))
                    .build());

            return true;
        }

        return false;
    }

    private void importWithSuccess(StringBuilder details, int savedJobs, int failedJobs) {

        userService.upsert(UpsertUserEventRequestContext.builder()
                .login(SecurityUtils.getCurrentUserName())
                .type(JOBS_IMPORT_OPERATION_NAME)
                .content(TextUtils.getText(JOBS_IMPORT, savedJobs, failedJobs, System.lineSeparator()))
                .details(details.toString())
                .build());
    }

    private List<JobDefinition> importAttachmentToJobsList(final Attachment fileAttachment) {

        JobDefinitionsRO input = null;
        java.nio.file.Path path = FileUtils.saveFileTempFolder(fileAttachment);
        try (FileInputStream fis = new FileInputStream(path.toFile())) {
            input = JsonUtils.read(fis, JobDefinitionsRO.class);
        } catch (Exception e) {
            throw new PlatformFailureException("Jobs import failed!", e, CoreRestExceptionIds.EX_CORE_JOBS_IMPORT_FAILED);
        } finally {
            try { Files.delete(path); } catch (Exception e) { /* NOP */ }
        }

        return Objects.isNull(input) || CollectionUtils.isEmpty(input.getDefinitions())
                ? Collections.emptyList()
                : jobDefinitionConverter.from(input.getDefinitions());
    }

    /**
     * @author Mikhail Mikhailov on Feb 2, 2021
     * Not really a functional class, but a OpenAPI helper.
     */
    static class JobsUploadDescriptor {
        @Schema(name = DATA_PARAM_FILE, description = "Upload content.", type = "string", format = "binary")
        public String file;
    }
}
