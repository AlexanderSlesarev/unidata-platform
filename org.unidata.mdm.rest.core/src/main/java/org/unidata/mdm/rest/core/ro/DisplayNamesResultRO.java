/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro;

import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.rest.core.ro.display.DisplayNameRO;
import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Nov 16, 2021
 */
public class DisplayNamesResultRO extends DetailedOutputRO {
    /**
     * Names, keyed by UPath spec.
     */
    @Schema(description = "Display names with identity information.")
    private List<DisplayNameRO> result;
    /**
     * Constructor.
     */
    public DisplayNamesResultRO() {
        super();
    }
    /**
     * Constructor.
     */
    public DisplayNamesResultRO(List<DisplayNameRO> result) {
        super();
        setResult(result);
    }
    /**
     * @return the names
     */
    public List<DisplayNameRO> getResult() {
        return CollectionUtils.isEmpty(result) ? Collections.emptyList() : result;
    }
    /**
     * @param result the names to set
     */
    public void setResult(List<DisplayNameRO> result) {
        this.result = result;
    }
}
