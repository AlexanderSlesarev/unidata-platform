/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.job;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Jun 29, 2021
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExecutionStateRO {

    @Schema(allowableValues = {"COMPLETED", "STARTING", "STARTED", "STOPPING", "STOPPED", "FAILED", "ABANDONED", "UNKNOWN"})
    private String status;
    /**
     * The exit code (if finished).
     */
    private String exitCode;
    /**
     * Exit description (if finished).
     */
    private String exitDescription;
    /**
     * Constructor.
     */
    public ExecutionStateRO() {
        super();
    }
    /**
     * Constructor.
     */
    public ExecutionStateRO(String lastExecutionStatus) {
        super();
        this.status = lastExecutionStatus;
    }
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String lastExecutionStatus) {
        this.status = lastExecutionStatus;
    }
    /**
     * @return the exitCode
     */
    public String getExitCode() {
        return exitCode;
    }
    /**
     * @param exitCode the exitCode to set
     */
    public void setExitCode(String exitCode) {
        this.exitCode = exitCode;
    }
    /**
     * @return the exitDescription
     */
    public String getExitDescription() {
        return exitDescription;
    }
    /**
     * @param exitDescription the exitDescription to set
     */
    public void setExitDescription(String exitDescription) {
        this.exitDescription = exitDescription;
    }
}
