/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.converter;

import java.util.Objects;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.job.AbstractExecutionState;
import org.unidata.mdm.rest.core.ro.job.ExecutionStateRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Jul 6, 2021
 */
@Component
public class ExecutionStateConverter extends Converter<AbstractExecutionState<?>, ExecutionStateRO> {

    /**
     * Constructor.
     */
    public ExecutionStateConverter() {
        super();
        this.to = this::convert;
    }

    private ExecutionStateRO convert(AbstractExecutionState<?> source) {

        ExecutionStateRO target = new ExecutionStateRO();

        target.setStatus(Objects.nonNull(source.getStatus()) ? source.getStatus().name() : null);
        target.setExitCode(source.getExitCode());
        target.setExitDescription(source.getExitDescription());

        return target;
    }
}
