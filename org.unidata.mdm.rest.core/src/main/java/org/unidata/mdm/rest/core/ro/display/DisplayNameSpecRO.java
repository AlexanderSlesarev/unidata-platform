/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.display;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Dmitriy Bobrov on Nov 15, 2021
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "The structure allows queries for display information of several kinds (namespace and typename are always required) : \n"
 + "- Subject is some data ID (record, rel, etc.), qualifier is empty. A successful request of such kind will return display information for a record according to model settings.\n"
 + "- Subject is some code|unique value and qualifier is not empty. A successful request of such kind will return display information for a record if qualifier denotes code|unique attribute name.\n")
public class DisplayNameSpecRO {

    @Schema(description = "Target namespace.")
    private String nameSpace;

    @Schema(description = "Target type name.")
    private String typeName;

    @Schema(description = "A record|relation ID, code|unique attribute value, other.")
    private Object subject;

    @Schema(description = "Qualifying path, (most probably code or unique attribute name).")
    private String qualifier;

    @Schema(description = "Deleted records should be consdiered.")
    private boolean deleted;

    @Schema(description = "Inactive records should be consdiered.")
    private boolean inactive;

    @Schema(description = "Period start (as local TS).")
    private LocalDateTime validFrom;

    @Schema(description = "Period end (as local TS).")
    private LocalDateTime validTo;

    @Schema(description = "A possibly unusual set of fields to return as display fields. Default from model returned if null or empty.")
    private List<String> fields;

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String upath) {
        this.nameSpace = upath;
    }

    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param typeName the typeName to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Object getSubject() {
        return subject;
    }

    public void setSubject(Object subject) {
        this.subject = subject;
    }

    public String getQualifier() {
        return qualifier;
    }

    public void setQualifier(String qualification) {
        this.qualifier = qualification;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }

    /**
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * @return the inactive
     */
    public boolean isInactive() {
        return inactive;
    }

    /**
     * @param inactive the inactive to set
     */
    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    /**
     * @return the fields
     */
    public List<String> getFields() {
        return fields;
    }

    /**
     * @param fields the fields to set
     */
    public void setFields(List<String> fields) {
        this.fields = fields;
    }
}
