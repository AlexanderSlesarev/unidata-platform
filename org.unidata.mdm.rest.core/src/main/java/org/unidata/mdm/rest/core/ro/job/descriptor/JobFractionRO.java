/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.job.descriptor;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * @author Mikhail Mikhailov on Jun 18, 2021
 */
public class JobFractionRO {
    /**
     * This fraction id.
     */
    private String id;
    /**
     * This fraction display name.
     */
    private String displayName;
    /**
     * This fraction description.
     */
    private String description;
    /**
     * This fraction default order.
     */
    private int defaultOrder;
    /**
     * Parameters.
     */
    private Collection<JobParameterDescriptorRO> parameters;
    /**
     * Constructor.
     */
    public JobFractionRO() {
        super();
    }
    /**
     * Gets this fraction ID.
     * @return ID
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * Gets this fraction display name
     * @return display name
     */
    public String getDisplayName() {
        return displayName;
    }
    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    /**
     * Gets this fraction description.
     * @return fraction description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * Additional job params, added by a fraction.
     */
    public Collection<JobParameterDescriptorRO> getParameters() {
        return Objects.isNull(parameters) ? Collections.emptyList() : parameters;
    }
    /**
     * @param parameters the parameters to set
     */
    public void setParameters(Collection<JobParameterDescriptorRO> parameters) {
        this.parameters = parameters;
    }
    /**
     * @return the defaultOrder
     */
    public int getDefaultOrder() {
        return defaultOrder;
    }
    /**
     * @param defaultOrder the defaultOrder to set
     */
    public void setDefaultOrder(int defaultOrder) {
        this.defaultOrder = defaultOrder;
    }
}
