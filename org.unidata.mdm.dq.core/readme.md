# DQ Core Module

# backend.properties:
org.unidata.mdm.dq.core.datasource.username=postgres
org.unidata.mdm.dq.core.datasource.password=postgres
org.unidata.mdm.dq.core.datasource.url=jdbc:postgresql://host:port/database?currentSchema=com_unidata_mdm_dq_core&reWriteBatchedInserts=true&ApplicationName=DataQuality-Core
org.unidata.mdm.dq.core.datasource.driverClassName=org.postgresql.Driver
org.unidata.mdm.dq.core.datasource.initialSize=10
org.unidata.mdm.dq.core.datasource.maxActive=10
org.unidata.mdm.dq.core.datasource.maxIdle=10
org.unidata.mdm.dq.core.datasource.minIdle=10
org.unidata.mdm.dq.core.datasource.minEvictableIdleTimeMillis=60000
org.unidata.mdm.dq.core.datasource.timeBetweenEvictionRunsMillis=30000
org.unidata.mdm.dq.core.datasource.removeAbandoned=true
org.unidata.mdm.dq.core.datasource.removeAbandonedTimeout=360
org.unidata.mdm.dq.core.datasource.jdbcInterceptors=ResetAbandonedTimer
org.unidata.mdm.dq.core.datasource.logAbandoned=true
org.unidata.mdm.dq.core.datasource.suspectTimeout=60
org.unidata.mdm.dq.core.datasource.testOnBorrow=true
org.unidata.mdm.dq.core.datasource.validationQuery=SELECT 1
org.unidata.mdm.dq.core.datasource.validationInterval=30000
org.unidata.mdm.dq.core.datasource.type=javax.sql.DataSource
