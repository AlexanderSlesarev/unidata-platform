-- data quality model table
create table data_quality (
    storage_id varchar(128) not null,
    revision integer not null,
    description text,
    operation_id varchar(128),
    content bytea not null,
    created_by varchar(256) not null,
    create_date timestamptz not null default current_timestamp,
    constraint pk_data_quality_storage_id_revision primary key(storage_id, revision));

drop schema if exists com_unidata_mdm_dq cascade;
drop schema if exists com_unidata_mdm_dq_core cascade;