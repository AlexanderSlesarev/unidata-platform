package org.unidata.mdm.dq.core.type.model.source;

import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionType;

/**
 * @author Mikhail Mikhailov on Jan 23, 2021
 * Python function type.
 */
public class PythonCleanseFunctionSource extends AbstractScriptCleanseFunction<PythonCleanseFunctionSource> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 774074290091881141L;
    /**
     * {@inheritDoc}
     */
    @Override
    public CleanseFunctionType getType() {
        return CleanseFunctionType.PYTHON;
    }
}
