package org.unidata.mdm.dq.core.type.model.source;

import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionType;

/**
 * @author Mikhail Mikhailov on Jan 23, 2021
 * Groovy function type.
 */
public class GroovyCleanseFunctionSource extends AbstractScriptCleanseFunction<GroovyCleanseFunctionSource> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 3659179541363942481L;
    /**
     * {@inheritDoc}
     */
    @Override
    public CleanseFunctionType getType() {
        return CleanseFunctionType.GROOVY;
    }
}
