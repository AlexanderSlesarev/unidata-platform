/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.instance;

import java.util.Collection;

import javax.annotation.Nonnull;

import org.unidata.mdm.core.type.model.CreateUpdateElement;
import org.unidata.mdm.core.type.model.IdentityElement;
import org.unidata.mdm.core.type.model.NamedDisplayableElement;

/**
 * @author Mikhail Mikhailov on Feb 28, 2021
 */
public interface MappingSetElement extends NamedDisplayableElement, IdentityElement, CreateUpdateElement, Iterable<RuleMappingElement> {
    /**
     * Gets rule mappings. Their order is preconfigured and fixed.
     * @return rule mappings collection
     */
    @Nonnull
    Collection<RuleMappingElement> getMappings();
    /**
     * Gets rule mappings by rule name.
     * @param name the rule name
     * @return rule mappings collection or empty collection
     */
    @Nonnull
    Collection<RuleMappingElement> getMapping(String name);
    /**
     * Returns true, if the set holds any number of rule mappings for the given rule name.
     * @param name the rule's name
     * @return true, if the set holds any number of rule mappings for the given rule name
     */
    boolean hasMapping(String name);
}
