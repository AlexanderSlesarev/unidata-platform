/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.instance;

import java.util.Collection;

import org.unidata.mdm.core.type.model.CreateUpdateElement;
import org.unidata.mdm.core.type.model.CustomPropertiesElement;
import org.unidata.mdm.core.type.model.IdentityElement;
import org.unidata.mdm.core.type.model.NamedDisplayableElement;
import org.unidata.mdm.dq.core.context.CleanseFunctionContext;
import org.unidata.mdm.dq.core.dto.CleanseFunctionResult;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionExecutionScope;

/**
 * @author Mikhail Mikhailov on Jan 20, 2021
 * Base cleanse function element.
 */
public interface CleanseFunctionElement extends IdentityElement, NamedDisplayableElement, CustomPropertiesElement, CreateUpdateElement {
    /**
     * Gets input ports collection.
     * @return input ports collection
     */
    Collection<PortElement> getInputPorts();
    /**
     * Gets output ports collection.
     * @return output ports collection
     */
    Collection<PortElement> getOutputPorts();
    /**
     * Gets input port by name.
     * @param name the port name
     * @return input port by name
     */
    PortElement getInputPortByName(String name);
    /**
     * Gets output port by name.
     * @param name the port name
     * @return output port by name
     */
    PortElement getOutputPortByName(String name);
    /**
     * Runs the underlaying cleanse function.
     * @param ctx the context
     * @return execution result
     */
    CleanseFunctionResult execute(CleanseFunctionContext ctx);
    /**
     * Returns true, if this cleanse function is a system one.
     * @return true, if this cleanse function is a system one.
     */
    boolean isSystem();
    /**
     * Returns true, if this cleanse function is ready for execution.
     * @return true, if this cleanse function is ready for execution.
     */
    boolean isReady();
    /**
     * Returns true, if this cleanse function ports can be externally configured.
     * @return true, if this cleanse function ports can be externally configured.
     */
    boolean isConfigurable();
    /**
     * Returns true, if this cleanse function properties, such as name, display name, description and custom properties, can be externally configured.
     * @return true, if this cleanse function properties, such as name, display name, description and custom properties, can be externally configured
     */
    boolean isEditable();
    /**
     * Retiurns true, if the given scope is supported by the cleanse function.
     * @param scope the scope to check
     * @return true, if the given scope is supported by the cleanse function
     */
    boolean supports(CleanseFunctionExecutionScope scope);
    /**
     * Gets state note (if the function can not be initialized this field will contain the reason).
     * Returns an empty string, if all right.
     * @return state note
     */
    String getNote();
    /**
     * Returns true, if this is a composite function.
     * @return true, if this is a composite function
     */
    default boolean isCompositeFunction() {
        return false;
    }
    /**
     * Gets composite function view or null, if this is not a composite function.
     * @return composite function view or null, if this is not a composite function.
     */
    default CompositeFunctionElement getCompositeFunction() {
        return null;
    }
    /**
     * Returns true, if this is a Java function.
     * @return true, if this is a Java function
     */
    default boolean isJavaFunction() {
        return false;
    }
    /**
     * Gets java function view or null, if this is not a java function.
     * @return java function view or null, if this is not a java function.
     */
    default JavaFunctionElement getJavaFunction() {
        return null;
    }
    /**
     * Returns true, if this is a Groovy function.
     * @return true, if this is a Groovy function
     */
    default boolean isGroovyFunction() {
        return false;
    }
    /**
     * Gets groovy function view or null, if this is not a groovy function.
     * @return groovy function view or null, if this is not a groovy function.
     */
    default GroovyFunctionElement getGroovyFunction() {
        return null;
    }
    /**
     * Returns true, if this is a (J)Python function.
     * @return true, if this is a (J)Python function
     */
    default boolean isPythonFunction() {
        return false;
    }
    /**
     * Gets python function view or null, if this is not a python function.
     * @return python function view or null, if this is not a python function.
     */
    default PythonFunctionElement getPythonFunction() {
        return null;
    }
}
