/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.service.impl.instance;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.dq.core.type.model.instance.MappingSetElement;
import org.unidata.mdm.dq.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.dq.core.type.model.source.assignment.EntityAssignmentSource;
import org.unidata.mdm.dq.core.type.model.source.assignment.NameSpaceAssignmentSource;
import org.unidata.mdm.dq.core.type.model.source.assignment.PhaseAssignmentSource;
import org.unidata.mdm.system.type.namespace.NameSpace;
import org.unidata.mdm.system.type.support.IdentityHashSet;
import org.unidata.mdm.system.util.NameSpaceUtils;

/**
 * @author Mikhail Mikhailov on Mar 26, 2021
 * Entity name to mapping sets element.
 */
public class NamespaceAssignmentImpl implements NamespaceAssignmentElement {
    /**
     * The assignments.
     */
    private final Map<String, EntityAssignmentImpl> assignments;
    /**
     * The source.
     */
    private final NameSpaceAssignmentSource source;
    /**
     * Constructor.
     */
    public NamespaceAssignmentImpl(DataQualityInstanceImpl dqi, NameSpaceAssignmentSource source) {
        super();
        this.source = source;
        this.assignments = source.getAssignments().stream()
                .collect(Collectors.toUnmodifiableMap(EntityAssignmentSource::getEntityName, a -> new EntityAssignmentImpl(dqi, a)));
    }
    /**
     * Gets the source.
     * @return the source
     */
    public NameSpaceAssignmentSource getSource() {
        return source;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public NameSpace getNameSpace() {
        return NameSpaceUtils.find(getId());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<String> getEntityNames() {
        return assignments.keySet();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return StringUtils.defaultIfBlank(source.getNameSpace(), NameSpace.GLOBAL_NAMESPACE_ID);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MappingSetElement> getAssigned(String name) {
        EntityAssignmentImpl eai = assignments.get(name);
        return Objects.isNull(eai) ? Collections.emptyList() : eai.getSets();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MappingSetElement> getAssigned(String name, String phase) {
        EntityAssignmentImpl eai = assignments.get(name);
        return Objects.isNull(eai) ? Collections.emptyList() : eai.getSets(phase);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAssigned(String name) {
        return assignments.containsKey(name);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAssigned(String name, String phase) {
        EntityAssignmentImpl eai = assignments.get(name);
        return Objects.nonNull(eai) && eai.isMapped(phase);
    }
    /**
     * @author Mikhail Mikhailov on Nov 2, 2021
     * Phase to set mappings.
     */
    private class EntityAssignmentImpl {
        /**
         * Storage.
         */
        private final Map<String, Collection<MappingSetElement>> phases;
        /**
         * Constructor.
         * @param dqi
         * @param source
         */
        public EntityAssignmentImpl(DataQualityInstanceImpl dqi, EntityAssignmentSource source) {
            this.phases = source.getPhases().stream()
                .collect(Collectors.toUnmodifiableMap(PhaseAssignmentSource::getPhaseName,
                    a -> a.getSets().stream()
                        .map(dqi::getSet)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList())));
        }
        /**
         * Gets mapped sets by phase name.
         * @param phase the phase name
         * @return collection of sets.
         */
        public Collection<MappingSetElement> getSets(String phase) {
            Collection<MappingSetElement> elements = phases.get(phase);
            return Objects.isNull(elements) ? Collections.emptyList() : elements;
        }
        /**
         * Returns true, if the given phase name is mapped to a collection of sets.
         * @param phase the phase name
         * @return true, if the given phase name is mapped to a collection of sets
         */
        public boolean isMapped(String phase) {
            return CollectionUtils.isNotEmpty(phases.get(phase));
        }
        /**
         * Gets all sets for all phases.
         * @return all sets for all phases
         */
        public Collection<MappingSetElement> getSets() {

            // To support basic CE case - no phases, just some sets, assigned to an entity
            if (phases.size() == 1) {
                return phases.values().iterator().next();
            }

            // Since all sets should belong to the same model instance
            // it should be ok to filter objects by identity
            return phases.values().stream()
                    .flatMap(Collection::stream)
                    .collect(Collectors.toCollection(IdentityHashSet::new));
        }
    }
}
