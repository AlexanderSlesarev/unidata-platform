/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.constant;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Cleanse function constant holder.
 */
public class CleanseFunctionConstant implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -1704113436448702340L;
    /**
     * Single value.
     */
    @JacksonXmlProperty(localName = "single")
    private AbstractSingleValueConstant<?, ?> single;
    /**
     * Array value.
     */
    @JacksonXmlProperty(localName = "array")
    private AbstractArrayValueConstant<?, ?> array;
    /**
     * @return the single
     */
    @SuppressWarnings("unchecked")
    public <X> SingleValueConstant<X> getSingle() {
        return (SingleValueConstant<X>) single;
    }
    /**
     * @param single the single to set
     */
    public void setSingle(AbstractSingleValueConstant<?, ?> single) {
        this.single = single;
        this.array = null;
    }
    /**
     * @return the array
     */
    @SuppressWarnings("unchecked")
    public <X> ArrayValueConstant<X> getArray() {
        return (ArrayValueConstant<X>) array;
    }
    /**
     * @param array the array to set
     */
    public void setArray(AbstractArrayValueConstant<?, ?> array) {
        this.array = array;
        this.single = null;
    }

    public boolean isArray() {
        return Objects.nonNull(array);
    }

    public boolean isSingle() {
        return Objects.nonNull(single);
    }

    public boolean isEmpty() {
        return !isArray() && !isSingle();
    }

    public CleanseFunctionConstant withSingle(AbstractSingleValueConstant<?, ?> value) {
        setSingle(value);
        return this;
    }

    public CleanseFunctionConstant withArray(AbstractArrayValueConstant<?, ?> value) {
        setArray(value);
        return this;
    }
}
