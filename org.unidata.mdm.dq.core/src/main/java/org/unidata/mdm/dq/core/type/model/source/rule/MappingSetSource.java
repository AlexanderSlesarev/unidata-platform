/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.rule;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Rule set source.
 */
public class MappingSetSource implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 7123026787966708804L;

    @JacksonXmlProperty(isAttribute = true, localName = "name")
    private String name;

    @JacksonXmlProperty(isAttribute = true, localName = "displayName")
    private String displayName;

    @JacksonXmlProperty(isAttribute = true, localName = "description")
    private String description;

    @JacksonXmlProperty(isAttribute = true)
    private OffsetDateTime createDate;

    @JacksonXmlProperty(isAttribute = true)
    private String createdBy;

    @JacksonXmlProperty(isAttribute = true)
    private OffsetDateTime updateDate;

    @JacksonXmlProperty(isAttribute = true)
    private String updatedBy;

    @JacksonXmlElementWrapper(useWrapping = true, localName = "mappings")
    @JacksonXmlProperty(localName = "mapping")
    private List<RuleMappingSource> mappings;

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setName(String value) {
        this.name = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the value of the createdAt property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public OffsetDateTime getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createdAt property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setCreateDate(OffsetDateTime value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the createdBy property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the updatedAt property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public OffsetDateTime getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the value of the updatedAt property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setUpdateDate(OffsetDateTime value) {
        this.updateDate = value;
    }

    /**
     * Gets the value of the updatedBy property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the value of the updatedBy property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUpdatedBy(String value) {
        this.updatedBy = value;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the dqRule property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dqRule property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDqRule().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QualityRuleSource }
     *
     *
     */
    public List<RuleMappingSource> getMappings() {
        if (mappings == null) {
            mappings = new ArrayList<>();
        }
        return this.mappings;
    }

    public MappingSetSource withName(String value) {
        setName(value);
        return this;
    }

    public MappingSetSource withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    public MappingSetSource withDescription(String value) {
        setDescription(value);
        return this;
    }

    public MappingSetSource withUpdateDate(OffsetDateTime value) {
        setUpdateDate(value);
        return this;
    }

    public MappingSetSource withCreateDate(OffsetDateTime value) {
        setCreateDate(value);
        return this;
    }

    public MappingSetSource withUpdatedBy(String value) {
        setUpdatedBy(value);
        return this;
    }

    public MappingSetSource withCreatedBy(String value) {
        setCreatedBy(value);
        return this;
    }

    public MappingSetSource withMappings(RuleMappingSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withMappings(Arrays.asList(values));
        }
        return this;
    }

    public MappingSetSource withMappings(Collection<RuleMappingSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getMappings().addAll(values);
        }
        return this;
    }
}
