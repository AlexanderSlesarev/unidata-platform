/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.dto;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.dq.core.type.model.source.PhaseSource;

/**
 * @author Mikhail Mikhailov on Nov 9, 2021
 */
public class GetPhasesModelResult {
    /**
     * The phases source.
     */
    private List<PhaseSource> phases;
    /**
     * Constructor.
     */
    public GetPhasesModelResult() {
        super();
    }
    /**
     * Constructor.
     * @param phases the phases
     */
    public GetPhasesModelResult(List<PhaseSource> phases) {
        super();
        this.phases = phases;
    }
    /**
     * @return the phases
     */
    public List<PhaseSource> getPhases() {
        return Objects.isNull(phases) ? Collections.emptyList() : phases;
    }
    /**
     * @param phases the phases to set
     */
    public void setPhases(List<PhaseSource> phases) {
        this.phases = phases;
    }
}
