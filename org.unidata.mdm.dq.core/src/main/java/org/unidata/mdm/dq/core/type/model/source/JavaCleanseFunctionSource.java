/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source;

import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionType;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * <p>Java cleanse function.
 */
public class JavaCleanseFunctionSource extends AbstractCleanseFunctionSource<JavaCleanseFunctionSource> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 5289121963846006126L;

    @JacksonXmlProperty(isAttribute = true)
    protected String javaClass;

    /**
     * {@inheritDoc}
     */
    @Override
    public CleanseFunctionType getType() {
        return CleanseFunctionType.JAVA;
    }

    /**
     * Gets the value of the javaClass property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getJavaClass() {
        return javaClass;
    }

    /**
     * Sets the value of the javaClass property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setJavaClass(String value) {
        this.javaClass = value;
    }

    public JavaCleanseFunctionSource withJavaClass(String value) {
        setJavaClass(value);
        return self();
    }
}
