package org.unidata.mdm.dq.core.po;

import java.util.Date;

/**
 * @author Mikhail Mikhailov on Oct 2, 2020
 * The data quality.
 */
public class DataQualityPO {
    /**
     * Table name.
     */
    public static final String TABLE_NAME = "data_quality";
    /**
     * Storgae ID field.
     */
    public static final String FIELD_STORAGE_ID = "storage_id";
    /**
     * Revision.
     */
    public static final String FIELD_REVISION = "revision";
    /**
     * General description.
     */
    public static final String FIELD_DESCRIPTION = "description";
    /**
     * OPID.
     */
    public static final String FIELD_OPERATION_ID = "operation_id";
    /**
     * Data.
     */
    public static final String FIELD_CONTENT = "content";
    /**
     * Create date.
     */
    public static final String FIELD_CREATE_DATE = "create_date";
    /**
     * Created by.
     */
    public static final String FIELD_CREATED_BY = "created_by";
    /**
     * The storage id.
     */
    private String storageId;
    /**
     * Revision (is also the table PK).
     * Assigned manually causing CV for duplicates.
     */
    private int revision;
    /**
     * General description.
     */
    private String description;
    /**
     * OPID.
     */
    private String operationId;
    /**
     * Data.
     */
    private byte[] content;
    /**
     * Creator's user name.
     */
    private String createdBy;
    /**
     * Create date.
     */
    private Date createDate;
    /**
     * Constructor.
     */
    public DataQualityPO() {
        super();
    }
    /**
     * @return the storageId
     */
    public String getStorageId() {
        return storageId;
    }
    /**
     * @param storageId the storageId to set
     */
    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }
    /**
     * @return the revision
     */
    public int getRevision() {
        return revision;
    }
    /**
     * @param revision the revision to set
     */
    public void setRevision(int revision) {
        this.revision = revision;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the operationId
     */
    public String getOperationId() {
        return operationId;
    }
    /**
     * @param operationId the operationId to set
     */
    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }
    /**
     * @return the content
     */
    public byte[] getContent() {
        return content;
    }
    /**
     * @param content the content to set
     */
    public void setContent(byte[] data) {
        this.content = data;
    }
    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }
    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
