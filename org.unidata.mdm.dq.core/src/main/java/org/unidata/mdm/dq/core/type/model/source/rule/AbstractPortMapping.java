/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.rule;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Port mapping.
 */
public abstract class AbstractPortMapping<X extends AbstractPortMapping<X>> implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 1766493466016318569L;

    @JacksonXmlProperty(isAttribute = true)
    protected String portName;

    /**
     * Gets the value of the portName property.
     *
     * @return portName
     */
    public String getPortName() {
        return portName;
    }
    /**
     * Sets the value of the portName property.
     *
     * @param value the portName
     */
    public void setPortName(String value) {
        this.portName = value;
    }

    public X withPortName(String value) {
        setPortName(value);
        return self();
    }

    @SuppressWarnings("unchecked")
    protected X self() {
        return (X) this;
    }
}
