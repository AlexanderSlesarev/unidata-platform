package org.unidata.mdm.dq.core.service.segments;

import org.springframework.stereotype.Component;
import org.unidata.mdm.dq.core.module.DataQualityModule;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.dto.DraftUpsertResult;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author mikhail
 * @since  13.01.2020
 */
@Component(QualityDraftUpsertStartExecutor.SEGMENT_ID)
public class QualityDraftUpsertStartExecutor extends Start<DraftUpsertContext, DraftUpsertResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataQualityModule.MODULE_ID + "[QUALITY_DRAFT_UPSERT_START]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataQualityModule.MODULE_ID + ".dq.draft.upsert.start.description";
    /**
     * Constructor.
     */
    public QualityDraftUpsertStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftUpsertContext.class, DraftUpsertResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void start(DraftUpsertContext ctx) {
        // NOOP. Start does nothing here.
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String subject(DraftUpsertContext ctx) {
        // No subject for this type of pipelines
        // This may be storage id in the future
        return null;
    }
}
