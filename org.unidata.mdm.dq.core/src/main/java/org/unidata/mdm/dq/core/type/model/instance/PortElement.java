/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.instance;

import java.util.Set;

import org.unidata.mdm.core.type.model.NamedDisplayableElement;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortFilteringMode;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortInputType;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortValueType;

/**
 * @author Mikhail Mikhailov on Jan 20, 2021
 * I/O port description
 */
public interface PortElement extends NamedDisplayableElement {
    /**
     * Tells whether this port is a required one.
     * @return true, this port is a required one, false otherwise
     */
    boolean isRequired();
    /**
     * Gets filtering mode for this port.
     * It is interpreted depending on the port's direction
     * @return filtering mode for this port
     */
    CleanseFunctionPortFilteringMode getFilteringMode();
    /**
     * Gets attribute data type, accepted by the port.
     * @return attribute data type, accepted by the port
     */
    Set<CleanseFunctionPortInputType> getInputTypes();
    /**
     * Gets attribute value type, accepted by the port.
     * @return attribute value type, accepted by the port
     */
    Set<CleanseFunctionPortValueType> getValueTypes();
}
