package org.unidata.mdm.dq.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.dq.core.serialization.DataQualitySerializer;
import org.unidata.mdm.dq.core.service.segments.QualityDraftGetStartExecutor;
import org.unidata.mdm.dq.core.service.segments.QualityDraftPublishStartExecutor;
import org.unidata.mdm.dq.core.service.segments.QualityDraftUpsertStartExecutor;
import org.unidata.mdm.dq.core.type.model.source.DataQualityModel;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.draft.type.DraftOperation;
import org.unidata.mdm.draft.type.DraftProvider;
import org.unidata.mdm.system.service.TextService;

/**
 * @author Mikhail Mikhailov on Oct 14, 2020
 * Data model draft provider.
 */
@Component
public class QualityDraftProviderComponent implements DraftProvider<DataQualityModel> {
    /**
     * This draft provider id.
     */
    private static final String ID = "quality-model";
    /**
     * This draft provider description.
     */
    private static final String DESCRIPTION = "app.dq.draft.model.provider.description";
    /**
     * The TS.
     */
    @Autowired
    private TextService textService;
    /**
     * Constructor.
     */
    @Autowired
    public QualityDraftProviderComponent(DraftService draftService) {
        super();
        draftService.register(this);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return textService.getText(DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getPipelineId(DraftOperation operation) {

        switch (operation) {
        case GET_DATA:
            return QualityDraftGetStartExecutor.SEGMENT_ID;
        case PUBLISH_DATA:
            return QualityDraftPublishStartExecutor.SEGMENT_ID;
        case UPSERT_DATA:
            return QualityDraftUpsertStartExecutor.SEGMENT_ID;
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DataQualityModel fromBytes(byte[] data) {
        return DataQualitySerializer.modelFromCompressedXml(data);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] toBytes(DataQualityModel data) {
        return DataQualitySerializer.modelToCompressedXml(data);
    }
}
