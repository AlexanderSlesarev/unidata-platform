/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.core.exception;

import org.unidata.mdm.system.exception.ExceptionId;

/**
 * @author Alexander Malyshev
 */
public final class DataQualityExceptionIds {
    private DataQualityExceptionIds() {
    }

    public static final ExceptionId EX_DQ_UPSERT_EXECUTION_EXEC = new ExceptionId("EX_DQ_UPSERT_EXECUTION_ERRORS", "app.dq.upsert.execution.errors");

    public static final ExceptionId EX_DQ_CLEANSE_FUNCTION_EXEC = new ExceptionId("EX_DQ_CLEANSE_FUNCTION_EXEC", "app.dq.cleanse.function.execution");

    public static final ExceptionId EX_DQ_CLEANSE_FUNCTION_NOT_FOUND_DQS = new ExceptionId("EX_DQ_CLEANSE_FUNCTION_NOT_FOUND_DQS", "app.dq.cleanse.function.not.found");

    public static final ExceptionId EX_DQ_EXECUTION_CONTEXT_MODE_NOT_SUPPORTED = new ExceptionId("EX_DQ_EXECUTION_CONTEXT_MODE_NOT_SUPPORTED", "app.dq.execution.context.mode.not.supported");

    public static final ExceptionId EX_META_CONSISTENCY_CHECK_RULE_INVALID_INPUT = new ExceptionId("EX_META_CONSISTENCY_CHECK_RULE_INVALID_INPUT", "app.meta.consistency.check.rule.invalid.input");

    public static final ExceptionId EX_META_LINK_CHECK_RULE_INVALID_INPUT = new ExceptionId("EX_META_LINK_CHECK_RULE_INVALID_INPUT", "app.meta.link.check.rule.invalid.input");

    public static final ExceptionId EX_DQ_CLEANSE_FUNCTION_NOT_FOUND = new ExceptionId("EX_DQ_CLEANSE_FUNCTION_NOT_FOUND", "app.dq.cleanse.function.not.found");

    public static final ExceptionId EX_DQ_CLEANSE_FUNCTION_EXCEPTION_CAUGHT = new ExceptionId("EX_DQ_CLEANSE_FUNCTION_EXCEPTION_CAUGHT", "app.dq.cleanse.function.exception.caught");

    public static final ExceptionId EX_SYSTEM_JAXB_CONTEXT_INIT_FAILURE = new ExceptionId("EX_SYSTEM_JAXB_CONTEXT_INIT_FAILURE", "app.data.jaxbContextInitFailure");

    public static final ExceptionId EX_META_CANNOT_MARSHAL_CLEANSE_FUNCTION_GROUP = new ExceptionId("EX_META_CANNOT_MARSHAL_CLEANSE_FUNCTION_GROUP", "app.meta.cannotMarshallCleanseFunctionGroup");

    public static final ExceptionId EX_META_CANNOT_UNMARSHAL_CLEANSE_FUNCTION = new ExceptionId("EX_META_CANNOT_UNMARSHAL_CLEANSE_FUNCTION", "app.meta.cannotUnmarshallCleanseFunction");

    public static final ExceptionId EX_META_CANNOT_UNMARSHAL_COMPOSITE_CLEANSE_FUNCTION = new ExceptionId("EX_META_CANNOT_UNMARSHAL_COMPOSITE_CLEANSE_FUNCTION", "app.meta.cannotUnmarshallCompositeCleanseFunction");

    public static final ExceptionId EX_META_CANNOT_UNMARSHAL_CLEANSE_FUNCTION_GROUP = new ExceptionId("EX_META_CANNOT_UNMARSHAL_CLEANSE_FUNCTION_GROUP", "app.meta.cannotUnmarshallCleanseFunctionGroup");

    public static final ExceptionId EX_META_CANNOT_MARSHAL_RULE =
            new ExceptionId("EX_META_CANNOT_MARSHAL_RULE", "app.meta.cannotUnmarshallRule");

    public static final ExceptionId EX_META_CANNOT_UNMARSHAL_RULE = new ExceptionId("EX_META_CANNOT_UNMARSHAL_RULE", "app.meta.unmarshal.rule");

    public static final ExceptionId EX_UPATH_INVALID_INPUT_ENTITY_OR_PATH_BLANK = new ExceptionId("EX_UPATH_INVALID_INPUT_ENTITY_OR_PATH_BLANK", "app.upath.invalid.input.entity.or.path.blank");

    public static final ExceptionId EX_UPATH_ENTITY_NOT_FOUND_BY_NAME = new ExceptionId("EX_UPATH_ENTITY_NOT_FOUND_BY_NAME", "app.upath.invalid.state.entity.not.found.by.name");

    public static final ExceptionId EX_UPATH_INVALID_INPUT_SPLIT_TO_ZERO_ELEMENTS = new ExceptionId("EX_UPATH_INVALID_INPUT_SPLIT_TO_ZERO_ELEMENTS", "app.upath.invalid.input.split.to.zero.elements");

    public static final ExceptionId EX_UPATH_NOT_A_COMPLEX_ATTRIBUTE_FOR_INTERMEDIATE_PATH_ELEMENT = new ExceptionId("EX_UPATH_NOT_A_COMPLEX_ATTRIBUTE_FOR_INTERMEDIATE_PATH_ELEMENT", "app.upath.invalid.state.not.complex.for.intermediate");

    public static final ExceptionId EX_UPATH_INVALID_SET_NOT_A_COMPLEX_FOR_INTERMEDIATE = new ExceptionId("EX_UPATH_INVALID_SET_NOT_A_COMPLEX_FOR_INTERMEDIATE", "app.upath.invalid.set.not.complex.for.intermediate");

    public static final ExceptionId EX_UPATH_INVALID_SET_WRONG_END_ELEMENT = new ExceptionId("EX_UPATH_INVALID_SET_WRONG_END_ELEMENT", "app.upath.invalid.set.wrong.end.element");

    public static final ExceptionId EX_UPATH_INVALID_SET_WRONG_TARGET_ATTRIBUTE_TYPE = new ExceptionId("EX_UPATH_INVALID_SET_WRONG_TARGET_ATTRIBUTE_TYPE", "app.upath.invalid.set.wrong.target.attribute.type");

    public static final ExceptionId EX_UPATH_INVALID_SET_WRONG_ATTRIBUTE_NAME = new ExceptionId("EX_UPATH_INVALID_SET_WRONG_ATTRIBUTE_NAME", "app.upath.invalid.set.wrong.attribute.name");

    public static final ExceptionId EX_UPATH_INVALID_ROOT_EXPRESSION = new ExceptionId("EX_UPATH_INVALID_ROOT_EXPRESSION", "app.upath.invalid.root.expression");

    public static final ExceptionId EX_UPATH_INVALID_SUBSCRIPT_EXPRESSION = new ExceptionId("EX_UPATH_INVALID_SUBSCRIPT_EXPRESSION", "app.upath.invalid.subscript.expression");

    public static final ExceptionId EX_UPATH_INVALID_FILTERING_EXPRESSION_ATTRIBUTE_TYPE = new ExceptionId("EX_UPATH_INVALID_FILTERING_EXPRESSION_ATTRIBUTE_TYPE", "app.upath.invalid.filtering.expression.attribute.type");

    public static final ExceptionId EX_UPATH_INVALID_FILTERING_EXPRESSION_DATE_FORMAT = new ExceptionId("EX_UPATH_INVALID_FILTERING_EXPRESSION_DATE_FORMAT", "app.upath.invalid.filtering.expression.date.format");

    public static final ExceptionId EX_UPATH_INVALID_FILTERING_EXPRESSION_TIME_FORMAT = new ExceptionId("EX_UPATH_INVALID_FILTERING_EXPRESSION_TIME_FORMAT", "app.upath.invalid.filtering.expression.time.format");

    public static final ExceptionId EX_UPATH_INVALID_FILTERING_EXPRESSION_TIMESTAMP_FORMAT = new ExceptionId("EX_UPATH_INVALID_FILTERING_EXPRESSION_TIMESTAMP_FORMAT", "app.upath.invalid.filtering.expression.timestamp.format");

    public static final ExceptionId EX_UPATH_INVALID_FILTERING_EXPRESSION_NUMBER_FORMAT = new ExceptionId("EX_UPATH_INVALID_FILTERING_EXPRESSION_NUMBER_FORMAT", "app.upath.invalid.filtering.expression.number.format");

    public static final ExceptionId EX_UPATH_INVALID_FILTERING_EXPRESSION_STRING_FORMAT = new ExceptionId("EX_UPATH_INVALID_FILTERING_EXPRESSION_STRING_FORMAT", "app.upath.invalid.filtering.expression.string.format");

    public static final ExceptionId EX_UPATH_INVALID_FILTERING_EXPRESSION_COMPLEX_ATTRIBUTE = new ExceptionId("EX_UPATH_INVALID_FILTERING_EXPRESSION_COMPLEX_ATTRIBUTE", "app.upath.invalid.filtering.expression.complex.attribute");

    public static final ExceptionId EX_UPATH_INVALID_INPUT_ATTRIBUTE_NOT_FOUND_BY_PATH = new ExceptionId("EX_UPATH_INVALID_INPUT_ATTRIBUTE_NOT_FOUND_BY_PATH", "app.upath.invalid.input.attribute.not.found.by.path");

    public static final ExceptionId EX_SYSTEM_CLEANSE_INIT_OUTPUT = new ExceptionId("EX_SYSTEM_CLEANSE_INIT_OUTPUT", "app.cleanse.unableToInitOutputPorts");

    public static final ExceptionId EX_SYSTEM_CLEANSE_INIT_INPUT = new ExceptionId("EX_SYSTEM_CLEANSE_INIT_INPUT", "app.cleanse.unableToInitInputPorts");

    public static final ExceptionId EX_DQ_CLEANSE_FUNCTION_REQUIRED_VALUE_MISSING = new ExceptionId("EX_DQ_CLEANSE_FUNCTION_REQUIRED_VALUE_MISSING", "app.dq.cleanse.function.required.value.missing");

    public static final ExceptionId EX_DQ_STORAGE_INSTALL_FAILED = new ExceptionId("EX_DQ_STORAGE_INSTALL_FAILED", "app.dq.storage.install.failed");
    public static final ExceptionId EX_DQ_INSTALL_RESOURCE_READ_FAILED = new ExceptionId("EX_DQ_INSTALL_RESOURCE_READ_FAILED", "app.dq.install.resource.read.failed");

    public static final ExceptionId EX_DQ_STORAGE_UNINSTALL_FAILED = new ExceptionId("EX_DQ_STORAGE_UNINSTALL_FAILED", "app.dq.storage.uninstall.failed");
    public static final ExceptionId EX_DQ_UNINSTALL_RESOURCE_READ_FAILED = new ExceptionId("EX_DQ_UNINSTALL_RESOURCE_READ_FAILED", "app.dq.uninstall.resource.read.failed");

    public static final ExceptionId EX_SYSTEM_CLEANSE_DELETE_FAILED = new ExceptionId("EX_SYSTEM_CLEANSE_DELETE_FAILED", "app.cleanse.deleteFailed");
    public static final ExceptionId EX_DATA_CANNOT_PARSE_LOCAL_DATE = new ExceptionId("EX_DATA_CANNOT_PARSE_LOCAL_DATE", "app.data.cannotParseLocalDate");
    public static final ExceptionId EX_DATA_CANNOT_PARSE_LOCAL_TIME = new ExceptionId("EX_DATA_CANNOT_PARSE_LOCAL_TIME", "app.data.cannotParseLocalTime");
    public static final ExceptionId EX_DATA_CANNOT_PARSE_LOCAL_DATE_TIME = new ExceptionId("EX_DATA_CANNOT_PARSE_LOCAL_DATE_TIME", "app.data.cannotParseLocalDateTime");
    public static final ExceptionId EX_DATA_CANNOT_PARSE_DATE = new ExceptionId("EX_DATA_CANNOT_PARSE_DATE", "app.data.cannotParseDate");
    public static final ExceptionId EX_META_INSERT_FAILED = new ExceptionId("EX_META_INSERT_FAILED", "app.meta.metadataInsertFailed");
    public static final ExceptionId EX_META_UPDATE_FAILED = new ExceptionId("EX_META_UPDATE_FAILED", "app.meta.metadataUpdateFailed");
    public static final ExceptionId EX_SYSTEM_JAXB_TYPE_FACTORY_INIT_FAILURE = new ExceptionId("EX_SYSTEM_JAXB_TYPE_FACTORY_INIT_FAILURE", "app.data.jaxbTypeFactoryInitFailure");

    public static final ExceptionId EX_DQ_ELEMENT_IS_NOT_JAVA
        = new ExceptionId("EX_DQ_ELEMENT_IS_NOT_JAVA", "app.dq.element.is.not.java");

    public static final ExceptionId EX_DQ_LIBRARY_NOT_FOUND
        = new ExceptionId("EX_DQ_LIBRARY_NOT_FOUND", "app.dq.library.not.found");

    public static final ExceptionId EX_DQ_LIBRARY_CANNOT_BE_CREATED
        = new ExceptionId("EX_DQ_LIBRARY_CANNOT_BE_CREATED", "app.dq.library.cannot.be.created");

    public static final ExceptionId EX_DQ_JAVA_FUNCTION_CANNOT_BE_CREATED
        = new ExceptionId("EX_DQ_JAVA_FUNCTION_CANNOT_BE_CREATED", "app.dq.java.function.cannot.be.created");

    /**
     * Cannot compress data.
     */
    public static final ExceptionId EX_DQ_CANNOT_COMPRESS_DATA =
            new ExceptionId("EX_DQ_CANNOT_COMPRESS_DATA", "app.dq.cannot.compress.data");
    /**
     * Cannot uncompress data.
     */
    public static final ExceptionId EX_DQ_CANNOT_UNCOMPRESS_DATA =
            new ExceptionId("EX_DQ_CANNOT_UNCOMPRESS_DATA", "app.dq.cannot.uncompress.data");

    public static final ExceptionId EX_DQ_UPSERT_DATA_MODEL_CONSISTENCY =
            new ExceptionId("EX_DQ_UPSERT_DATA_MODEL_CONSISTENCY", "app.dq.upsert.data.model.consistency");

    public static final ExceptionId EX_DQ_UPSERT_DATA_MODEL_VALIDATION =
            new ExceptionId("EX_DQ_UPSERT_DATA_MODEL_VALIDATION", "app.dq.upsert.data.model.validation");

    public static final ExceptionId EX_DQ_UPSERT_DATA_MODEL_REVISION_EXISTS =
            new ExceptionId("EX_DQ_UPSERT_DATA_MODEL_REVISION_EXISTS", "app.dq.upsert.data.model.revision.exists");

    public static final ExceptionId EX_DQ_CANNOT_INSTALL_SYSTEM_CLEANSE_FUNCTIONS =
            new ExceptionId("EX_DQ_CANNOT_INSTALL_SYSTEM_CLEANSE_FUNCTIONS", "app.dq.can.not.install.system.cleanse.functions");

    public static final ExceptionId EX_DQ_SYSTEM_CLEANSE_FUNCTION_NOT_ORDERED =
            new ExceptionId("EX_DQ_SYSTEM_CLEANSE_FUNCTION_NOT_ORDERED", "app.dq.system.cleanse.function.not.ordered");

    public static final ExceptionId EX_DQ_PUBLISH_QUALITY_MODEL_VALIDATION =
            new ExceptionId("EX_DQ_PUBLISH_QUALITY_MODEL_VALIDATION", "app.dq.publish.quality.model.validation");
}
