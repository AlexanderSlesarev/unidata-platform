package org.unidata.mdm.dq.core.type.model.source.constant;

import java.time.LocalTime;

import org.unidata.mdm.dq.core.type.constant.SingleValueConstantType;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Mikhail Mikhailov on Jan 22, 2021
 */
public class TimeSingleValueConstant extends AbstractSingleValueConstant<TimeSingleValueConstant, LocalTime> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 7412998120372053220L;
    /**
     * Constructor.
     */
    public TimeSingleValueConstant() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @JsonProperty("type")
    @Override
    public SingleValueConstantType getType() {
        return SingleValueConstantType.TIME;
    }
}
