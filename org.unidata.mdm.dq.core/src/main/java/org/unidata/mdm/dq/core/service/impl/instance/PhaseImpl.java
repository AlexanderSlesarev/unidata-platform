/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.service.impl.instance;

import org.unidata.mdm.core.type.model.instance.AbstractNamedDisplayableCustomPropertiesImpl;
import org.unidata.mdm.dq.core.type.model.instance.PhaseElement;
import org.unidata.mdm.dq.core.type.model.source.PhaseSource;

/**
 * @author Mikhail Mikhailov on Nov 9, 2021
 * Phase tag implementation.
 */
public class PhaseImpl extends AbstractNamedDisplayableCustomPropertiesImpl implements PhaseElement {
    /**
     * The source.
     */
    private final PhaseSource source;
    /**
     * Constructor.
     * @param source the source
     */
    PhaseImpl(PhaseSource source) {
        super(source.getName(), source.getDisplayName(), source.getDescription(), source.getCustomProperties());
        this.source = source;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return getName();
    }
    /**
     * Gets the source for internal MM use.
     * @return source
     */
    public PhaseSource getSource() {
        return source;
    }
}
