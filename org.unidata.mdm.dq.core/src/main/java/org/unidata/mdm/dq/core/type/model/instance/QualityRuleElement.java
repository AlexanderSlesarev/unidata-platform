/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.instance;

import org.unidata.mdm.core.type.model.CreateUpdateElement;
import org.unidata.mdm.core.type.model.CustomPropertiesElement;
import org.unidata.mdm.core.type.model.IdentityElement;
import org.unidata.mdm.core.type.model.NamedDisplayableElement;
import org.unidata.mdm.dq.core.type.rule.QualityRuleRunCondition;

/**
 * @author Mikhail Mikhailov on Feb 17, 2021
 * The DQ rule.
 */
public interface QualityRuleElement extends IdentityElement, NamedDisplayableElement, CustomPropertiesElement, CreateUpdateElement {
    /**
     * Gets configured cleanse function name.
     * @return configured cleanse function name
     */
    CleanseFunctionElement getCleanseFunction();
    /**
     * Gets the run condition.
     * @return the run condition
     */
    QualityRuleRunCondition getRunCondition();
    /**
     * Checks, whether given source system is supported by the rule.
     * @param sourceSystem the source system to check
     * @return true, if source system is supported
     */
    boolean supports(String sourceSystem);
    /**
     * Returns true, if this rule is a validating one.
     * @return true, if this rule is a validating one
     */
    boolean isValidating();
    /**
     * Gets validating view, if this rule has validating capabilities.
     * @return validating view, if this rule has validating capabilities
     */
    ValidatingRuleElement getValidating();
    /**
     * Returns true, if this rule is an enriching one.
     * @return true, if this rule is an enriching one
     */
    boolean isEnriching();
    /**
     * Gets enriching view, if this rule has enrichment capabilities.
     * @return enriching view, if this rule has enrichment capabilities
     */
    EnrichingRuleElement getEnriching();
}
