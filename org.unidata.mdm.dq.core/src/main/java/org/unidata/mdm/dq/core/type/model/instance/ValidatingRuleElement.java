/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.instance;

import org.unidata.mdm.dq.core.type.rule.SeverityIndicator;

/**
 * @author Mikhail Mikhailov on Feb 27, 2021
 * Validating element view.
 */
public interface ValidatingRuleElement {
    /**
     * Gets the raise port name.
     * @return the raise port name
     */
    String getRaisePort();
    /**
     * Returns true, if the element has message port defined.
     * @return true, if the element has message port defined
     */
    boolean hasMessagePort();
    /**
     * Gets the message port name.
     * @return the message port name
     */
    String getMessagePort();
    /**
     * Gets the message text, that will be used, if message port is not defined.
     * @return the message text, that will be used, if message port is not defined
     */
    String getMessageText();
    /**
     * Returns true, if the element has severity port defined.
     * @return true, if the element has severity port defined
     */
    boolean hasSeverityPort();
    /**
     * Gets the severity port name.
     * @return the severity port name
     */
    String getSeverityPort();
    /**
     * Gets severity indicator, that will be used, if severity port is not defined.
     * @return the severity indicator, that will be used, if severity port is not defined
     */
    SeverityIndicator getSeverityIndicator();
    /**
     * Gets severity score, that will be used, if severity indicator is resolved to {@link SeverityIndicator#YELLOW}.
     * @return severity score, that will be used, if severity indicator is resolved to {@link SeverityIndicator#YELLOW}
     */
    int getSeverityScore();
    /**
     * Returns true, if the element has category port defined.
     * @return true, if the element has category port defined
     */
    boolean hasCategoryPort();
    /**
     * Gets the category port name.
     * @return the category port name
     */
    String getCategoryPort();
    /**
     * Gets the category text, that will be used, if category port is not defined.
     * @return the category text, that will be used, if category port is not defined
     */
    String getCategoryText();
}
