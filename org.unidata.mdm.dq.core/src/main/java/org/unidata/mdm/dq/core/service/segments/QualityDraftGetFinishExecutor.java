/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.dq.core.service.segments;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.UPathService;
import org.unidata.mdm.dq.core.module.DataQualityModule;
import org.unidata.mdm.dq.core.service.impl.CleanseFunctionCacheComponent;
import org.unidata.mdm.dq.core.service.impl.DataQualityModelComponent;
import org.unidata.mdm.dq.core.service.impl.instance.DataQualityInstanceImpl;
import org.unidata.mdm.dq.core.type.model.instance.DataQualityInstance;
import org.unidata.mdm.dq.core.type.model.source.DataQualityModel;
import org.unidata.mdm.draft.context.DraftGetContext;
import org.unidata.mdm.draft.dto.DraftGetResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Mikhail Mikhailov on Nov 28, 2019
 */
@Component(QualityDraftGetFinishExecutor.SEGMENT_ID)
public class QualityDraftGetFinishExecutor extends Finish<DraftGetContext, DraftGetResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataQualityModule.MODULE_ID + "[QUALITY_DRAFT_GET_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataQualityModule.MODULE_ID + ".dq.get.finish.description";
    /**
     * CFCC.
     */
    @Autowired
    private CleanseFunctionCacheComponent cleanseFunctionCacheComponent;
    /**
     * UPath service.
     */
    @Autowired
    private UPathService upathService;
    /**
     * The data model component.
     */
    @Autowired
    private DataQualityModelComponent qualityModelComponent;
    /**
     * Constructor.
     */
    public QualityDraftGetFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftGetResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DraftGetResult finish(DraftGetContext ctx) {

        Draft draft = ctx.currentDraft();
        Edition current = ctx.currentEdition();

        DataQualityModel m =  Objects.isNull(current)
                ? new DataQualityModel().withVersion(0)
                : current.getContent();

        DataQualityInstance i = new DataQualityInstanceImpl(m, cleanseFunctionCacheComponent, upathService);

        DraftGetResult result = new DraftGetResult();
        result.setDraft(draft);
        result.setPayload(qualityModelComponent.disassemble(i, ctx.getPayload()));

        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return DraftGetContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
