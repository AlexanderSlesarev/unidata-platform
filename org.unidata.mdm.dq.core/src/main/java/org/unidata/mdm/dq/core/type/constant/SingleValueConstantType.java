package org.unidata.mdm.dq.core.type.constant;

import org.unidata.mdm.core.type.data.SimpleAttribute.SimpleDataType;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortValueType;

/**
 * Type of the value, supported by DQ constants.
 */
public enum SingleValueConstantType {
    /**
     * LD.
     */
    DATE("Date"),
    /**
     * LT.
     */
    TIME("Time"),
    /**
     * LDT.
     */
    TIMESTAMP("Timestamp"),
    /**
     * String.
     */
    STRING("String"),
    /**
     * Integer.
     */
    INTEGER("Integer"),
    /**
     * Double
     */
    NUMBER("Number"),
    /**
     * Boolean
     */
    BOOLEAN("Boolean");

    private final String value;

    SingleValueConstantType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public CleanseFunctionPortValueType toFunctionPortValueType() {

        switch (this) {
        case BOOLEAN:
            return CleanseFunctionPortValueType.BOOLEAN;
        case DATE:
            return CleanseFunctionPortValueType.DATE;
        case INTEGER:
            return CleanseFunctionPortValueType.INTEGER;
        case NUMBER:
            return CleanseFunctionPortValueType.NUMBER;
        case STRING:
            return CleanseFunctionPortValueType.STRING;
        case TIME:
            return CleanseFunctionPortValueType.TIME;
        case TIMESTAMP:
            return CleanseFunctionPortValueType.TIMESTAMP;
        default:
            return null;
        }
    }

    public static SingleValueConstantType fromValue(String v) {
        for (SingleValueConstantType c: SingleValueConstantType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }

        throw new IllegalArgumentException(v);
    }

    public static SingleValueConstantType fromDataType(SimpleDataType type) {

        switch (type) {
        case BOOLEAN:
            return SingleValueConstantType.BOOLEAN;
        case DICTIONARY:
        case ENUM:
        case LINK:
        case STRING:
            return SingleValueConstantType.STRING;
        case INTEGER:
            return SingleValueConstantType.INTEGER;
        case MEASURED:
        case NUMBER:
            return SingleValueConstantType.NUMBER;
        case DATE:
            return SingleValueConstantType.DATE;
        case TIME:
            return SingleValueConstantType.TIME;
        case TIMESTAMP:
            return SingleValueConstantType.TIMESTAMP;
        default:
            break;
        }

        return null;
    }
}
