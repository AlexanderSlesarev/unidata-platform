/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.constant;

import org.unidata.mdm.core.type.data.ArrayAttribute.ArrayDataType;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortValueType;

/**
 * Type of the value, supported by DQ constants.
 */
public enum ArrayValueConstantType {
    /**
     * LD.
     */
    DATE("Date"),
    /**
     * LT.
     */
    TIME("Time"),
    /**
     * LDT.
     */
    TIMESTAMP("Timestamp"),
    /**
     * String.
     */
    STRING("String"),
    /**
     * Integer.
     */
    INTEGER("Integer"),
    /**
     * Double
     */
    NUMBER("Number");

    private final String value;

    ArrayValueConstantType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public CleanseFunctionPortValueType toFunctionPortValueType() {

        switch (this) {
        case DATE:
            return CleanseFunctionPortValueType.DATE;
        case INTEGER:
            return CleanseFunctionPortValueType.INTEGER;
        case NUMBER:
            return CleanseFunctionPortValueType.NUMBER;
        case STRING:
            return CleanseFunctionPortValueType.STRING;
        case TIME:
            return CleanseFunctionPortValueType.TIME;
        case TIMESTAMP:
            return CleanseFunctionPortValueType.TIMESTAMP;
        default:
            return null;
        }
    }

    public static ArrayValueConstantType fromValue(String v) {
        for (ArrayValueConstantType c: ArrayValueConstantType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }

        throw new IllegalArgumentException(v);
    }

    public static ArrayValueConstantType fromDataType(ArrayDataType type) {
        switch (type) {
        case DATE:
            return ArrayValueConstantType.DATE;
        case DICTIONARY:
        case STRING:
            return ArrayValueConstantType.STRING;
        case INTEGER:
            return ArrayValueConstantType.INTEGER;
        case NUMBER:
            return ArrayValueConstantType.NUMBER;
        case TIME:
            return ArrayValueConstantType.TIME;
        case TIMESTAMP:
            return ArrayValueConstantType.TIMESTAMP;
        default:
            break;
        }

        return null;
    }
}
