/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.dto;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.dq.core.type.model.source.rule.MappingSetSource;

/**
 * @author Mikhail Mikhailov on Feb 5, 2021
 * Get rule sets result.
 */
public class GetSetsModelResult {
    /**
     * Functions, if asked as standalone.
     */
    private List<MappingSetSource> sets;
    /**
     * Constructor.
     */
    public GetSetsModelResult(List<MappingSetSource> functions) {
        super();
        this.sets = functions;
    }
    /**
     * @return the functions
     */
    public List<MappingSetSource> getSets() {
        return Objects.isNull(sets) ? Collections.emptyList() : sets;
    }
    /**
     * @param sets the functions to set
     */
    public void setSets(List<MappingSetSource> sets) {
        this.sets = sets;
    }
}
