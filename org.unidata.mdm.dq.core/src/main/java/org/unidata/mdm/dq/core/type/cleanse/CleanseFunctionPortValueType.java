/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.core.type.cleanse;

import java.util.Objects;

/**
 * @author Mikhail Mikhailov
 * Types, which are supported by a port.
 */
public enum CleanseFunctionPortValueType {
    /**
     * The string type.
     */
    STRING,
    /**
     * Dictionary type (attribute-local enumeration type).
     */
    DICTIONARY,
    /**
     * The integer type (long 8 bytes).
     */
    INTEGER,
    /**
     * The floating point type (double 8 bytes).
     */
    NUMBER,
    /**
     * The boolean type.
     */
    BOOLEAN,
    /**
     * Binary large object.
     */
    BLOB,
    /**
     * Character large object.
     */
    CLOB,
    /**
     * The date type.
     */
    DATE,
    /**
     * The time type.
     */
    TIME,
    /**
     * The timestamp type.
     */
    TIMESTAMP,
    /**
     * Link to a global enum value.
     */
    ENUM,
    /**
     * Special href template, processed by get post-processor, type.
     */
    LINK,
    /**
     * Special type of number.
     */
    MEASURED,
    /**
     * Any type.
     */
    ANY;

    public static boolean isSimpleValueType(CleanseFunctionPortValueType v) {
        // All types except ANY are currently used in SA.
        return Objects.nonNull(v) && v != ANY;
    }

    public static boolean isCodeValueType(CleanseFunctionPortValueType v) {
        // Check for specific CODE types.
        return Objects.nonNull(v) && (v == STRING || v == INTEGER);
    }

    public static boolean isArrayValueType(CleanseFunctionPortValueType v) {

        if (Objects.isNull(v)) {
            return false;
        }

        // Check for specific ARRAY types.
        switch (v) {
        case STRING:
        case DICTIONARY:
        case INTEGER:
        case NUMBER:
        case DATE:
        case TIME:
        case TIMESTAMP:
            return true;
        default:
            return false;
        }
    }
}
