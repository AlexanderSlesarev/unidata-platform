/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.exception;

import java.util.Collection;
import java.util.Collections;

import org.unidata.mdm.dq.core.type.io.DataQualityError;
import org.unidata.mdm.system.exception.DomainId;
import org.unidata.mdm.system.exception.ExceptionId;
import org.unidata.mdm.system.exception.PlatformRuntimeException;

/**
 * @author Alexey Tsarapkin
 */
public class DataQualityExecutionException extends PlatformRuntimeException {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 7356247101456509352L;
    /**
     * This exception domain.
     */
    private static final DomainId DATA_QUALITY_EXECUTION_EXCEPTION = () -> "DATA_QUALITY_EXECUTION_EXCEPTION";

    private final Collection<DataQualityError> errors;

    public DataQualityExecutionException(String message, ExceptionId id, Collection<DataQualityError> errors, Object... args) {
        super(message, id, args);
        this.errors = errors == null ? Collections.emptyList() : errors;
    }

    @Override
    public DomainId getDomain() {
        return DATA_QUALITY_EXECUTION_EXCEPTION;
    }

    public Collection<DataQualityError> getErrors() {
        return errors;
    }
}
