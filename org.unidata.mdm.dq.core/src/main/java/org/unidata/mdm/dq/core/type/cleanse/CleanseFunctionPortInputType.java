/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.cleanse;

/**
 * @author Mikhail Mikhailov on Jan 20, 2021
 * Input type -- a type of attribute, that this port accepts.
 */
public enum CleanseFunctionPortInputType {
    /**
     * Simple attributes are accepted.
     */
    SIMPLE,
    /**
     * Array attributes are accepted.
     */
    ARRAY,
    /**
     * Code attributes are accepted.
     */
    CODE,
    /**
     * Complex attributes are accepted.
     */
    COMPLEX,
    /**
     * Filtered record (ar the whole root record) are accepted.
     */
    RECORD,
    /**
     * Any kind of input is accepted (no additional checks performed).
     */
    ANY
}
