/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.serialization.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov on Jun 9, 2021
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataQualityErrorJS {
    /**
     * DQE field: functionName.
     */
    private String functionName;
    /**
     * DQE field: message.
     */
    private String message;
    /**
     * DQE field: severity.
     */
    private String severity;
    /**
     * DQE field: score.
     */
    private String score;
    /**
     * DQE field: category.
     */
    private String category;
    /**
     * Constructor.
     */
    public DataQualityErrorJS() {
        super();
    }
    /**
     * @return the functionName
     */
    public String getFunctionName() {
        return functionName;
    }
    /**
     * @param functionName the functionName to set
     */
    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }
    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    /**
     * @return the severity
     */
    public String getSeverity() {
        return severity;
    }
    /**
     * @param severity the severity to set
     */
    public void setSeverity(String severity) {
        this.severity = severity;
    }
    /**
     * @return the score
     */
    public String getScore() {
        return score;
    }
    /**
     * @param score the score to set
     */
    public void setScore(String score) {
        this.score = score;
    }
    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }
    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }
}
