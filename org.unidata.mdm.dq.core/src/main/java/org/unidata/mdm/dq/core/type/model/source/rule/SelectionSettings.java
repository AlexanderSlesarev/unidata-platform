/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.rule;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Apply to source systems
 */
public class SelectionSettings implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 6429241572789259793L;

    @JacksonXmlElementWrapper(useWrapping = true, localName = "sourceSystems")
    @JacksonXmlProperty(localName = "sourceSystem")
    private HashSet<String> sourceSystems;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean all;

    /**
     * Gets source systems.
     */
    public Set<String> getSourceSystems() {
        if (sourceSystems == null) {
            sourceSystems = new HashSet<>();
        }
        return this.sourceSystems;
    }

    /**
     * Gets the value of the all property.
     *
     */
    public boolean isAll() {
        return all;
    }

    /**
     * Sets the value of the all property.
     *
     */
    public void setAll(boolean value) {
        this.all = value;
    }

    public SelectionSettings withSourceSystems(String... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withSourceSystems(Arrays.asList(values));
        }
        return this;
    }

    public SelectionSettings withSourceSystems(Collection<String> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getSourceSystems().addAll(values);
        }
        return this;
    }

    public SelectionSettings withAll(boolean value) {
        setAll(value);
        return this;
    }

}
