/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.serialization.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov on Jun 9, 2021
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RuleResultJS {
    /**
     * Set ID.
     */
    private String setName;
    /**
     * Rule ID.
     */
    private String ruleName;
    /**
     * Executions.
     */
    private List<RuleExecutionResultJS> executions;
    /**
     * Constructor.
     */
    public RuleResultJS() {
        super();
    }
    /**
     * @return the setName
     */
    public String getSetName() {
        return setName;
    }
    /**
     * @param setName the setName to set
     */
    public void setSetName(String setName) {
        this.setName = setName;
    }
    /**
     * @return the ruleName
     */
    public String getRuleName() {
        return ruleName;
    }
    /**
     * @param ruleName the ruleName to set
     */
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
    /**
     * @return the executions
     */
    public List<RuleExecutionResultJS> getExecutions() {
        return executions;
    }
    /**
     * @param executions the executions to set
     */
    public void setExecutions(List<RuleExecutionResultJS> executions) {
        this.executions = executions;
    }
}
