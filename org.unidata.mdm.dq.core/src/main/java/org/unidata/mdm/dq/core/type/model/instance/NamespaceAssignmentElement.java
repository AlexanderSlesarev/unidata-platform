/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.instance;

import java.util.Collection;
import javax.annotation.Nonnull;

import org.unidata.mdm.core.type.model.IdentityElement;
import org.unidata.mdm.system.type.namespace.NameSpace;

/**
 * @author Mikhail Mikhailov on Mar 26, 2021
 * Assignments of rule mapping sets to entities for a namespace.
 */
public interface NamespaceAssignmentElement extends IdentityElement {
    /**
     * Gets the namespace object it is assigned to.
     * @return namespace object it is assigned to
     */
    NameSpace getNameSpace();
    /**
     * Gets entity names, mapped by this assignment.
     * @return entity names, mapped by this assignment
     */
    Collection<String> getEntityNames();
    /**
     * Gets a collection of ALL rule mapping sets, associated with the given name.
     * @param name the entity name
     * @return collection of rule mapping sets
     */
    @Nonnull
    Collection<MappingSetElement> getAssigned(String name);
    /**
     * Gets a collection of rule mapping sets, associated with the given name AND marked by the given phase.
     * @param name the entity name
     * @param phase the name of the phase
     * @return collection of rule mapping sets
     */
    @Nonnull
    Collection<MappingSetElement> getAssigned(String name, String phase);
    /**
     * Returns true, if the entity name is linked to a collection of rule mapping sets.
     * @param name the entity name
     * @return true if the entity name is linked to a collection of rule mapping sets
     */
    boolean isAssigned(String name);
    /**
     * Returns true, if the entity name is linked to a collection of rule mapping sets and one of the sets is mapped to the given phase.
     * @param name the name of the entity
     * @param phase the name of the phase
     * @return true, if the entity name is linked to a collection of rule mapping sets and one of the sets is mapped to the given phase
     */
    boolean isAssigned(String name, String phase);
}
