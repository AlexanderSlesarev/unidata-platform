/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.core.type.cleanse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.unidata.mdm.core.type.data.ArrayAttribute.ArrayDataType;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.BinaryLargeValue;
import org.unidata.mdm.core.type.data.CharacterLargeValue;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.SimpleAttribute.SimpleDataType;
import org.unidata.mdm.core.type.data.impl.AbstractArrayAttribute;
import org.unidata.mdm.core.type.data.impl.AbstractSimpleAttribute;
import org.unidata.mdm.core.type.data.impl.ComplexAttributeImpl;

/**
 * @author Mikhail Mikhailov
 * Output param type.
 */
public class CleanseFunctionOutputParam extends CleanseFunctionParam {
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, Collection<DataRecord> value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(ComplexAttributeImpl.ofUnattended(portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, Attribute value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(value));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam ofIntegers(String portName, List<Long> value) {
        return new CleanseFunctionOutputParam(portName,
                Collections.singletonList(AbstractArrayAttribute.of(ArrayDataType.INTEGER, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam ofNumbers(String portName, List<Double> value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractArrayAttribute.of(ArrayDataType.NUMBER, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam ofStrings(String portName, List<String> value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractArrayAttribute.of(ArrayDataType.STRING, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam ofTimestamps(String portName, List<LocalDateTime> value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractArrayAttribute.of(ArrayDataType.TIMESTAMP, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam ofTimes(String portName, List<LocalTime> value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractArrayAttribute.of(ArrayDataType.TIME, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam ofDates(String portName, List<LocalDate> value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractArrayAttribute.of(ArrayDataType.DATE, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, Boolean value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractSimpleAttribute.of(SimpleDataType.BOOLEAN, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, Long value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractSimpleAttribute.of(SimpleDataType.INTEGER, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, Double value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractSimpleAttribute.of(SimpleDataType.NUMBER, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, String value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractSimpleAttribute.of(SimpleDataType.STRING, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, LocalDateTime value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractSimpleAttribute.of(SimpleDataType.TIMESTAMP, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, LocalTime value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractSimpleAttribute.of(SimpleDataType.TIME, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, LocalDate value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractSimpleAttribute.of(SimpleDataType.DATE, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, BinaryLargeValue value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractSimpleAttribute.of(SimpleDataType.BLOB, portName, value)));
    }
    /**
     * Creates output value param.
     * @param portName
     * @param value
     * @return param
     */
    public static CleanseFunctionOutputParam of(String portName, CharacterLargeValue value) {
        return new CleanseFunctionOutputParam(portName, Collections.singletonList(AbstractSimpleAttribute.of(SimpleDataType.CLOB, portName, value)));
    }
    /**
     * Constructor.
     */
    private CleanseFunctionOutputParam(String portName, List<Attribute> values) {
        super(ParamType.OUTPUT, portName, values);
    }

}
