/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.core.type.cleanse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.core.type.data.ArrayAttribute;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.Attribute.AttributeType;
import org.unidata.mdm.core.type.data.CodeAttribute;
import org.unidata.mdm.core.type.data.SimpleAttribute;

/**
 * @author Mikhail Mikhailov
 * Cleanse function param base type.
 */
public abstract class CleanseFunctionParam {
    /**
     * The values.
     */
    protected final List<Attribute> values;
    /**
     * Name of the port.
     */
    private final String portName;
    /**
     * Type of the param.
     */
    private final ParamType paramType;
    /**
     * Type of the param
     * @author Mikhail Mikhailov
     */
    public enum ParamType {
        /**
         * Input param type.
         */
        INPUT,
        /**
         * Output param type.
         */
        OUTPUT
    }
    /**
     * Constructor.
     * @param paramType type of the param
     * @param portName name ofthe port
     * @param values the values to hold
     */
    protected CleanseFunctionParam(ParamType paramType, String portName, List<Attribute> values) {
        super();
        this.paramType = paramType;
        this.portName = portName;
        this.values = values;
    }
    /**
     * @return the portName
     */
    public String getPortName() {
        return portName;
    }
    /**
     * @return the paramType
     */
    public ParamType getParamType() {
        return paramType;
    }
    /**
     * Tells whether this param holds no value.
     * @return true if so, false otherwise
     */
    public boolean isEmpty() {

        if (CollectionUtils.isEmpty(values)) {
            return true;
        }

        Iterator<Attribute> i = values.iterator();
        while (i.hasNext()) {
            Attribute attribute = i.next();
            if (Objects.nonNull(attribute) && !attribute.isEmpty()) {
                return false;
            }
        }

        return true;
    }
    /**
     * Returns true, if the underlaying collection holds only one element.
     * @return true, if holds a single value, false otherwise
     */
    public boolean isSingleton() {
        return CollectionUtils.isNotEmpty(values) && values.size() == 1;
    }
    /**
     * Returns value, if this is the only value, hold by the underlaying collection.
     * Otherwise returns null
     * @return object or null
     */
    @SuppressWarnings("unchecked")
    public<T extends Attribute> T getSingleton() {

        if (isSingleton()) {
            return (T) values.iterator().next();
        }

        return null;
    }
    /**
     * Extracts singleton as typed object.
     * @return object or null
     * @throws ClassCastException
     */
    @SuppressWarnings("unchecked")
    public<T> T toSingletonValue() {
        return (T) toSingletonValueObject();
    }
    /**
     * Extracts singleton as object.
     * @return object or null
     */
    public Object toSingletonValueObject() {

        if (isSingleton()) {

            Attribute attribute = getSingleton();
            if (attribute.getAttributeType() == AttributeType.SIMPLE) {
                return ((SimpleAttribute<?>) attribute).castValue();
            } else if (attribute.getAttributeType() == AttributeType.CODE) {
                return ((CodeAttribute<?>) attribute).castValue();
            } else if (attribute.getAttributeType() == AttributeType.ARRAY) {
                return ((ArrayAttribute<?>) attribute).toArray();
            }
        }

        return null;
    }
    /**
     * Gets values as typed objects array.
     * @return array
     * @throws ClassCastException
     */
    @SuppressWarnings("unchecked")
    public<T> T[] toValues() {
        return (T[]) toValuesObjects();
    }
    /**
     * Gets values as objects array.
     * @return array
     */
    public Object[] toValuesObjects() {

        List<Object> objects = new ArrayList<>(values.size());
        for (int i = 0; i < values.size(); i++) {

            Attribute attribute = values.get(i);
            if (attribute.isEmpty()) {
                continue;
            }

            if (attribute.getAttributeType() == AttributeType.SIMPLE) {
                objects.add(((SimpleAttribute<?>) attribute).getValue());
            } else if (attribute.getAttributeType() == AttributeType.CODE) {
                objects.add(((CodeAttribute<?>) attribute).getValue());
            } else if (attribute.getAttributeType() == AttributeType.ARRAY) {
                objects.addAll(((ArrayAttribute<?>) attribute).getValues());
            }
        }

        return objects.toArray();
    }
}
