/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.constant;

import java.io.Serializable;

import org.unidata.mdm.dq.core.type.constant.SingleValueConstantType;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author Mikhail Mikhailov on Jan 22, 2021
 * Types for CF constants.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
    @Type(value = BooleanSingleValueConstant.class, name = "BOOLEAN"),
    @Type(value = DateSingleValueConstant.class, name = "DATE"),
    @Type(value = IntegerSingleValueConstant.class, name = "INTEGER"),
    @Type(value = NumberSingleValueConstant.class, name = "NUMBER"),
    @Type(value = StringSingleValueConstant.class, name = "STRING"),
    @Type(value = TimeSingleValueConstant.class, name = "TIME"),
    @Type(value = TimestampSingleValueConstant.class, name = "TIMESTAMP")
})
public abstract class AbstractSingleValueConstant<X extends AbstractSingleValueConstant<X, V>, V>
    implements Serializable, SingleValueConstant<V> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 1199956737222699976L;
    /**
     * The value
     */
    protected V value;
    /**
     * Constructor.
     */
    protected AbstractSingleValueConstant() {
        super();
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link SingleValueConstantType }
     *
     */
    @Override
    @JsonProperty("type")
    public abstract SingleValueConstantType getType();

    /**
     * Gets the value.
     * @return the value
     */
    @Override
    public V getValue() {
        return value;
    }

    /**
     * Sets the value.
     * @param value the value to set
     */
    public void setValue(V value) {
        this.value = value;
    }

    public X withValue(V value) {
        setValue(value);
        return self();
    }

    @SuppressWarnings("unchecked")
    protected X self() {
        return (X) this;
    }
}
