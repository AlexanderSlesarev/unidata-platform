/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.rule;

import java.io.Serializable;

import org.unidata.mdm.dq.core.type.rule.SeverityIndicator;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Raise source.
 */
public class ValidationSettings implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -9150024184540907467L;

    @JacksonXmlProperty(isAttribute = true)
    private String raisePort;

    @JacksonXmlProperty(isAttribute = true)
    private String messagePort;

    @JacksonXmlProperty(isAttribute = true)
    private String messageText;

    @JacksonXmlProperty(isAttribute = true)
    private String severityPort;

    @JacksonXmlProperty(isAttribute = true)
    private SeverityIndicator severityIndicator;

    @JacksonXmlProperty(isAttribute = true)
    private int severityScore;

    @JacksonXmlProperty(isAttribute = true)
    private String categoryPort;

    @JacksonXmlProperty(isAttribute = true)
    private String categoryText;

    /**
     * Gets the value of the functionRaiseErrorPort property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRaisePort() {
        return raisePort;
    }

    /**
     * Sets the value of the functionRaiseErrorPort property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRaisePort(String value) {
        this.raisePort = value;
    }

    /**
     * Gets the value of the messagePort property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessagePort() {
        return messagePort;
    }

    /**
     * Sets the value of the messagePort property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessagePort(String value) {
        this.messagePort = value;
    }

    /**
     * Gets the value of the messageText property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * Sets the value of the messageText property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessageText(String value) {
        this.messageText = value;
    }

    /**
     * Gets the value of the severityPort property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSeverityPort() {
        return severityPort;
    }

    /**
     * Sets the value of the severityPort property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSeverityPort(String value) {
        this.severityPort = value;
    }

    /**
     * Gets the value of the severityValue property.
     *
     * @return
     *     possible object is
     *     {@link SeverityIndicator }
     *
     */
    public SeverityIndicator getSeverityIndicator() {
        return severityIndicator;
    }

    /**
     * Sets the value of the severityValue property.
     *
     * @param value
     *     allowed object is
     *     {@link SeverityIndicator }
     *
     */
    public void setSeverityIndicator(SeverityIndicator value) {
        this.severityIndicator = value;
    }

    /**
     * @return the severityScore
     */
    public int getSeverityScore() {
        return severityScore;
    }

    /**
     * @param severityScore the severityScore to set
     */
    public void setSeverityScore(int severityScore) {
        this.severityScore = severityScore;
    }

    /**
     * Gets the value of the categoryPort property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCategoryPort() {
        return categoryPort;
    }

    /**
     * Sets the value of the categoryPort property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCategoryPort(String value) {
        this.categoryPort = value;
    }

    /**
     * Gets the value of the categoryText property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCategoryText() {
        return categoryText;
    }

    /**
     * Sets the value of the categoryText property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCategoryText(String value) {
        this.categoryText = value;
    }

    public ValidationSettings withRaisePort(String value) {
        setRaisePort(value);
        return this;
    }

    public ValidationSettings withMessagePort(String value) {
        setMessagePort(value);
        return this;
    }

    public ValidationSettings withMessageText(String value) {
        setMessageText(value);
        return this;
    }

    public ValidationSettings withSeverityPort(String value) {
        setSeverityPort(value);
        return this;
    }

    public ValidationSettings withSeverityIndicator(SeverityIndicator value) {
        setSeverityIndicator(value);
        return this;
    }

    public ValidationSettings withSeverityScore(int value) {
        setSeverityScore(value);
        return this;
    }

    public ValidationSettings withCategoryPort(String value) {
        setCategoryPort(value);
        return this;
    }

    public ValidationSettings withCategoryText(String value) {
        setCategoryText(value);
        return this;
    }
}
