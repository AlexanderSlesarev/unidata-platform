package org.unidata.mdm.dq.core.type.model.source;

import java.io.Serializable;
import java.math.BigInteger;

import org.unidata.mdm.dq.core.type.composite.CompositeFunctionNodeType;
import org.unidata.mdm.dq.core.type.model.source.constant.CleanseFunctionConstant;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Composite function node (vertex).
 */
public class CompositeCleanseFunctionNode implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -7522149236374635789L;

    @JacksonXmlProperty(localName = "constant")
    protected CleanseFunctionConstant constant;

    @JacksonXmlProperty(isAttribute = true)
    protected Integer nodeId;

    @JacksonXmlProperty(isAttribute = true)
    protected CompositeFunctionNodeType nodeType;

    @JacksonXmlProperty(isAttribute = true)
    protected String functionName;

    /**
     * Gets the value of the constant property.
     *
     * @return
     *     possible object is
     *     {@link CleanseFunctionConstant }
     *
     */
    public CleanseFunctionConstant getConstant() {
        return constant;
    }

    /**
     * Sets the value of the constant property.
     *
     * @param value
     *     allowed object is
     *     {@link CleanseFunctionConstant }
     *
     */
    public void setConstant(CleanseFunctionConstant value) {
        this.constant = value;
    }

    /**
     * Gets the value of the nodeId property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public Integer getNodeId() {
        return nodeId;
    }

    /**
     * Sets the value of the nodeId property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setNodeId(Integer value) {
        this.nodeId = value;
    }

    /**
     * Gets the value of the nodeType property.
     *
     * @return
     *     possible object is
     *     {@link CompositeFunctionNodeType }
     *
     */
    public CompositeFunctionNodeType getNodeType() {
        return nodeType;
    }

    /**
     * Sets the value of the nodeType property.
     *
     * @param value
     *     allowed object is
     *     {@link CompositeFunctionNodeType }
     *
     */
    public void setNodeType(CompositeFunctionNodeType value) {
        this.nodeType = value;
    }

    /**
     * Gets the value of the functionName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * Sets the value of the functionName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFunctionName(String value) {
        this.functionName = value;
    }

    public CompositeCleanseFunctionNode withConstant(CleanseFunctionConstant value) {
        setConstant(value);
        return this;
    }

    public CompositeCleanseFunctionNode withNodeId(Integer value) {
        setNodeId(value);
        return this;
    }

    public CompositeCleanseFunctionNode withNodeType(CompositeFunctionNodeType value) {
        setNodeType(value);
        return this;
    }

    public CompositeCleanseFunctionNode withFunctionName(String value) {
        setFunctionName(value);
        return this;
    }
}