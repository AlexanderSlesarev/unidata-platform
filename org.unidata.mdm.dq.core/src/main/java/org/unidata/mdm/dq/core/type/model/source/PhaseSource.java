/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source;

import org.unidata.mdm.core.type.model.source.AbstractCustomPropertiesHolder;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * @author Mikhail Mikhailov on Nov 9, 2021
 */
public class PhaseSource extends AbstractCustomPropertiesHolder<PhaseSource> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -2032355300216724389L;

    @JacksonXmlProperty(isAttribute = true, localName = "name")
    protected String name;

    @JacksonXmlProperty(isAttribute = true, localName = "displayName")
    protected String displayName;

    @JacksonXmlProperty(isAttribute = true, localName = "description")
    protected String description;
    /**
     * Constructor.
     */
    public PhaseSource() {
        super();
    }
    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     */
    public String getName() {
        return name;
    }
    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     */
    public void setName(String value) {
        this.name = value;
    }
    /**
     * Gets the value of the displayName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     */
    public String getDisplayName() {
        return displayName;
    }
    /**
     * Gets the value of the displayName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     */
    public void setDisplayName(String value) {
        this.displayName = value;
    }
    /**
     * Gets the value of the description property.
     *
     * @return
     *     possible object is
     *     {@link String }
     */
    public String getDescription() {
        return description;
    }
    /**
     * Sets the value of the description property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     */
    public void setDescription(String value) {
        this.description = value;
    }

    public PhaseSource withName(String value) {
        setName(value);
        return self();
    }

    public PhaseSource withDisplayName(String value) {
        setDisplayName(value);
        return self();
    }

    public PhaseSource withDescription(String value) {
        setDescription(value);
        return self();
    }
}
