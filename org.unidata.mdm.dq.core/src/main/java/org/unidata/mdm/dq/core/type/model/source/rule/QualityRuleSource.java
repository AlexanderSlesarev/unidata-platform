/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.rule;

import java.time.OffsetDateTime;

import javax.xml.datatype.XMLGregorianCalendar;

import org.unidata.mdm.core.type.model.source.AbstractCustomPropertiesHolder;
import org.unidata.mdm.dq.core.type.rule.QualityRuleRunCondition;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Data quality rule source.
 */
public class QualityRuleSource extends AbstractCustomPropertiesHolder<QualityRuleSource> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -6688053357794591239L;

    @JacksonXmlProperty(isAttribute = true, localName = "name")
    private String name;

    @JacksonXmlProperty(isAttribute = true, localName = "displayName")
    private String displayName;

    @JacksonXmlProperty(isAttribute = true, localName = "description")
    private String description;

    private SelectionSettings selectionSettings;

    private ValidationSettings validationSettings;

    private EnrichmentSettings enrichmentSettings;

    @JacksonXmlProperty(isAttribute = true)
    private String cleanseFunctionName;

    @JacksonXmlProperty(isAttribute = true)
    private QualityRuleRunCondition runCondition;

    @JacksonXmlProperty(isAttribute = true)
    private OffsetDateTime createDate;

    @JacksonXmlProperty(isAttribute = true)
    private String createdBy;

    @JacksonXmlProperty(isAttribute = true)
    private OffsetDateTime updateDate;

    @JacksonXmlProperty(isAttribute = true)
    private String updatedBy;

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setName(String value) {
        this.name = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }


    /**
     * Gets the value of the description property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the sourceSystemsSettings property.
     *
     * @return
     *     possible object is
     *     {@link SelectionSettings }
     *
     */
    public SelectionSettings getSelectionSettings() {
        return selectionSettings;
    }

    /**
     * Sets the value of the origins property.
     *
     * @param value
     *     allowed object is
     *     {@link SelectionSettings }
     *
     */
    public void setSelectionSettings(SelectionSettings value) {
        this.selectionSettings = value;
    }

    /**
     * Gets the value of the raise property.
     *
     * @return
     *     possible object is
     *     {@link ValidationSettings }
     *
     */
    public ValidationSettings getValidationSettings() {
        return validationSettings;
    }

    /**
     * Sets the value of the raise property.
     *
     * @param value
     *     allowed object is
     *     {@link ValidationSettings }
     *
     */
    public void setValidationSettings(ValidationSettings value) {
        this.validationSettings = value;
    }

    /**
     * Gets the value of the enrich property.
     *
     * @return
     *     possible object is
     *     {@link EnrichmentSettings }
     *
     */
    public EnrichmentSettings getEnrichmentSettings() {
        return enrichmentSettings;
    }

    /**
     * Sets the value of the enrich property.
     *
     * @param value
     *     allowed object is
     *     {@link EnrichmentSettings }
     *
     */
    public void setEnrichmentSettings(EnrichmentSettings value) {
        this.enrichmentSettings = value;
    }

    /**
     * Gets the value of the cleanseFunctionName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCleanseFunctionName() {
        return cleanseFunctionName;
    }

    /**
     * Sets the value of the cleanseFunctionName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCleanseFunctionName(String value) {
        this.cleanseFunctionName = value;
    }

    /**
     * Gets the value of the runCondition property.
     *
     * @return
     *     possible object is
     *     {@link QualityRuleRunCondition }
     *
     */
    public QualityRuleRunCondition getRunCondition() {
        if (runCondition == null) {
            return QualityRuleRunCondition.RUN_ON_REQUIRED_PRESENT;
        } else {
            return runCondition;
        }
    }

    /**
     * Sets the value of the runCondition property.
     *
     * @param value
     *     allowed object is
     *     {@link QualityRuleRunCondition }
     *
     */
    public void setRunCondition(QualityRuleRunCondition value) {
        this.runCondition = value;
    }

    /**
     * Gets the value of the createdAt property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public OffsetDateTime getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createdAt property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setCreateDate(OffsetDateTime value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the createdBy property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the updatedAt property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public OffsetDateTime getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the value of the updatedAt property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setUpdateDate(OffsetDateTime value) {
        this.updateDate = value;
    }

    /**
     * Gets the value of the updatedBy property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the value of the updatedBy property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUpdatedBy(String value) {
        this.updatedBy = value;
    }

    public QualityRuleSource withName(String value) {
        setName(value);
        return this;
    }

    public QualityRuleSource withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    public QualityRuleSource withDescription(String value) {
        setDescription(value);
        return this;
    }

    public QualityRuleSource withSelectionSettings(SelectionSettings value) {
        setSelectionSettings(value);
        return this;
    }

    public QualityRuleSource withValidationSettings(ValidationSettings value) {
        setValidationSettings(value);
        return this;
    }

    public QualityRuleSource withEnrichmentSettings(EnrichmentSettings value) {
        setEnrichmentSettings(value);
        return this;
    }

    public QualityRuleSource withCleanseFunctionName(String value) {
        setCleanseFunctionName(value);
        return this;
    }

    public QualityRuleSource withRunCondition(QualityRuleRunCondition value) {
        setRunCondition(value);
        return this;
    }

    public QualityRuleSource withUpdateDate(OffsetDateTime value) {
        setUpdateDate(value);
        return this;
    }

    public QualityRuleSource withCreateDate(OffsetDateTime value) {
        setCreateDate(value);
        return this;
    }

    public QualityRuleSource withUpdatedBy(String value) {
        setUpdatedBy(value);
        return this;
    }

    public QualityRuleSource withCreatedBy(String value) {
        setCreatedBy(value);
        return this;
    }
}
