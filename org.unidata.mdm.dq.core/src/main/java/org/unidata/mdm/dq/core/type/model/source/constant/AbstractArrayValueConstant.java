/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.constant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.dq.core.type.constant.ArrayValueConstantType;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author Mikhail Mikhailov on Jan 22, 2021
 * Types for CF constants.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
    @Type(value = DateArrayValueConstant.class, name = "DATE"),
    @Type(value = IntegerArrayValueConstant.class, name = "INTEGER"),
    @Type(value = NumberArrayValueConstant.class, name = "NUMBER"),
    @Type(value = StringArrayValueConstant.class, name = "STRING"),
    @Type(value = TimeArrayValueConstant.class, name = "TIME"),
    @Type(value = TimestampArrayValueConstant.class, name = "TIMESTAMP")
})
public abstract class AbstractArrayValueConstant<X extends AbstractArrayValueConstant<X, V>, V>
    implements Serializable, ArrayValueConstant<V> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 1199956737222699976L;
    /**
     * The value
     */
    protected List<V> values;
    /**
     * Constructor.
     */
    protected AbstractArrayValueConstant() {
        super();
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link ArrayValueConstantType }
     *
     */
    @Override
    @JsonProperty("type")
    public abstract ArrayValueConstantType getType();

    /**
     * Gets the value.
     * @return the value
     */
    @Override
    public List<V> getValues() {
        return Objects.isNull(values) ? Collections.emptyList() : values;
    }

    /**
     * Sets the value.
     * @param value the value to set
     */
    public void setValues(List<V> value) {
        this.values = value;
    }

    @SuppressWarnings("unchecked")
    public X withValues(V... value) {
        if (ArrayUtils.isNotEmpty(value)) {
            return withValues(Arrays.asList(value));
        }
        return self();
    }

    public X withValues(Collection<V> value) {
        if (CollectionUtils.isNotEmpty(value)) {

            if (Objects.isNull(this.values)) {
                this.values = new ArrayList<>();
            }

            this.values.addAll(value);
        }
        return self();
    }

    @SuppressWarnings("unchecked")
    protected X self() {
        return (X) this;
    }
}
