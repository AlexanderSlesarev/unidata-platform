/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.core.type.io;

import java.util.List;

import org.unidata.mdm.core.type.data.Attribute;

/**
 * @author Alexey Tsarapkin
 * Post state - the values, that were put to ports during DQ call at the time of function calling.
 */
public class DataQualityState {
    /**
     * Model path
     */
    private final String path;
    /**
     * DQ port
     */
    private final String port;
    /**
     * Port values
     */
    private final List<Attribute> value;
    /**
     * Constructor.
     * @param port the port name
     * @param path the model path
     * @param value the value
     */
    public DataQualityState(String port, String path, List<Attribute> value) {
        super();
        this.path = path;
        this.port = port;
        this.value = value;
    }
    /**
     * Model path
     * @return path
     */
    public String getPath() {
        return path;
    }
    /**
     * Get DQ port
     * @return
     */
    public String getPort() {
        return port;
    }
    /**
     * Get port values
     * @return
     */
    public List<Attribute> getValue() {
        return value;
    }
}
