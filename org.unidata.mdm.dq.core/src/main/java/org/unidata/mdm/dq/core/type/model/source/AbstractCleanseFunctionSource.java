/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.core.type.model.source.AbstractCustomPropertiesHolder;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionExecutionScope;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Base CF configuration class.
 */
public abstract class AbstractCleanseFunctionSource<X extends AbstractCleanseFunctionSource<X>> extends AbstractCustomPropertiesHolder<X> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -7821850345815096524L;

    @JacksonXmlProperty(isAttribute = true)
    private String groupName;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "executionScope")
    private List<CleanseFunctionExecutionScope> executionScopes;

    @JacksonXmlProperty(isAttribute = true, localName = "name")
    protected String name;

    @JacksonXmlProperty(isAttribute = true, localName = "displayName")
    protected String displayName;

    @JacksonXmlProperty(isAttribute = true, localName = "description")
    protected String description;

    @JacksonXmlProperty(isAttribute = true)
    protected String libraryName;

    @JacksonXmlProperty(isAttribute = true)
    protected String libraryVersion;

    @JacksonXmlProperty(isAttribute = true)
    protected OffsetDateTime createDate;

    @JacksonXmlProperty(isAttribute = true)
    protected String createdBy;

    @JacksonXmlProperty(isAttribute = true)
    protected OffsetDateTime updateDate;

    @JacksonXmlProperty(isAttribute = true)
    protected String updatedBy;

    @JacksonXmlElementWrapper(localName = "inputPorts", useWrapping = true)
    @JacksonXmlProperty(localName = "inputPort")
    private List<CleanseFunctionPort> inputPorts;

    @JacksonXmlElementWrapper(localName = "outputPorts", useWrapping = true)
    @JacksonXmlProperty(localName = "outputPort")
    private List<CleanseFunctionPort> outputPorts;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore(true)
    protected transient String note;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore(true)
    protected transient boolean system;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore(true)
    protected transient boolean ready;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore(true)
    protected transient boolean configurable;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore(true)
    protected transient boolean editable;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore(true)
    protected transient Supplier<String> displayNameSupplier;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore(true)
    protected transient Supplier<String> descriptionSupplier;

    @JsonProperty(value = "type")
    public abstract CleanseFunctionType getType();

    /**
     * Gets the value of the supportedExecutionContexts property.
     */
    public List<CleanseFunctionExecutionScope> getExecutionScopes() {
        if (executionScopes == null) {
            executionScopes = new ArrayList<>();
        }
        return this.executionScopes;
    }

    /**
     * @param executionScopes the executionScopes to set
     */
    public void setExecutionScopes(List<CleanseFunctionExecutionScope> executionScopes) {
        this.executionScopes = executionScopes;
    }

    /**
     * Gets the value of the functionName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the functionName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setName(String value) {
        this.name = value;
    }

    public String getDisplayName() {
        return Objects.nonNull(displayNameSupplier) ? displayNameSupplier.get() : displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    /**
     * @param displayNameSupplier the displayNameSupplier to set
     */
    public void setDisplayName(Supplier<String> displayNameSupplier) {
        this.displayNameSupplier = displayNameSupplier;
    }

    /**
     * Gets the value of the description property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescription() {
        return Objects.nonNull(descriptionSupplier) ? descriptionSupplier.get() : description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * @param descriptionSupplier the descriptionSupplier to set
     */
    public void setDescription(Supplier<String> descriptionSupplier) {
        this.descriptionSupplier = descriptionSupplier;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String value) {
        this.groupName = value;
    }


    /**
     * @return the libraryName
     */
    public String getLibraryName() {
        return libraryName;
    }

    /**
     * @param libraryName the libraryName to set
     */
    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    /**
     * @return the libraryVersion
     */
    public String getLibraryVersion() {
        return libraryVersion;
    }

    /**
     * @param libraryVersion the libraryVersion to set
     */
    public void setLibraryVersion(String libraryVersion) {
        this.libraryVersion = libraryVersion;
    }

    /**
     * Gets the value of the createdAt property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public OffsetDateTime getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createdAt property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setCreateDate(OffsetDateTime value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the createdBy property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the updatedAt property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public OffsetDateTime getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the value of the updatedAt property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setUpdateDate(OffsetDateTime value) {
        this.updateDate = value;
    }

    /**
     * Gets the value of the updatedBy property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the value of the updatedBy property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUpdatedBy(String value) {
        this.updatedBy = value;
    }

    public List<CleanseFunctionPort> getInputPorts() {
        if (inputPorts == null) {
            inputPorts = new ArrayList<>();
        }
        return inputPorts;
    }

    public void setInputPorts(List<CleanseFunctionPort> inputPorts) {
        this.inputPorts = inputPorts;
    }

    public List<CleanseFunctionPort> getOutputPorts() {
        if (outputPorts == null) {
            outputPorts = new ArrayList<>();
        }
        return outputPorts;
    }

    public void setOutputPorts(List<CleanseFunctionPort> outputPorts) {
        this.outputPorts = outputPorts;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the system
     */
    public boolean isSystem() {
        return system;
    }

    /**
     * @param system the system to set
     */
    public void setSystem(boolean system) {
        this.system = system;
    }

    /**
     * @return the ready
     */
    public boolean isReady() {
        return ready;
    }

    /**
     * @param ready the ready to set
     */
    public void setReady(boolean ready) {
        this.ready = ready;
    }

    /**
     * @return the configurable
     */
    public boolean isConfigurable() {
        return configurable;
    }

    /**
     * @param configurable the configurable to set
     */
    public void setConfigurable(boolean configurable) {
        this.configurable = configurable;
    }

    /**
     * @return the editable
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * @param editable the editable to set
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public X withExecutionScopes(CleanseFunctionExecutionScope... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withExecutionScopes(Arrays.asList(values));
        }
        return self();
    }

    public X withExecutionScopes(Collection<CleanseFunctionExecutionScope> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getExecutionScopes().addAll(values);
        }
        return self();
    }

    public X withName(String value) {
        setName(value);
        return self();
    }

    public X withDisplayName(String value) {
        setDisplayName(value);
        return self();
    }

    public X withDisplayName(Supplier<String> value) {
        setDisplayName(value);
        return self();
    }

    public X withDescription(String value) {
        setDescription(value);
        return self();
    }

    public X withDescription(Supplier<String> value) {
        setDescription(value);
        return self();
    }

    public X withGroupName(String value) {
        setGroupName(value);
        return self();
    }

    public X withLibraryName(String value) {
        setLibraryName(value);
        return self();
    }

    public X withLibraryVersion(String value) {
        setLibraryVersion(value);
        return self();
    }

    public X withUpdateDate(OffsetDateTime value) {
        setUpdateDate(value);
        return self();
    }

    public X withCreateDate(OffsetDateTime value) {
        setCreateDate(value);
        return self();
    }

    public X withUpdatedBy(String value) {
        setUpdatedBy(value);
        return self();
    }

    public X withCreatedBy(String value) {
        setCreatedBy(value);
        return self();
    }

    public X withInputPorts(CleanseFunctionPort... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withInputPorts(Arrays.asList(values));
        }
        return self();
    }

    public X withInputPorts(Collection<CleanseFunctionPort> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getInputPorts().addAll(values);
        }
        return self();
    }

    public X withOutputPorts(CleanseFunctionPort... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withOutputPorts(Arrays.asList(values));
        }
        return self();
    }

    public X withOutputPorts(Collection<CleanseFunctionPort> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getOutputPorts().addAll(values);
        }
        return self();
    }

    public X withNote(String value) {
        setNote(value);
        return self();
    }

    public X withSystem(boolean value) {
        setSystem(value);
        return self();
    }

    public X withReady(boolean value) {
        setReady(value);
        return self();
    }

    public X withConfigurable(boolean value) {
        setConfigurable(value);
        return self();
    }

    public X withEditable(boolean value) {
        setEditable(value);
        return self();
    }
}
