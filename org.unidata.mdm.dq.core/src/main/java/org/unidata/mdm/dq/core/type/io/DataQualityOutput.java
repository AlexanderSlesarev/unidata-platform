/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.io;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.impl.SerializableDataRecord;

/**
 * @author Mikhail Mikhailov on Mar 6, 2021
 * Type, holding changes
 */
public class DataQualityOutput extends AbstractInputOutput {
    /**
     * Constructor.
     */
    DataQualityOutput(DataQualityInput input) {
        super(DataQualityOutput.clone(input));
    }
    /**
     * Clones the input records to new content.
     * @param input the input
     * @return map
     */
    private static Map<String, List<Pair<String, DataRecord>>> clone(DataQualityInput input) {

        if (Objects.isNull(input) || input.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<String, List<Pair<String, DataRecord>>> result = new HashMap<>(input.records.size());
        for (Entry<String, List<DataRecord>> r : input.records.entrySet()) {

            List<Pair<String, DataRecord>> part = new ArrayList<>(r.getValue().size());
            for (int i = 0; i < r.getValue().size(); i++) {
                DataRecord current = r.getValue().get(i);
                part.add(Pair.of(input.identity.get(current), SerializableDataRecord.of(current)));
            }

            result.put(r.getKey(), part);
        }

        return result;
    }
}
