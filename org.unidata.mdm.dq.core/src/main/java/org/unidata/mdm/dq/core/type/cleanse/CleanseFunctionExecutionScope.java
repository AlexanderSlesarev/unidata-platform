/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.cleanse;

/**
 * Types of execution scopes, supported by a cleanse function.
 */
public enum CleanseFunctionExecutionScope {
    /**
     * Function supports execution against a set of attributes,
     * collected by recursive full tree filtering in any (sub)record,
     * that matches given UPath expression.
     * Function is executed ones per rule, since it does not matter,
     * where an attribute comes from and how its neighbours do look like.
     */
    GLOBAL,
    /**
     * Function supports execution against a set of attributes from the same single record,
     * that matches given UPath expression.
     * Because of this a function can be executed multiple times per single rule,
     * since it is executed for each filtered record (from a complex attribute, for example).
     */
    LOCAL;
}
