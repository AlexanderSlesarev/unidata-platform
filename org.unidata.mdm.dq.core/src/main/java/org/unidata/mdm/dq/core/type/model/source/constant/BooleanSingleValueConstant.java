package org.unidata.mdm.dq.core.type.model.source.constant;

import org.unidata.mdm.dq.core.type.constant.SingleValueConstantType;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Mikhail Mikhailov on Jan 22, 2021
 */
public class BooleanSingleValueConstant extends AbstractSingleValueConstant<BooleanSingleValueConstant, Boolean> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -3803510837446170865L;
    /**
     * Constructor.
     */
    public BooleanSingleValueConstant() {
        super();
    }

    @JsonProperty("type")
    @Override
    public SingleValueConstantType getType() {
        return SingleValueConstantType.BOOLEAN;
    }
}
