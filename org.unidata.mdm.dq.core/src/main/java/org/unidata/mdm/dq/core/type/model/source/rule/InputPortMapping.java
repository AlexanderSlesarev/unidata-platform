/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.rule;

import org.unidata.mdm.dq.core.type.model.source.constant.CleanseFunctionConstant;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * @author Mikhail Mikhailov on Feb 26, 2021
 */
public class InputPortMapping extends AbstractPortMapping<InputPortMapping> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -776130805008821071L;

    @JacksonXmlProperty(localName = "constantValue")
    protected CleanseFunctionConstant constantValue;

    @JacksonXmlProperty(localName = "inputPath")
    protected String inputPath;

    /**
     * Constructor.
     */
    public InputPortMapping() {
        super();
    }

    /**
     * Gets the value of the constant property.
     *
     * @return constant
     */
    public CleanseFunctionConstant getConstantValue() {
        return constantValue;
    }

    /**
     * Sets the value of the constant property.
     *
     * @param value the constant
     */
    public void setConstantValue(CleanseFunctionConstant value) {
        this.constantValue = value;
    }

    /**
     * Gets the value of the path property.
     *
     * @return path
     */
    public String getInputPath() {
        return inputPath;
    }

    /**
     * Sets the value of the path property.
     *
     * @param value the path value
     */
    public void setInputPath(String value) {
        this.inputPath = value;
    }

    public InputPortMapping withConstantValue(CleanseFunctionConstant value) {
        setConstantValue(value);
        return self();
    }

    public InputPortMapping withInputPath(String value) {
        setInputPath(value);
        return self();
    }
}
