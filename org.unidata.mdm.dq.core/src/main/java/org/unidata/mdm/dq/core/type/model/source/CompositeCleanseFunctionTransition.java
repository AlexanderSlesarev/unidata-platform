package org.unidata.mdm.dq.core.type.model.source;

import java.io.Serializable;
import java.math.BigInteger;

import org.unidata.mdm.dq.core.type.composite.CompositeFunctionNodeType;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Transition.
 */
public class CompositeCleanseFunctionTransition implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -2934925866481236714L;

    @JacksonXmlProperty(isAttribute = true)
    protected Integer fromNodeId;

    @JacksonXmlProperty(isAttribute = true)
    protected String fromPort;

    @JacksonXmlProperty(isAttribute = true)
    protected CompositeFunctionNodeType fromPortType;

    @JacksonXmlProperty(isAttribute = true)
    protected Integer toNodeId;

    @JacksonXmlProperty(isAttribute = true)
    protected String toPort;

    @JacksonXmlProperty(isAttribute = true)
    protected CompositeFunctionNodeType toPortType;

    /**
     * Gets the value of the fromNodeId property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public Integer getFromNodeId() {
        return fromNodeId;
    }

    /**
     * Sets the value of the fromNodeId property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setFromNodeId(Integer value) {
        this.fromNodeId = value;
    }

    /**
     * Gets the value of the fromPort property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFromPort() {
        return fromPort;
    }

    /**
     * Sets the value of the fromPort property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFromPort(String value) {
        this.fromPort = value;
    }

    /**
     * Gets the value of the fromPortType property.
     *
     * @return
     *     possible object is
     *     {@link CompositeFunctionNodeType }
     *
     */
    public CompositeFunctionNodeType getFromPortType() {
        return fromPortType;
    }

    /**
     * Sets the value of the fromPortType property.
     *
     * @param value
     *     allowed object is
     *     {@link CompositeFunctionNodeType }
     *
     */
    public void setFromPortType(CompositeFunctionNodeType value) {
        this.fromPortType = value;
    }

    /**
     * Gets the value of the toNodeId property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public Integer getToNodeId() {
        return toNodeId;
    }

    /**
     * Sets the value of the toNodeId property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setToNodeId(Integer value) {
        this.toNodeId = value;
    }

    /**
     * Gets the value of the toPort property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getToPort() {
        return toPort;
    }

    /**
     * Sets the value of the toPort property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setToPort(String value) {
        this.toPort = value;
    }

    /**
     * Gets the value of the toPortType property.
     *
     * @return
     *     possible object is
     *     {@link CompositeFunctionNodeType }
     *
     */
    public CompositeFunctionNodeType getToPortType() {
        return toPortType;
    }

    /**
     * Sets the value of the toPortType property.
     *
     * @param value
     *     allowed object is
     *     {@link CompositeFunctionNodeType }
     *
     */
    public void setToPortType(CompositeFunctionNodeType value) {
        this.toPortType = value;
    }

    public CompositeCleanseFunctionTransition withFromNodeId(Integer value) {
        setFromNodeId(value);
        return this;
    }

    public CompositeCleanseFunctionTransition withFromPort(String value) {
        setFromPort(value);
        return this;
    }

    public CompositeCleanseFunctionTransition withFromPortType(CompositeFunctionNodeType value) {
        setFromPortType(value);
        return this;
    }

    public CompositeCleanseFunctionTransition withToNodeId(Integer value) {
        setToNodeId(value);
        return this;
    }

    public CompositeCleanseFunctionTransition withToPort(String value) {
        setToPort(value);
        return this;
    }

    public CompositeCleanseFunctionTransition withToPortType(CompositeFunctionNodeType value) {
        setToPortType(value);
        return this;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("(from node ID = ")
                .append(fromNodeId)
                .append(", from port # = ")
                .append(fromPort)
                .append(", to node ID = ")
                .append(toNodeId)
                .append(", to port # = ")
                .append(toPort)
                .append(")")
                .toString();
    }
}