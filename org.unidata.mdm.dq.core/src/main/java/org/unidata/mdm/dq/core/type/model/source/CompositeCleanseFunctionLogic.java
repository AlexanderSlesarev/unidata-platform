package org.unidata.mdm.dq.core.type.model.source;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * Composite cleanse function logic.
 */
public class CompositeCleanseFunctionLogic implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -4738065825693366692L;
    /**
     * Vertices.
     */
    @JacksonXmlElementWrapper(localName = "nodes", useWrapping = true)
    @JacksonXmlProperty(localName = "node")
    private List<CompositeCleanseFunctionNode> nodes;
    /**
     * Edges.
     */
    @JacksonXmlElementWrapper(localName = "transitions")
    @JacksonXmlProperty(localName = "transition")
    private List<CompositeCleanseFunctionTransition> transitions;

    public List<CompositeCleanseFunctionNode> getNodes() {
        if (nodes == null) {
            nodes = new ArrayList<>();
        }
        return nodes;
    }

    public void setNodes(List<CompositeCleanseFunctionNode> nodes) {
        this.nodes = nodes;
    }

    public List<CompositeCleanseFunctionTransition> getTransitions() {
        if (transitions == null) {
            transitions = new ArrayList<>();
        }
        return transitions;
    }

    public void setTransitions(List<CompositeCleanseFunctionTransition> links) {
        this.transitions = links;
    }

    public CompositeCleanseFunctionLogic withNodes(CompositeCleanseFunctionNode... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withNodes(Arrays.asList(values));
        }
        return this;
    }

    public CompositeCleanseFunctionLogic withNodes(Collection<CompositeCleanseFunctionNode> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getNodes().addAll(values);
        }
        return this;
    }

    public CompositeCleanseFunctionLogic withTransitions(CompositeCleanseFunctionTransition... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withTransitions(Arrays.asList(values));
        }
        return this;
    }

    public CompositeCleanseFunctionLogic withTransitions(Collection<CompositeCleanseFunctionTransition> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getTransitions().addAll(values);
        }
        return this;
    }

}
