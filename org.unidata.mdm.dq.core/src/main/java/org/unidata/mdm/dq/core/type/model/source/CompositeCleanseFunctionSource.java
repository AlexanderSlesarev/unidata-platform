package org.unidata.mdm.dq.core.type.model.source;

import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionType;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Composite cleanse function.
 */
public class CompositeCleanseFunctionSource extends AbstractCleanseFunctionSource<CompositeCleanseFunctionSource> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 8146283710207062950L;
    /**
     * The logic.
     */
    private CompositeCleanseFunctionLogic logic;
    /**
     * {@inheritDoc}
     */
    @JsonProperty(value = "type")
    @Override
    public CleanseFunctionType getType() {
        return CleanseFunctionType.COMPOSITE;
    }

    /**
     * Gets the value of the logic property.
     *
     * @return
     *     possible object is
     *     {@link CompositeCleanseFunctionLogic }
     *
     */
    public CompositeCleanseFunctionLogic getLogic() {
        return logic;
    }

    /**
     * Sets the value of the logic property.
     *
     * @param value
     *     allowed object is
     *     {@link CompositeCleanseFunctionLogic }
     *
     */
    public void setLogic(CompositeCleanseFunctionLogic value) {
        this.logic = value;
    }

    public CompositeCleanseFunctionSource withLogic(CompositeCleanseFunctionLogic value) {
        setLogic(value);
        return this;
    }
}
