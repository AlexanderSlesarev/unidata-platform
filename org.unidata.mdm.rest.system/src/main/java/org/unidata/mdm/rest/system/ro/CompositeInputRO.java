package org.unidata.mdm.rest.system.ro;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.unidata.mdm.system.type.rendering.InputRenderingAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * The base input composite object.
 * Contains fields for dynamic processing by system modules
 *
 * @author Alexandr Serov
 * @since 28.09.2020
 **/
public abstract class CompositeInputRO implements RestInputSource {
    /**
     * Dynamic object fields
     */
    private FragmentsRO payload;

    @JsonIgnore
    public abstract InputRenderingAction getInputRenderingAction();

    /**
     * Adds a part to output.
     *
     * @param name the property name
     * @param value the value
     */
    // @JsonAnySetter
    @JsonIgnore
    public void setAny(String name, JsonNode value) {
        // payload.put(name, value);
    }

    @Override
    // @JsonAnyGetter
    @JsonIgnore
    public Map<String, JsonNode> getAny() {
        return Objects.nonNull(payload) ? payload.getAny() : Collections.emptyMap();
    }

    /**
     * @return the payload
     */
    public FragmentsRO getPayload() {
        return payload;
    }

    /**
     * @param payload the payload to set
     */
    public void setPayload(FragmentsRO payload) {
        this.payload = payload;
    }
}
