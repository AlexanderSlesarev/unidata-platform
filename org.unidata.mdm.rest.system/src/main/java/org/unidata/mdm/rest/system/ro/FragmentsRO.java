/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.system.ro;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Mikhail Mikhailov on Apr 4, 2021
 */
public class FragmentsRO {
    /**
     * Fragments.
     */
    private Map<String, JsonNode> fragments = new HashMap<>();
    /**
     * Constructor.
     */
    public FragmentsRO() {
        super();
    }
    /**
     * Sets a field.
     * @param name the field name
     * @param value the field value
     */
    @JsonAnySetter
    public void setAny(String name, JsonNode value) {
        fragments.put(name, value);
    }
    /**
     * Gets fragments
     * @return output parts
     */
    @JsonAnyGetter
    public Map<String, JsonNode> getAny() {
        return Objects.isNull(fragments) ? Collections.emptyMap() : fragments;
    }
}
