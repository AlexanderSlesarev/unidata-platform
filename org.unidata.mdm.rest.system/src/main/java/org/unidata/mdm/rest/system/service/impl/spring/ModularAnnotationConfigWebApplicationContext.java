package org.unidata.mdm.rest.system.service.impl.spring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.unidata.mdm.system.type.spring.ModularApplicationContext;

/**
 * @author Mikhail Mikhailov on May 19, 2020
 */
public class ModularAnnotationConfigWebApplicationContext
    extends AnnotationConfigWebApplicationContext
    implements ModularApplicationContext {
    /**
     * Modules stack.
     */
    private final List<AbstractApplicationContext> stack = new ArrayList<>(32);
    /**
     * Was already refreshed.
     */
    private boolean ready;
    /**
     * Constructor.
     */
    public ModularAnnotationConfigWebApplicationContext() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractApplicationContext getFirst() {
        return stack.get(0);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractApplicationContext getLast() {
        return stack.get(stack.size() - 1);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void addChild(AbstractApplicationContext context) {
        stack.add(context);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public AbstractApplicationContext getChild(String id) {
        return stack.stream()
                .filter(c -> c.getId().equals(id))
                .findFirst()
                .orElse(null);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<AbstractApplicationContext> getChildren() {
        return stack;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getSize() {
        return stack.size();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<String> getIds() {
        return stack.stream()
                .map(AbstractApplicationContext::getId)
                .collect(Collectors.toList());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isReady() {
        return ready;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setReady(boolean ready) {
        this.ready = ready;
    }
}
