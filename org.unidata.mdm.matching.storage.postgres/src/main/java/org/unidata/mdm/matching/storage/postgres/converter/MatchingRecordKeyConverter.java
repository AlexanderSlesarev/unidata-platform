/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.converter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.matching.core.type.cluster.ClusterRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordKeyPO;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 09.09.2021
 */
public class MatchingRecordKeyConverter {

    /**
     * Constructor.
     */
    private MatchingRecordKeyConverter() {
        super();
    }

    /**
     * Convert key to PO.
     *
     * @param key the key
     * @return po
     */
    public static MatchingRecordKeyPO keyToPO(MatchingRecordKey key) {

        if (Objects.isNull(key)) {
            return null;
        }

        MatchingRecordKeyPO po = new MatchingRecordKeyPO();

        po.setNamespace(key.getNamespace());
        po.setTypeName(key.getTypeName());
        po.setSubjectId(key.getSubjectId());
        po.setValidFrom(key.getValidFrom());
        po.setValidTo(key.getValidTo());

        return po;
    }

    /**
     * Convert keys to POs.
     *
     * @param keys keys
     * @return pos
     */
    public static List<MatchingRecordKeyPO> keyToPO(Collection<MatchingRecordKey> keys) {
        if (CollectionUtils.isEmpty(keys)) {
            return Collections.emptyList();
        }

        return keys.stream()
                .map(MatchingRecordKeyConverter::keyToPO)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Convert key to cluster record.
     *
     * @param key key
     * @return cluster record
     */
    public static ClusterRecord keyToClusterRecord(MatchingRecordKey key) {

        ClusterRecord clusterRecord = new ClusterRecord();

        clusterRecord.setNamespace(key.getNamespace());
        clusterRecord.setTypeName(key.getTypeName());
        clusterRecord.setSubjectId(key.getSubjectId());
        clusterRecord.setValidFrom(key.getValidFrom());
        clusterRecord.setValidTo(key.getValidTo());

        return clusterRecord;
    }

    /**
     * Convert key po to cluster record.
     *
     * @param key          key po
     * @param matchingRate the matching rate
     * @return cluster record
     */
    public static ClusterRecord keyPOToClusterRecord(MatchingRecordKeyPO key, int matchingRate) {

        ClusterRecord clusterRecord = new ClusterRecord();

        clusterRecord.setNamespace(key.getNamespace());
        clusterRecord.setTypeName(key.getTypeName());
        clusterRecord.setSubjectId(key.getSubjectId());
        clusterRecord.setValidFrom(key.getValidFrom());
        clusterRecord.setValidTo(key.getValidTo());
        clusterRecord.setMatchingRate(matchingRate);

        return clusterRecord;
    }

    /**
     * Convert cluster record to key.
     *
     * @param clusterRecord cluster record
     * @return key
     */
    public static MatchingRecordKey clusterRecordToKey(ClusterRecord clusterRecord) {

        MatchingRecordKey key = new MatchingRecordKey();

        key.setNamespace(clusterRecord.getNamespace());
        key.setTypeName(clusterRecord.getTypeName());
        key.setSubjectId(clusterRecord.getSubjectId());
        key.setValidFrom(clusterRecord.getValidFrom());
        key.setValidTo(clusterRecord.getValidTo());

        return key;
    }

    /**
     * Convert matching record key to id.
     *
     * @param key matching record key
     * @return id
     */
    public static String keyPOToId(MatchingRecordKeyPO key) {
        Object[] parts = new Object[]{
                key.getNamespace(),
                key.getTypeName(),
                key.getSubjectId(),
                key.getValidTo().getTime()
        };

        return StringUtils.join(parts, "_");
    }

    /**
     * Convert matching record keys to ids.
     *
     * @param keys matching record keys
     * @return ids
     */
    public static List<String> keyPOToId(Collection<MatchingRecordKeyPO> keys) {

        if (CollectionUtils.isEmpty(keys)) {
            return Collections.emptyList();
        }

        return keys.stream()
                .map(MatchingRecordKeyConverter::keyPOToId)
                .collect(Collectors.toList());

    }

    /**
     * Convert matching record key to id.
     *
     * @param key matching record key
     * @return id
     */
    public static String keyToId(MatchingRecordKey key) {
        Object[] parts = new Object[]{
                key.getNamespace(),
                key.getTypeName(),
                key.getSubjectId(),
                key.getValidTo().getTime()
        };

        return StringUtils.join(parts, "_");
    }

    /**
     * Convert matching record keys to ids.
     *
     * @param keys matching record keys
     * @return ids
     */
    public static List<String> keyToId(List<MatchingRecordKey> keys) {

        if (CollectionUtils.isEmpty(keys)) {
            return Collections.emptyList();
        }

        return keys.stream()
                .map(MatchingRecordKeyConverter::keyToId)
                .collect(Collectors.toList());

    }
}
