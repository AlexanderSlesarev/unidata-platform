/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.service.impl.algorithm;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.matching.core.service.impl.algorithm.AbstractSystemMatchingAlgorithm;
import org.unidata.mdm.matching.core.type.algorithm.AlgorithmParamConfiguration;
import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithmConfiguration;
import org.unidata.mdm.matching.core.type.data.MatchingDataType;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmSettingsElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingColumnElement;
import org.unidata.mdm.matching.storage.postgres.configuration.PostgresMatchingStorageConfigurationConstants;
import org.unidata.mdm.matching.storage.postgres.type.algorithm.PostgresMatchingAlgorithm;
import org.unidata.mdm.matching.storage.postgres.type.algorithm.PreprocessingRule;
import org.unidata.mdm.matching.storage.postgres.type.column.Column;
import org.unidata.mdm.matching.storage.postgres.type.column.ColumnType;
import org.unidata.mdm.system.util.TextUtils;

import java.util.Collections;

/**
 * @author Sergey Murskiy on 23.08.2021
 */
public class ExactAlgorithm extends AbstractSystemMatchingAlgorithm implements PostgresMatchingAlgorithm {

    /**
     * Name.
     */
    private static final String EXACT_ALGORITHM_NAME = "exactMatchingAlgorithm";
    /**
     * Display name.
     */
    private static final String EXACT_ALGORITHM_DISPLAY_NAME = "org.unidata.mdm.matching.storage.postgres.algorithm.exactMatchingAlgorithm.display.name";
    /**
     * Description.
     */
    private static final String EXACT_ALGORITHM_DESCRIPTION = "org.unidata.mdm.matching.storage.postgres.algorithm.exactMatchingAlgorithm.description";


    /**
     * Case insensitive parameter.
     */
    private static final String CASE_INSENSITIVE_PARAM = "caseInsensitive";
    /**
     * Display name.
     */
    private static final String CASE_INSENSITIVE_PARAM_DISPLAY_NAME = "org.unidata.mdm.matching.storage.postgres.algorithm.exactMatchingAlgorithm.param.caseInsensitive.display.name";
    /**
     * Description.
     */
    private static final String CASE_INSENSITIVE_PARAM_DESCRIPTION = "org.unidata.mdm.matching.storage.postgres.algorithm.exactMatchingAlgorithm.param.caseInsensitive.description";

    /**
     * Case insensitive parameter.
     */
    private static final String CASE_INSENSITIVE_COLUMN_POSTFIX = "_ci";
    /**
     * Case insensitive preprocessing rule.
     */
    private static final PreprocessingRule CASE_INSENSITIVE_RULE = input -> input.getValue().toString().toLowerCase();

    /**
     * Constructor.
     */
    public ExactAlgorithm() {
        super(EXACT_ALGORITHM_NAME,
                () -> TextUtils.getText(EXACT_ALGORITHM_DISPLAY_NAME),
                () -> TextUtils.getText(EXACT_ALGORITHM_DESCRIPTION),
                PostgresMatchingStorageConfigurationConstants.MATCHING_STORAGE_NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Column processColumn(MatchingColumnElement column, AlgorithmSettingsElement settings) {
        if (column.getType() == MatchingDataType.STRING) {
            Boolean caseInsensitive = settings.getParameter(CASE_INSENSITIVE_PARAM).getValue();

            if (BooleanUtils.isTrue(caseInsensitive)) {
                return Column.of(StringUtils.join(column.getName(), CASE_INSENSITIVE_COLUMN_POSTFIX),
                        column.getName(),
                        ColumnType.STRING,
                        CASE_INSENSITIVE_RULE);
            }
        }

        return Column.of(column.getName(), column.getName(), ColumnType.fromDataType(column.getType()), null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingAlgorithmConfiguration configure() {
        AlgorithmParamConfiguration<Boolean> caseInsensitive = AlgorithmParamConfiguration.bool()
                .name(CASE_INSENSITIVE_PARAM)
                .displayName(() -> TextUtils.getText(CASE_INSENSITIVE_PARAM_DISPLAY_NAME))
                .description(() -> TextUtils.getText(CASE_INSENSITIVE_PARAM_DESCRIPTION))
                .required(false)
                .defaultValue(false)
                .build();

        return MatchingAlgorithmConfiguration.configuration()
                .parameter(Collections.singletonList(caseInsensitive))
                .build();
    }
}
