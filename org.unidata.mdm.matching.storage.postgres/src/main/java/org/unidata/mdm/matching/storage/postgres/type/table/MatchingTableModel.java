/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.type.table;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.storage.postgres.type.column.Column;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
public class MatchingTableModel {
    /**
     * Matching table name.
     */
    private String tableName;
    /**
     * Matching table columns.
     */
    private List<Column> columns;

    /**
     * Gets matching table name.
     *
     * @return the matching table name
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * Sets matching table name.
     *
     * @param tableName the matching table name to set
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Gets matching table columns.
     *
     * @return matching table columns
     */
    public List<Column> getColumns() {
        if (CollectionUtils.isEmpty(columns)) {
            columns = new ArrayList<>();
        }

        return columns;
    }

    /**
     * Sets matching table columns.
     *
     * @param columns matching table columns to set
     */
    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    /**
     * Matching table with name.
     *
     * @param tableName the name to set
     * @return self
     */
    public MatchingTableModel withTableName(String tableName) {
        setTableName(tableName);
        return this;
    }

    /**
     * Matching table with columns.
     *
     * @param columns matching table columns to set
     * @return self
     */
    public MatchingTableModel withColumns(Collection<Column> columns) {
        getColumns().addAll(columns);
        return this;
    }
}
