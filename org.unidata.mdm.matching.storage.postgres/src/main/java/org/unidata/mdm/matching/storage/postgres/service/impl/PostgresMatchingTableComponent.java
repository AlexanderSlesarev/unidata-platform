/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.type.model.instance.MatchingDeploymentElement;
import org.unidata.mdm.matching.storage.postgres.dao.PostgresMatchingTableDAO;
import org.unidata.mdm.matching.storage.postgres.type.table.MatchingTableModel;
import org.unidata.mdm.matching.storage.postgres.util.MatchingTableModelUtils;


/**
 * @author Sergey Murskiy on 18.08.2021
 */
@Component
public class PostgresMatchingTableComponent {
    /**
     * Matching table DAO.
     */
    @Autowired
    private PostgresMatchingTableDAO postgresMatchingTableDAO;

    /**
     * Creates table for matching deployment.
     *
     * @param deployment matching deployment
     */
    public void createTable(MatchingDeploymentElement deployment) {
        MatchingTableModel model = MatchingTableModelUtils.createModel(deployment);

        postgresMatchingTableDAO.create(model.getTableName(), model.getColumns());
    }

    /**
     * Drop table by name
     * @param tableName the table name
     */
    public void dropTable(String tableName) {
        postgresMatchingTableDAO.drop(tableName);
    }
}
