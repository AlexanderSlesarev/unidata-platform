/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.dao.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.unidata.mdm.matching.storage.postgres.dao.PostgresMatchingTableDAO;
import org.unidata.mdm.matching.storage.postgres.type.column.Column;
import org.unidata.mdm.system.dao.impl.BaseDAOImpl;

import javax.sql.DataSource;
import java.util.Collection;
import java.util.Properties;

/**
 * @author Sergey Murskiy on 23.08.2021
 */
@Repository
public class PostgresMatchingTableDAOImpl extends BaseDAOImpl implements PostgresMatchingTableDAO {

    /**
     * SQLs.
     */
    private final String createSQL;
    private final String dropSQL;

    /**
     * Constructor.
     */
    @Autowired
    public PostgresMatchingTableDAOImpl(
            @Qualifier("postgresMatchingStorageDatasource") final DataSource dataSource,
            @Qualifier("matching-table-sql") final Properties sql) {

        super(dataSource);

        this.createSQL = sql.getProperty("createSQL");
        this.dropSQL = sql.getProperty("dropSQL");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void create(String tableName, Collection<Column> columns) {
        String statement = fillCreateTemplate(tableName, columns);

        getJdbcTemplate().update(statement);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void drop(String tableName) {
        String statement = fillDropTemplate(tableName);

        getJdbcTemplate().update(statement);
    }

    private String fillCreateTemplate(String tableName, Collection<Column> columns) {

        String tableColumns = StringUtils.join(columns.stream()
                .map(column -> column.getName() + " " + column.getType().getPostgresType())
                .toArray(), ", ");

        return createSQL
                .replace("#{tableName}", tableName)
                .replace("#{tableColumns}", tableColumns);
    }

    private String fillDropTemplate(String tableName) {

        return dropSQL
                .replace("#{tableName}", tableName);
    }
}
