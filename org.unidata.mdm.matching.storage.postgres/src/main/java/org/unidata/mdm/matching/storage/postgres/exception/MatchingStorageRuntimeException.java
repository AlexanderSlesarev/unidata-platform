package org.unidata.mdm.matching.storage.postgres.exception;

import org.unidata.mdm.system.exception.DomainId;
import org.unidata.mdm.system.exception.ExceptionId;
import org.unidata.mdm.system.exception.PlatformRuntimeException;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
public class MatchingStorageRuntimeException extends PlatformRuntimeException {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -4084658878753448735L;

    /**
     * Domain.
     */
    private static final DomainId MATCHING_EXCEPTION = () -> "MATCHING_STORAGE_RUNTIME_EXCEPTION";

    /**
     * @param message the message
     * @param id the id
     */
    public MatchingStorageRuntimeException(String message, ExceptionId id, Object... args) {
        super(message, id, args);
    }

    /**
     * @param message the message
     * @param cause the cause
     * @param id the id
     */
    public MatchingStorageRuntimeException(String message, Throwable cause, ExceptionId id, Object... args) {
        super(message, cause, id, args);
    }

    /**
     * @param cause the cause
     * @param id the id
     */
    public MatchingStorageRuntimeException(Throwable cause, ExceptionId id, Object... args) {
        super(cause, id, args);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DomainId getDomain() {
        return MATCHING_EXCEPTION;
    }
}
