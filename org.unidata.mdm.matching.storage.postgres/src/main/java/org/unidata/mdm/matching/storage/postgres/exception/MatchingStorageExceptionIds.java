/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.exception;

import org.unidata.mdm.system.exception.ExceptionId;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public final class MatchingStorageExceptionIds {
    /**
     * Constructor.
     */
    private MatchingStorageExceptionIds() {
    }

    public static final ExceptionId EX_CE_STORAGE_INSERT_DATA_BATCH_FAILED =
            new ExceptionId("EX_CE_STORAGE_INSERT_DATA_BATCH_FAILED", "app.matching.cannot.uncompress.data");

    public static final ExceptionId EX_CE_STORAGE_DELETE_DATA_BATCH_FAILED =
            new ExceptionId("EX_CE_STORAGE_DELETE_DATA_BATCH_FAILED", "app.matching.cannot.uncompress.data");


}
