/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.util;

import org.unidata.mdm.matching.core.type.model.instance.AlgorithmSettingsElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingColumnElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingDeploymentElement;
import org.unidata.mdm.matching.storage.postgres.type.algorithm.PostgresMatchingAlgorithm;
import org.unidata.mdm.matching.storage.postgres.type.column.Column;
import org.unidata.mdm.matching.storage.postgres.type.column.SystemColumns;
import org.unidata.mdm.matching.storage.postgres.type.table.MatchingTableModel;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
public final class MatchingTableModelUtils {

    /**
     * Constructor.
     */
    private MatchingTableModelUtils() {
        super();
    }

    /**
     * Created matching table model.
     *
     * @param deployment matching deployment
     * @return matching table model
     */
    public static MatchingTableModel createModel(MatchingDeploymentElement deployment) {

        LinkedHashSet<Column> targetColumns = new LinkedHashSet<>(SystemColumns.getColumns());

        Map<MatchingColumnElement, List<AlgorithmSettingsElement>> columns = deployment.getColumnAssignments();

        for (Map.Entry<MatchingColumnElement, List<AlgorithmSettingsElement>> e : columns.entrySet()) {
            MatchingColumnElement columnElement = e.getKey();
            for (AlgorithmSettingsElement settings : e.getValue()) {
                Column column = ((PostgresMatchingAlgorithm) settings.getAlgorithm()).processColumn(columnElement,
                        settings);
                targetColumns.add(column);
            }
        }

        int columnOrder = 1;
        for (Column column : targetColumns) {
            column.setOrder(columnOrder++);
        }

        return new MatchingTableModel()
                .withTableName(deployment.getMatchingTableName())
                .withColumns(targetColumns);
    }
}
