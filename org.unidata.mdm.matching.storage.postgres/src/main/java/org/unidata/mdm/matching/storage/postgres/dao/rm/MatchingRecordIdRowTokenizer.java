/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.dao.rm;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.dao.tokenizer.AbstractRowTokenizer;
import org.unidata.mdm.core.dao.tokenizer.CompositeValueIterator;
import org.unidata.mdm.core.dao.tokenizer.CompositeValueTokenizer;
import org.unidata.mdm.core.dao.tokenizer.RowTokenizerFields;
import org.unidata.mdm.core.dao.vendor.VendorUtils;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordKeyPO;

import java.util.function.BiConsumer;

/**
 * @author Sergey Murskiy on 17.08.2021
 */
public class MatchingRecordIdRowTokenizer extends AbstractRowTokenizer<MatchingRecordKeyPO> {
    /**
     * Default tokenizer.
     */
    public static final MatchingRecordIdRowTokenizer DEFAULT_MATCHING_RECORD_ID_TOKENIZER
            = new MatchingRecordIdRowTokenizer();

    /**
     * Fields as they follow in the declaration.
     */
    enum MatchingRecordIdFields implements RowTokenizerFields<MatchingRecordKeyPO> {

        NAMESPACE((v, po) -> po.setNamespace(v)),
        TYPE_NAME((v, po) -> po.setTypeName(v)),
        SUBJECT_ID((v, po) -> po.setSubjectId(v)),
        VALID_FROM((v, po) -> po.setValidFrom(VendorUtils.stringToTimestamp(v))),
        VALID_TO((v, po) -> po.setValidTo(VendorUtils.stringToTimestamp(v)));

        MatchingRecordIdFields(BiConsumer<String, MatchingRecordKeyPO> f) {
            this.converter = f;
        }

        private final BiConsumer<String, MatchingRecordKeyPO> converter;

        /**
         * {@inheritDoc}
         */
        @Override
        public BiConsumer<String, MatchingRecordKeyPO> consumer() {
            return converter;
        }
    }

    /**
     * Constructor.
     */
    protected MatchingRecordIdRowTokenizer() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int size() {
        return MatchingRecordIdFields.values().length;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void process(CompositeValueIterator tk, MatchingRecordKeyPO po) {

        for (int i = 0; i < MatchingRecordIdFields.values().length && tk.hasNext(); i++) {

            String token = tk.next();
            if (StringUtils.isBlank(token)) {
                continue;
            }

            MatchingRecordIdFields.values()[i].consumer().accept(token, po);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected MatchingRecordKeyPO process(CompositeValueTokenizer fields) {

        int sz = size();
        if (fields.getSize() != sz) {
            return null;
        }

        CompositeValueIterator tk = new CompositeValueIterator(fields);
        MatchingRecordKeyPO po = new MatchingRecordKeyPO();
        process(tk, po);

        return po;
    }
}
