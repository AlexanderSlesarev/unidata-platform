/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.service.impl;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.context.ClusterUpdateContext;
import org.unidata.mdm.matching.core.context.MatchingExecutionContext;
import org.unidata.mdm.matching.core.dto.MatchingExecutionResult;
import org.unidata.mdm.matching.core.type.cluster.Cluster;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.execution.MatchingRealTimeInput;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;
import org.unidata.mdm.matching.storage.postgres.converter.ClusterConverter;
import org.unidata.mdm.matching.storage.postgres.converter.MatchingRecordKeyConverter;
import org.unidata.mdm.matching.storage.postgres.dao.PostgresMatchingExecutionDAO;
import org.unidata.mdm.matching.storage.postgres.po.ClusterPO;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordKeyPO;
import org.unidata.mdm.matching.storage.postgres.type.algorithm.PostgresMatchingAlgorithm;
import org.unidata.mdm.matching.storage.postgres.type.column.Column;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
@Component
public class PostgresMatchingExecutionComponent {
    /**
     * Matching execution DAO.
     */
    @Autowired
    private PostgresMatchingExecutionDAO postgresMatchingExecutionDAO;
    /**
     * Cluster component.
     */
    @Autowired
    private PostgresClusterComponent postgresClusterComponent;

    /**
     * Execute matching.
     *
     * @param ctx matching execution context
     * @return matching execution result
     */
    public MatchingExecutionResult execute(MatchingExecutionContext ctx) {

        if (Objects.nonNull(ctx.getRealTimeInput())) {
            return executeRealTimeMatching(ctx);
        } else {
            return executeStandardMatching(ctx);
        }
    }

    private MatchingExecutionResult executeStandardMatching(MatchingExecutionContext ctx) {
        Date matchingDate = Objects.nonNull(ctx.getMatchingDate())
                ? ctx.getMatchingDate()
                : new Date();

        MatchingExecutionResult result = new MatchingExecutionResult();

        MatchingRuleMappingElement ruleMapping = ctx.getRuleMapping();
        MatchingRuleSetElement ruleSet = ctx.getSet();

        // 1. Compute grouping columns for matching execution.
        List<Column> groupingColumns = computeGroupingColumns(ruleMapping);

        // 2. Execute matching.
        List<ClusterPO> clusterPOs = postgresMatchingExecutionDAO.execute(ruleSet.getMatchingTable().getName(),
                groupingColumns);

        // 3. Calculate results.
        if (!ctx.isSkipResultCalculation()) {
            result.setResult(clusterPOs.stream()
                    .map(cluster -> ClusterConverter.convert(ruleSet, ruleMapping.getRule(), matchingDate, cluster))
                    .collect(Collectors.toMap(cluster -> MatchingRecordKeyConverter
                            .clusterRecordToKey(cluster.getOwner()), Function.identity())));
        }

        // 4. Calculate cluster updates.
        if (!ctx.isSkipClusterUpdates()) {
            Collection<Cluster> clusters;

            if (ctx.isSkipResultCalculation()) {
                clusters = ClusterConverter.convert(ruleSet, ruleMapping.getRule(), matchingDate, clusterPOs);
            } else {
                clusters = result.getResult().values();
            }

            ClusterUpdateContext.ClusterUpdateContextBuilder updatesBuilder = ClusterUpdateContext.builder();

            clusters.forEach(cluster -> postgresClusterComponent.processUpdates(cluster, ctx.isSkipClusterUpdates(),
                    updatesBuilder));

            result.setClusterUpdate(updatesBuilder.build());
        }

        // 5. Return computed result.
        return result;
    }

    private MatchingExecutionResult executeRealTimeMatching(MatchingExecutionContext ctx) {
        Date matchingDate = Objects.nonNull(ctx.getMatchingDate())
                ? ctx.getMatchingDate()
                : new Date();

        MatchingExecutionResult result = new MatchingExecutionResult();

        MatchingRealTimeInput realTimeInput = ctx.getRealTimeInput();
        MatchingRuleMappingElement ruleMapping = ctx.getRuleMapping();
        MatchingRuleSetElement set = ctx.getSet();

        // 1. Compute grouping columns for matching execution.
        List<Column> groupingColumns = computeGroupingColumns(ruleMapping);

        // 2. Execute matching.
        List<ClusterPO> clusterPOs = postgresMatchingExecutionDAO.execute(set.getMatchingTable().getName(), groupingColumns,
                MatchingRecordKeyConverter.keyToPO(realTimeInput.getProcessedKeys()));

        // 3. Calculate results.
        if (!ctx.isSkipResultCalculation()) {
            result.setResult(realTimeInput.getProcessedKeys().stream()
                    .map(key -> mapCluster(key, clusterPOs))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toMap(
                            Pair::getKey,
                            p -> ClusterConverter.convert(set, ruleMapping.getRule(), matchingDate, p.getValue()))));
        }

        // 4. Calculate cluster updates.
        if (!ctx.isSkipClusterUpdates()) {
            Collection<Cluster> clusters;

            if (ctx.isSkipResultCalculation()) {
                clusters = ClusterConverter.convert(set, ruleMapping.getRule(), matchingDate, clusterPOs);
            } else {
                clusters = result.getResult().values();
            }

            ClusterUpdateContext.ClusterUpdateContextBuilder updatesBuilder = ClusterUpdateContext.builder();

            // 4.1. Exclude deleted records from cluster.
            postgresClusterComponent.excludeFromCluster(realTimeInput.getExcludedKeys(), set.getName(),
                    ruleMapping.getRule().getName(), updatesBuilder);

            // 4.2. Collect cluster updates.
            clusters.forEach(cluster -> postgresClusterComponent.processUpdates(cluster, ctx.isSkipClusterUpdates(),
                    updatesBuilder));

            result.setClusterUpdate(updatesBuilder.build());
        }

        // 5. Return computed result.
        return result;
    }

    private List<Column> computeGroupingColumns(MatchingRuleMappingElement ruleMapping) {

        List<Column> result = new ArrayList<>();

        ruleMapping.getMappings().forEach(mapping -> {
            PostgresMatchingAlgorithm algorithm = (PostgresMatchingAlgorithm) mapping.getAlgorithmSettings().getAlgorithm();
            Column column = algorithm.processColumn(mapping.getColumn(), mapping.getAlgorithmSettings());

            result.add(column);
        });

        return result;
    }

    private Pair<MatchingRecordKey, ClusterPO> mapCluster(MatchingRecordKey key, List<ClusterPO> clusters) {
        MatchingRecordKeyPO keyPO = MatchingRecordKeyConverter.keyToPO(key);

        Optional<ClusterPO> cluster = clusters.stream()
                .filter(c -> c.getRecordKeys().contains(keyPO))
                .findFirst();

        return cluster.map(clusterPO -> Pair.of(key, clusterPO)).orElse(null);
    }
}
