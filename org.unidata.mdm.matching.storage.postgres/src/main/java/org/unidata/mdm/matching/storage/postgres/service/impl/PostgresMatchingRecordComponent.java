/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.context.MatchingUpdateContext;
import org.unidata.mdm.matching.core.type.model.instance.MatchingDeploymentElement;
import org.unidata.mdm.matching.storage.postgres.converter.MatchingRecordConverter;
import org.unidata.mdm.matching.storage.postgres.converter.MatchingRecordKeyConverter;
import org.unidata.mdm.matching.storage.postgres.dao.PostgresMatchingRecordDAO;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordKeyPO;
import org.unidata.mdm.matching.storage.postgres.po.MatchingRecordPO;
import org.unidata.mdm.matching.storage.postgres.type.column.Column;
import org.unidata.mdm.matching.storage.postgres.type.table.MatchingTableModel;
import org.unidata.mdm.matching.storage.postgres.util.MatchingTableModelUtils;

import java.util.List;
import java.util.Map;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
@Component
public class PostgresMatchingRecordComponent {

    /**
     * Matching record DAO.
     */
    @Autowired
    private PostgresMatchingRecordDAO postgresMatchingRecordDAO;

    /**
     * Updates matching records (insert, delete).
     *
     * @param ctx matching update context
     */
    public void update(MatchingUpdateContext ctx) {
        // 1. Process deletes.
        processDeletes(ctx);

        // 2. Process inserts.
        processInserts(ctx);
    }

    private void processDeletes(MatchingUpdateContext ctx) {

        if (!ctx.hasDeletes()) {
            return;
        }

        ctx.getDeletes().forEach((table, keys) -> {
            List<MatchingRecordKeyPO> pos = MatchingRecordKeyConverter.keyToPO(keys);

            postgresMatchingRecordDAO.delete(table, pos);
        });
    }

    private void processInserts(MatchingUpdateContext ctx) {

        if (!ctx.hasInserts()) {
            return;
        }

        Map<String, MatchingDeploymentElement> deployments = ctx.deployments();

        ctx.getInserts().forEach((table, records) -> {
            MatchingTableModel model = MatchingTableModelUtils.createModel(deployments.get(table));
            List<Column> columns = model.getColumns();

            List<MatchingRecordPO> pos = MatchingRecordConverter.recordToPO(columns, records);

            postgresMatchingRecordDAO.insert(model.getTableName(), columns, pos);
        });
    }
}
