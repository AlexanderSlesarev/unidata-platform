/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.type.column;

import org.unidata.mdm.matching.storage.postgres.type.algorithm.PreprocessingRule;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 17.08.2021
 */
public class Column {
    /**
     * Column name.
     */
    private String name;
    /**
     * Column alias.
     */
    private String alias;
    /**
     * Column type.
     */
    private ColumnType type;
    /**
     * Preprocessing rule.
     */
    private PreprocessingRule preprocessingRule;
    /**
     * Column order.
     */
    private int order;

    /**
     * Creates column.
     *
     * @param name              the column name
     * @param alias             the column alias
     * @param type              the column type
     * @param preprocessingRule preprocessing rule
     * @return column
     */
    public static Column of(String name, String alias, ColumnType type, PreprocessingRule preprocessingRule) {
        Column column = new Column();

        column.setName(name);
        column.setAlias(alias);
        column.setType(type);
        column.setPreprocessingRule(preprocessingRule);

        return column;
    }

    /**
     * Creates column.
     *
     * @param name              the column name
     * @param type              the column type
     * @param preprocessingRule preprocessing rule
     * @return column
     */
    public static Column of(String name, ColumnType type, PreprocessingRule preprocessingRule) {
        Column column = new Column();

        column.setName(name);
        column.setType(type);
        column.setPreprocessingRule(preprocessingRule);

        return column;
    }

    /**
     * Creates column.
     *
     * @param name              the column name
     * @param type              the column type
     * @return column
     */
    public static Column of(String name, ColumnType type) {
        Column column = new Column();

        column.setName(name);
        column.setType(type);

        return column;
    }

    /**
     * Gets column name.
     *
     * @return the column name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets column name.
     *
     * @param name the column name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets column alias.
     *
     * @return the column alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets column alias.
     *
     * @param alias the column alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * Gets column type.
     *
     * @return the column type
     */
    public ColumnType getType() {
        return type;
    }

    /**
     * Sets column type.
     *
     * @param type the column type to set
     */
    public void setType(ColumnType type) {
        this.type = type;
    }

    /**
     * Gets preprocessing rule.
     *
     * @return the preprocessing rule
     */
    public PreprocessingRule getPreprocessingRule() {
        return preprocessingRule;
    }

    /**
     * Sets preprocessing rule.
     *
     * @param preprocessingRule the preprocessing rule to set
     */
    public void setPreprocessingRule(PreprocessingRule preprocessingRule) {
        this.preprocessingRule = preprocessingRule;
    }

    /**
     * Has preprocessing rule.
     *
     * @return is column has preprocessing rule
     */
    public boolean hasPreprocessing() {
        return Objects.nonNull(preprocessingRule);
    }

    /**
     * Gets column order.
     *
     * @return column order
     */
    public int getOrder() {
        return order;
    }

    /**
     * Sets column order.
     *
     * @param order column order to set
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Column column = (Column) o;

        return Objects.equals(getName(), column.getName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
