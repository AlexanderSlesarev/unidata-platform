/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.context.MatchingExecutionContext;
import org.unidata.mdm.matching.core.context.MatchingUpdateContext;
import org.unidata.mdm.matching.core.dto.MatchingExecutionResult;
import org.unidata.mdm.matching.core.service.MatchingStorageService;
import org.unidata.mdm.matching.core.type.storage.MatchingStorage;
import org.unidata.mdm.matching.core.type.storage.MatchingStorageDescriptor;
import org.unidata.mdm.matching.storage.postgres.configuration.PostgresMatchingStorageDescriptors;
import org.unidata.mdm.system.service.AfterModuleStartup;

/**
 * @author Sergey Murskiy on 16.08.2021
 */
@Component
public class PostgresMatchingStorageImpl implements MatchingStorage, AfterModuleStartup {

    /**
     * Matching storage service.
     */
    @Autowired
    private MatchingStorageService matchingStorageService;
    /**
     * Matching execution component.
     */
    @Autowired
    private PostgresMatchingExecutionComponent postgresMatchingExecutionComponent;
    /**
     * Matching record component.
     */
    @Autowired
    private PostgresMatchingRecordComponent postgresMatchingRecordComponent;
    /**
     * Matching table component.
     */
    @Autowired
    private PostgresMatchingTableComponent postgresMatchingTableComponent;

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingStorageDescriptor descriptor() {
        return PostgresMatchingStorageDescriptors.MATCHING_STORAGE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(MatchingUpdateContext ctx) {
        postgresMatchingRecordComponent.update(ctx);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingExecutionResult match(MatchingExecutionContext ctx) {
        return postgresMatchingExecutionComponent.execute(ctx);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterModuleStartup() {
        matchingStorageService.register(this);
    }
}
