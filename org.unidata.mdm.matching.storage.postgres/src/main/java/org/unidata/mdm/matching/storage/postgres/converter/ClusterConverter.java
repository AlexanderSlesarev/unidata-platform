/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.converter;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.lang.NonNull;
import org.unidata.mdm.matching.core.type.cluster.Cluster;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;
import org.unidata.mdm.matching.storage.postgres.po.ClusterPO;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 23.08.2021
 */
public class ClusterConverter {

    /**
     * Constructor.
     */
    private ClusterConverter() {
        super();
    }

    /**
     * Convert cluster PO to cluster model.
     *
     * @param set          matching set
     * @param rule         matching rule
     * @param matchingDate matching date
     * @param po           cluster PO
     * @return cluster model
     */
    public static Cluster convert(MatchingRuleSetElement set, MatchingRuleElement rule, Date matchingDate,
                                  @NonNull ClusterPO po) {
        Cluster cluster = new Cluster();
        cluster.setSetName(set.getName());
        cluster.setSetDisplayName(set.getDisplayName());
        cluster.setRuleName(rule.getName());
        cluster.setRuleDisplayName(rule.getDisplayName());
        cluster.setMatchingDate(matchingDate);
        cluster.setOwner(MatchingRecordKeyConverter.keyPOToClusterRecord(po.getRecordKeys().iterator().next(), 100));
        cluster.setClusterRecords(po.getRecordKeys().stream()
                .map(keyPO -> MatchingRecordKeyConverter.keyPOToClusterRecord(keyPO, 100))
                .collect(Collectors.toSet()));

        return cluster;
    }

    /**
     * Convert cluster PO to cluster model.
     *
     * @param set          matching set
     * @param rule         matching rule
     * @param matchingDate matching date
     * @param pos          cluster POs
     * @return cluster models
     */
    public static List<Cluster> convert(MatchingRuleSetElement set, MatchingRuleElement rule, Date matchingDate,
                                        List<ClusterPO> pos) {
        if (CollectionUtils.isEmpty(pos)) {
            return Collections.emptyList();
        }

        return pos.stream()
                .map(po -> convert(set, rule, matchingDate, po))
                .collect(Collectors.toList());
    }
}
