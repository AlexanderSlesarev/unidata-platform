/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.service.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.context.ClusterSearchContext;
import org.unidata.mdm.matching.core.context.ClusterUpdateContext.ClusterUpdateContextBuilder;
import org.unidata.mdm.matching.core.dto.ClusterGetResult;
import org.unidata.mdm.matching.core.service.ClusterService;
import org.unidata.mdm.matching.core.type.cluster.Cluster;
import org.unidata.mdm.matching.core.type.cluster.ClusterRecord;
import org.unidata.mdm.matching.core.type.cluster.ClusterSearchQuery;
import org.unidata.mdm.matching.core.type.cluster.ClusterSearchQuery.ClusterRecordSearchQuery;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.storage.postgres.converter.MatchingRecordKeyConverter;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
@Component
public class PostgresClusterComponent {
    /**
     * Cluster search service.
     */
    @Autowired
    private ClusterService clusterService;

    /**
     * Process existing cluster updates for fresh cluster.
     *
     * @param freshCluster      the new cluster.
     * @param skipExistingCheck is skip existing clusters check
     * @param updatesBuilder    cluster updates builder
     */
    public void processUpdates(Cluster freshCluster, boolean skipExistingCheck,
                               ClusterUpdateContextBuilder updatesBuilder) {

        List<Cluster> existingClusters;

        if (skipExistingCheck) {
            existingClusters = Collections.emptyList();
        } else {
            ClusterSearchQuery query = new ClusterSearchQuery()
                    .withSetNames(freshCluster.getSetName())
                    .withRuleNames(freshCluster.getRuleName())
                    .withRecordsQueries(freshCluster.getClusterRecords().stream()
                            .map(r -> new ClusterRecordSearchQuery()
                                    .withNamespace(r.getNamespace())
                                    .withTypeName(r.getTypeName())
                                    .withSubjectId(r.getSubjectId())
                                    .withValidFrom(r.getValidFrom())
                                    .withValidTo(r.getValidTo()))
                            .collect(Collectors.toList()));

            ClusterGetResult searchResult = clusterService.search(ClusterSearchContext.builder()
                    .query(query)
                    .fetchClusterRecords(true)
                    .build());

            existingClusters = searchResult.getClusters();
        }

        collectUpdates(freshCluster, existingClusters, updatesBuilder);
    }

    /**
     * Exclude cluster records from cluster by matching record keys.
     *
     * @param keys           matching records keys
     * @param setName        the matching set name
     * @param ruleName       the matching rule name
     * @param updatesBuilder cluster updates builder
     */
    public void excludeFromCluster(List<MatchingRecordKey> keys, String setName, String ruleName,
                                   ClusterUpdateContextBuilder updatesBuilder) {

        keys.forEach(key -> excludeFromCluster(key, setName, ruleName, updatesBuilder));
    }

    private void collectUpdates(Cluster freshCluster, List<Cluster> existingClusters,
                                ClusterUpdateContextBuilder updatesBuilder) {

        if (existingClusters.stream()
                .anyMatch(clusterPO -> clusterPO.getClusterRecords().containsAll(freshCluster.getClusterRecords()))) {
            return;
        }

        for (Cluster existCluster : existingClusters) {
            Collection<ClusterRecord> forExclude = CollectionUtils.retainAll(existCluster.getClusterRecords(),
                    freshCluster.getClusterRecords());

            if (!forExclude.isEmpty()) {
                if (existCluster.getClusterRecords().size() - forExclude.size() < 2) {
                    updatesBuilder.clusterDelete(existCluster.getId());
                } else {
                    if (forExclude.contains(existCluster.getOwner())
                            || !existCluster.getClusterRecords().contains(existCluster.getOwner())) {

                        existCluster.getClusterRecords().stream()
                                .filter(clusterRecord -> !forExclude.contains(clusterRecord))
                                .findFirst()
                                .ifPresent(freshOwner ->
                                        updatesBuilder.clusterUpdate(updateOwner(existCluster, freshOwner)));
                    }

                    updatesBuilder.clusterRecordDelete(existCluster.getId(), forExclude);
                }
            }
        }

        updatesBuilder.clusterInsert(freshCluster);
    }

    private void excludeFromCluster(MatchingRecordKey key, String setName, String ruleName,
                                    ClusterUpdateContextBuilder updatesBuilder) {

        ClusterRecord forExclude = MatchingRecordKeyConverter.keyToClusterRecord(key);

        ClusterSearchQuery query = new ClusterSearchQuery()
                .withSetNames(setName)
                .withRuleNames(ruleName)
                .withRecordsQueries(new ClusterRecordSearchQuery()
                        .withNamespace(key.getNamespace())
                        .withTypeName(key.getTypeName())
                        .withSubjectId(key.getSubjectId())
                        .withValidFrom(key.getValidFrom())
                        .withValidTo(key.getValidTo()));

        ClusterGetResult clusterSearchResult = clusterService.search(ClusterSearchContext.builder()
                .query(query)
                .fetchClusterRecords(true)
                .build());

        if (clusterSearchResult.isEmpty()) {
            return;
        }

        for (Cluster existCluster : clusterSearchResult.getClusters()) {

            if (existCluster.getClusterRecords().size() <= 2) {
                updatesBuilder.clusterDelete(existCluster.getId());
            } else {
                Optional<ClusterRecord> freshOwner = Optional.empty();
                if (forExclude.equals(existCluster.getOwner())
                        || !existCluster.getClusterRecords().contains(existCluster.getOwner())) {

                    freshOwner = existCluster.getClusterRecords().stream()
                            .filter(clusterRecord -> !forExclude.equals(clusterRecord))
                            .findFirst();
                }

                ClusterRecord nextOwner = freshOwner.orElse(existCluster.getOwner());
                updatesBuilder.clusterRecordDelete(existCluster.getId(), forExclude);
                updatesBuilder.clusterUpdate(updateClusterAfterExclude(existCluster, nextOwner));
            }
        }
    }

    private Cluster updateOwner(Cluster existCluster, ClusterRecord freshOwner) {

        Cluster updatedCluster = new Cluster();

        updatedCluster.setId(existCluster.getId());
        updatedCluster.setSetName(existCluster.getSetName());
        updatedCluster.setSetDisplayName(existCluster.getSetDisplayName());
        updatedCluster.setRuleName(existCluster.getRuleName());
        updatedCluster.setRuleDisplayName(existCluster.getRuleDisplayName());
        updatedCluster.setMatchingDate(existCluster.getMatchingDate());
        updatedCluster.setOwner(freshOwner);
        updatedCluster.setAdditionalParameters(existCluster.getAdditionalParameters());

        return updatedCluster;
    }

    private Cluster updateClusterAfterExclude(Cluster existCluster, ClusterRecord freshOwner) {

        Cluster updatedCluster = new Cluster();

        updatedCluster.setId(existCluster.getId());
        updatedCluster.setSetName(existCluster.getSetName());
        updatedCluster.setSetDisplayName(existCluster.getSetDisplayName());
        updatedCluster.setRuleName(existCluster.getRuleName());
        updatedCluster.setRuleDisplayName(existCluster.getRuleDisplayName());
        updatedCluster.setMatchingDate(existCluster.getMatchingDate());
        updatedCluster.setOwner(freshOwner);
        updatedCluster.setAdditionalParameters(existCluster.getAdditionalParameters());
        updatedCluster.setRecordsCount(existCluster.getRecordsCount() - 1);

        return updatedCluster;
    }
}
