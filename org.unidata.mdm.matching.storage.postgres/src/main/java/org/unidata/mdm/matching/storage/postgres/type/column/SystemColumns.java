/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.type.column;

import java.util.List;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
public final class SystemColumns {
    /**
     * System columns.
     */
    public static final Column ID = Column.of("id", ColumnType.STRING);
    public static final Column NAMESPACE = Column.of("namespace", ColumnType.STRING);
    public static final Column TYPE_NAME = Column.of("type_name", ColumnType.STRING);
    public static final Column SUBJECT_ID = Column.of("subject_id", ColumnType.STRING);
    public static final Column VALID_FROM = Column.of("valid_from", ColumnType.LOCAL_DATE_TIME);
    public static final Column VALID_TO = Column.of("valid_to", ColumnType.LOCAL_DATE_TIME);

    /**
     * Constructor.
     */
    private SystemColumns() {
        super();
    }

    /**
     * Gets system columns.
     *
     * @return columns
     */
    public static List<Column> getColumns() {
        return List.of(ID, NAMESPACE, TYPE_NAME, SUBJECT_ID, VALID_FROM, VALID_TO);
    }
}
