/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.storage.postgres.service.impl.refresh;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.context.ModelRefreshContext;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.matching.core.configuration.MatchingDescriptors;
import org.unidata.mdm.matching.core.service.impl.refresh.AbstractMatchingModelRefreshListener;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmSettingsElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingColumnElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingDeploymentElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.matching.storage.postgres.configuration.PostgresMatchingStorageConfigurationConstants;
import org.unidata.mdm.matching.storage.postgres.service.impl.PostgresMatchingTableComponent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.08.2021
 */
@Component
public class PostgresMatchingModelRefreshListener extends AbstractMatchingModelRefreshListener {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PostgresMatchingModelRefreshListener.class);

    /**
     * Matching table component.
     */
    @Autowired
    private PostgresMatchingTableComponent postgresMatchingTableComponent;

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh(ModelRefreshContext refresh) {
        try {
            String storageId = SecurityUtils.getStorageId(refresh);
            MatchingModelInstance fresh = metaModelService.instance(MatchingDescriptors.MATCHING, storageId, null);

            int revision = fresh.getVersion();
            boolean isNew = revision == 1;

            Map<String, MatchingDeploymentElement> freshDeployments =
                    fresh.getStorageDeployments(PostgresMatchingStorageConfigurationConstants.MATCHING_STORAGE_NAME);

            List<String> deletes = new ArrayList<>();
            List<MatchingDeploymentElement> creates = new ArrayList<>();

            if (isNew) {
                creates.addAll(new ArrayList<>(freshDeployments.values()));
            } else {
                MatchingModelInstance p = getPreviousState(refresh);
                Map<String, MatchingDeploymentElement> previousDeployments =
                        p.getStorageDeployments(PostgresMatchingStorageConfigurationConstants.MATCHING_STORAGE_NAME);

                // 1. Process matching tables updates (update, delete).
                previousDeployments.forEach((tableName, previousDeployment) ->
                        processTableUpdates(previousDeployment, freshDeployments.get(tableName), creates, deletes));

                // 2. Process matching tables creates.
                freshDeployments.forEach((tableName, freshDeployment) ->
                        processTableCreates(previousDeployments.get(tableName), freshDeployment, creates));
            }

            deletes.forEach(tableName -> postgresMatchingTableComponent.dropTable(tableName));
            creates.forEach(deployment -> postgresMatchingTableComponent.createTable(deployment));
        } catch (Exception e) {
            final String message = "Error while refreshing matching model.";
            LOGGER.error(message, e);
        }
    }

    private void processTableUpdates(MatchingDeploymentElement previousDeployment,
                                     MatchingDeploymentElement freshDeployment,
                                     List<MatchingDeploymentElement> creates, List<String> deletes) {

        if (Objects.isNull(freshDeployment)) {
            deletes.add(previousDeployment.getMatchingTableName());
            return;
        }

        Map<MatchingColumnElement, List<AlgorithmSettingsElement>> previousAssignments =
                previousDeployment.getColumnAssignments();
        Map<MatchingColumnElement, List<AlgorithmSettingsElement>> freshColumnAssignments =
                freshDeployment.getColumnAssignments();

        for (Map.Entry<MatchingColumnElement, List<AlgorithmSettingsElement>> entry : previousAssignments.entrySet()) {
            MatchingColumnElement column = entry.getKey();
            List<AlgorithmSettingsElement> previousAssignment = entry.getValue();
            List<AlgorithmSettingsElement> freshAssignment = freshColumnAssignments.getOrDefault(column, Collections.emptyList());

            if (!CollectionUtils.isEqualCollection(previousAssignment, freshAssignment)) {
                deletes.add(freshDeployment.getMatchingTableName());
                creates.add(freshDeployment);
                return;
            }
        }

        boolean isNewAssignment = freshColumnAssignments.keySet().stream()
                .map(previousAssignments::get)
                .anyMatch(CollectionUtils::isEmpty);

        if (isNewAssignment) {
            deletes.add(freshDeployment.getMatchingTableName());
            creates.add(freshDeployment);
        }
    }

    private void processTableCreates(MatchingDeploymentElement previousDeployment,
                                     MatchingDeploymentElement freshDeployment,
                                     List<MatchingDeploymentElement> creates) {

        if (Objects.isNull(previousDeployment)) {
            creates.add(freshDeployment);
        }
    }
}
