package org.unidata.mdm.rest.v1.data.ro.favorite;

import java.util.List;
import java.util.UUID;

/**
 * Delete favorite etalons request
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class DeleteFavoriteRequestRO {

    private String entityName;
    private List<UUID> ids;

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public List<UUID> getIds() {
        return ids;
    }

    public void setIds(List<UUID> ids) {
        this.ids = ids;
    }
}
