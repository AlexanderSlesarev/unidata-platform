package org.unidata.mdm.rest.v1.data.ro.merge;

import java.util.List;

import org.unidata.mdm.rest.v1.data.ro.WinnerRO;

/**
 * Merge entities request
 *
 * @author Alexandr Serov
 * @since 16.10.2020
 **/
public class MergeRequestRO {


    /** The entity name. */
    private String entityName;
    /** The winner etalon id. */
    private String winnerEtalonId;
    /** The etalon ids. */
    private List<String> etalonIds;
    /** The winners. */
    private List<WinnerRO> winners;

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getWinnerEtalonId() {
        return winnerEtalonId;
    }

    public void setWinnerEtalonId(String winnerEtalonId) {
        this.winnerEtalonId = winnerEtalonId;
    }

    public List<String> getEtalonIds() {
        return etalonIds;
    }

    public void setEtalonIds(List<String> etalonIds) {
        this.etalonIds = etalonIds;
    }

    public List<WinnerRO> getWinners() {
        return winners;
    }

    public void setWinners(List<WinnerRO> winners) {
        this.winners = winners;
    }
}
