package org.unidata.mdm.rest.v1.data.ro.relations;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.data.ro.records.EtalonRelationToRO;

/**
 * Upsert relation result
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class UpsertRelationsResultRO extends DetailedOutputRO {

    /**
     * Etalon id of the RELATION.
     */
    private String etalonId;
    /**
     * Containment etalon record.
     */
    private List<EtalonRelationToRO> etalonRelations;

    public String getEtalonId() {
        return etalonId;
    }

    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }

    public List<EtalonRelationToRO> getEtalonRelations() {
        return etalonRelations;
    }

    public void setEtalonRelations(List<EtalonRelationToRO> etalonRelations) {
        this.etalonRelations = etalonRelations;
    }
}
