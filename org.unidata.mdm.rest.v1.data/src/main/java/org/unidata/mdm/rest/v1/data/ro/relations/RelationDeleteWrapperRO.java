/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro.relations;


import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

/**
 * Wrapper object to delete context
 *
 * @author Dmitry Kopin on 19.06.2018.
 */
public class RelationDeleteWrapperRO {
    /**
     * DID.
     */
    private Long draftId;
    /**
     * Etalon id relation
     */
    @JsonProperty("etalonId")
    private String etalonRelationId;
    /**
     * Origin id relation
     */
    @JsonProperty("originId")
    private String originRelationId;
    /**
     * Valid from period
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime validFrom;
    /**
     * Valid to period
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime validTo;

    private boolean inactivateEtalon;

    private boolean inactivateOrigin;

    private boolean inactivatePeriod;

    /**
     * Wipe flag
     */
    private boolean wipe = false;
    /**
     * Relation name
     */
    private String relName;

    /**
     * @return the draftId
     */
    public Long getDraftId() {
        return draftId;
    }

    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

    public String getEtalonRelationId() {
        return etalonRelationId;
    }

    public void setEtalonRelationId(String etalonRelationId) {
        this.etalonRelationId = etalonRelationId;
    }

    public String getOriginRelationId() {
        return originRelationId;
    }

    public void setOriginRelationId(String originRelationId) {
        this.originRelationId = originRelationId;
    }


    public boolean isWipe() {
        return wipe;
    }

    public void setWipe(boolean wipe) {
        this.wipe = wipe;
    }

    public String getRelName() {
        return relName;
    }

    public void setRelName(String relName) {
        this.relName = relName;
    }

    /**
     * Optional validity range start date.
     */
    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * Optional validity range end date.
     */
    public LocalDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }

    /**
     * @return the inactivateEtalon
     */
    public boolean isInactivateEtalon() {
        return inactivateEtalon;
    }

    /**
     * @param inactivateEtalon the inactivateEtalon to set
     */
    public void setInactivateEtalon(boolean inactivateEtalon) {
        this.inactivateEtalon = inactivateEtalon;
    }

    /**
     * @return the inactivateOrigin
     */
    public boolean isInactivateOrigin() {
        return inactivateOrigin;
    }

    /**
     * @param inactivateOrigin the inactivateOrigin to set
     */
    public void setInactivateOrigin(boolean inactivateOrigin) {
        this.inactivateOrigin = inactivateOrigin;
    }

    /**
     * @return the inactivatePeriod
     */
    public boolean isInactivatePeriod() {
        return inactivatePeriod;
    }

    /**
     * @param inactivatePeriod the inactivatePeriod to set
     */
    public void setInactivatePeriod(boolean inactivatePeriod) {
        this.inactivatePeriod = inactivatePeriod;
    }
}
