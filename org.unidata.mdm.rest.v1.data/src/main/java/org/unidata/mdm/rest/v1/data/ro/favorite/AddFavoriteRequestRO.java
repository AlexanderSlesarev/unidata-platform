package org.unidata.mdm.rest.v1.data.ro.favorite;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Add favorite etalons request
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class AddFavoriteRequestRO  {

    private Map<String, List<UUID>> favorites;

    public Map<String, List<UUID>> getFavorites() {
        return favorites;
    }

    public void setFavorites(Map<String, List<UUID>> favorites) {
        this.favorites = favorites;
    }

}
