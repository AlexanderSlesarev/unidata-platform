package org.unidata.mdm.rest.v1.data.ro.merge;

import java.util.List;

import org.unidata.mdm.rest.v1.data.ro.WinnerRO;
import org.unidata.mdm.rest.v1.data.ro.records.EtalonRecordRO;

/**
 * Merge preview result
 *
 * @author Alexandr Serov
 * @since 16.10.2020
 **/
public class MergePreviewRO {

    /**
     * Winner etalon id.
     */
    private String winnerEtalonId;

    /** The data record. */
    private EtalonRecordRO dataRecord;

    /** The winners. */
    private List<WinnerRO> winners;

    /** The new data record. */
    private EtalonRecordRO manualDataRecord;

    /** The winners. */
    private List<WinnerRO> manualWinners;

    public String getWinnerEtalonId() {
        return winnerEtalonId;
    }

    public void setWinnerEtalonId(String winnerEtalonId) {
        this.winnerEtalonId = winnerEtalonId;
    }

    public EtalonRecordRO getDataRecord() {
        return dataRecord;
    }

    public void setDataRecord(EtalonRecordRO dataRecord) {
        this.dataRecord = dataRecord;
    }

    public List<WinnerRO> getWinners() {
        return winners;
    }

    public void setWinners(List<WinnerRO> winners) {
        this.winners = winners;
    }

    public EtalonRecordRO getManualDataRecord() {
        return manualDataRecord;
    }

    public void setManualDataRecord(EtalonRecordRO manualDataRecord) {
        this.manualDataRecord = manualDataRecord;
    }

    public List<WinnerRO> getManualWinners() {
        return manualWinners;
    }

    public void setManualWinners(List<WinnerRO> manualWinners) {
        this.manualWinners = manualWinners;
    }
}
