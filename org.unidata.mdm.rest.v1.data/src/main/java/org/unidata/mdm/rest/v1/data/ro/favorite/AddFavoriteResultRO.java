package org.unidata.mdm.rest.v1.data.ro.favorite;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Add favorite etalons result
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class AddFavoriteResultRO extends DetailedOutputRO {

    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
