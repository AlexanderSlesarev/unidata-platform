/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.converter;

import java.util.Objects;

import org.unidata.mdm.data.type.keys.RecordEtalonKey;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.rest.v1.data.ro.keys.LsnRO;
import org.unidata.mdm.rest.v1.data.ro.keys.RecordKeysRO;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov
 *
 */
public class RecordKeysConverter {
    /**
     * EKC.
     */
    private static final EtalonKeyConverter ETALON_KEY_CONVERTER = new EtalonKeyConverter();
    /**
     * Constructor.
     */
    private RecordKeysConverter() {
        super();
    }

    public static RecordKeysRO to(RecordKeys keys) {

        if (Objects.isNull(keys)) {
            return null;
        }

        RecordEtalonKey ek = keys.getEtalonKey();

        RecordKeysRO result = new RecordKeysRO();
        result.setEtalonKey(ETALON_KEY_CONVERTER.to(ek));
        result.setOriginKeys(OriginKeyConverter.to(keys.getSupplementaryKeys()));
        result.setEntityName(keys.getEntityName());
        result.setNode(keys.getNode());
        result.setLsn(new LsnRO(ek.getLsn(), keys.getShard()));
        result.setCreateDate(ConvertUtils.date2OffsetDateTime(keys.getCreateDate()));
        result.setUpdateDate(ConvertUtils.date2OffsetDateTime(keys.getUpdateDate()));
        result.setCreatedBy(keys.getCreatedBy());
        result.setUpdatedBy(keys.getUpdatedBy());
        result.setActive(keys.isActive());
        result.setPublished(!keys.isNew());

        return result;
    }
}
