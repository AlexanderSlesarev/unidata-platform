package org.unidata.mdm.rest.v1.data.ro.records;

import java.time.LocalDateTime;


/**
 * Get entity
 *
 * @author Alexandr Serov
 * @since 09.10.2020
 **/
public class GetRecordRequestRO extends AbstractRecordRequestRO {

    private String operationId;
    private LocalDateTime timelineDate;
    private boolean includeInactive;
    private boolean diffToPrevious;
    private boolean diffToDraft;
    private Long draftId;

    public GetRecordRequestRO() {}

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public LocalDateTime getTimelineDate() {
        return timelineDate;
    }

    public void setTimelineDate(LocalDateTime timelineDate) {
        this.timelineDate = timelineDate;
    }

    public boolean isIncludeInactive() {
        return includeInactive;
    }

    public void setIncludeInactive(boolean includeInactive) {
        this.includeInactive = includeInactive;
    }

    public boolean isDiffToPrevious() {
        return diffToPrevious;
    }

    public void setDiffToPrevious(boolean diffToPrevious) {
        this.diffToPrevious = diffToPrevious;
    }

    public boolean isDiffToDraft() {
        return diffToDraft;
    }

    public void setDiffToDraft(boolean diffToDraft) {
        this.diffToDraft = diffToDraft;
    }

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}
