package org.unidata.mdm.rest.v1.data.ro.search;

public enum SearchResultProcessingElements {
    LOOKUP, MEASURED, ENUM, DATE
}