package org.unidata.mdm.rest.v1.data.ro.records;


/**
 * The result of deleting an entity
 *
 * @author Alexandr Serov
 * @since 12.10.2020
 **/
public class DeleteRecordResultRO extends RecordKeysSupportResultRO {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
