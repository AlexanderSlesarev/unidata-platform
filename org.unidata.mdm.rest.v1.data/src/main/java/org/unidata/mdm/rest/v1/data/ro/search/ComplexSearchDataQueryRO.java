package org.unidata.mdm.rest.v1.data.ro.search;

import java.util.List;

import io.swagger.v3.oas.annotations.Parameter;

/**
 * Complex data query
 *
 * @author Alexandr Serov
 * @since 05.12.2020
 **/
public class ComplexSearchDataQueryRO extends SimpleSearchDataQueryRO {

    @Parameter(description = "Supplementary query")
    private List<SimpleSearchDataQueryRO> supplementary;


    public List<SimpleSearchDataQueryRO> getSupplementary() {
        return supplementary;
    }

    public void setSupplementary(List<SimpleSearchDataQueryRO> supplementary) {
        this.supplementary = supplementary;
    }
}
