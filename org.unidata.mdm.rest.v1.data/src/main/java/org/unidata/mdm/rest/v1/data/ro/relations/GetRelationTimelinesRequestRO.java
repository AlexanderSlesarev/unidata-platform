package org.unidata.mdm.rest.v1.data.ro.relations;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Get relation timeline request
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class GetRelationTimelinesRequestRO {

    private Long parentDraftId;
    private String etalonId;
    private LocalDateTime dateFor;
    private LocalDateTime validFrom;
    private LocalDateTime validTo;
    private List<String> relationNames;
    private boolean includeInactive;

    /**
     * @return the parentDraftId
     */
    public Long getParentDraftId() {
        return parentDraftId;
    }

    /**
     * @param parentDraftId the parentDraftId to set
     */
    public void setParentDraftId(Long parentDraftId) {
        this.parentDraftId = parentDraftId;
    }

    public String getEtalonId() {
        return etalonId;
    }

    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }

    public List<String> getRelationNames() {
        return relationNames;
    }

    public void setRelationNames(List<String> relationNames) {
        this.relationNames = relationNames;
    }

    public LocalDateTime getDateFor() {
        return dateFor;
    }

    public void setDateFor(LocalDateTime dateFor) {
        this.dateFor = dateFor;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }

    /**
     * @return the includeInactive
     */
    public boolean isIncludeInactive() {
        return includeInactive;
    }

    /**
     * @param includeInactive the includeInactive to set
     */
    public void setIncludeInactive(boolean includeInactive) {
        this.includeInactive = includeInactive;
    }
}
