package org.unidata.mdm.rest.v1.data.ro.favorite;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Delete favorite etalons result
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class DeleteFavoriteResultRO extends DetailedOutputRO {

    private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
