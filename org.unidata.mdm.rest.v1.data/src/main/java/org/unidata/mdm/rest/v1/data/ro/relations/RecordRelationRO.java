package org.unidata.mdm.rest.v1.data.ro.relations;

import org.unidata.mdm.rest.v1.data.ro.records.DataRecordRO;

/**
 * Record relation rest object
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class RecordRelationRO {

    private String relationName;
    private String relationEtalonKey;
//    private EtalonRecordRO etalonRecord;
    private DataRecordRO record;

    /**
     * Data record
     */
    private Long draftId;

    public String getRelationEtalonKey() {
        return relationEtalonKey;
    }

    public void setRelationEtalonKey(String relationEtalonKey) {
        this.relationEtalonKey = relationEtalonKey;
    }

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    public DataRecordRO getRecord() {
        return record;
    }

    public void setRecord(DataRecordRO record) {
        this.record = record;
    }

    /**
     * @return the draftId
     */
    public Long getDraftId() {
        return draftId;
    }

    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

    //    public EtalonRecordRO getEtalonRecord() {
//        return etalonRecord;
//    }
//
//    public void setEtalonRecord(EtalonRecordRO etalonRecord) {
//        this.etalonRecord = etalonRecord;
//    }
}
