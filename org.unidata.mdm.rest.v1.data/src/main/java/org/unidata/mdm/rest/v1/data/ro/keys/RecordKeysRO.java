/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro.keys;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov
 * Keys RO.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecordKeysRO {
    /**
     * Etalon id.
     */
    private RecordEtalonKeyRO etalonKey;
    /**
     * Origin keys.
     */
    private List<RecordOriginKeyRO> originKeys;
    /**
     * Entity name.
     */
    private String entityName;
    /**
     * LSN.
     */
    private LsnRO lsn;
    /**
     * Node number.
     */
    private int node;
    /**
     * Record create date.
     */
    private OffsetDateTime createDate;
    /**
     * Record update date.
     */
    private OffsetDateTime updateDate;
    /**
     * Created by tag.
     */
    private String createdBy;
    /**
     * Updated by tag.
     */
    private String updatedBy;
    /**
     * Active or not (same as etalonKey.status == 'ACTIVE').
     */
    private boolean active;
    /**
     * Published (same as originKeys[*].revision > 0)
     */
    private boolean published;
    /**
     * Constructor.
     */
    public RecordKeysRO() {
        super();
    }
    /**
     * @return the etalonId
     */
    public RecordEtalonKeyRO getEtalonKey() {
        return etalonKey;
    }
    /**
     * @param etalonId the etalonId to set
     */
    public void setEtalonKey(RecordEtalonKeyRO etalonId) {
        this.etalonKey = etalonId;
    }
    /**
     * @return the originKeys
     */
    public List<RecordOriginKeyRO> getOriginKeys() {
        return Objects.isNull(originKeys) ? Collections.emptyList() : originKeys;
    }
    /**
     * @param originKeys the originKeys to set
     */
    public void setOriginKeys(List<RecordOriginKeyRO> originKeys) {
        this.originKeys = originKeys;
    }
    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }
    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
    /**
     * @return the lsn
     */
    public LsnRO getLsn() {
        return lsn;
    }
    /**
     * @param lsn the lsn to set
     */
    public void setLsn(LsnRO lsn) {
        this.lsn = lsn;
    }
    /**
     * @return the node
     */
    public int getNode() {
        return node;
    }
    /**
     * @param node the node to set
     */
    public void setNode(int node) {
        this.node = node;
    }
    /**
     * @return the createDate
     */
    public OffsetDateTime getCreateDate() {
        return createDate;
    }
    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(OffsetDateTime createDate) {
        this.createDate = createDate;
    }
    /**
     * @return the updateDate
     */
    public OffsetDateTime getUpdateDate() {
        return updateDate;
    }
    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(OffsetDateTime updateDate) {
        this.updateDate = updateDate;
    }
    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    /**
     * @return the updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }
    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }
    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }
    /**
     * @return the published
     */
    public boolean isPublished() {
        return published;
    }
    /**
     * @param published the published to set
     */
    public void setPublished(boolean published) {
        this.published = published;
    }
}
