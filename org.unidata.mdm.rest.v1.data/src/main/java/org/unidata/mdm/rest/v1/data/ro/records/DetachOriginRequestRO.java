package org.unidata.mdm.rest.v1.data.ro.records;

import org.unidata.mdm.rest.v1.data.ro.keys.LsnRO;
import org.unidata.mdm.rest.v1.data.ro.keys.RecordExternalIdRO;

/**
 * Detach origin request
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class DetachOriginRequestRO {

    private String etalonId;
    private LsnRO lsn;
    private String entityName;
    private RecordExternalIdRO originKey;

    public LsnRO getLsn() {
        return lsn;
    }

    public void setLsn(LsnRO lsn) {
        this.lsn = lsn;
    }

    public RecordExternalIdRO getOriginKey() {
        return originKey;
    }

    public void setOriginKey(RecordExternalIdRO recordKey) {
        this.originKey = recordKey;
    }

    /**
     * @return the etalonId
     */
    public String getEtalonId() {
        return etalonId;
    }

    /**
     * @param etalonId the etalonId to set
     */
    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
}
