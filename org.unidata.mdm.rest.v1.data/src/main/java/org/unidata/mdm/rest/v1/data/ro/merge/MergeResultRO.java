package org.unidata.mdm.rest.v1.data.ro.merge;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Merge etalons result
 *
 * @author Alexandr Serov
 * @since 16.10.2020
 **/
public class MergeResultRO extends DetailedOutputRO {

    /** The etalon id. */
    private String etalonId;


    public String getEtalonId() {
        return etalonId;
    }

    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }
}
