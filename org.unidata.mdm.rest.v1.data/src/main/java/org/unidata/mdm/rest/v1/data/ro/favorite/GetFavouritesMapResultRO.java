/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.data.ro.favorite;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * @author Mikhail Mikhailov on Sep 20, 2021
 */
public class GetFavouritesMapResultRO extends DetailedOutputRO {

    private Map<String, List<UUID>> map;
    /**
     * Constructor.
     */
    public GetFavouritesMapResultRO() {
        super();
    }
    /**
     * Constructor.
     */
    public GetFavouritesMapResultRO(Map<String, List<UUID>> map) {
        this();
        this.map = map;
    }
    /**
     * @return the map
     */
    public Map<String, List<UUID>> getMap() {
        return map;
    }
    /**
     * @param map the map to set
     */
    public void setMap(Map<String, List<UUID>> map) {
        this.map = map;
    }
}
