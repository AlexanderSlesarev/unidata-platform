package org.unidata.mdm.rest.v1.data.ro.keys;


/**
 * ExternalId
 *
 * @author Alexandr Serov
 * @since 20.11.2020
 **/
public class RecordExternalIdRO {
    /**
     * External id.
     */
    private String externalId;
    /**
     * Source system.
     */
    private String sourceSystem;

    public RecordExternalIdRO() {}

    public RecordExternalIdRO(String externalId, String sourceSystem) {
        this.externalId = externalId;
        this.sourceSystem = sourceSystem;
    }

    public RecordExternalIdRO(String externalId) {
        this(externalId, null);
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }
}
