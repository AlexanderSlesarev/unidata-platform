package org.unidata.mdm.rest.v1.data.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.data.type.keys.RelationOriginKey;
import org.unidata.mdm.rest.v1.data.ro.relations.RelationKeyRO;
import org.unidata.mdm.rest.v1.data.ro.relations.RelationKeysRO;

/**
 * Relation keys object converter
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class RelationKeysConverter {

    public static RelationKeysRO to(RelationKeys keys) {
        RelationKeysRO result = null;
        if (keys != null) {
            result = new RelationKeysRO();
            result.setFromEntityName(keys.getFromEntityName());
            result.setToEntityName(keys.getToEntityName());
            List<RelationOriginKey> sourceKeys = keys.getSupplementaryKeysWithoutEnrichments();
            if (sourceKeys != null) {
                result.setRelationKeys(sourceKeys.stream()
                    .map(RelationKeysConverter::toRelationKeyRO)
                    .collect(Collectors.toList()));
            }
        }
        return result;
    }

    private static RelationKeyRO toRelationKeyRO(RelationOriginKey key) {
        RelationKeyRO result = new RelationKeyRO();
        result.setFrom(OriginKeyConverter.to(key.getFrom()));
        result.setTo(OriginKeyConverter.to(key.getTo()));
        return result;
    }

}
