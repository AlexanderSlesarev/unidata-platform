/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.springframework.util.CollectionUtils;
import org.unidata.mdm.data.type.keys.RecordOriginKey;
import org.unidata.mdm.rest.v1.data.ro.keys.RecordExternalIdRO;
import org.unidata.mdm.rest.v1.data.ro.keys.RecordOriginKeyRO;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov
 *
 */
public class OriginKeyConverter {

    /**
     * Constructor.
     */
    private OriginKeyConverter() {
        super();
    }

    public static RecordOriginKeyRO to(RecordOriginKey key) {

        if (Objects.isNull(key)) {
            return null;
        }

        RecordOriginKeyRO result = new RecordOriginKeyRO();
        result.setExternalId(new RecordExternalIdRO(key.getExternalId(), key.getSourceSystem()));
        result.setEnrichment(key.isEnrichment());
        result.setCreateDate(ConvertUtils.date2OffsetDateTime(key.getCreateDate()));
        result.setUpdateDate(ConvertUtils.date2OffsetDateTime(key.getUpdateDate()));
        result.setCreatedBy(key.getCreatedBy());
        result.setUpdatedBy(key.getUpdatedBy());
        result.setStatus(Objects.nonNull(key.getStatus()) ? key.getStatus().name() : null);
        result.setRevision(key.getRevision());

        return result;
    }

    public static List<RecordOriginKeyRO> to(List<RecordOriginKey> keys) {

        if (CollectionUtils.isEmpty(keys)) {
            return Collections.emptyList();
        }

        List<RecordOriginKeyRO> result = new ArrayList<>(keys.size());
        for (RecordOriginKey key : keys) {
            RecordOriginKeyRO ro = to(key);
            if (Objects.isNull(ro)) {
                continue;
            }

            result.add(ro);
        }

        return result;
    }
}
