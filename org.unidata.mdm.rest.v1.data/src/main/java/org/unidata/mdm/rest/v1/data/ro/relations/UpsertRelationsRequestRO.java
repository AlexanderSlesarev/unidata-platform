package org.unidata.mdm.rest.v1.data.ro.relations;

import java.util.List;

import org.unidata.mdm.rest.v1.data.ro.keys.RecordExternalIdRO;
import org.unidata.mdm.rest.v1.data.ro.keys.LsnRO;

/**
 * Upsert relations request
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class UpsertRelationsRequestRO {

    /**
     * Data record
     */
    private Long draftId;
    private String etalonId;
    private String entityName;
    private RecordExternalIdRO externalId;
    private LsnRO lsn;
    private List<RecordRelationRO> to;

    public List<RecordRelationRO> getTo() {
        return to;
    }

    public void setTo(List<RecordRelationRO> to) {
        this.to = to;
    }

    /**
     * @return the etalonId
     */
    public String getEtalonId() {
        return etalonId;
    }

    /**
     * @param etalonId the etalonId to set
     */
    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the externalId
     */
    public RecordExternalIdRO getExternalId() {
        return externalId;
    }

    /**
     * @param externalId the externalId to set
     */
    public void setExternalId(RecordExternalIdRO externalId) {
        this.externalId = externalId;
    }

    /**
     * @return the lsn
     */
    public LsnRO getLsn() {
        return lsn;
    }

    /**
     * @param lsn the lsn to set
     */
    public void setLsn(LsnRO lsn) {
        this.lsn = lsn;
    }

    /**
     * @return the draftId
     */
    public Long getDraftId() {
        return draftId;
    }

    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

}