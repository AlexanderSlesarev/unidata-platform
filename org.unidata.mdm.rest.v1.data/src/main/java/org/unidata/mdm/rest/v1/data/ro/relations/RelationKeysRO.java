package org.unidata.mdm.rest.v1.data.ro.relations;

import java.util.List;

/**
 * Relation keys rest object
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class RelationKeysRO {

    private String fromEntityName;
    private String toEntityName;
    private List<RelationKeyRO> relationKeys;

    public String getFromEntityName() {
        return fromEntityName;
    }

    public void setFromEntityName(String fromEntityName) {
        this.fromEntityName = fromEntityName;
    }

    public String getToEntityName() {
        return toEntityName;
    }

    public void setToEntityName(String toEntityName) {
        this.toEntityName = toEntityName;
    }

    public List<RelationKeyRO> getRelationKeys() {
        return relationKeys;
    }

    public void setRelationKeys(List<RelationKeyRO> relationKeys) {
        this.relationKeys = relationKeys;
    }
}
