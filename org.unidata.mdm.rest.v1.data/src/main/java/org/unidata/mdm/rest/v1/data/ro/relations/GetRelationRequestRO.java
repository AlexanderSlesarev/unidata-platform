package org.unidata.mdm.rest.v1.data.ro.relations;

import java.time.LocalDateTime;

/**
 * Get relation request
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class GetRelationRequestRO {

    private String etalonId;
    private String operationId;
    private String relationEtalonKey;
    private LocalDateTime forDate;
    private Long draftId;

    public String getEtalonId() {
        return etalonId;
    }

    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getRelationEtalonKey() {
        return relationEtalonKey;
    }

    public void setRelationEtalonKey(String relationEtalonKey) {
        this.relationEtalonKey = relationEtalonKey;
    }

    public LocalDateTime getForDate() {
        return forDate;
    }

    public void setForDate(LocalDateTime forDate) {
        this.forDate = forDate;
    }

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}
