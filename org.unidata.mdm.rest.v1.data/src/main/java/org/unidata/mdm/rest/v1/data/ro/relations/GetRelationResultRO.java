package org.unidata.mdm.rest.v1.data.ro.relations;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.data.ro.records.EtalonRelationToRO;

/**
 * Get relation result
 *
 * @author Alexandr Serov
 * @since 23.10.2020
 **/
public class GetRelationResultRO extends DetailedOutputRO {

    private EtalonRelationToRO etalonRelation;
    private RelationKeysRO relationKeys;

    public EtalonRelationToRO getEtalonRelation() {
        return etalonRelation;
    }

    public void setEtalonRelation(EtalonRelationToRO etalonRelation) {
        this.etalonRelation = etalonRelation;
    }

    public void setRelationKeys(RelationKeysRO relationKeys) {
        this.relationKeys = relationKeys;
    }

    public RelationKeysRO getRelationKeys() {
        return relationKeys;
    }

}
