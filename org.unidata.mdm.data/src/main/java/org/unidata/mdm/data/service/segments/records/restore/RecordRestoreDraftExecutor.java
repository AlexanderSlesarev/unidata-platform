/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.records.restore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.data.context.RecordIdentityContextSupport;
import org.unidata.mdm.data.context.RestoreRecordRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.impl.RecordDraftProviderComponent;
import org.unidata.mdm.data.type.draft.DataDraftOperation;
import org.unidata.mdm.data.type.draft.DataDraftParameters;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov
 */
@Component(RecordRestoreDraftExecutor.SEGMENT_ID)
public class RecordRestoreDraftExecutor
        extends Point<RestoreRecordRequestContext>
        implements RecordIdentityContextSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_RESTORE_DRAFT]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.restore.draft.description";
    /**
     * The DS.
     */
    @Autowired
    private DraftService draftService;
    /**
     * The RDPC.
     */
    @Autowired
    private RecordDraftProviderComponent recordDraftProviderComponent;
    /**
     * Constructor.
     */
    public RecordRestoreDraftExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RestoreRecordRequestContext ctx) {

        MeasurementPoint.start();
        try {

            draftService.upsert(DraftUpsertContext.builder()
                    .payload(ctx)
                    .draftId(recordDraftProviderComponent.ensureDraftId(ctx))
                    .parameter(DataDraftParameters.DRAFT_OPERATION, ctx.isPeriodRestore()
                            ? DataDraftOperation.RESTORE_PERIOD
                            : DataDraftOperation.RESTORE_RECORD)
                    .build());

        } finally {
            MeasurementPoint.stop();
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RestoreRecordRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
