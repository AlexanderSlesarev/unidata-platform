/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.data.service.impl.load.xlsx;

import java.io.Serializable;
import java.util.Date;

/**
 * Key for the main displayable cache.
 */
public class DNKey implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The etalon id. */
    private String etalonId;

    /** The valid from. */
    private Date validFrom;
    /** The valid to. */
    private Date validTo;

    /** The entity name. */
    private String entityName;

    /**
     * Not a singleton and not supposed to be.
     *
     * @return new instance of the {@link DNKey}
     */
    public static DNKey newInstance() {
        return new DNKey();
    }

    /**
     * Gets the etalon id.
     *
     * @return the etalon id
     */
    public String getEtalonId() {
        return etalonId;
    }

    /**
     * Gets the valid from.
     *
     * @return the valid from
     */
    public Date getValidFrom() {
        return validFrom;
    }


    /**
     * Gets the valid to.
     *
     * @return the valid to
     */
    public Date getValidTo() {
        return validTo;
    }

    /**
     * Gets the entity name.
     *
     * @return the entity name
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * With etalon id.
     *
     * @param etalonId
     *            the etalon id
     * @return the DN key
     */
    public DNKey withEtalonId(String etalonId) {
        this.etalonId = etalonId;
        return this;
    }

    /**
     * With as valid from.
     *
     * @param validFrom
     *            the valid from
     * @return the DN key
     */
    public DNKey withValidFrom(Date validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    /**
     * With as valid to.
     *
     * @param validTo
     *            the valid to
     * @return the DN key
     */
    public DNKey withValidTo(Date validTo) {
        this.validTo = validTo;
        return this;
    }

    /**
     * With entity name.
     *
     * @param entityName
     *            the entity name
     * @return the DN key
     */
    public DNKey withEntityName(String entityName) {
        this.entityName = entityName;
        return this;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((validFrom == null) ? 0 : validFrom.hashCode());
        result = prime * result + ((validTo == null) ? 0 : validTo.hashCode());
        result = prime * result + ((entityName == null) ? 0 : entityName.hashCode());
        result = prime * result + ((etalonId == null) ? 0 : etalonId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DNKey other = (DNKey) obj;
        if (validFrom == null) {
            if (other.validFrom != null) {
                return false;
            }
        } else if (!validFrom.equals(other.validFrom)) {
            return false;
        }

        if (validTo == null) {
            if (other.validTo != null) {
                return false;
            }
        } else if (!validTo.equals(other.validTo)) {
            return false;
        }

        if (entityName == null) {
            if (other.entityName != null) {
                return false;
            }
        } else if (!entityName.equals(other.entityName)) {
            return false;
        }
        if (etalonId == null) {
            if (other.etalonId != null) {
                return false;
            }
        } else if (!etalonId.equals(other.etalonId)) {
            return false;
        }

        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DNKey [etalonId=").append(etalonId)
                .append(", validFrom=").append(validFrom)
                .append(", validTo=").append(validTo)
                .append(", entityName=").append(entityName)
                .append("]");
        return builder.toString();
    }
}
