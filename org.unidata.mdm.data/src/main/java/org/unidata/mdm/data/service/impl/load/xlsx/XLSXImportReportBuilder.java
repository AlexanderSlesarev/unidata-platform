/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.data.service.impl.load.xlsx;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.system.util.TextUtils;

/**
 * @author Dmitry Kopin on 13.02.2019.
 */
public class XLSXImportReportBuilder {

    private String recordName = null;
    private Long totalRecords = 0l;
    private Long insertedRecords = 0l;
    private Long updatedRecords = 0l;
    private Long failedRecords = 0l;
    private Long deletedRecords = 0l;

    private Map<String, Long> totalRelations = new HashMap<>();
    private Map<String, Long> insertedRelations = new HashMap<>();
    private Map<String, Long> updatedRelations = new HashMap<>();
    private Map<String, Long> deletedRelations = new HashMap<>();
    private Map<String, Long> failedRelations = new HashMap<>();

    private Map<String, Long> totalClassifiers = new HashMap<>();
    private Map<String, Long> insertedClassifiers = new HashMap<>();
    private Map<String, Long> updatedClassifiers = new HashMap<>();
    private Map<String, Long> deletedClassifiers = new HashMap<>();
    private Map<String, Long> failedClassifiers = new HashMap<>();

    private Map<String, Long> totalComplexAttribues = new HashMap<>();
    private Map<String, Long> insertedComplexAttribues = new HashMap<>();
    private Map<String, Long> skipComplexAttribues = new HashMap<>();

    private Map<String, String> relationNames = new HashMap<>();
    private Map<String, String> classifierNames = new HashMap<>();
    private Map<String, String> complexAttributeNames = new HashMap<>();

    public XLSXImportReportBuilder(String recordName) {
        this.recordName = recordName;
    }

    public void incrementInsertedRecords(long insertedRecords) {
        this.insertedRecords += insertedRecords;
    }

    public void incrementUpdatedRecords(long updatedRecords) {
        this.updatedRecords += updatedRecords;
    }

    public void incrementDeletedRecords(long deletedRecords) {
        this.deletedRecords += deletedRecords;
    }

    public void incrementTotalRecords(long totalRecords) {
        this.totalRecords += totalRecords;
    }

    public void incrementFailedRecords(long failedRecords) {
        this.failedRecords += failedRecords;
    }

    public void incrementInsertedRelations(String relationName, long insertedRelations) {
        this.insertedRelations.compute(relationName, (k, v) -> v == null ? insertedRelations : v + insertedRelations);
    }

    public void incrementUpdatedRelations(String relationName, long updatedRelations) {
        this.updatedRelations.compute(relationName, (k, v) -> v == null ? updatedRelations : v + updatedRelations);
    }

    public void incrementDeletedRelations(String relationName, long deletedRelations) {
        this.deletedRelations.compute(relationName, (k, v) -> v == null ? deletedRelations : v + deletedRelations);
    }

    public void incrementTotalRelations(String relationName, long totalRelations) {
        this.totalRelations.compute(relationName, (k, v) -> v == null ? totalRelations : v + totalRelations);
    }

    public void incrementFailedRelations(String relationName, long failedRelations) {
        this.failedRelations.compute(relationName, (k, v) -> v == null ? failedRelations : v + failedRelations);
    }

    public void incrementInsertedClassifiers(String classifierName, long insertedClassifiers) {
        this.insertedClassifiers.compute(classifierName, (k, v) -> v == null ? insertedClassifiers : v + insertedClassifiers);
    }

    public void incrementUpdatedClassifiers(String classifierName, long updatedClassifiers) {
        this.updatedClassifiers.compute(classifierName, (k, v) -> v == null ? updatedClassifiers : v + updatedClassifiers);
    }

    public void incrementDeletedClassifiers(String classifierName, long deletedClassifiers) {
        this.deletedClassifiers.compute(classifierName, (k, v) -> v == null ? deletedClassifiers : v + deletedClassifiers);
    }

    public void incrementFailedClassifiers(String classifierName, long failedClassifiers) {
        this.failedClassifiers.compute(classifierName, (k, v) -> v == null ? failedClassifiers : v + failedClassifiers);
    }

    public void incrementTotalClassifiers(String classifierName, long totalClassifiers) {
        this.totalClassifiers.compute(classifierName, (k, v) -> v == null ? totalClassifiers : v + totalClassifiers);
    }

    public void incrementTotalComplexAttributes(String attributeName, long total) {
        this.totalComplexAttribues.compute(attributeName, (k, v) -> v == null ? total : v + total);
    }

    public void incrementInsertedComplexAttributes(String attributeName, long totals) {
        this.insertedComplexAttribues.compute(attributeName, (k, v) -> v == null ? totals : v + totals);
    }

    public void incrementSkipComplexAttributes(String attributeName, long skip) {
        this.skipComplexAttribues.compute(attributeName, (k, v) -> v == null ? skip : v + skip);
    }

    public void incrementInsertedRecords() {
        this.insertedRecords++;
    }

    public void incrementUpdatedRecords() {
        this.updatedRecords++;
    }

    public void incrementDeletedRecords() {
        this.deletedRecords++;
    }


    public void incrementFailedRecords() {
        this.failedRecords++;
    }

    public void incrementInsertedRelations(String relationName) {
        this.insertedRelations.compute(relationName, (k, v) -> v == null ? 1 : v + 1);
    }

    public void incrementUpdatedRelations(String relationName) {
        this.updatedRelations.compute(relationName, (k, v) -> v == null ? 1 : v + 1);
    }

    public void incrementDeletedRelations(String relationName) {
        this.deletedRelations.compute(relationName, (k, v) -> v == null ? 1 : v + 1);
    }


    public void incrementFailedRelations(String relationName) {
        this.failedRelations.compute(relationName, (k, v) -> v == null ? 1 : v + 1);
    }

    public void incrementInsertedClassifiers(String classifierName) {
        this.insertedClassifiers.compute(classifierName, (k, v) -> v == null ? 1 : v + 1);
    }

    public void incrementUpdatedClassifiers(String classifierName) {
        this.updatedClassifiers.compute(classifierName, (k, v) -> v == null ? 1 : v + 1);
    }

    public void incrementDeletedClassifiers(String classifierName) {
        this.deletedClassifiers.compute(classifierName, (k, v) -> v == null ? 1 : v + 1);
    }


    public void incrementFailedClassifiers(String classifierName) {
        this.failedClassifiers.compute(classifierName, (k, v) -> v == null ? 1 : v + 1);
    }

    public String generateReport() {
        StringBuilder content = new StringBuilder()
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_HEADER, recordName))
                .append(StringUtils.LF)
                .append(StringUtils.LF);

        fillRecordStatistic(content);
        // Relations
        fillRelationStatistic(content);
        // Classifiers
        fillClassifierStatisticRecord(content);
        // ComplexAttribute
        fillComplexAttributeStatistic(content);

        return content.toString();
    }

    private void fillComplexAttributeStatistic(StringBuilder content) {
        Long totalComplexAttributeCount = totalComplexAttribues.values().stream().reduce(0l, Long::sum);
        Long skipComplexAttributeCount = skipComplexAttribues.values().stream().reduce(0l, Long::sum);
        content.append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_DETAILS_COMPLEX_ATTRIBUTE_TOTAL))
                .append(' ')
                .append(totalComplexAttributeCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_SKEPT))
                .append(' ')
                .append(skipComplexAttributeCount)
                .append(".\n\n");
    }

    private void fillRecordStatistic(StringBuilder content) {
        content.append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_RECORDS_TOTAL))
                .append(' ')
                .append(totalRecords)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_INSERTED))
                .append(' ')
                .append(insertedRecords)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_UPDATED))
                .append(' ')
                .append(updatedRecords)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_SKEPT))
                .append(' ')
                .append(totalRecords - insertedRecords - updatedRecords - failedRecords - deletedRecords)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_DELETED))
                .append(' ')
                .append(deletedRecords)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_FAILED))
                .append(' ')
                .append(failedRecords)
                .append(".\n\n");
    }

    private void fillRelationStatistic(StringBuilder content) {
        Long insertedRelationsCount = insertedRelations.values().stream().reduce(0l, Long::sum);
        Long updatedRelationsCount = updatedRelations.values().stream().reduce(0l, Long::sum);
        Long totalRelationsCount = totalRelations.values().stream().reduce(0l, Long::sum);
        Long failedRelationsCount = failedRelations.values().stream().reduce(0l, Long::sum);
        Long deletedRelationsCount = deletedRelations.values().stream().reduce(0l, Long::sum);
        content.append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_RELATIONS_TOTAL))
                .append(' ')
                .append(totalRelationsCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_INSERTED))
                .append(' ')
                .append(insertedRelationsCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_UPDATED))
                .append(' ')
                .append(updatedRelationsCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_SKEPT))
                .append(' ')
                .append(totalRelationsCount - insertedRelationsCount - updatedRelationsCount - failedRelationsCount - deletedRelationsCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_DELETED))
                .append(' ')
                .append(deletedRelationsCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_FAILED))
                .append(' ')
                .append(failedRelationsCount)
                .append(".\n\n");
    }

    private void fillClassifierStatisticRecord(StringBuilder content) {
        Long insertedClassifiersCount = insertedClassifiers.values().stream().reduce(0l, Long::sum);
        Long updatedClassifiersCount = updatedClassifiers.values().stream().reduce(0l, Long::sum);
        Long totalClassifiersCount = totalClassifiers.values().stream().reduce(0l, Long::sum);
        Long failedClassifiersCount = failedClassifiers.values().stream().reduce(0l, Long::sum);
        Long deletedClassifiersCount = deletedClassifiers.values().stream().reduce(0l, Long::sum);
        content.append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_CLASSIFIERS_TOTAL))
                .append(' ')
                .append(totalClassifiersCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_INSERTED))
                .append(' ')
                .append(insertedClassifiersCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_UPDATED))
                .append(' ')
                .append(updatedClassifiersCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_SKEPT))
                .append(' ')
                .append(totalClassifiersCount - insertedClassifiersCount - updatedClassifiersCount - deletedClassifiersCount - failedClassifiersCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_DELETED))
                .append(' ')
                .append(deletedClassifiersCount)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_FAILED))
                .append(' ')
                .append(failedClassifiersCount)
                .append("\n\n");
    }

    public String generateReportDetails() {
        StringBuilder content = new StringBuilder();

        content.append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_DETAILS_RECORD_TOTAL))
                .append(' ')
                .append(recordName)
                .append(':')
                .append(' ')
                .append(totalRecords)
                .append(".\n\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_INSERTED))
                .append(' ')
                .append(insertedRecords)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_UPDATED))
                .append(' ')
                .append(updatedRecords)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_SKEPT))
                .append(' ')
                .append(totalRecords - insertedRecords - updatedRecords - deletedRecords - failedRecords)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_DELETED))
                .append(' ')
                .append(deletedRecords)
                .append(".\n")
                .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_FAILED))
                .append(' ')
                .append(failedRecords)
                .append(".\n\n");

        for (Map.Entry<String, String> relation : relationNames.entrySet()) {
            Long insertedRelationsCount = insertedRelations.getOrDefault(relation.getKey(), 0l);
            Long updatedRelationsCount = updatedRelations.getOrDefault(relation.getKey(), 0l);
            Long skippedRelationsCount = totalRelations.getOrDefault(relation.getKey(), 0l);
            Long failedRelationsCount = failedRelations.getOrDefault(relation.getKey(), 0l);
            Long deletedRelationsCount = deletedRelations.getOrDefault(relation.getKey(), 0l);
            content.append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_DETAILS_RELATION_TOTAL))
                    .append(' ')
                    .append(relation.getValue())
                    .append(':')
                    .append(' ')
                    .append(failedRelationsCount + skippedRelationsCount + insertedRelationsCount + updatedRelationsCount)
                    .append(".\n")
                    .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_INSERTED))
                    .append(' ')
                    .append(insertedRelationsCount)
                    .append(".\n")
                    .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_UPDATED))
                    .append(' ')
                    .append(updatedRelationsCount)
                    .append(".\n")
                    .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_SKEPT))
                    .append(' ')
                    .append(skippedRelationsCount)
                    .append(".\n")
                    .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_DELETED))
                    .append(' ')
                    .append(deletedRelationsCount)
                    .append(".\n")
                    .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_FAILED))
                    .append(' ')
                    .append(failedRelationsCount)
                    .append(".\n\n");
        }

        for (Map.Entry<String, String> classifier : classifierNames.entrySet()) {
            Long insertedClassifiersCount = insertedClassifiers.getOrDefault(classifier.getKey(), 0l);
            Long updatedClassifiersCount = updatedClassifiers.getOrDefault(classifier.getKey(), 0l);
            Long skippedClassifiersCount = totalClassifiers.getOrDefault(classifier.getKey(), 0l);
            Long failedClassifiersCount = failedClassifiers.getOrDefault(classifier.getKey(), 0l);
            Long deletedClassifiersCount = deletedClassifiers.getOrDefault(classifier.getKey(), 0l);
            content.append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_DETAILS_CLASSIFIER_TOTAL))
                    .append(' ')
                    .append(classifier.getValue())
                    .append(':')
                    .append(' ')
                    .append(failedClassifiersCount + insertedClassifiersCount + updatedClassifiersCount + skippedClassifiersCount)
                    .append(".\n")
                    .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_INSERTED))
                    .append(' ')
                    .append(insertedClassifiersCount)
                    .append(".\n")
                    .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_UPDATED))
                    .append(' ')
                    .append(updatedClassifiersCount)
                    .append(".\n")
                    .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_SKEPT))
                    .append(' ')
                    .append(skippedClassifiersCount)
                    .append(".\n")
                    .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_DELETED))
                    .append(' ')
                    .append(deletedClassifiersCount)
                    .append(".\n")
                    .append(TextUtils.getText(XLSXImportConstants.MSG_REPORT_FAILED))
                    .append(' ')
                    .append(failedClassifiersCount)
                    .append(".\n\n");
        }
        return content.toString();
    }

    public Map<String, String> getRelationNames() {
        return relationNames;
    }

    public void setRelationNames(Map<String, String> relationNames) {
        this.relationNames = relationNames;
    }

    public Map<String, String> getClassifierNames() {
        return classifierNames;
    }

    public void setClassifierNames(Map<String, String> classifierNames) {
        this.classifierNames = classifierNames;
    }
    public void setComplexAttributeNames(Map<String, String> complexAttributeNames) {
        this.complexAttributeNames = complexAttributeNames;
    }
}
