/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.data.type.snapshot.FieldsSnapshot;

/**
 * @author Mikhail Mikhailov on Oct 22, 2021
 * Lookup query result.
 */
public class FieldsQueryResult {
    /**
     * The source type name.
     */
    private final String typeName;
    /**
     * Original value for this search.
     */
    private final Serializable value;
    /**
     * The values, keyed by record ID.
     */
    private final Map<String, FieldsSnapshot> fields;
    /**
     * Constructor.
     * @param typeName the source type name.
     * @param value original value for this query
     * @param snapshots field snapshots found
     */
    public FieldsQueryResult(String typeName, Serializable value, Map<String, FieldsSnapshot> snapshots) {
        super();
        this.typeName = typeName;
        this.value = value;
        this.fields = snapshots;
    }
    /**
     * The source type name.
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }
    /**
     * Returns original value supplied for this search.
     * @return original value supplied for this search
     */
    public Serializable getValue() {
        return value;
    }
    /**
     * Gets the values.
     * @return values
     */
    public Map<String, FieldsSnapshot> getFields() {
        return Objects.isNull(fields) ? Collections.emptyMap() : fields;
    }
    /**
     * Gets record keys.
     * @return record keys
     */
    public Collection<String> getKeys() {
        return Objects.isNull(fields) ? Collections.emptySet() : fields.keySet();
    }
    /**
     * Returns lookup value by etalon id.
     * @param etalonId the etalon key
     * @return lookup value
     */
    public FieldsSnapshot getFields(String etalonId) {
        return Objects.isNull(fields) ? null : fields.get(etalonId);
    }
    /**
     * Returns true, if this result has fields for the given record id.
     * @param etalonId the record id
     * @return true, if this result has fields for the given record id
     */
    public boolean hasFields(String etalonId) {
        return Objects.nonNull(fields) && fields.containsKey(etalonId);
    }
    /**
     * Tells whether it has no result.
     * @return true, if empty, false otherwise
     */
    public boolean isEmpty() {
        return MapUtils.isEmpty(fields);
    }
    /**
     * Returns true, if the map holds one single value.
     * @return true, if the map holds one single value
     */
    public boolean isSingleton() {
        return !isEmpty() && fields.size() == 1;
    }
    /**
     * Gets singleton value, if the map holds one single value instance.
     * @return singleton value, if the map holds one single value instance
     */
    public FieldsSnapshot getSingleton() {
        return isSingleton() ? fields.values().iterator().next() : null;
    }
}
