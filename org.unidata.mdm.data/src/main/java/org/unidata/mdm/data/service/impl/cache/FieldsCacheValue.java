/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.impl.cache;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nullable;

import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.data.type.snapshot.FieldsSnapshot;

/**
 * @author Mikhail Mikhailov on Nov 25, 2021
 * The cached value.
 */
public class FieldsCacheValue implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 3784391585266296941L;
    /**
     * The values.
     */
    private HashMap<String, FieldsSnapshot> snapshots;
    /**
     * Constructor.
     */
    public FieldsCacheValue() {
        super();
    }
    /**
     * Appends a value to the result.
     * @param field field name
     * @param val the value
     */
    public void appendSnapshot(String id, CachedFieldsSnapshot entry) {

        if (Objects.isNull(snapshots)) {
            snapshots = new HashMap<>();
        }

        snapshots.put(id, entry);
    }
    /**
     * Gets collected snapshots.
     * @return collected snapshots
     */
    public Map<String, FieldsSnapshot> getSnapshots() {
        return MapUtils.isEmpty(snapshots) ? Collections.emptyMap() : snapshots;
    }
    /**
     * Gets snapshot for an id.
     * @param id the data id
     * @return snapshot for an id or null
     */
    @Nullable
    public CachedFieldsSnapshot getSnapshot(String id) {
        return MapUtils.isEmpty(snapshots) ? null : (CachedFieldsSnapshot) snapshots.get(id);
    }
    /**
     * Empty check support.
     * @return true, if empty
     */
    public boolean isEmpty() {
        return MapUtils.isEmpty(snapshots);
    }
}
