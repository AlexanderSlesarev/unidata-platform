/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.data.service.segments.records.draft;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.data.configuration.DataNamespace;
import org.unidata.mdm.data.context.RecordIdentityContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.data.type.draft.DataDraftOperation;
import org.unidata.mdm.data.type.draft.DataDraftTags;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.dto.DraftUpsertResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.DraftTags;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.system.type.namespace.NameSpace;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Alexey Tsarapkin
 */
@Component(RecordDraftUpsertFinishExecutor.SEGMENT_ID)
public class RecordDraftUpsertFinishExecutor extends Finish<DraftUpsertContext, DraftUpsertResult> {

    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_DRAFT_UPSERT_FINISH]";

    private static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.draft.upsert.finish.description";

    @Autowired
    private MetaModelService metaModelService;

    public RecordDraftUpsertFinishExecutor(){
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftUpsertResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DraftUpsertResult finish(DraftUpsertContext ctx) {

        Draft draft = ctx.currentDraft();
        DraftUpsertResult result = new DraftUpsertResult(true);

        boolean reset = BooleanUtils.isTrue(ctx.getFromUserStorage(DataDraftConstants.DRAFT_STATE_RESET));
        boolean setup = !draft.isExisting() || reset;

        // First run
        if (setup) {
            // 1. Set variables
            variables(draft, result);
            // 2. Set subject, if needed
            subject(draft, result);
            // 3. Update tags
            tags(draft, result, ctx.getPayload(), reset);
        }

        // Regular save runs
        result.setEdition(ctx.currentEdition());
        result.setDraft(draft);

        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return DraftUpsertContext.class.isAssignableFrom(start.getInputTypeClass());
    }
    /*
     * Just put the variables to result.
     */
    private void variables(Draft draft, DraftUpsertResult result) {
        result.setVariables(draft.getVariables());
    }
    /*
     * Set subject if not set.
     */
    private void subject(Draft draft, DraftUpsertResult result) {
        // Support update of subject id for new records,
        // created with this draft (association phase)
        String etalonId = draft.getVariables().valueGet(DataDraftConstants.ETALON_ID);
        if (StringUtils.isBlank(draft.getSubjectId()) && StringUtils.isNotBlank(etalonId)) {
            result.setSubjectId(etalonId);
        }
    }
    /*
     * Collect user tags and re-initialize system tags.
     */
    private void tags(Draft draft, DraftUpsertResult result, RecordIdentityContext ric, boolean reset) {

        Set<String> retval = new HashSet<>();
        Collection<String> current = Objects.isNull(draft.getTags())
                ? Collections.emptyList()
                : draft.getTags();

        for (String tag : current) {
            if (DataDraftTags.RECORD_SYSTEM_TAGS.stream().noneMatch(tag::startsWith)) {
                retval.add(tag);
            }
        }

        String entityName = draft.getVariables().valueGet(DataDraftConstants.ENTITY_NAME);

        EntityElement el = metaModelService.instance(Descriptors.DATA)
                .getElement(entityName);

        NameSpace ns = el.isLookup() ? DataNamespace.LOOKUP : DataNamespace.REGISTER;

        retval.add(DraftTags.toTag(DataDraftTags.NAMESPACE, ns.getId()));
        retval.add(DraftTags.toTag(DataDraftTags.ENTITY_NAME, entityName));

        if (reset) {

            RecordKeys keys = ric.keys();

            DataDraftOperation operation = draft.getVariables()
                    .valueGet(DataDraftConstants.OPERATION_CODE, DataDraftOperation.class);

            retval.add(DraftTags.toTag(DataDraftTags.OPERATION_CODE, operation.name()));
            retval.add(DraftTags.toTag(DataDraftTags.RECORD_EXTERNAL_ID, keys.getOriginKey().toExternalId().compact()));
        }

        // Add tags
        result.addTags(retval);
    }
}
