/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.data.ArrayAttribute;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.CodeAttribute;
import org.unidata.mdm.core.type.data.ComplexAttribute;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.LargeValue;
import org.unidata.mdm.core.type.data.MeasuredValue;
import org.unidata.mdm.core.type.data.SimpleAttribute;
import org.unidata.mdm.core.type.data.impl.DateArrayValue;
import org.unidata.mdm.core.type.data.impl.IntegerArrayValue;
import org.unidata.mdm.core.type.data.impl.NumberArrayValue;
import org.unidata.mdm.core.type.data.impl.StringArrayValue;
import org.unidata.mdm.core.type.data.impl.TimeArrayValue;
import org.unidata.mdm.core.type.data.impl.TimestampArrayValue;
import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.search.type.indexing.IndexingField;
import org.unidata.mdm.search.type.indexing.impl.IndexingRecordImpl;

/**
 * @author Mikhail Mikhailov on Oct 12, 2019
 * Abstract, common to all data kinds, 'DataRecord' part of the indexing support.
 */
public abstract class AbstractIndexingComponent {
    /**
     * The MMS.
     */
    protected final MetaModelService metaModelService;
    /**
     * Constructor.
     */
    protected AbstractIndexingComponent(MetaModelService metaModelService) {
        super();
        this.metaModelService = metaModelService;
    }
    /**
     * Builds JSON representation of an object for insert or update.
     *
     * @param el the top level element
     * @param data the record
     * @return collection of fields
     */
    protected List<IndexingField> buildRecord(EntityElement el, DataRecord data) {

        if (Objects.isNull(data) || data.isEmpty()) {
            return Collections.emptyList();
        }

        List<IndexingField> collected = new ArrayList<>(data.getSize());
        for (Attribute attr : data.getAttributeValues()) {

            IndexingField retval = buildAttribute(el, attr);
            if (Objects.nonNull(retval)) {
                collected.add(retval);
            }
        }

        return collected;
    }
    /**
     * Builds JSON representation of an attribute for insert or update.
     *
     * @param el the entity element
     * @param attr the attr
     * @return the field
     */
    protected IndexingField buildAttribute(EntityElement el, Attribute attr) {

        if (Objects.nonNull(attr)) {

            switch (attr.getAttributeType()) {
            case SIMPLE:
                return buildSimpleAttribute(el, attr);
            case CODE:
                return buildCodeAttribute(el, attr);
            case ARRAY:
                return buildArrayAttribute(el, attr);
            case COMPLEX:
                return buildComplexAttribute(el, attr);
            }
        }

        return null;
    }

    protected IndexingField buildSimpleAttribute(EntityElement el, Attribute attr) {

        AttributeElement ael = el.getAttributes().get(attr.toModelPath());
        if (ael == null || !ael.isSimple() || !ael.isIndexed()) {
            return null;
        }

        final String name = ael.getName();
        final SimpleAttribute<?> simple = (SimpleAttribute<?>) attr;

        switch (simple.getDataType()) {
        case STRING:
        case LINK:
        case ENUM:
        case DICTIONARY:
            return IndexingField.of(name, simple.<String>castValue());
        case NUMBER:
            return IndexingField.of(name, simple.<Double>castValue());
        case MEASURED:
            return IndexingField.of(name, simple.<MeasuredValue>castValue().getBaseValue());
        case BOOLEAN:
            return IndexingField.of(name, simple.<Boolean>castValue());
        case DATE:
            return IndexingField.of(name, simple.<LocalDate>castValue());
        case TIME:
            return IndexingField.of(name, simple.<LocalTime>castValue());
        case TIMESTAMP:
            return IndexingField.of(name, simple.<LocalDateTime>castValue());
        case INTEGER:
            return IndexingField.of(name, simple.<Long>castValue());
        case BLOB:
        case CLOB:
            LargeValue largeValue = simple.castValue();
            return IndexingField.of(name, largeValue == null ? null : largeValue.getFileName());
        default:
            break;
        }

        return null;
    }

    protected IndexingField buildCodeAttribute(EntityElement el, Attribute attr) {

        // Code attributes are always indexed
        AttributeElement ael = el.getAttributes().get(attr.toModelPath());
        if (ael == null || !ael.isCode()) {
            return null;
        }

        final String name = ael.getName();
        final CodeAttribute<?> code = (CodeAttribute<?>) attr;

        switch (code.getDataType()) {
        case INTEGER:
            return IndexingField.ofIntegers(name, Stream.concat(Stream.of(code.<Long>castValue()), code.<Long>castSupplementary().stream())
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
        case STRING:
            return IndexingField.ofStrings(name, Stream.concat(Stream.of(code.<String>castValue()), code.<String>castSupplementary().stream())
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
        default:
            break;
        }

        return null;
    }

    protected IndexingField buildArrayAttribute(EntityElement el, Attribute attr) {

        AttributeElement ael = el.getAttributes().get(attr.toModelPath());
        if (ael == null || !ael.isArray() || !ael.isIndexed()) {
            return null;
        }

        final String name = ael.getName();
        final ArrayAttribute<?> array = (ArrayAttribute<?>) attr;

        if (array.isEmpty()) {
            return null;
        }

        switch (array.getDataType()) {
        case DICTIONARY:
        case STRING:
            return IndexingField.ofStrings(name, array.getValue().stream()
                .map(value -> ((StringArrayValue) value).getValue())
                .collect(Collectors.toList()));
        case NUMBER:
            return IndexingField.ofNumbers(name, array.getValue().stream()
                .map(value -> ((NumberArrayValue) value).getValue())
                .collect(Collectors.toList()));
        case INTEGER:
            return IndexingField.ofIntegers(name, array.getValue().stream()
                .map(value -> ((IntegerArrayValue) value).getValue())
                .collect(Collectors.toList()));
        case DATE:
            return IndexingField.ofDates(name, array.getValue().stream()
                .map(value -> ((DateArrayValue) value).getValue())
                .collect(Collectors.toList()));
        case TIME:
            return IndexingField.ofTimes(name, array.getValue().stream()
                .map(value -> ((TimeArrayValue) value).getValue())
                .collect(Collectors.toList()));
        case TIMESTAMP:
            return IndexingField.ofTimestamps(name, array.getValue().stream()
                .map(value -> ((TimestampArrayValue) value).getValue())
                .collect(Collectors.toList()));
        default:
            break;
        }

        return null;
    }

    protected IndexingField buildComplexAttribute(EntityElement el, Attribute attr) {

        AttributeElement ael = el.getAttributes().get(attr.toModelPath());
        if (ael == null || !ael.isComplex()) {
            return null;
        }

        final String name = ael.getName();
        final ComplexAttribute complexAttribute = (ComplexAttribute) attr;

        if (complexAttribute.isEmpty()) {
            return null;
        }

        return IndexingField.ofRecords(name, complexAttribute.stream()
            .filter(Objects::nonNull)
            .map(r -> buildRecord(el, r))
            .map(IndexingRecordImpl::new)
            .collect(Collectors.toList()));
    }
}
