/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.impl.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.lang3.tuple.Triple;
import org.unidata.mdm.data.type.snapshot.FieldsSnapshot;

/**
 * @author Mikhail Mikhailov on Nov 25, 2021
 */
public class CachedFieldsSnapshot implements FieldsSnapshot, Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -2441043115137106188L;
    /**
     * The fields data in the format <from date, values multimap, to date>.
     */
    private final ArrayList<Triple<Date, ArrayListValuedHashMap<String, Serializable>, Date>> data = new ArrayList<>(2);
    /**
     * Constructor.
     */
    public CachedFieldsSnapshot() {
        super();
    }
    /**
     * Appends period data to the store.
     * @param from the right boundary date (period start)
     * @param to the left boundary date (period end)
     * @param values the fields
     */
    public void append(Date from, Date to, ArrayListValuedHashMap<String, Serializable> values) {
        data.add(Triple.of(from, values, to));
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getNumberOfPeriods() {
        return data.size();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MultiValuedMap<String, Serializable> getFieldsAt(int index) {

        if (isEmpty() || index < 0 || index > (data.size() - 1)) {
            return null;
        }

        return data.get(index).getMiddle();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MultiValuedMap<String, Serializable> getFieldsHead() {

        if (isEmpty()) {
            return null;
        }

        return data.get(0).getMiddle();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MultiValuedMap<String, Serializable> getFieldsTail() {

        if (isEmpty()) {
            return null;
        }

        return data.get(data.size() - 1).getMiddle();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MultiValuedMap<String, Serializable> getFieldsFor(Date validFrom, Date validTo) {

        if (isEmpty()) {
            return null;
        }

        for (int i = 0; i < data.size(); i++) {

            Triple<Date, ArrayListValuedHashMap<String, Serializable>, Date> entry = data.get(i);
            if (((validTo == null || entry.getLeft() == null || !entry.getLeft().after(validTo))
             && ((validFrom == null) || entry.getRight() == null || !entry.getRight().before(validFrom)))) {
                return entry.getMiddle();
            }
        }

        return null;
    }
    /**
     * Empty check support.
     * @return true, if empty
     */
    @Override
    public boolean isEmpty() {
        return CollectionUtils.isEmpty(data);
    }
}
