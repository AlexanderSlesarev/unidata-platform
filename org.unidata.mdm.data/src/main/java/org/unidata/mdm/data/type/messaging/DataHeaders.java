package org.unidata.mdm.data.type.messaging;

import org.unidata.mdm.system.type.messaging.Header;
import org.unidata.mdm.system.type.messaging.Header.HeaderType;

/**
 * @author Mikhail Mikhailov on Jul 15, 2020
 */
public class DataHeaders {
    /**
     * Constructor.
     */
    private DataHeaders() {
        super();
    }
    /**
     * Entity name parameter.
     */
    public static final Header ENTITY_NAME = new Header("entity_name", HeaderType.STRING);
    /**
     * Etalon id parameter.
     */
    public static final Header ETALON_ID = new Header("etalon_id", HeaderType.STRING);
    /**
     * External id parameter.
     */
    public static final Header EXTERNAL_ID = new Header("external_id", HeaderType.STRING);
    /**
     * Source system parameter.
     */
    public static final Header SOURCE_SYSTEM = new Header("source_system", HeaderType.STRING);
}
