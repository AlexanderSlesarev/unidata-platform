/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.records.restore;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.RestoreRecordRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.impl.RecordsIndexingComponent;
import org.unidata.mdm.data.type.apply.RecordRestoreChangeSet;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.meta.type.search.RecordIndexId;
import org.unidata.mdm.search.configuration.SearchConfigurationConstants;
import org.unidata.mdm.search.context.IndexRequestContext;
import org.unidata.mdm.search.type.id.ManagedIndexId;
import org.unidata.mdm.search.type.indexing.Indexing;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * Restore indexing executor.
 * @author Mikhail Mikhailov on Nov 10, 2019
 */
@Component(RecordRestoreIndexingExecutor.SEGMENT_ID)
public class RecordRestoreIndexingExecutor extends Point<RestoreRecordRequestContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_RESTORE_INDEXING]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.restore.indexing.description";
    /**
     * Delay for async audit operations.
     */
    @ConfigurationRef(SearchConfigurationConstants.PROPERTY_REFRESH_IMMEDIATE)
    private ConfigurationValue<Boolean> refreshImmediate;
    /**
     * The RIC.
     */
    @Autowired
    private RecordsIndexingComponent recordsIndexingComponent;
    /**
     * Constructor.
     * @param id
     * @param description
     */
    public RecordRestoreIndexingExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RestoreRecordRequestContext ctx) {

        MeasurementPoint.start();
        try {

            final RecordKeys keys = ctx.keys();
            final RecordRestoreChangeSet cs = ctx.changeSet();

            IndexRequestContext irc = IndexRequestContext.builder()
                    .drop(true)
                    .entity(keys.getEntityName())
                    .delete(collectDeletes(ctx))
                    .index(collectUpdates(ctx))
                    .routing(keys.getEtalonKey().getId())
                    .refresh(!ctx.isBatchOperation() && refreshImmediate.getValue())
                    .build();

            cs.addIndexRequestContext(irc);

        } finally {
            MeasurementPoint.stop();
        }
    }

    private Collection<ManagedIndexId> collectDeletes(RestoreRecordRequestContext ctx) {

        Timeline<OriginRecord> current = ctx.currentTimeline();
        if (current.isEmpty()) {
            return Collections.emptyList();
        }

        RecordKeys keys = current.getKeys();
        return current.stream()
                .map(interval -> RecordIndexId.of(keys.getEntityName(), keys.getEtalonKey().getId(), interval.getPeriodId()))
                .collect(Collectors.toList());
    }

    private Collection<Indexing> collectUpdates(RestoreRecordRequestContext ctx) {

        Timeline<OriginRecord> next = ctx.nextTimeline();
        if (next.isEmpty()) {
            return Collections.emptyList();
        }

        return recordsIndexingComponent.build(next.getKeys(), next);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RestoreRecordRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
