/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service;

import java.util.Collection;
import javax.annotation.Nonnull;
import org.unidata.mdm.data.context.FieldsQueryContext;
import org.unidata.mdm.data.dto.FieldsQueryResult;

/**
 * @author Dmitry Kopin on 31.05.2019.
 * The service, responsible for querying and caching of code/unique attribute values,
 * operating on top of search indexes.
 */
public interface FieldsCacheService {
    /**
     * Fetches attributes, for the given request.
     * Please note, response may contain fields for several records, so it must be filtered by record id.
     * @param input the context
     * @return result
     */
    @Nonnull
    FieldsQueryResult fetch(FieldsQueryContext input);
    /**
     * Fetches attributes for several requested values.
     * @param input collection of contexts
     * @return collection with results
     */
    Collection<FieldsQueryResult> fetch(Collection<FieldsQueryContext> input);
    /**
     * Discards value for the context.
     * @param ctx the context
     */
    void discard(FieldsQueryContext ctx);
    /**
     * Discards several values.
     * @param set the contexts
     */
    void discard(Collection<FieldsQueryContext> set);
}
