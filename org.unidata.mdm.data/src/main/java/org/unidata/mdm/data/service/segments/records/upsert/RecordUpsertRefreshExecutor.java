/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.segments.records.upsert;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.SingleValueAttribute;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.FieldsQueryContext;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.FieldsCacheService;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.data.UpsertAction;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.search.RecordHeaderField;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Mikhail Mikhailov on Nov 24, 2021
 * Performs refresh/cleanup for the record being upserted.
 */
@Component(RecordUpsertRefreshExecutor.SEGMENT_ID)
public class RecordUpsertRefreshExecutor extends Point<UpsertRequestContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_UPSERT_REFRESH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.upsert.refresh.description";
    /**
     * FCS.
     */
    @Autowired
    private FieldsCacheService fieldsCacheService;
    /**
     * MMS.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Constructor.
     */
    public RecordUpsertRefreshExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(UpsertRequestContext ctx) {

        UpsertAction ua = ctx.upsertAction();
        if (ua == UpsertAction.INSERT || ua == UpsertAction.NO_ACTION) {
            return;
        }

        Timeline<OriginRecord> prev = ctx.currentTimeline();
        RecordKeys keys = prev.getKeys();

        EntityElement el = metaModelService.instance(Descriptors.DATA)
                .getElement(keys.getEntityName());

        // 1. Discard cache
        discardCache(ctx, el, prev, keys);
    }

    private void discardCache(UpsertRequestContext ctx, EntityElement el, Timeline<OriginRecord> prev, RecordKeys keys) {

        List<FieldsQueryContext> queries = new ArrayList<>(4);

        queries.add(FieldsQueryContext.builder()
                .elementName(el.getName())
                .lookupValue(keys.getEtalonKey().getId())
                .lookupField(RecordHeaderField.FIELD_ETALON_ID.getPath())
                .deleted(false)
                .inactive(false)
                .build());

        final List<EtalonRecord> etalons = prev.selectBy(ctx.getValidFrom(), ctx.getValidTo()).stream()
                .map(TimeInterval::<EtalonRecord>getCalculationResult)
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(etalons)) {

            el.getAttributes().entrySet().stream()
                .filter(entry -> entry.getValue().getLevel() == 0 && (entry.getValue().isUnique() || entry.getValue().isCode()))
                .forEach(entry -> {

                    for (EtalonRecord etalon : etalons) {

                        Attribute attr = etalon.getAttribute(entry.getKey());
                        Serializable val = attr == null ? null : ((SingleValueAttribute<?>) attr).castValue();
                        if (Objects.isNull(val)) {
                            continue;
                        }

                        queries.add(FieldsQueryContext.builder()
                                .elementName(el.getName())
                                .lookupValue(val)
                                .lookupField(entry.getValue().getIndexed().getPath())
                                .deleted(false)
                                .inactive(false)
                                .build());
                    }
                });
        }

        fieldsCacheService.discard(queries);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return UpsertRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
