/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.configuration;

import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.system.type.namespace.NameSpace;
import org.unidata.mdm.system.util.TextUtils;

/**
 * @author Mikhail Mikhailov on Mar 26, 2021
 * Data namespaces.
 */
public final class DataNamespace {
    /**
     * Lookups namespace.
     */
    public static final NameSpace LOOKUP = NameSpace.of("lookup", DataModule.MODULE_ID,
            () -> TextUtils.getText(DataModule.MODULE_ID + ".lookup.namespace.display.name"),
            () -> TextUtils.getText(DataModule.MODULE_ID + ".lookup.namespace.description"));
    /**
     * Registers namespace.
     */
    public static final NameSpace REGISTER = NameSpace.of("register", DataModule.MODULE_ID,
            () -> TextUtils.getText(DataModule.MODULE_ID + ".register.namespace.display.name"),
            () -> TextUtils.getText(DataModule.MODULE_ID + ".register.namespace.description"));
    /**
     * Relations namespace.
     */
    public static final NameSpace RELATION = NameSpace.of("relation", DataModule.MODULE_ID,
            () -> TextUtils.getText(DataModule.MODULE_ID + ".relation.namespace.display.name"),
            () -> TextUtils.getText(DataModule.MODULE_ID + ".relation.namespace.description"));
    /**
     * The values as array.
     */
    private static final NameSpace[] VALUES = new NameSpace[] {
            LOOKUP,
            REGISTER,
            RELATION
    };
    /**
     * Constructor.
     */
    private DataNamespace() {
        super();
    }
    /**
     * Gets values at whole.
     * @return values
     */
    public static NameSpace[] values() {
        return VALUES;
    }
}
