/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.segments;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.core.type.data.ArrayAttribute;
import org.unidata.mdm.core.type.data.CodeAttribute;
import org.unidata.mdm.core.type.data.ComplexAttribute;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.SimpleAttribute;
import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.model.LookupElement;
import org.unidata.mdm.core.type.model.NestedElement;
import org.unidata.mdm.data.context.FieldsQueryContext;
import org.unidata.mdm.data.dto.FieldsQueryResult;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.FieldsCacheService;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.snapshot.FieldsSnapshot;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.context.ConsistencyCheckContext;
import org.unidata.mdm.meta.service.ConsistencyCheckSupport;
import org.unidata.mdm.system.exception.ValidationResult;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov on Oct 25, 2021
 * Detailed record consistency check support.
 */
public interface RecordConsistencyCheckSupport extends ConsistencyCheckSupport {
    /**
     * Ambigous value distribution. More then one record holds the same code value.
     */
    String EX_DATA_ATTRIBUTE_AMBIGOUS_CODE_VALUE_DISTRIBUTION = DataModule.MODULE_ID + ".attribute.ambigous.code.value.distribution";
    /**
     * Code value is already used by another record.
     */
    String EX_DATA_ATTRIBUTE_CODE_VALUE_ALREADY_USED = DataModule.MODULE_ID + ".attribute.code.value.already.used";
    /**
     * Code value does not cover the period.
     */
    String EX_DATA_ATTRIBUTE_CODE_VALUE_DOES_NOT_COVER = DataModule.MODULE_ID + ".attribute.code.value.does.not.cover";
    /**
     * Unique value is already used by another record.
     */
    String EX_DATA_ATTRIBUTE_UNIQUE_VALUE_ALREADY_USED = DataModule.MODULE_ID + ".attribute.unique.value.already.used";
    /**
     * Duplicate neighbour value exists.
     */
    String EX_DATA_ATTRIBUTE_UNIQUE_NEIGHBOUR_VALUE = DataModule.MODULE_ID + ".attribute.unique.neighbour.value";
    /**
     * Code value, referenced by attribute, is missing in the target lookup.
     */
    String EX_DATA_ATTRIBUTE_CODE_REFERENCED_VALUE_MISSING = DataModule.MODULE_ID + ".attribute.code.referenced.value.missing";
    /**
     * Lookup service.
     * @return service
     */
    FieldsCacheService fieldsCacheService();
    /**
     * {@inheritDoc}
     */
    @Override
    default void checkComplexAttribute(ConsistencyCheckContext check, ComplexAttribute attr, AttributeElement el) {

        ConsistencyCheckSupport.super.checkComplexAttribute(check, attr, el);
        if (attr.isEmpty()) {
            return;
        }

        // It is guaranteed, that an attribute can reference only existing objects.
        // So no check.
        NestedElement ne = metaModelService()
              .instance(Descriptors.DATA)
              .getNested(el.getComplex().getNestedEntityName());

        check.offer(attr.toCollection(), ne);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    default void checkCodeAttribute(ConsistencyCheckContext check, CodeAttribute<?> attr, AttributeElement el) {

        int memo = check.count();
        ConsistencyCheckSupport.super.checkCodeAttribute(check, attr, el);

        if (check.count() != memo) {
            return;
        }

        EtalonRecord er = check.getPayload();
        Serializable v = attr.castValue();

        // 1. Check, whether this code attribute is itself a lookup link and has a valid value
        if (el.isLookupLink()) {
            checkLookupLink(check, el, er, v);
        }

        // 2. Check own index
        FieldsQueryResult lqr = fieldsCacheService().fetch(FieldsQueryContext.builder()
                .lookupValue(v)
                .lookupField(el.getName())
                .elementName(el.getContainer().getName())
                .build());

        if (!lqr.isEmpty()) {

            ensureSingleton(check, lqr, v);

            if (!lqr.hasFields(er.getInfoSection().getEtalonKey().getId())) {
                check.append(new ValidationResult("Code value ({}) is already used by another record ({}, {}).",
                        EX_DATA_ATTRIBUTE_CODE_VALUE_ALREADY_USED,
                        v, lqr.getKeys().iterator().next(), el.getName()));
            }
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    default void checkArrayAttribute(ConsistencyCheckContext check, ArrayAttribute<?> attr, AttributeElement el) {

        int memo = check.count();
        ConsistencyCheckSupport.super.checkArrayAttribute(check, attr, el);

        if (check.count() != memo || (attr.isEmpty() && el.isNullable())) {
            return;
        }

        if (el.isLookupLink()) {

            EtalonRecord er = check.getPayload();
            List<Serializable> v = attr.getValues();

            for (Serializable vv : v) {
                checkLookupLink(check, el, er, vv);
            }
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    default void checkSimpleAttribute(ConsistencyCheckContext check, SimpleAttribute<?> attr, AttributeElement el) {

        int memo = check.count();
        ConsistencyCheckSupport.super.checkSimpleAttribute(check, attr, el);

        if (check.count() != memo || (attr.isEmpty() && el.isNullable())) {
            return;
        }

        EtalonRecord er = check.getPayload();
        if (el.isLookupLink()) {
            checkLookupLink(check, el, er, attr.castValue());
        }

        if (el.isUnique()) {
            checkUnique(check, attr, er, attr.castValue());
        }
    }

    private void checkUnique(ConsistencyCheckContext check, SimpleAttribute<?> attr, EtalonRecord er, Serializable v) {

        EntityElement eel = check.getEntity();

        // 1. Compare to other records
        FieldsQueryResult lqr = fieldsCacheService().fetch(FieldsQueryContext.builder()
                .lookupValue(v)
                .lookupField(attr.toModelPath())
                .elementName(eel.getName())
                .build());

        if (!lqr.isEmpty()) {

            ensureSingleton(check, lqr, v);

            if (!lqr.hasFields(er.getInfoSection().getEtalonKey().getId())) {
                check.append(new ValidationResult("Unique value ({}) is already used by another record ({}, {}).",
                        EX_DATA_ATTRIBUTE_UNIQUE_VALUE_ALREADY_USED,
                        v, lqr.getKeys().iterator().next(), attr.toModelPath()));
            }
        }

        // 2. Check other siblings records, which may possibly exist
        DataRecord data = attr.getRecord();
        if (Objects.nonNull(data) && !data.isTopLevel()) {

            ComplexAttribute holder = data.getHolderAttribute();
            for (DataRecord other : holder) {

                if (other == data) {
                    continue;
                }

                SimpleAttribute<?> sibling = other.getSimpleAttribute(attr.getName());
                if (Objects.nonNull(sibling) && Objects.equals(sibling.getValue(), v)) {

                    check.append(new ValidationResult("Unique attribute ({}) value ({}) duplicates in neighbour records in ({}).",
                            EX_DATA_ATTRIBUTE_UNIQUE_NEIGHBOUR_VALUE,
                            attr.getName(), v, holder.toLocalPath()));
                }
            }
        }
    }

    private void checkLookupLink(ConsistencyCheckContext check, AttributeElement el, EtalonRecord er, Serializable v) {

        LookupElement ref = metaModelService()
                .instance(Descriptors.DATA)
                .getLookup(el.getLookupLink().getLookupLinkName());

        FieldsQueryResult lqr = fieldsCacheService().fetch(FieldsQueryContext.builder()
                .lookupValue(v)
                .lookupField(ref.getCodeAttribute().getName())
                .elementName(ref.getName())
                .build());

        if (lqr.isEmpty()) {
            check.append(new ValidationResult("Code value {}, referenced by attribute {}, is missing in the target lookup {}.",
                    EX_DATA_ATTRIBUTE_CODE_REFERENCED_VALUE_MISSING,
                    v, el.getName(), ref.getName()));
        } else {

            ensureSingleton(check, lqr, v);

            FieldsSnapshot fs = lqr.getSingleton();
            if (fs.getFieldsFor(er.getInfoSection().getValidFrom(), er.getInfoSection().getValidTo()) == null) {

                check.append(new ValidationResult("Code value {}, does not cover upsert period ({} - {}).",
                        EX_DATA_ATTRIBUTE_CODE_VALUE_DOES_NOT_COVER,
                        v,
                        ConvertUtils.date2String(er.getInfoSection().getValidFrom()),
                        ConvertUtils.date2String(er.getInfoSection().getValidTo())));
            }
        }
    }

    private void ensureSingleton(ConsistencyCheckContext check, FieldsQueryResult lqr, Object v) {

        if (!lqr.isSingleton()) {
            check.append(new ValidationResult(
                    "Ambiguous value distribution. More than one active record holds the same code / unique value ({}).",
                    EX_DATA_ATTRIBUTE_AMBIGOUS_CODE_VALUE_DISTRIBUTION,
                    v));
        }
    }
}
