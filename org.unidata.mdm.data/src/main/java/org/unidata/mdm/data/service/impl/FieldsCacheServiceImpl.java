/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.data.configuration.DataConfigurationConstants;
import org.unidata.mdm.data.context.FieldsQueryContext;
import org.unidata.mdm.data.dto.FieldsQueryResult;
import org.unidata.mdm.data.service.FieldsCacheService;
import org.unidata.mdm.data.service.impl.cache.FieldsCacheKey;
import org.unidata.mdm.data.service.impl.cache.FieldsCacheStore;
import org.unidata.mdm.data.service.impl.cache.FieldsCacheValue;
import org.unidata.mdm.system.service.ConfigurationActionService;
import org.unidata.mdm.system.type.action.ConfigurationAction;
import com.hazelcast.config.InMemoryFormat;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MapStoreConfig;
import com.hazelcast.config.MergePolicyConfig;
import com.hazelcast.config.NearCacheConfig;
import com.hazelcast.config.NearCacheConfig.LocalUpdatePolicy;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;

/**
 * @author Dmitry Kopin on 31.05.2019.
 */
@Component
public class FieldsCacheServiceImpl implements FieldsCacheService {
    /**
     * This class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FieldsCacheServiceImpl.class);
    /**
     * Empty QR.
     */
    private static final FieldsQueryResult EMPTY_QUERY_RESULT = new FieldsQueryResult(StringUtils.EMPTY, null, Collections.emptyMap());
    /**
     * Config action.
     */
    private final ConfigurationAction ensureFieldsAction = new ConfigurationAction() {
        /**
         * {@inheritDoc}
         */
        @Override
        public int getTimes() {
            return 1;
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public String getId() {
            return DataConfigurationConstants.DATA_FIELDS_CACHE_MAP_ACTION;
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public boolean execute() {

            try {

                final MapConfig tokensMapConfig = new MapConfig(DataConfigurationConstants.DATA_FIELDS_CACHE_MAP)
                        .setBackupCount(0)
                        .setReadBackupData(true)
                        .setInMemoryFormat(InMemoryFormat.BINARY)
                        .setMaxIdleSeconds(5)
                        .setMergePolicyConfig(new MergePolicyConfig("com.hazelcast.spi.merge.PassThroughMergePolicy", 1))
                        .setNearCacheConfig(new NearCacheConfig()
                                .setCacheLocalEntries(true)
                                .setInMemoryFormat(InMemoryFormat.OBJECT)
                                .setMaxIdleSeconds(5)
                                .setInvalidateOnChange(true)
                                .setLocalUpdatePolicy(LocalUpdatePolicy.INVALIDATE))
                        .setMapStoreConfig(new MapStoreConfig()
                                .setClassName(FieldsCacheStore.class.getName())
                                .setEnabled(true));

                hazelcastInstance.getConfig().addMapConfig(tokensMapConfig);

            } catch (Exception e) {
                LOGGER.warn("Cannot set up fields cache map configuration. Exception caught.", e);
                return false;
            }

            return true;
        }
    };
    /**
     * HZ instance.
     */
    @Autowired
    private HazelcastInstance hazelcastInstance;
    /**
     * The fields cache.
     */
    private IMap<FieldsCacheKey, FieldsCacheValue> cache;
    /**
     * CAS.
     */
    @Autowired
    private ConfigurationActionService configurationActionService;
    /**
     * Constructor.
     */
    public FieldsCacheServiceImpl() {
        super();
    }
    /**
     * Init.
     */
    @PostConstruct
    public void init() {
        configurationActionService.execute(ensureFieldsAction);
        cache = hazelcastInstance.getMap(DataConfigurationConstants.DATA_FIELDS_CACHE_MAP);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FieldsQueryResult fetch(FieldsQueryContext ctx) {

        if (Objects.nonNull(ctx)) {

            final FieldsCacheKey fck = new FieldsCacheKey(ctx);
            final FieldsCacheValue fcv = cache.get(fck);

            if (Objects.nonNull(fcv)) {
                return new FieldsQueryResult(fck.getElementName(), fck.getLookupValue(), fcv.getSnapshots());
            } else {
                // Near cache caches null values too, so it must be evicted explicitly.
                evict(fck);
            }
        }

        return EMPTY_QUERY_RESULT;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<FieldsQueryResult> fetch(Collection<FieldsQueryContext> input) {

        if (CollectionUtils.isEmpty(input)) {
            return Collections.emptyList();
        }

        Set<FieldsCacheKey> m = new HashSet<>();
        for (FieldsQueryContext fqc : input) {

            if (Objects.nonNull(fqc)) {
                m.add(new FieldsCacheKey(fqc));
            }
        }

        Map<FieldsCacheKey, FieldsCacheValue> result = cache.getAll(m);
        if (MapUtils.isEmpty(result)) {
            // Near cache caches null values too, so it must be evicted explicitly.
            evictAll(m);
            return Collections.emptyList();
        }

        List<FieldsQueryResult> retval = new ArrayList<>(result.size());
        for (FieldsCacheKey key : m) {

            FieldsCacheValue fcv = result.get(key);
            if (Objects.isNull(fcv) || fcv.isEmpty()) {
                // Near cache caches null values too, so it must be evicted explicitly.
                evict(key);
                continue;
            }

            retval.add(new FieldsQueryResult(key.getElementName(), key.getLookupValue(), fcv.getSnapshots()));
        }

        return retval;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void discard(FieldsQueryContext ctx) {

        if (Objects.nonNull(ctx)) {
            cache.delete(new FieldsCacheKey(ctx));
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void discard(Collection<FieldsQueryContext> set) {

        if (CollectionUtils.isNotEmpty(set)) {
            for (FieldsQueryContext ctx : set) {
                discard(ctx);
            }
        }
    }
    /*
     * Near cache caches null values too (what we don't want),
     * so it must be evicted explicitly.
     */
    private void evict(FieldsCacheKey fck) {
        cache.evict(fck);
    }
    /*
     * Near cache caches null values too (what we don't want),
     * so it must be evicted explicitly.
     */
    private void evictAll(Collection<FieldsCacheKey> items) {
        for (FieldsCacheKey fck : items) {
            evict(fck);
        }
    }
}
