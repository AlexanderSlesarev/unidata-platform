/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.impl.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.model.AttributeElement.AttributeValueType;
import org.unidata.mdm.core.util.AttributeUtils;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.exception.DataProcessingException;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.search.RecordHeaderField;
import org.unidata.mdm.meta.type.search.RelationHeaderField;
import org.unidata.mdm.search.dto.SearchResultHitDTO;
import org.unidata.mdm.search.dto.SearchResultHitFieldDTO;
import org.unidata.mdm.search.type.IndexField;
import org.unidata.mdm.search.type.form.FieldsGroup;
import org.unidata.mdm.search.type.form.FormField;
import org.unidata.mdm.search.util.SearchUtils;

/**
 * @author Mikhail Mikhailov on Nov 25, 2021
 * A temporary storage for search related bits.
 */
public class FieldsQueryState {

    private final MetaModelService metaModelService;

    private final Map<Object, MutablePair<FieldsCacheKey, FieldsCacheValue>> memo = new HashMap<>();

    private final EntityElement entityElement;

    private final IndexField lookupField;

    private final AttributeValueType lookupFieldValueType;

    private final Set<String> fieldsSpace;

    private boolean lookupDeleted;

    private boolean lookupInactive;
    /**
     * Constructor.
     * @param entityName the name
     * @param fieldName the field
     */
    public FieldsQueryState(MetaModelService metaModelService, String entityName, String fieldName) {
        super();

        Objects.requireNonNull(metaModelService, "Meta model service must not be null.");

        this.metaModelService = metaModelService;
        this.entityElement = element(entityName);
        this.lookupField = field(this.entityElement, fieldName);
        this.lookupFieldValueType = type(this.entityElement, this.lookupField);
        this.fieldsSpace = entityElement.getAttributes().entrySet().stream()
                .filter(entry -> entry.getValue().getLevel() == 0 && entry.getValue().isIndexed())
                .map(Entry::getKey)
                .collect(Collectors.toSet());
    }
    /**
     * @return the entityElement
     */
    public EntityElement getEntityElement() {
        return entityElement;
    }
    /**
     * Appends a key to the memorizer.
     * @param val the value
     * @param key the key
     */
    public void appendKey(FieldsCacheKey key) {

        if (MapUtils.isEmpty(memo)) {
            lookupDeleted = key.lookupDeleted();
            lookupInactive = key.lookupInactive();
        } else {
            throwIfDeletedFlagMismatch(lookupDeleted, key.lookupDeleted());
            throwIfInactiveFlagMismatch(lookupInactive, key.lookupInactive());
        }

        memo.put(key.getLookupValue(), new MutablePair<>(key, null));
    }
    /**
     * Appends a search hit to the result.
     * @param hit the hit to append
     */
    public void appendHit(SearchResultHitDTO hit) {

        // Bucket
        Pair<FieldsCacheKey, FieldsCacheValue> bucket = ensureCacheBucket(hit);

        // Value container
        FieldsCacheValue value = bucket.getRight();

        // Snapshot fields container
        CachedFieldsSnapshot cfs = ensureSnapshot(value, hit);

        ArrayListValuedHashMap<String, Serializable> fields = new ArrayListValuedHashMap<>(fieldsSpace.size(), 2);
        for (String attr : fieldsSpace) {

            SearchResultHitFieldDTO hf = hit.getFieldValue(attr);

            // UN-7814
            if (hf != null && !hf.isEmpty()) {

                AttributeElement ael = entityElement.getAttributes().get(attr);
                if (Objects.isNull(ael)) {
                    continue;
                }

                // Narrow ES values to types, used by UD.
                // Otherwise memorized values can't be found.
                AttributeValueType vType = ael.getValueType();
                hf.getValues().stream()
                    .filter(Objects::nonNull)
                    .forEach(obj -> fields.put(attr, AttributeUtils.toNearestType(vType, obj)));
            }
        }

        Date validFrom = SearchUtils.parse(hit.getFieldFirstValue(entityElement.isRelation() ? RelationHeaderField.FIELD_FROM.getPath() : RecordHeaderField.FIELD_FROM.getPath()));
        Date validTo = SearchUtils.parse(hit.getFieldFirstValue(entityElement.isRelation() ? RelationHeaderField.FIELD_TO.getPath() : RecordHeaderField.FIELD_TO.getPath()));

        cfs.append(validFrom, validTo, fields);
    }
    /**
     * Converts collected state to result.
     * @return result
     */
    public Map<FieldsCacheKey, FieldsCacheValue> toResult() {
        return MapUtils.isEmpty(memo)
                ? Collections.emptyMap()
                : memo.values().stream()
                    .filter(mp -> Objects.nonNull(mp.getValue()))
                    .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
    }
    /**
     * Converts collected state to return fields.
     * @return return fields
     */
    public Collection<String> toReturnFields() {

        List<String> fields = new ArrayList<>(fieldsSpace);

        if (!fieldsSpace.contains(lookupField.getPath())) {
            fields.add(lookupField.getPath());
        }

        IndexField id = entityElement.isRelation() ? RelationHeaderField.FIELD_ETALON_ID : RecordHeaderField.FIELD_ETALON_ID;
        if (!fieldsSpace.contains(id.getPath())) {
            fields.add(id.getPath());
        }

        fields.add(entityElement.isRelation() ? RelationHeaderField.FIELD_FROM.getPath() : RecordHeaderField.FIELD_FROM.getPath());
        fields.add(entityElement.isRelation() ? RelationHeaderField.FIELD_TO.getPath() : RecordHeaderField.FIELD_TO.getPath());

        return fields;
    }
    /**
     * Converts collected state to search fields group.
     * @return search fields group
     */
    public FieldsGroup toSearchGroup() {

        FieldsGroup and = FieldsGroup.and();
        if (!lookupDeleted) {
            and.add(FormField.exact(entityElement.isRelation()
                    ? RelationHeaderField.FIELD_DELETED
                    : RecordHeaderField.FIELD_DELETED, Boolean.FALSE));
        }

        if (!lookupInactive) {
            and.add(FormField.exact(entityElement.isRelation()
                    ? RelationHeaderField.FIELD_INACTIVE
                    : RecordHeaderField.FIELD_INACTIVE, Boolean.FALSE));
        }

        return and.add(FieldsGroup.and(FormField.exact(lookupField, memo.keySet())));
    }

    private EntityElement element(String name) {

        EntityElement el = metaModelService
                .instance(Descriptors.DATA)
                .getElement(name);

        throwIfElementNotFound(el, name);

        return el;
    }

    private IndexField field(EntityElement el, String fieldName) {

        IndexField field = null;
        if (Objects.nonNull(fieldName)) {

            // 1. System field
            if (fieldName.charAt(0) == '$') {

                field = el.isRegister()
                        ? RecordHeaderField.fieldByName(fieldName)
                        : RelationHeaderField.fieldByName(fieldName);
            // 2. Regular field
            } else {

                AttributeElement ael = el.getAttributes().get(fieldName);
                throwIfNullOrNotCodeOrUnique(ael, fieldName);

                field = ael.getIndexed();
            }
        } else {

            field = el.isLookup() ? el.getLookup().getCodeAttribute().getIndexed() : null;
        }

        throwIfIndexFieldIsNull(field);

        return field;
    }

    private AttributeValueType type(EntityElement el, IndexField f) {

        if (f.getName().charAt(0) == '$') {
            return AttributeValueType.STRING;
        }

        AttributeElement ael = el.getAttributes().get(f.getPath());
        return ael.getValueType();
    }

    private Pair<FieldsCacheKey, FieldsCacheValue> ensureCacheBucket(SearchResultHitDTO hit) {

        MutablePair<FieldsCacheKey, FieldsCacheValue> bucket = null;

        List<Object> all = hit.getFieldValues(lookupField.getPath());
        for (int i = 0; all != null && i < all.size(); i++) {

            // Narrow ES values to types, used by UD.
            // Otherwise memorized values can't be found.
            bucket = memo.get(AttributeUtils.toNearestType(lookupFieldValueType, all.get(i)));
            if (Objects.nonNull(bucket)) {
                break;
            }
        }

        Objects.requireNonNull(bucket, "Value bucket resoved to null!");

        FieldsCacheValue value = bucket.getRight();
        if (Objects.isNull(value)) {
            value = new FieldsCacheValue();
            bucket.setValue(value);
        }

        return bucket;
    }

    private CachedFieldsSnapshot ensureSnapshot(FieldsCacheValue value, SearchResultHitDTO hit) {

        String etalonId = hit.getFieldFirstValue(entityElement.isRelation() ? RelationHeaderField.FIELD_ETALON_ID.getPath() : RecordHeaderField.FIELD_ETALON_ID.getPath());
        CachedFieldsSnapshot cfs = value.getSnapshot(etalonId);
        if (Objects.isNull(cfs)) {
            cfs = new CachedFieldsSnapshot();
            value.appendSnapshot(etalonId, cfs);
        }

        return cfs;
    }

    private void throwIfElementNotFound(EntityElement el, String name) {
        if (Objects.isNull(el)) {
            throw new DataProcessingException("Entity element {} not found.",
                    DataExceptionIds.EX_DATA_RECORD_LOOKUP_CACHE_NO_ELEMENT,
                    name);
        }
    }

    private void throwIfIndexFieldIsNull(IndexField f) {
        if (Objects.isNull(f)) {
            throw new DataProcessingException("Caching lookup failed. Lookup field is missing in the request or cannot be resolved.",
                    DataExceptionIds.EX_DATA_RECORD_LOOKUP_CACHE_FIELD_UNRESOLVED);
        }
    }

    private void throwIfNullOrNotCodeOrUnique(AttributeElement el, String name) {
        if (Objects.isNull(el) || (!el.isCode() && !el.isCodeAlternative() && !el.isUnique())) {
            throw new DataProcessingException("Attribute {} is either null or not a code|unique attribute.",
                    DataExceptionIds.EX_DATA_RECORD_LOOKUP_CACHE_NOT_ACCEPTABLE_ATTRIBUTE,
                    name);
        }
    }

    private void throwIfDeletedFlagMismatch(boolean v1, boolean v2) {
        if (v1 != v2) {
            throw new DataProcessingException("Bulk requests to cached fields cannot have different 'deleted' policies.",
                    DataExceptionIds.EX_DATA_RECORD_LOOKUP_CACHE_DISTINCT_DELETED_STATE);
        }
    }

    private void throwIfInactiveFlagMismatch(boolean v1, boolean v2) {
        if (v1 != v2) {
            throw new DataProcessingException("Bulk requests to cached fields cannot have different 'inactive' policies.",
                    DataExceptionIds.EX_DATA_RECORD_LOOKUP_CACHE_DISTINCT_INACTIVE_STATE);
        }
    }
}
