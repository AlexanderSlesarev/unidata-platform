/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.impl;

import java.io.Serializable;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.context.DisplayNameResolutionContext;
import org.unidata.mdm.core.dto.DisplayNameResolutionResult;
import org.unidata.mdm.core.service.DisplayNameService;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.display.DisplayNameResolver;
import org.unidata.mdm.core.type.display.DisplayableField;
import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.model.EnumerationElement;
import org.unidata.mdm.core.type.model.LookupElement;
import org.unidata.mdm.core.type.model.MeasurementCategoryElement;
import org.unidata.mdm.data.configuration.DataConfigurationConstants;
import org.unidata.mdm.data.configuration.DataNamespace;
import org.unidata.mdm.data.context.FieldsQueryContext;
import org.unidata.mdm.data.dto.FieldsQueryResult;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.FieldsCacheService;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.search.RecordHeaderField;
import org.unidata.mdm.meta.type.search.RelationHeaderField;
import org.unidata.mdm.search.util.SearchUtils;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.service.TextService;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Dmitriy Bobrov on Nov 12, 2021
 */
public abstract class AbstractRecordDisplayNameResolver implements DisplayNameResolver {
    /**
     * No display values for a record.
     */
    private static final String LABEL_DISPLAY_VALUES_NOT_FOUND = DataModule.MODULE_ID + ".display.values.not.found";
    /**
     * No display values for a field.
     */
    private static final String LABEL_DISPLAY_VALUE_NOT_FOUND = DataModule.MODULE_ID + ".display.value.not.found";
    /**
     * Ambiguous resolution result. Two or more records match search condition.
     */
    private static final String LABEL_DISPLAY_AMBIGUOUS_RESOLUTION_RESULT = DataModule.MODULE_ID + ".display.ambguous.resolution.result";
    /**
     * Requested period not covered with data.
     */
    private static final String LABEL_DISPLAY_REQUESTED_PERIOD_NOT_COVERED = DataModule.MODULE_ID + ".display.requested.period.not.covered";
    /**
     * Cannot render display data - attribute 'name' not found in model.
     */
    private static final String LABEL_DISPLAY_RENDER_ATTRIBUTE_NOT_FOUND = DataModule.MODULE_ID + ".display.render.attribute.not.found";

    @Autowired
    private MetaModelService metaModelService;

    @Autowired
    private FieldsCacheService fieldsCacheService;

    @Autowired
    private TextService textService;

    private final DisplayNameService displayNameService;

    @ConfigurationRef(DataConfigurationConstants.PROPERTY_INDEX_DATE_DISPLAY_FORMAT)
    private ConfigurationValue<DateTimeFormatter> dateDisplayFormatter;

    @ConfigurationRef(DataConfigurationConstants.PROPERTY_INDEX_TIME_DISPLAY_FORMAT)
    private ConfigurationValue<DateTimeFormatter> timeDisplayFormatter;

    @ConfigurationRef(DataConfigurationConstants.PROPERTY_INDEX_TIMESTAMP_DISPLAY_FORMAT)
    private ConfigurationValue<DateTimeFormatter> dateTimeDisplayFormatter;

    protected AbstractRecordDisplayNameResolver(DisplayNameService displayNameService) {
        super();
        this.displayNameService = displayNameService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DisplayNameResolutionResult resolve(DisplayNameResolutionContext ctx) {

        if (Objects.nonNull(ctx)) {

            List<DisplayNameResolutionResult> results = resolve(Collections.singletonList(ctx));
            return CollectionUtils.isEmpty(results) ? null : results.get(0);
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public List<DisplayNameResolutionResult> resolve(List<DisplayNameResolutionContext> input) {

        if (CollectionUtils.isNotEmpty(input)) {

            DisplayNamesResolutionState state = new DisplayNamesResolutionState();

            // 1. Prepare requests
            for (DisplayNameResolutionContext c : input) {
                state.appendRequest(c);
            }

            // 2. Run query
            Collection<FieldsQueryResult> direct = fieldsCacheService.fetch(state.toQueries());

            // 3. Collect direct result
            for (FieldsQueryResult fqr : direct) {
                state.appendResult(fqr);
            }

            // 4. Run backrefs, if collected
            List<DisplayNameResolutionResult> backrefs = displayNameService.resolve(state.toBackrefs());
            for (DisplayNameResolutionResult backref : backrefs) {
                state.appendBackref(backref);
            }

            // 5. Return result
            return state.toResults();
        }

        return Collections.emptyList();
    }

    private class DisplayNamesResolutionState {
        /*
         * [entity name : [lookup object : result (and also intermediate storage)]]
         */
        private Map<String, Map<Object, DisplayNameResolutionResult>> dm;
        /*
         * Back references for lookups names resolution.
         */
        private Map<String, Map<Object, List<Triple<Date, DisplayableField, Date>>>> bm;
        /*
         * Forward queries.
         */
        private List<FieldsQueryContext> dq;
        /*
         * Backref contexts.
         */
        private List<DisplayNameResolutionContext> bq;

        public DisplayNamesResolutionState() {
            super();
        }

        public void appendRequest(DisplayNameResolutionContext ctx) {

            if (Objects.isNull(ctx)) {
                return;
            }

            EntityElement el = metaModelService.instance(Descriptors.DATA)
                    .getElement(ctx.getTypeName());

            ensureEntityElement(el, ctx.getTypeName());

            if (Objects.isNull(dm)) {
                dm = new HashMap<>();
            }

            dm.computeIfAbsent(ctx.getTypeName(), k -> new HashMap<>())
                .put(ctx.getSubject(), new DisplayNameResolutionResult(ctx));

            // Default set of display attributes will be taken by FCS.
            FieldsQueryContext i = FieldsQueryContext.builder()
                .elementName(ctx.getTypeName())
                .lookupField(selectLookupField(el, ctx))
                .lookupValue(ctx.getSubject())
                .deleted(ctx.isDeleted())
                .inactive(ctx.isInactive())
                .build();

            if (Objects.isNull(dq)) {
                dq = new ArrayList<>();
            }

            dq.add(i);
        }

        public void appendResult(FieldsQueryResult fqr) {

            if (fqr.isEmpty()) {
                return;
            }

            EntityElement el = metaModelService.instance(Descriptors.DATA)
                    .getElement(fqr.getTypeName());

            DisplayNameResolutionResult target = dm
                    .get(el.getName())
                    .get(fqr.getValue());

            // Huh.
            if (Objects.isNull(target)) {
                return;
            }

            // Select period. Exit on 'no data'
            MultiValuedMap<String, Serializable> fields = ensureFields(fqr, target);
            if (Objects.isNull(fields)) {
                return;
            }

            Set<String> fieldNames = target.getFieldNames();
            if (CollectionUtils.isEmpty(fieldNames)) {
                // LHM, with fixed iteration order as in the model
                fieldNames = el.getMainDisplayableAttributes().keySet();
            }

            for (String fieldName : fieldNames) {

                AttributeElement ael = el.getAttributes().get(fieldName);
                if (Objects.isNull(ael)) {
                    continue;
                }

                Collection<Serializable> content = fields.get(ael.getIndexed().getPath());
                if (ael.isSimple()) {
                    appendSimple(target, content, ael);
                } else if (ael.isCode()) {
                    appendCode(target, content, ael);
                } else if (ael.isArray()) {
                    appendArray(target, content, ael);
                }
            }
        }

        public void appendBackref(DisplayNameResolutionResult br) {

            Map<Object, List<Triple<Date, DisplayableField, Date>>> om = bm.get(br.getTypeName());
            if (MapUtils.isEmpty(om)) {
                return;
            }

            List<Triple<Date, DisplayableField, Date>> refs = om.get(br.getSubject());
            if (CollectionUtils.isEmpty(refs)) {
                return;
            }

            Date validFrom = br.getValidFrom();
            Date validTo = br.getValidTo();

            ListIterator<Triple<Date, DisplayableField, Date>> li = refs.listIterator();
            while (li.hasNext()) {

                // Check this tripple <from | val | to> overlapps with result being processed.
                // Use the value, if so.
                Triple<Date, DisplayableField, Date> df = li.next();

                boolean left = validTo == null || df.getLeft() == null || !df.getLeft().after(validTo);
                boolean right = validFrom == null || df.getRight() == null || !df.getRight().before(validFrom);

                if (left && right) {

                    df.getMiddle()
                      .getDisplayValues()
                      .add(br.getDisplayValue());

                    // Remove self after value is processed,
                    // allowing to clear value lists faster for repetitive values and periods
                    li.remove();
                }
            }

            // Clear stuff, if not needed any more to prevent further executions
            if (CollectionUtils.isEmpty(refs)) {
                om.remove(br.getSubject());
            }

            // Clear stuff, if not needed any more to prevent further executions
            if (MapUtils.isEmpty(om)) {
                bm.remove(br.getTypeName());
            }
        }

        public List<FieldsQueryContext> toQueries() {
            return Objects.isNull(dq) ? Collections.emptyList() : dq;
        }

        public List<DisplayNameResolutionResult> toResults() {
            return dm.values().stream()
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .map(Entry::getValue)
                .map(this::filterResult)
                .collect(Collectors.toList());
        }

        public List<DisplayNameResolutionContext> toBackrefs() {
            return Objects.isNull(bq) ? Collections.emptyList() : bq;
        }

        private String selectLookupField(EntityElement el, DisplayNameResolutionContext ctx) {

            // 0. With record id
            if (StringUtils.isBlank(ctx.getQualifier())) {

                return el.isRelation()
                        ? RelationHeaderField.FIELD_ETALON_ID.getName()
                        : RecordHeaderField.FIELD_ETALON_ID.getName();
            }

            // 2. With qualifier
            AttributeElement ael = el.getAttributes().get(ctx.getQualifier());

            ensureQueryAttribute(el, ael, ctx.getQualifier());

            return ael.getIndexed().getPath();
        }
        /*
         * Add simple attribute
         */
        private void appendSimple(DisplayNameResolutionResult target, Collection<Serializable> values, AttributeElement el) {

            DisplayableField df = new DisplayableField();
            df.setName(el.getName());
            df.setDisplayName(el.getDisplayName());

            List<Object> raw = new ArrayList<>(values.size());
            List<String> formatted = new ArrayList<>(values.size());

            // Single value
            for (Serializable v : values) {

                if (Objects.isNull(v)) {
                    continue;
                }

                raw.add(v);

                if (el.isEnumValue()) {

                    EnumerationElement en = metaModelService
                            .instance(Descriptors.ENUMERATIONS)
                            .getEnumeration(el.getEnumName());

                    formatted.add(en.getEnumerationValue(v.toString()).getDisplayName());

                } else if (el.isMeasured()) {

                    MeasurementCategoryElement mc = metaModelService
                            .instance(Descriptors.MEASUREMENT_UNITS)
                            .getCategory(el.getMeasured().getCategoryId());

                    formatted.add(v.toString() + " " + mc.getBaseUnit().getDisplayName());

                } else if (el.isLookupLink()) {
                    appendLookup(target, el, v, df);
                } else if (el.isDate()) {
                    formatted.add(filterTemporalField(v.toString(), el));
                } else {
                    formatted.add(v.toString());
                }
            }

            df.setValues(raw);
            df.setDisplayValues(formatted);

            target.addField(df);
        }
        /*
         * Code can be either string or int.
         * Ok with default toString
         */
        private void appendCode(DisplayNameResolutionResult target, Collection<Serializable> values, AttributeElement el) {

            DisplayableField df = new DisplayableField();
            df.setName(el.getName());
            df.setDisplayName(el.getDisplayName());
            df.setValues(new ArrayList<>(values));
            df.setDisplayValues(values.stream()
                    .filter(Objects::nonNull)
                    .map(Object::toString)
                    .collect(Collectors.toList()));

            List<Object> raw = new ArrayList<>(values.size());
            List<String> formatted = new ArrayList<>(values.size());

            // Single value
            for (Serializable v : values) {

                if (Objects.isNull(v)) {
                    continue;
                }

                raw.add(v);

                // Very rare OCRV case
                if (el.isLookupLink()) {
                    appendLookup(target, el, v, df);
                } else {
                    formatted.add(v.toString());
                }
            }

            df.setValues(raw);
            df.setDisplayValues(formatted);

            target.addField(df);
        }
        /*
         * Appends array
         */
        private void appendArray(DisplayNameResolutionResult target, Collection<Serializable> values, AttributeElement el) {

            DisplayableField df = new DisplayableField();
            df.setName(el.getName());
            df.setDisplayName(el.getDisplayName());

            List<Object> raw = new ArrayList<>(values.size());
            List<String> formatted = new ArrayList<>(values.size());

            for (Serializable v : values) {

                if (Objects.isNull(v)) {
                    continue;
                }

                raw.add(v);

                if (el.isLookupLink()) {
                    appendLookup(target, el, v, df);
                } else if (el.isDate()) {
                    formatted.add(filterTemporalField(v.toString(), el));
                } else {
                    formatted.add(v.toString());
                }
            }

            df.setValues(raw);
            df.setDisplayValues(formatted);

            target.addField(df);
        }

        private void appendLookup(DisplayNameResolutionResult target, AttributeElement el, Serializable value, DisplayableField df) {

            if (Objects.isNull(bm)) {
                bm = new HashMap<>();
            }

            LookupElement le = metaModelService.instance(Descriptors.DATA)
                    .getLookup(el.getLookupLink().getLookupLinkName());

            bm.computeIfAbsent(le.getName(), k -> new HashMap<>())
              .computeIfAbsent(value, k -> new ArrayList<>())
              .add(Triple.of(target.getValidFrom(), df, target.getValidTo()));

            DisplayNameResolutionContext i = DisplayNameResolutionContext.builder()
                    .nameSpace(DataNamespace.LOOKUP)
                    .typeName(le.getName())
                    .qualifier(le.getCodeAttribute().getName())
                    .subject(value)
                    .deleted(target.isDeleted())
                    .inactive(target.isInactive())
                    .validFrom(target.getValidFrom())
                    .validTo(target.getValidTo())
                    .fields(el.getLookupLink().getPresentation().getDisplayAttributes())
                    .build();

            if (Objects.isNull(bq)) {
                bq = new ArrayList<>();
            }

            bq.add(i);
        }

        private String filterTemporalField(String s, AttributeElement el) {

            if (el.isDate()) {
                switch (el.getValueType()) {
                case DATE:
                    return dateDisplayFormatter.getValue().format(DateTimeFormatter.ISO_LOCAL_DATE.parse(s));
                case TIME:
                    return timeDisplayFormatter.getValue().format(DateTimeFormatter.ISO_LOCAL_TIME.parse(s));
                case TIMESTAMP:
                    return SearchUtils.formatIndexTimestamp(s, dateTimeDisplayFormatter.getValue());
                default:
                    break;
                }
            }

            return s;
        }

        private DisplayNameResolutionResult filterResult(DisplayNameResolutionResult in) {

            if (!in.hasFields()) {
                in.setDisplayValue(textService.getText(LABEL_DISPLAY_VALUES_NOT_FOUND));
            } else {

                EntityElement el = metaModelService.instance(Descriptors.DATA)
                        .getElement(in.getTypeName());

                ensureEntityElement(el, in.getTypeName());

                in.setDisplayValue(in.getFields().stream()
                        .filter(Objects::nonNull)
                        .map(f -> printField(el.getAttributes().get(f.getName()), f))
                        .filter(StringUtils::isNotBlank)
                        .collect(Collectors.joining(", ")));
            }

            return in;
        }

        private String printField(AttributeElement ael, DisplayableField f) {

            if (Objects.isNull(ael)) {
                return textService.getText(LABEL_DISPLAY_RENDER_ATTRIBUTE_NOT_FOUND, f.getName());
            }

            if (!f.getDisplayValues().isEmpty()) {

                if (f.getDisplayValues().size() == 1) {
                    return ael.isLookupLink()
                            ? "(" + f.getDisplayValues().get(0) + ")"
                            : f.getDisplayValues().get(0);
               } else {
                    String a = f.getDisplayValues().stream()
                            .filter(StringUtils::isNotBlank)
                            .map(s -> (ael.isLookupLink() ? "(" + s + ")" : s))
                            .collect(Collectors.joining(", "));
                    return "[" + a + "]";
                }
            }

            return textService.getText(LABEL_DISPLAY_VALUE_NOT_FOUND, ael.getDisplayName());
        }

        private MultiValuedMap<String, Serializable> ensureFields(FieldsQueryResult fqr, DisplayNameResolutionResult target) {

            if (!fqr.isSingleton()) {
                target.setDisplayValue(textService.getText(LABEL_DISPLAY_AMBIGUOUS_RESOLUTION_RESULT));
                return null;
            }

            // Set target ID.
            target.setId(fqr.getKeys().iterator().next());

            // Select period
            MultiValuedMap<String, Serializable> fields = fqr
                    .getSingleton()
                    .getFieldsFor(target.getValidFrom(), target.getValidTo());

            if (Objects.isNull(fields)) {
                target.setDisplayValue(textService.getText(LABEL_DISPLAY_REQUESTED_PERIOD_NOT_COVERED,
                        ConvertUtils.date2String(target.getValidFrom()),
                        ConvertUtils.date2String(target.getValidTo())));
                return null;
            }

            return fields;
        }

        private void ensureEntityElement(EntityElement el, String name) {
            if (Objects.isNull(el)) {
                throw new PlatformFailureException("Entity element not found by name [{}].",
                        DataExceptionIds.EX_DATA_DISPLAY_NAME_ENTITY_NOT_FOUND, name);
            }
        }

        private void ensureQueryAttribute(EntityElement el, AttributeElement ael, String name) {
            if (Objects.isNull(ael)) {
                throw new PlatformFailureException("Atribute element [{}] not found by name in [{}].",
                        DataExceptionIds.EX_DATA_DISPLAY_NAME_ATTRIBUTE_NOT_FOUND, name, el.getName());
            } else if (!(ael.isCode() || ael.isCodeAlternative() || ael.isUnique())) {
                throw new PlatformFailureException("Attribute [{}] in [{}] is not code, code alternative or unique value attribute.",
                        DataExceptionIds.EX_DATA_DISPLAY_NAME_ATTRIBUTE_ILLEGAL, name, el.getName());
            }
        }
    }
}
