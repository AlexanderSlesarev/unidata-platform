/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.type.draft;

/**
 * @author Mikhail Mikhailov on Apr 26, 2021
 *
 */
public class DataDraftParameters {
    /**
     * Subject entity (register, lookup or relation) name.
     */
    public static final String ENTITY_NAME = "entity-name";
    /**
     * The data operation (UPSERT, RESTORE, DELETE, etc.).
     */
    public static final String DRAFT_OPERATION = "draft-operation";
    /**
     * Postponed subject ID update param.
     */
    public static final String SUBJECT_ID = "subject-id";
    /**
     * Constructor.
     */
    private DataDraftParameters() {
        super();
    }
}
