/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations.upsert;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.LargeObjectsService;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.data.ArrayAttribute;
import org.unidata.mdm.core.type.data.ComplexAttribute;
import org.unidata.mdm.core.type.data.SimpleAttribute;
import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.model.LookupElement;
import org.unidata.mdm.core.type.model.NestedElement;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.FieldsQueryContext;
import org.unidata.mdm.data.context.UpsertRelationRequestContext;
import org.unidata.mdm.data.dto.FieldsQueryResult;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.FieldsCacheService;
import org.unidata.mdm.data.type.data.EtalonRelation;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.data.RelationType;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.data.type.snapshot.FieldsSnapshot;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.context.ConsistencyCheckContext;
import org.unidata.mdm.meta.service.ConsistencyCheckSupport;
import org.unidata.mdm.system.exception.PlatformValidationException;
import org.unidata.mdm.system.exception.ValidationResult;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.util.ConvertUtils;

@Component(RelationUpsertValidateExecutor.SEGMENT_ID)
public class RelationUpsertValidateExecutor extends Point<UpsertRelationRequestContext> implements ConsistencyCheckSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_UPSERT_VALIDATE]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relations.upsert.validate.description";
    /**
     * Message.
     */
    private static final String EX_DATA_RELATION_VALIDATION_PERIOD_INCONSISTENT = DataModule.MODULE_ID + ".relation.validation.period.inconsistent";
    /**
     * Unique value is already used by another record.
     */
    public static final String EX_RELATION_ATTRIBUTE_UNIQUE_VALUE_ALREADY_USED = DataModule.MODULE_ID + ".relation.attribute.unique.value.already.used";
    /**
     * Code value, referenced by attribute, is missing in the target lookup.
     */
    public static final String EX_RELATION_ATTRIBUTE_CODE_REFERENCED_VALUE_MISSING = DataModule.MODULE_ID + ".relation.attribute.code.referenced.value.missing";
    /**
     * Code value, does not cover upsert period.
     */
    public static final String EX_RELATION_ATTRIBUTE_CODE_VALUE_DOES_NOT_COVER = DataModule.MODULE_ID + ".relation.attribute.code.value.does.not.cover";
    /**
     * Ambigous value distribution. More then one record holds the same code value.
     */
    public static final String EX_RELATION_ATTRIBUTE_AMBIGOUS_CODE_VALUE_DISTRIBUTION = DataModule.MODULE_ID + ".relation.attribute.ambigous.code.value.distribution";
    /**
     * MetaModel service.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * The LS.
     */
    @Autowired
    private FieldsCacheService fieldsCacheService;
    /**
     * LOB component.
     */
    @Autowired
    private LargeObjectsService lobComponent;
    /**
     * Constructor.
     */
    public RelationUpsertValidateExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    @Override
    public void point(UpsertRelationRequestContext ctx) {

        // Containments are checked by record upsert.
        if (ctx.relationType() == RelationType.CONTAINS) {
            return;
        }

        // Check record validity
        Timeline<OriginRelation> next = ctx.nextTimeline();
        List<TimeInterval<OriginRelation>> affected = next.selectBy(ctx.getValidFrom(), ctx.getValidTo());
        EntityElement el = metaModelService.instance(Descriptors.DATA).getElement(next.<RelationKeys>getKeys().getRelationName());

        Map<TimeInterval<OriginRelation>, ConsistencyCheckContext> collected = new HashMap<>();
        for (TimeInterval<OriginRelation> hit : affected) {

            EtalonRelation er = hit.getCalculationResult();
            if (Objects.isNull(er)) {
                continue;
            }

            ConsistencyCheckContext check = ConsistencyCheckContext.builder()
                    .data(er)
                    .entity(el)
                    .payload(er)
                    .build();

            check(check);

            if (!check.valid()) {
                collected.put(hit, check);
            }
        }

        if (MapUtils.isNotEmpty(collected)) {

            List<ValidationResult> periods = collected.entrySet().stream()
                .map(entry ->
                    new ValidationResult(
                        "Relation record for interval ({} - {}) is inconsistent with model.",
                        entry.getValue().validations(), EX_DATA_RELATION_VALIDATION_PERIOD_INCONSISTENT,
                        ConvertUtils.date2String(entry.getKey().getValidFrom()),
                        ConvertUtils.date2String(entry.getKey().getValidTo())))
                .collect(Collectors.toList());

            throw new PlatformValidationException("Relation upsert to {} rejected. Validation errors exist.",
                    DataExceptionIds.EX_DATA_RELATION_VALIDATION_UPSERT_REJECTED, periods, el.getName());
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void checkComplexAttribute(ConsistencyCheckContext check, ComplexAttribute attr, AttributeElement el) {

        ConsistencyCheckSupport.super.checkComplexAttribute(check, attr, el);
        if (attr.isEmpty()) {
            return;
        }

        // It is guaranteed, that an attribute can reference only existing objects.
        // So no check.
        NestedElement ne = metaModelService
              .instance(Descriptors.DATA)
              .getNested(el.getComplex().getNestedEntityName());

        check.offer(attr.toCollection(), ne);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void checkArrayAttribute(ConsistencyCheckContext check, ArrayAttribute<?> attr, AttributeElement el) {

        int memo = check.count();
        ConsistencyCheckSupport.super.checkArrayAttribute(check, attr, el);

        if (check.count() != memo) {
            return;
        }

        if (el.isLookupLink()) {

            EtalonRelation er = check.getPayload();
            List<Serializable> v = attr.getValues();

            for (Serializable vv : v) {
                checkLookupLink(check, el, er, vv);
            }
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void checkSimpleAttribute(ConsistencyCheckContext check, SimpleAttribute<?> attr, AttributeElement el) {

        int memo = check.count();
        ConsistencyCheckSupport.super.checkSimpleAttribute(check, attr, el);

        if (check.count() != memo) {
            return;
        }

        EtalonRelation er = check.getPayload();
        if (el.isLookupLink()) {
            checkLookupLink(check, el, er, attr.castValue());
        }

        if (el.isUnique()) {
            checkUnique(check, el, er, attr.castValue());
        }
    }

    private void checkLookupLink(ConsistencyCheckContext check, AttributeElement el, EtalonRelation er, Serializable v) {

        LookupElement ref = metaModelService
                .instance(Descriptors.DATA)
                .getLookup(el.getLookupLink().getLookupLinkName());

        FieldsQueryResult lqr = fieldsCacheService.fetch(FieldsQueryContext.builder()
                .lookupValue(v)
                .lookupField(ref.getCodeAttribute().getName())
                .elementName(ref.getName())
                .build());

        if (lqr.isEmpty()) {
            check.append(new ValidationResult("Code value {}, referenced by relation attribute {}, is missing in the target lookup {}.",
                    EX_RELATION_ATTRIBUTE_CODE_REFERENCED_VALUE_MISSING,
                    v, el.getName(), ref.getName()));
        } else {

            ensureSingleton(check, lqr, v);

            FieldsSnapshot fs = lqr.getSingleton();
            if (fs.getFieldsFor(er.getInfoSection().getValidFrom(), er.getInfoSection().getValidTo()) == null) {

                check.append(new ValidationResult("Code value {}, referenced by relation attribute {}, does not cover upsert period ({} - {}).",
                        EX_RELATION_ATTRIBUTE_CODE_VALUE_DOES_NOT_COVER,
                        v,
                        el.getName(),
                        ConvertUtils.date2String(er.getInfoSection().getValidFrom()),
                        ConvertUtils.date2String(er.getInfoSection().getValidTo())));
            }
        }
    }

    private void checkUnique(ConsistencyCheckContext check, AttributeElement el, EtalonRelation er, Serializable v) {

        EntityElement eel = check.getEntity();

        FieldsQueryResult lqr = fieldsCacheService.fetch(FieldsQueryContext.builder()
                .lookupValue(v)
                .lookupField(el.getPath())
                .elementName(eel.getName())
                .build());

        if (!lqr.isEmpty()) {

            ensureSingleton(check, lqr, v);

            if (!lqr.hasFields(er.getInfoSection().getRelationEtalonKey())) {
                check.append(new ValidationResult("Unique value ({}) is already used by another relation record ({}, {}).",
                        EX_RELATION_ATTRIBUTE_UNIQUE_VALUE_ALREADY_USED,
                        v, lqr.getKeys().iterator().next(), el.getPath()));
            }
        }
    }

    private void ensureSingleton(ConsistencyCheckContext check, FieldsQueryResult lqr, Object v) {

        if (!lqr.isSingleton()) {
            check.append(new ValidationResult(
                    "Ambiguous value distribution. More than one active relation holds the same code / unique value ({}).",
                    EX_RELATION_ATTRIBUTE_AMBIGOUS_CODE_VALUE_DISTRIBUTION, v));
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MetaModelService metaModelService() {
        return metaModelService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public LargeObjectsService lobComponent() {
        return lobComponent;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return UpsertRelationRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
