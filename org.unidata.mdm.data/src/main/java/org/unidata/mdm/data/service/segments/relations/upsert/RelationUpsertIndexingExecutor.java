/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations.upsert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.UpsertRelationRequestContext;
import org.unidata.mdm.data.context.UpsertRelationRequestContext.UpsertRelationHint;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.impl.RelationsIndexingComponent;
import org.unidata.mdm.data.type.apply.RelationUpsertChangeSet;
import org.unidata.mdm.data.type.data.EtalonRelation;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.data.RelationType;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.meta.type.RelativeDirection;
import org.unidata.mdm.meta.type.search.RelationFromIndexId;
import org.unidata.mdm.meta.type.search.RelationToIndexId;
import org.unidata.mdm.search.context.IndexRequestContext;
import org.unidata.mdm.search.type.id.ManagedIndexId;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov on Dec 8, 2019
 */
@Component(RelationUpsertIndexingExecutor.SEGMENT_ID)
public class RelationUpsertIndexingExecutor extends Point<UpsertRelationRequestContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_UPSERT_INDEXING]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.upsert.indexing.description";
    /**
     * RIC.
     */
    @Autowired
    private RelationsIndexingComponent relationsIndexingComponent;
    /**
     * Constructor.
     */
    public RelationUpsertIndexingExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(UpsertRelationRequestContext ctx) {

        MeasurementPoint.start();
        try {

            // 1. Upsert etalon
            if (ctx.relationType() != RelationType.REFERENCES) {
                processRelToOrContainmentIndexing(ctx);
            } else {
                processReferenceIndexing(ctx);
            }

        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return UpsertRelationRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }

    private void processReferenceIndexing(UpsertRelationRequestContext ctx) {

        List<Timeline<OriginRelation>> oldVirtual = ctx.previousReferences();
        List<Timeline<OriginRelation>> newVirtual = ctx.nextReferences();

        if (CollectionUtils.isEmpty(newVirtual)) {
            return;
        }

        Timeline<OriginRelation> next = ctx.nextTimeline();
        RelationKeys keys = next.getKeys();
        RelationUpsertChangeSet set = ctx.changeSet();

        List<ManagedIndexId> deletes = new ArrayList<>(oldVirtual.size());
        List<TimeInterval<OriginRelation>> updates = new ArrayList<>(newVirtual.size());

        RelativeDirection pd = ctx.getHint(UpsertRelationHint.HINT_PROCESSING_SIDE);

        // Deletes
        if (!ctx.isSkipIndexDrop()) {

            for (Timeline<OriginRelation> timeline : oldVirtual) {
                for (TimeInterval<OriginRelation> interval : timeline) {
                    delete(deletes, interval.getCalculationResult(), pd);
                }
            }
        }

        // Updates
        for (Timeline<OriginRelation> timeline : newVirtual) {
            for (TimeInterval<OriginRelation> interval : timeline) {
                updates.add(interval);
            }
        }

        set.getIndexRequestContexts().add(IndexRequestContext.builder()
                .index(relationsIndexingComponent.build(keys, updates, pd))
                .delete(deletes)
                .drop(!deletes.isEmpty())
                .entity(keys.getFromEntityName())
                .build());
    }

    private void processRelToOrContainmentIndexing(UpsertRelationRequestContext ctx) {

        RelationUpsertChangeSet set = ctx.changeSet();
        Timeline<OriginRelation> prev = ctx.currentTimeline();
        Timeline<OriginRelation> next = ctx.nextTimeline();
        RelationKeys keys = next.getKeys();

        Date from = ctx.relationType() == RelationType.CONTAINS ? ctx.containmentContext().getValidFrom() : ctx.getValidFrom();
        Date to = ctx.relationType() == RelationType.CONTAINS ? ctx.containmentContext().getValidTo() : ctx.getValidTo();

        prev = prev.reduceBy(from, to);

        List<ManagedIndexId> deletes = new ArrayList<>(prev.size());

        RelativeDirection pd = ctx.getHint(UpsertRelationHint.HINT_PROCESSING_SIDE);

        // Deletes
        if (!ctx.isSkipIndexDrop()) {
            for (TimeInterval<OriginRelation> interval : prev) {
                delete(deletes, interval.getCalculationResult(), pd);
            }
        }

        IndexRequestContext context = IndexRequestContext.builder()
                .index(next.isEmpty() ? Collections.emptyList() : relationsIndexingComponent.build(keys, next, pd))
                .delete(deletes)
                .drop(!deletes.isEmpty())
                .entity(keys.getFromEntityName())
                .build();

        set.getIndexRequestContexts().add(context);
    }

    private void delete(List<ManagedIndexId> deletes, EtalonRelation etalon, RelativeDirection processing) {

        if (Objects.isNull(etalon)) {
            return;
        }

        if (processing == null || processing == RelativeDirection.FROM) {

            deletes.add(RelationFromIndexId.of(
                    etalon.getInfoSection().getFromEntityName(),
                    etalon.getInfoSection().getRelationName(),
                    etalon.getInfoSection().getFromEtalonKey().getId(),
                    etalon.getInfoSection().getToEtalonKey().getId(),
                    etalon.getInfoSection().getValidTo()));
        }

        if (processing == null || processing == RelativeDirection.TO) {

            deletes.add(RelationToIndexId.of(
                    etalon.getInfoSection().getToEntityName(),
                    etalon.getInfoSection().getRelationName(),
                    etalon.getInfoSection().getFromEtalonKey().getId(),
                    etalon.getInfoSection().getToEtalonKey().getId(),
                    etalon.getInfoSection().getValidTo()));
        }
    }
}
