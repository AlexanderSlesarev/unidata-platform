/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.impl.cache;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.commons.collections4.CollectionUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.meta.type.search.EntityIndexType;
import org.unidata.mdm.meta.type.search.RecordHeaderField;
import org.unidata.mdm.meta.type.search.RelationHeaderField;
import org.unidata.mdm.search.configuration.SearchConfigurationProperty;
import org.unidata.mdm.search.context.SearchRequestContext;
import org.unidata.mdm.search.dto.SearchResultDTO;
import org.unidata.mdm.search.dto.SearchResultHitDTO;
import org.unidata.mdm.search.service.SearchService;
import org.unidata.mdm.search.type.query.SearchQuery;
import org.unidata.mdm.search.type.sort.SortField;
import org.unidata.mdm.search.type.sort.SortField.SortOrder;
import org.unidata.mdm.system.configuration.ModuleConfiguration;
import org.unidata.mdm.system.service.RuntimePropertiesService;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;

import com.hazelcast.map.MapStoreAdapter;

/**
 * @author Mikhail Mikhailov on Nov 25, 2021
 * The HZ store.
 */
public class FieldsCacheStore extends MapStoreAdapter<FieldsCacheKey, FieldsCacheValue> {
    /**
     * SS instance.
     */
    private static SearchService searchService;
    /**
     * MMS instance.
     */
    private static MetaModelService metaModelService;
    /**
     * Max window size property.
     */
    private static ConfigurationValue<Long> maxWindowSize;
    /**
     * Static initializer.
     * Needed to overcome cluster early store initialization, when contexts are not available yet.
     */
    public static void init() {
        searchService = ModuleConfiguration.getBean(SearchService.class);
        metaModelService = ModuleConfiguration.getBean(MetaModelService.class);

        RuntimePropertiesService rps = ModuleConfiguration.getBean(RuntimePropertiesService.class);
        maxWindowSize = rps.getByProperty(SearchConfigurationProperty.SEARCH_HITS_LIMIT);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public FieldsCacheValue load(FieldsCacheKey key) {

        // 0. Init
        FieldsQueryState state = new FieldsQueryState(metaModelService, key.getElementName(), key.getLookupField());

        // 1. Append single key
        state.appendKey(key);

        // 2. Create context
        EntityElement el = state.getEntityElement();
        SearchRequestContext ctx = SearchRequestContext.builder(el.isRelation()
                ? EntityIndexType.RELATION
                : EntityIndexType.RECORD, el.isRelation()
                ? el.getRelation().getLeft().getName()
                : el.getName())
            .fetchAll(true)
            .filter(SearchQuery.formQuery(state.toSearchGroup()))
            .returnFields(state.toReturnFields())
            .count(maxWindowSize.getValue().intValue())
            .sorting(sorting(el))
            .build();

        // 3. Run search
        SearchResultDTO result = searchService.search(ctx);

        // 4. Process hits
        for (SearchResultHitDTO hit : result.getHits()) {
            state.appendHit(hit);
        }

        // 5. Return result
        return state.toResult().get(key);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public Map<FieldsCacheKey, FieldsCacheValue> loadAll(Collection<FieldsCacheKey> bulk) {

        if (CollectionUtils.isEmpty(bulk)) {
            return Collections.emptyMap();
        }

        Map<FieldsCacheKey, FieldsCacheValue> retval = new HashMap<>();
        bulk.stream()
            .collect(Collectors.groupingBy(FieldsCacheKey::getElementName))
            .forEach((elementName, keys1) ->

                keys1.stream()
                    .collect(Collectors.groupingBy(FieldsCacheKey::getLookupField))
                    .forEach((fieldName, keys2) -> {

                        // 0. Init
                        FieldsQueryState state = new FieldsQueryState(metaModelService, elementName, fieldName);

                        // 1. Memorize keys
                        for (FieldsCacheKey key : keys2) {
                            state.appendKey(key);
                        }

                        // 2. Create context
                        EntityElement el = state.getEntityElement();
                        SearchRequestContext ctx = SearchRequestContext.builder(el.isRelation()
                                ? EntityIndexType.RELATION
                                : EntityIndexType.RECORD, el.isRelation()
                                ? el.getRelation().getLeft().getName()
                                : el.getName())
                            .fetchAll(true)
                            .filter(SearchQuery.formQuery(state.toSearchGroup()))
                            .returnFields(state.toReturnFields())
                            .count(maxWindowSize.getValue().intValue())
                            .sorting(sorting(el))
                            .build();

                        // 3. Search
                        SearchResultDTO result = searchService.search(ctx);

                        // 4. Process hits
                        for (SearchResultHitDTO hit : result.getHits()) {
                            state.appendHit(hit);
                        }

                        // 5. Save result
                        retval.putAll(state.toResult());
                    })
            );

        return retval;
    }

    private Collection<SortField> sorting(EntityElement el) {
        return Arrays.asList(SortField.of(el.isRelation() ? RelationHeaderField.FIELD_FROM : RecordHeaderField.FIELD_FROM, SortOrder.ASC));
    }
}
