/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.impl.cache;

import java.io.Serializable;
import java.util.Objects;
import org.unidata.mdm.data.context.FieldsQueryContext;

/**
 * @author Mikhail Mikhailov on Nov 25, 2021
 * The cached key.
 */
public class FieldsCacheKey implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 6728301518308508839L;
    /**
     * Deleted records should be also considered (filetered out otherwise).
     */
    private static final int DELETED_FLAG = 1 << 0;
    /**
     * Inactive periods should be also considered (filetered out otherwise).
     */
    private static final int INACTIVE_FLAG = 1 << 1;
    /**
     * The lookup name.
     */
    private final String elementName;
    /**
     * The code attribute (either main or alternative).
     */
    private final String lookupField;
    /**
     * The value.
     */
    private final Serializable lookupValue;
    /**
     * Deleted + inactive.
     */
    private int flags;
    /**
     * Constructor.
     * @param ctx the context
     */
    public FieldsCacheKey(FieldsQueryContext ctx) {
        super();
        this.elementName = ctx.getElementName();
        this.lookupField = ctx.getLookupField();
        this.lookupValue = ctx.getLookupValue();

        if (ctx.isDeleted()) {
            this.flags |= DELETED_FLAG;
        }

        if (ctx.isInactive()) {
            this.flags |= INACTIVE_FLAG;
        }
    }
    /**
     * @return the element
     */
    public String getElementName() {
        return elementName;
    }
    /**
     * @return the lookupValue
     */
    public Serializable getLookupValue() {
        return lookupValue;
    }
    /**
     * @return the lookupField
     */
    public String getLookupField() {
        return lookupField;
    }
    /**
     * @return deleted state
     */
    public boolean lookupDeleted() {
        return (this.flags & DELETED_FLAG) != 0;
    }
    /**
     * @return iactive state
     */
    public boolean lookupInactive() {
        return (this.flags & INACTIVE_FLAG) != 0;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (elementName == null ? 0 : elementName.hashCode());
        result = prime * result + (lookupField == null ? 0 : lookupField.hashCode());
        result = prime * result + (lookupValue == null ? 0 : lookupValue.hashCode());
        result = prime * result + Boolean.hashCode(lookupDeleted());
        result = prime * result + Boolean.hashCode(lookupInactive());
        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        FieldsCacheKey other = (FieldsCacheKey) obj;
        if (!Objects.equals(elementName, other.elementName)) {
            return false;
        }

        if (!Objects.equals(lookupField, other.lookupField)) {
            return false;
        }

        if (!Objects.equals(lookupValue, other.lookupValue)) {
            return false;
        }

        return flags == other.flags;
    }
}
