/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.util.CollectionUtils.isEmpty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.unidata.mdm.core.context.DisplayNameResolutionContext;
import org.unidata.mdm.core.dto.DisplayNameResolutionResult;
import org.unidata.mdm.core.service.DisplayNameService;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.data.ArrayAttribute;
import org.unidata.mdm.core.type.data.ArrayValue;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.Attribute.AttributeType;
import org.unidata.mdm.core.type.data.AttributeIterator;
import org.unidata.mdm.core.type.data.CodeLinkValue;
import org.unidata.mdm.core.type.data.ComplexAttribute;
import org.unidata.mdm.core.type.data.DataFrame;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.DisplayValue;
import org.unidata.mdm.core.type.data.SimpleAttribute;
import org.unidata.mdm.core.type.data.SimpleAttribute.SimpleDataType;
import org.unidata.mdm.core.type.data.impl.AbstractSimpleAttribute;
import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.model.EnumerationElement;
import org.unidata.mdm.core.type.model.LookupLinkElement;
import org.unidata.mdm.data.configuration.DataNamespace;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.util.ModelUtils;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov
 *
 */
public interface AttributesPostProcessingSupport {
    /**
     * Link field.
     */
    static final Pattern LINK_FIELD_TEMPLATE = Pattern.compile("\\{[a-zA-Z][_\\-a-zA-Z0-9]*\\}");

    MetaModelService metaModelService();

    DisplayNameService displayNameService();
    /**
     * Does attributes post - processing -- sets display names, target ids, etc.
     * @param el the model element
     * @param data the data to process
     */
    default void process(EntityElement el, Collection<? extends DataFrame> data) {

        if (Objects.isNull(el) || CollectionUtils.isEmpty(data)) {
            return;
        }

        Map<String, AttributeElement> attrs = el.getAttributes();
        Map<String, AttributeElement> hits = attrs.entrySet()
                .stream()
                .filter(ent ->
                           ent.getValue().isLinkTemplate()
                        || ent.getValue().isLookupLink()
                        || ent.getValue().isEnumValue())
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

        // Have no attributes for post processing. Exit.
        if (MapUtils.isEmpty(hits)) {
            return;
        }

        data.stream()
            .filter(r -> Objects.nonNull(r) && !r.isEmpty())
            .forEach(r -> {
                filterAttributesNotFoundInModel(attrs, r, EMPTY);
                process(r, hits, r.getValidFrom(), r.getValidTo());
            });
    }
    /**
     * Process entry point.
     * @param r the record to process
     * @param linksEnumsOrCodeAttributes link templates, enums values, or code attribute references
     */
    private void process(DataRecord r, Map<String, AttributeElement> linksEnumsOrCodeAttributes, Date validFrom, Date validTo) {

        if (MapUtils.isEmpty(linksEnumsOrCodeAttributes)) {
            return;
        }

        List<DisplayNameResolutionContext> lookupResolutions = new ArrayList<>();
        Map<String, MultiValuedMap<Object, DisplayValue>> lookupLinks = new HashMap<>();
        Map<String, Set<Attribute>> enumValues = new HashMap<>();
        for (Entry<String, AttributeElement> attrEntry : linksEnumsOrCodeAttributes.entrySet()) {

            if (attrEntry.getValue().isLinkTemplate()) {

                // Historically, only 1st level is processed
                // Any previous attributes are just overwritten
                String transformed = processLinkTemplate(r, attrEntry.getValue().getLinkTemplate());
                r.addAttribute(AbstractSimpleAttribute.of(SimpleDataType.STRING, attrEntry.getKey(), transformed));

            } else if (attrEntry.getValue().isLookupLink()) {
                selectLookupLinks(r, attrEntry.getValue(), lookupResolutions, lookupLinks, validFrom, validTo);
            } else if (attrEntry.getValue().isEnumValue()) {
                enumValues
                    .computeIfAbsent(attrEntry.getValue().getEnumName(), k -> new HashSet<>())
                    .addAll(r.getAttributeRecursive(attrEntry.getKey()));
            }
        }

        processLookupLinks(lookupResolutions, lookupLinks);
        processEnumDisplayValues(enumValues);
    }

    private void selectLookupLinks(DataRecord r,
            AttributeElement ae,
            List<DisplayNameResolutionContext> lookupResolutions,
            Map<String, MultiValuedMap<Object, DisplayValue>> lookupLinks,
            Date validFrom, Date validTo) {

        LookupLinkElement lle = ae.getLookupLink();

        Collection<Attribute> attrs = r.getAttributeRecursive(ae.getPath());
        for (Attribute attr : attrs) {

            if (attr.isEmpty()) {
                continue;
            }

            if (attr.getAttributeType() == AttributeType.ARRAY) {

                ArrayAttribute<?> arrayAttribute = attr.narrow();

                AttributeElement ael = metaModelService()
                        .instance(Descriptors.DATA)
                        .getLookup(lle.getLookupLinkName())
                        .getCodeAttribute();

                for (ArrayValue<?> arrayValue : arrayAttribute) {

                    Serializable obj = arrayValue.castValue();
                    lookupResolutions.add(DisplayNameResolutionContext.builder()
                            .nameSpace(DataNamespace.LOOKUP)
                            .typeName(lle.getLookupLinkName())
                            .fields(lle.getPresentation().getDisplayAttributes())
                            .subject(obj)
                            .validFrom(validFrom)
                            .validTo(validTo)
                            .qualifier(ael.getName())
                            .build());

                    lookupLinks.computeIfAbsent(lle.getLookupLinkName(), k -> new ArrayListValuedHashMap<>())
                               .put(obj, arrayValue);
                }

            } else {

                SimpleAttribute<?> simpleAttribute = attr.narrow();

                AttributeElement ael = metaModelService()
                        .instance(Descriptors.DATA)
                        .getLookup(lle.getLookupLinkName())
                        .getCodeAttribute();

                Serializable obj = simpleAttribute.castValue();
                lookupResolutions.add(DisplayNameResolutionContext.builder()
                        .nameSpace(DataNamespace.LOOKUP)
                        .typeName(lle.getLookupLinkName())
                        .fields(lle.getPresentation().getDisplayAttributes())
                        .subject(obj)
                        .validFrom(validFrom)
                        .validTo(validTo)
                        .qualifier(ael.getName())
                        .build());

                lookupLinks.computeIfAbsent(lle.getLookupLinkName(), k -> new ArrayListValuedHashMap<>())
                           .put(obj, simpleAttribute);
            }
        }
    }

    private void processLookupLinks(List<DisplayNameResolutionContext> lookupResolutions,
            Map<String, MultiValuedMap<Object, DisplayValue>> lookupLinks) {

        if (CollectionUtils.isNotEmpty(lookupResolutions)) {

            List<DisplayNameResolutionResult> result = displayNameService().resolve(lookupResolutions);
            for (DisplayNameResolutionResult dnr : result) {

                MultiValuedMap<Object, DisplayValue> target = lookupLinks.get(dnr.getTypeName());
                if (target != null) {

                    target.get(dnr.getSubject()).forEach(dv -> {
                        dv.setDisplayValue(dnr.getDisplayValue());
                        ((CodeLinkValue) dv).setLinkEtalonId(dnr.getId());
                    });
                }
            }
        }
    }

    /**
     * Processes enum display values.
     * @param values value map with enum name key and attr values
     */
    @SuppressWarnings("unchecked")
    private void processEnumDisplayValues(Map<String, Set<Attribute>> values) {

        if (MapUtils.isEmpty(values)) {
            return;
        }

        MeasurementPoint.start();
        try {

            for (Entry<String, Set<Attribute>> entry : values.entrySet()) {

                if (isEmpty(entry.getValue())) {
                    continue;
                }

                EnumerationElement el = metaModelService()
                        .instance(Descriptors.ENUMERATIONS)
                        .getEnumeration(entry.getKey());

                entry.getValue().stream()
                    .map(attr -> (SimpleAttribute<String>) attr)
                    .filter(attr -> Objects.nonNull(attr.getValue()))
                    .forEach(attr -> attr.setDisplayValue(el.getEnumerationValue(attr.getValue()).getDisplayName()));
            }


        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * Actually processes the template and replaces place holders with value.
     * @param r the record
     * @param template the template
     * @return processed template
     */
    private String processLinkTemplate(DataRecord r, String template) {

        StringBuffer result = new StringBuffer();
        Matcher m = LINK_FIELD_TEMPLATE.matcher(template);

        while (m.find()) {
            int left = m.start();
            int right = m.end();

            String field = template.substring(left + 1, right - 1);
            String replacement = extractFieldValue(r, field);
            m.appendReplacement(result, Matcher.quoteReplacement(replacement));
        }
        m.appendTail(result);

        return result.toString();
    }

    /**
     * Extracts field value.
     * @param r the record
     * @param field the field
     * @return value or hint <invalid value for link> for attributes, that cannot be processed
     */
    private String extractFieldValue(DataRecord r, String field) {

        SimpleAttribute<?> attr = r.getSimpleAttribute(field);
        if (attr == null || attr.getValue() == null) {
            return EMPTY;
        }

        switch (attr.getDataType()) {
            case BOOLEAN:
                return Boolean.toString((Boolean) attr.getValue());
            case INTEGER:
                return Long.toString((Long) attr.getValue());
            case NUMBER:
                return Double.toString((Double) attr.getValue());
            case STRING:
                return attr.getValue().toString();
            default:
                return EMPTY;
        }
    }
    /**
     *
     * @param map - attribute map
     * @param r -  record
     * @param path - prefix path
     */
    private void filterAttributesNotFoundInModel(Map<String, AttributeElement> map, DataRecord r, String path) {

        if (map == null) {
            return;
        }

        AttributeIterator it = r.attributeIterator();
        while (it.hasNext()) {

            Attribute attr = it.next();
            String fullPath = ModelUtils.getAttributePath(path, attr.getName());
            if (!map.containsKey(fullPath)) {
                it.remove();
                continue;
            }

            if (attr.getAttributeType() == AttributeType.COMPLEX) {
                ComplexAttribute cattr = (ComplexAttribute) attr;
                for (DataRecord nested : cattr) {
                    filterAttributesNotFoundInModel(map, nested, fullPath);
                }
            }
        }
    }
}
