/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.records.upsert;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.impl.RecordsIndexingComponent;
import org.unidata.mdm.data.type.apply.RecordUpsertChangeSet;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.data.UpsertAction;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.meta.type.search.RecordIndexId;
import org.unidata.mdm.search.configuration.SearchConfigurationConstants;
import org.unidata.mdm.search.context.IndexRequestContext;
import org.unidata.mdm.search.type.id.ManagedIndexId;
import org.unidata.mdm.search.type.indexing.Indexing;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;
import org.unidata.mdm.system.util.TimeBoundaryUtils;

/**
 * Th
 * @author Mikhail Mikhailov on Nov 10, 2019
 */
@Component(RecordUpsertIndexingExecutor.SEGMENT_ID)
public class RecordUpsertIndexingExecutor extends Point<UpsertRequestContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_UPSERT_INDEXING]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.upsert.indexing.description";
    /**
     * Delay for async audit operations.
     */
    @ConfigurationRef(SearchConfigurationConstants.PROPERTY_REFRESH_IMMEDIATE)
    private ConfigurationValue<Boolean> refreshImmediate;
    /**
     * The RIC.
     */
    @Autowired
    private RecordsIndexingComponent recordsIndexingComponent;
    /**
     * Constructor.
     * @param id
     * @param description
     */
    public RecordUpsertIndexingExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(UpsertRequestContext ctx) {

        MeasurementPoint.start();
        try {

            // Detect 'no change' condition and skip collecting,
            // if we're called from regular upsert.
            // Proceed further for jobs anyway.
            if (!ctx.isRecalculateTimeline() && ctx.upsertAction() == UpsertAction.NO_ACTION) {
                return;
            }

            RecordKeys keys = ctx.keys();
            RecordUpsertChangeSet cs = ctx.changeSet();
            Timeline<OriginRecord> current = ctx.currentTimeline();
            Timeline<OriginRecord> next = ctx.nextTimeline();

            IndexRequestContext irc = IndexRequestContext.builder()
                    .drop(UpsertAction.INSERT != ctx.upsertAction())
                    .entity(keys.getEntityName())
                    .delete(collectDeletes(current, ctx))
                    .index(collectUpdates(current, next, ctx))
                    .routing(keys.getEtalonKey().getId())
                    .refresh(!ctx.isBatchOperation() && refreshImmediate.getValue())
                    .build();

            cs.addIndexRequestContext(irc);

        } finally {
            MeasurementPoint.stop();
        }
    }

    private Collection<ManagedIndexId> collectDeletes(Timeline<OriginRecord> current, UpsertRequestContext ctx) {

        if (current.isEmpty()) {
            return Collections.emptyList();
        }

        RecordKeys keys = current.getKeys();
        Timeline<OriginRecord> reduced = current.reduceBy(current.earliestFrom(ctx.getValidFrom()), current.latestTo(ctx.getValidTo()));
        return reduced.stream()
                .map(interval -> RecordIndexId.of(keys.getEntityName(), keys.getEtalonKey().getId(), interval.getPeriodId()))
                .collect(Collectors.toList());
    }

    private Collection<Indexing> collectUpdates(Timeline<OriginRecord> current, Timeline<OriginRecord> next, UpsertRequestContext ctx) {

        if (next.isEmpty()) {
            return Collections.emptyList();
        }

        return recordsIndexingComponent.build(next.getKeys(), selectFrame(current, next, ctx));
    }

    private Timeline<OriginRecord> selectFrame(Timeline<OriginRecord> current, Timeline<OriginRecord> next, UpsertRequestContext ctx) {

        Timeline<OriginRecord> reduced = current.reduceBy(current.earliestFrom(ctx.getValidFrom()), current.latestTo(ctx.getValidTo()));

        Date from = TimeBoundaryUtils.selectEarliest(reduced.isEmpty() ? ctx.getValidFrom() : reduced.first().getValidFrom(), ctx.getValidFrom());
        Date to = TimeBoundaryUtils.selectLatest(reduced.isEmpty() ? ctx.getValidTo() : reduced.last().getValidTo(), ctx.getValidTo());

        return next.reduceBy(next.earliestFrom(from), next.latestTo(to));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return UpsertRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
