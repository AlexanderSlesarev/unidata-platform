/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.type.snapshot;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Nullable;

import org.apache.commons.collections4.MultiValuedMap;

/**
 * @author Mikhail Mikhailov on Oct 23, 2021
 * Cached data values view.
 */
public interface FieldsSnapshot extends Serializable {
    /**
     * Gets the number of periods, collected to the object.
     * @return number of periods
     */
    int getNumberOfPeriods();
    /**
     * Empty check support.
     * @return true, if empty (ne periods data collected)
     */
    boolean isEmpty();
    /**
     * Gets field values, keyed by attribute names, for the period with the given index.
     * @param index the index
     * @return field values, keyed by attribute names, for the period with the given index, or null, if empty, or index is out of bounds
     */
    @Nullable
    MultiValuedMap<String, Serializable> getFieldsAt(int index);
    /**
     * Gets the requested field values, keyed by attribute names for the given period boundary.
     * @param validFrom the period start
     * @param validTo the period end
     * @return field values, keyed by attribute names, or null, if empty or no periods match
     */
    @Nullable
    MultiValuedMap<String, Serializable> getFieldsFor(Date validFrom, Date validTo);
    /**
     * Gets field values, keyed by attribute names, for the first period.
     * @return field values, keyed by attribute names, for the first period or null
     */
    @Nullable
    MultiValuedMap<String, Serializable> getFieldsHead();
    /**
     * Gets field values, keyed by attribute names, for the last period.
     * @return field values, keyed by attribute names, for the last period or null
     */
    @Nullable
    MultiValuedMap<String, Serializable> getFieldsTail();
}
