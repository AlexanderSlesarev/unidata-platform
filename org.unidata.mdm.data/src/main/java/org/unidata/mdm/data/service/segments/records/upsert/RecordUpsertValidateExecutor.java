/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.records.upsert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.Objects;

import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.LargeObjectsService;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.RecordIdentityContextSupport;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.FieldsCacheService;
import org.unidata.mdm.data.service.segments.RecordConsistencyCheckSupport;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.context.ConsistencyCheckContext;
import org.unidata.mdm.system.exception.PlatformValidationException;
import org.unidata.mdm.system.exception.ValidationResult;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov
 */
@Component(RecordUpsertValidateExecutor.SEGMENT_ID)
public class RecordUpsertValidateExecutor
        extends Point<UpsertRequestContext>
        implements RecordIdentityContextSupport, RecordConsistencyCheckSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_UPSERT_VALIDATE]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.upsert.validate.description";
    /**
     * Message.
     */
    private static final String EX_DATA_RECORD_VALIDATION_PERIOD_INCONSISTENT = DataModule.MODULE_ID + ".record.validation.period.inconsistent";
    /**
     * MetaModel service.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * The LS.
     */
    @Autowired
    private FieldsCacheService fieldsCacheService;
    /**
     * LOB component.
     */
    @Autowired
    private LargeObjectsService lobComponent;
    /**
     * Constructor.
     */
    public RecordUpsertValidateExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(UpsertRequestContext ctx) {

        Timeline<OriginRecord> next = ctx.nextTimeline();
        List<TimeInterval<OriginRecord>> affected = next.selectBy(ctx.getValidFrom(), ctx.getValidTo());
        EntityElement el = metaModelService.instance(Descriptors.DATA).getElement(selectEntityName(ctx));

        Map<TimeInterval<OriginRecord>, ConsistencyCheckContext> collected = new HashMap<>();
        for (TimeInterval<OriginRecord> hit : affected) {

            EtalonRecord er = hit.getCalculationResult();
            if (Objects.isNull(er)) {
                continue;
            }

            ConsistencyCheckContext check = ConsistencyCheckContext.builder()
                    .data(er)
                    .entity(el)
                    .payload(er)
                    .build();

            check(check);

            if (!check.valid()) {
                collected.put(hit, check);
            }
        }

        if (MapUtils.isNotEmpty(collected)) {

            List<ValidationResult> periods = collected.entrySet().stream()
                .map(entry ->
                    new ValidationResult(
                        "Data record for interval ({} - {}) is inconsistent with model.",
                        entry.getValue().validations(), EX_DATA_RECORD_VALIDATION_PERIOD_INCONSISTENT,
                        ConvertUtils.date2String(entry.getKey().getValidFrom()),
                        ConvertUtils.date2String(entry.getKey().getValidTo())))
                .collect(Collectors.toList());

            throw new PlatformValidationException("Record upsert to {0} rejected. Validation errors exist.",
                    DataExceptionIds.EX_DATA_RECORD_VALIDATION_UPSERT_REJECTED, periods, el.getName());
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MetaModelService metaModelService() {
        return metaModelService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public LargeObjectsService lobComponent() {
        return lobComponent;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FieldsCacheService fieldsCacheService() {
        return fieldsCacheService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return UpsertRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
