/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.unidata.mdm.core.type.calculables.Calculable;

/**
 * @author Mikhail Mikhailov on Apr 17, 2021
 */
public class GetTimelinesResult<X extends Calculable> {

    private Map<String, List<GetTimelineResult<X>>> timelines;

    /**
     * Constructor.
     */
    public GetTimelinesResult() {
        super();
    }
    /**
     * Constructor.
     */
    public GetTimelinesResult(Map<String, List<GetTimelineResult<X>>> timelines) {
        this();
        this.timelines = timelines;
    }
    /**
     * @return the timelines
     */
    public Map<String, List<GetTimelineResult<X>>> getTimelines() {
        return Objects.isNull(timelines) ? Collections.emptyMap() : timelines;
    }
    /**
     * @return the timelines
     */
    public List<GetTimelineResult<X>> getTimelines(String key) {
        List<GetTimelineResult<X>> selection = getTimelines().get(key);
        return Objects.isNull(selection) ? Collections.emptyList() : selection;
    }
    /**
     * @param timelines the timelines to set
     */
    public void setTimelines(Map<String, List<GetTimelineResult<X>>> timelines) {
        this.timelines = timelines;
    }
    /**
     * Adds a timeline to keyed collection.
     * @param key the collection key
     * @param timeline the timeline to add
     */
    public void add(String key, GetTimelineResult<X> timeline) {

        if (Objects.nonNull(timeline)) {

            if (Objects.isNull(timelines)) {
                timelines = new HashMap<>();
            }

            timelines.computeIfAbsent(key, k -> new ArrayList<>())
                     .add(timeline);
        }
    }
}
