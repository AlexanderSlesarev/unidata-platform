package org.unidata.mdm.data.type.apply;

/**
 * @author Mikhail Mikhailov on Jun 3, 2020
 */
public class RelationRestoreChangeSet extends RelationChangeSet {
    /**
     * Constructor.
     */
    public RelationRestoreChangeSet() {
        super();
    }
}
