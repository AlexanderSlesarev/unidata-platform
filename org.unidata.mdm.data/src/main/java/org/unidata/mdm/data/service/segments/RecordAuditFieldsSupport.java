/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.segments;

import java.util.HashMap;
import java.util.Map;

import org.unidata.mdm.data.context.RecordIdentityContext;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.data.type.keys.RecordOriginKey;
import org.unidata.mdm.data.type.messaging.DataHeaders;
import org.unidata.mdm.system.type.messaging.Header;

/**
 * @author Mikhail Mikhailov on Sep 3, 2021
 * Creates record audit header fields.
 */
public interface RecordAuditFieldsSupport {
    /**
     * Process body.
     * @param context the context.
     * @return map
     */
    default Map<Header, Object> headers(final RecordIdentityContext context) {

        final Map<Header, Object> info = new HashMap<>();
        RecordKeys keys = context.keys();
        if (keys != null) {

            RecordOriginKey originKey = keys.getOriginKey();
            info.put(DataHeaders.ENTITY_NAME, keys.getEntityName() == null ? context.getEntityName() : keys.getEntityName());
            info.put(DataHeaders.ETALON_ID, keys.getEtalonKey() == null ? context.getEtalonKey() : keys.getEtalonKey().getId());
            info.put(DataHeaders.EXTERNAL_ID, originKey == null ? context.getExternalId() : originKey.getExternalId());
            info.put(DataHeaders.SOURCE_SYSTEM, originKey == null ? context.getSourceSystem() : originKey.getSourceSystem());
        } else {

            info.put(DataHeaders.ENTITY_NAME, context.getEntityName());
            info.put(DataHeaders.ETALON_ID, context.getEtalonKey());
            info.put(DataHeaders.EXTERNAL_ID, context.getExternalId());
            info.put(DataHeaders.SOURCE_SYSTEM, context.getSourceSystem());
        }

        return info;
    }
}
