/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.data.type.data.EtalonRelation;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.RelativeDirection;
import org.unidata.mdm.meta.type.search.EntityIndexType;
import org.unidata.mdm.meta.type.search.RelationFromIndexId;
import org.unidata.mdm.meta.type.search.RelationHeaderField;
import org.unidata.mdm.meta.type.search.RelationToIndexId;
import org.unidata.mdm.search.type.id.AbstractManagedIndexId;
import org.unidata.mdm.search.type.indexing.Indexing;
import org.unidata.mdm.search.type.indexing.IndexingField;

/**
 * @author Mikhail Mikhailov on Dec 2, 2021
 * Relations indexing.
 */
@Component
public class RelationsIndexingComponent extends AbstractIndexingComponent {
    /**
     * Constructor.
     */
    @Autowired
    public RelationsIndexingComponent(MetaModelService metaModelService) {
        super(metaModelService);
    }
    /**
     * Creates new indexing objects for given periods.
     *
     * @param keys the keys
     * @param next the periods
     * @return indexing objects
     */
    public Collection<Indexing> build(@Nonnull RelationKeys keys, @Nonnull Iterable<TimeInterval<OriginRelation>> next) {
        return build(keys, next, null);
    }
    /**
     * Creates new indexing objects for given periods.
     *
     * @param keys the keys
     * @param next the periods
     * @param direction the direction (null means both)
     * @return indexing objects
     */
    public Collection<Indexing> build(@Nonnull RelationKeys keys, @Nonnull Iterable<TimeInterval<OriginRelation>> next, RelativeDirection direction) {

        EntityElement el = metaModelService.instance(Descriptors.DATA)
                .getElement(keys.getRelationName());

        ensureEntityElement(el, keys.getRelationName());

        boolean isNew = keys.isNew();

        List<Indexing> result = new ArrayList<>();
        for (TimeInterval<OriginRelation> ti : next) {

            if (!ti.hasCalculationResult()) {
                continue;
            }

            EtalonRelation etalon = ti.getCalculationResult();
            List<IndexingField> data = buildRecord(el, etalon);
            List<IndexingField> header = Arrays.asList(
                IndexingField.of(RelationHeaderField.FIELD_RELATION_NAME.getName(), keys.getRelationName()),
                IndexingField.of(RelationHeaderField.FIELD_RELATION_TYPE.getName(), keys.getRelationType().name()),
                IndexingField.of(RelationHeaderField.FIELD_ETALON_ID.getName(), keys.getEtalonKey().getId()),
                IndexingField.of(RelationHeaderField.FIELD_FROM_ETALON_ID.getName(), keys.getEtalonKey().getFrom().getId()),
                IndexingField.of(RelationHeaderField.FIELD_TO_ETALON_ID.getName(), keys.getEtalonKey().getTo().getId()),
                IndexingField.of(RelationHeaderField.FIELD_PERIOD_ID.getName(), AbstractManagedIndexId.periodIdValToString(etalon.getInfoSection().getValidTo())),
                IndexingField.of(RelationHeaderField.FIELD_DELETED.getName(), keys.getEtalonKey().getStatus() == RecordStatus.INACTIVE),
                IndexingField.of(RelationHeaderField.FIELD_INACTIVE.getName(), !ti.isActive()),
                IndexingField.of(RelationHeaderField.FIELD_ORIGINATOR.getName(), etalon.getInfoSection().getUpdatedBy()),
                IndexingField.of(RelationHeaderField.FIELD_OPERATION_TYPE.getName(), etalon.getInfoSection().getOperationType().name()),
                IndexingField.of(RelationHeaderField.FIELD_FROM.getName(), ti.getValidFrom()),
                IndexingField.of(RelationHeaderField.FIELD_TO.getName(), ti.getValidTo()),
                IndexingField.of(RelationHeaderField.FIELD_CREATED_AT.getName(), etalon.getInfoSection().getCreateDate()),
                IndexingField.of(RelationHeaderField.FIELD_UPDATED_AT.getName(), isNew ? null : etalon.getInfoSection().getUpdateDate()),
                IndexingField.of(RelationHeaderField.FIELD_CREATED_BY.getName(), etalon.getInfoSection().getCreatedBy()),
                IndexingField.of(RelationHeaderField.FIELD_UPDATED_BY.getName(), isNew ? null : etalon.getInfoSection().getUpdatedBy()));

            // 1. From
            if (direction == null || direction == RelativeDirection.FROM) {

                result.add(new Indexing(EntityIndexType.RELATION,
                        RelationFromIndexId.of(
                                keys.getFromEntityName(),
                                keys.getRelationName(),
                                keys.getEtalonKey().getFrom().getId(),
                                keys.getEtalonKey().getTo().getId(),
                                etalon.getInfoSection().getValidTo()))
                            .withFields(header)
                            .withFields(data)
                            .withFields(IndexingField.of(RelationHeaderField.FIELD_DIRECTION_FROM.getName(), true)));
            }

            // 2. To
            if (direction == null || direction == RelativeDirection.TO) {

                result.add(new Indexing(EntityIndexType.RELATION,
                        RelationToIndexId.of(
                                keys.getToEntityName(),
                                keys.getRelationName(),
                                keys.getEtalonKey().getFrom().getId(),
                                keys.getEtalonKey().getTo().getId(),
                                etalon.getInfoSection().getValidTo()))
                            .withFields(header)
                            .withFields(data)
                            .withFields(IndexingField.of(RelationHeaderField.FIELD_DIRECTION_FROM.getName(), false)));
            }
        }

        return result;
    }

    private void ensureEntityElement(EntityElement el, String name) {
        // FIXME: dedicated exception!
    }
}
