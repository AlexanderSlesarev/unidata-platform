/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.search.EntityIndexType;
import org.unidata.mdm.meta.type.search.EtalonHeaderField;
import org.unidata.mdm.meta.type.search.EtalonIndexId;
import org.unidata.mdm.meta.type.search.EtalonIndexType;
import org.unidata.mdm.meta.type.search.RecordHeaderField;
import org.unidata.mdm.meta.type.search.RecordIndexId;
import org.unidata.mdm.search.type.id.AbstractManagedIndexId;
import org.unidata.mdm.search.type.indexing.Indexing;
import org.unidata.mdm.search.type.indexing.IndexingField;
import org.unidata.mdm.search.util.SearchUtils;

/**
 * @author Mikhail Mikhailov on Dec 2, 2021
 * Records indexing.
 */
@Component
public class RecordsIndexingComponent extends AbstractIndexingComponent {
    /**
     * Constructor.
     */
    @Autowired
    public RecordsIndexingComponent(MetaModelService metaModelService) {
        super(metaModelService);
    }
    /**
     * Creates indexing objects for a number of periods.
     *
     * @param keys the record keys
     * @param next the periods
     * @return collection of indexing objects
     */
    public Collection<Indexing> build(@Nonnull RecordKeys keys, @Nonnull Iterable<TimeInterval<OriginRecord>> next) {

        EntityElement el = metaModelService.instance(Descriptors.DATA)
                .getElement(keys.getEntityName());

        ensureEntityElement(el, keys.getEntityName());

        boolean isNew = keys.isNew();
        List<Indexing> result = new ArrayList<>();
        if (isNew) {
            result.add(new Indexing(EtalonIndexType.ETALON,
                EtalonIndexId.of(keys.getEntityName(), keys.getEtalonKey().getId()))
                    .withFields(IndexingField.of(EtalonHeaderField.FIELD_ETALON_ID.getName(), keys.getEtalonKey().getId())));
        }

        for (TimeInterval<OriginRecord> ti : next) {

            if (!ti.hasCalculationResult()) {
                continue;
            }

            EtalonRecord etalon = ti.getCalculationResult();

            Indexing i = new Indexing(EntityIndexType.RECORD,
                RecordIndexId.of(
                        keys.getEntityName(),
                        keys.getEtalonKey().getId(),
                        etalon.getInfoSection().getValidTo()));

            i.withFields(buildRecord(el, etalon))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_FROM.getName(), ti.getValidFrom()))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_TO.getName(), ti.getValidTo()))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_CREATED_AT.getName(), etalon.getInfoSection().getCreateDate()))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_UPDATED_AT.getName(), isNew ? null : etalon.getInfoSection().getUpdateDate()))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_CREATED_BY.getName(), etalon.getInfoSection().getCreatedBy()))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_UPDATED_BY.getName(), isNew ? null : etalon.getInfoSection().getUpdatedBy()))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_PERIOD_ID.getName(), AbstractManagedIndexId.periodIdValToString(etalon.getInfoSection().getValidTo())))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_ORIGINATOR.getName(), etalon.getInfoSection().getUpdatedBy()))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_DELETED.getName(), keys.getEtalonKey().getStatus() == RecordStatus.INACTIVE))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_INACTIVE.getName(), !ti.isActive()))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_ETALON_ID.getName(), keys.getEtalonKey().getId()))
             .withFields(IndexingField.of(RecordHeaderField.FIELD_OPERATION_TYPE.getName(), etalon.getInfoSection().getOperationType().name()))
             .withFields(IndexingField.ofStrings(RecordHeaderField.FIELD_EXTERNAL_KEYS.getName(), ti.unlock().toCalculables().stream()
                 .map(origin -> origin.getSourceSystem() + SearchUtils.COLON_SEPARATOR + origin.getExternalId())
                 .collect(Collectors.toList())));

            result.add(i);
        }

        return result;
    }

    private void ensureEntityElement(EntityElement el, String name) {
        // FIXME: dedicated exception!
    }
}
