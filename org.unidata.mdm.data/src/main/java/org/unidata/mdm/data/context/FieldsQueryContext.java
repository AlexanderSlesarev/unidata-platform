/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.context;

import java.io.Serializable;

/**
 * @author Mikhail Mikhailov on Oct 22, 2021
 * Lookup values query context.
 */
public class FieldsQueryContext {
    /**
     * Lookup name.
     */
    private final String elementName;
    /**
     * Code value.
     */
    private final Serializable lookupValue;
    /**
     * Code attribute.
     */
    private final String lookupField;
    /**
     * Include inactive.
     */
    private final boolean inactive;
    /**
     * Include deleted.
     */
    private final boolean deleted;
    /**
     * Constructor.
     */
    private FieldsQueryContext(FieldsQueryContextBuilder b) {
        super();
        this.elementName = b.elementName;
        this.lookupValue = b.lookupValue;
        this.lookupField = b.lookupField;
        this.inactive = b.inactive;
        this.deleted = b.deleted;
    }
    /**
     * @return the lookupName
     */
    public String getElementName() {
        return elementName;
    }
    /**
     * @return the codeValue
     */
    public Serializable getLookupValue() {
        return lookupValue;
    }
    /**
     * @return the codeAttribute
     */
    public String getLookupField() {
        return lookupField;
    }
    /**
     * @return the inactive
     */
    public boolean isInactive() {
        return inactive;
    }
    /**
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }
    /**
     * Gets builder instance.
     * @return builder instance
     */
    public static FieldsQueryContextBuilder builder() {
        return new FieldsQueryContextBuilder();
    }
    /**
     * @author Mikhail Mikhailov on Oct 22, 2021
     * Builder.
     */
    public static class FieldsQueryContextBuilder {
        /**
         * Lookup name.
         */
        private String elementName;
        /**
         * Code value.
         */
        private Serializable lookupValue;
        /**
         * Code attribute.
         */
        private String lookupField;
        /**
         * Include inactive.
         */
        private boolean inactive;
        /**
         * Include deleted.
         */
        private boolean deleted;
        /**
         * Constructor.
         */
        private FieldsQueryContextBuilder() {
            super();
        }

        public FieldsQueryContextBuilder elementName(String elementName) {
            this.elementName = elementName;
            return this;
        }

        public FieldsQueryContextBuilder lookupValue(Serializable attributeValue) {
            this.lookupValue = attributeValue;
            return this;
        }

        public FieldsQueryContextBuilder lookupField(String attributeName) {
            this.lookupField = attributeName;
            return this;
        }

        /**
         * Include inactive.
         * @param inactive include inactive flag
         * @return self
         */
        public FieldsQueryContextBuilder inactive(boolean inactive) {
            this.inactive = inactive;
            return this;
        }
        /**
         * Include deleted.
         * @param deleted include deleted flag
         * @return self
         */
        public FieldsQueryContextBuilder deleted(boolean deleted) {
            this.deleted = deleted;
            return this;
        }
        /**
         * Builder method.
         * @return LQC instance
         */
        public FieldsQueryContext build() {
            return new FieldsQueryContext(this);
        }
    }
}
