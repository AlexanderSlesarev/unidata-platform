package org.unidata.mdm.data.type.apply;

/**
 * @author Mikhail Mikhailov on Jun 2, 2020
 */
public class RecordRestoreChangeSet extends RecordChangeSet {
    /**
     * Constructor.
     */
    public RecordRestoreChangeSet() {
        super();
    }
}
