/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.context;

import java.util.List;
import java.util.Map;

import org.unidata.mdm.core.type.calculables.Calculable;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.system.context.StorageCapableContext;
import org.unidata.mdm.system.context.StorageId;

/**
 * Records merge interface.
 * @author Mikhail Mikhailov on Nov 6, 2019
 */
public interface MergeDuplicatesContext<C extends Calculable> extends StorageCapableContext {
    /**
     * Duplicate record keys.
     */
    StorageId SID_MERGE_DUPLICATE_KEYS = new StorageId("MERGE_DUPLICATE_KEYS");
    /**
     * Last duplicate timelines.
     */
    StorageId SID_MERGE_DUPLICATE_TIMELINES = new StorageId("MERGE_DUPLICATE_TIMELINES");
    /**
     * Discarded incomplete record keys.
     */
    StorageId SID_MERGE_DISCARDED_KEYS = new StorageId("MERGE_DISCARDED_KEYS");
    /**
     * GET duplicates.
     * @return key list
     */
    default List<RecordKeys> duplicateKeys() {
        return getFromStorage(SID_MERGE_DUPLICATE_KEYS);
    }
    /**
     * PUT duplicates.
     * @param duplicates key list
     */
    default void duplicateKeys(List<RecordKeys> duplicates) {
        putToStorage(SID_MERGE_DUPLICATE_KEYS, duplicates);
    }
    /**
     * GET discarded.
     * @return key list
     */
    default List<RecordKeys> discardedKeys() {
        return getFromStorage(SID_MERGE_DISCARDED_KEYS);
    }
    /**
     * PUT discarded.
     * @param duplicates key list
     */
    default void discardedKeys(List<RecordKeys> duplicates) {
        putToStorage(SID_MERGE_DISCARDED_KEYS, duplicates);
    }
    /**
     * Get TLs.
     * @return timeline
     */
    default Map<String, Timeline<C>> duplicateTimelines() {
        return getFromStorage(SID_MERGE_DUPLICATE_TIMELINES);
    }
    /**
     * Put TLs.
     * @param timelines timelines map
     */
    default void duplicateTimelines(Map<String, Timeline<C>> timelines) {
        putToStorage(SID_MERGE_DUPLICATE_TIMELINES, timelines);
    }
}
