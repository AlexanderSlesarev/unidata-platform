/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.data.service.impl.load.xlsx;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.configuration.UserMessageConstants;
import org.unidata.mdm.core.context.DataImportInputContext;
import org.unidata.mdm.core.context.DataImportTemplateContext;
import org.unidata.mdm.core.context.UpsertLargeObjectContext;
import org.unidata.mdm.core.context.UpsertUserEventRequestContext;
import org.unidata.mdm.core.dto.DataImportTemplateResult;
import org.unidata.mdm.core.dto.UserEventDTO;
import org.unidata.mdm.core.service.AsyncExecutor;
import org.unidata.mdm.core.service.LargeObjectsService;
import org.unidata.mdm.core.service.SecurityService;
import org.unidata.mdm.core.service.UserService;
import org.unidata.mdm.core.type.data.ArrayAttribute;
import org.unidata.mdm.core.type.data.CodeAttribute;
import org.unidata.mdm.core.type.data.ComplexAttribute;
import org.unidata.mdm.core.type.data.SimpleAttribute;
import org.unidata.mdm.core.type.data.impl.AbstractArrayAttribute;
import org.unidata.mdm.core.type.data.impl.AbstractCodeAttribute;
import org.unidata.mdm.core.type.data.impl.AbstractSimpleAttribute;
import org.unidata.mdm.core.type.data.impl.ComplexAttributeImpl;
import org.unidata.mdm.core.type.data.impl.SerializableDataRecord;
import org.unidata.mdm.core.type.load.DataImportFormat;
import org.unidata.mdm.core.type.lob.LargeObjectAcceptance;
import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.AttributeElement.AttributeValueType;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.model.NestedElement;
import org.unidata.mdm.core.type.model.RelationElement;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.data.context.DeleteRequestContext;
import org.unidata.mdm.data.context.UpsertRelationRequestContext;
import org.unidata.mdm.data.context.UpsertRelationsRequestContext;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.dto.DeleteRecordDTO;
import org.unidata.mdm.data.dto.RecordsBulkResultDTO;
import org.unidata.mdm.data.dto.RelationsBulkResultDTO;
import org.unidata.mdm.data.dto.UpsertRecordDTO;
import org.unidata.mdm.data.dto.UpsertRelationsDTO;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.exception.DataProcessingException;
import org.unidata.mdm.data.service.DataRecordsService;
import org.unidata.mdm.data.service.DataRelationsService;
import org.unidata.mdm.data.service.impl.load.RecordsImportImplementation;
import org.unidata.mdm.data.service.impl.load.xlsx.XlsxImportState.XlsxImportUpsert;
import org.unidata.mdm.data.service.impl.load.xlsx.XlsxImportState.XlsxSheetState;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.exception.PlatformRuntimeException;
import org.unidata.mdm.system.type.runtime.MeasurementContextName;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;
import org.unidata.mdm.system.util.IdUtils;
import org.unidata.mdm.system.util.TextUtils;

/**
 * The Class XLSXImportService.
 */
@Component
@Qualifier("xlsxImportService")
public class XLSXImportComponent extends XLSXProcessor implements RecordsImportImplementation {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(XLSXImportComponent.class);

    public static final String XLSX_IMPORT = "XLSX_IMPORT";

    public static final String DEFAULT_REPORT_FILE_NAME = "Report.txt";
    private static final String JOB_REPORT_TYPE = "XLSX_IMPORT";
    /**
     * Async executor.
     */
    @Autowired
    private AsyncExecutor asyncExecutor;
    /**
     * DRS.
     */
    @Autowired
    private DataRecordsService dataRecordService;
    /**
     * Rel service.
     */
    @Autowired
    private DataRelationsService dataRelationsService;
    /**
     * LOS.
     */
    @Autowired
    private LargeObjectsService largeObjectsService;
    /**
     * The user service.
     */
    @Autowired
    private UserService userService;
    /**
     * The security service.
     */
    @Autowired
    private SecurityService securityService;
    /**
     * {@inheritDoc}
     */
    @Override
    public DataImportTemplateResult template(DataImportTemplateContext ctx) {
        try (ByteArrayOutputStream output = new ByteArrayOutputStream();
             Workbook wb = createTemplateWorkbook(ctx.getEntityName())) {
            wb.write(output);
            return new DataImportTemplateResult(output.toByteArray());
        } catch (IOException e) {
            LOGGER.error("Unable to create template XLS file", e);
            throw new PlatformFailureException("Unable to create template XLS file for [{}].",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_UNABLE_TO_CREATE_TEMPLATE, ctx.getEntityName());
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DataImportFormat format() {
        return DataImportFormat.XLSX;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void handle(DataImportInputContext ctx) {

        Path path = preValidateAndSave(ctx);

        String userName = SecurityUtils.getCurrentUserName();
        String userToken = SecurityUtils.getCurrentUserToken();

        asyncExecutor.async(() -> {
            try {
                importDataSync(path, userName, userToken, ctx);
            } catch (Exception e) {
                LOGGER.error("Cannot import file. Exception caught.", e);
                try {

                    final String message = e instanceof PlatformRuntimeException
                            ? formatSystemRuntimeException((PlatformRuntimeException) e)
                            : e.getLocalizedMessage();

                    Path reportFile = Files.write(Paths.get(DEFAULT_REPORT_FILE_NAME),
                            message.getBytes(Charset.forName(StandardCharsets.UTF_8.name())),
                            StandardOpenOption.TRUNCATE_EXISTING,
                            StandardOpenOption.CREATE);

                    UpsertUserEventRequestContext uueCtx = UpsertUserEventRequestContext.builder()
                            .login(userName)
                            .type(XLSX_IMPORT)
                            .content(TextUtils.getText(UserMessageConstants.DATA_IMPORT_UNSUCCESS, ctx.getFileName()))
                            .build();

                    UserEventDTO userEventDTO = userService.upsert(uueCtx);

                    // save result and attach it to the early created user event
                    UpsertLargeObjectContext slorCTX = UpsertLargeObjectContext.builder()
                            .subjectId(userEventDTO.getId())
                            .mimeType("text/csv")
                            .binary(true)
                            .input(Files.newInputStream(reportFile, StandardOpenOption.DELETE_ON_CLOSE))
                            .filename("Report.csv")
                            .acceptance(LargeObjectAcceptance.ACCEPTED)
                            .build();

                    largeObjectsService.saveLargeObject(slorCTX);
                } catch (Exception e1) {
                    LOGGER.error("Cannot create report file due to exception", e1);
                }
            } finally {
                try {
                    Files.delete(path);
                } catch (IOException e) {
                    LOGGER.warn("Cannot remove temporary file [{}].", path, e);
                }
            }
        });
    }
    /**
     * Prevalidate.
     *
     * @param file the file
     * @param entityName the entity name
     */
    private Path preValidateAndSave(DataImportInputContext ctx) {

        // 1. Check
        if (Objects.isNull(ctx.getFileName())
         || Objects.isNull(ctx.getEntityName())
         || Objects.isNull(ctx.getSourceSystem())) {
            throw new DataProcessingException("Some required parameters missing. File name [{}], Entity name [{}], Source system [{}] must be present.",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_INPUT_PARAMETERS_MISSING,
                    ctx.getFileName(), ctx.getEntityName(), ctx.getSourceSystem());
        }

        if (!StringUtils.endsWith(ctx.getFileName().toLowerCase(), ".xlsx")) {
            throw new DataProcessingException("Unable to parse file {}. Invalid format. Only XLSX files accepted.",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_UNKNOWN_FILE_FORMAT, ctx.getFileName());
        }

        // 2. Entity name check.
        EntityElement el = metaModelService.instance(Descriptors.DATA)
            .getElement(ctx.getEntityName());
        if (Objects.isNull(el) || !(el.isRegister() || el.isLookup())) {
            ensureEntityElement(ctx.getEntityName());
        }

        // 3. Copy stream
        Path path = saveStreamToFile(ctx);

        // 4. Basic stuff.
        try (XSSFWorkbook workbook = new XSSFWorkbook(path.toFile())) {

            XSSFSheet sheet = workbook.getSheetAt(0);
            if (sheet == null) {
                throw new DataProcessingException("Invalid file structure [{}].",
                        DataExceptionIds.EX_DATA_XLSX_IMPORT_INVALID_FILE_STRUCTURE, ctx.getFileName());
            }
        } catch (InvalidFormatException | InvalidOperationException | IOException e) {
            FileUtils.deleteQuietly(path.toFile());
            throwImportParseFile(ctx.getFileName(), e);
        } catch (PlatformRuntimeException e) {
            FileUtils.deleteQuietly(path.toFile());
            throw e;
        }

        return path;
    }

    private Path saveStreamToFile(DataImportInputContext ctx) {

        try {

            Path path = Files.createTempFile("unidata-xlsx-import", ".xlsx");
            File file = path.toFile();
            try (FileOutputStream fos = new FileOutputStream(file);
                 InputStream is = ctx.getInput().get()) {

                int read;
                byte[] buf = new byte[4096];
                while ((read = is.read(buf, 0, buf.length)) != -1) {
                    fos.write(buf, 0, read);
                }
            }

            return path;
        } catch (Exception e) {
            throw new PlatformFailureException("Failed to save data stream to temporary file.",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_TEMPORARY_TABLE, e);
        }
    }

    private String formatSystemRuntimeException(PlatformRuntimeException pre) {
        return "Notice [" + TextUtils.getText(pre.getId().code(), pre.getArgs()) + "], system code [" + pre.getId().code() + "]";
    }

    /**
     * Import data sync.
     *
     * @param path the file
     * @param entityName the entity name
     * @param sourceSystem the source system
     * @param creationDate the creation date
     * @param userName the user name
     * @param userToken the user token
     */
    private void importDataSync(Path path, String userName, String userToken, DataImportInputContext ctx) {

        securityService.authenticate(userToken, true);

        MeasurementPoint.init(MeasurementContextName.MEASURE_STEP_IMPORT_RECORDS);
        MeasurementPoint.start();

        try (XSSFWorkbook workbook = new XSSFWorkbook(path.toFile())) {

            XSSFFormulaEvaluator fe = new XSSFFormulaEvaluator(workbook);

            // 1. Init state
            XlsxImportState state = new XlsxImportState(this, ctx);

            // 2. Process top level records.
            fillTopLevelRecords(state, workbook, fe);

            // 3. Fill complex attributes
            fillComplexAttributes(state, workbook, fe);

            // 4. Fill relations.
            fillRelations(state, workbook, fe);

            // 5. Run batches
            RecordsBulkResultDTO recordsUpsert = dataRecordService.batchUpsertRecords(state.toRecordUpserts());
            RecordsBulkResultDTO recordsDelete = dataRecordService.batchDeleteRecords(state.toRecordDeletes());
            RelationsBulkResultDTO relationsUpsert = dataRelationsService.batchUpsertRelations(state.toRelationUpserts());

            // 6. Generate report
            generateReport(state, userName, recordsUpsert, recordsDelete, relationsUpsert);

        } catch (InvalidFormatException | IOException e) {
            throwImportParseFile(ctx.getFileName(), e);
        } finally {
            MeasurementPoint.stop();
        }
    }

    private void fillTopLevelRecords(XlsxImportState state, XSSFWorkbook workbook, XSSFFormulaEvaluator fe) {

        // 1. Entity
        DataImportInputContext ctx = state.getInput();
        EntityElement el = ensureEntityElement(ctx.getEntityName());

        // 2. Sheet and state
        XlsxSheetState sst = state.withTopLevel(workbook, el);
        Sheet sheet = identifySheet(workbook, el.getName(), OBJECT_TYPE.ENTITY);

        long totalNotEmptyRows = 0L;
        int lastRowNum = sheet.getLastRowNum();
        for (int rowNum = H_R_HEADER_IDX + 1; rowNum < lastRowNum + 1; rowNum++) {

            final Row currentRow = sheet.getRow(rowNum);
            if (isEmptyRow(currentRow)) {
                continue;
            }

            totalNotEmptyRows++;
            fillTopLevelRecord(state, sst, currentRow, fe, rowNum);
        }

        state.add(new StatisticSheet(OBJECT_TYPE.ENTITY, sst.getEntity().getName(), sst.getEntity().getDisplayName(), totalNotEmptyRows));
    }

    private void fillTopLevelRecord(XlsxImportState is, XlsxSheetState state, Row currentRow, XSSFFormulaEvaluator fe, int rowNum) {

        String externalId = null;
        String linkedId = null;
        String etalonId = null;
        Boolean isActive = null;
        Date validFrom = null;
        Date validTo = null;
        try {

            DataImportInputContext ctx = is.getInput();

            SerializableDataRecord dataRecord = SerializableDataRecord.of();
            for (Entry<Integer, String> headerRow : state.getHeaders().entrySet()) {

                Cell cell = currentRow.getCell(headerRow.getKey());
                if (headerRow.getValue().equals(ID)) {
                    linkedId = convertToString(cell, fe);
                } else if (headerRow.getValue().equals(ETALON_ID)) {
                    etalonId = convertToString(cell, fe);
                } else if (headerRow.getValue().equals(EXTERNAL_ID)) {
                    externalId = convertToString(cell, fe);
                } else if (headerRow.getValue().equals(FROM)) {
                    validFrom = convertValidityPeriod(cell, fe, true);
                } else if (headerRow.getValue().equals(TO)) {
                    validTo = convertValidityPeriod(cell, fe, false);
                } else if (headerRow.getValue().equals(IS_ACTIVE)) {
                    isActive = convertToBooleanWithDefault(cell, fe, Boolean.TRUE);
                } else {
                    processAttributeRow(fe, dataRecord, cell, state.getTypes().get(headerRow.getValue()));
                }
            }

            if (BooleanUtils.isFalse(isActive)) {

                is.add(DeleteRequestContext.builder()
                    .entityName(state.getEntity().getName())
                    .etalonKey(etalonId)
                    .externalId(externalId)
                    .sourceSystem(ctx.getSourceSystem())
                    .operationId(ctx.getOperationId())
                    .batchOperation(true)
                    .validFrom(validFrom)
                    .validTo(validTo)
                    .inactivatePeriod(true)
                    .wipe(false)
                    .build());

            } else {

                if (externalId == null && etalonId == null && !state.getEntity().isGenerating()) {
                    externalId = IdUtils.v1String();
                }

                UpsertRequestContext result = UpsertRequestContext.builder()
                    .etalonKey(etalonId)
                    .externalId(externalId)
                    .sourceSystem(ctx.getSourceSystem())
                    .entityName(state.getEntity().getName())
                    .validFrom(validFrom)
                    .validTo(validTo)
                    .operationId(ctx.getOperationId())
                    .mergeWithPrevVersion(ctx.isMergeWithPrevious())
                    .batchOperation(true)
                    .record(dataRecord)
                    .build();

                if (linkedId != null) {
                    is.add(wrapLinkedId(linkedId), getExcelCoordinates(rowNum), result);
                } else if (etalonId != null) {
                    is.add(wrapEtalonId(etalonId), getExcelCoordinates(rowNum), result);
                } else if (externalId != null) {
                    is.add(externalId, getExcelCoordinates(rowNum), result);
                } else {
                    is.add(StringUtils.EMPTY, getExcelCoordinates(rowNum), result);
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Failed to import record from XLS. Entity name [{}], Excel coordinates [{}].",
                    state.getEntity().getName(), getExcelCoordinates(rowNum), e);
        }
    }

    private void fillComplexAttributes(XlsxImportState state, XSSFWorkbook workbook, XSSFFormulaEvaluator formulaEvaluator) {

        int lastRowNum;
        long totalNotEmptyRows = 0;
        long skippedRowSize = 0;

        XlsxSheetState top = state.getTopLevel();
        if (!top.getEntity().isRegister()
         || CollectionUtils.isEmpty(top.getEntity().getRegister().getReferencedNesteds())) {
            return;
        }

        Collection<AttributeElement> complex = top.getEntity().getAttributes().values().stream()
                .filter(AttributeElement::isComplex)
                .filter(attr -> attr.getLevel() == 0)
                .collect(Collectors.toList());

        for (AttributeElement cattr : complex) {

            NestedElement ne = metaModelService.instance(Descriptors.DATA)
                    .getNested(cattr.getComplex().getNestedEntityName());

            XlsxSheetState sst = state.withNested(workbook, ne);

            // No data.
            if (Objects.isNull(sst)) {
                continue;
            }

            Sheet nestedSheet = identifySheet(workbook, ne.getName(), OBJECT_TYPE.NESTED);
            lastRowNum = nestedSheet.getLastRowNum();

            for (int rowNum = H_R_HEADER_IDX + 1; rowNum < lastRowNum + 1; rowNum++) {

                final Row currentRow = nestedSheet.getRow(rowNum);
                if (isEmptyRow(currentRow)) {
                    continue;
                }

                Date validFrom = null;
                Date validTo = null;
                String externalId = null;
                String linkedId = null;
                String etalonId = null;
                SerializableDataRecord nestedDataRecord = SerializableDataRecord.of();

                try {

                    for (Entry<Integer, String> headerRow : sst.getHeaders().entrySet()) {

                        Cell cell = currentRow.getCell(headerRow.getKey());
                        switch (headerRow.getValue()) {
                            case ID:
                                linkedId = convertToString(cell, formulaEvaluator);
                                break;
                            case ETALON_ID:
                                etalonId = convertToString(cell, formulaEvaluator);
                                break;
                            case EXTERNAL_ID:
                                externalId = convertToString(cell, formulaEvaluator);
                                break;
                            case FROM:
                                validFrom = convertValidityPeriod(cell, formulaEvaluator, true);
                                break;
                            case TO:
                                validTo = convertValidityPeriod(cell, formulaEvaluator, false);
                                break;
                            default:
                                XLSXHeader header = sst.getTypes().get(headerRow.getValue());
                                if (header.getType() == XLSXHeader.TYPE.SYSTEM) {
                                    continue;
                                }
                                processAttributeRow(formulaEvaluator, nestedDataRecord, cell, sst.getTypes().get(headerRow.getValue()));
                                break;
                        }
                    }

                    // Just skip for now
                    if (linkedId == null && externalId == null && etalonId == null) {
                        continue;
                    }

                    Collection<XlsxImportUpsert> upserts = state.getById(linkedId != null
                                ? wrapLinkedId(linkedId)
                                : wrapEtalonId(externalId, etalonId));

                    if (CollectionUtils.isEmpty(upserts)) {
                        continue;
                    }

                    for (XlsxImportUpsert upsert : upserts) {

                        UpsertRequestContext uCtx = upsert.getContext();
                        if (!Objects.equals(uCtx.getValidFrom(), validFrom) || !Objects.equals(uCtx.getValidTo(), validTo)) {
                            totalNotEmptyRows++;
                            skippedRowSize++;
                            continue;
                        }

                        ComplexAttribute ca = uCtx.getRecord().getComplexAttribute(cattr.getName());
                        if (ca == null) {
                            ca = new ComplexAttributeImpl(cattr.getName());
                            uCtx.getRecord().addAttribute(ca);
                        }

                        ca.add(nestedDataRecord);
                        totalNotEmptyRows++;
                    }

                } catch (Exception e) {
                    LOGGER.warn("Failed to import complex attribute record from XLS. Nested entity name [{}], Excel coordinates [{}].",
                            ne.getName(), getExcelCoordinates(rowNum), e);
                }
            }

            state.add(new StatisticSheet(OBJECT_TYPE.COMPLEX_ATTRIBUTE, ne.getName(), ne.getDisplayName(), totalNotEmptyRows, skippedRowSize));
        }
    }

    private void fillRelations(XlsxImportState state, XSSFWorkbook workbook, XSSFFormulaEvaluator fe) {

        DataImportInputContext ctx = state.getInput();
        XlsxSheetState top = state.getTopLevel();
        if (!top.getEntity().isRegister()) {
            return;
        }

        Map<RelationElement, EntityElement> relations = top.getEntity()
                .getRegister()
                .getOutgoingRelations();

        for (Map.Entry<RelationElement, EntityElement> er : relations.entrySet()) {

            RelationElement relation = er.getKey();
            XlsxSheetState sst = state.withRelation(workbook, relation);
            if (sst == null) {
                continue;
            }

            Sheet relationSheet = identifySheet(workbook, relation.getName(), OBJECT_TYPE.RELATION);
            int lastRowNum = relationSheet.getLastRowNum();
            long totalNotEmptyRows = 0l;
            for (int rowNum = H_R_HEADER_IDX + 1; rowNum < lastRowNum + 1; rowNum++) {

                Date validFrom = null;
                Date validTo = null;
                String externalId = null;
                String linkedId = null;
                String toEtalonId = null;
                String toExternalId = null;
                String etalonId = null;

                final Row currentRow = relationSheet.getRow(rowNum);
                if (isEmptyRow(currentRow)) {
                    continue;
                }

                try {

                    totalNotEmptyRows++;
                    SerializableDataRecord relationDataRecord = SerializableDataRecord.of();
                    for (Map.Entry<Integer, String> headerRow : sst.getHeaders().entrySet()) {
                        Cell cell = currentRow.getCell(headerRow.getKey());
                        if (headerRow.getValue().equals(ID)) {
                            linkedId = convertToString(cell, fe);
                        } else if (headerRow.getValue().equals(ETALON_ID)) {
                            etalonId = convertToString(cell, fe);
                        } else if (headerRow.getValue().equals(EXTERNAL_ID)) {
                            externalId = convertToString(cell, fe);
                        } else if (headerRow.getValue().equals(FROM)) {
                            validFrom = convertValidityPeriod(cell, fe, true);
                        } else if (headerRow.getValue().equals(TO)) {
                            validTo = convertValidityPeriod(cell, fe, false);
                        } else if (headerRow.getValue().equals(TO_ETALON_ID)) {
                            toEtalonId = convertToString(cell, fe);
                        } else if (headerRow.getValue().equals(TO_EXTERNAL_ID)) {
                            toExternalId = convertToString(cell, fe);
                        } else {

                            XLSXHeader header = sst.getTypes().get(headerRow.getValue());
                            if (header.getType() == XLSXHeader.TYPE.SYSTEM) {
                                continue;
                            }

                            processAttributeRow(fe, relationDataRecord, cell, sst.getTypes().get(headerRow.getValue()));
                        }
                    }

                    // No identity at all. Just proceed for now.
                    if (linkedId == null && externalId == null && etalonId == null) {
                        continue;
                    }

                    List<XlsxImportUpsert> upserts = state.getById(linkedId != null
                            ? wrapLinkedId(linkedId)
                            : wrapEtalonId(externalId, etalonId));

                    if (CollectionUtils.isNotEmpty(upserts)) {

                        UpsertRequestContext uCtx = upserts.get(upserts.size() - 1).getContext();
                        if (Objects.isNull(toExternalId)) {
                            toExternalId = relation.isContainment() && toEtalonId == null
                                    ? IdUtils.v1String()
                                    : null;
                        }

                        UpsertRelationsRequestContext result = UpsertRelationsRequestContext.builder()
                                .externalId(uCtx.getExternalIdAsObject())
                                .etalonKey(uCtx.getEtalonKey())
                                .operationId(ctx.getOperationId())
                                .relationFrom(relation.getName(), UpsertRelationRequestContext.builder()
                                        .relationName(relation.getName())
                                        .etalonKey(toEtalonId)
                                        .validFrom(validFrom)
                                        .validTo(validTo)
                                        .externalId(toExternalId)
                                        .sourceSystem(ctx.getSourceSystem())
                                        .record(relationDataRecord)
                                        .entityName(relation.getRight().getName())
                                        .batchOperation(true)
                                        .operationId(ctx.getOperationId())
                                        .build())
                                .build();

                        state.add(result);
                    }
                } catch (Exception e) {
                    LOGGER.warn("Failed to import relation record from XLS. Relation name [{}], Excel coordinates [{}].",
                            relation.getName(), getExcelCoordinates(rowNum), e);
                }
            }

            state.add(new StatisticSheet(OBJECT_TYPE.RELATION, relation.getName(), relation.getDisplayName(), totalNotEmptyRows));
        }
    }

    private void processAttributeRow(XSSFFormulaEvaluator formulaEvaluator, SerializableDataRecord nestedDataRecord, Cell cell, XLSXHeader attrHeader) {

        AttributeElement attrInfo = attrHeader.getAttributeHolder();
        if (attrInfo == null) {
            return;
        }

        if (attrInfo.isSimple()) {

            if (attrInfo.getValueType() == AttributeValueType.BLOB
             || attrInfo.getValueType() == AttributeValueType.CLOB) {
                return;
            }

            Object fieldValue = convertCell(cell, attrHeader, formulaEvaluator);
            if (fieldValue == null) {
                return;
            }

            SimpleAttribute.SimpleDataType dataType = SimpleAttribute.SimpleDataType.valueOf(attrInfo.getValueType().name());
            nestedDataRecord.addAttribute(AbstractSimpleAttribute.of(
                    dataType,
                    attrInfo.getPath(),
                    fieldValue));
        } else if (attrInfo.isArray()) {

            Object fieldValue = convertCell(cell, attrHeader, formulaEvaluator);
            if (fieldValue == null) {
                return;
            }

            nestedDataRecord.addAttribute(AbstractArrayAttribute.of(
                    ArrayAttribute.ArrayDataType.valueOf(attrInfo.getValueType().name()),
                    attrInfo.getPath(),
                    fieldValue instanceof List ? (List<?>) fieldValue : Collections.singletonList(fieldValue)));

        } else if (attrInfo.isCode()) {

            Object fieldValue = convertCell(cell, attrHeader, formulaEvaluator);
            if (fieldValue == null) {
                return;
            }

            nestedDataRecord.addAttribute(AbstractCodeAttribute.of(
                    CodeAttribute.CodeDataType.valueOf(attrInfo.getValueType().name()),
                    attrInfo.getPath(),
                    fieldValue));
        }
    }

    private String wrapEtalonId(String externalId, String etalonId) {
        return etalonId != null ? wrapEtalonId(etalonId) : externalId;
    }

    private String wrapLinkedId(String linkedId) {
        return "li:" + linkedId;
    }

    private String wrapEtalonId(String etalonId) {
        return "et:" + etalonId;
    }

    private void generateReport(XlsxImportState state, String userName,
            RecordsBulkResultDTO recordsUpsert,
            RecordsBulkResultDTO recordsDelete,
            RelationsBulkResultDTO relationsUpsert) {

        StatisticSheet mainSheet = state.toStatistics().stream()
                .filter(statisticSheet -> statisticSheet.getObjectType() == OBJECT_TYPE.ENTITY)
                .findFirst()
                .orElse(null);

        if (mainSheet == null) {
            return;
        }

        final XLSXImportReportBuilder reportBuilder = new XLSXImportReportBuilder(mainSheet.getDisplayName());

        // calculate initial size
        fillTotalCounts(state.toStatistics(), reportBuilder);

        reportBuilder.setRelationNames(state.toStatistics().stream()
                .filter(statisticSheet -> statisticSheet.getObjectType() == OBJECT_TYPE.RELATION)
                .collect(Collectors.toMap(StatisticSheet::getName, StatisticSheet::getDisplayName)));

        reportBuilder.setComplexAttributeNames(state.toStatistics().stream()
                .filter(statisticSheet -> statisticSheet.getObjectType() == OBJECT_TYPE.COMPLEX_ATTRIBUTE)
                .collect(Collectors.toMap(StatisticSheet::getName, StatisticSheet::getDisplayName)));

        fillRecordCounts(recordsUpsert, reportBuilder);

        fillRelationCounts(relationsUpsert, reportBuilder);

        if (Objects.nonNull(recordsDelete) && CollectionUtils.isNotEmpty(recordsDelete.getDeleteRecords())) {
            reportBuilder.incrementDeletedRecords(recordsDelete.getDeleteRecords().stream()
                    .filter(DeleteRecordDTO::wasSuccess)
                    .count());
        }

        UpsertUserEventRequestContext eCtx = UpsertUserEventRequestContext.builder()
                .type(JOB_REPORT_TYPE)
                .content(reportBuilder.generateReport())
                .details(reportBuilder.generateReportDetails())
                .login(userName)
                .build();

        userService.upsert(eCtx);
    }

    private void fillTotalCounts(List<StatisticSheet> statisticBySheets, XLSXImportReportBuilder reportBuilder) {
        for (StatisticSheet statisticSheet : statisticBySheets) {
            switch (statisticSheet.getObjectType()) {
                case ENTITY:
                    reportBuilder.incrementTotalRecords(statisticSheet.getTotalSize());
                    break;
                case RELATION:
                    reportBuilder.incrementTotalRelations(statisticSheet.getName(), statisticSheet.getTotalSize());
                    break;
                case COMPLEX_ATTRIBUTE:
                    reportBuilder.incrementTotalComplexAttributes(statisticSheet.getName(), statisticSheet.getTotalSize());
                    reportBuilder.incrementSkipComplexAttributes(statisticSheet.getName(), statisticSheet.getAdditionalInfoSkipedSize());
                    break;
                default:
                    break;
            }
        }
    }

    private void fillRelationCounts(RelationsBulkResultDTO result, XLSXImportReportBuilder reportBuilder) {

        if (Objects.nonNull(result) && CollectionUtils.isNotEmpty(result.getUpsertRelations())) {

            for (UpsertRelationsDTO upsertRelationsDTO : result.getUpsertRelations()) {

                if (MapUtils.isNotEmpty(upsertRelationsDTO.getRelations())) {
                    upsertRelationsDTO.getRelations().forEach((relationStateDTO, upsertRelationDTOS) ->
                        upsertRelationDTOS.forEach(upsertRelationDTO -> {
                            if (upsertRelationDTO.getAction() == null) {
                                reportBuilder.incrementFailedRelations(relationStateDTO.getRelationName());
                            }
                            switch (upsertRelationDTO.getAction()) {
                                case INSERT:
                                    reportBuilder.incrementInsertedRelations(relationStateDTO.getRelationName());
                                    break;
                                case UPDATE:
                                    reportBuilder.incrementUpdatedRelations(relationStateDTO.getRelationName());
                                    break;
                                case NO_ACTION:
                                default:
                                    break;
                            }
                        })
                    );
                }
            }
        }
    }

    private void fillRecordCounts(RecordsBulkResultDTO result, XLSXImportReportBuilder reportBuilder) {
        if (CollectionUtils.isNotEmpty(result.getUpsertRecords())) {
            for (UpsertRecordDTO upsertRecordDTO : result.getUpsertRecords()) {
                if (upsertRecordDTO.getAction() == null) {
                    reportBuilder.incrementFailedRecords();
                }
                switch (upsertRecordDTO.getAction()) {
                    case INSERT:
                        reportBuilder.incrementInsertedRecords();
                        break;
                    case UPDATE:
                        reportBuilder.incrementUpdatedRecords();
                        break;
                    case NO_ACTION:
                    default:
                        break;
                }
            }
        }
    }

//    private void addAttachment(UserEventDTO userEventDTO, String operationId) {
//
//        byte[] report = auditComponent.getCVSReportBytes(operationId, null, Arrays.asList(XLSXImportCvsHeaders.values()));
//        if (report == null) {
//            return;
//        }
//
//        StringBuilder fileName = new StringBuilder("xlsxImport_")
//                .append(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm")))
//                .append("_")
//                .append(operationId)
//                .append(".csv");
//
//        UpsertLargeObjectContext slorCTX = UpsertLargeObjectContext.builder()
//                .subjectId(userEventDTO.getId())
//                .mimeType("text/x-csv")
//                .binary(true)
//                .input(new ByteArrayInputStream(report))
//                .filename(fileName.toString())
//                .acceptance(LargeObjectAcceptance.ACCEPTED)
//                .build();
//
//        largeObjectsService.saveLargeObject(slorCTX);
//    }

    private void throwImportParseFile(String fileName, Exception e) {
        throw new DataProcessingException("Unable to parse incoming import file [{}]. Invalid format.",
                DataExceptionIds.EX_DATA_XLSX_IMPORT_PARSE_FILE, fileName, e);
    }

    private EntityElement ensureEntityElement(String entityName) {

        EntityElement el = metaModelService.instance(Descriptors.DATA)
                .getElement(entityName);

        if (Objects.isNull(el)) {
            throw new DataProcessingException("Entity with name [{}] was not found in current model.",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_UNKNOWN_ENTITY, entityName);
        }

        return el;
    }
}
