package org.unidata.mdm.rest.v1.dq.core.converter;

import org.unidata.mdm.dq.core.type.model.source.AbstractCleanseFunctionSource;
import org.unidata.mdm.dq.core.type.model.source.CompositeCleanseFunctionSource;
import org.unidata.mdm.dq.core.type.model.source.GroovyCleanseFunctionSource;
import org.unidata.mdm.dq.core.type.model.source.JavaCleanseFunctionSource;
import org.unidata.mdm.dq.core.type.model.source.PythonCleanseFunctionSource;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.CleanseFunctionRO;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.CompositeCleanseFunctionRO;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.GroovyCleanseFunctionRO;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.JavaCleanseFunctionRO;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.PythonCleanseFunctionRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Feb 8, 2021
 */
public class CleanseFunctionConverter extends Converter<AbstractCleanseFunctionSource<?>, CleanseFunctionRO> {

    private static final CompositeCleanseFunctionConverter COMPOSITE_CLEANSE_FUNCTION_CONVERTER
        = new CompositeCleanseFunctionConverter();

    private static final JavaCleanseFunctionConverter JAVA_CLEANSE_FUNCTION_CONVERTER
        = new JavaCleanseFunctionConverter();

    private static final GroovyCleanseFunctionConverter GROOVY_CLEANSE_FUNCTION_CONVERTER
        = new GroovyCleanseFunctionConverter();

    private static final PythonCleanseFunctionConverter PYTHON_CLEANSE_FUNCTION_CONVERTER
        = new PythonCleanseFunctionConverter();
    /**
     * Constructor.
     * @param to
     * @param from
     */
    public CleanseFunctionConverter() {
        super(CleanseFunctionConverter::convert, CleanseFunctionConverter::convert);
    }

    private static CleanseFunctionRO convert(AbstractCleanseFunctionSource<?> source) {

        CleanseFunctionRO target = null;
        switch (source.getType()) {
        case COMPOSITE:
            target = COMPOSITE_CLEANSE_FUNCTION_CONVERTER.to((CompositeCleanseFunctionSource) source);
            break;
        case GROOVY:
            target = GROOVY_CLEANSE_FUNCTION_CONVERTER.to((GroovyCleanseFunctionSource) source);
            break;
        case JAVA:
            target = JAVA_CLEANSE_FUNCTION_CONVERTER.to((JavaCleanseFunctionSource) source);
            break;
        case PYTHON:
            target = PYTHON_CLEANSE_FUNCTION_CONVERTER.to((PythonCleanseFunctionSource) source);
            break;
        default:
            break;
        }

        return target;
    }

    private static AbstractCleanseFunctionSource<?> convert(CleanseFunctionRO source) {

        AbstractCleanseFunctionSource<?> target = null;
        switch (source.getType()) {
        case COMPOSITE:
            target = COMPOSITE_CLEANSE_FUNCTION_CONVERTER.from((CompositeCleanseFunctionRO) source);
            break;
        case GROOVY:
            target = GROOVY_CLEANSE_FUNCTION_CONVERTER.from((GroovyCleanseFunctionRO) source);
            break;
        case JAVA:
            target = JAVA_CLEANSE_FUNCTION_CONVERTER.from((JavaCleanseFunctionRO) source);
            break;
        case PYTHON:
            target = PYTHON_CLEANSE_FUNCTION_CONVERTER.from((PythonCleanseFunctionRO) source);
            break;
        default:
            break;
        }

        return target;
    }
}
