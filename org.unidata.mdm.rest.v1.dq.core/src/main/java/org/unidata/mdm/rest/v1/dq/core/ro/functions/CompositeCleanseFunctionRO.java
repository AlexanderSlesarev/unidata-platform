/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.functions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The Class CleanseFunctionDefinition.
 *
 * @author Michael Yashin. Created on 20.05.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompositeCleanseFunctionRO extends CleanseFunctionRO {
    /**
     * The logic.
     */
    protected CompositeFunctionLogicRO logic;

    public CompositeCleanseFunctionRO() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Schema(description = "COMPOSITE for Composite functions.")
    @Override
    public CleanseFunctionType getType() {
        return CleanseFunctionType.COMPOSITE;
    }

    /**
     * Gets the logic.
     *
     * @return the logic
     */
    public CompositeFunctionLogicRO getLogic() {
        return logic;
    }

    /**
     * Sets the logic.
     *
     * @param logic the new logic
     */
    public void setLogic(CompositeFunctionLogicRO logic) {
        this.logic = logic;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CleanseFunctionDefinition [inputPorts=");
        builder.append(inputPorts);
        builder.append(", outputPorts=");
        builder.append(outputPorts);
        builder.append(", logic=");
        builder.append(logic);
        builder.append("]");
        return builder.toString();
    }
}
