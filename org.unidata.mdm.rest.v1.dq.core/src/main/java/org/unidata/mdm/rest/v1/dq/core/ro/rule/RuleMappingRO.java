package org.unidata.mdm.rest.v1.dq.core.ro.rule;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mikhail Mikhailov on Mar 4, 2021
 */
public class RuleMappingRO {
    /**
     * The rule name.
     */
    private String ruleName;
    /**
     * The LOCAL execution context path.
     */
    private String localPath;
    /**
     * The inputs.
     */
    private List<PortMappingRO> inputs = new ArrayList<>();
    /**
     * The outputs.
     */
    private List<PortMappingRO> outputs = new ArrayList<>();
    /**
     * Constructor.
     */
    public RuleMappingRO() {
        super();
    }
    /**
     * @return the ruleName
     */
    public String getRuleName() {
        return ruleName;
    }
    /**
     * @param ruleName the ruleName to set
     */
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
    /**
     * Gets the execution context path.
     *
     * @return the entryPoint
     */
    public String getLocalPath() {
        return localPath;
    }
    /**
     * Sets the execution context path.
     *
     * @param entryPoint the entryPoint to set
     */
    public void setLocalPath(String entryPoint) {
        this.localPath = entryPoint;
    }
    /**
     * Gets the inputs.
     *
     * @return the inputs
     */
    public List<PortMappingRO> getInputs() {
        return inputs;
    }
    /**
     * Sets the inputs.
     *
     * @param inputs            the inputs to set
     */
    public void setInputs(List<PortMappingRO> inputs) {
        this.inputs = inputs;
    }
    /**
     * Gets the outputs.
     *
     * @return the outputs
     */
    public List<PortMappingRO> getOutputs() {
        return outputs;
    }
    /**
     * Sets the outputs.
     *
     * @param outputs            the outputs to set
     */
    public void setOutputs(List<PortMappingRO> outputs) {
        this.outputs = outputs;
    }
}
