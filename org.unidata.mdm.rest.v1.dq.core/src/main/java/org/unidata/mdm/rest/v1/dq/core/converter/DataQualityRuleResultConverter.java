/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.converter;

import org.unidata.mdm.dq.core.dto.DataQualityResult.RuleExecutionResult;
import org.unidata.mdm.rest.v1.dq.core.ro.result.DataQualityRuleExecutionResultRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Apr 1, 2021
 * One way DQE result.
 */
public class DataQualityRuleResultConverter extends Converter<RuleExecutionResult, DataQualityRuleExecutionResultRO> {
    /**
     * DQE converter.
     */
    private static final DataQualityErrorConverter DATA_QUALITY_ERROR_CONVERTER = new DataQualityErrorConverter();
    /**
     * DQS converter.
     */
    private static final DataQualitySpotConverter DATA_QUALITY_SPOT_CONVERTER = new DataQualitySpotConverter();
    /**
     * Constructor.
     * @param to
     * @param from
     */
    public DataQualityRuleResultConverter() {
        super(DataQualityRuleResultConverter::convert, null);
    }

    private static DataQualityRuleExecutionResultRO convert(RuleExecutionResult source) {

        DataQualityRuleExecutionResultRO target = new DataQualityRuleExecutionResultRO();

        target.setEnriched(source.isEnriched());
        target.setSkipped(source.isSkipped());
        target.setValid(source.isValid());
        target.setSpots(DATA_QUALITY_SPOT_CONVERTER.to(source.getSpots()));
        target.setErrors(DATA_QUALITY_ERROR_CONVERTER.to(source.getErrors()));

        return target;
    }
}
