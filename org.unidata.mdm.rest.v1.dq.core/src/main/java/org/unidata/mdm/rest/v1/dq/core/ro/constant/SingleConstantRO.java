/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro.constant;

/**
 * @author Mikhail Mikhailov on Feb 9, 2021
 */
public class SingleConstantRO extends AbstractConstantRO {

    private SingleValueRO single;

    /**
     * Constructor.
     */
    public SingleConstantRO() {
        super();
    }

    /**
     * Constructor.
     */
    public SingleConstantRO(SingleValueRO single) {
        super();
        setSingle(single);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValueTypeRO getType() {
        return ValueTypeRO.SINGLE;
    }

    /**
     * @return the single
     */
    public SingleValueRO getSingle() {
        return single;
    }

    /**
     * @param single the single to set
     */
    public void setSingle(SingleValueRO single) {
        this.single = single;
    }
}
