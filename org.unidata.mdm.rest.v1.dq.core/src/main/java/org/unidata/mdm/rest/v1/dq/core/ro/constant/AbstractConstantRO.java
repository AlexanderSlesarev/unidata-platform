/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro.constant;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import io.swagger.v3.oas.annotations.media.DiscriminatorMapping;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Feb 9, 2021
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type", visible = false)
@JsonSubTypes({
    @Type(value = ArrayConstantRO.class, name = "ARRAY"),
    @Type(value = SingleConstantRO.class, name = "SINGLE")
})
@Schema(type = "object",
        subTypes = {ArrayConstantRO.class, SingleConstantRO.class},
        discriminatorMapping = {
                @DiscriminatorMapping(value = "ARRAY", schema = ArrayConstantRO.class),
                @DiscriminatorMapping(value = "SINGLE", schema = SingleConstantRO.class)
        },
        discriminatorProperty = "type")
public abstract class AbstractConstantRO {

    protected AbstractConstantRO() {
        super();
    }

    public abstract ValueTypeRO getType();
}
