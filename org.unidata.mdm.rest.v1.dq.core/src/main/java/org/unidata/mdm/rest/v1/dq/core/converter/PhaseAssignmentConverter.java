/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.converter;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.dq.core.type.model.source.assignment.PhaseAssignmentSource;
import org.unidata.mdm.dq.core.util.DQUtils;
import org.unidata.mdm.rest.v1.dq.core.ro.assignment.PhaseAssignmentRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Nov 3, 2021
 */
public class PhaseAssignmentConverter extends Converter<PhaseAssignmentSource, PhaseAssignmentRO> {
    /**
     * Constructor.
     */
    public PhaseAssignmentConverter() {
        super(PhaseAssignmentConverter::convert, PhaseAssignmentConverter::convert);
    }

    private static PhaseAssignmentRO convert(PhaseAssignmentSource source) {

        PhaseAssignmentRO target = new PhaseAssignmentRO();

        target.setPhase(source.getPhaseName());
        target.setSets(source.getSets());

        return target;
    }

    private static PhaseAssignmentSource convert(PhaseAssignmentRO source) {

        // Treat blank|null phase name as default phase.
        return new PhaseAssignmentSource()
                .withPhaseName(StringUtils.defaultIfBlank(StringUtils.trim(source.getPhase()), DQUtils.DEFAULT_PHASE_ID))
                .withSets(source.getSets());
    }
}
