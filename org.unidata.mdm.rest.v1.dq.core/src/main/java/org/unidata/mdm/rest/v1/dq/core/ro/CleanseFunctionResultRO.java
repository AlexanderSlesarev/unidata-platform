package org.unidata.mdm.rest.v1.dq.core.ro;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * @author Mikhail Mikhailov on Feb 9, 2021
 */
public class CleanseFunctionResultRO extends DetailedOutputRO {
    /**
     * Success mark.
     */
    boolean success;
    /**
     * Constructor.
     */
    public CleanseFunctionResultRO() {
        super();
    }
    /**
     * Constructor.
     */
    public CleanseFunctionResultRO(boolean success) {
        super();
        this.success = success;
    }
    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }
    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }
}
