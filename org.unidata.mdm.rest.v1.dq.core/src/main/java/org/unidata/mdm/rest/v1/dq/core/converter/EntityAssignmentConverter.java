/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.converter;

import org.unidata.mdm.dq.core.type.model.source.assignment.EntityAssignmentSource;
import org.unidata.mdm.rest.v1.dq.core.ro.assignment.EntityAssignmentRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Mar 1, 2021
 */
public class EntityAssignmentConverter extends Converter<EntityAssignmentSource, EntityAssignmentRO> {
    /**
     *
     */
    private static final PhaseAssignmentConverter PHASE_ASSIGNMENT_CONVERTER = new PhaseAssignmentConverter();
    /**
     * Constructor.
     * @param to
     * @param from
     */
    public EntityAssignmentConverter() {
        super(EntityAssignmentConverter::convert, EntityAssignmentConverter::convert);
    }

    private static EntityAssignmentRO convert(EntityAssignmentSource source) {

        EntityAssignmentRO target = new EntityAssignmentRO();

        target.setEntityName(source.getEntityName());
        target.setAssignment(PHASE_ASSIGNMENT_CONVERTER.to(source.getPhases()));

        return target;
    }

    private static EntityAssignmentSource convert(EntityAssignmentRO source) {
        return new EntityAssignmentSource()
                .withEntityName(source.getEntityName())
                .withPhases(PHASE_ASSIGNMENT_CONVERTER.from(source.getAssignment()));
    }
}
