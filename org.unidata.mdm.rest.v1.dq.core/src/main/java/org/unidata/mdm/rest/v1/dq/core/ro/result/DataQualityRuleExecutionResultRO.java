/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro.result;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Mikhail Mikhailov on Apr 1, 2021
 */
public class DataQualityRuleExecutionResultRO {
    /**
     * Skip indicator.
     */
    private boolean skipped;
    /**
     * The overall rule's validity state - is true,
     * if all cycles completed with true.
     */
    private boolean valid;
    /**
     * Is true, if the rule produced some successful enrichments.
     */
    private boolean enriched;
    /**
     * The errors.
     */
    private List<DataQualityErrorRO> errors;
    /**
     * Spots.
     */
    private List<DataQualitySpotRO> spots;
    /**
     * Constructor.
     */
    public DataQualityRuleExecutionResultRO() {
        super();
    }
    /**
     * @return the skipped
     */
    public boolean isSkipped() {
        return skipped;
    }
    /**
     * @param skipped the skipped to set
     */
    public void setSkipped(boolean skipped) {
        this.skipped = skipped;
    }
    /**
     * @return the valid
     */
    public boolean isValid() {
        return valid;
    }
    /**
     * @param valid the valid to set
     */
    public void setValid(boolean valid) {
        this.valid = valid;
    }
    /**
     * @return the enriched
     */
    public boolean isEnriched() {
        return enriched;
    }
    /**
     * @param enriched the enriched to set
     */
    public void setEnriched(boolean enriched) {
        this.enriched = enriched;
    }
    /**
     * @return the errors
     */
    public List<DataQualityErrorRO> getErrors() {
        return Objects.isNull(errors) ? Collections.emptyList() : errors;
    }
    /**
     * @param errors the errors to set
     */
    public void setErrors(List<DataQualityErrorRO> errors) {
        this.errors = errors;
    }
    /**
     * @return the spots
     */
    public List<DataQualitySpotRO> getSpots() {
        return Objects.isNull(spots) ? Collections.emptyList() : spots;
    }
    /**
     * @param spots the spots to set
     */
    public void setSpots(List<DataQualitySpotRO> spots) {
        this.spots = spots;
    }
}
