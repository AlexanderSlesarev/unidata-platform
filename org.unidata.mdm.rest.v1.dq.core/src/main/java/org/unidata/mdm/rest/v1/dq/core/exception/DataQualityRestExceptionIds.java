package org.unidata.mdm.rest.v1.dq.core.exception;

import org.unidata.mdm.system.exception.ExceptionId;

/**
 * @author Mikhail Mikhailov on Feb 9, 2021
 */
public final class DataQualityRestExceptionIds {
    /**
     * Constructor.
     */
    private DataQualityRestExceptionIds() {
        super();
    }
    /**
     * Function not found by id.
     */
    public static final ExceptionId EX_DQ_EXECUTE_FUNCTION_NOT_FOUND =
            new ExceptionId("EX_DQ_EXECUTE_FUNCTION_NOT_FOUND", "app.dq.execute.function.not.found");
    /**
     * Function not found by id.
     */
    public static final ExceptionId EX_DQ_SUGGEST_LIBRARY_NOT_FOUND =
            new ExceptionId("EX_DQ_SUGGEST_LIBRARY_NOT_FOUND", "app.dq.suggest.library.not.found");

    public static final ExceptionId EX_DQ_IMPORT_MODEL_TYPE_UNSUPPORTED =
            new ExceptionId("EX_DQ_IMPORT_MODEL_TYPE_UNSUPPORTED", "app.dq.import.model.type.unsupported");

    public static final ExceptionId EX_DQ_IMPORT_MODEL_EMPTY_INPUT =
            new ExceptionId("EX_DQ_IMPORT_MODEL_EMPTY_INPUT", "app.dq.import.model.empty.input");

    public static final ExceptionId EX_DQ_IMPORT_MODEL_MARSHALING_FAILED =
            new ExceptionId("EX_DQ_IMPORT_MODEL_MARSHALING_FAILED", "app.dq.import.model.marshaling.failed");

    public static final ExceptionId EX_DQ_IMPORT_MODEL_INVALID_FILE_FORMAT =
            new ExceptionId("EX_DQ_IMPORT_MODEL_INVALID_FILE_FORMAT", "app.dq.import.model.invalid.file.format");
}
