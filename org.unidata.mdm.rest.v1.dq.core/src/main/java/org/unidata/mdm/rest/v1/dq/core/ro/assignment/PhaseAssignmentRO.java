/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro.assignment;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Nov 3, 2021
 * Phase to set assignment.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PhaseAssignmentRO {
    /**
     * Phase name.
     */
    @Schema(description = "Valid phase name (phase must be created) or null. Must be unique across entity assignment.")
    private String phase;
    /**
     * The set names.
     */
    @Schema(description = "List of set names. No duplicates allowed.")
    private List<String> sets;
    /**
     * Constructor.
     */
    public PhaseAssignmentRO() {
        super();
    }
    /**
     * @return the phase
     */
    public String getPhase() {
        return phase;
    }
    /**
     * @param phase the phase to set
     */
    public void setPhase(String phase) {
        this.phase = phase;
    }
    /**
     * @return the sets
     */
    public List<String> getSets() {
        return Objects.isNull(sets) ? Collections.emptyList() : sets;
    }
    /**
     * @param sets the sets to set
     */
    public void setSets(List<String> sets) {
        this.sets = sets;
    }
}
