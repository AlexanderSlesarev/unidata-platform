/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.converter;

import org.unidata.mdm.dq.core.type.model.source.JavaCleanseFunctionSource;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.JavaCleanseFunctionRO;

/**
 * @author Mikhail Mikhailov on Feb 8, 2021
 */
public class JavaCleanseFunctionConverter extends AbstractCleanseFunctionConverter<JavaCleanseFunctionSource, JavaCleanseFunctionRO> {
    /**
     * Constructor.
     * @param in
     * @param out
     */
    public JavaCleanseFunctionConverter() {
        super(JavaCleanseFunctionConverter::convert, JavaCleanseFunctionConverter::convert);
    }

    private static JavaCleanseFunctionRO convert(JavaCleanseFunctionSource source) {

        JavaCleanseFunctionRO target = new JavaCleanseFunctionRO();

        convert(source, target);

        target.setJavaClass(source.getJavaClass());
        target.setLibraryName(source.getLibraryName());
        target.setLibraryVersion(source.getLibraryVersion());

        return target;
    }

    private static JavaCleanseFunctionSource convert(JavaCleanseFunctionRO source) {

        JavaCleanseFunctionSource target = new JavaCleanseFunctionSource();

        convert(source, target);

        return target
            .withJavaClass(source.getJavaClass())
            .withLibraryName(source.getLibraryName())
            .withLibraryVersion(source.getLibraryVersion());
    }
}
