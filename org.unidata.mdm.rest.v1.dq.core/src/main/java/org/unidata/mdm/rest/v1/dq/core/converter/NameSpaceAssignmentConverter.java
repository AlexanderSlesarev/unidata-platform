/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.converter;

import org.unidata.mdm.dq.core.type.model.source.assignment.NameSpaceAssignmentSource;
import org.unidata.mdm.rest.v1.dq.core.ro.assignment.NamespaceAssignmentRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Mar 1, 2021
 */
public class NameSpaceAssignmentConverter extends Converter<NameSpaceAssignmentSource, NamespaceAssignmentRO> {
    /**
     * EAC.
     */
    private static final EntityAssignmentConverter ENTITY_ASSIGNMENT_CONVERTER = new EntityAssignmentConverter();
    /**
     * Constructor.
     * @param to
     * @param from
     */
    public NameSpaceAssignmentConverter() {
        super(NameSpaceAssignmentConverter::convert, NameSpaceAssignmentConverter::convert);
    }

    private static NamespaceAssignmentRO convert(NameSpaceAssignmentSource source) {

        NamespaceAssignmentRO target = new NamespaceAssignmentRO();

        target.setNameSpace(source.getNameSpace());
        target.setAssignments(ENTITY_ASSIGNMENT_CONVERTER.to(source.getAssignments()));

        return target;
    }

    private static NameSpaceAssignmentSource convert(NamespaceAssignmentRO source) {
        return new NameSpaceAssignmentSource()
                .withNameSpace(source.getNameSpace())
                .withAssignments(ENTITY_ASSIGNMENT_CONVERTER.from(source.getAssignments()));
    }
}
