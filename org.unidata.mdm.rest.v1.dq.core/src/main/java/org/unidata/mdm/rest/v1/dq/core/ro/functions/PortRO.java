/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.functions;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortFilteringMode;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortInputType;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortValueType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;


/**
 * The Class PortDefinition.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortRO {

    /**
     * The name.
     */
    @JsonProperty(value="name")
    private String name;
    /**
     * Display name.
     */
    @JsonProperty(value="displayName")
    private String displayName;
    /**
     * The description.
     */
    @JsonProperty(value="description")
    private String description;
    /**
     * The required indicator.
     */
    @JsonProperty(value="required")
    private boolean required;
    /**
     * One of {@link CleanseFunctionPortFilteringMode}.
     */
    @Schema(allowableValues = { "MODE_ALL", "MODE_ALL_WITH_INCOMPLETE", "MODE_ONCE", "MODE_UNDEFINED" },
            description = "Filtering mode denotes, how UPath collects attributes. "
                    + "MODE_ALL means \"collect all hits (attributes) from all subtrees, which may be filtered by UPath\". "
                    + "MODE_ALL_WITH_INCOMPLETE means the same as MODE_ALL + paths, that were requested, but could not be located (missing attributes), are also written to spacial storage in UPathResult. "
                    + "MODE_ONCE - means filtering stops after first hit. MODE_UNDEFINED means \"undefined\" (function selection). "
                    + "Most functions do not support autoselection, so one of the first three must be set.")
    private String filteringMode;
    /**
     * Valid combination of {@link CleanseFunctionPortInputType}.
     */
    @Schema(allowableValues = {"SIMPLE", "ARRAY", "CODE", "COMPLEX", "RECORD", "ANY"}, 
            description = "Accepted port input types.")
    private List<String> inputTypes;
    /**
     * Valid combination of {@link CleanseFunctionPortValueType}.
     */
    @Schema(allowableValues = 
        {"STRING", "DICTIONARY", "INTEGER", "NUMBER", "BOOLEAN", "BLOB", "CLOB", "DATE", "TIME", "TIMESTAMP", "ENUM", "LINK", "MEASURED", "ANY"}, 
            description = "Accepted value types.")
    private List<String> valueTypes;

    public PortRO(){
        super();
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Checks if is required.
     *
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * Sets the required.
     *
     * @param required            the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * @return the filteringMode
     */
    public String getFilteringMode() {
        return filteringMode;
    }

    /**
     * @param filteringMode the filteringMode to set
     */
    public void setFilteringMode(String filteringMode) {
        this.filteringMode = filteringMode;
    }

    /**
     * @return the inputTypes
     */
    public List<String> getInputTypes() {
        return Objects.isNull(inputTypes) ? Collections.emptyList() : inputTypes;
    }

    /**
     * @param inputTypes the inputTypes to set
     */
    public void setInputTypes(List<String> inputType) {
        this.inputTypes = inputType;
    }

    /**
     * @return the valueTypes
     */
    public List<String> getValueTypes() {
        return Objects.isNull(valueTypes) ? Collections.emptyList() : valueTypes;
    }

    /**
     * @param valueTypes the valueTypes to set
     */
    public void setValueTypes(List<String> valueType) {
        this.valueTypes = valueType;
    }
}
