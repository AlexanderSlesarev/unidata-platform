/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.core.ro.CustomPropertyRO;
import org.unidata.mdm.rest.v1.dq.core.ro.execute.FunctionExecutionParamRO;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class CleanseFunctionDataRo.
 */
public class ExecuteFunctionRequestRO {
    /**
     * The name of the function to execute.
     */
    @JsonProperty(value = "functionName")
    private String functionName;
    /**
     * The params.
     */
    @JsonProperty(value = "params")
    private List<FunctionExecutionParamRO> params;
    /**
     * Custom properties.
     */
    private List<CustomPropertyRO> customProperties;
    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getFunctionName() {
        return functionName;
    }
    /**
     * Sets the name.
     *
     * @param functionName the name to set
     */
    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }
    /**
     * Gets the simple attributes.
     *
     * @return the simpleAttributes
     */
    public List<FunctionExecutionParamRO> getParams() {
        return params;
    }
    /**
     * Sets the simple attributes.
     *
     * @param params the simpleAttributes to set
     */
    public void setParams(List<FunctionExecutionParamRO> params) {
        this.params = params;
    }
    /**
     * @return the customProperties
     */
    public List<CustomPropertyRO> getCustomProperties() {
        return Objects.isNull(customProperties) ? Collections.emptyList() : customProperties;
    }
    /**
     * @param customProperties the customProperties to set
     */
    public void setCustomProperties(List<CustomPropertyRO> customProperties) {
        this.customProperties = customProperties;
    }
}
