/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro.result;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Mikhail Mikhailov on Jun 8, 2021
 */
public class DataQualityRuleResultRO {
    /**
     * Rule name.
     */
    private String ruleName;
    /**
     * Rule display name.
     */
    private String ruleDisplayName;
    /**
     * Rule execution results.
     */
    private List<DataQualityRuleExecutionResultRO> executions;
    /**
     * Constructor.
     */
    public DataQualityRuleResultRO() {
        super();
    }
    /**
     * Constructor.
     */
    public DataQualityRuleResultRO(String name, String displayName, List<DataQualityRuleExecutionResultRO> executions) {
        super();
        this.ruleName = name;
        this.ruleDisplayName = displayName;
        this.executions = executions;
    }
    /**
     * Gets the rule name.
     *
     * @return the ruleName
     */
    public String getRuleName() {
        return ruleName;
    }
    /**
     * Sets the rule name.
     *
     * @param ruleName the ruleName to set
     */
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
    /**
     * @return the ruleDisplayName
     */
    public String getRuleDisplayName() {
        return ruleDisplayName;
    }
    /**
     * @param ruleDisplayName the ruleDisplayName to set
     */
    public void setRuleDisplayName(String ruleDisplayName) {
        this.ruleDisplayName = ruleDisplayName;
    }
    /**
     * @return the executions
     */
    public List<DataQualityRuleExecutionResultRO> getExecutions() {
        return Objects.isNull(executions) ? Collections.emptyList() : executions;
    }
    /**
     * @param executions the executions to set
     */
    public void setExecutions(List<DataQualityRuleExecutionResultRO> executions) {
        this.executions = executions;
    }
}
