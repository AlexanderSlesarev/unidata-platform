/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro.result;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Mikhail Mikhailov on Apr 1, 2021
 * Set results to RO.
 */
public class DataQualitySetResultRO {
    /**
     * Rule set name.
     */
    private String setName;
    /**
     * Set's display name.
     */
    private String setDisplayName;
    /**
     * The rules result.
     */
    private List<DataQualityRuleResultRO> rules;
    /**
     * Constructor.
     */
    public DataQualitySetResultRO() {
        super();
    }
    /**
     * @return the setName
     */
    public String getSetName() {
        return setName;
    }
    /**
     * @param setName the setName to set
     */
    public void setSetName(String setName) {
        this.setName = setName;
    }
    /**
     * @return the setDisplayName
     */
    public String getSetDisplayName() {
        return setDisplayName;
    }
    /**
     * @param setDisplayName the setDisplayName to set
     */
    public void setSetDisplayName(String setDisplayName) {
        this.setDisplayName = setDisplayName;
    }
    /**
     * @return the rules
     */
    public List<DataQualityRuleResultRO> getRules() {
        return Objects.isNull(rules) ? Collections.emptyList() : rules;
    }
    /**
     * @param rules the rules to set
     */
    public void setRules(List<DataQualityRuleResultRO> rules) {
        this.rules = rules;
    }
}
