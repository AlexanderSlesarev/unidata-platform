/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro;

import org.unidata.mdm.rest.v1.dq.core.ro.functions.CleanseFunctionRO;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.CompositeCleanseFunctionRO;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.GroovyCleanseFunctionRO;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.JavaCleanseFunctionRO;
import org.unidata.mdm.rest.v1.dq.core.ro.functions.PythonCleanseFunctionRO;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Feb 9, 2021
 */
public class UpsertCleanseFunctionRO {

    @Schema(subTypes = {
            JavaCleanseFunctionRO.class,
            CompositeCleanseFunctionRO.class,
            GroovyCleanseFunctionRO.class,
            PythonCleanseFunctionRO.class
    }, description = "A function of exact type - Java, Composite, Python or Groovy.")
    private CleanseFunctionRO function;

    /**
     * Constructor.
     */
    public UpsertCleanseFunctionRO() {
        super();
    }

    /**
     * @return the function
     */
    public CleanseFunctionRO getFunction() {
        return function;
    }

    /**
     * @param function the function to set
     */
    public void setFunction(CleanseFunctionRO function) {
        this.function = function;
    }

}
