/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.rule;

import org.unidata.mdm.rest.v1.dq.core.ro.constant.AbstractConstantRO;
import org.unidata.mdm.rest.v1.dq.core.ro.constant.ArrayConstantRO;
import org.unidata.mdm.rest.v1.dq.core.ro.constant.SingleConstantRO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * The Class DQRMappingDefinition.
 *
 * @author Michael Yashin. Created on 11.06.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortMappingRO {
    /**
     * The attribute name.
     */
    protected String path;
    /**
     * The function port.
     */
    protected String portName;
    /**
     * Constant
     */
    @Schema(type = "object", oneOf = { ArrayConstantRO.class, SingleConstantRO.class })
    protected AbstractConstantRO constant;
    /**
     * Gets the attribute name.
     *
     * @return the attribute name
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets the attribute name.
     *
     * @param attributeName
     *            the new attribute name
     */
    public void setPath(String attributeName) {
        this.path = attributeName;
    }

    /**
     * Gets the function port.
     *
     * @return the functionPort
     */
    public String getPortName() {
        return portName;
    }

    /**
     * Sets the function port.
     *
     * @param functionPort
     *            the functionPort to set
     */
    public void setPortName(String functionPort) {
        this.portName = functionPort;
    }

    /**
     * @return the attributeConstantValue
     */
    public AbstractConstantRO getConstant() {
        return constant;
    }

    /**
     * @param attributeConstantValue the attributeConstantValue to set
     */
    public void setConstant(AbstractConstantRO attributeConstantValue) {
        this.constant = attributeConstantValue;
    }
}
