/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.functions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The Class FunctionLogic.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompositeFunctionLogicRO {

    /** The nodes. */
    private List<CompositeFunctionNodeRO> nodes;

    /** The links. */
    private List<CompositeFunctionTransitionRO> transitions;

    public CompositeFunctionLogicRO() {
        super();
    }

    /**
     * Gets the nodes.
     *
     * @return the nodes
     */
    public List<CompositeFunctionNodeRO> getNodes() {
        return Objects.isNull(nodes) ? Collections.emptyList() : nodes;
    }

    /**
     * Sets the nodes.
     *
     * @param nodes
     *            the new nodes
     */
    public void setNodes(List<CompositeFunctionNodeRO> nodes) {
        this.nodes = nodes;
    }

    /**
     * Adds new node to node list.
     *
     * @param node
     *            new node.
     */
    public void addNode(CompositeFunctionNodeRO node) {
        if (this.nodes == null) {
            this.nodes = new ArrayList<>();
        }
        this.nodes.add(node);
    }

    /**
     * Gets the links.
     *
     * @return the links
     */
    public List<CompositeFunctionTransitionRO> getTransitions() {
        return Objects.isNull(transitions) ? Collections.emptyList() : transitions;
    }

    /**
     * Sets the links.
     *
     * @param links
     *            the new links
     */
    public void setTransitions(List<CompositeFunctionTransitionRO> links) {
        this.transitions = links;
    }

    /**
     * Add new node link.
     *
     * @param link
     *            node link.
     */
    public void addLink(CompositeFunctionTransitionRO link) {
        if (this.transitions == null) {
            this.transitions = new ArrayList<>();
        }
        this.transitions.add(link);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FunctionLogic [nodes=");
        builder.append(nodes);
        builder.append(", links=");
        builder.append(transitions);
        builder.append("]");
        return builder.toString();
    }
}
