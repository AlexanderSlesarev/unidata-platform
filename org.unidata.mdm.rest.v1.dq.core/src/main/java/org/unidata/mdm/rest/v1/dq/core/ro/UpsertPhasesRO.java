/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Mikhail Mikhailov on Nov 3, 2021
 * Phases container.
 */
public class UpsertPhasesRO {
    /**
     * Quality phase names.
     */
    private List<String> phases;
    /**
     * Constructor.
     */
    public UpsertPhasesRO() {
        super();
    }
    /**
     * Constructor.
     */
    public UpsertPhasesRO(Collection<String> phases) {
        super();
        this.phases = Objects.isNull(phases) ? Collections.emptyList() : new ArrayList<>(phases);
    }
    /**
     * @return the phases
     */
    public List<String> getPhases() {
        return Objects.isNull(phases) ? Collections.emptyList() : phases;
    }
    /**
     * @param phases the phases to set
     */
    public void setPhases(List<String> phases) {
        this.phases = phases;
    }
}
