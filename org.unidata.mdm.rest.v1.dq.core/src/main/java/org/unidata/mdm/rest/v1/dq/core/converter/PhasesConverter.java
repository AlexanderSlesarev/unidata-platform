/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.converter;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.dq.core.type.model.source.PhaseSource;
import org.unidata.mdm.dq.core.util.DQUtils;
import org.unidata.mdm.rest.core.converter.CustomPropertiesConverter;
import org.unidata.mdm.rest.v1.dq.core.ro.phase.QualityPhaseRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Nov 9, 2021
 */
public class PhasesConverter extends Converter<PhaseSource, QualityPhaseRO> {
    /**
     * Constructor.
     */
    public PhasesConverter() {
        super(PhasesConverter::convert, PhasesConverter::convert);
    }

    private static QualityPhaseRO convert(PhaseSource source) {

        QualityPhaseRO target = new QualityPhaseRO();

        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        target.setDescription(source.getDescription());
        target.setCustomProperties(CustomPropertiesConverter.to(source.getCustomProperties()));

        return target;
    }

    private static PhaseSource convert(QualityPhaseRO source) {
        return new PhaseSource()
                .withName(StringUtils.defaultIfBlank(StringUtils.trim(source.getName()), DQUtils.DEFAULT_PHASE_ID))
                .withDisplayName(source.getDisplayName())
                .withDescription(source.getDescription())
                .withCustomProperties(CustomPropertiesConverter.from(source.getCustomProperties()));
    }
}
