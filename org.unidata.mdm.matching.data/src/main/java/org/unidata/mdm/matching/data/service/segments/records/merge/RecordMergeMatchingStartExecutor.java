/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.merge;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.matching.data.context.RecordMatchingMergeContext;
import org.unidata.mdm.matching.data.dto.RecordMatchingMergeResult;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.type.apply.RecordMatchingUpsertChangeSet;
import org.unidata.mdm.matching.data.type.state.RecordMatchingState;
import org.unidata.mdm.system.type.pipeline.Start;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 03.11.2021
 */
@Component(RecordMergeMatchingStartExecutor.SEGMENT_ID)
public class RecordMergeMatchingStartExecutor extends Start<RecordMatchingMergeContext, RecordMatchingMergeResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[RECORD_MERGE_MATCHING_START]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".record.merge.matching.start.description";

    /**
     * Constructor.
     */
    public RecordMergeMatchingStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, RecordMatchingMergeContext.class, RecordMatchingMergeResult.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(RecordMatchingMergeContext ctx) {

        ctx.matchingState(new RecordMatchingState());

        if (Objects.isNull(ctx.changeSet())) {
            ctx.changeSet(new RecordMatchingUpsertChangeSet());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String subject(RecordMatchingMergeContext ctx) {
        Timeline<OriginRecord> t = ctx.nextTimeline();
        return Objects.nonNull(t) && Objects.nonNull(t.getKeys()) ? t.<RecordKeys>getKeys().getEntityName() : null;
    }
}
