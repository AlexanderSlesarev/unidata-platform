/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.merge;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.matching.data.context.RecordMatchingMergeContext;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.type.state.RecordMatchingState;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 03.11.2021
 */
@Component(RecordMergeMatchingPrepareExecutor.SEGMENT_ID)
public class RecordMergeMatchingPrepareExecutor extends Point<RecordMatchingMergeContext> {

    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[RECORD_MERGE_MATCHING_PREPARE]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".record.merge.matching.prepare.description";

    /**
     * Constructor.
     */
    public RecordMergeMatchingPrepareExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RecordMatchingMergeContext ctx) {
        prepareUpdates(ctx);
        prepareDeletes(ctx);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RecordMatchingMergeContext.class.isAssignableFrom(start.getInputTypeClass());
    }

    private void prepareDeletes(RecordMatchingMergeContext ctx) {

        RecordKeys keys = ctx.getPayload().keys();
        Timeline<OriginRecord> current = ctx.currentTimeline();
        RecordMatchingState state = ctx.matchingState();

        current.forEach(interval -> state.putDelete(MatchingRecordKey.of(
                ctx.getNamespace().getId(),
                keys.getEntityName(),
                keys.getEtalonKey().getId(),
                interval.getValidFrom(),
                interval.getValidTo())));

        for (Timeline<OriginRecord> dt : ctx.duplicateTimelines().values()) {

            RecordKeys dk = dt.getKeys();

            dt.forEach(interval -> state.putDelete(MatchingRecordKey.of(
                    ctx.getNamespace().getId(),
                    dk.getEntityName(),
                    dk.getEtalonKey().getId(),
                    interval.getValidFrom(),
                    interval.getValidTo())));
        }
    }

    private void prepareUpdates(RecordMatchingMergeContext ctx) {

        Timeline<OriginRecord> next = ctx.nextTimeline();
        if (next.isEmpty()) {
            return;
        }

        NamespaceAssignmentElement assignment = ctx.getAssignment();
        RecordMatchingState state = ctx.matchingState();

        for (TimeInterval<OriginRecord> i : next) {

            EtalonRecord etalon = i.getCalculationResult();
            if (Objects.isNull(etalon) || etalon.getInfoSection().getStatus() == RecordStatus.INACTIVE) {
                continue;
            }

            MatchingRecordKey key = MatchingRecordKey.of(
                    assignment.getNameSpace().getId(),
                    assignment.getTypeName(),
                    etalon.getInfoSection().getEtalonKey().getId(),
                    etalon.getInfoSection().getValidFrom(),
                    etalon.getInfoSection().getValidTo()
            );

            state.putCalculation(i, key, etalon);
        }
    }
}
