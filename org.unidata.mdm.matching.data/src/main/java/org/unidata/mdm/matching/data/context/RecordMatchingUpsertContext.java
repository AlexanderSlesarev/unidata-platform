/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.context;

import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.matching.data.service.segments.records.upsert.RecordUpsertMatchingStartExecutor;

/**
 * @author Sergey Murskiy on 06.08.2021
 */
public class RecordMatchingUpsertContext extends DataMatchingContext implements DataMatchingCalculationContext<OriginRecord> {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 3714440837488986097L;

    /**
     * Constructor.
     *
     * @param b the builder
     */
    private RecordMatchingUpsertContext(RecordMatchingUpsertContextBuilder b) {
        super(b);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return RecordUpsertMatchingStartExecutor.SEGMENT_ID;
    }

    /**
     * Builder shorthand.
     *
     * @return builder
     */
    public static RecordMatchingUpsertContextBuilder builder() {
        return new RecordMatchingUpsertContextBuilder();
    }

    /**
     * Builder for the context.
     */
    public static class RecordMatchingUpsertContextBuilder extends DataMatchingContextBuilder<RecordMatchingUpsertContextBuilder> {
        /**
         * {@inheritDoc}
         */
        @Override
        public RecordMatchingUpsertContext build() {
            return new RecordMatchingUpsertContext(this);
        }
    }
}
