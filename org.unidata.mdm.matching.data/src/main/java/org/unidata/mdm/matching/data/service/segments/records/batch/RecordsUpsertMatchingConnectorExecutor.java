/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.batch;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.data.context.DataContextFlags;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.type.apply.batch.impl.RecordUpsertBatchSetAccumulator;
import org.unidata.mdm.matching.core.configuration.MatchingDescriptors;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.matching.data.context.RecordMatchingUpsertContext;
import org.unidata.mdm.matching.data.dto.RecordsMatchingBulkResult;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.service.segments.MatchingAssignmentSupport;
import org.unidata.mdm.matching.data.type.apply.batch.RecordMatchingUpsertBatchSetAccumulator;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.instance.DataModelInstance;
import org.unidata.mdm.system.service.ExecutionService;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.pipeline.batch.BatchedConnector;
import org.unidata.mdm.system.type.pipeline.batch.BatchedPipelineInput;
import org.unidata.mdm.system.type.pipeline.fragment.InputFragmentContainer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 02.09.2021
 */
@Component(RecordsUpsertMatchingConnectorExecutor.SEGMENT_ID)
public class RecordsUpsertMatchingConnectorExecutor extends BatchedConnector<BatchedPipelineInput, RecordsMatchingBulkResult>
        implements MatchingAssignmentSupport {

    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[BATCH_RECORD_UPSERT_MATCHING_CONNECTOR]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".batch.record.upsert.matching.connector.description";
    /**
     * The ES instance.
     */
    @Autowired
    private ExecutionService executionService;
    /**
     * Metamodel service.
     */
    @Autowired
    private MetaModelService metaModelService;

    /**
     * Constructor.
     */
    public RecordsUpsertMatchingConnectorExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RecordsMatchingBulkResult connect(BatchedPipelineInput ctx) {

        InputFragmentContainer target = (InputFragmentContainer) ctx;
        RecordMatchingUpsertBatchSetAccumulator payload = target.fragment(RecordMatchingUpsertBatchSetAccumulator.ID);

        if (Objects.isNull(payload)) {
            payload = accumulate((RecordUpsertBatchSetAccumulator) ctx);
        }

        return execute(payload, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RecordsMatchingBulkResult connect(BatchedPipelineInput ctx, Pipeline p) {

        InputFragmentContainer target = (InputFragmentContainer) ctx;
        RecordMatchingUpsertBatchSetAccumulator payload = target.fragment(RecordMatchingUpsertBatchSetAccumulator.ID);

        if (Objects.isNull(payload)) {
            payload = accumulate((RecordUpsertBatchSetAccumulator) ctx);
        }

        return execute(payload, p);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return super.supports(start) && BatchedPipelineInput.class.isAssignableFrom(start.getInputTypeClass());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MetaModelService metaModelService() {
        return metaModelService;
    }

    /**
     * Execute.
     *
     * @param bsa accumulator
     * @param p   pipeline
     * @return bulk result
     */
    public RecordsMatchingBulkResult execute(RecordMatchingUpsertBatchSetAccumulator bsa, Pipeline p) {

        if (Objects.isNull(p)) {
            return executionService.execute(bsa);
        } else {
            return executionService.execute(p, bsa);
        }
    }

    private RecordMatchingUpsertBatchSetAccumulator accumulate(RecordUpsertBatchSetAccumulator bsa) {

        if (CollectionUtils.isEmpty(bsa.workingCopy())) {
            return null;
        }

        List<RecordMatchingUpsertContext> matchingPayload = new ArrayList<>(bsa.workingCopy().size());
        DataModelInstance dataModelInstance = metaModelService.instance(Descriptors.DATA);
        MatchingModelInstance matchingModelInstance = metaModelService.instance(MatchingDescriptors.MATCHING);

        for (UpsertRequestContext upsertCtx : bsa.workingCopy()) {

            RecordMatchingUpsertContext result = append(upsertCtx, dataModelInstance, matchingModelInstance);
            if (Objects.nonNull(result)) {
                matchingPayload.add(result);
            }
        }

        RecordMatchingUpsertBatchSetAccumulator payload
                = new RecordMatchingUpsertBatchSetAccumulator(matchingPayload.size());
        payload.charge(matchingPayload);

        return payload;
    }

    @SuppressWarnings("DuplicatedCode")
    private RecordMatchingUpsertContext append(UpsertRequestContext upsertCtx, DataModelInstance dataInstance,
                                               MatchingModelInstance matchingInstance) {

        NamespaceAssignmentElement assignment = getAssignment(upsertCtx.keys(), dataInstance, matchingInstance);

        if (Objects.isNull(assignment)) {
            return null;
        }

        RecordMatchingUpsertContext in = RecordMatchingUpsertContext.builder()
                .assignment(assignment)
                .namespace(assignment.getNameSpace())
                .payload(upsertCtx)
                .operationId(upsertCtx.getOperationId())
                .build();

        in.currentTimeline(upsertCtx.currentTimeline());
        in.nextTimeline(upsertCtx.nextTimeline());
        in.operationType(upsertCtx.operationType());
        in.setFlag(DataContextFlags.FLAG_BATCH_OPERATION, upsertCtx.isBatchOperation());

        return in;
    }
}
