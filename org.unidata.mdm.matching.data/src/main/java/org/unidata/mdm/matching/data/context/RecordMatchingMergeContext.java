/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.context;

import org.unidata.mdm.data.context.MergeDuplicatesContext;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.matching.data.service.segments.records.merge.RecordMergeMatchingStartExecutor;

/**
 * @author Sergey Murskiy on 03.11.2021
 */
public class RecordMatchingMergeContext extends DataMatchingContext
        implements DataMatchingCalculationContext<OriginRecord>, MergeDuplicatesContext<OriginRecord> {

    /**
     * SVUID.
     */
    private static final long serialVersionUID = -468886243461568875L;

    /**
     * Constructor.
     *
     * @param b the builder
     */
    private RecordMatchingMergeContext(RecordMatchingMergeContextBuilder b) {
        super(b);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return RecordMergeMatchingStartExecutor.SEGMENT_ID;
    }

    /**
     * Builder shorthand.
     *
     * @return builder
     */
    public static RecordMatchingMergeContextBuilder builder() {
        return new RecordMatchingMergeContextBuilder();
    }

    /**
     * Builder for the context.
     */
    public static class RecordMatchingMergeContextBuilder extends DataMatchingContextBuilder<RecordMatchingMergeContextBuilder> {

        /**
         * {@inheritDoc}
         */
        @Override
        public RecordMatchingMergeContext build() {
            return new RecordMatchingMergeContext(this);
        }
    }
}
