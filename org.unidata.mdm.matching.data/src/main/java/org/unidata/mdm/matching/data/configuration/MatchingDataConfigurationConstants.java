/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.configuration;

import org.unidata.mdm.matching.data.module.MatchingDataModule;

/**
 * @author Sergey Murskiy on 13.09.2021
 */
public final class MatchingDataConfigurationConstants {

    /**
     * Constructor.
     */
    private MatchingDataConfigurationConstants() {
        super();
    }

    /**
     * Matching group.
     */
    public static final String PROPERTY_MATCHING_DATA_GROUP = MatchingDataModule.MODULE_ID + ".matching.group";

    /**
     * Is real-time matching enabled.
     */
    public static final String PROPERTY_REAL_TIME_MATCHING_ENABLED = MatchingDataModule.MODULE_ID + ".real.time.matching.enabled";
}
