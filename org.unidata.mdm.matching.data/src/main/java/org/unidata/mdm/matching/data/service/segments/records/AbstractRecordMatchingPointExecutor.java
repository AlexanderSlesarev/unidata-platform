/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.matching.core.context.MatchingContext;
import org.unidata.mdm.matching.core.dto.MatchingResult;
import org.unidata.mdm.matching.core.service.MatchingService;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.execution.MatchingRealTimeInput;
import org.unidata.mdm.matching.core.type.model.instance.MatchingTableElement;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.matching.data.configuration.MatchingDataConfigurationConstants;
import org.unidata.mdm.matching.data.context.DataMatchingCalculationContext;
import org.unidata.mdm.matching.data.type.apply.MatchingChangeSet;
import org.unidata.mdm.matching.data.type.state.RecordMatchingState;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.pipeline.Point;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 13.09.2021
 */
public abstract class AbstractRecordMatchingPointExecutor<T extends DataMatchingCalculationContext<?>> extends Point<T> {

    /**
     * Is real-time matching enabled.
     */
    @ConfigurationRef(MatchingDataConfigurationConstants.PROPERTY_REAL_TIME_MATCHING_ENABLED)
    private ConfigurationValue<Boolean> realTimeMatchingEnabled;

    /**
     * Matching service.
     */
    @Autowired
    protected MatchingService matchingService;

    /**
     * Constructor.
     */
    protected AbstractRecordMatchingPointExecutor(String segmentId, String description) {
        super(segmentId, description);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void point(T ctx) {

        collectUpdates(ctx);

        if (!ctx.isBatchOperation()) {

            MatchingChangeSet changeSet = ctx.changeSet();

            if (changeSet.isEmpty()) {
                return;
            }

            MatchingContext.MatchingContextBuilder builder = MatchingContext.builder()
                    .delete(changeSet.getDeletes())
                    .insert(changeSet.getInserts());

            if (Boolean.TRUE.equals(realTimeMatchingEnabled.getValue())) {
                builder
                        .matchingSets(changeSet.getMatchingSets())
                        .matchingInput(changeSet.getMatchingInput());

                Map<MatchingRecordKey, MatchingResult> result = matchingService.updateWithResult(builder.build());

                ctx.matchingResult(result);
            } else {
                matchingService.update(builder.build());
            }
        }
    }

    /**
     * Collect changes.
     *
     * @param ctx data matching context
     */
    protected void collectUpdates(T ctx) {

        NamespaceAssignmentElement assignment = ctx.getAssignment();
        RecordMatchingState state = ctx.matchingState();
        MatchingChangeSet changeSet = ctx.changeSet();

        Map<String, List<MatchingRecordKey>> deletes = assignment.getAssignedTables().stream()
                .collect(Collectors.toMap(MatchingTableElement::getName, k -> state.getDeletes()));

        changeSet.getDeletes().putAll(deletes);
        changeSet.getInserts().putAll(state.getInserts());
        changeSet.getMatchingSets().addAll(assignment.getAssignedSets());
        changeSet.setMatchingInput(createInput(state));
    }

    private MatchingRealTimeInput createInput(RecordMatchingState state) {

        Set<MatchingRecordKey> deletes = new HashSet<>(state.getDeletes());
        deletes.removeAll(state.getIdentity().values());

        MatchingRealTimeInput input = new MatchingRealTimeInput();
        input.setProcessedKeys(state.getIdentity().values());
        input.setExcludedKeys(deletes);

        return input;
    }
}
