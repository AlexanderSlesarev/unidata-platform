/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.type.apply.batch;

import org.unidata.mdm.data.context.DataContextFlags;
import org.unidata.mdm.matching.data.context.RecordMatchingUpsertContext;
import org.unidata.mdm.matching.data.dto.RecordMatchingUpsertResult;
import org.unidata.mdm.matching.data.service.segments.records.batch.RecordsUpsertMatchingStartExecutor;
import org.unidata.mdm.system.type.batch.BatchIterator;
import org.unidata.mdm.system.type.batch.BatchSetStatistics;
import org.unidata.mdm.system.type.pipeline.fragment.FragmentId;
import org.unidata.mdm.system.type.pipeline.fragment.InputFragment;

import java.util.ListIterator;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 10.09.2021
 */
public class RecordMatchingUpsertBatchSetAccumulator extends AbstractMatchingBatchSetAccumulator<RecordMatchingUpsertContext,
        RecordMatchingUpsertResult, RecordMatchingUpsertBatchSetAccumulator>
        implements InputFragment<RecordMatchingUpsertBatchSetAccumulator> {

    /**
     * Fragment ID for convenience.
     */
    public static final FragmentId<RecordMatchingUpsertBatchSetAccumulator> ID
            = new FragmentId<>("RECORD_UPSERT_MATCHING_BATCH_SET");

    /**
     * Constructor.
     *
     * @param commitSize chunk size
     *                   If true, a simple id cache will be build.
     *                   The accumulator must exist during the step then.
     */
    public RecordMatchingUpsertBatchSetAccumulator(int commitSize) {
        super(commitSize);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FragmentId<RecordMatchingUpsertBatchSetAccumulator> fragmentId() {
        return ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return RecordsUpsertMatchingStartExecutor.SEGMENT_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BatchIterator<RecordMatchingUpsertContext> iterator() {
        return new RecordMatchingUpsertBatchSetAccumulator.RecordMatchingUpsertBatchIterator();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public BatchSetStatistics<?> statistics() {
        return null;
    }

    /**
     * Simple batch iterator.
     */
    private class RecordMatchingUpsertBatchIterator implements BatchIterator<RecordMatchingUpsertContext> {
        /**
         * List iterator.
         */
        private final ListIterator<RecordMatchingUpsertContext> i = workingCopy.listIterator();
        /**
         * Current entry.
         */
        private RecordMatchingUpsertContext current = null;

        /**
         * Constructor.
         */
        public RecordMatchingUpsertBatchIterator() {
            super();
        }

        /**
         * If there are more elements to iterate.
         *
         * @return true, if so, false otherwise
         */
        @Override
        public boolean hasNext() {

            boolean hasNext = i.hasNext();
            if (!hasNext && current != null) {
                accumulate(current);
            }

            return hasNext;
        }

        /**
         * Next context for origin upsert
         *
         * @return next context
         */
        @Override
        public RecordMatchingUpsertContext next() {

            RecordMatchingUpsertContext next = i.next();
            if (current != null) {
                accumulate(current);
            }

            init(next);

            current = next;
            return next;
        }

        /**
         * Removes current element.
         */
        @Override
        public void remove() {
            i.remove();
            current = null;
        }

        /**
         * Does some preprocessing.
         *
         * @param ctx the upsert context
         */
        private void init(RecordMatchingUpsertContext ctx) {

            // Already initialized
            if (Objects.nonNull(ctx.changeSet())) {
                return;
            }

            ctx.changeSet(new RecordMatchingUpsertBatchSet(RecordMatchingUpsertBatchSetAccumulator.this));

            // Ensure the flag is set
            ctx.setFlag(DataContextFlags.FLAG_BATCH_OPERATION, true);
        }
    }
}
