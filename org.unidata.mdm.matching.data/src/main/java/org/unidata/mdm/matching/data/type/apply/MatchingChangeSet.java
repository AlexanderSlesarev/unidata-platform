/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.type.apply;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.core.type.change.ChangeSet;
import org.unidata.mdm.matching.core.type.data.MatchingRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.execution.MatchingRealTimeInput;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey Murskiy on 21.08.2021
 */
public abstract class MatchingChangeSet implements ChangeSet {
    /**
     * Real-time matching inputs.
     */
    protected MatchingRealTimeInput matchingInput;
    /**
     * Matching rule sets to execute.
     */
    protected final List<MatchingRuleSetElement> matchingSets = new ArrayList<>(2);
    /**
     * Matching records keys to delete.
     */
    protected final Map<String, List<MatchingRecordKey>> deletes = new HashMap<>(2);
    /**
     * Matching records to insert.
     */
    protected final Map<String, List<MatchingRecord>> inserts = new HashMap<>(2);

    /**
     * Gets real-time matching input.
     *
     * @return input
     */
    public MatchingRealTimeInput getMatchingInput() {
        return matchingInput;
    }

    /**
     * Sets matching input.
     *
     * @param matchingInput matching input
     */
    public void setMatchingInput(MatchingRealTimeInput matchingInput) {
        this.matchingInput = matchingInput;
    }

    /**
     * Gets matching sets.
     *
     * @return sets
     */
    public List<MatchingRuleSetElement> getMatchingSets() {
        return matchingSets;
    }

    /**
     * Gets matching record deletes.
     *
     * @return deletes
     */
    public Map<String, List<MatchingRecordKey>> getDeletes() {
        return deletes;
    }

    /**
     * Gets matching record inserts.
     *
     * @return inserts
     */
    public Map<String, List<MatchingRecord>> getInserts() {
        return inserts;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return CollectionUtils.isEmpty(matchingSets) && MapUtils.isEmpty(deletes) && MapUtils.isEmpty(inserts);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        matchingInput = null;
        matchingSets.clear();
        deletes.clear();
        inserts.clear();
    }
}
