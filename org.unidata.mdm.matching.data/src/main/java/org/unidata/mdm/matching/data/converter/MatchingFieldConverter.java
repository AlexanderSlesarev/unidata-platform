/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.converter;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.type.data.ArrayAttribute;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.matching.core.type.data.MatchingDataType;
import org.unidata.mdm.matching.core.type.data.field.AbstractMatchingRecordField;
import org.unidata.mdm.matching.core.type.data.field.MatchingRecordField;

import java.util.Arrays;

/**
 * @author Sergey Murskiy on 21.08.2021
 */
public class MatchingFieldConverter {

    /**
     * Constructor.
     */
    private MatchingFieldConverter() {
        super();
    }

    /**
     * Convert data record attribute to matching record field.
     *
     * @param name      matching record filed name
     * @param attribute attribute
     * @return matching record field
     */
    public static MatchingRecordField<?> to(String name, Attribute attribute) {
        if (attribute.isSimple()) {
            return AbstractMatchingRecordField.of(name, attribute.narrow());
        } else if (attribute.isArray()) {
            ArrayAttribute<?> arrayAttribute = attribute.narrow();

            Object[] values = Arrays.stream(arrayAttribute.toArray())
                    .map(Object::toString)
                    .sorted(String::compareTo)
                    .toArray();

            return AbstractMatchingRecordField.of(MatchingDataType.STRING, name, StringUtils.join(values, '|'));
        }

        return null;
    }
}
