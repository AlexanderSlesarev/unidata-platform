/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.batch;

import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.data.dto.RecordsMatchingBulkResult;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.type.apply.batch.RecordMatchingUpsertBatchSetAccumulator;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.pipeline.batch.BatchedFinish;

/**
 * @author Sergey Murskiy on 12.09.2021
 */
@Component(RecordsUpsertMatchingFinishExecutor.SEGMENT_ID)
public class RecordsUpsertMatchingFinishExecutor extends BatchedFinish<RecordMatchingUpsertBatchSetAccumulator,
        RecordsMatchingBulkResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[BATCH_RECORD_UPSERT_MATCHING_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".batch.record.upsert.matching.finish.description";

    /**
     * Constructor.
     */
    public RecordsUpsertMatchingFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, RecordsMatchingBulkResult.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RecordsMatchingBulkResult finish(RecordMatchingUpsertBatchSetAccumulator accumulator) {
        return new RecordsMatchingBulkResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RecordMatchingUpsertBatchSetAccumulator.class.isAssignableFrom(start.getInputTypeClass());
    }
}
