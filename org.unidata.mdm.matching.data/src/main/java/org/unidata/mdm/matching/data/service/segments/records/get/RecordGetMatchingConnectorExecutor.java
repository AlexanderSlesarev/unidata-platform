/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.get;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.data.context.GetRequestContext;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.matching.core.configuration.MatchingDescriptors;
import org.unidata.mdm.matching.core.context.ClusterSearchContext;
import org.unidata.mdm.matching.core.dto.ClusterGetResult;
import org.unidata.mdm.matching.core.dto.MatchingResult;
import org.unidata.mdm.matching.core.service.ClusterService;
import org.unidata.mdm.matching.core.type.cluster.Cluster;
import org.unidata.mdm.matching.core.type.cluster.ClusterSearchQuery;
import org.unidata.mdm.matching.core.type.cluster.ClusterSearchQuery.ClusterRecordSearchQuery;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.matching.data.dto.RecordMatchingGetResult;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.service.segments.MatchingAssignmentSupport;
import org.unidata.mdm.search.service.SearchService;
import org.unidata.mdm.system.type.namespace.NameSpace;
import org.unidata.mdm.system.type.pipeline.Connector;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.Start;

import java.util.Date;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 24.08.2021
 */
@Component(RecordGetMatchingConnectorExecutor.SEGMENT_ID)
public class RecordGetMatchingConnectorExecutor extends Connector<GetRequestContext, RecordMatchingGetResult>
        implements MatchingAssignmentSupport {

    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[RECORD_GET_MATCHING_CONNECTOR]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".record.get.matching.connector.description";
    /**
     * The MMS instance.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Cluster service.
     */
    @Autowired
    private ClusterService clusterService;
    /**
     * Search service.
     */
    @Autowired
    private SearchService searchService;

    /**
     * Constructor.
     */
    public RecordGetMatchingConnectorExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RecordMatchingGetResult connect(GetRequestContext ctx) {
        return execute(ctx, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RecordMatchingGetResult connect(GetRequestContext ctx, Pipeline p) {
        return execute(ctx, p);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return GetRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MetaModelService metaModelService() {
        return metaModelService;
    }

    /**
     * Gets Matching Data data.
     *
     * @param ctx the context
     * @param p   the pipeline (we don't care)
     * @return result
     */
    public RecordMatchingGetResult execute(GetRequestContext ctx, Pipeline p) {

        RecordMatchingGetResult result = new RecordMatchingGetResult();
        RecordKeys keys = ctx.keys();

        if (keys != null) {

            NamespaceAssignmentElement assignment = getAssignment(ctx.keys());

            if (Objects.isNull(assignment)) {
                return result;
            }

            ClusterGetResult search = executeSearch(assignment.getNameSpace(), keys, ctx);
            if (search.isEmpty()) {
                return result;
            }

            result.setPayload(extractSearch(search));
        }

        return result;
    }

    private ClusterGetResult executeSearch(NameSpace namespace, RecordKeys keys, GetRequestContext gCtx) {

        Date point = gCtx.getForDate() == null ? new Date() : gCtx.getForDate();

        return clusterService.search(ClusterSearchContext.builder()
                .query(new ClusterSearchQuery()
                        .withRecordsQueries(new ClusterRecordSearchQuery()
                                .withNamespace(namespace.getId())
                                .withTypeName(keys.getEntityName())
                                .withSubjectId(keys.getEtalonKey().getId())
                                .withAsOfDate(point)))
                .fetchClusterRecords(false)
                .build());
    }

    private MatchingResult extractSearch(ClusterGetResult clustersResult) {
        MatchingResult result = new MatchingResult();

        for (Cluster cluster : clustersResult.getClusters()) {
            MatchingRuleSetElement set = metaModelService.instance(MatchingDescriptors.MATCHING)
                    .getSet(cluster.getSetName());

            MatchingRuleElement rule = metaModelService.instance(MatchingDescriptors.MATCHING)
                    .getRule(cluster.getRuleName());

            if (Objects.isNull(set) || Objects.isNull(rule)) {
                continue;
            }

            MatchingResult.RuleExecutionResult ruleResult = new MatchingResult.RuleExecutionResult(set, rule);
            ruleResult.cluster(cluster);
            result.add(ruleResult);
        }

        return result;
    }
}
