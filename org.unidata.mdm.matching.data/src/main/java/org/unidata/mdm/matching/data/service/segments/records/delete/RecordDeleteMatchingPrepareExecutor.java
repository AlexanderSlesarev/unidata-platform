/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.delete;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.DeleteRequestContext;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.data.context.DataMatchingCalculationContext;
import org.unidata.mdm.matching.data.context.RecordMatchingDeleteContext;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.type.state.RecordMatchingState;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 21.08.2021
 */
@Component(RecordDeleteMatchingPrepareExecutor.SEGMENT_ID)
public class RecordDeleteMatchingPrepareExecutor extends Point<RecordMatchingDeleteContext> {

    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[RECORD_DELETE_MATCHING_PREPARE]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".record.delete.matching.prepare.description";

    /**
     * Constructor.
     */
    public RecordDeleteMatchingPrepareExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RecordMatchingDeleteContext ctx) {

        // Not yet supported
        if (ctx.isInactivateOrigin()) {
            return;
        }

        if (ctx.isInactivatePeriod()) {
            handlePeriodDelete(ctx);
        } else {
            handleRecordDelete(ctx);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RecordMatchingDeleteContext.class.isAssignableFrom(start.getInputTypeClass());
    }

    private void handlePeriodDelete(RecordMatchingDeleteContext ctx) {
        DeleteRequestContext payload = ctx.getPayload();

        RecordMatchingState state = ctx.matchingState();
        Timeline<OriginRecord> current = ctx.currentTimeline();
        Timeline<OriginRecord> next = ctx.nextTimeline();

        List<TimeInterval<OriginRecord>> removed = current.selectBy(payload.getValidFrom(), payload.getValidTo());

        Date from = removed.isEmpty()
                ? payload.getValidFrom()
                : removed.get(0).getValidFrom();
        Date to = removed.isEmpty()
                ? payload.getValidTo()
                : removed.get(removed.size() - 1).getValidTo();

        List<TimeInterval<OriginRecord>> added = Objects.nonNull(next)
                ? next.selectBy(from, to)
                : new ArrayList<>();

        for (TimeInterval<OriginRecord> ti : added) {

            EtalonRecord etalon = ti.getCalculationResult();
            if (Objects.isNull(etalon) || etalon.getInfoSection().getStatus() == RecordStatus.INACTIVE) {
                continue;
            }

            MatchingRecordKey key = MatchingRecordKey.of(
                    ctx.getAssignment().getNameSpace().getId(),
                    ctx.getAssignment().getTypeName(),
                    etalon.getInfoSection().getEtalonKey().getId(),
                    etalon.getInfoSection().getValidFrom(),
                    etalon.getInfoSection().getValidTo()
            );

            state.putCalculation(ti, key, etalon);
        }

        processDeletes(removed, ctx);
    }

    private void handleRecordDelete(RecordMatchingDeleteContext ctx) {
        processDeletes(ctx.currentTimeline().getIntervals(), ctx);
    }

    private void processDeletes(List<TimeInterval<OriginRecord>> intervals, DataMatchingCalculationContext<?> ctx) {

        if (CollectionUtils.isEmpty(intervals)) {
            return;
        }

        RecordMatchingState state = ctx.matchingState();
        intervals.stream()
                .map(i -> (EtalonRecord) i.getCalculationResult())
                .filter(Objects::nonNull)
                .forEach(er -> state.putDelete(MatchingRecordKey.of(
                        ctx.getAssignment().getNameSpace().getId(),
                        ctx.getAssignment().getTypeName(),
                        er.getInfoSection().getEtalonKey().getId(),
                        er.getInfoSection().getValidFrom(),
                        er.getInfoSection().getValidTo())));
    }
}
