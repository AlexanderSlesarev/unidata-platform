/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.delete;

import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.data.context.RecordMatchingDeleteContext;
import org.unidata.mdm.matching.data.dto.RecordMatchingDeleteResult;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Sergey Murskiy on 13.09.2021
 */
@Component(RecordDeleteMatchingFinishExecutor.SEGMENT_ID)
public class RecordDeleteMatchingFinishExecutor extends Finish<RecordMatchingDeleteContext, RecordMatchingDeleteResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[RECORD_DELETE_MATCHING_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".record.delete.matching.finish.description";

    /**
     * Constructor.
     */
    public RecordDeleteMatchingFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, RecordMatchingDeleteResult.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RecordMatchingDeleteResult finish(RecordMatchingDeleteContext ctx) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RecordMatchingDeleteContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
