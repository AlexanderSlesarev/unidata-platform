/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.upsert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.data.context.DataContextFlags;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.type.data.UpsertAction;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.matching.data.context.RecordMatchingUpsertContext;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.service.segments.MatchingAssignmentSupport;
import org.unidata.mdm.system.service.ExecutionService;
import org.unidata.mdm.system.type.pipeline.Connector;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.PipelineOutput;
import org.unidata.mdm.system.type.pipeline.Start;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 06.08.2021
 */
@Component(RecordUpsertMatchingConnectorExecutor.SEGMENT_ID)
public class RecordUpsertMatchingConnectorExecutor extends Connector<UpsertRequestContext, PipelineOutput>
        implements MatchingAssignmentSupport {

    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[RECORD_UPSERT_MATCHING_CONNECTOR]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".record.upsert.matching.connector.description";
    /**
     * The MMS instance.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * The ES.
     */
    @Autowired
    private ExecutionService executionService;

    /**
     * Constructor.
     */
    public RecordUpsertMatchingConnectorExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PipelineOutput connect(UpsertRequestContext ctx) {
        return execute(ctx, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PipelineOutput connect(UpsertRequestContext ctx, Pipeline p) {
        return execute(ctx, p);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return UpsertRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MetaModelService metaModelService() {
        return metaModelService;
    }

    /**
     * Gets matching data.
     *
     * @param ctx the context
     * @param p   the pipeline
     * @return result
     */
    @SuppressWarnings("DuplicatedCode")
    private PipelineOutput execute(UpsertRequestContext ctx, Pipeline p) {

        if (ctx.isBatchOperation() || (!ctx.isRecalculateTimeline() && ctx.upsertAction() == UpsertAction.NO_ACTION)) {
            return null;
        }

        NamespaceAssignmentElement assignment = getAssignment(ctx.keys());

        if (Objects.isNull(assignment)) {
            return null;
        }

        RecordMatchingUpsertContext in = RecordMatchingUpsertContext.builder()
                .assignment(assignment)
                .namespace(assignment.getNameSpace())
                .payload(ctx)
                .operationId(ctx.getOperationId())
                .build();

        in.currentTimeline(ctx.currentTimeline());
        in.nextTimeline(ctx.nextTimeline());
        in.operationType(ctx.operationType());
        in.setFlag(DataContextFlags.FLAG_BATCH_OPERATION, ctx.isBatchOperation());

        return executionService.execute(in);
    }
}
