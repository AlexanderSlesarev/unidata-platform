/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.segments.records.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.data.context.RecordMatchingUpsertContext;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.type.apply.batch.RecordMatchingUpsertBatchSetAccumulator;
import org.unidata.mdm.system.service.ExecutionService;
import org.unidata.mdm.system.type.batch.BatchIterator;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.pipeline.batch.BatchedPoint;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 12.09.2021
 */
@Component(RecordsUpsertMatchingProcessExecutor.SEGMENT_ID)
public class RecordsUpsertMatchingProcessExecutor extends BatchedPoint<RecordMatchingUpsertBatchSetAccumulator> {
    /**
     * This logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordsUpsertMatchingProcessExecutor.class);
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingDataModule.MODULE_ID + "[BATCH_RECORD_UPSERT_MATCHING_PROCESS]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingDataModule.MODULE_ID + ".batch.record.upsert.matching.process.description";

    /**
     * The ES.
     */
    @Autowired
    private ExecutionService executionService;

    /**
     * Constructor.
     */
    public RecordsUpsertMatchingProcessExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("DuplicatedCode")
    @Override
    public void point(RecordMatchingUpsertBatchSetAccumulator accumulator) {

        for (BatchIterator<RecordMatchingUpsertContext> li = accumulator.iterator(); li.hasNext(); ) {

            RecordMatchingUpsertContext ctx = li.next();

            try {

                if (Objects.isNull(accumulator.pipeline())) {
                    executionService.execute(ctx);
                } else {
                    executionService.execute(accumulator.pipeline(), ctx);
                }

            } catch (Exception e) {

                LOGGER.warn("Bulk upsert, exception caught.", e);
                if (accumulator.isAbortOnFailure()) {
                    throw e;
                }

                li.remove();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RecordMatchingUpsertBatchSetAccumulator.class.isAssignableFrom(start.getInputTypeClass());
    }
}
