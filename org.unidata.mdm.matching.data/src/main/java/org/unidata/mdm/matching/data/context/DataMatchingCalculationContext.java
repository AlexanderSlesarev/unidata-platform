/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.context;

import org.unidata.mdm.core.type.calculables.Calculable;
import org.unidata.mdm.data.context.BatchAwareContext;
import org.unidata.mdm.data.context.OperationTypeContext;
import org.unidata.mdm.data.context.ReadWriteTimelineContext;
import org.unidata.mdm.matching.core.dto.MatchingResult;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.matching.data.type.state.RecordMatchingState;
import org.unidata.mdm.system.context.StorageId;

import java.util.Map;

/**
 * @author Sergey Murskiy on 06.08.2021
 */
public interface DataMatchingCalculationContext<T extends Calculable>
        extends
        ReadWriteTimelineContext<T>,
        DataMatchingWriteContext,
        BatchAwareContext,
        OperationTypeContext {

    /**
     * Matching state id.
     */
    StorageId SID_MATCHING_STATE = new StorageId("SID_MATCHING_STATE");
    /**
     * Matching result id.
     */
    StorageId SID_MATCHING_RESULT = new StorageId("SID_MATCHING_RESULT");

    /**
     * Gets matching state from storage.
     *
     * @return matching state
     */
    default RecordMatchingState matchingState() {
        return getFromStorage(SID_MATCHING_STATE);
    }

    /**
     * Puts matching state to storage.
     *
     * @param state matching state
     */
    default void matchingState(RecordMatchingState state) {
        putToStorage(SID_MATCHING_STATE, state);
    }

    /**
     * Gets matching result from storage.
     *
     * @return matching result
     */
    default Map<MatchingRecordKey, MatchingResult> matchingResult() {
        return getFromStorage(SID_MATCHING_RESULT);
    }

    /**
     * Puts matching result to storage.
     *
     * @param result matching result
     */
    default void matchingResult(Map<MatchingRecordKey, MatchingResult> result) {
        putToStorage(SID_MATCHING_RESULT, result);
    }

    /**
     * Gets matching assignment.
     *
     * @return assignment
     */
    NamespaceAssignmentElement getAssignment();
}

