/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.data.service.job;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.service.job.step.ItemWriteListenerAdapter;
import org.unidata.mdm.core.type.annotation.JobRef;
import org.unidata.mdm.core.type.job.JobFraction;
import org.unidata.mdm.core.type.job.JobParameterDefinition;
import org.unidata.mdm.core.type.job.JobParameterDescriptor;
import org.unidata.mdm.core.util.JobUtils;
import org.unidata.mdm.data.configuration.DataNamespace;
import org.unidata.mdm.matching.core.configuration.MatchingDescriptors;
import org.unidata.mdm.matching.core.context.ClusterDeleteContext;
import org.unidata.mdm.matching.core.service.ClusterService;
import org.unidata.mdm.matching.core.type.cluster.ClusterSearchQuery;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.matching.data.module.MatchingDataModule;
import org.unidata.mdm.matching.data.service.segments.records.batch.RecordsUpsertMatchingConnectorExecutor;
import org.unidata.mdm.matching.data.service.segments.records.batch.RecordsUpsertMatchingFinishExecutor;
import org.unidata.mdm.matching.data.service.segments.records.batch.RecordsUpsertMatchingPersistenceExecutor;
import org.unidata.mdm.matching.data.service.segments.records.batch.RecordsUpsertMatchingProcessExecutor;
import org.unidata.mdm.matching.data.service.segments.records.batch.RecordsUpsertMatchingStartExecutor;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.instance.DataModelInstance;
import org.unidata.mdm.system.service.PipelineService;
import org.unidata.mdm.system.service.TextService;
import org.unidata.mdm.system.service.TouchService;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.connection.PipelineConnection;
import org.unidata.mdm.system.type.touch.Touch;
import org.unidata.mdm.system.type.touch.TouchParams;
import org.unidata.mdm.system.type.touch.Touchable;
import org.unidata.mdm.system.util.NameSpaceUtils;
import org.unidata.mdm.system.util.TextUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 31.08.2021
 */
@JobRef("reindexDataJob")
@Component("reindexMatchingFraction")
public class ReindexMatchingJobFraction extends JobFraction implements Touchable {
    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReindexMatchingJobFraction.class);
    /**
     * Connect reindex data pipeline.
     */
    public static final Touch<PipelineConnection> TOUCH_REINDEX_PIPELINE_CONNECT
            = Touch.builder(PipelineConnection.class)
            .touchName("[touch-reindex-pipeline-connect]")
            .paramType("step-name", String.class)
            .paramType("step-execution", StepExecution.class)
            .build();

    /**
     * This fraction ID.
     */
    private static final String ID = "reindex-matching-data-fraction";
    /**
     * This fraction display name.
     */
    private static final String DISPLAY_NAME = MatchingDataModule.MODULE_ID + ".reindex.matching.data.name";
    /**
     * This fraction description.
     */
    private static final String DESCRIPTION = MatchingDataModule.MODULE_ID + ".reindex.matching.data.description";
    /**
     * Reindex matching data param.
     */
    private static final String REINDEX_MATCHING_DATA_PARAM = "reindexMatchingData";
    /**
     * Reindex matching data param display name.
     */
    private static final String REINDEX_MATCHING_DATA_PARAM_DISPLAY_NAME = MatchingDataModule.MODULE_ID + ".reindex.matching.data.param.display.name";
    /**
     * Reindex matching data param description.
     */
    private static final String REINDEX_MATCHING_DATA_PARAM_DESCRIPTION = MatchingDataModule.MODULE_ID + ".reindex.matching.data.param.description";

    /**
     * Clean clusters listener.
     */
    private final ItemWriteListener<String> cleanClustersListener = new ItemWriteListenerAdapter<>() {
        /**
         * {@inheritDoc}
         */
        @Override
        public void afterWrite(List<? extends String> items) {

            DataModelInstance dataModel = metaModelService.instance(Descriptors.DATA);
            MatchingModelInstance matchingModel = metaModelService.instance(MatchingDescriptors.MATCHING);
            if (Objects.isNull(dataModel) || Objects.isNull(matchingModel)) {
                return;
            }

            for (String entityName : items) {

                LOGGER.info("Cleaning clusters for {}.", entityName);

                NamespaceAssignmentElement assignment = null;

                if (dataModel.isRegister(entityName)) {
                    assignment = matchingModel.getAssignment(NameSpaceUtils.join(DataNamespace.REGISTER, entityName));
                } else if (dataModel.isLookup(entityName)) {
                    assignment = matchingModel.getAssignment(NameSpaceUtils.join(DataNamespace.LOOKUP, entityName));
                }

                if (Objects.nonNull(assignment)) {
                    clusterService.delete(ClusterDeleteContext.builder()
                            .query(new ClusterSearchQuery()
                                    .withSetNames(assignment.getAssignedSets().stream()
                                            .map(MatchingRuleSetElement::getName)
                                            .collect(Collectors.toList())))
                            .build());
                }
            }
        }
    };

    /**
     * IWL.
     */
    private final Map<String, ItemWriteListener<?>> itemWriteListeners = Map.of("reindexDataJobMappingStep", cleanClustersListener);

    /**
     * This fraction parameters.
     */
    private static final List<JobParameterDescriptor<?>> PARAMETERS = List.of(
            JobParameterDescriptor.bool()
                    .defaultSingle(Boolean.TRUE)
                    .name(REINDEX_MATCHING_DATA_PARAM)
                    .displayName(() -> TextUtils.getText(REINDEX_MATCHING_DATA_PARAM_DISPLAY_NAME))
                    .description(() -> TextUtils.getText(REINDEX_MATCHING_DATA_PARAM_DESCRIPTION))
                    .build());

    /**
     * The MMS.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Cluster service.
     */
    @Autowired
    private ClusterService clusterService;
    /**
     * PLS.
     */
    @Autowired
    private PipelineService pipelineService;
    /**
     * The TS.
     */
    @Autowired
    private TextService textService;

    /**
     * Constructor.
     */
    @Autowired
    public ReindexMatchingJobFraction(final TouchService touchService) {
        super(ID, PARAMETERS);
        touchService.register(this, TOUCH_REINDEX_PIPELINE_CONNECT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName() {
        return textService.getText(DISPLAY_NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return textService.getText(DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getOrder() {
        return 11;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object touch(TouchParams<?> t) {

        if (StringUtils.equals(t.getTouch().getTouchName(), TOUCH_REINDEX_PIPELINE_CONNECT.getTouchName())) {
            return connect(t.getParam("step-execution"));
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ItemWriteListener<?> itemWriteListener(String stepName) {
        return itemWriteListeners.get(stepName);
    }

    private PipelineConnection connect(StepExecution step) {

        if (unused(step)) {
            return null;
        }

        return PipelineConnection.single()
                .pipeline(Pipeline
                        .start(Objects.requireNonNull(pipelineService.start(RecordsUpsertMatchingStartExecutor.SEGMENT_ID)))
                        .with(Objects.requireNonNull(pipelineService.point(RecordsUpsertMatchingProcessExecutor.SEGMENT_ID)))
                        .with(Objects.requireNonNull(pipelineService.point(RecordsUpsertMatchingPersistenceExecutor.SEGMENT_ID)))
                        .end(Objects.requireNonNull(pipelineService.finish(RecordsUpsertMatchingFinishExecutor.SEGMENT_ID))))
                .segment(pipelineService.connector(RecordsUpsertMatchingConnectorExecutor.SEGMENT_ID))
                .build();
    }

    private boolean unused(StepExecution step) {

        JobParameterDefinition<Boolean> jobReindexMatchingData =
                JobUtils.fromString(step.getJobParameters().getString(REINDEX_MATCHING_DATA_PARAM));

        return !jobReindexMatchingData.single();
    }
}

