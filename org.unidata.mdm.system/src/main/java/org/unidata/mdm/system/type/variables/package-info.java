/**
 * @author Mikhail Mikhailov on Sep 10, 2020
 * General purpose named variables, covering simple types.
 */
package org.unidata.mdm.system.type.variables;