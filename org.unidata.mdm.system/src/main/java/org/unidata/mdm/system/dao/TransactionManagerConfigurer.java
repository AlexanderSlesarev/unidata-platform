/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.dao;

import java.util.Objects;
import java.util.Properties;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.jta.JtaTransactionManager;
import org.unidata.mdm.system.service.IdentityService;

import bitronix.tm.Configuration;
import bitronix.tm.TransactionManagerServices;

/**
 * @author Mikhail Mikhailov on May 15, 2021
 * TX manager settings.
 * This may be used to support different TX manager implementations in the future.
 */
public class TransactionManagerConfigurer {
    /**
     * PC, required to get node id.
     */
    private final IdentityService platformConfiguration;
    /**
     * Constructor.
     * @param platformConfiguration the platform configuration
     */
    public TransactionManagerConfigurer(IdentityService platformConfiguration) {
        super();
        this.platformConfiguration = platformConfiguration;
    }
    /**
     * Configures TX manager according to supplied settings.
     * @param properties the properties
     * @return TX manager
     */
    public PlatformTransactionManager configure(Properties properties) {

        bitronix.tm.Configuration btmc = TransactionManagerServices.getConfiguration();
        populate(btmc, Objects.isNull(properties) ? new Properties() : properties);

        // Server ID is set separately here.
        btmc.setServerId("BTM-" + platformConfiguration.getNodeId());

        JtaTransactionManager jtm = new bitronix.tm.integration.spring.PlatformTransactionManager();
        jtm.setAllowCustomIsolationLevels(true);

        return jtm;
    }
    /**
     * Populates configuration with values from properties.
     * @param conf the configuration
     * @param props the properties
     */
    private void populate(Configuration conf, Properties props) {

        conf.setJournal(TransactionManagerSetting.BTM_JOURNAL_TYPE.getOrDefault(props));
        conf.setLogPart1Filename(TransactionManagerSetting.BTM_LOG_PART_ONE.getOrDefault(props));
        conf.setLogPart2Filename(TransactionManagerSetting.BTM_LOG_PART_TWO.getOrDefault(props));
        conf.setForcedWriteEnabled(TransactionManagerSetting.BTM_FORCE_WRITE_ENABLED.getOrDefault(props));
        conf.setForcedWriteEnabled(TransactionManagerSetting.BTM_FORCE_BATCHING_ENABLED.getOrDefault(props));
        conf.setMaxLogSizeInMb(TransactionManagerSetting.BTM_MAX_LOG_SIZE_IN_MB.getOrDefault(props));
        conf.setFilterLogStatus(TransactionManagerSetting.BTM_FILTER_LOG_STATUS.getOrDefault(props));
        conf.setSkipCorruptedLogs(TransactionManagerSetting.BTM_SKIP_CORRUPTED_LOGS.getOrDefault(props));
        conf.setAsynchronous2Pc(TransactionManagerSetting.BTM_ASYNCHRONOUS_2PC.getOrDefault(props));
        conf.setWarnAboutZeroResourceTransaction(TransactionManagerSetting.BTM_WARN_ABOUT_ZERO_RESOURCE_TRANSACTION.getOrDefault(props));
        conf.setDebugZeroResourceTransaction(TransactionManagerSetting.BTM_DEBUG_ZERO_RESOURCE_TRANSACTION.getOrDefault(props));
        conf.setDefaultTransactionTimeout(TransactionManagerSetting.BTM_DEFAULT_TRANSACTION_TIMEOUT.getOrDefault(props));
        conf.setGracefulShutdownInterval(TransactionManagerSetting.BTM_GRACEFUL_SHUTDOWN_INTERVAL.getOrDefault(props));
        conf.setBackgroundRecoveryIntervalSeconds(TransactionManagerSetting.BTM_BACKGROUND_RECOVERY_INTERVAL_SECONDS.getOrDefault(props));
        conf.setDisableJmx(TransactionManagerSetting.BTM_DISABLE_JMX.getOrDefault(props));
        conf.setSynchronousJmxRegistration(TransactionManagerSetting.BTM_SYNCHRONOUS_JMX_REGISTRATION.getOrDefault(props));
        conf.setJndiUserTransactionName(TransactionManagerSetting.BTM_JNDI_USER_TRANSACTION_NAME.getOrDefault(props));
        conf.setJndiTransactionSynchronizationRegistryName(TransactionManagerSetting.BTM_JNDI_TRANSACTION_SYNCHRONIZATION_REGISTRY_NAME.getOrDefault(props));
        conf.setExceptionAnalyzer(TransactionManagerSetting.BTM_EXCEPTION_ANALYZER.getOrDefault(props));
        conf.setCurrentNodeOnlyRecovery(TransactionManagerSetting.BTM_CURRENT_NODE_ONLY_RECOVERY.getOrDefault(props));
        conf.setAllowMultipleLrc(TransactionManagerSetting.BTM_ALLOW_MULTIPLE_LRC.getOrDefault(props));
        conf.setResourceConfigurationFilename(TransactionManagerSetting.BTM_RESOURCE_CONFIGURATION_FILENAME.getOrDefault(props));
        conf.setConservativeJournaling(TransactionManagerSetting.BTM_CONSERVATIVE_JOURNALING.getOrDefault(props));
        conf.setJdbcProxyFactoryClass(TransactionManagerSetting.BTM_JDBC_PROXY_FACTORY_CLASS.getOrDefault(props));
    }
}
