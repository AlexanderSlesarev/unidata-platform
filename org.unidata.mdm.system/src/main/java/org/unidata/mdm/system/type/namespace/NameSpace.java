/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.type.namespace;

import java.util.Objects;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Mikhail Mikhailov on Mar 4, 2021
 */
public class NameSpace {
    /**
     * Denotes undefined namespace.
     */
    public static final String GLOBAL_NAMESPACE_ID = StringUtils.EMPTY;
    /**
     * Global namespace constant.
     */
    public static final NameSpace GLOBAL_NAMESPACE = of(NameSpace.GLOBAL_NAMESPACE_ID,
            "Global namespace",
            StringUtils.EMPTY,
            StringUtils.EMPTY);
    /**
     * Namespace separator.
     */
    public static final String NAMESPACE_SEPARATOR = ":";
    /**
     * Empty ns array.
     */
    public static final NameSpace[] EMPTY_NAMESPACE_ARRAY = new NameSpace[0];
    /**
     * This namespace ID.
     */
    private final String id;
    /**
     * Namespace' display name.
     */
    private final Supplier<String> displayName;
    /**
     * Namespace' description.
     */
    private final Supplier<String> description;
    /**
     * The module, exporting this namespace.
     */
    private final String moduleId;
    /**
     * HC.
     */
    private final int h;
    /**
     * Constructor.
     * @param id this namespace ID
     * @param moduleId the module ID, this NS is exported from
     * @param displayName this namespace display name supplier
     * @param description this namespace description supplier
     */
    private NameSpace(String id, String moduleId, Supplier<String> displayName, Supplier<String> description) {
        super();

        Objects.requireNonNull(id, "Namespace ID must not be null.");
        Objects.requireNonNull(moduleId, "Module ID must not be null.");

        this.id = id;
        this.moduleId = moduleId;
        this.displayName = displayName;
        this.description = description;
        this.h = Objects.hash(id, moduleId);
    }
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return Objects.isNull(displayName) ? StringUtils.EMPTY : displayName.get();
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return Objects.isNull(description) ? StringUtils.EMPTY : description.get();
    }
    /**
     * @return the module
     */
    public String getModuleId() {
        return moduleId;
    }
    /**
     * Instance.
     * @param id NS ID
     * @param moduleId exporting module ID
     * @return NS instance
     */
    public static NameSpace of(String id, String moduleId) {
        return of(id, moduleId, (String) null, (String) null);
    }
    /**
     * Instance.
     * @param id NS ID
     * @param moduleId exporting module ID
     * @param displayName this NS display name
     * @param description this NS description
     * @return NS instance
     */
    public static NameSpace of(String id, String moduleId, String displayName, String description) {
        return of(id, moduleId,
                    StringUtils.isBlank(displayName) ? null : () -> displayName,
                    StringUtils.isBlank(description) ? null : () -> description);
    }
    /**
     * Instance.
     * @param id NS ID
     * @param moduleId exporting module ID
     * @param displayName this NS display name
     * @param description this NS description
     * @return NS instance
     */
    public static NameSpace of(String id, String moduleId, Supplier<String> displayName, Supplier<String> description) {
        return new NameSpace(id, moduleId, displayName, description);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return h;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        NameSpace other = (NameSpace) obj;
        return Objects.equals(id, other.id) && Objects.equals(moduleId, other.moduleId);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return new StringBuilder("(id = ")
                .append(this.id)
                .append(", module = ")
                .append(this.moduleId)
                .append(", displayName = ")
                .append(getDisplayName())
                .append(")")
                .toString();
    }
}
