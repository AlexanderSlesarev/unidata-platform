/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.type.identity;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.UUID;

import com.hazelcast.version.MemberVersion;

/**
 * Node identity digest.
 * @author Mikhail Mikhailov on Aug 31, 2021
 */
public class NodeIdentity implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 3326967083793219031L;
    /**
     * Member's address.
     */
    private final InetSocketAddress memberAddress;
    /**
     * Member's UUID.
     */
    private final UUID memberUuid;
    /**
     * Member's HZ version.
     */
    private final MemberVersion memberVersion;
    /**
     * Member's 6 octet sequence, used in UUID v1 generation.
     */
    private final byte[] memberSequence;
    /**
     * Constructor.
     */
    public NodeIdentity(NodeIdentityBuilder b) {
        super();
        this.memberAddress = b.memberAddress;
        this.memberSequence = b.memberSequence;
        this.memberUuid = b.memberUuid;
        this.memberVersion = b.memberVersion;
    }
    /**
     * @return the address
     */
    public InetSocketAddress getMemberAddress() {
        return memberAddress;
    }
    /**
     * @return the memberUuid
     */
    public UUID getMemberUuid() {
        return memberUuid;
    }
    /**
     * @return the memberVersion
     */
    public MemberVersion getMemberVersion() {
        return memberVersion;
    }
    /**
     * @return the memberSequence
     */
    public byte[] getMemberSequence() {
        return memberSequence;
    }
    /**
     * Returns true, if this identity holds the same sequence as the one supplied as argument.
     * @param s the sequence
     * @return true, if this identity holds the same sequence as the one supplied as argument
     */
    public boolean isSame(byte[] s) {
        return Arrays.equals(s, memberSequence);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return new StringBuilder()
                .append(memberAddress)
                .append("|")
                .append(memberUuid)
                .append("|")
                .append(memberVersion)
                .toString();
    }
    /**
     * Builder method.
     * @return builder
     */
    public static NodeIdentityBuilder builder() {
        return new NodeIdentityBuilder();
    }
    /**
     * Simple builder.
     * @author Mikhail Mikhailov on Aug 31, 2021
     */
    public static class NodeIdentityBuilder {
        /**
         * Member's address.
         */
        private InetSocketAddress memberAddress;
        /**
         * Member's UUID.
         */
        private UUID memberUuid;
        /**
         * Member's HZ version.
         */
        private MemberVersion memberVersion;
        /**
         * Member's 6 octet sequence, used in UUID v1 generation.
         */
        private byte[] memberSequence;
        /**
         * Constructor.
         */
        public NodeIdentityBuilder() {
            super();
        }
        /**
         * @param memberAddress the memberAddress to set
         */
        public NodeIdentityBuilder memberAddress(InetSocketAddress memberAddress) {
            this.memberAddress = memberAddress;
            return this;
        }
        /**
         * @param memberUuid the memberUuid to set
         */
        public NodeIdentityBuilder memberUuid(UUID memberUuid) {
            this.memberUuid = memberUuid;
            return this;
        }
        /**
         * @param memberVersion the memberVersion to set
         */
        public NodeIdentityBuilder memberVersion(MemberVersion memberVersion) {
            this.memberVersion = memberVersion;
            return this;
        }
        /**
         * @param memberSequence the memberSequence to set
         */
        public NodeIdentityBuilder memberSequence(byte[] memberSequence) {
            this.memberSequence = memberSequence;
            return this;
        }
        /**
         * Build identity.
         * @return node identity
         */
        public NodeIdentity build() {
            return new NodeIdentity(this);
        }
    }
}
