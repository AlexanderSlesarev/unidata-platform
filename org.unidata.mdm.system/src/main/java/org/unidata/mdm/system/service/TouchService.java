/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.service;

import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.unidata.mdm.system.type.touch.Touch;
import org.unidata.mdm.system.type.touch.TouchParams;
import org.unidata.mdm.system.type.touch.Touchable;

/**
 * @author Mikhail Mikhailov on Apr 21, 2021
 * This is the service, allowing weak calls of unknown number of subscribed handlers.
 */
public interface TouchService {
    /**
     * Binds a touchable to one or more touch definitions.
     * @param t the touchable
     * @param touches touch definitions
     */
    void register(@Nonnull Touchable t, Touch<?>... touches);
    /**
     * Runs registered touchable handlers and returns a first non-null result.
     * @param <X> return type
     * @param p params
     * @return result or null
     */
    @Nullable
    <X> X singleTouch(TouchParams<X> p);
    /**
     * Runs registered touchable handlers and returns all non-null results as a collection.
     * @param <X> return type
     * @param p params
     * @return result or empty collection
     */
    @Nonnull
    <X> Collection<X> multipleTouch(TouchParams<X> p);
}
