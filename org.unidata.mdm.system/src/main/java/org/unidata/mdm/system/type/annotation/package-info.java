/**
 * UD 'system' annotations.
 * @author Mikhail Mikhailov on Jan 17, 2020
 */
package org.unidata.mdm.system.type.annotation;