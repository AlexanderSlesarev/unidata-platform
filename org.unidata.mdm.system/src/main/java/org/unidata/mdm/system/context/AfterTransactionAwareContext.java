/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.context;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;

import org.apache.commons.collections4.CollectionUtils;
/**
 * @author Mikhail Mikhailov
 * The _former_ finalizer participant context, extracted to interface.
 */
public interface AfterTransactionAwareContext extends StorageCapableContext {
    /**
     * The transaction finalizers SID.
     */
    StorageId SID_AFTER_TRANSACTION_ACTIONS = new StorageId("AFTER_TRANSACTION_ACTIONS");
    /**
     * Adds callables to execute after transaction commit.
     * @param action the action to run after transaction commit
     */
    default void afterTransaction(Collection<Callable<Boolean>> actions) {

        if (CollectionUtils.isNotEmpty(actions)) {

            List<Callable<Boolean>> collected = getFromStorage(SID_AFTER_TRANSACTION_ACTIONS);
            if (collected == null) {
                collected = new ArrayList<>();
            }

            collected.addAll(actions);
            putToStorage(SID_AFTER_TRANSACTION_ACTIONS, collected);
        }
    }
    /**
     * Adds a callable to execute after transaction commit.
     * @param action the action to run after transaction commit
     */
    default void afterTransaction(Callable<Boolean> action) {

        List<Callable<Boolean>> collected = getFromStorage(SID_AFTER_TRANSACTION_ACTIONS);
        if (collected == null) {
            collected = new ArrayList<>();
        }

        collected.add(action);
        putToStorage(SID_AFTER_TRANSACTION_ACTIONS, collected);
    }
    /**
     * Gets the list of collected actions.
     * @return list of actions
     */
    default List<Callable<Boolean>> afterTransaction() {
        List<Callable<Boolean>> collected = getFromStorage(SID_AFTER_TRANSACTION_ACTIONS);
        return Objects.isNull(collected) ? Collections.emptyList() : collected;
    }
}