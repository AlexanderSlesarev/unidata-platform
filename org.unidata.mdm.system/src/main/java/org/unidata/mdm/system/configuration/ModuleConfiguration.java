/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.CompositePropertySource;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.PropertySource;
import org.unidata.mdm.system.type.spring.ModularApplicationContext;

/**
 * Root configuration beans class.
 * @author Mikhail Mikhailov on Nov 5, 2019
 */
public abstract class ModuleConfiguration implements ApplicationContextAware {
    /**
     * Configured contexts.
     */
    protected static final Map<ConfigurationId, ModuleConfiguration>
        CONFIGURATIONS_MAP = new IdentityHashMap<>();
    /**
     * This configuration application context.
     */
    protected ApplicationContext applicationContext;
    /**
     * Just a pointer for configured context map.
     * @author Mikhail Mikhailov on Nov 9, 2019
     */
    @FunctionalInterface
    public interface ConfigurationId {
        /**
         * @return the name
         */
        String getName();
    }
    /**
     * Constructor.
     */
    protected ModuleConfiguration() {
        super();
    }
    /**
     * Gets the id.
     * @return id
     */
    public abstract ConfigurationId getId();
    /**
     * {@inheritDoc}
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        CONFIGURATIONS_MAP.put(getId(), this);
    }
    /**
     * Gets the underlying context
     * @return context or null
     */
    public ApplicationContext getConfiguredApplicationContext() {
        return applicationContext;
    }
    /**
     * Gets a named bean/component.
     *
     * @param <T> the bean type
     * @param name the bean name
     * @return bean or null
     */
    @SuppressWarnings("unchecked")
    @Nullable
    public <T> T getBeanByName(String name) {
        ApplicationContext context = getConfiguredApplicationContext();
        if (Objects.nonNull(context)) {
            return (T) context.getBean(name);
        }

        return null;
    }

    /**
     * Gets named beans/components.
     *
     * @param <T> the bean type
     * @param names the bean names
     * @return bean or empty collection
     */
    @SuppressWarnings("unchecked")
    @Nonnull
    public <T> Collection<T> getBeansByNames(String... names) {

        ApplicationContext context = getConfiguredApplicationContext();
        if (Objects.nonNull(context) && ArrayUtils.isNotEmpty(names)) {

            List<T> result = new ArrayList<>(names.length);
            for (int i = 0; i < names.length; i++) {

                if (Objects.isNull(names[i])) {
                    continue;
                }

                T val = (T) context.getBean(names[i]);
                result.add(val);
            }

            return result;
        }

        return Collections.emptyList();
    }
    /**
     * Gets a bean.
     *
     * @param <T>
     * @param beanClass the bean class
     * @return bean
     */
    @Nullable
    public <T> T getBeanByClass(Class<T> beanClass) {
        ApplicationContext context = getConfiguredApplicationContext();
        if (Objects.nonNull(context)) {
            return context.getBean(beanClass);
        }

        return null;
    }
    /**
     * Gets beans of type.
     *
     * @param <T>
     * @param beanClass the bean class
     * @return bean
     */
    public <T> Map<String, T> getBeansOfType(Class<T> beanClass) {
        ApplicationContext context = getConfiguredApplicationContext();
        if (Objects.nonNull(context)) {
            return context.getBeansOfType(beanClass);
        }

        return Collections.emptyMap();
    }
    /**
     * Gets all properties, string with specified prefix.
     * @param prefix the prefix to filter properties by
     * @param strip the flag, telling to strip prefix in the result, if true or letting the name unchanged, if false
     * @return properties
     */
    public Properties getPropertiesByPrefix(String prefix, boolean strip) {

        final Properties result = new Properties();
        ConfigurableEnvironment ce = (ConfigurableEnvironment) getConfiguredApplicationContext().getEnvironment();

        ce.getPropertySources().forEach(ps -> {
            if (ps instanceof CompositePropertySource) {
                CompositePropertySource cps = (CompositePropertySource) ps;
                processPropertySources(prefix, strip, cps.getPropertySources(), result, ce);
            } else if (ps instanceof EnumerablePropertySource) {
                processEnumerablePropertySource(prefix, strip, (EnumerablePropertySource<?>) ps, result, ce);
            }
        });

        return result;
    }
    /**
     * Gets configuration, identified by the given id.
     * @param <X> the target confguretion type
     * @param id the ID
     * @return confguration or null
     */
    @SuppressWarnings("unchecked")
    public static <X extends ModuleConfiguration> X getConfiguration(ConfigurationId id) {
        return (X) CONFIGURATIONS_MAP.get(id);
    }
    /**
     * Gets application context context, hold by configuration, denoted by the given configuration id.
     * @param id the configuration ID
     * @return context or null
     */
    public static ApplicationContext getApplicationContext(ConfigurationId id) {
        ModuleConfiguration mc = getConfiguration(id);
        return Objects.nonNull(mc) ? mc.getConfiguredApplicationContext() : null;
    }
    /**
     * Gets a bean 'globally'.
     * It means, that bean will be searched in the whole context hierarchy, initialized to the moment.
     *
     * @param <T> target type
     * @param beanClass the bean class
     * @return bean or null
     */
    public static <T> T getBean(Class<T> beanClass) {

        ModularApplicationContext mc =
                (ModularApplicationContext) getApplicationContext(SystemConfiguration.ID);
        if (Objects.nonNull(mc)) {
            return mc.getLast().getBean(beanClass);
        }

        return null;
    }
    /**
     * Gets a bean of a given type with specific name 'globally'.
     * It means, that bean will be searched in the whole context hierarchy, initialized to the moment.
     *
     * @param <T> target type
     * @param name target name
     * @param beanClass the bean type
     * @return bean or null
     */
    public static <T> T getBean(String name, Class<T> beanClass) {

        ModularApplicationContext mc =
                (ModularApplicationContext) getApplicationContext(SystemConfiguration.ID);
        if (Objects.nonNull(mc)) {
            return mc.getLast().getBean(name, beanClass);
        }

        return null;
    }
    /**
     * Gets beans of type 'globally'.
     * It means, that beans of type will be searched in the whole context hierarchy, initialized to the moment.
     *
     * @param <T>
     * @param beanClass the bean class
     * @return beans map
     */
    public static <T> Map<String, T> getBeans(Class<T> beanClass) {

        ModularApplicationContext mc =
                (ModularApplicationContext) getApplicationContext(SystemConfiguration.ID);
        if (Objects.nonNull(mc)) {
            return mc.getLast().getBeansOfType(beanClass);
        }

        return Collections.emptyMap();
    }

    private void processPropertySources(String prefix, boolean strip, Collection<PropertySource<?>> sources, Properties result, ConfigurableEnvironment ce) {
        sources.forEach(ps -> {
            if (ps instanceof CompositePropertySource) {
                CompositePropertySource cps = (CompositePropertySource) ps;
                processPropertySources(prefix, strip, cps.getPropertySources(), result, ce);
            } else if (ps instanceof EnumerablePropertySource) {
                processEnumerablePropertySource(prefix, strip, (EnumerablePropertySource<?>) ps, result, ce);
            }
        });
    }

    private void processEnumerablePropertySource(String prefix, boolean strip, EnumerablePropertySource<?> eps, Properties result, ConfigurableEnvironment ce) {

        for (String propName : eps.getPropertyNames()) {

            if (!propName.startsWith(prefix)) {
                continue;
            }

            // Resolve it once again via environment properly,
            // so that place holders are also processed
            result.setProperty(strip ? StringUtils.substringAfter(propName, prefix) : propName, ce.getProperty(propName));
        }
    }
}
