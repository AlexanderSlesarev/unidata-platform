package org.unidata.mdm.system.type.configuration.impl;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.system.type.configuration.ConfigurationProperty;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.configuration.ConfigurationValueUpdatesListener;

/**
 * Mutable configuration value.
 * @author Mikhail Mikhailov on Apr 21, 2020
 */
public class MutableConfigurationValue<T> implements ConfigurationValue<T> {
    /**
     * The property desсription.
     */
    private final ConfigurationProperty<T> property;
    /**
     * Current value.
     */
    private final AtomicReference<T> value;
    /**
     * This value listeners.
     */
    private Set<ConfigurationValueUpdatesListener> listeners;
    /**
     * Constructor.
     * @param property the property description
     * @param initialValue initial value
     */
    public MutableConfigurationValue(ConfigurationProperty<T> property, T initialValue) {
        super();
        this.property = property;
        this.value = new AtomicReference<>(initialValue);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigurationProperty<T> getProperty() {
        return property;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public T getValue() {

        if (isEmpty()) {
            return null;
        }

        T v = value.get();
        return Objects.isNull(v) ? property.getDefaultValue() : v;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return Objects.isNull(property) && Objects.isNull(value.get());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasValue() {
        return Objects.nonNull(getValue());
    }
    /**
     * Updates current value with a new one, invoking any listeners and / or static setters, if defined.
     * @param update the value to set
     */
    @SuppressWarnings("unchecked")
    public void update(Object update) {

        // 1. Set internal
        value.set((T) update);

        // 2. Invoke static stuff, if defined
        if (Objects.nonNull(property.getSetter())) {
            property.getSetter().accept(getValue());
        }

        // 3. Invoke listeners, if registered
        if (CollectionUtils.isNotEmpty(listeners)) {
            for (ConfigurationValueUpdatesListener l : listeners) {
                l.configurationValueUpdated(property);
            }
        }
    }
    /**
     * Adds a listener for this property updates.
     * Listeners set is implemented as {@link WeakHashMap} wrapped set to prevent collecting of unloadable beans.
     * @param listener the listener
     */
    public void add(ConfigurationValueUpdatesListener listener) {

        if (Objects.isNull(listeners)) {
            listeners = Collections.newSetFromMap(new WeakHashMap<ConfigurationValueUpdatesListener, Boolean>());
        }

        listeners.add(listener);
    }
}
