/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.exception.SystemExceptionIds;

/**
 * @author Mikhail Mikhailov on Aug 25, 2021
 * Pool setting configurer.
 */
public class PoolSettingConfigurer {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PoolSettingConfigurer.class);
    /**
     * Tomcat DSF class name.
     */
    private static final String TOMCAT_FACTORY_CLASS_NAME = "org.apache.tomcat.jdbc.pool.DataSourceFactory";
    /**
     * Tomcat pool class name.
     */
    @SuppressWarnings("unused")
    private static final String TOMCAT_DATA_SOURCE_CLASS_NAME = "org.apache.tomcat.jdbc.pool.DataSource";
    /**
     * Apache DS factory instance.
     */
    @SuppressWarnings("unused")
    private static final Object TOMCAT_DATA_SOURCE_FACTORY;
    /**
     * 'createDataSource' method.
     */
    @SuppressWarnings("unused")
    private static final Method TOMCAT_CREATE_DATA_SOURCE_METHOD;
    /**
     * Tomcat pool class name.
     */
    private static final String BITRONIX_DATA_SOURCE_CLASS_NAME = "bitronix.tm.resource.jdbc.PoolingDataSource";
    /**
     * Static initialization.
     */
    static {

        // Bad things happen here
        Object instance = null;
        Method method = null;
        try {
            Class<?> klass = Class.forName(TOMCAT_FACTORY_CLASS_NAME);
            instance = klass.getDeclaredConstructor().newInstance();
            method = klass.getMethod("createDataSource", Properties.class);
        } catch (ClassNotFoundException | InstantiationException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            final String message = "Failed to instantiate '{}' NON-XA data source factory class.";
            LOGGER.error(message, TOMCAT_FACTORY_CLASS_NAME, e);
            throw new PlatformFailureException(message,
                    SystemExceptionIds.EX_SYSTEM_CANNOT_INITIALIZE_NON_XA_FACTORY,
                    TOMCAT_FACTORY_CLASS_NAME);
        } finally {
            TOMCAT_DATA_SOURCE_FACTORY = instance;
            TOMCAT_CREATE_DATA_SOURCE_METHOD = method;
        }
    }
    /**
     * Constructor.
     */
    public PoolSettingConfigurer() {
        super();
    }
    /**
     * Only bitronix is supported for now. We
     * @param properties
     * @return
     */
    public DataSource configure(Properties properties) {

        // Bad things happen here
        Class<?> klass = null;
        Method method = null;
        Object instance = null;
        try {
            klass = Class.forName(BITRONIX_DATA_SOURCE_CLASS_NAME);
            instance = klass.getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            final String message = "Failed to instantiate '{}' XA data source.";
            LOGGER.error(message, BITRONIX_DATA_SOURCE_CLASS_NAME, e);
            throw new PlatformFailureException(message,
                    SystemExceptionIds.EX_SYSTEM_CANNOT_INITIALIZE_XA_FACTORY,
                    BITRONIX_DATA_SOURCE_CLASS_NAME);
        }

        String url = properties.getProperty("url");
        String uniqueName = properties.getProperty("uniqueName");

        if (StringUtils.isBlank(url)) {
            throwInvalidURL();
        } else if (StringUtils.isBlank(uniqueName)) {
            throwInvalidUniqueName();
        }

        try {

            setProperty(instance, PoolSetting.POOL_UNIQUE_NAME.getPropertyName(), uniqueName);
            setProperty(instance, PoolSetting.POOL_CLASS_NAME.getPropertyName(), PoolSetting.POOL_CLASS_NAME.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_MIN_POOL_SIZE.getPropertyName(), PoolSetting.POOL_MIN_POOL_SIZE.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_MAX_POOL_SIZE.getPropertyName(), PoolSetting.POOL_MAX_POOL_SIZE.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_MAX_IDLE_TIME.getPropertyName(), PoolSetting.POOL_MAX_IDLE_TIME.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_MIN_LIFE_TIME.getPropertyName(), PoolSetting.POOL_MIN_LIFE_TIME.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_AUTOMATIC_ENLISTING_ENABLED.getPropertyName(), PoolSetting.POOL_AUTOMATIC_ENLISTING_ENABLED.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_USE_TM_JOIN.getPropertyName(), PoolSetting.POOL_USE_TM_JOIN.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_ACQUIRE_INCREMENT.getPropertyName(), PoolSetting.POOL_ACQUIRE_INCREMENT.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_ACQUISITION_TIMEOUT.getPropertyName(), PoolSetting.POOL_ACQUISITION_TIMEOUT.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_ACQUISITION_INTERVAL.getPropertyName(), PoolSetting.POOL_ACQUISITION_INTERVAL.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_DEFER_CONNECTION_RELEASE.getPropertyName(), PoolSetting.POOL_DEFER_CONNECTION_RELEASE.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_ALLOW_LOCAL_TRANSACTIONS.getPropertyName(), PoolSetting.POOL_ALLOW_LOCAL_TRANSACTIONS.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_TWO_PC_ORDERING_POSITION.getPropertyName(), PoolSetting.POOL_TWO_PC_ORDERING_POSITION.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_APPLY_TRANSACTION_TIMEOUT.getPropertyName(), PoolSetting.POOL_APPLY_TRANSACTION_TIMEOUT.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_SHARE_TRANSACTION_CONNECTIONS.getPropertyName(), PoolSetting.POOL_SHARE_TRANSACTION_CONNECTIONS.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_DISABLED.getPropertyName(), PoolSetting.POOL_DISABLED.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_IGNORE_RECOVERY_FAILURES.getPropertyName(), PoolSetting.POOL_IGNORE_RECOVERY_FAILURES.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_CONNECTION_TEST_QUERY.getPropertyName(), PoolSetting.POOL_CONNECTION_TEST_QUERY.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_CONNECTION_TEST_TIMEOUT.getPropertyName(), PoolSetting.POOL_CONNECTION_TEST_TIMEOUT.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_ENABLE_JDBC4_CONNECTION_TEST.getPropertyName(), PoolSetting.POOL_ENABLE_JDBC4_CONNECTION_TEST.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_PREPARED_STATEMENT_CACHE_SIZE.getPropertyName(), PoolSetting.POOL_PREPARED_STATEMENT_CACHE_SIZE.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_ISOLATION_LEVEL.getPropertyName(), PoolSetting.POOL_ISOLATION_LEVEL.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_CURSOR_HOLDABILITY.getPropertyName(), PoolSetting.POOL_CURSOR_HOLDABILITY.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_LOCAL_AUTO_COMMIT.getPropertyName(), PoolSetting.POOL_LOCAL_AUTO_COMMIT.getOrDefault(properties));
            setProperty(instance, PoolSetting.POOL_JMX_NAME.getPropertyName(), PoolSetting.POOL_JMX_NAME.getOrDefault(properties));

            Properties p = new Properties();
            p.setProperty("url", url);

            method = klass.getMethod("setDriverProperties", Properties.class);
            method.invoke(instance, p);

            method = klass.getMethod("init");
            method.invoke(instance);

        } catch (Exception e) {
            final String message = "Failed to create XA data source.";
            throw new PlatformFailureException(message, e, SystemExceptionIds.EX_SYSTEM_CANNOT_CREATE_XA_DATASOURCE);
        }

        return (DataSource) instance;
    }

    private static void throwInvalidURL() {
        throw new PlatformFailureException(
                "Invalid (null or blank) DB URL.",
                SystemExceptionIds.EX_SYSTEM_DATASOURCE_URL_MISSING);
    }

    private static void throwInvalidUniqueName() {
        throw new PlatformFailureException(
                "Invalid (null or blank) DS unique name.",
                SystemExceptionIds.EX_SYSTEM_DATASOURCE_UNIQUE_NAME_MISSING);
    }

    private void setProperty(Object instance, String propertyName, String propertyValue)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

        Class<?> targetClazz = PropertyUtils.getPropertyType(instance, propertyName);
        if (targetClazz != null) {

            if (targetClazz == String.class) {
                PropertyUtils.setProperty(instance, propertyName, propertyValue);
            } else {

                Object targetValue = ConvertUtils.convert(propertyValue, targetClazz);
                if (targetValue != propertyValue) {
                    PropertyUtils.setProperty(instance, propertyName, targetValue);
                }
            }
        }
    }
}
