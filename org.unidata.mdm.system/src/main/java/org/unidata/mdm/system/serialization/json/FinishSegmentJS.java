package org.unidata.mdm.system.serialization.json;

/**
 * @author Mikhail Mikhailov on May 25, 2020
 */
public class FinishSegmentJS extends SegmentJS {
    /**
     * Constructor.
     */
    public FinishSegmentJS() {
        super();
    }
}
