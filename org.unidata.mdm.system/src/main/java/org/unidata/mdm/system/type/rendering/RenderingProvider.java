package org.unidata.mdm.system.type.rendering;

import java.util.Collection;
import java.util.Collections;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Mikhail Mikhailov on Jan 15, 2020
 */
public interface RenderingProvider {
    /**
     * Gets renderer for an action or null
     * @param action the action
     * @return renderer or null
     */
    @Nullable
    default Collection<InputFragmentRenderer> get(@Nonnull InputRenderingAction action) {
        return Collections.emptyList();
    }
    /**
     * Gets renderer for an action or null
     * @param action the action
     * @return renderer or null
     */
    @Nullable
    default Collection<OutputFragmentRenderer> get(@Nonnull OutputRenderingAction action) {
        return Collections.emptyList();
    }
    /**
     * Gets renderer for errors or null
     * @param action the action
     * @return renderer or null
     */
    @Nullable
    default ErrorOutputFragmentRenderer onError(@Nonnull OutputRenderingAction action) {
        return null;
    }
}
