/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.service.impl.spring;

import org.unidata.mdm.system.util.ContextUtils;

import com.hazelcast.core.ManagedContext;
import com.hazelcast.executor.impl.RunnableAdapter;
import com.hazelcast.spring.context.SpringAware;
import com.hazelcast.spring.context.SpringManagedContext;

/**
 * @author Mikhail Mikhailov on Dec 1, 2021
 * Hazelcast spring support.
 * Same as {@link SpringManagedContext} but with broader visibility.
 */
public class ModularManagedContextSupport implements ManagedContext {
    /**
     * Constructor.
     */
    public ModularManagedContextSupport() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Object initialize(Object obj) {
        Object resultObject = obj;
        if (obj != null) {
            if (obj instanceof RunnableAdapter) {
                RunnableAdapter adapter = (RunnableAdapter) obj;
                Object runnable = adapter.getRunnable();
                runnable = initializeIfSpringAwareIsPresent(runnable);
                adapter.setRunnable((Runnable) runnable);
            } else {
                resultObject = initializeIfSpringAwareIsPresent(obj);
            }
        }
        return resultObject;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Object initializeIfSpringAwareIsPresent(Object obj) {
        Class clazz = obj.getClass();
        SpringAware s = (SpringAware) clazz.getAnnotation(SpringAware.class);
        Object resultObject = obj;
        if (s != null) {
            String name = s.beanName().trim();
            if (name.isEmpty()) {
                name = clazz.getName();
            }
            resultObject = ContextUtils.initOrphanBean(name, obj);
        }
        return resultObject;
    }}
