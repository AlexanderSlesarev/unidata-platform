/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.type.touch;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import javax.annotation.Nonnull;

/**
 * @author Mikhail Mikhailov on Apr 20, 2021
 */
public class Touch<T> {
    /**
     * @author Mikhail Mikhailov on Apr 20, 2021
     * Touch type.
     */
    public enum TouchType {
        /**
         * If there are multiple touchable actors registered,
         * take first non null result and return.
         */
        SINGLE,
        /**
         * If there are multiple touchable actors registered,
         * take all non null results and return a collection of results.
         */
        MULTIPLE
    }
    /**
     * The name of the touch.
     */
    private final String touchName;
    /**
     * The output type, this touch expects.
     */
    private final Class<T> outputType;
    /**
     * Param types this touch supplies.
     */
    private final Map<String, Class<?>> paramTypes;
    /**
     * Hash code.
     */
    private final int h;
    /**
     * Constructor.
     */
    private Touch(TouchBuilder<T> b) {
        super();

        Objects.requireNonNull(b.touchName, "Touch name must not be null.");
        Objects.requireNonNull(b.outputType, "Output type must not be null.");

        this.touchName = b.touchName;
        this.outputType = b.outputType;
        this.paramTypes = Objects.isNull(b.paramTypes) ? Collections.emptyMap() : Map.copyOf(b.paramTypes);

        final Object[] fields = new Object[2 + (this.paramTypes.size() * 2)];

        int i = 0;
        for (Entry<String, Class<?>> p : this.paramTypes.entrySet()) {
            fields[i++] = p.getKey();
            fields[i++] = p.getValue();
        }

        fields[i++] = this.touchName;
        fields[i++] = this.outputType;

        h = Arrays.hashCode(fields);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return h;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Touch)) {
            return false;
        }

        Touch<?> other = (Touch<?>) obj;

        final Object[] thisFields = { this.touchName, this.outputType, this.paramTypes };
        final Object[] otherFields = { other.touchName, other.outputType, other.paramTypes };

        return Arrays.equals(thisFields, otherFields);
    }
    /**
     * @return the name
     */
    public String getTouchName() {
        return touchName;
    }
    /**
     * @return the outputType
     */
    public Class<T> getOutputType() {
        return outputType;
    }
    /**
     * Creates builder.
     * @param <X> the return type
     * @return builder
     */
    public static<X> TouchBuilder<X> builder(Class<X> outputType) {
        return new TouchBuilder<>(outputType);
    }
    /**
     * @return the paramTypes
     */
    public Map<String, Class<?>> getParamTypes() {
        return Objects.isNull(paramTypes) ? Collections.emptyMap() : paramTypes;
    }
    /**
     * @author Mikhail Mikhailov on Apr 21, 2021
     * Touch builder.
     */
    public static class TouchBuilder<T> {
        /**
         * The output type, this touch expects.
         */
        private Class<T> outputType;
        /**
         * Param types this touch supplies.
         */
        private Map<String, Class<?>> paramTypes;
        /**
         * The name of the touch.
         */
        private String touchName;
        /**
         * Constructor.
         */
        private TouchBuilder(Class<T> outputType) {
            super();
            this.outputType = outputType;
        }
        /**
         * The name of the touch. Must be set.
         * @param name of the touch
         * @return self
         */
        public TouchBuilder<T> touchName(@Nonnull String name) {
            this.touchName = name;
            return this;
        }
        /**
         * A single param type.
         * Multiple calls add more params.
         * Does not accept null names or null types
         * @param paramName the name of the param
         * @param paramType the type of the param
         * @return self
         */
        public TouchBuilder<T> paramType(String paramName, Class<?> paramType) {

            if (Objects.nonNull(paramName) && Objects.nonNull(paramType)) {

                if (paramTypes == null) {
                    paramTypes = new HashMap<>();
                }

                paramTypes.put(paramName, paramType);
            }

            return this;
        }
        /**
         * Build the touch.
         * @return touch instance
         */
        public Touch<T> build() {
            return new Touch<>(this);
        }
    }
}
