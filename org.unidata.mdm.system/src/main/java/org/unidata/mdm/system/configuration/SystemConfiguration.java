/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.configuration;

import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static java.time.temporal.ChronoField.SECOND_OF_MINUTE;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Arrays;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.camel.management.JmxManagementStrategyFactory;
import org.apache.camel.processor.aggregate.GroupedBodyAggregationStrategy;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.unidata.mdm.system.dao.TransactionManagerConfigurer;
import org.unidata.mdm.system.service.IdentityService;
import org.unidata.mdm.system.service.impl.spring.ModularManagedContextSupport;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MulticastConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.config.JetConfig;

/**
 * @author Mikhail Mikhailov
 * Root spring context link.
 */
@Configuration
@EnableTransactionManagement
@ComponentScan({"org.unidata.mdm.system"})
@PropertySource("file:///${unidata.conf}/backend.properties")
public class SystemConfiguration extends ModuleConfiguration {
    /**
     * UI LDT formatter.
     */
    private static final DateTimeFormatter REST_ISO_LOCAL_DATE_TIME = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .append(DateTimeFormatter.ISO_LOCAL_DATE)
            .appendLiteral('T')
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2)
            .appendLiteral(':')
            .appendValue(SECOND_OF_MINUTE, 2)
            .appendFraction(ChronoField.MILLI_OF_SECOND, 3, 3, true)
            .toFormatter();
    /**
     * This configuration ID.
     */
    public static final ConfigurationId ID = () -> "SYSTEM_CONFIGURATION";
    /**
     * RBMS.
     */
    private ResourceBundleMessageSource systemMessageSource;
    /**
     * Constructor.
     */
    public SystemConfiguration() {
        super();

        // Do it here, because otherwise, we can not control bundles joining
        systemMessageSource = new ResourceBundleMessageSource();
        systemMessageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
    }
    /**
     * Link to system bundles.
     */
    public ResourceBundleMessageSource getSystemMessageSource() {
        return systemMessageSource;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigurationId getId() {
        return ID;
    }
    /**
     * The Jet instance.
     * @param env the environment
     * @return jet instance
     */
    @Bean
    public JetInstance jetInstance(@Autowired Environment env) {
        // Go with defaults for now.
        return Jet.newJetInstance(new JetConfig()
                .setHazelcastConfig(configureHazelcastInstance(env)));
    }
    /**
     * The app HZ instance.
     * @param env environment
     * @return HZ instance
     */
    @Bean
    public HazelcastInstance hazelcastInstance(@Autowired JetInstance jetInstance) {
        return jetInstance.getHazelcastInstance();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        // Spring will peek the right Environment by itself.
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public ObjectMapper objectMapper() {

        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(REST_ISO_LOCAL_DATE_TIME));
        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

        ObjectMapper m = new ObjectMapper();

        m.configure(JsonParser.Feature.ALLOW_YAML_COMMENTS, true);
        m.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        m.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        m.registerModule(javaTimeModule);

        return m;
    }

    @Bean("systemDataSource")
    public DataSource systemDataSource(@Autowired Environment env) {

        // Single connection data source
        String url = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_DATASOURCE_URL);
        String username = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_DATASOURCE_USER);
        String password = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_DATASOURCE_PASSWORD);

        Objects.requireNonNull(url, "System datasource URL cannot be null");

        SingleConnectionDataSource scds = new SingleConnectionDataSource(url, username, password, true);
        scds.setDriverClassName("org.postgresql.Driver");

        return scds;
    }

    @Bean
    public PlatformTransactionManager transactionManager(TransactionManagerConfigurer transactionManagerConfigurer) {
        Properties properties = getPropertiesByPrefix(SystemConfigurationConstants.PROPERTY_BITRONIX_PREFIX, true);
        return transactionManagerConfigurer.configure(properties);
    }

    @Bean("configuration-sql")
    public PropertiesFactoryBean configurationSql() {
        final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/db/configuration-sql.xml"));
        return propertiesFactoryBean;
    }

    @Bean("pipelines-sql")
    public PropertiesFactoryBean pipelinesSql() {
        final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/db/pipelines-sql.xml"));
        return propertiesFactoryBean;
    }

    @Bean("nodes-sql")
    public PropertiesFactoryBean nodesSql() {
        final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/db/nodes-sql.xml"));
        return propertiesFactoryBean;
    }

    @Bean
    public MessageSource messageSource() {
        return systemMessageSource;
    }

    @Bean
    public JmxManagementStrategyFactory jmxManagementStrategyFactory() {
        return new JmxManagementStrategyFactory();
    }

    @Bean
    public GroupedBodyAggregationStrategy groupedBodyAggregationStrategy() {
        return new GroupedBodyAggregationStrategy();
    }

    @Bean
    public TransactionManagerConfigurer transactionManagerConfigurer(IdentityService platformConfiguration) {
        return new TransactionManagerConfigurer(platformConfiguration);
    }

    private Config configureHazelcastInstance(Environment env) {

        final Config config = new Config();

        // 0. Top container
        config.setInstanceName(SystemConfigurationConstants.SYSTEM_APLLICATION_NAME);

        // 1. General network settings
        final NetworkConfig network = config.getNetworkConfig();

        int port  = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_PORT, Integer.class, 5701);
        boolean increment = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_PORT_AUTOINCREMENT, Boolean.class, false);
        boolean reuse = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_REUSE_ADDERSS, Boolean.class, false);

        network.setPort(port);
        network.setPortAutoIncrement(increment);
        network.setReuseAddress(reuse);

        int count = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_PORT_COUNT, Integer.class, -1);
        if (count > 0) {
            network.setPortCount(count);
        }

        String publicAddress = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_PUBLIC_ADDRESS, String.class, StringUtils.EMPTY);
        if (StringUtils.isNotBlank(publicAddress)) {
            network.setPublicAddress(StringUtils.trim(publicAddress));
        }

        String interfaces = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_INTERFACES, String.class, StringUtils.EMPTY);
        if (StringUtils.isNotBlank(interfaces)) {

            String[] split = StringUtils.split(StringUtils.trim(interfaces), ',');
            if (ArrayUtils.isNotEmpty(split)) {

                network.getInterfaces()
                    .setEnabled(true)
                    .setInterfaces(Arrays.asList(split));
            }
        }

        String outboundPorts = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_OUTBOUND_PORTS, String.class, StringUtils.EMPTY);
        if (StringUtils.isNotBlank(outboundPorts)) {
            network.addOutboundPortDefinition(StringUtils.trim(outboundPorts));
        }

        // 2. Join methods
        final JoinConfig join = network.getJoin();

        // 2.1. TCP/IP
        TcpIpConfig tcp = join.getTcpIpConfig();

        boolean tcpEnabled = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_TCP_IP_ENABLED, Boolean.class, false);

        tcp.setEnabled(tcpEnabled);

        String tcpMembers = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_TCP_IP_MEMBERS, String.class, StringUtils.EMPTY);
        if (StringUtils.isNotBlank(tcpMembers)) {
            tcp.addMember(StringUtils.trim(tcpMembers));
        }

        int tcpTimeout = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_TCP_IP_TIMEOUT, Integer.class, -1);
        if (tcpTimeout > 0) {
            tcp.setConnectionTimeoutSeconds(tcpTimeout);
        }

        // 2.2. Multicast
        MulticastConfig multicast = join.getMulticastConfig();

        boolean multicastEnabled = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_ENABLED, Boolean.class, false);

        multicast.setEnabled(multicastEnabled);

        String multicastGroup = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_GROUP, String.class, StringUtils.EMPTY);
        if (StringUtils.isNotBlank(multicastGroup)) {
            multicast.setMulticastGroup(StringUtils.trim(multicastGroup));
        }

        int multicastPort  = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_PORT, Integer.class, -1);
        if (multicastPort > 0) {
            multicast.setMulticastPort(multicastPort);
        }

        int multicastTTL  = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_TTL, Integer.class, -1);
        if (multicastTTL > 0) {
            multicast.setMulticastTimeToLive(multicastTTL);
        }

        int multicastTimeout  = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_TIMEOUT, Integer.class, -1);
        if (multicastTimeout > 0) {
            multicast.setMulticastTimeoutSeconds(multicastTimeout);
        }

        String multicastTrusted = env.getProperty(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_TRUSTED, String.class, StringUtils.EMPTY);
        if (StringUtils.isNotBlank(multicastTrusted)) {

            String[] split = StringUtils.split(StringUtils.trim(multicastTrusted), ',');
            if (ArrayUtils.isNotEmpty(split)) {
                multicast.setTrustedInterfaces(Set.of(split));
            }
        }

        // 3. Make the context be spring aware
        config.setManagedContext(new ModularManagedContextSupport());

        return config;
    }
}
