/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.service.impl;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.unidata.mdm.system.configuration.SystemConfigurationConstants;
import org.unidata.mdm.system.dao.NodesDAO;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.exception.SystemExceptionIds;
import org.unidata.mdm.system.service.IdentityService;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.identity.NodeIdentity;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.RandomBasedGenerator;
import com.fasterxml.uuid.impl.TimeBasedGenerator;
import com.hazelcast.cluster.Member;
import com.hazelcast.cluster.MembershipAdapter;
import com.hazelcast.cluster.MembershipEvent;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;

/**
 * @author Mikhail Mikhailov
 * Platform configuration.
 */
@Component
public class IdentityServiceImpl extends MembershipAdapter implements IdentityService, InitializingBean {
    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(IdentityServiceImpl.class);
    /**
     * Max allowed node name length.
     */
    private static final int NODE_NAME_MAX_LENGTH = 128;
    /**
     * Node name/ID validation pattern.
     */
    private static final Pattern NODE_NAME_PATTERN = Pattern.compile("^[a-zA-Z][a-zA-Z0-9_-]*$", Pattern.CASE_INSENSITIVE);
    /**
     * Node ID. Fail, if null.
     */
    @ConfigurationRef(SystemConfigurationConstants.PROPERTY_NODE_ID)
    private ConfigurationValue<String> nodeId;
    /**
     * Version string. Fail, if null.
     */
    @ConfigurationRef(SystemConfigurationConstants.PROPERTY_PLATFORM_VERSION)
    private ConfigurationValue<String> versionString;
    /**
     * Major number.
     */
    private int platformMajor;
    /**
     * Minor number.
     */
    private int platformMinor;
    /**
     * Patch number.
     */
    private int platformPatch;
    /**
     * Revision number.
     */
    private int platformRevision;
    /**
     * V1 generator.
     */
    private TimeBasedGenerator v1Generator;
    /**
     * V4 generator.
     */
    private RandomBasedGenerator v4Generator;
    /**
     * Instance.
     */
    private final HazelcastInstance hazelcastInstance;
    /**
     * Nodes DAO.
     */
    private final NodesDAO nodesDAO;
    /**
     * Nodes.
     */
    private final IMap<String, NodeIdentity> nodes;
    /**
     * This node identity.
     */
    private NodeIdentity nodeIdentity;
    /**
     * Constructor.
     */
    public IdentityServiceImpl(HazelcastInstance hazelcastInstance, NodesDAO nodesDAO) {
        super();
        this.hazelcastInstance = hazelcastInstance;
        this.nodesDAO = nodesDAO;
        this.nodes = hazelcastInstance.getMap(SystemConfigurationConstants.SYSTEM_NODES_MAP_NAME);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getPlatformMajor() {
        return platformMajor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPlatformMinor() {
        return platformMinor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPlatformPatch() {
        return platformPatch;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getPlatformRevision() {
        return platformRevision;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getNodeId() {
        return nodeId.getValue();
    }
    /**
     * @return the nodeIdentity
     */
    @Override
    public NodeIdentity getNodeIdentity() {
        return nodeIdentity;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String v1IdString() {
        return v1Generator.generate().toString();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public UUID v1Id() {
        return v1Generator.generate();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String v4IdString() {
        return v4Generator.generate().toString();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public UUID v4Id() {
        return v4Generator.generate();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() {

        initVersionNumbers();
        initNodeId();
        initIdGenerators();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void memberRemoved(MembershipEvent membershipEvent) {

        // Of course, we're causing unnecessary traffic,
        // since all the nodes will receive the event but we don't care,
        // because the map is generally a low activity one.
        String n = membershipEvent.getMember()
            .getAttribute(SystemConfigurationConstants.SYSTEM_NODE_NAME_ATTRIBUTE);
        if (StringUtils.isNotBlank(n)) {
            LOGGER.info("Handling removal of node [{}].", n);
            nodes.delete(n);
        }
    }

    private void initNodeId() {

        ensureNodeId();

        String n = nodeId.getValue();
        nodes.lock(n);
        try {

            // 1. Check node name presence
            NodeIdentity ni = nodes.get(n);
            if (Objects.nonNull(ni)) {
                String message = "Platform start impossible - node name ({}) is already used by ({}).";
                LOGGER.error(message, n, ni);
                throw new PlatformFailureException(message, SystemExceptionIds.EX_SYSTEM_NODE_ID_OCCUPIED, n, ni);
            }

            // 2. Generate sequence from member UUID
            byte[] sequence = nodesDAO.process(n);

            // 3. Publish
            Member lm = hazelcastInstance.getCluster().getLocalMember();
            InetSocketAddress isa = null;
            try {
                isa = lm.getAddress().getInetSocketAddress();
            } catch (UnknownHostException e) {
                LOGGER.warn("Unknown host exception caught.", e);
            }

            this.nodeIdentity = NodeIdentity.builder()
                    .memberAddress(isa)
                    .memberSequence(sequence)
                    .memberUuid(lm.getUuid())
                    .memberVersion(lm.getVersion())
                    .build();

            nodes.put(n, nodeIdentity);

            // 4. Set the node name attribute on the instance (member)
            hazelcastInstance.getCluster().addMembershipListener(this);
            hazelcastInstance.getConfig()
                .getMemberAttributeConfig()
                .setAttribute(SystemConfigurationConstants.SYSTEM_NODE_NAME_ATTRIBUTE, n);

        } finally {
            nodes.unlock(n);
        }
    }
    /**
     * Initializes node id
     */
    private void initIdGenerators() {
        this.v1Generator = Generators.timeBasedGenerator(new EthernetAddress(this.nodeIdentity.getMemberSequence()));
        this.v4Generator = Generators.randomBasedGenerator();
    }
    /**
     * Initializes version numbers.
     */
    private void initVersionNumbers() {

        if (!versionString.hasValue()) {
            final String message = "backend.properties doesn't contain 'unidata.platform.version' property or platform version is not defined. Exiting!";
            LOGGER.error(message);
            throw new PlatformFailureException(message, SystemExceptionIds.EX_SYSTEM_PLATFORM_VERSION_UNDEFINED);
        }

        boolean versionFieldValid = false;
        String[] versionTokens = StringUtils.split(versionString.getValue(), '.');
        if (versionTokens.length >= 2 && versionTokens.length <= 4) {
            try {
                platformMajor = Integer.parseInt(versionTokens[0]);
                platformMinor = Integer.parseInt(versionTokens[1]);
                platformPatch = versionTokens.length > 2 ? Integer.parseInt(versionTokens[2]) : 0;
                platformRevision = versionTokens.length > 3 ? Integer.parseInt(versionTokens[3]) : 0;
                versionFieldValid = true;
            } catch (NumberFormatException e) {
                LOGGER.warn("NumberFormatException caught while parsing version number. Input {}.", versionString, e);
            }
        }

        if (!versionFieldValid) {
            final String message = "'unidata.platform.version' property contains invalid data. Version must be given in the major.minor[.patch optional][.revision optional] (all integer numbers) format. Exiting!";
            LOGGER.error(message);
            throw new PlatformFailureException(message, SystemExceptionIds.EX_SYSTEM_PLATFORM_VERSION_INVALID);
        }
    }

    private void ensureNodeId() {

        if (!nodeId.hasValue() || StringUtils.isBlank(nodeId.getValue())) {
            String message = "Node id (property 'org.unidata.mdm.system.node.id') is undefined. Platform cannot start.";
            LOGGER.error(message);
            throw new PlatformFailureException(message, SystemExceptionIds.EX_SYSTEM_NODE_ID_UNDEFINED);
        }

        if (nodeId.getValue().length() > NODE_NAME_MAX_LENGTH
         || !NODE_NAME_PATTERN.matcher(nodeId.getValue()).matches()) {
            String message = "Node id ({}) (property 'org.unidata.mdm.system.node.id') has invalid format (names with max length of 128, containing only '^[a-zA-Z][a-zA-Z0-9_-]*$' are allowed). Platform cannot start.";
            LOGGER.error(message, nodeId.getValue());
            throw new PlatformFailureException(message, SystemExceptionIds.EX_SYSTEM_NODE_ID_INVALID, nodeId.getValue());
        }
    }
}
