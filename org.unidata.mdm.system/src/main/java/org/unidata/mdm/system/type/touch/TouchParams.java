/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.type.touch;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Mikhail Mikhailov on Apr 21, 2021
 */
public class TouchParams<T> {

    private final Touch<T> touch;

    private Map<String, Object> params;
    /**
     * Constructor.
     */
    public TouchParams(Touch<T> t) {
        super();

        Objects.requireNonNull(t, "Touch object must not be null.");
        this.touch = t;
    }
    /**
     * Adds touch param.
     * @param name param's name
     * @param value param's value
     * @return self
     */
    public TouchParams<T> with(String name, Object value) {

        if (params == null) {
            params = new HashMap<>();
        }

        params.put(name, value);
        return this;
    }
    /**
     * Gets supplied touch.
     * @return the touch
     */
    public Touch<T> getTouch() {
        return touch;
    }
    /**
     * Gets named param by name.
     * @param <X> param's expected type (CCE may be thrown, if the type is wrong).
     * @param name param's name
     * @return param value
     */
    @SuppressWarnings("unchecked")
    public<X> X getParam(String name) {
        return (X) getParams().get(name);
    }
    /**
     * Gets all params as map
     * @return the params
     */
    public Map<String, Object> getParams() {
        return Objects.isNull(params) ? Collections.emptyMap() : params;
    }
}
