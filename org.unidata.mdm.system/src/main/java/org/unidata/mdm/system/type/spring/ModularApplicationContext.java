/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.type.spring;

import java.util.Collection;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
/**
 * @author Mikhail Mikhailov on Sep 22, 2021
 * Modular context interface
 */
public interface ModularApplicationContext extends ConfigurableApplicationContext {
    /**
     * Gets the first context in the chain.
     * @return context
     */
    AbstractApplicationContext getFirst();
    /**
     * Gets the last context in the chain.
     * @return context
     */
    AbstractApplicationContext getLast();
    /**
     * Adds a child context.
     * @param context the context
     */
    void addChild(AbstractApplicationContext context);
    /**
     * Gets a child by id.
     * @param id the id
     * @return child or null
     */
    AbstractApplicationContext getChild(String id);
    /**
     * All children as a collection.
     * @return children
     */
    Collection<AbstractApplicationContext> getChildren();
    /**
     * Gets the number of contexts currently running.
     * @return number of contexts
     */
    int getSize();
    /**
     * All ids as a collection.
     * @return children ids
     */
    Collection<String> getIds();
    /**
     * @return the ready
     */
    boolean isReady();
    /**
     * @param ready the ready to set
     */
    void setReady(boolean ready);
}