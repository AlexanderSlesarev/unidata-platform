/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.system.dao.PoolSettingConfigurer;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.exception.SystemExceptionIds;

/**
 * @author mikhail
 * Data source factory utils.
 */
public final class DataSourceUtils {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourceUtils.class);
    /**
     * Tomcat DSF class name.
     */
    private static final String TOMCAT_FACTORY_CLASS_NAME = "org.apache.tomcat.jdbc.pool.DataSourceFactory";
    /**
     * Tomcat pool class name.
     */
    private static final String TOMCAT_DATA_SOURCE_CLASS_NAME = "org.apache.tomcat.jdbc.pool.DataSource";
    /**
     * Apache DS factory instance.
     */
    private static final Object TOMCAT_DATA_SOURCE_FACTORY;
    /**
     * 'createDataSource' method.
     */
    private static final Method TOMCAT_CREATE_DATA_SOURCE_METHOD;
    /**
     * Tomcat pool class name.
     */
    private static final String BITRONIX_DATA_SOURCE_CLASS_NAME = "bitronix.tm.resource.jdbc.PoolingDataSource";
    /**
     * Configurer.
     */
    private static final PoolSettingConfigurer POOL_SETTINGS_CONFIGURER = new PoolSettingConfigurer();
    /**
     * Static initialization.
     */
    static {

        // Bad things happen here
        Object instance = null;
        Method method = null;
        try {
            Class<?> klass = Class.forName(DataSourceUtils.TOMCAT_FACTORY_CLASS_NAME);
            instance = klass.getDeclaredConstructor().newInstance();
            method = klass.getMethod("createDataSource", Properties.class);
        } catch (ClassNotFoundException | InstantiationException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            final String message = "Failed to instantiate '{}' NON-XA data source factory class.";
            LOGGER.error(message, TOMCAT_FACTORY_CLASS_NAME, e);
            throw new PlatformFailureException(message,
                    SystemExceptionIds.EX_SYSTEM_CANNOT_INITIALIZE_NON_XA_FACTORY,
                    TOMCAT_FACTORY_CLASS_NAME);
        } finally {
            TOMCAT_DATA_SOURCE_FACTORY = instance;
            TOMCAT_CREATE_DATA_SOURCE_METHOD = method;
        }
    }
    /**
     * Constructor.
     */
    private DataSourceUtils() {
        super();
    }
    /**
     * Creates a data source.
     * @param properties the properties
     * @return data source or null
     */
    public static DataSource newPoolingNonXADataSource(Properties properties) {

        if (Objects.isNull(TOMCAT_DATA_SOURCE_FACTORY)
         || Objects.isNull(TOMCAT_CREATE_DATA_SOURCE_METHOD)) {
            return null;
        }

        try {
            return (DataSource) TOMCAT_CREATE_DATA_SOURCE_METHOD.invoke(TOMCAT_DATA_SOURCE_FACTORY, properties);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            final String message = "Failed to create NON-XA data source.";
            throw new PlatformFailureException(message, e, SystemExceptionIds.EX_SYSTEM_CANNOT_CREATE_NON_XA_DATASOURCE);
        }
    }
    /**
     * Creates a data source.
     * @param properties the properties
     * @return data source or null
     */
    public static DataSource newPoolingXADataSource(Properties properties) {
        return POOL_SETTINGS_CONFIGURER.configure(properties);
    }

    // Bad stuff.
    public static void shutdown(DataSource ds) {

        if (Objects.isNull(ds)) {
            return;
        }

        try {

            Class<?> tomcatDataSourceKlass = Class.forName(TOMCAT_DATA_SOURCE_CLASS_NAME);
            Class<?> bitronixDataSourceKlass = Class.forName(BITRONIX_DATA_SOURCE_CLASS_NAME);

            if (bitronixDataSourceKlass.isAssignableFrom(ds.getClass())) {
                Method m = ds.getClass().getMethod("close"); // Spring proxy
                m.invoke(ds);
            } else if (tomcatDataSourceKlass.isAssignableFrom(ds.getClass())) {
                Method m = ds.getClass().getMethod("close", boolean.class); // Spring proxy
                m.invoke(ds, true);
            }

        } catch (Exception e) {
            LOGGER.warn("Exception caught, while closing pooling data source!", e);
        }
    }
}
