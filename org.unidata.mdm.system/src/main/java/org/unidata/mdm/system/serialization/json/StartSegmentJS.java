package org.unidata.mdm.system.serialization.json;

/**
 * @author Mikhail Mikhailov on May 25, 2020
 */
public class StartSegmentJS extends SegmentJS {
    /**
     * Constructor.
     */
    public StartSegmentJS() {
        super();
    }
}
