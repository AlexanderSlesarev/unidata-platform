/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.unidata.mdm.system.service.TouchService;
import org.unidata.mdm.system.type.touch.Touch;
import org.unidata.mdm.system.type.touch.TouchParams;
import org.unidata.mdm.system.type.touch.Touchable;

/**
 * @author Mikhail Mikhailov on Apr 22, 2021
 * Touch service implementation.
 */
@Service
public class TouchServiceImpl implements TouchService {
    /**
     * This service logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TouchServiceImpl.class);
    /**
     * A very simple touchable registry.
     */
    private final Map<Touch<?>, List<Touchable>> registry = new HashMap<>();
    /**
     * Constructor.
     */
    public TouchServiceImpl() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public <X> X singleTouch(TouchParams<X> p) {

        if (Objects.isNull(p) || Objects.isNull(p.getTouch())) {
            return null;
        }

        List<Touchable> touchables = registry.get(p.getTouch());
        if (CollectionUtils.isEmpty(touchables)) {
            LOGGER.warn("No touchables registerd for touch type [{}].", p.getTouch().getTouchName());
            return null;
        }

        for (int i = 0; i < touchables.size(); i++) {

            Touchable touchable = touchables.get(i);
            X result = execTouchable(touchable, p);

            if (p.getTouch().getOutputType() == Void.class) {
                return null;
            }

            if (Objects.nonNull(result)) {
                return result;
            }
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public <X> Collection<X> multipleTouch(TouchParams<X> p) {

        if (Objects.isNull(p) || Objects.isNull(p.getTouch())) {
            return Collections.emptyList();
        }

        List<Touchable> touchables = registry.get(p.getTouch());
        if (CollectionUtils.isEmpty(touchables)) {
            LOGGER.warn("No touchables registerd for touch type [{}].", p.getTouch().getTouchName());
            return Collections.emptyList();
        }

        boolean isVoid = p.getTouch().getOutputType() == Void.class;
        List<X> result = isVoid ? Collections.emptyList() : new ArrayList<>(touchables.size());
        for (int i = 0; i < touchables.size(); i++) {

            Touchable touchable = touchables.get(i);
            X retval = execTouchable(touchable, p);

            if (!isVoid && Objects.nonNull(retval)) {
                result.add(retval);
            }
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    private<X> X execTouchable(Touchable touchable, TouchParams<?> p) {

        try {

            Object o = touchable.touch(p);
            if (null == o || p.getTouch().getOutputType() == Void.class) {
                return null;
            }

            if (!p.getTouch().getOutputType().isAssignableFrom(o.getClass())) {
                throw new ClassCastException("Touchable result [" + o.getClass().getName()
                        + "] is not compatible with touch output type [" + p.getTouch().getOutputType().getName()
                        + "].");
            }

            return (X) o;

        } catch (Exception e) {
            LOGGER.warn("Exception caught while executing touchable element.", e);
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void register(Touchable t, Touch<?>... touches) {

        Objects.requireNonNull(t, "Touchable instance must not be null.");
        if (ArrayUtils.isEmpty(touches)) {
            return;
        }

        for (int i = 0; i < touches.length; i++) {

            if (Objects.isNull(touches[i])) {
                continue;
            }

            registry.computeIfAbsent(touches[i], k -> new ArrayList<>()).add(t);
        }
    }
}
