/**
 * Semi-private configuration API implementation classes.
 * @author Mikhail Mikhailov on Apr 21, 2020
 */
package org.unidata.mdm.system.type.configuration.impl;