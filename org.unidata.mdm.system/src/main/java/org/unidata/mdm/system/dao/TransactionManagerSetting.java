/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.dao;

import java.util.Objects;
import java.util.Properties;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Mikhail Mikhailov on May 16, 2021
 */
public enum TransactionManagerSetting {
    /**
     * Journal type ('disk', null, class name).
     */
    BTM_JOURNAL_TYPE("journal", "disk", String.class),
    /**
     * TX log, part 1.
     */
    BTM_LOG_PART_ONE("journal.disk.logPart1Filename", "btm1.tlog", String.class),
    /**
     * TX log, part 2.
     */
    BTM_LOG_PART_TWO("journal.disk.logPart2Filename", "btm2.tlog", String.class),
    /**
     * Write enabled.
     */
    BTM_FORCE_WRITE_ENABLED("journal.disk.forcedWriteEnabled", true, Boolean.class),
    /**
     * Journal batching enabled.
     */
    BTM_FORCE_BATCHING_ENABLED("journal.disk.forceBatchingEnabled", true, Boolean.class),
    /**
     * Log size in MB.
     */
    BTM_MAX_LOG_SIZE_IN_MB("journal.disk.maxLogSize", 2, Integer.class),
    /**
     * Filter log status.
     */
    BTM_FILTER_LOG_STATUS("journal.disk.filterLogStatus", false, Boolean.class),
    /**
     * Skip corrupted logs.
     */
    BTM_SKIP_CORRUPTED_LOGS("journal.disk.skipCorruptedLogs",  false, Boolean.class),
    /**
     * Perform 2PC async.
     */
    BTM_ASYNCHRONOUS_2PC("2pc.async", false, Boolean.class),
    /**
     * Warn in case of zero resource transaction.
     */
    BTM_WARN_ABOUT_ZERO_RESOURCE_TRANSACTION("2pc.warnAboutZeroResourceTransactions", true, Boolean.class),
    /**
     * Debug zero resource transactions.
     */
    BTM_DEBUG_ZERO_RESOURCE_TRANSACTION("2pc.debugZeroResourceTransactions", false, Boolean.class),
    /**
     * TX timeout.
     */
    BTM_DEFAULT_TRANSACTION_TIMEOUT("timer.defaultTransactionTimeout", 60, Integer.class),
    /**
     * SD interval.
     */
    BTM_GRACEFUL_SHUTDOWN_INTERVAL("timer.gracefulShutdownInterval", 60, Integer.class),
    /**
     * Recovery seconds interval.
     */
    BTM_BACKGROUND_RECOVERY_INTERVAL_SECONDS("timer.backgroundRecoveryIntervalSeconds", 60, Integer.class),
    /**
     * Disable JMX.
     */
    BTM_DISABLE_JMX("disableJmx", false, Boolean.class),
    /**
     * Sync JMX registration.
     */
    BTM_SYNCHRONOUS_JMX_REGISTRATION("jmx.sync", false, Boolean.class),
    /**
     * JNDI TX name.
     */
    BTM_JNDI_USER_TRANSACTION_NAME("jndi.userTransactionName", "java:comp/UserTransaction", String.class),
    /**
     * TSR name.
     */
    BTM_JNDI_TRANSACTION_SYNCHRONIZATION_REGISTRY_NAME("jndi.transactionSynchronizationRegistryName",  "java:comp/TransactionSynchronizationRegistry", String.class),
    /**
     * EA class name.
     */
    BTM_EXCEPTION_ANALYZER("exceptionAnalyzer",  null, String.class),
    /**
     * Recovery.
     */
    BTM_CURRENT_NODE_ONLY_RECOVERY("currentNodeOnlyRecovery", true, Boolean.class),
    /**
     * Allow multi LRC.
     */
    BTM_ALLOW_MULTIPLE_LRC("allowMultipleLrc", false, Boolean.class),
    /**
     * Resource loading configuration filename.
     */
    BTM_RESOURCE_CONFIGURATION_FILENAME("resource.configuration", null, String.class),
    /**
     * Conservative journaling.
     */
    BTM_CONSERVATIVE_JOURNALING("conservativeJournaling", false, Boolean.class),
    /**
     * Set java factory if none defined (this is different to factory configuration, which defines 'auto').
     */
    BTM_JDBC_PROXY_FACTORY_CLASS("jdbcProxyFactoryClass", "bitronix.tm.resource.jdbc.proxy.JdbcJavaProxyFactory", String.class),
    ;
    /**
     * Bean property name.
     */
    private final String propertyName;
    /**
     * Default value.
     */
    private final Object defaultValue;
    /**
     * Target type.
     */
    private final Class<?> targetType;
    /**
     * Constructor.
     * @param configurationName
     * @param propertyName
     * @param valueSetter
     */
    private TransactionManagerSetting(String propertyName, Object defaultValue, Class<?> targetType) {
        this.propertyName = propertyName;
        this.defaultValue = defaultValue;
        this.targetType = targetType;
    }
    /**
     * Property name.
     */
    public String getPropertyName() {
        return propertyName;
    }
    /**
     * @return the defaultValue
     */
    public Object getDefaultValue() {
        return defaultValue;
    }
    /**
     * Extracts supplied value for the property name and returns it, if it was set or returns the default value.
     * @param properties the properties to extract value from
     * @return value
     */
    @SuppressWarnings("unchecked")
    public<X> X getOrDefault(Properties properties) {

        if (Objects.isNull(properties)) {
            return (X) defaultValue;
        }

        String v = properties.getProperty(propertyName);
        if (StringUtils.isBlank(v)) {
            return (X) defaultValue;
        }

        if (targetType == Boolean.class) {
            return (X) BooleanUtils.toBooleanObject(StringUtils.trim(v));
        } else if (targetType == Integer.class) {
            return (X) Integer.valueOf(StringUtils.trim(v));
        }

        // String
        return (X) StringUtils.trim(v);
    }
}
