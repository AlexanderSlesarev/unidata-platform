/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.exception;

import org.unidata.mdm.system.exception.ExceptionId;

/**
 * @author Alexander Malyshev
 */
public final class DataRestExceptionIds {
    private DataRestExceptionIds() { }

    public static final ExceptionId EX_DATA_ETALON_DELETE_FAILED =
            new ExceptionId("EX_DATA_ETALON_DELETE_FAILED", "");

    public static final ExceptionId EX_DATA_ETALON_WIPE_FAILED =
            new ExceptionId("EX_DATA_ETALON_WIPE_FAILED", "");

    public static final ExceptionId EX_DATA_ETALON_NOT_FOUND =
            new ExceptionId("EX_DATA_ETALON_NOT_FOUND", "");

    public static final ExceptionId EX_DATA_ORIGIN_DEACTIVATION_FAILED =
            new ExceptionId("EX_DATA_ORIGIN_DEACTIVATION_FAILED", "");

    public static final ExceptionId EX_DATA_INVALID_CLOB_OBJECT =
            new ExceptionId("EX_DATA_INVALID_CLOB_OBJECT", "");

    public static final ExceptionId EX_DATA_CANNOT_DELETE_REF_EXIST =
            new ExceptionId("EX_DATA_CANNOT_DELETE_REF_EXIST", "");

    public static final ExceptionId EX_DATA_ORIGIN_DELETE_FAILED =
            new ExceptionId("EX_DATA_ORIGIN_DELETE_FAILED", "");

    public static final ExceptionId EX_DATA_BOUNDARY_TIMESTAMPS_ARE_NOT_EVEN =
            new ExceptionId("EX_DATA_BOUNDARY_TIMESTAMPS_ARE_NOT_EVEN", "");

    public static final ExceptionId EX_SEARCH_ILLEGAL_CHILD_TYPE_FOR_POST_PROCESSING =
            new ExceptionId("EX_SEARCH_ILLEGAL_CHILD_TYPE_FOR_POST_PROCESSING", "");
}
