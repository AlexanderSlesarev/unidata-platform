/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michael Yashin. Created on 02.06.2015.
 */
public class NestedRecordRO {

    /**
     * Simple attributes.
     */
    private List<SimpleAttributeRO> simpleAttributes = new ArrayList<>();
    /**
     * Simple attributes.
     */
    private List<ArrayAttributeRO> arrayAttributes = new ArrayList<>();
    /**
     * Complex attributes.
     */
    private List<ComplexAttributeRO> complexAttributes = new ArrayList<>();
    // TODO: 07.11.2019 DQ Module 
    /**
     * DQ errors list.
     */
//    private List<DQErrorRO> dqErrors = new ArrayList<>();
    /**
     * Gets simple attributes.
     * @return list
     */
    public List<SimpleAttributeRO> getSimpleAttributes() {
        return simpleAttributes;
    }
    /**
     * Sets simple attributes.
     * @param simpleAttributes attributes list
     */
    public void setSimpleAttributes(List<SimpleAttributeRO> simpleAttributes) {
        this.simpleAttributes = simpleAttributes;
    }
    /**
     * @return the arrayAttributes
     */
    public List<ArrayAttributeRO> getArrayAttributes() {
        return arrayAttributes;
    }
    /**
     * @param arrayAttributes the arrayAttributes to set
     */
    public void setArrayAttributes(List<ArrayAttributeRO> arrayAttributes) {
        this.arrayAttributes = arrayAttributes;
    }
    /**
     * Gets simple attributes.
     * @return complex attributes list
     */
    public List<ComplexAttributeRO> getComplexAttributes() {
        return complexAttributes;
    }
    /**
     * Sets simple attributes.
     * @param complexAttributes complex attributes list
     */
    public void setComplexAttributes(List<ComplexAttributeRO> complexAttributes) {
        this.complexAttributes = complexAttributes;
    }
    // TODO: 07.11.2019 DQ Module
//
//    /**
//     * @return the errors
//     */
//    public List<DQErrorRO> getDqErrors() {
//        if (dqErrors == null) {
//            this.dqErrors = new ArrayList<>();
//        }
//        return dqErrors;
//    }
//
//    /**
//     * @param errors
//     *            the errors to set
//     */
//    public void setDqErrors(List<DQErrorRO> errors) {
//        this.dqErrors = errors;
//    }

}
