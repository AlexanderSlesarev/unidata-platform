/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.converter;

import org.unidata.mdm.data.context.DeleteRelationRequestContext;
import org.unidata.mdm.rest.data.ro.FullRecordRO;
import org.unidata.mdm.rest.data.ro.RelationDeleteWrapperRO;
import org.unidata.mdm.system.util.ConvertUtils;


/**
 * @author Dmitry Kopin on 20.06.2018.
 */
public final class FullRecordConverter {
    private FullRecordConverter() { }

    public static DeleteRelationRequestContext convert(RelationDeleteWrapperRO deleteWrapper, FullRecordRO fullRecordRO) {
        // we already remove period relation UN-10507
        return DeleteRelationRequestContext.builder()
                .relationName(deleteWrapper.getRelName())
                .entityName(fullRecordRO.getDataRecord().getEntityName())
                .relationEtalonKey(deleteWrapper.getEtalonRelationId())
                .inactivateEtalon(false)
                .relationOriginKey(deleteWrapper.getOriginRelationId())
                .inactivateOrigin(false)
                .inactivatePeriod(true)
                .validFrom(ConvertUtils.localDateTime2Date(deleteWrapper.getValidFrom()))
                .validTo(ConvertUtils.localDateTime2Date(deleteWrapper.getValidTo()))
                .wipe(deleteWrapper.isWipe())
                .build();
    }
}
