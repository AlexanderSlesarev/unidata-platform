/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.data.rendering;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.unidata.mdm.data.dto.GetRecordDTO;
import org.unidata.mdm.data.dto.UpsertRecordDTO;
import org.unidata.mdm.matching.data.dto.RecordMatchingGetResult;
import org.unidata.mdm.matching.data.dto.RecordMatchingUpsertResult;
import org.unidata.mdm.rest.system.service.TypedRestOutputRenderer;
import org.unidata.mdm.rest.v1.data.ro.atomic.AtomicDataGetResultRO;
import org.unidata.mdm.rest.v1.data.ro.atomic.AtomicDataUpsertResultRO;
import org.unidata.mdm.rest.v1.data.type.rendering.DataRestOutputRenderingAction;
import org.unidata.mdm.rest.v1.matching.data.converter.GetRecordMatchingConverter;
import org.unidata.mdm.rest.v1.matching.data.converter.UpsertRecordMatchingConverter;
import org.unidata.mdm.rest.v1.matching.data.module.MatchingDataRestModule;
import org.unidata.mdm.rest.v1.matching.data.ro.GetRecordMatchingResultRO;
import org.unidata.mdm.rest.v1.matching.data.ro.UpsertRecordsMatchingResultRO;
import org.unidata.mdm.system.type.rendering.FieldDef;
import org.unidata.mdm.system.type.rendering.FragmentDef;
import org.unidata.mdm.system.type.rendering.OutputFragmentRenderer;
import org.unidata.mdm.system.type.rendering.OutputRenderingAction;
import org.unidata.mdm.system.type.rendering.RenderingProvider;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * @author Sergey Murskiy on 22.08.2021
 */
@Component
public class RecordsMatchingRenderingProvider implements RenderingProvider {
    /**
     * Upsert record matching converter.
     */
    @Autowired
    private UpsertRecordMatchingConverter upsertRecordMatchingConverter;
    /**
     * Get record matching converter.
     */
    @Autowired
    private GetRecordMatchingConverter getRecordMatchingConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<OutputFragmentRenderer> get(@NonNull OutputRenderingAction action) {

        if (DataRestOutputRenderingAction.ATOMIC_UPSERT_OUTPUT == action) {
            return Collections.singletonList(new RecordsMatchingUpsertResultRenderer());
        } else if (DataRestOutputRenderingAction.ATOMIC_GET_OUTPUT == action) {
            return Collections.singletonList(new RecordsMatchingGetResultRenderer());
        }

        return Collections.emptyList();
    }

    /**
     * Upsert rendering.
     */
    private class RecordsMatchingUpsertResultRenderer extends TypedRestOutputRenderer<AtomicDataUpsertResultRO, UpsertRecordDTO> {

        /**
         * Constructor.
         */
        public RecordsMatchingUpsertResultRenderer() {
            super( AtomicDataUpsertResultRO.class,
                    UpsertRecordDTO.class,
                    Collections.singletonList(FieldDef.fieldDef(MatchingDataRestModule.MODULE_ID, UpsertRecordsMatchingResultRO.class)));
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Map<String, Object> renderFragmentFields(FragmentDef fragmentDef, UpsertRecordDTO container) {
            return Collections.singletonMap(
                    MatchingDataRestModule.MODULE_ID,
                    upsertRecordMatchingConverter.to(container.fragment(RecordMatchingUpsertResult.ID)));
        }
    }

    /**
     * Get renderer.
     */
    private class RecordsMatchingGetResultRenderer extends TypedRestOutputRenderer<AtomicDataGetResultRO, GetRecordDTO> {

        /**
         * Constructor.
         */
        public RecordsMatchingGetResultRenderer() {
            super(  AtomicDataGetResultRO.class,
                    GetRecordDTO.class,
                    Collections.singletonList(FieldDef.fieldDef(MatchingDataRestModule.MODULE_ID, GetRecordMatchingResultRO.class)));
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected Map<String, Object> renderFragmentFields(FragmentDef fragmentDef, GetRecordDTO container) {
            return Collections.singletonMap(
                    MatchingDataRestModule.MODULE_ID,
                    getRecordMatchingConverter.to(container.fragment(RecordMatchingGetResult.ID)));
        }
    }
}
