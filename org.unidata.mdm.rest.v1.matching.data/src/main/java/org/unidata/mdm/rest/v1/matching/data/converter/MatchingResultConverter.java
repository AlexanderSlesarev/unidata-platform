/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.data.converter;

import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.dto.MatchingResult;
import org.unidata.mdm.rest.v1.matching.data.ro.RecordsClusterRO;
import org.unidata.mdm.system.convert.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Sergey Murskiy on 14.09.2021
 */
@Component
public class MatchingResultConverter extends Converter<MatchingResult, List<RecordsClusterRO>> {

    /**
     * Constructor.
     */
    public MatchingResultConverter() {
        this.to = this::convert;
    }

    private List<RecordsClusterRO> convert(MatchingResult source) {
        if (MapUtils.isEmpty(source.getResults())) {
            return Collections.emptyList();
        }

        List<RecordsClusterRO> target = new ArrayList<>();

        source.getResults().forEach((key, value) -> value.forEach(ruleResult -> {

            RecordsClusterRO cluster = new RecordsClusterRO();
            cluster.setId(ruleResult.getCluster().getId());
            cluster.setRuleName(ruleResult.getRule().getName());
            cluster.setRuleDisplayName(ruleResult.getRule().getDisplayName());
            cluster.setSetName(key.getName());
            cluster.setSetDisplayName(key.getDisplayName());
            cluster.setMatchingDate(ruleResult.getCluster().getMatchingDate());
            cluster.setRecordsCount(ruleResult.getCluster().getRecordsCount());

            target.add(cluster);
        }));

        return target;
    }
}
