/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.data.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.data.dto.RecordMatchingUpsertResult;
import org.unidata.mdm.rest.v1.data.converter.TimelineToTimelineROConverter;
import org.unidata.mdm.rest.v1.matching.data.ro.PeriodMatchingResultRO;
import org.unidata.mdm.rest.v1.matching.data.ro.UpsertRecordsMatchingResultRO;
import org.unidata.mdm.system.convert.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.08.2021
 */
@Component
public class UpsertRecordMatchingConverter extends Converter<RecordMatchingUpsertResult, UpsertRecordsMatchingResultRO> {

    /**
     * Matching result converter.
     */
    @Autowired
    private MatchingResultConverter matchingResultConverter;

    /**
     * Constructor.
     */
    public UpsertRecordMatchingConverter() {
        this.to = this::convert;
    }

    private UpsertRecordsMatchingResultRO convert(RecordMatchingUpsertResult source) {

        UpsertRecordsMatchingResultRO target = new UpsertRecordsMatchingResultRO();

        List<PeriodMatchingResultRO> periods = new ArrayList<>();
        source.getPayload().forEach((interval, result) -> {
            PeriodMatchingResultRO period = new PeriodMatchingResultRO();
            period.setInterval(TimelineToTimelineROConverter.convert(interval));
            period.setResults(Objects.isNull(result.getResults())
                    ? Collections.emptyList()
                    : matchingResultConverter.to(result));

            periods.add(period);
        });

        target.setPeriods(periods);

        return target;
    }
}
