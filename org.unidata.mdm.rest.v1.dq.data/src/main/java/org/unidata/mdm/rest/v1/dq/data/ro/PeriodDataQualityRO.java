package org.unidata.mdm.rest.v1.dq.data.ro;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.v1.data.ro.TimeIntervalRO;
import org.unidata.mdm.rest.v1.dq.core.ro.result.DataQualitySetResultRO;

/**
 * @author Mikhail Mikhailov on Apr 2, 2021
 */
public class PeriodDataQualityRO {
    /**
     * The relevant interval.
     */
    private TimeIntervalRO interval;
    /**
     * Results by set.
     */
    private List<DataQualitySetResultRO> results;
    /**
     * Constructor.
     */
    public PeriodDataQualityRO() {
        super();
    }
    /**
     * @return the results
     */
    public List<DataQualitySetResultRO> getResults() {
        return Objects.isNull(results) ? Collections.emptyList() : results;
    }
    /**
     * @param results the results to set
     */
    public void setResults(List<DataQualitySetResultRO> results) {
        this.results = results;
    }
    /**
     * @return the interval
     */
    public TimeIntervalRO getInterval() {
        return interval;
    }
    /**
     * @param interval the interval to set
     */
    public void setInterval(TimeIntervalRO interval) {
        this.interval = interval;
    }
}
