/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.data.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.v1.dq.data.ro.DeleteSandboxRecordRO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;

@Path("dq-sandbox")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class DQSandboxRestService extends AbstractRestService {

    private static final String RECORD_ID_PATH_PARAM = "recordId";
//    @Autowired
//    private SandboxRecordsService dqSandboxService;
//
//    @Autowired
//    private RenderingService renderingService;

    @GET
    @Operation(
        description = "Создать тестовую запись",
        method = HttpMethod.GET,
        responses = {
              @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = DeleteSandboxRecordRO.class)), responseCode = "200"),
              @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        })
    public Response dummy() {
        return ok(null);
    }

//    @POST
//    @Path("record")
//    @Operation(
//            description = "Создать тестовую запись",
//            method = "POST",
//            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = EtalonRecordRO.class)), description = "Параметры DQ"),
//            responses = {
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = EtalonRecordRO.class)), responseCode = "200"),
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
//            })
//    public Response create(final EtalonRecordRO sandboxRecord) {
//        sandboxRecord.setEtalonId(null);
//        final EtalonRecord etalonRecord = dqSandboxService.upsert(from(sandboxRecord));
//        return ok(new RestResponse<>(toEtalonRecordRO(etalonRecord)));
//    }
//
//    @PUT
//    @Path("record/{" + RECORD_ID_PATH_PARAM + "}")
//    @Operation(
//            description = "Обновить тестовую запись",
//            method = "PUT",
//            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = EtalonRecordRO.class)), description = "Параметры DQ"),
//            responses = {
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = EtalonRecordRO.class)), responseCode = "200"),
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
//            })
//    public Response update(@PathParam(RECORD_ID_PATH_PARAM) final long recordId, final EtalonRecordRO sandboxRecord) {
//        sandboxRecord.setEtalonId(String.valueOf(recordId));
//        final EtalonRecord etalonRecord = dqSandboxService.upsert(from(sandboxRecord));
//        return ok(new RestResponse<>(toEtalonRecordRO(etalonRecord)));
//    }
//
//    private EtalonRecord from(EtalonRecordRO sandboxRecord) {
//        EtalonRecordImpl etalonRecord = new EtalonRecordImpl(DataRecordEtalonConverter.from(sandboxRecord));
//        final EtalonRecordInfoSection infoSection = new EtalonRecordInfoSection();
//        infoSection.setEntityName(sandboxRecord.getEntityName());
//        infoSection.setEtalonKey(RecordEtalonKey.builder().id(sandboxRecord.getEtalonId()).build());
//        etalonRecord.setInfoSection(infoSection);
//        return etalonRecord;
//    }
//
//    @GET
//    @Path("test/{" + RECORD_ID_PATH_PARAM + "}")
//    public Response getTest(@PathParam(RECORD_ID_PATH_PARAM) final long recordId) {
//        return ok(new RestResponse<>("test success: " + recordId));
//    }
//
//
//    @GET
//    @Path("record/{" + RECORD_ID_PATH_PARAM + "}")
//    @Operation(
//            description = "Найти тестовую запись по ID",
//            method = "GET",
//            responses = {
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = EtalonRecordRO.class)), responseCode = "200"),
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
//            })
//    public Response findRecordById(@PathParam(RECORD_ID_PATH_PARAM) final long recordId) {
//        return ok(new RestResponse<>(toEtalonRecordRO(dqSandboxService.findRecordById(recordId))));
//    }
//
//    @DELETE
//    @Path("record/{" + RECORD_ID_PATH_PARAM + "}")
//    @Operation(
//            description = "Удалить тестовую запись по ID",
//            method = "DELETE",
//            responses = {
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = Boolean.class)), responseCode = "200"),
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
//            })
//    public Response deleteRecordById(@PathParam(RECORD_ID_PATH_PARAM) final long recordId) {
//        dqSandboxService.deleteRecords(Collections.singletonList(recordId), null);
//        return ok(new RestResponse<>(true));
//    }
//
//    @POST
//    @Path("record/delete")
//    @Operation(
//            description = "Удалить тестовые записи по запросу",
//            method = "POST",
//            responses = {
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = Boolean.class)), responseCode = "200"),
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
//            })
//    public Response deleteRecordByRequest(final DeleteSandboxRecordRO deleteSandboxRecordRO) {
//        dqSandboxService.deleteRecords(deleteSandboxRecordRO.getIds(), deleteSandboxRecordRO.getEntityName());
//        return ok(new RestResponse<>(true));
//    }
//
//    @POST
//    @Path("record/search")
//    @Operation(
//            description = "Поиск записей",
//            method = "POST",
//            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = EtalonRecordRO.class)), description = "Параметры"),
//            responses = {
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = SearchResultRO.class)), responseCode = "200"),
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
//            })
//    public Response searchRecords(final SearchComplexRO request) {
//
//        SearchRequestContextBuilder srcb = SearchRequestContext.builder(request.getEntity());
//        renderingService.renderInput(SearchRestInputRenderingAction.SIMPLE_SEARCH_INPUT, srcb, request);
//
//        return ok(SearchResultToRestSearchResultConverter.convert(dqSandboxService.searchRecords(srcb.build())));
//    }
//
//    @POST
//    @Path("run")
//
//    @Operation(
//            description = "Запуск правил качества для выбранных записей",
//            method = "POST",
//            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = RunDQRulesRO.class)), description = "Параметры"),
//            responses = {
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = RunDQRulesResultRO.class)), responseCode = "200"),
//                    @io.swagger.v3.oas.annotations.responses.ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
//            })
//    public Response runDQRules(final RunDQRulesRO runDQRulesRO) {
//        final RunDQRulesContext runDQRulesContext = new RunDQRulesContext(
//                runDQRulesRO.getSelectedByIds(),
//                runDQRulesRO.getEntityName(),
//                runDQRulesRO.isSandbox(),
//                runDQRulesRO.getRules()
//        );
//        final List<RunDQRulesResultRO> result = dqSandboxService.runDataQualityRules(runDQRulesContext)
//                .entrySet().stream()
//                .map(e -> new RunDQRulesResultRO(
//                        DataRecordEtalonConverter.to(e.getKey(), Collections.emptyList()),
//                        e.getValue().stream()
//                                .map(DQSandboxRestService::toDQErrorRO)
//                                .collect(Collectors.toList())
//                ))
//                .collect(Collectors.toList());
//        return ok(new RestResponse<>(result));
//    }
//
//    private EtalonRecordRO toEtalonRecordRO(final EtalonRecord etalonRecord) {
//        //return DataRecordEtalonConverter.to(etalonRecord, Collections.emptyList(), Collections.emptyList());
//        return DataRecordEtalonConverter.to(etalonRecord, Collections.emptyList());
//    }
//
//    /**
//     * Convert DQ error
//     *
//     * @param sourceError source
//     * @return target
//     */
//    public static DQErrorRO toDQErrorRO(DQDataRecordError sourceError) {
//
//        DQErrorRO targetError = new DQErrorRO();
//        targetError.setCategory(sourceError.getCategory());
//        targetError.setMessage(sourceError.getMessage());
//        targetError.setRuleName(sourceError.getRuleName());
//        targetError.setSeverity(sourceError.getSeverity());
//        targetError.setPaths(sourceError.getValues().stream().map(Pair::getKey).collect(Collectors.toList()));
//        // TODO: 23.10.2019 classifiers build path
//        //targetError.setPaths(sourceError.getValues().stream().map(Pair::getKey).map(s -> sourceError.buildPath(s)).collect(Collectors.toList()));
//
//        return targetError;
//    }
}
