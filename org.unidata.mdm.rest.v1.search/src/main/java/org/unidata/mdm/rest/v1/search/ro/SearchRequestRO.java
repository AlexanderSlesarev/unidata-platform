package org.unidata.mdm.rest.v1.search.ro;

import org.unidata.mdm.rest.system.ro.CompositeInputRO;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestInputRenderingAction;
import org.unidata.mdm.system.type.rendering.InputRenderingAction;

/**
 * Renderable search request
 *
 * @author Alexandr Serov
 * @since 04.12.2020
 **/
public class SearchRequestRO extends CompositeInputRO {

    @Override
    public InputRenderingAction getInputRenderingAction() {
        return SearchRestInputRenderingAction.SIMPLE_SEARCH_INPUT;
    }

}
