package org.unidata.mdm.rest.v1.search.ro;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.Parameter;

/**
 * Search query
 *
 * @author Alexandr Serov
 * @since 02.12.2020
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchQueryRO {


    @Parameter(description = "Entity (index / type) name")
    private String entity;

    @Parameter(description = "Search text")
    private String text;

    @Parameter(description = "Date hint")
    private LocalDateTime asOf;

    @Parameter(description = "Search fields")
    private List<String> searchFields;

    @Parameter(description = "Fields")
    private List<SearchFormFieldRO> formFields;

    @Parameter(description = "Fields groups")
    private List<SearchFormFieldsGroupRO> formGroups;


    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getAsOf() {
        return asOf;
    }

    public void setAsOf(LocalDateTime asOf) {
        this.asOf = asOf;
    }

    public List<String> getSearchFields() {
        return searchFields;
    }

    public void setSearchFields(List<String> searchFields) {
        this.searchFields = searchFields;
    }

    public List<SearchFormFieldRO> getFormFields() {
        return formFields;
    }

    public void setFormFields(List<SearchFormFieldRO> formFields) {
        this.formFields = formFields;
    }

    public List<SearchFormFieldsGroupRO> getFormGroups() {
        return formGroups;
    }

    public void setFormGroups(List<SearchFormFieldsGroupRO> formGroups) {
        this.formGroups = formGroups;
    }
}
