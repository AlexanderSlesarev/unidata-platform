package org.unidata.mdm.meta.type.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

public class MatchingSettings implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<String> attributes;

    @JacksonXmlProperty(isAttribute = true)
    protected String strategyName;

    public List<String> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<>();
        }
        return this.attributes;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String value) {
        this.strategyName = value;
    }

    public MatchingSettings withAttributes(String... values) {
        if (values!= null) {
            Collections.addAll(getAttributes(), values);
        }
        return this;
    }

    public MatchingSettings withAttributes(Collection<String> values) {
        if (values!= null && !values.isEmpty()) {
            getAttributes().addAll(values);
        }
        return this;
    }

    public MatchingSettings withStrategyName(String value) {
        setStrategyName(value);
        return this;
    }

}
