package org.unidata.mdm.meta.type.event;

import org.unidata.mdm.system.type.event.AbstractMulticastEvent;

@Deprecated(forRemoval = true)
public class MetaModelImportEvent extends AbstractMulticastEvent {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -5882461539147072931L;

    public static final String OPERATION_ID = "MetaModelImportOperation";
    public static final String START_MAINTENANCE_STATUS = "START_MAINTENANCE";
    public static final String STOP_MAINTENANCE_STATUS = "STOP_MAINTENANCE";

    private String metaModelImportStatus;

    public MetaModelImportEvent(String metaModelImportStatus, String typeName, String id) {
        super(typeName, id);
        this.metaModelImportStatus = metaModelImportStatus;
    }

    public String getMetaModelImportStatus() {
        return metaModelImportStatus;
    }

    public void setMetaModelImportStatus(String metaModelImportStatus) {
        this.metaModelImportStatus = metaModelImportStatus;
    }

    @Override
    public boolean isLocal() {
        return true;
    }
}
