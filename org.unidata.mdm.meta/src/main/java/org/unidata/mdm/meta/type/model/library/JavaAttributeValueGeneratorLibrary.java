/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.type.model.library;

import java.io.ByteArrayInputStream;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarInputStream;
import javax.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.core.dto.UserLibraryResult;
import org.unidata.mdm.core.type.model.support.AttributeValueGenerator;
import org.unidata.mdm.meta.exception.MetaExceptionIds;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.service.ModuleService;
import org.unidata.mdm.system.util.JarUtils.SingleJarClassLoader;

/**
 * @author Dmitriy Bobrov on Oct 25, 2021
 */
public class JavaAttributeValueGeneratorLibrary extends AttributeValueGeneratorLibrary {
    /**
     * This class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JavaAttributeValueGeneratorLibrary.class);
    /**
     * This class loader.
     */
    private final SingleJarClassLoader classLoader;
    /**
     * The MS instance
     */
    private final ModuleService moduleService;
    /**
     * Beans.
     */
    private final Map<String, AttributeValueGenerator> beans = new ConcurrentHashMap<>();
    /**
     * Constructor.
     */
    public JavaAttributeValueGeneratorLibrary(ModuleService moduleService, UserLibraryResult ulr) {
        super(ulr);
        this.moduleService = moduleService;
        try (JarInputStream jis = new JarInputStream(new ByteArrayInputStream(ulr.getPayload()))) {
            this.classLoader = new SingleJarClassLoader(this.name, Thread.currentThread().getContextClassLoader(), jis);
        } catch (Exception e) {
            throw new PlatformFailureException("Exception caught while creating Java library instance.", e,
                MetaExceptionIds.EX_META_LIBRARY_CANNOT_BE_CREATED);
        }
    }
    /**
     * Gets function implementation
     * @param className the class name
     * @return function instance or null, if not found
     */
    public AttributeValueGenerator lookup(String className) {
        return beans.computeIfAbsent(className, key -> wireFunction(className));
    }
    /**
     * Lazy creates function implementation.
     * @param className the class name
     * @return function or NIL (not found/unreachable) indicator
     */
    private AttributeValueGenerator wireFunction(String className) {
        try {
            Class<?> klass = Class.forName(className, true, this.classLoader);
            if (!AttributeValueGenerator.class.isAssignableFrom(klass)) {
                throw new IllegalArgumentException("The class "
                    + klass.getName()
                    + " is NOT an instance of AttributeValueGenerator interface!");
            }
            Object o = klass.getConstructor().newInstance();
            return moduleService.initOrphanBean(klass.getSimpleName(), (AttributeValueGenerator) o);
        } catch (Exception e) {
            throw new PlatformFailureException("Cannot create Java auto generation instance for [{}].", e,
                MetaExceptionIds.EX_META_JAVA_AUTO_GENERATION_CANNOT_BE_CREATED, className);
        }
    }
    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Collection<String> suggest() {
        return classLoader.filterAssignableNames(AttributeValueGenerator.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanup() {

        beans.forEach((name, bean) -> {
            try {
                // Call @PreDestroy, destroy() etc.
                moduleService.releaseOrphanBean(bean);
            } catch (Exception e) {
                LOGGER.error("Exception caught while passivating a Java Cleanse Function [{}].", name, e);
            }
        });

        beans.clear();
        classLoader.cleanup();
    }
}
