package org.unidata.mdm.meta.type.model.merge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

public class BVTMergeType extends AbstractMergeType implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(localName = "attribute", namespace = META_MODEL_NAMESPACE)
    protected List<MergeAttribute> attributes;

    public List<MergeAttribute> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<>();
        }
        return this.attributes;
    }

    public BVTMergeType withAttributes(MergeAttribute... values) {
        if (values!= null) {
            Collections.addAll(getAttributes(), values);

        }
        return this;
    }

    public BVTMergeType withAttributes(Collection<MergeAttribute> values) {
        if (values!= null) {
            getAttributes().addAll(values);
        }
        return this;
    }

}
