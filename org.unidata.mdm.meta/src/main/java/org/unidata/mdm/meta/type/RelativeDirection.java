/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *
 */
package org.unidata.mdm.meta.type;

/**
 * @author Mikhail Mikhailov
 * Relation side, depending from resolution point.
 */
public enum RelativeDirection {
    /**
     * You resolve, looking for, match or relate something from the left (or the "from") side.
     * I. e. you have the left side record keys (or outgoing),
     * relation name and you would resolve the right side of the relation.
     */
    FROM,
    /**
     * You resolve, looking for, match or relate something from the right (or the "to") side.
     * I. e. you have the right side record keys (or incomming),
     * relation name and you would resolve the left side of the relation.
     */
    TO;
}
