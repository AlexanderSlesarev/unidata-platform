package org.unidata.mdm.meta.type.model;

/**
 * Constants for customizing object serialization
 *
 * @author Alexandr Serov
 * @since 29.07.2020
 **/
public final class ModelNamespace {

    private ModelNamespace() {
        super();
    }
    /**
     * module namespace
     */
    public static final String META_MODEL_NAMESPACE = "http://meta.mdm.unidata.org/";
}
