package org.unidata.mdm.meta.po;

/**
 * @author Mikhail Mikhailov on Oct 1, 2020
 * Source systems CLOB PO object.
 */
public class SourceSystemsPO extends AbstractModelPO {
    /**
     * Table name.
     */
    public static final String TABLE_NAME = "source_systems";
}
