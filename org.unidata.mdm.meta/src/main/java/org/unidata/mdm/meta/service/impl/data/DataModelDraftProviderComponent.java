package org.unidata.mdm.meta.service.impl.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.draft.type.DraftOperation;
import org.unidata.mdm.draft.type.DraftProvider;
import org.unidata.mdm.meta.serialization.MetaSerializer;
import org.unidata.mdm.meta.service.segments.ModelDraftGetStartExecutor;
import org.unidata.mdm.meta.service.segments.ModelDraftPublishStartExecutor;
import org.unidata.mdm.meta.service.segments.ModelDraftUpsertStartExecutor;
import org.unidata.mdm.meta.type.model.DataModel;
import org.unidata.mdm.system.service.TextService;

/**
 * @author Mikhail Mikhailov on Oct 14, 2020
 * Data model draft provider.
 */
@Component
public class DataModelDraftProviderComponent implements DraftProvider<DataModel> {
    /**
     * This draft provider id.
     */
    private static final String ID = "data-model";
    /**
     * This draft provider description.
     */
    private static final String DESCRIPTION = "app.meta.draft.data.model.provider.description";
    /**
     * The TS.
     */
    @Autowired
    private TextService textService;
    /**
     * Constructor.
     */
    @Autowired
    public DataModelDraftProviderComponent(DraftService draftService) {
        super();
        draftService.register(this);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return textService.getText(DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getPipelineId(DraftOperation operation) {

        switch (operation) {
        case GET_DATA:
            return ModelDraftGetStartExecutor.SEGMENT_ID;
        case PUBLISH_DATA:
            return ModelDraftPublishStartExecutor.SEGMENT_ID;
        case UPSERT_DATA:
            return ModelDraftUpsertStartExecutor.SEGMENT_ID;
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DataModel fromBytes(byte[] data) {
        return MetaSerializer.modelFromCompressedXml(data);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] toBytes(DataModel data) {
        return MetaSerializer.modelToCompressedXml(data);
    }
}
