package org.unidata.mdm.meta.exception;

import org.unidata.mdm.system.exception.DomainId;
import org.unidata.mdm.system.exception.ExceptionId;
import org.unidata.mdm.system.exception.PlatformRuntimeException;

/**
 * @author Mikhail Mikhailov on May 15, 2020
 */
public class ModelRuntimeException extends PlatformRuntimeException {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 7234942853851965123L;
    /**
     * Runtime exceptions from Model domain.
     */
    private static final DomainId MODEL_RUNTIME_EXCEPTION = () -> "MODEL_RUNTIME_EXCEPTION";
    /**
     * Constructor.
     * @param message
     * @param id
     * @param args
     */
    public ModelRuntimeException(String message, ExceptionId id, Object... args) {
        super(message, id, args);
    }
    /**
     * Constructor.
     * @param message
     * @param cause
     * @param id
     * @param args
     */
    public ModelRuntimeException(String message, Throwable cause, ExceptionId id, Object... args) {
        super(message, cause, id, args);
    }
    /**
     * Constructor.
     * @param cause
     * @param id
     * @param args
     */
    public ModelRuntimeException(Throwable cause, ExceptionId id, Object... args) {
        super(cause, id, args);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DomainId getDomain() {
        return MODEL_RUNTIME_EXCEPTION;
    }
}
