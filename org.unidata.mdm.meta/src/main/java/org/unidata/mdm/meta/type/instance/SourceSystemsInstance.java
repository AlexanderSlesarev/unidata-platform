package org.unidata.mdm.meta.type.instance;

import java.util.Collection;
import java.util.Map;

import javax.annotation.Nullable;

import org.unidata.mdm.core.type.model.ModelInstance;
import org.unidata.mdm.core.type.model.SourceSystemElement;
import org.unidata.mdm.meta.type.model.sourcesystem.SourceSystemsModel;

/**
 * @author Mikhail Mikhailov on Oct 6, 2020
 * A source systems set instance, defined for a storage (ID).
 */
public interface SourceSystemsInstance extends ModelInstance<SourceSystemsModel> {
    /**
     * Gets the admin element.
     * @return admin element
     */
    SourceSystemElement getAdminElement();
    /**
     * Gets a source system by name.
     * @param name the name
     * @return source system element or null
     */
    @Nullable
    SourceSystemElement getSourceSystem(String name);
    /**
     * Returns true if a source system with the given name exists.
     * @param name the source system name
     * @return true if a source system with the given name exists.
     */
    boolean exists(String name);
    /**
     * Gets all source systems registered at the instance.
     * @return collection
     */
    Collection<SourceSystemElement> getSourceSystems();
    /**
     * Gets a map, where source systems are placed in ascending weight order.
     * @return map
     */
    Map<String, Integer> getAscendingMap();
    /**
     * Gets a map, where source systems are placed in descending weight order.
     * @return map
     */
    Map<String, Integer> getDescendingMap();
}
