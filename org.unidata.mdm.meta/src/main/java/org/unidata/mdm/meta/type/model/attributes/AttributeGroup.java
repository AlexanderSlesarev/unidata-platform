package org.unidata.mdm.meta.type.model.attributes;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.unidata.mdm.core.type.model.source.AbstractCustomPropertiesHolder;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class AttributeGroup extends AbstractCustomPropertiesHolder<AttributeGroup> implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -7546171746637645372L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private List<String> attributes;

    @JacksonXmlProperty(isAttribute = true)
    private int row;

    @JacksonXmlProperty(isAttribute = true)
    private int column;

    @JacksonXmlProperty(isAttribute = true)
    private String title;

    @JacksonXmlProperty(isAttribute = true)
    private boolean defaultGroup;

    public List<String> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<>();
        }
        return this.attributes;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int value) {
        this.row = value;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int value) {
        this.column = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * @return the defaultGroup
     */
    public boolean isDefaultGroup() {
        return defaultGroup;
    }

    /**
     * @param defaultGroup the defaultGroup to set
     */
    public void setDefaultGroup(boolean defaultGroup) {
        this.defaultGroup = defaultGroup;
    }

    public AttributeGroup withAttributes(String... values) {
        if (values!= null) {
            for (String value: values) {
                getAttributes().add(value);
            }
        }
        return this;
    }

    public AttributeGroup withAttributes(Collection<String> values) {
        if (values!= null) {
            getAttributes().addAll(values);
        }
        return this;
    }

    public AttributeGroup withRow(int value) {
        setRow(value);
        return this;
    }

    public AttributeGroup withColumn(int value) {
        setColumn(value);
        return this;
    }

    public AttributeGroup withTitle(String value) {
        setTitle(value);
        return this;
    }

    public AttributeGroup withDefaultGroup(boolean value) {
        setDefaultGroup(value);
        return this;
    }
}
