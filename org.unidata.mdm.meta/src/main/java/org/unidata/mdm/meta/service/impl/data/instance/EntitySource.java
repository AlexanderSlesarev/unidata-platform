package org.unidata.mdm.meta.service.impl.data.instance;

import org.unidata.mdm.meta.type.model.entities.AbstractEntity;

/**
 * @author Mikhail Mikhailov on Oct 19, 2020
 * An internal interface for editable source holding types.
 */
public interface EntitySource<T extends AbstractEntity<?>> {
    /**
     * Gets the source.
     * @return source
     */
    T getSource();
}
