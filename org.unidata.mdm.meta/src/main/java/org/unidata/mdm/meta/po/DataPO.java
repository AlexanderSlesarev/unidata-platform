package org.unidata.mdm.meta.po;

/**
 * @author Mikhail Mikhailov on Oct 2, 2020
 */
public class DataPO extends AbstractModelPO {
    /**
     * Table name.
     */
    public static final String TABLE_NAME = "data";
}
