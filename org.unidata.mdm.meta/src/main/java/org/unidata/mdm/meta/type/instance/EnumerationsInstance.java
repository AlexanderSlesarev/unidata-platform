package org.unidata.mdm.meta.type.instance;

import java.util.Collection;

import javax.annotation.Nullable;

import org.unidata.mdm.core.type.model.EnumerationElement;
import org.unidata.mdm.core.type.model.ModelInstance;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationsModel;

/**
 * @author Mikhail Mikhailov on Oct 6, 2020
 * A enumeration set instance, defined for a storage (ID).
 */
public interface EnumerationsInstance extends ModelInstance<EnumerationsModel> {
    /**
     * Gets all enumerations.
     * @return enumerations collection
     */
    Collection<EnumerationElement> getEnumerations();
    /**
     * Gets specific enumeration by name.
     * @param name the name
     * @return element or null
     */
    @Nullable
    EnumerationElement getEnumeration(String name);
    /**
     * Returns true, if an enumeration with the given name exists.
     * @param name the name to check
     * @return true, if an enumeration with the given name exists.
     */
    boolean exists(String name);
}
