/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.context;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.system.exception.ValidationResult;

/**
 * @author Mikhail Mikhailov on Oct 20, 2021
 * The check state.
 */
public class ConsistencyCheckContext {
    /**
     * The check deque.
     */
    private final Deque<Pair<DataRecord, EntityElement>> queue = new ArrayDeque<>();
    /**
     * Validation results.
     */
    private final List<ValidationResult> validations = new ArrayList<>();
    /**
     * The root record to check.
     */
    private final DataRecord data;
    /**
     * The root entity element.
     */
    private final EntityElement entity;
    /**
     * Some arbitrary payload.
     */
    private final Object payload;
    /**
     * Constructor.
     * @param data initial record
     * @param el initial element
     */
    private ConsistencyCheckContext(ConsistencyCheckContextBuilder b) {
        super();

        Objects.requireNonNull(b.data, "Root data record must not be null.");
        Objects.requireNonNull(b.entity, "Root entity element must not be null.");

        this.data = b.data;
        this.entity = b.entity;
        this.payload = b.payload;

        offer(this.data, this.entity);
    }
    /**
     * Gets top level record
     * @return the data
     */
    public DataRecord getData() {
        return data;
    }
    /**
     * Gets top level entity
     * @return the entity
     */
    public EntityElement getEntity() {
        return entity;
    }
    /**
     * Gets optional payload.
     * @return the payload
     */
    @SuppressWarnings("unchecked")
    public<X> X getPayload() {
        return (X) payload;
    }
    /**
     * Removes and returns an element from the head.
     * @return an element from the head or null, if the queue is empty
     */
    public Pair<DataRecord, EntityElement> poll() {
        return queue.poll();
    }
    /**
     * Adds an element to the tail and returns true, if successful.
     * @return true, if successful
     */
    public boolean offer(DataRecord data, EntityElement el) {

        if (Objects.isNull(data)) {
            return true;
        }

        return queue.offer(Pair.of(data, el));
    }
    /**
     * Adds an element to the tail and returns true, if successful.
     * @return true, if successful
     */
    public boolean offer(Collection<DataRecord> data, EntityElement el) {

        if (CollectionUtils.isNotEmpty(data)) {
            for (DataRecord r : data) {
                if (!offer(r, el)) {
                    return false;
                }
            }
        }

        return true;
    }
    /**
     * Appends a number of validation results.
     * @param results the validation results
     */
    public void append(ValidationResult...results) {

        if (ArrayUtils.isNotEmpty(results)) {
            append(Arrays.asList(results));
        }
    }
    /**
     * Appends a number of validation results.
     * @param results the validation results
     */
    public void append(Collection<ValidationResult> results) {

        if (CollectionUtils.isNotEmpty(results)) {
            for (ValidationResult result : results) {

                if (Objects.isNull(result)) {
                    continue;
                }

                validations.add(result);
            }
        }
    }
    /**
     * Returns list of collected validations.
     * @return list of collected validations
     */
    public Collection<ValidationResult> validations() {
        return validations;
    }
    /**
     * Returns true, if traversal is finished.
     * @return true, if traversal is finished
     */
    public boolean finished() {
        return queue.isEmpty();
    }
    /**
     * Returns true if the check run collected no validation results (the record is valid).
     * @return true if the check run collected no validation results (the record is valid)
     */
    public boolean valid() {
        return validations.isEmpty();
    }
    /**
     * Returns current validation errors count.
     * @return current validation errors count
     */
    public int count() {
        return validations.size();
    }
    /**
     * Creates builder instance.
     * @return builder instance
     */
    public static ConsistencyCheckContextBuilder builder() {
        return new ConsistencyCheckContextBuilder();
    }
    /**
     * @author Mikhail Mikhailov on Oct 21, 2021
     * Builder check class.
     */
    public static class ConsistencyCheckContextBuilder {
        /**
         * The root record to check.
         */
        private DataRecord data;
        /**
         * The root entoty element.
         */
        private EntityElement entity;
        /**
         * Some arbitrary payload.
         */
        private Object payload;
        /**
         * Constructor.
         */
        private ConsistencyCheckContextBuilder() {
            super();
        }
        /**
         * Sets root data record.
         * @param data root data record
         * @return self
         */
        public ConsistencyCheckContextBuilder data(DataRecord data) {
            this.data = data;
            return this;
        }
        /**
         * Sets root entity element.
         * @param entity root entity element
         * @return self
         */
        public ConsistencyCheckContextBuilder entity(EntityElement entity) {
            this.entity = entity;
            return this;
        }
        /**
         * Sets optional payload.
         * @param payload optional payload
         * @return self
         */
        public ConsistencyCheckContextBuilder payload(Object payload) {
            this.payload = payload;
            return this;
        }
        /**
         * Builds CCC.
         * @return CCC instance
         */
        public ConsistencyCheckContext build() {
            return new ConsistencyCheckContext(this);
        }
    }
}
