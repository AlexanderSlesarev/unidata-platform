package org.unidata.mdm.meta.type.model.attributes;

import org.unidata.mdm.meta.type.model.SimpleDataType;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Base implementation for simple attribute
 */
public abstract class AbstractSimpleMetaModelAttribute<X extends AbstractSimpleMetaModelAttribute<X>>
    extends AbstractSearchableMetaModelAttribute<X>
    implements SimpleTypeMetaModelAttribute {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 988957139054589216L;

    @JacksonXmlProperty(isAttribute = true)
    private SimpleDataType simpleDataType;

    @JacksonXmlProperty(isAttribute = true)
    private boolean nullable;

    @JacksonXmlProperty(isAttribute = true)
    private boolean unique;

    @Override
    public SimpleDataType getSimpleDataType() {
        return simpleDataType;
    }

    @Override
    public void setSimpleDataType(SimpleDataType value) {
        this.simpleDataType = value;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    public X withSimpleDataType(SimpleDataType value) {
        setSimpleDataType(value);
        return self();
    }

    public X withNullable(boolean nullable) {
        setNullable(nullable);
        return self();
    }

    public X withUnique(boolean unique) {
        setUnique(unique);
        return self();
    }
}
