/**
 * @author Mikhail Mikhailov on Dec 1, 2020
 * Enumerations model bits.
 */
package org.unidata.mdm.meta.service.impl.enumerations;