/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.dto;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;

/**
 * @author Mikhail Mikhailov
 * Entities groups transfer object.
 */
public class GetEntitiesGroupsDTO {
    /**
     * Groups.
     */
    private Map<String, EntitiesGroup> groups;
    /**
     * Nested.
     */
    private Map<EntitiesGroup, Pair<List<Entity>, List<LookupEntity>>> nested;
    /**
     * Constructor.
     * @param groups groups
     * @param nested nested
     */
    public GetEntitiesGroupsDTO(Map<String, EntitiesGroup> groups, Map<EntitiesGroup, Pair<List<Entity>, List<LookupEntity>>> nested) {
        super();
        this.groups = groups;
        this.nested = nested;
    }
    /**
     * @return the groups
     */
    @Nonnull
    public Map<String, EntitiesGroup> getGroups() {
        return Objects.isNull(groups) ? Collections.emptyMap() : groups;
    }
    /**
     * @return the nested
     */
    @Nonnull
    public Map<EntitiesGroup, Pair<List<Entity>, List<LookupEntity>>> getNested() {
        return Objects.isNull(nested) ? Collections.emptyMap() : nested;
    }

    public Pair<List<Entity>, List<LookupEntity>> getNestedObjects(String id) {
        EntitiesGroup def = getGroups().get(id);
        return Objects.isNull(def) ? null : getNested().get(def);
    }

    public List<Entity> getNestedEntities(String id) {
        Pair<List<Entity>, List<LookupEntity>> deps = getNestedObjects(id);
        if (Objects.nonNull(deps)) {
             return Objects.isNull(deps.getLeft()) ? Collections.emptyList() : deps.getLeft();
        }

        return Collections.emptyList();
    }

    public List<LookupEntity> getNestedLookupEntities(String id) {
        Pair<List<Entity>, List<LookupEntity>> deps = getNestedObjects(id);
        if (Objects.nonNull(deps)) {
             return Objects.isNull(deps.getRight()) ? Collections.emptyList() : deps.getRight();
        }

        return Collections.emptyList();
    }
}
