/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.impl;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.context.UserLibraryGetContext;
import org.unidata.mdm.core.dto.UserLibraryResult;
import org.unidata.mdm.core.service.UserLibraryService;
import org.unidata.mdm.core.type.model.support.AttributeValueGenerator;
import org.unidata.mdm.meta.exception.MetaExceptionIds;
import org.unidata.mdm.meta.type.model.library.AttributeValueGeneratorLibrary;
import org.unidata.mdm.core.type.libraries.UserLibrariesListener;
import org.unidata.mdm.meta.type.model.library.GroovyAttributeValueGeneratorLibrary;
import org.unidata.mdm.meta.type.model.library.JavaAttributeValueGeneratorLibrary;
import org.unidata.mdm.meta.type.model.library.PythonAttributeValueGeneratorLibrary;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.service.ModuleService;

/**
 * @author Dmitriy Bobrov on Oct 25, 2021
 */
@Component
public class AttributeValueGeneratorCacheComponent implements UserLibrariesListener {
    /**
     * A ULS instance.
     */
    private final UserLibraryService userLibraryService;
    /**
     * A MS instance.
     */
    private final ModuleService moduleService;
    /**
     * Local libraries cache.
     */
    private Map<String, AttributeValueGeneratorLibrary> libraries = new ConcurrentHashMap<>();
    /**
     * Constructor
     */
    @Autowired
    public AttributeValueGeneratorCacheComponent(UserLibraryService userLibraryService,
        ModuleService moduleService) {
        super();
        this.userLibraryService = userLibraryService;
        this.moduleService = moduleService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void libraryUpserted(String storage, String name, String version) {
        libraryRemoved(storage, name, version);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void libraryRemoved(String storage, String name, String version) {
        String id = AttributeValueGeneratorLibrary.toName(storage, name, version);
        AttributeValueGeneratorLibrary library = libraries.remove(id);
        if(Objects.nonNull(library)) {
            library.cleanup();
        }
    }

    public AttributeValueGenerator find(String storageId, String libraryName, String version, String className) {
        String id = AttributeValueGeneratorLibrary.toName(storageId, libraryName, version);
        return libraries.computeIfAbsent(id, k -> load(storageId, libraryName, version)).lookup(className);
    }

    public AttributeValueGeneratorLibrary find(String storageId, String libraryName, String version) {
        String id = AttributeValueGeneratorLibrary.toName(storageId, libraryName, version);
        return libraries.computeIfAbsent(id, k -> load(storageId, libraryName, version));
    }

    public AttributeValueGeneratorLibrary load(String storageId, String libraryName, String libraryVersion) {
        UserLibraryResult ulr = userLibraryService.get(UserLibraryGetContext.builder()
            .storageId(storageId)
            .filename(libraryName)
            .version(libraryVersion)
            .withData(true)
            .build());

        if (Objects.nonNull(ulr)) {
            switch (ulr.getMimeType()) {
                case JAR_FILE:
                    return new JavaAttributeValueGeneratorLibrary(this.moduleService, ulr);
                case GROOVY_SOURCE:
                    return new GroovyAttributeValueGeneratorLibrary(ulr);
                case PYTHON_CODE:
                case PYTHON_SOURCE:
                    return new PythonAttributeValueGeneratorLibrary(ulr);
                default:
                    break;
            }
        }

        throw new PlatformFailureException("Library [{}:{}] not found.", MetaExceptionIds.EX_META_LIBRARY_NOT_FOUND,
            libraryName, libraryVersion);
    }
}
