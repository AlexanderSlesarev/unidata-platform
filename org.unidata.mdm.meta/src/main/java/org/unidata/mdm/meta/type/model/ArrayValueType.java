package org.unidata.mdm.meta.type.model;

import com.fasterxml.jackson.annotation.JsonProperty;


public enum ArrayValueType {

    @JsonProperty("Date")
    DATE(SimpleDataType.DATE),
    @JsonProperty("Time")
    TIME(SimpleDataType.TIME),
    @JsonProperty("Timestamp")
    TIMESTAMP(SimpleDataType.TIMESTAMP),
    @JsonProperty("String")
    STRING(SimpleDataType.STRING),
    @JsonProperty("Integer")
    INTEGER(SimpleDataType.INTEGER),
    @JsonProperty("Number")
    NUMBER(SimpleDataType.NUMBER);

    private final SimpleDataType value;

    ArrayValueType(SimpleDataType v) {
        value = v;
    }

    public SimpleDataType value() {
        return value;
    }

    public static ArrayValueType fromValue(SimpleDataType v) {
        for (ArrayValueType c: ArrayValueType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
