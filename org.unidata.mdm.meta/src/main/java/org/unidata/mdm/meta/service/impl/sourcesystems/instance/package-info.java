/**
 * @author Mikhail Mikhailov on Dec 1, 2020
 * Source systems instance bits.
 */
package org.unidata.mdm.meta.service.impl.sourcesystems.instance;