/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.type.model.library;

import java.util.Collection;
import java.util.Collections;
import javax.annotation.Nonnull;
import org.unidata.mdm.core.dto.UserLibraryResult;
import org.unidata.mdm.core.type.model.support.ExternalIdValueGenerator;

/**
 * @author Dmitriy Bobrov on Oct 26, 2021
 */
public class PythonEntityValueGeneratorLibrary extends EntityValueGeneratorLibrary {

    /**
     * Constructor.
     *
     * @param ulr UserLibraryResult
     */
    public PythonEntityValueGeneratorLibrary(UserLibraryResult ulr) {
        super(ulr);
        // TODO Auto-generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalIdValueGenerator lookup(String className) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Collection<String> suggest() {
        // TODO Auto-generated method stub
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanup() {
        // TODO Auto-generated method stub
    }
}
