package org.unidata.mdm.meta.type.model.entities;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

import java.io.Serializable;

import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Entity extends ComplexAttributesHolderEntity<Entity> implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 2873926497330643920L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private ValueGenerationStrategy externalIdGenerationStrategy;

    @JacksonXmlProperty(isAttribute = true)
    private boolean dashboardVisible;

    @JacksonXmlProperty(isAttribute = true)
    private String groupName;

    public ValueGenerationStrategy getExternalIdGenerationStrategy() {
        return externalIdGenerationStrategy;
    }

    public void setExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        this.externalIdGenerationStrategy = value;
    }

    public boolean isDashboardVisible() {
        return dashboardVisible;
    }

    public void setDashboardVisible(Boolean value) {
        this.dashboardVisible = value;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String value) {
        this.groupName = value;
    }

    public Entity withExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        setExternalIdGenerationStrategy(value);
        return this;
    }

    public Entity withDashboardVisible(boolean value) {
        setDashboardVisible(value);
        return this;
    }

    public Entity withGroupName(String value) {
        setGroupName(value);
        return this;
    }
}
