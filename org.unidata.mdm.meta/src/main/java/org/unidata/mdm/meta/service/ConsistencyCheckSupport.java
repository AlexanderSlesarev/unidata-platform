/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.core.context.FetchLargeObjectContext;
import org.unidata.mdm.core.service.LargeObjectsService;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.data.ArrayAttribute;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.Attribute.AttributeType;
import org.unidata.mdm.core.type.data.CodeAttribute;
import org.unidata.mdm.core.type.data.ComplexAttribute;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.MeasuredValue;
import org.unidata.mdm.core.type.data.SimpleAttribute;
import org.unidata.mdm.core.type.data.impl.AbstractLargeValue;
import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.AttributeElement.AttributeValueType;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.model.EnumerationElement;
import org.unidata.mdm.core.type.model.MeasurementCategoryElement;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.context.ConsistencyCheckContext;
import org.unidata.mdm.meta.module.MetaModule;
import org.unidata.mdm.system.exception.ValidationResult;

/**
 * @author Mikhail Mikhailov on Nov 5, 2019
 */
public interface ConsistencyCheckSupport {
    /**
     * Invalid number of DRs in a complex attribute.
     */
    String EX_CHECK_INCORRECT_QUANTITY_OF_COMPLEX_ATTRIBUTES_IN_RANGE = MetaModule.MODULE_ID + ".check.incorrect.quantity.complex.attrs.range";
    /**
     * Attribute is missing.
     */
    String EX_CHECK_MISSING_ATTRIBUTE = MetaModule.MODULE_ID + ".check.missing.attribute";
    /**
     * Required attributes missing.
     */
    String EX_CHECK_REQUIRED_ATTRS_NOT_PRESENT = MetaModule.MODULE_ID + ".check.required.attributes.not.present";
    /**
     * Wrong attribute type.
     */
    String EX_CHECK_WRONG_ATTRIBUTE_TYPE = MetaModule.MODULE_ID + ".check.wrong.attribute.type";
    /**
     * Enum value not present in model.
     */
    String EX_CHECK_ENUM_ATTRIBUTE_INCORRECT = MetaModule.MODULE_ID + ".check.attribute.enum.value.incorrect";
    /**
     * Measured category mismatch.
     */
    String EX_CHECK_MEASURED_CATEGORY_MISMATCH = MetaModule.MODULE_ID + ".check.measured.category.not.present";
    /**
     * Measured unit mismatch.
     */
    String EX_CHECK_MEASURED_UNIT_MISMATCH = MetaModule.MODULE_ID + ".check.measured.unit.not.present";
    /**
     * Attribute has wrong value type.
     */
    String EX_CHECK_WRONG_VALUE_TYPE = MetaModule.MODULE_ID + ".check.wrong.value.type";
    /**
     * Attribute (lookup link) has wrong value type.
     */
    String EX_CHECK_LINK_WRONG_VALUE_TYPE = MetaModule.MODULE_ID + ".check.link.wrong.value.type";
    /**
     * Not nullable attribute has no value.
     */
    String EX_CHECK_REQUIRED_ATTRIBUTE_NO_VALUE = MetaModule.MODULE_ID + ".required.attribute.no.value";
    /**
     * Dictionary values mismatch.
     */
    String EX_CHECK_DICTIONARY_VALUES_MISMATCH = MetaModule.MODULE_ID + ".check.dictionary.values.mismatch";
    /**
     * LOB value missing.
     */
    String EX_CHECK_LARGE_VALUE_UNAVAILABLE = MetaModule.MODULE_ID + ".check.large.value.unavailable";
    /**
     * MetaModel service.
     */
    MetaModelService metaModelService();
    /**
     * LOB component.
     */
    LargeObjectsService lobComponent();
    /**
     * A very simple breadth first traversal of the DR tree to collect validation errors.
     * New elements are added to queue by customizations.
     *
     * @param data the top level record
     * @param el the element
     */
    default void check(@Nonnull ConsistencyCheckContext check) {

        Pair<DataRecord, EntityElement> current;
        while (!check.finished()) {

            current = check.poll();

            final DataRecord value = current.getLeft();
            final EntityElement model = current.getRight();

            check(check, value, model);
        }
    }
    /**
     * Runs check for a record and element.
     *
     * @param check the context
     * @param data the value
     * @param el the element
     */
    default void check(ConsistencyCheckContext check, final DataRecord data, final EntityElement el) {

        // We copy the map to be able to remove attributes
        // and check the remaining after this.
        final Map<String, AttributeElement> attrs = el.getAttributes().entrySet().stream()
                .filter(attr -> attr.getValue().getLevel() == 0)
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

        for (Attribute attr : data.getAttributeValues()) {

            AttributeElement ael = attrs.remove(attr.getName());
            if (ael == null) {

                check.append(new ValidationResult(
                        "Attribute {} supplied for upsert is missing in the model.",
                        EX_CHECK_MISSING_ATTRIBUTE, attr.toLocalPath()));

                continue;
            }

            checkAttribute(check, attr, ael);
        }

        List<String> missing = attrs.values().stream()
                .filter(this:: checkAttributeRequired)
                .map(AttributeElement::getDisplayName)
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(missing)) {

            check.append(new ValidationResult(
                    "Required attributes {} from {} are not present in the record{}.",
                    EX_CHECK_REQUIRED_ATTRS_NOT_PRESENT, missing, el.getName(), data.isTopLevel() ? StringUtils.EMPTY : " " + data.toLocalPath()));
        }
    }
    /**
     * Checks particular attribute.
     *
     * @param check the context
     * @param attr the attribute
     * @param el the element
     */
    default void checkAttribute(ConsistencyCheckContext check, Attribute attr, AttributeElement el) {

        // 1. Check attribute type. If this returns true all the other checks make no sence.
        if (!checkAttributeType(check, attr, el)) {
            return;
        }

        // 2. Check attribute according to its type (the above call returned true)
        switch (attr.getAttributeType()) {
        case ARRAY:
            checkArrayAttribute(check, attr.narrow(), el);
            break;
        case CODE:
            checkCodeAttribute(check, attr.narrow(), el);
            break;
        case COMPLEX:
            checkComplexAttribute(check, attr.narrow(), el);
            break;
        case SIMPLE:
            checkSimpleAttribute(check, attr.narrow(), el);
            break;
        default:
            break;
        }
    }
    /**
     * Checks attribute type.
     *
     * @param check the state
     * @param attr the attribute
     * @param el the element
     * @return true if the attribute has proper type, false otherwise
     */
    default boolean checkAttributeType(ConsistencyCheckContext check, Attribute attr, AttributeElement el) {

        boolean wrongType =
                (el.isSimple() && attr.getAttributeType() != Attribute.AttributeType.SIMPLE)
             || (el.isCode() && attr.getAttributeType() != Attribute.AttributeType.CODE)
             || (el.isArray() && attr.getAttributeType() != Attribute.AttributeType.ARRAY)
             || (el.isComplex() && attr.getAttributeType() != Attribute.AttributeType.COMPLEX);

        if (wrongType) {

            check.append(new ValidationResult(
                    "Attribute {} has a wrong type {} compared to that, defined in the model ({} is expected).",
                    EX_CHECK_WRONG_ATTRIBUTE_TYPE,
                    attr.toLocalPath(), attr.getAttributeType().name(), el.getTypeName()));
        }

        return !wrongType;
    }
    /**
     * Check expected value and type attribute.
     *
     * @param attr the attribute
     * @param el attribute definition
     * @return true, if the attribute has correct value type
     */
    default boolean checkValueType(ConsistencyCheckContext check, Attribute attr, AttributeElement el) {

        boolean wrongType = false;
        String currentTypeName = null;
        String expectedTypeName = null;
        switch (attr.getAttributeType()) {
        case ARRAY:
            ArrayAttribute.ArrayDataType arrayType = ArrayAttribute.ArrayDataType.fromModelType(el.getValueType());
            wrongType = attr.<ArrayAttribute<?>>narrow().getDataType() != arrayType;
            currentTypeName = attr.<ArrayAttribute<?>>narrow().getDataType().name();
            expectedTypeName = arrayType.name();
            break;
        case CODE:
            CodeAttribute.CodeDataType codeType = CodeAttribute.CodeDataType.fromModelType(el.getValueType());
            wrongType = attr.<CodeAttribute<?>>narrow().getDataType() != codeType;
            currentTypeName = attr.<CodeAttribute<?>>narrow().getDataType().name();
            expectedTypeName = codeType.name();
            break;
        case SIMPLE:
            SimpleAttribute.SimpleDataType simpleType = SimpleAttribute.SimpleDataType.fromModelType(el.getValueType());
            wrongType = attr.<SimpleAttribute<?>>narrow().getDataType() != simpleType;
            currentTypeName = attr.<SimpleAttribute<?>>narrow().getDataType().name();
            expectedTypeName = simpleType.name();
            break;
        default:
            break;
        }

        if (wrongType) {

            if (el.isLookupLink()) {

                check.append(new ValidationResult(
                        "Attribute {} referencing {} has value type {} while type {} is expected from the model.",
                        EX_CHECK_LINK_WRONG_VALUE_TYPE,
                        attr.toLocalPath(), el.getLookupLink().getLookupLinkName(), currentTypeName, expectedTypeName));
            } else {

                check.append(new ValidationResult(
                        "Array attribute {} has value type {} while type {} is expected from the model.",
                        EX_CHECK_WRONG_VALUE_TYPE,
                        attr.toLocalPath(), currentTypeName, expectedTypeName));
            }
        }

        return !wrongType;
    }
    /**
     * Checks whether the attribute has value.
     * Returns true, if value is present, false otherwise.
     * Adds validation message if value is missing while being required.
     *
     * @param check the check context
     * @param attr the attribute
     * @param el the element
     * @return true, if value is present, false otherwise
     */
    default boolean checkValuePresence(ConsistencyCheckContext check, Attribute attr, AttributeElement el) {

        boolean empty = false;
        if (attr.getAttributeType() == Attribute.AttributeType.ARRAY) {
            empty = ((ArrayAttribute<?>) attr).isEmpty();
        } else if (attr.getAttributeType() == Attribute.AttributeType.CODE) {
            empty = ((CodeAttribute<?>) attr).isEmpty();
        } else if (attr.getAttributeType() == Attribute.AttributeType.SIMPLE) {
            empty = ((SimpleAttribute<?>) attr).isEmpty();
        } else if (attr.getAttributeType() == AttributeType.COMPLEX) {
            empty = ((ComplexAttribute) attr).isEmpty();
        }

        if (empty && (!el.isNullable() || el.isCode() || el.isCodeAlternative())) {

            check.append(new ValidationResult(
                    "Attribute {} is empty while value is required by the model.",
                    EX_CHECK_REQUIRED_ATTRIBUTE_NO_VALUE,
                    attr.toLocalPath()));
        }

        return !empty;
    }
    /**
     * Checks array attribute.
     *
     * @param check the check object
     * @param attr the array attribute to check
     * @param el definition of the attribute
     */
    default void checkArrayAttribute(ConsistencyCheckContext check, ArrayAttribute<?> attr, AttributeElement el) {

        if (!checkValueType(check, attr, el) || !checkValuePresence(check, attr, el)) {
            return;
        }

        if (el.isDictionary()) {
            checkDictionaryAttribute(check, attr, el);
        }
    }
    /**
     * Checks code attribute.
     *
     * @param check the check object
     * @param attr the code attribute to check
     * @param el definition of the attribute
     */
    default void checkCodeAttribute(ConsistencyCheckContext check, CodeAttribute<?> attr, AttributeElement el) {

        if (!checkValueType(check, attr, el)) {
            return;
        }

        checkValuePresence(check, attr, el);
    }
    /**
     * Checks simple attribute.
     *
     * @param check the check object
     * @param attr the simple attribute to check
     * @param el definition of the attribute
     */
    default void checkSimpleAttribute(ConsistencyCheckContext check, SimpleAttribute<?> attr, AttributeElement el) {

        // Stop on problems with value, since further check make no sence
        if (!checkValueType(check, attr, el) || !checkValuePresence(check, attr, el)) {
            return;
        }

        if (el.isEnumValue()) {
            checkEnumAttribute(check, attr, el);
        } else if (el.isMeasured()) {
            checkMeasuredAttribute(check, attr.narrow(), el);
        } else if (el.isBlob() || el.isClob()) {
            checkLargeObjectAttribute(check, attr.narrow(), el);
        } else if (el.isDictionary()) {
            checkDictionaryAttribute(check, attr, el);
        }
    }
    /**
     * Checks complex attribute.
     *
     * @param check the check object
     * @param attr the complex attribute to check
     * @param el definition of the attribute
     */
    default void checkComplexAttribute(ConsistencyCheckContext check, ComplexAttribute attr, @Nonnull AttributeElement el) {

        int count = attr.size();
        int minCount = el.getComplex().getMinCount();
        int maxCount = el.getComplex().getMaxCount();
        if ((count - minCount < 0) || (count - maxCount > 0)) {

            check.append(new ValidationResult(
                    "Quantity of complex attribute {} should be in range {} - {} but current value is {}.",
                    EX_CHECK_INCORRECT_QUANTITY_OF_COMPLEX_ATTRIBUTES_IN_RANGE,
                    el.getName(), minCount, maxCount, count));
        }
    }
    /**
     * Checks enum attribute.
     *
     * @param check the context
     * @param attr the attribute
     * @param el the element
     */
    default void checkEnumAttribute(ConsistencyCheckContext check, SimpleAttribute<?> attr, AttributeElement el) {

        String enumId = el.getEnumName();
        if (StringUtils.isBlank(enumId)) {
            return;
        }

        EnumerationElement enumeration = metaModelService()
                .instance(Descriptors.ENUMERATIONS)
                .getEnumeration(enumId);

        String value = attr.castValue();
        if (Objects.isNull(enumeration) || !enumeration.valueExists(value)) {

            check.append(new ValidationResult(
                    "Attribute {} is of type Enumeration and has value {} not found in model (Enumeration {}).",
                    EX_CHECK_ENUM_ATTRIBUTE_INCORRECT,
                    attr.toLocalPath(), value, enumId));
        }
    }
    /**
     * Checks measured attribute.
     *
     * @param check the context
     * @param attr the attribute
     * @param el the element
     */
    default void checkMeasuredAttribute(ConsistencyCheckContext check, SimpleAttribute<MeasuredValue> attr, AttributeElement el) {

        if (!attr.getValue().hasMetadata()) {
            return;
        }

        MeasurementCategoryElement mc = metaModelService()
                .instance(Descriptors.MEASUREMENT_UNITS)
                .getCategory(el.getMeasured().getCategoryId());

        if (mc == null) {
            return;
        }

        if (!attr.getValue().getCategoryId().equals(mc.getName())) {

            check.append(new ValidationResult(
                    "Attribute {} is of type Measured and has value {} for Unit Category, which doesn't match with the model (Category expected {}).",
                    EX_CHECK_MEASURED_CATEGORY_MISMATCH,
                    attr.toLocalPath(), attr.getValue().getCategoryId(), el.getMeasured().getCategoryId()));
        }

        if (!mc.exists(attr.getValue().getUnitId())) {

            check.append(new ValidationResult(
                    "Attribute {} is of type Measured and has value {} for Unit ID, which is not found in the model (Category {}).",
                    EX_CHECK_MEASURED_UNIT_MISMATCH,
                    attr.toLocalPath(), attr.getValue().getUnitId(), el.getMeasured().getCategoryId()));
        }
    }
    /**
     * Checks LOB value.
     *
     * @param check the context
     * @param attr the attribute
     * @param el the element
     */
    default void checkLargeObjectAttribute(ConsistencyCheckContext check, SimpleAttribute<AbstractLargeValue<?>> attr, AttributeElement el) {

        boolean isExist = lobComponent().checkExistLargeObject(FetchLargeObjectContext.builder()
                .largeObjectId(attr.getValue().getId())
                .binary(el.getValueType() == AttributeValueType.BLOB)
                .build());

        if (!isExist) {

            check.append(new ValidationResult(
                    "Large Object Value for attribute {} not found.",
                    EX_CHECK_LARGE_VALUE_UNAVAILABLE,
                    attr.toLocalPath()));
        }
    }
    /**
     * Checks dictionary values.
     *
     * @param check the context
     * @param attr the attribute
     * @param el the element
     */
    default void checkDictionaryAttribute(ConsistencyCheckContext check, Attribute attr, AttributeElement el) {

        boolean valuesMatch = true;
        if (el.isArray()) {
            valuesMatch = el
                    .getDictionary()
                    .getDictionaryValues()
                    .containsAll(attr.<ArrayAttribute<String>>narrow().getValues());
        } else if (el.isSimple()) {
            valuesMatch = el
                    .getDictionary()
                    .getDictionaryValues()
                    .contains(attr.<SimpleAttribute<String>>narrow().getValue());
        }

        if (!valuesMatch) {

            check.append(new ValidationResult(
                    "Attribute {} is of dictionary type and has values not found in dictionary.",
                    EX_CHECK_DICTIONARY_VALUES_MISMATCH,
                    attr.toLocalPath()));
        }
    }
    /**
     * Tells whether the attribute is required (for plin attributes).
     *
     * @param el the element
     * @return true, if the attribute is a plane one and is not nullable
     */
    default boolean checkAttributeRequired(AttributeElement el) {

        if (el.isSimple() || el.isCode() || el.isArray()) {
            return !el.isNullable();
        }

        return false;
    }
}
