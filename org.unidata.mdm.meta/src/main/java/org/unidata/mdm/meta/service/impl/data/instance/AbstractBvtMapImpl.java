/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.impl.data.instance;

import java.util.List;
import java.util.Map;

import org.unidata.mdm.core.type.model.BvtMapElement;
import org.unidata.mdm.meta.type.model.entities.AbstractEntity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov
 * BVT map holder.
 */
public abstract class AbstractBvtMapImpl<X extends AbstractEntity<?>> extends AbstractEntityImpl<X>
    implements BvtMapElement {
    /**
     * BVT attributes map.
     */
    protected final Map<String, Map<String, Integer>> bvtMap;
    /**
     * Constructor.
     * @param e the top level entity
     * @param nested nested types
     * @param map the global source systems descending map
     */
    protected AbstractBvtMapImpl(X e, List<NestedEntity> nested, Map<String, Integer> map) {
        super(e, nested);
        this.bvtMap = ModelUtils.createBvtMap(e, map, getAttributes());
    }
    /**
     * @return the bvtMap
     */
    @Override
    public Map<String, Map<String, Integer>> getBvtMap() {
        return bvtMap;
    }
}
