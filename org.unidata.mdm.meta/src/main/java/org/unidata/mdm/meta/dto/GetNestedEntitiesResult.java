package org.unidata.mdm.meta.dto;

import org.unidata.mdm.meta.type.model.entities.NestedEntity;

/**
 * @author Mikhail Mikhailov on Oct 22, 2020
 * Single netsed entity result.
 */
public class GetNestedEntitiesResult {
    /**
     * The nested entity.
     */
    private NestedEntity nested;
    /**
     * Constructor.
     */
    public GetNestedEntitiesResult() {
        super();
    }
    /**
     * Constructor.
     */
    public GetNestedEntitiesResult(NestedEntity nested) {
        super();
        this.nested = nested;
    }
    /**
     * @return the nested
     */
    public NestedEntity getNested() {
        return nested;
    }
    /**
     * @param nested the nested to set
     */
    public void setNested(NestedEntity nested) {
        this.nested = nested;
    }
}
