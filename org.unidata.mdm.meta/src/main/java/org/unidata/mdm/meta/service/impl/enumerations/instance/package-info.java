/**
 * @author Mikhail Mikhailov on Dec 1, 2020
 * Enumeration instance bits.
 */
package org.unidata.mdm.meta.service.impl.enumerations.instance;