-- Rename according to new schema
alter table meta_storage rename to storage;
alter table storage rename column name to description;
alter table storage alter column id type varchar(128);

-- records and relations
create table data (
    storage_id varchar(128) not null,
    revision integer not null,
    description text,
    operation_id varchar(128),
    content bytea not null,
    created_by varchar(256) not null,
    create_date timestamptz not null default current_timestamp,
    constraint pk_models_storage_id_revision primary key(storage_id, revision),
    constraint fk_models_storage_id foreign key (storage_id) references storage (id) on delete cascade on update cascade
);

-- enumerations
create table enumerations (
    storage_id varchar(128) not null,
    revision integer not null,
    description text,
    operation_id varchar(128),
    content bytea not null,
    created_by varchar(256) not null,
    create_date timestamptz not null default current_timestamp,
    constraint pk_enumerations_storage_id_revision primary key(storage_id, revision),
    constraint fk_enumerations_storage_id foreign key (storage_id) references storage (id) on delete cascade on update cascade
);

-- source systems
create table source_systems (
    storage_id varchar(128) not null,
    revision integer not null,
    description text,
    operation_id varchar(128),
    content bytea not null,
    created_by varchar(256) not null,
    create_date timestamptz not null default current_timestamp,
    constraint pk_source_systems_storage_id_revision primary key(storage_id, revision),
    constraint fk_source_systems_storage_id foreign key (storage_id) references storage (id) on delete cascade on update cascade
);

-- measurement units
drop table if exists measurement_units;
create table measurement_units (
    storage_id varchar(128) not null,
    revision integer not null,
    description text,
    operation_id varchar(128),
    content bytea not null,
    created_by varchar(256) not null,
    create_date timestamptz not null default current_timestamp,
    constraint pk_measurement_units_storage_id_revision primary key(storage_id, revision),
    constraint fk_measurement_units_storage_id foreign key (storage_id) references storage (id) on delete cascade on update cascade
);
