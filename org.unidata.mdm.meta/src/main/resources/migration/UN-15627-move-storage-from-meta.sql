-- Drop FK to storage 
alter table data drop constraint if exists fk_models_storage_id;
alter table enumerations drop constraint if exists fk_enumerations_storage_id;
alter table source_systems drop constraint if exists fk_source_systems_storage_id;
alter table measurement_units drop constraint if exists fk_measurement_units_storage_id;

-- Drop old unused sequences
drop sequence if exists meta_draft_id_seq cascade;
drop sequence if exists meta_model_revision_seq cascade;
drop sequence if exists meta_process_assignment_id_seq cascade;
drop sequence if exists meta_storage_revision_seq cascade;
drop sequence if exists meta_ui_revision_seq cascade;

-- Drop old tables
drop table if exists measurement_values;
drop table if exists meta_draft;
drop table if exists meta_model;
drop table if exists storage;
