/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.search.ro;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.JsonNode;

public class SearchComplexRO extends SearchRequestRO {
    /**
     * Search fields.
     */
    private List<String> searchFields;
    /**
     * Search text.
     */
    private String text;
    /**
     * Fields.
     */
    private List<SearchFormFieldRO> formFields;
    /**
     * Fields groups
     */
    private List<SearchFormFieldsGroupRO> formGroups;
    /**
     * supplementary Requests
     */
    private List<SearchComplexRO> supplementaryRequests = Collections.emptyList();

    /**
     * Is sayt search.
     */
    private boolean sayt = false;

    private boolean onlyQuery = false;

    private Map<String, JsonNode> any = new HashMap<>();

    /**
     * Constructor.
     */
    public SearchComplexRO() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @JsonAnyGetter
    @Override
    public Map<String, JsonNode> getAny() {
        return any;
    }

    @JsonAnySetter
    public void setAny(String name, JsonNode value) {
        any.put(name, value);
    }

    /**
     * @return the searchFields
     */
    public List<String> getSearchFields() {
        return searchFields;
    }

    /**
     * @param searchFields the searchFields to set
     */
    public void setSearchFields(List<String> searchFields) {
        this.searchFields = searchFields;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the formFields
     */
    public List<SearchFormFieldRO> getFormFields() {
        return formFields;
    }

    /**
     * @param fields the formFields to set
     */
    public void setFormFields(List<SearchFormFieldRO> fields) {
        this.formFields = fields;
    }

    /**
     * @return - supplementary Requests
     */
    public List<SearchComplexRO> getSupplementaryRequests() {
        return supplementaryRequests;
    }

    /**
     * @param supplementaryRequests - supplementary Requests
     */
    public void setSupplementaryRequests(List<SearchComplexRO> supplementaryRequests) {
        this.supplementaryRequests = supplementaryRequests;
    }


    /**
     * @return is sayt
     */
    public boolean isSayt() {
        return sayt;
    }

    /**
     * @param sayt sayt
     */
    public void setSayt(boolean sayt) {
        this.sayt = sayt;
    }

    /**
     * Form fields array.
     */
    public List<SearchFormFieldsGroupRO> getFormGroups() {
        return formGroups;
    }

    public void setFormGroups(List<SearchFormFieldsGroupRO> formGroups) {
        this.formGroups = formGroups;
    }

    public boolean isOnlyQuery() {
        return onlyQuery;
    }

    public void setOnlyQuery(boolean onlyQuery) {
        this.onlyQuery = onlyQuery;
    }
}
