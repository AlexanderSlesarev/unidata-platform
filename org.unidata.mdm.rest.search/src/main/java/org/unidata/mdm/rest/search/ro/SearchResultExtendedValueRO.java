/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.search.ro;

/**
 * Extended value for search result.
 * Can contains special name for display and other additional information.
 */
public class SearchResultExtendedValueRO {
    /**
     * Original value
     */
    private Object value;
    /**
     * Display value
     */
    private String displayValue;
    /**
     * Linked etalon id
     */
    private String linkedEtalonId;

    public SearchResultExtendedValueRO(Object value, String displayValue, String linkedEtalonId) {
        this.value = value;
        this.displayValue = displayValue;
        this.linkedEtalonId = linkedEtalonId;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getLinkedEtalonId() {
        return linkedEtalonId;
    }

    public void setLinkedEtalonId(String linkedEtalonId) {
        this.linkedEtalonId = linkedEtalonId;
    }
}