/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro.references;

import javax.annotation.Nonnull;

/**
 * Attribute registry key
 */
public class AttributeReferenceRO implements UniqueReferenceRO {

    public static final ReferenceType TYPE = new ReferenceType("ATTRIBUTE", "атрибут");
    /**
     * Attribute name
     */
    @Nonnull
    private final String fullAttributeName;

    /**
     * Entity name
     */
    @Nonnull
    private final String entityName;

    /**
     * @param fullAttributeName - full attribute name
     * @param entityName        - entity name
     */
    public AttributeReferenceRO(@Nonnull String fullAttributeName, @Nonnull String entityName) {
        this.entityName = entityName;
        this.fullAttributeName = fullAttributeName;
    }

    @Override
    public ReferenceType keyType() {
        return TYPE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AttributeReferenceRO)) return false;

        AttributeReferenceRO that = (AttributeReferenceRO) o;

        if (!fullAttributeName.equals(that.fullAttributeName)) return false;
        return entityName.equals(that.entityName);

    }

    @Override
    public int hashCode() {
        int result = fullAttributeName.hashCode();
        result = 31 * result + entityName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "fullAttributeName='" + fullAttributeName + '\'' +
                ", entityName='" + entityName + '\'' +
                '}';
    }

    @Nonnull
    public String getFullAttributeName() {
        return fullAttributeName;
    }

    @Nonnull
    public String getEntityName() {
        return entityName;
    }
}
