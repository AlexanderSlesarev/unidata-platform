package org.unidata.mdm.rest.meta.ro;

import java.util.ArrayList;
import java.util.List;

import org.unidata.mdm.meta.type.input.meta.MetaEdge;
import org.unidata.mdm.meta.type.input.meta.MetaVertex;


/**
 * The Class MetaEdgeROToDTOConverter.
 * 
 * @author ilya.bykov
 */
public class MetaEdgeROToDTOConverter {

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the list
	 */
	public static List<MetaEdge<MetaVertex>> convert(List<MetaEdgeRO> source) {
		if (source == null) {
			return null;
		}
		List<MetaEdge<MetaVertex>> target = new ArrayList<>();
		for (MetaEdgeRO s : source) {
			target.add(convert(s));
		}
		return target;
	}

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the meta edge
	 */
	public static MetaEdge<MetaVertex> convert(MetaEdgeRO source) {
		if (source == null) {
			return null;
		}
		return new MetaEdge<>(
				new MetaVertex(source.getFrom().getId(), MetaTypeROToDTOConverter.convert(source.getFrom().getType())),
				new MetaVertex(source.getTo().getId(), MetaTypeROToDTOConverter.convert(source.getTo().getType())));

	}
}
