package org.unidata.mdm.rest.meta.ro;


import org.unidata.mdm.meta.type.input.meta.MetaAction;

/**
 * The Class MetaActionDTOToROConverter.
 * @author ilya.bykov
 */
public class MetaActionDTOToROConverter {
	
	/**
	 * Convert.
	 *
	 * @param source the source
	 * @return the meta action RO
	 */
	public static MetaActionRO convert(MetaAction source) {
		if (source == null) {
			return null;
		}
		return MetaActionRO.valueOf(source.name());
	}
}
