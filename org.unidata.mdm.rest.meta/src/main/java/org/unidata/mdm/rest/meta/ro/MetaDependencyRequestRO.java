/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The Class MetaDependencyRequestRO.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetaDependencyRequestRO {
	
	/** The storage id. */
	private String storageId;
	
	/** The for types. */
	private List<MetaTypeRO> forTypes;
	
	/** The skip types. */
	private List<MetaTypeRO> skipTypes;

	/**
	 * Gets the storage id.
	 *
	 * @return the storage id
	 */
	public String getStorageId() {
		return storageId;
	}

	/**
	 * Sets the storage id.
	 *
	 * @param storageId the new storage id
	 */
	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}

	/**
	 * Gets the for types.
	 *
	 * @return the for types
	 */
	public List<MetaTypeRO> getForTypes() {
		return forTypes;
	}

	/**
	 * Sets the for types.
	 *
	 * @param forTypes the new for types
	 */
	public void setForTypes(List<MetaTypeRO> forTypes) {
		this.forTypes = forTypes;
	}

	/**
	 * Gets the skip types.
	 *
	 * @return the skip types
	 */
	public List<MetaTypeRO> getSkipTypes() {
		return skipTypes;
	}

	/**
	 * Sets the skip types.
	 *
	 * @param skipTypes the new skip types
	 */
	public void setSkipTypes(List<MetaTypeRO> skipTypes) {
		this.skipTypes = skipTypes;
	}
}
