package org.unidata.mdm.rest.meta.ro;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.unidata.mdm.meta.type.input.meta.MetaEdge;
import org.unidata.mdm.meta.type.input.meta.MetaVertex;

/**
 * The Class MetaEdgeDTOToROConverter.
 *
 * @author ilya.bykov
 */
public class MetaEdgeDTOToROConverter {

    /**
     * Convert.
     *
     * @param source the source
     * @return the list
     */
    public static List<MetaEdgeRO> convert(Collection<MetaEdge<MetaVertex>> source) {

        List<MetaEdgeRO> target = new ArrayList<>();
        for (MetaEdge<MetaVertex> s : source) {
            target.add(convert(s));
        }
        return target;

    }

    /**
     * Convert.
     *
     * @param source the source
     * @return the meta edge RO
     */
    public static MetaEdgeRO convert(MetaEdge<MetaVertex> source) {
        return new MetaEdgeRO(
                MetaVertexDTOToROConverter.convert(source.getFrom()),
                MetaVertexDTOToROConverter.convert(source.getTo())
        );
    }
}
