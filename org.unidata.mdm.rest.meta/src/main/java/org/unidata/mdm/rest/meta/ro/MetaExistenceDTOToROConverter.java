package org.unidata.mdm.rest.meta.ro;


import org.unidata.mdm.meta.type.input.meta.MetaExistence;

/**
 * The Class MetaStatusDTOToROConverter.
 * @author ilya.bykov
 */
public class MetaExistenceDTOToROConverter {

	private MetaExistenceDTOToROConverter() {
	}

	/**
	 * Convert.
	 *
	 * @param source the source
	 * @return the meta status RO
	 */
	public static MetaExistenceRO convert(MetaExistence source) {
		return MetaExistenceRO.valueOf(source.name());
	}
}
