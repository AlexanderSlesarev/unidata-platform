/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro.references;

import javax.annotation.Nonnull;

/**
 * Unique key of lookup entity help identifying lookup entity
 */
public class LookupReferenceRO implements UniqueReferenceRO {
    /**
     * The type.
     */
    public static final ReferenceType TYPE = new ReferenceType("LOOKUP_ENTITY", "справочник");
    /**
     * lookup entity name
     */
    @Nonnull
    private final String entityName;
    /**
     * constructor
     *
     * @param entityName - lookup entity name
     */
    public LookupReferenceRO(@Nonnull String entityName) {
        this.entityName = entityName;
    }
    /**
     * @return entity name
     */
    @Nonnull
    public String getEntityName() {
        return entityName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LookupReferenceRO)) return false;

        LookupReferenceRO that = (LookupReferenceRO) o;

        return entityName.equals(that.entityName);

    }

    @Override
    public int hashCode() {
        return entityName.hashCode();
    }

    /**
     * @return type of key
     */
    @Override
    public ReferenceType keyType() {
        return TYPE;
    }

    @Override
    public String toString() {
        return "{" +
                "entityName='" + entityName + '\'' +
                '}';
    }
}
