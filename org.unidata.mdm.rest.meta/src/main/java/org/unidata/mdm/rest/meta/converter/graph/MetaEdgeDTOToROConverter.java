/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.unidata.mdm.meta.type.input.meta.MetaEdge;
import org.unidata.mdm.meta.type.input.meta.MetaVertex;
import org.unidata.mdm.rest.meta.ro.MetaEdgeRO;

/**
 * The Class MetaEdgeDTOToROConverter.
 * 
 * @author ilya.bykov
 */
public class MetaEdgeDTOToROConverter {

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the list
	 */
	public static List<MetaEdgeRO> convert(Collection<MetaEdge<MetaVertex>> source) {
		if (source == null) {
			return null;
		}

		List<MetaEdgeRO> target = new ArrayList<>();
		for (MetaEdge<MetaVertex> s : source) {
			target.add(convert(s));
		}
		return target;

	}

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the meta edge RO
	 */
	public static MetaEdgeRO convert(MetaEdge<MetaVertex> source) {
		if (source == null) {
			return null;
		}
		return new MetaEdgeRO(
				MetaVertexDTOToROConverter.convert(source.getFrom()),
				MetaVertexDTOToROConverter.convert(source.getTo())
		);
	}
}
