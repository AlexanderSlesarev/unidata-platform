/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.core.context.AbstractModelChangeContext;
import org.unidata.mdm.core.service.segments.ModelUpsertStartExecutor;
import org.unidata.mdm.draft.context.DraftDataContext;
import org.unidata.mdm.matching.core.configuration.MatchingModelIds;
import org.unidata.mdm.matching.core.type.model.source.algorithm.MatchingAlgorithmSource;
import org.unidata.mdm.matching.core.type.model.source.assignment.NamespaceAssignmentSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSetSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableSource;
import org.unidata.mdm.system.context.DraftAwareContext;
import org.unidata.mdm.system.context.StorageSpecificContext;
import org.unidata.mdm.system.type.pipeline.PipelineInput;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 14.06.2021
 */
public class MatchingModelUpsertContext extends AbstractModelChangeContext
        implements DraftAwareContext, DraftDataContext, PipelineInput, StorageSpecificContext {

    /**
     * SVUID.
     */
    private static final long serialVersionUID = 3943588370756959978L;

    /**
     * Algorithms update.
     */
    private final List<MatchingAlgorithmSource> algorithmsUpdate;
    /**
     * Algorithms deletes.
     */
    private final List<String> algorithmsDelete;
    /**
     * Marching tables update.
     */
    private final List<MatchingTableSource> matchingTablesUpdate;
    /**
     * Matching tables delete.
     */
    private final List<String> matchingTablesDelete;
    /**
     * Rules update.
     */
    private final List<MatchingRuleSource> rulesUpdate;
    /**
     * Rules deletes.
     */
    private final List<String> rulesDelete;
    /**
     * Rule sets update.
     */
    private final List<MatchingRuleSetSource> setsUpdate;
    /**
     * Rule sets deletes.
     */
    private final List<String> setsDelete;
    /**
     * Assignments update.
     */
    private final List<NamespaceAssignmentSource> assignmentsUpdate;
    /**
     * Assignments deletes.
     */
    private final List<String> assignmentsDelete;
    /**
     * A possibly set draft id.
     */
    private final Long draftId;
    /**
     * A possibly set parent draft id.
     */
    private final Long parentDraftId;

    /**
     * Constructor.
     */
    private MatchingModelUpsertContext(MatchingModelUpsertContextBuilder b) {
        super(b);

        this.algorithmsDelete = Objects.isNull(b.algorithmsDelete) ? Collections.emptyList() : b.algorithmsDelete;
        this.algorithmsUpdate = Objects.isNull(b.algorithmsUpdate) ? Collections.emptyList() : b.algorithmsUpdate;
        this.matchingTablesDelete = Objects.isNull(b.matchingTablesDelete) ? Collections.emptyList() :
                b.matchingTablesDelete;
        this.matchingTablesUpdate = Objects.isNull(b.matchingTablesUpdate) ? Collections.emptyList() :
                b.matchingTablesUpdate;
        this.rulesUpdate = Objects.isNull(b.rulesUpdate) ? Collections.emptyList() : b.rulesUpdate;
        this.rulesDelete = Objects.isNull(b.rulesDelete) ? Collections.emptyList() : b.rulesDelete;
        this.setsUpdate = Objects.isNull(b.setsUpdate) ? Collections.emptyList() : b.setsUpdate;
        this.setsDelete = Objects.isNull(b.setsDelete) ? Collections.emptyList() : b.setsDelete;
        this.assignmentsUpdate = Objects.isNull(b.assignmentsUpdate) ? Collections.emptyList() : b.assignmentsUpdate;
        this.assignmentsDelete = Objects.isNull(b.assignmentsDelete) ? Collections.emptyList() : b.assignmentsDelete;
        this.draftId = b.draftId;
        this.parentDraftId = b.parentDraftId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return MatchingModelIds.MATCHING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return MatchingModelIds.DEFAULT_MODEL_INSTANCE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return ModelUpsertStartExecutor.SEGMENT_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getDraftId() {
        return draftId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getParentDraftId() {
        return parentDraftId;
    }

    /**
     * @return the algorithms update
     */
    public List<MatchingAlgorithmSource> getAlgorithmsUpdate() {
        return algorithmsUpdate;
    }

    /**
     * @return the algorithms delete
     */
    public List<String> getAlgorithmsDelete() {
        return algorithmsDelete;
    }

    /**
     * @return the matching tables update
     */
    public List<MatchingTableSource> getMatchingTablesUpdate() {
        return matchingTablesUpdate;
    }

    /**
     * @return the matching tables delete
     */
    public List<String> getMatchingTablesDelete() {
        return matchingTablesDelete;
    }

    /**
     * @return the rules update
     */
    public List<MatchingRuleSource> getRulesUpdate() {
        return rulesUpdate;
    }

    /**
     * @return the rules delete
     */
    public List<String> getRulesDelete() {
        return rulesDelete;
    }

    /**
     * @return the sets update
     */
    public List<MatchingRuleSetSource> getSetsUpdate() {
        return setsUpdate;
    }

    /**
     * @return the sets delete
     */
    public List<String> getSetsDelete() {
        return setsDelete;
    }

    /**
     * @return the assignments update
     */
    public List<NamespaceAssignmentSource> getAssignmentsUpdate() {
        return assignmentsUpdate;
    }

    /**
     * @return the assignments delete
     */
    public List<String> getAssignmentsDelete() {
        return assignmentsDelete;
    }

    /**
     * Has algorithms update.
     *
     * @return true if so false otherwise
     */
    public boolean hasAlgorithmsUpdate() {
        return CollectionUtils.isNotEmpty(algorithmsUpdate);
    }

    /**
     * Has algorithms delete.
     *
     * @return true if so false otherwise
     */
    public boolean hasAlgorithmsDelete() {
        return CollectionUtils.isNotEmpty(algorithmsDelete);
    }

    /**
     * Has matching tables update.
     *
     * @return true if so false otherwise
     */
    public boolean hasMatchingTablesUpdate() {
        return CollectionUtils.isNotEmpty(matchingTablesUpdate);
    }

    /**
     * Has matching tables delete.
     *
     * @return true if so false otherwise
     */
    public boolean hasMatchingTablesDelete() {
        return CollectionUtils.isNotEmpty(matchingTablesDelete);
    }

    /**
     * Has rules update.
     *
     * @return true if so false otherwise
     */
    public boolean hasRulesUpdate() {
        return CollectionUtils.isNotEmpty(rulesUpdate);
    }

    /**
     * Has rules delete.
     *
     * @return true if so false otherwise
     */
    public boolean hasRulesDelete() {
        return CollectionUtils.isNotEmpty(rulesDelete);
    }

    /**
     * Has sets update.
     *
     * @return true if so false otherwise
     */
    public boolean hasSetsUpdate() {
        return CollectionUtils.isNotEmpty(setsUpdate);
    }

    /**
     * Has sets delete.
     *
     * @return true if so false otherwise
     */
    public boolean hasSetsDelete() {
        return CollectionUtils.isNotEmpty(setsDelete);
    }

    /**
     * Has assignments update.
     *
     * @return true if so false otherwise
     */
    public boolean hasAssignmentsUpdate() {
        return CollectionUtils.isNotEmpty(assignmentsUpdate);
    }

    /**
     * Has assignments delete.
     *
     * @return true if so false otherwise
     */
    public boolean hasAssignmentsDelete() {
        return CollectionUtils.isNotEmpty(assignmentsDelete);
    }

    /**
     * Builder instance.
     *
     * @return builder instance
     */
    public static MatchingModelUpsertContextBuilder builder() {
        return new MatchingModelUpsertContextBuilder();
    }

    /**
     * Builder class.
     */
    public static class MatchingModelUpsertContextBuilder extends AbstractModelChangeContextBuilder<MatchingModelUpsertContextBuilder> {
        /**
         * Algorithms update.
         */
        private List<MatchingAlgorithmSource> algorithmsUpdate;
        /**
         * Algorithms deletes.
         */
        private List<String> algorithmsDelete;
        /**
         * Matching tables update.
         */
        private List<MatchingTableSource> matchingTablesUpdate;
        /**
         * Matching tables deletes.
         */
        private List<String> matchingTablesDelete;
        /**
         * Rules update.
         */
        private List<MatchingRuleSource> rulesUpdate;
        /**
         * Rules deletes.
         */
        private List<String> rulesDelete;
        /**
         * Rule sets update.
         */
        private List<MatchingRuleSetSource> setsUpdate;
        /**
         * Rule sets deletes.
         */
        private List<String> setsDelete;
        /**
         * Assignments update.
         */
        private List<NamespaceAssignmentSource> assignmentsUpdate;
        /**
         * Assignments deletes.
         */
        private List<String> assignmentsDelete;
        /**
         * The draft id.
         */
        private Long draftId;
        /**
         * The parent draft id.
         */
        private Long parentDraftId;

        /**
         * Constructor.
         */
        private MatchingModelUpsertContextBuilder() {
            super();
        }

        /**
         * Sets draft id.
         *
         * @param draftId the draft id
         * @return self
         */
        public MatchingModelUpsertContextBuilder draftId(Long draftId) {
            this.draftId = draftId;
            return self();
        }

        /**
         * Sets parent draft id.
         *
         * @param parentDraftId the parent draft id
         * @return self
         */
        public MatchingModelUpsertContextBuilder parentDraftId(Long parentDraftId) {
            this.parentDraftId = parentDraftId;
            return self();
        }

        /**
         * Sets algorithm updates.
         *
         * @param values sources
         * @return self
         */
        public MatchingModelUpsertContextBuilder algorithmsUpdate(MatchingAlgorithmSource... values) {
            if (ArrayUtils.isNotEmpty(values)) {
                return algorithmsUpdate(Arrays.asList(values));
            }
            return self();
        }

        /**
         * Sets algorithm updates.
         *
         * @param values sources
         * @return self
         */
        public MatchingModelUpsertContextBuilder algorithmsUpdate(Collection<MatchingAlgorithmSource> values) {
            if (CollectionUtils.isNotEmpty(values)) {
                if (algorithmsUpdate == null) {
                    algorithmsUpdate = new ArrayList<>();
                }

                algorithmsUpdate.addAll(values);
            }
            return self();
        }

        /**
         * Sets matching tables updates.
         *
         * @param values sources
         * @return self
         */
        public MatchingModelUpsertContextBuilder matchingTablesUpdate(MatchingTableSource... values) {
            if (ArrayUtils.isNotEmpty(values)) {
                return matchingTablesUpdate(Arrays.asList(values));
            }
            return self();
        }

        /**
         * Sets matching tables updates.
         *
         * @param values sources
         * @return self
         */
        public MatchingModelUpsertContextBuilder matchingTablesUpdate(Collection<MatchingTableSource> values) {
            if (CollectionUtils.isNotEmpty(values)) {
                if (matchingTablesUpdate == null) {
                    matchingTablesUpdate = new ArrayList<>();
                }

                matchingTablesUpdate.addAll(values);
            }
            return self();
        }

        /**
         * Sets matching rules updates.
         *
         * @param values sources
         * @return self
         */
        public MatchingModelUpsertContextBuilder rulesUpdate(MatchingRuleSource... values) {
            if (ArrayUtils.isNotEmpty(values)) {
                return rulesUpdate(Arrays.asList(values));
            }
            return self();
        }

        /**
         * Sets matching rules updates.
         *
         * @param values sources
         * @return self
         */
        public MatchingModelUpsertContextBuilder rulesUpdate(Collection<MatchingRuleSource> values) {
            if (CollectionUtils.isNotEmpty(values)) {
                if (rulesUpdate == null) {
                    rulesUpdate = new ArrayList<>();
                }

                rulesUpdate.addAll(values);
            }
            return self();
        }

        /**
         * Sets matching sets updates.
         *
         * @param values sources
         * @return self
         */
        public MatchingModelUpsertContextBuilder setsUpdate(MatchingRuleSetSource... values) {
            if (ArrayUtils.isNotEmpty(values)) {
                return setsUpdate(Arrays.asList(values));
            }
            return self();
        }

        /**
         * Sets matching sets updates.
         *
         * @param values sources
         * @return self
         */
        public MatchingModelUpsertContextBuilder setsUpdate(Collection<MatchingRuleSetSource> values) {
            if (CollectionUtils.isNotEmpty(values)) {
                if (setsUpdate == null) {
                    setsUpdate = new ArrayList<>();
                }

                setsUpdate.addAll(values);
            }
            return self();
        }

        /**
         * Sets matching assignments updates.
         *
         * @param values sources
         * @return self
         */
        public MatchingModelUpsertContextBuilder assignmentsUpdate(NamespaceAssignmentSource... values) {
            if (ArrayUtils.isNotEmpty(values)) {
                return assignmentsUpdate(Arrays.asList(values));
            }
            return self();
        }

        /**
         * Sets matching assignments updates.
         *
         * @param values sources
         * @return self
         */
        public MatchingModelUpsertContextBuilder assignmentsUpdate(Collection<NamespaceAssignmentSource> values) {
            if (CollectionUtils.isNotEmpty(values)) {
                if (assignmentsUpdate == null) {
                    assignmentsUpdate = new ArrayList<>();
                }

                assignmentsUpdate.addAll(values);
            }
            return self();
        }

        /**
         * Sets algorithms to delete.
         *
         * @param values algorithm ids
         * @return self
         */
        public MatchingModelUpsertContextBuilder algorithmsDelete(String... values) {
            if (ArrayUtils.isNotEmpty(values)) {
                return algorithmsDelete(Arrays.asList(values));
            }
            return self();
        }

        /**
         * Sets algorithms to delete.
         *
         * @param values algorithm ids
         * @return self
         */
        public MatchingModelUpsertContextBuilder algorithmsDelete(Collection<String> values) {
            if (CollectionUtils.isNotEmpty(values)) {
                if (algorithmsDelete == null) {
                    algorithmsDelete = new ArrayList<>();
                }

                algorithmsDelete.addAll(values);
            }
            return self();
        }

        /**
         * Sets matching tables to delete.
         *
         * @param values matching tables ids
         * @return self
         */
        public MatchingModelUpsertContextBuilder matchingTablesDelete(String... values) {
            if (ArrayUtils.isNotEmpty(values)) {
                return matchingTablesDelete(Arrays.asList(values));
            }
            return self();
        }

        /**
         * Sets matching tables to delete.
         *
         * @param values matching tables ids
         * @return self
         */
        public MatchingModelUpsertContextBuilder matchingTablesDelete(Collection<String> values) {
            if (CollectionUtils.isNotEmpty(values)) {
                if (matchingTablesDelete == null) {
                    matchingTablesDelete = new ArrayList<>();
                }

                matchingTablesDelete.addAll(values);
            }
            return self();
        }

        /**
         * Sets matching rules to delete.
         *
         * @param values matching rules ids
         * @return self
         */
        public MatchingModelUpsertContextBuilder rulesDelete(String... values) {
            if (ArrayUtils.isNotEmpty(values)) {
                return rulesDelete(Arrays.asList(values));
            }
            return self();
        }

        /**
         * Sets matching rules to delete.
         *
         * @param values matching rules ids
         * @return self
         */
        public MatchingModelUpsertContextBuilder rulesDelete(Collection<String> values) {
            if (CollectionUtils.isNotEmpty(values)) {
                if (rulesDelete == null) {
                    rulesDelete = new ArrayList<>();
                }

                rulesDelete.addAll(values);
            }
            return self();
        }

        /**
         * Sets matching rule sets to delete.
         *
         * @param values matching rule sets ids
         * @return self
         */
        public MatchingModelUpsertContextBuilder setsDelete(String... values) {
            if (ArrayUtils.isNotEmpty(values)) {
                return setsDelete(Arrays.asList(values));
            }
            return self();
        }

        /**
         * Sets matching rule sets to delete.
         *
         * @param values matching rule sets ids
         * @return self
         */
        public MatchingModelUpsertContextBuilder setsDelete(Collection<String> values) {
            if (CollectionUtils.isNotEmpty(values)) {
                if (setsDelete == null) {
                    setsDelete = new ArrayList<>();
                }

                setsDelete.addAll(values);
            }
            return self();
        }

        /**
         * Sets matching assignments to delete.
         *
         * @param values matching assignments keys
         * @return self
         */
        public MatchingModelUpsertContextBuilder assignmentsDelete(String... values) {
            if (ArrayUtils.isNotEmpty(values)) {
                return assignmentsDelete(Arrays.asList(values));
            }
            return self();
        }

        /**
         * Sets matching assignments to delete.
         *
         * @param values matching assignments keys
         * @return self
         */
        public MatchingModelUpsertContextBuilder assignmentsDelete(Collection<String> values) {
            if (CollectionUtils.isNotEmpty(values)) {
                if (assignmentsDelete == null) {
                    assignmentsDelete = new ArrayList<>();
                }

                assignmentsDelete.addAll(values);
            }
            return self();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MatchingModelUpsertContext build() {
            return new MatchingModelUpsertContext(this);
        }
    }
}
