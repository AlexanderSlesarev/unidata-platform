/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dto;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSetSource;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class SetsModelGetResult {
    /**
     * Matching rule sets, if asked as standalone.
     */
    private List<MatchingRuleSetSource> sets;

    /**
     * Constructor.
     *
     * @param sets matching sets
     */
    public SetsModelGetResult(List<MatchingRuleSetSource> sets) {
        super();
        this.sets = sets;
    }

    /**
     * Constructor.
     *
     * @param set matching set
     */
    public SetsModelGetResult(MatchingRuleSetSource set) {
        super();
        this.sets = Collections.singletonList(set);
    }

    /**
     * @return the sets
     */
    public List<MatchingRuleSetSource> getSets() {
        return Objects.isNull(sets) ? Collections.emptyList() : sets;
    }

    /**
     * @param sets the rule sets to set
     */
    public void setSets(List<MatchingRuleSetSource> sets) {
        this.sets = sets;
    }

    /**
     * Gets first value.
     *
     * @return algorithm
     */
    public MatchingRuleSetSource singleValue() {
        return CollectionUtils.isNotEmpty(sets)
                ? sets.get(0)
                : null;
    }
}
