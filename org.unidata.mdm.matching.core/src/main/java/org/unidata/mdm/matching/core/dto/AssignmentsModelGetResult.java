/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dto;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.model.source.assignment.NamespaceAssignmentSource;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class AssignmentsModelGetResult {
    /**
     * Assignments, if asked as standalone.
     */
    private List<NamespaceAssignmentSource> assignments;

    /**
     * Constructor.
     *
     * @param assignments matching assignments
     */
    public AssignmentsModelGetResult(List<NamespaceAssignmentSource> assignments) {
        super();
        this.assignments = assignments;
    }

    /**
     * Constructor.
     *
     * @param assignment matching assignment
     */
    public AssignmentsModelGetResult(NamespaceAssignmentSource assignment) {
        super();
        this.assignments = Collections.singletonList(assignment);
    }

    /**
     * @return the assignments
     */
    public List<NamespaceAssignmentSource> getAssignments() {
        return Objects.isNull(assignments) ? Collections.emptyList() : assignments;
    }

    /**
     * @param assignments the assignments to set
     */
    public void setAssignments(List<NamespaceAssignmentSource> assignments) {
        this.assignments = assignments;
    }

    /**
     * Gets first value.
     *
     * @return assignment
     */
    public NamespaceAssignmentSource singleValue() {
        return CollectionUtils.isNotEmpty(assignments)
                ? assignments.get(0)
                : null;
    }
}
