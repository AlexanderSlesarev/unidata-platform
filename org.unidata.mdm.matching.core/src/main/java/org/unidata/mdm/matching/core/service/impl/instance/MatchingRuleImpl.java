/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl.instance;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.type.model.instance.AbstractNamedDisplayableCustomPropertiesImpl;
import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithm;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmParameterElement;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmSettingsElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleElement;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmSettingsSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSource;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 20.06.2021
 */
public class MatchingRuleImpl extends AbstractNamedDisplayableCustomPropertiesImpl
        implements MatchingRuleElement {

    /**
     * Matching rule source.
     */
    private final MatchingRuleSource source;
    /**
     * Selected algorithms.
     */
    private final Map<String, AlgorithmSettingsElement> algorithms;

    /**
     * Constructor.
     *
     * @param source matching rule source
     */
    public MatchingRuleImpl(MatchingRuleSource source, MatchingModelInstance instance) {
        super(source.getName(), source.getDisplayName(), source.getDescription(), source.getCustomProperties());

        this.source = source;

        this.algorithms = new HashMap<>(source.getAlgorithms().size());
        int orderCount = 0;

        for (AlgorithmSettingsSource s : source.getAlgorithms()) {
            AlgorithmSettingsImpl impl = new AlgorithmSettingsImpl(s, source.getName(), orderCount++, instance);
            this.algorithms.put(impl.getId(), impl);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<AlgorithmSettingsElement> getAlgorithms() {
        return algorithms.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AlgorithmSettingsElement getAlgorithm(String id) {
        return algorithms.get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getCreateDate() {
        return source.getCreateDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreatedBy() {
        return source.getCreatedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getUpdateDate() {
        return source.getUpdateDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUpdatedBy() {
        return source.getUpdatedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMatchingStorageId() {
        return source.getMatchingStorageId();
    }

    /**
     * Gets matching rule source.
     *
     * @return matching rule source
     */
    public MatchingRuleSource getSource() {
        return source;
    }

    /**
     * Algorithm configuration implementation.
     */
    private static class AlgorithmSettingsImpl implements AlgorithmSettingsElement {
        /**
         * Source.
         */
        private final AlgorithmSettingsSource source;
        /**
         * Order.
         */
        private final int order;
        /**
         * Matching algorithm.
         */
        private final MatchingAlgorithm algorithm;
        /**
         * Algorithm parameters.
         */
        private final Map<String, AlgorithmParameterElement> parameters;

        /**
         * Constructor.
         *
         * @param source algorithm configuration source
         */
        public AlgorithmSettingsImpl(AlgorithmSettingsSource source, String ruleName, int order, MatchingModelInstance instance) {
            this.source = source;
            this.order = order;

            this.algorithm = instance.getAlgorithm(source.getAlgorithmName()).getAlgorithm();
            parameters = source.getParameters().stream()
                    .map(AlgorithmParameterImpl::new)
                    .collect(Collectors.toMap(AlgorithmParameterElement::getId, Function.identity()));

            String[] parts = new String[] {
                    ruleName, getAlgorithmName(), String.valueOf(getOrder())
            };

            String id = StringUtils.join(parts, "_");
            this.source.setId(id);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getAlgorithmName() {
            return source.getAlgorithmName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MatchingAlgorithm getAlgorithm() {
            return algorithm;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Collection<AlgorithmParameterElement> getParameters() {
            return parameters.values();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public AlgorithmParameterElement getParameter(String id) {
            return parameters.get(id);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getId() {
            return source.getId();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int getOrder() {
            return order;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AlgorithmSettingsImpl that = (AlgorithmSettingsImpl) o;

            return Objects.equals(getAlgorithmName(), that.getAlgorithmName()) &&
                    CollectionUtils.isEqualCollection(getParameters(), that.getParameters());
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode() {
            return Objects.hash(getAlgorithmName(), Arrays.hashCode(parameters.values().toArray()));
        }
    }
}
