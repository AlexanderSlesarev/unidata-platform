/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.storage;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.model.refresh.MatchingModelRefreshListener;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author Sergey Murskiy on 07.09.2021
 */
public class MatchingStorageDescriptor {
    /**
     * Matching storage id.
     */
    private final String storageId;
    /**
     * Matching storage display name.
     */
    private final Supplier<String> displayName;
    /**
     * Matching storage description.
     */
    private final Supplier<String> description;
    /**
     * Matching storage algorithms.
     */
    private final List<String> matchingAlgorithms;
    /**
     * Matching model refresh listeners.
     */
    private final List<MatchingModelRefreshListener> modelRefreshListeners;

    /**
     * Constructor.
     *
     * @param storageId             the matching storage id
     * @param displayName           the matching storage display name
     * @param description           the matching storage description
     * @param matchingAlgorithms    matching storage algorithms
     * @param modelRefreshListeners matching model refresh listeners
     */
    public MatchingStorageDescriptor(String storageId, Supplier<String> displayName, Supplier<String> description,
                                     List<String> matchingAlgorithms,
                                     List<MatchingModelRefreshListener> modelRefreshListeners) {
        super();
        Objects.requireNonNull(storageId, "Matching storage id must not be null.");
        this.storageId = storageId;
        this.displayName = displayName;
        this.description = description;
        this.matchingAlgorithms = matchingAlgorithms;
        this.modelRefreshListeners = modelRefreshListeners;
    }

    /**
     * Gets matching storage id.
     *
     * @return the matching storage id
     */
    public String getStorageId() {
        return storageId;
    }

    /**
     * Gets matching storage display name.
     *
     * @return the display name
     */
    public String getDisplayName() {
        return displayName.get();
    }

    /**
     * Gets matching storage description.
     *
     * @return the description
     */
    public String getDescription() {
        return description.get();
    }

    /**
     * Gets matching storage algorithms.
     *
     * @return matching algorithms
     */
    public List<String> getMatchingAlgorithms() {
        return CollectionUtils.isEmpty(matchingAlgorithms)
                ? Collections.emptyList()
                : matchingAlgorithms;
    }

    /**
     * Gets matching model refresh listeners.
     *
     * @return matching model refresh listeners
     */
    public List<MatchingModelRefreshListener> getModelRefreshListeners() {
        return CollectionUtils.isEmpty(modelRefreshListeners)
                ? Collections.emptyList()
                : modelRefreshListeners;
    }

    /**
     * Creates descriptor.
     *
     * @param storageId          the matching storage id
     * @param displayName        the matching storage display name
     * @param description        the matching storage description
     * @param matchingAlgorithms matching storage algorithms
     * @param listeners          matching model refresh listeners
     */
    public static MatchingStorageDescriptor of(String storageId, Supplier<String> displayName,
                                               Supplier<String> description,
                                               List<String> matchingAlgorithms,
                                               List<MatchingModelRefreshListener> listeners) {

        return new MatchingStorageDescriptor(storageId, displayName, description, matchingAlgorithms, listeners);
    }
}
