/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.segments;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.dto.DraftUpsertResult;
import org.unidata.mdm.matching.core.module.MatchingCoreModule;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
@Component(MatchingDraftUpsertStartExecutor.SEGMENT_ID)
public class MatchingDraftUpsertStartExecutor extends Start<DraftUpsertContext, DraftUpsertResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingCoreModule.MODULE_ID + "[MATCHING_DRAFT_UPSERT_START]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingCoreModule.MODULE_ID + ".matching.draft.upsert.start.description";

    /**
     * Constructor.
     */
    public MatchingDraftUpsertStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftUpsertContext.class, DraftUpsertResult.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(@NonNull DraftUpsertContext ctx) {
        // NOOP. Start does nothing here.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String subject(DraftUpsertContext ctx) {
        // No subject for this type of pipelines
        // This may be storage id in the future
        return null;
    }
}
