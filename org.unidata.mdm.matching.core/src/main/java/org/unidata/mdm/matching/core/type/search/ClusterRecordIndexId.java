/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.search;

import org.unidata.mdm.matching.core.configuration.MatchingCoreConfigurationConstants;
import org.unidata.mdm.search.type.IndexType;
import org.unidata.mdm.search.type.id.AbstractManagedIndexId;

import java.util.Date;

/**
 * @author Sergey Murskiy on 20.07.2021
 */
public class ClusterRecordIndexId extends AbstractManagedIndexId {
    /**
     * {@inheritDoc}
     */
    @Override
    public String getEntityName() {
        return MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexType getSearchType() {
        return ClusterHeadIndexType.CLUSTER;
    }

    /**
     * Creates cluster record index id.
     *
     * @param clusterId the cluster id
     * @param namespace the namespace
     * @param typeName  the type name
     * @param subjectId the subject id
     * @param validTo   the valid to
     * @return cluster record index id
     */
    public static ClusterRecordIndexId of(String clusterId, String namespace, String typeName, String subjectId,
                                          Date validTo) {
        ClusterRecordIndexId id = new ClusterRecordIndexId();

        id.setIndexId(AbstractManagedIndexId.periodId(
                clusterId,
                namespace,
                typeName,
                subjectId,
                AbstractManagedIndexId.periodIdValToString(validTo)));
        id.setRouting(clusterId);

        return id;
    }

    private void setIndexId(String indexId) {
        this.indexId = indexId;
    }

    private void setRouting(String routing) {
        this.routing = routing;
    }
}
