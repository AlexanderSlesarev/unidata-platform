/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.algorithm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author Sergey Murskiy on 20.06.2021
 */
public class EnumValueSource implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -3055830659667251977L;

    @JacksonXmlProperty(isAttribute = true)
    private String name;

    @JacksonXmlProperty(isAttribute = true)
    private String displayName;

    @JsonIgnore
    protected transient Supplier<String> displayNameSupplier;

    /**
     * @return name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return Objects.nonNull(displayNameSupplier) ? displayNameSupplier.get() : displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @param displayNameSupplier the displayNameSupplier to set
     */
    public void setDisplayName(Supplier<String> displayNameSupplier) {
        this.displayNameSupplier = displayNameSupplier;
    }


    /**
     * Enum value with name.
     *
     * @param name enum name
     * @return self
     */
    public EnumValueSource withName(String name) {
        setName(name);
        return self();
    }

    /**
     * Enum value with display name.
     *
     * @param displayName enum display name
     * @return self
     */
    public EnumValueSource withDisplayName(String displayName) {
        setDisplayName(displayName);
        return self();
    }

    /**
     * Enum value with display name.
     *
     * @param value enum display name
     * @return self
     */
    public EnumValueSource withDisplayName(Supplier<String> value) {
        setDisplayName(value);
        return this;
    }

    private EnumValueSource self() {
        return this;
    }
}
