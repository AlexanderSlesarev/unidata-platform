/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl.algorithm;

import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithm;

import java.util.function.Supplier;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
public abstract class AbstractSystemMatchingAlgorithm implements MatchingAlgorithm {
    /**
     * Name of the function.
     */
    private final String name;
    /**
     * Display name.
     */
    private final Supplier<String> displayName;
    /**
     * Description.
     */
    private final Supplier<String> description;
    /**
     * Matching storage id.
     */
    private final String matchingStorageId;

    /**
     * Instantiates a new matching algorithm abstract.
     *
     * @param name              the name
     * @param displayName       the display name
     * @param description       the description
     * @param matchingStorageId the matching storage id
     */
    protected AbstractSystemMatchingAlgorithm(String name, Supplier<String> displayName,
                                              Supplier<String> description, String matchingStorageId) {
        super();
        this.name = name;
        this.displayName = displayName;
        this.description = description;
        this.matchingStorageId = matchingStorageId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName.get();
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description.get();
    }

    /**
     * @return the matching storage id
     */
    public String getMatchingStorageId() {
        return matchingStorageId;
    }
}
