/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.cluster;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.08.2021
 */
public class ClusterSearchQuery {
    /**
     * Cluster ids.
     */
    private List<String> clusterIds;
    /**
     * Matching set names.
     */
    private List<String> setNames;
    /**
     * Matching rule name.
     */
    private List<String> ruleNames;
    /**
     * Records query.
     */
    private List<ClusterRecordSearchQuery> recordsQueries;

    /**
     * Gets cluster ids.
     *
     * @return the cluster ids
     */
    public List<String> getClusterIds() {
        return ListUtils.emptyIfNull(clusterIds);
    }

    /**
     * Gets set names.
     *
     * @return the matching set names
     */
    public List<String> getSetNames() {
        return ListUtils.emptyIfNull(setNames);
    }

    /**
     * Gets rule names.
     *
     * @return the matching rule names
     */
    public List<String> getRuleNames() {
        return ListUtils.emptyIfNull(ruleNames);
    }

    /**
     * Gets records query.
     *
     * @return records query
     */
    public List<ClusterRecordSearchQuery> getRecordsQueries() {
        return ListUtils.emptyIfNull(recordsQueries);
    }

    /**
     * Sets cluster ids to query.
     *
     * @param clusterIds the cluster id
     * @return self
     */
    public ClusterSearchQuery withClusterIds(String... clusterIds) {
        if (ArrayUtils.isEmpty(clusterIds)) {
            return this;
        }

        return withClusterIds(Arrays.asList(clusterIds));
    }

    /**
     * Sets cluster ids to query.
     *
     * @param clusterIds the cluster id
     * @return self
     */
    public ClusterSearchQuery withClusterIds(Collection<String> clusterIds) {
        if (CollectionUtils.isEmpty(clusterIds)) {
            return this;
        }

        if (Objects.isNull(this.clusterIds)) {
            this.clusterIds = new ArrayList<>();
        }

        this.clusterIds.addAll(clusterIds);

        return this;
    }

    /**
     * Sets matching set names to query.
     *
     * @param setNames the set names
     * @return self
     */
    public ClusterSearchQuery withSetNames(String... setNames) {
        if (ArrayUtils.isEmpty(setNames)) {
            return this;
        }

        return withSetNames(Arrays.asList(setNames));
    }

    /**
     * Sets matching set names to query.
     *
     * @param setNames the set names
     * @return self
     */
    public ClusterSearchQuery withSetNames(Collection<String> setNames) {
        if (CollectionUtils.isEmpty(setNames)) {
            return this;
        }

        if (Objects.isNull(this.setNames)) {
            this.setNames = new ArrayList<>();
        }

        this.setNames.addAll(setNames);

        return this;
    }

    /**
     * Sets matching rule names to query.
     *
     * @param ruleNames the matching rule names
     * @return self
     */
    public ClusterSearchQuery withRuleNames(String... ruleNames) {
        if (ArrayUtils.isEmpty(ruleNames)) {
            return this;
        }

        return withRuleNames(Arrays.asList(ruleNames));
    }

    /**
     * Sets matching rule names to query.
     *
     * @param ruleNames the matching rule names
     * @return self
     */
    public ClusterSearchQuery withRuleNames(Collection<String> ruleNames) {
        if (CollectionUtils.isEmpty(ruleNames)) {
            return this;
        }

        if (Objects.isNull(this.ruleNames)) {
            this.ruleNames = new ArrayList<>();
        }

        this.ruleNames.addAll(ruleNames);

        return this;
    }

    /**
     * Sets cluster records queries to query.
     *
     * @param recordsQueries the records queries
     * @return self
     */
    public ClusterSearchQuery withRecordsQueries(ClusterRecordSearchQuery... recordsQueries) {
        if (ArrayUtils.isEmpty(recordsQueries)) {
            return this;
        }

        return withRecordsQueries(Arrays.asList(recordsQueries));
    }

    /**
     * Sets cluster records queries to query.
     *
     * @param recordsQueries the records queries
     * @return self
     */
    public ClusterSearchQuery withRecordsQueries(Collection<ClusterRecordSearchQuery> recordsQueries) {
        if (CollectionUtils.isEmpty(recordsQueries)) {
            return this;
        }

        if (Objects.isNull(this.recordsQueries)) {
            this.recordsQueries = new ArrayList<>();
        }

        this.recordsQueries.addAll(recordsQueries);

        return this;
    }

    /**
     * Cluster record query
     */
    public static class ClusterRecordSearchQuery {
        /**
         * Namespace.
         */
        private String namespace;
        /**
         * Type name.
         */
        private String typeName;
        /**
         * Subject id.
         */
        private String subjectId;
        /**
         * Valid from.
         */
        private Date validFrom;
        /**
         * Valid to.
         */
        private Date validTo;
        /**
         * As of date.
         */
        private Date asOf;

        /**
         * Gets namespace.
         *
         * @return the namespace
         */
        public String getNamespace() {
            return namespace;
        }

        /**
         * Sets namespace.
         *
         * @param namespace the namespace to set
         */
        public void setNamespace(String namespace) {
            this.namespace = namespace;
        }

        /**
         * Gets type name.
         *
         * @return the type name
         */
        public String getTypeName() {
            return typeName;
        }

        /**
         * Sets type name.
         *
         * @param typeName the type name to set
         */
        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        /**
         * Gets subject id.
         *
         * @return the subject id
         */
        public String getSubjectId() {
            return subjectId;
        }

        /**
         * Sets subject id.
         *
         * @param subjectId the subject id to set
         */
        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        /**
         * Gets valid from.
         *
         * @return the valid from
         */
        public Date getValidFrom() {
            return validFrom;
        }

        /**
         * Sets valid from.
         *
         * @param validFrom the valid from to sets
         */
        public void setValidFrom(Date validFrom) {
            this.validFrom = validFrom;
        }

        /**
         * Gets valid to.
         *
         * @return the valid to
         */
        public Date getValidTo() {
            return validTo;
        }

        /**
         * Sets valid to.
         *
         * @param validTo the valid to to sets
         */
        public void setValidTo(Date validTo) {
            this.validTo = validTo;
        }

        /**
         * Gets as of date.
         * @return as of date
         */
        public Date getAsOf() {
            return asOf;
        }

        /**
         * Sets as of date.
         *
         * @param asOf as of date to set
         */
        public void setAsOf(Date asOf) {
            this.asOf = asOf;
        }

        /**
         * Cluster record query with namespace.
         *
         * @param namespace the namespace
         * @return self
         */
        public ClusterRecordSearchQuery withNamespace(String namespace) {
            setNamespace(namespace);
            return this;
        }

        /**
         * Cluster record query with type name.
         *
         * @param typeName the type name
         * @return self
         */
        public ClusterRecordSearchQuery withTypeName(String typeName) {
            setTypeName(typeName);
            return this;
        }

        /**
         * Cluster record query with subject id.
         *
         * @param subjectId the subject id
         * @return self
         */
        public ClusterRecordSearchQuery withSubjectId(String subjectId) {
            setSubjectId(subjectId);
            return this;
        }

        /**
         * Cluster record query with valid form.
         *
         * @param validFrom the valid from
         * @return self
         */
        public ClusterRecordSearchQuery withValidFrom(Date validFrom) {
            setValidFrom(validFrom);
            return this;
        }

        /**
         * Cluster record query with valid to.
         *
         * @param validTo the valid to
         * @return self
         */
        public ClusterRecordSearchQuery withValidTo(Date validTo) {
            setValidTo(validTo);
            return this;
        }

        /**
         * Cluster record query with as of date.
         *
         * @param asOf the as of date
         * @return self
         */
        public ClusterRecordSearchQuery withAsOfDate(Date asOf) {
            setAsOf(asOf);
            return this;
        }
    }
}
