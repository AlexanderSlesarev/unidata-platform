/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.algorithm;

import java.util.function.Supplier;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
public class EnumValue {
    /**
     * Name of the enum value.
     */
    private final String name;
    /**
     * Plain display name.
     */
    private final String displayName;
    /**
     * Display name supplier to support i18n.
     */
    private final Supplier<String> displayNameSupplier;

    /**
     * Constructor.
     */
    private EnumValue(EnumValueBuilder b) {
        this.name = b.name;
        this.displayName = b.displayName;
        this.displayNameSupplier = b.displayNameSupplier;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @return the display name supplier
     */
    public Supplier<String> getDisplayNameSupplier() {
        return displayNameSupplier;
    }

    /**
     * @return builder
     */
    public static EnumValueBuilder builder() {
        return new EnumValueBuilder();
    }

    /**
     * Builder.
     */
    public static class EnumValueBuilder {
        /**
         * Name of the enum value.
         */
        private String name;
        /**
         * Plain display name.
         */
        private String displayName;
        /**
         * Display name supplier to support i18n.
         */
        private Supplier<String> displayNameSupplier;

        /**
         * Constructor.
         */
        private EnumValueBuilder() {
            super();
        }

        /**
         * @param name the name to set
         */
        public EnumValueBuilder name(String name) {
            this.name = name;
            return this;
        }

        /**
         * @param displayName the displayName to set
         */
        public EnumValueBuilder displayName(String displayName) {
            this.displayName = displayName;
            return this;
        }

        /**
         * @param displayNameSupplier the displayNameSupplier to set
         */
        public EnumValueBuilder displayName(Supplier<String> displayNameSupplier) {
            this.displayNameSupplier = displayNameSupplier;
            return this;
        }

        /**
         * Build.
         *
         * @return enum value
         */
        public EnumValue build() {
            return new EnumValue(this);
        }
    }

}
