/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.algorithm;

import org.springframework.lang.Nullable;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
public interface MatchingAlgorithm {
    /**
     * Returns MatchingAlgorithm definition in terms of
     * <br>
     * - name<br>
     * - display name<br>
     * - description<br>
     * - list of parameters<br>
     * <br>
     * If a algorithm returns configuration, it is considered "autoconfigured" and
     * configuration for this element is not available on UI (set read only).
     *
     * @return configuration or null
     */
    @Nullable
    default MatchingAlgorithmConfiguration configure() {
        return null;
    }
}
