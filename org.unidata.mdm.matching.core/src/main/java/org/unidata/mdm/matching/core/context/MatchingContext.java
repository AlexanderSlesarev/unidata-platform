/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.matching.core.type.data.MatchingRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.execution.MatchingExecutionMode;
import org.unidata.mdm.matching.core.type.execution.MatchingRealTimeInput;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.08.2021
 */
public class MatchingContext extends AbstractMatchingContext {
    /**
     * Matching input for execution.
     */
    private final MatchingRealTimeInput realTimeInput;
    /**
     * Matching sets to execution.
     */
    private final List<MatchingRuleSetElement> matchingSets;
    /**
     * Matching rules to execution.
     */
    private final Map<MatchingRuleSetElement, List<MatchingRuleElement>> matchingRules;
    /**
     * Matching execution mode.
     */
    private final MatchingExecutionMode executionMode;
    /**
     * Matching records ids to delete.
     */
    private final Map<String, List<MatchingRecordKey>> deletes;
    /**
     * Matching records to insert.
     */
    private final Map<String, List<MatchingRecord>> inserts;
    /**
     * Is skip result calculation.
     */
    private final boolean skipResultCalculation;
    /**
     * Is skip computing cluster updates.
     */
    private final boolean skipClusterUpdates;
    /**
     * Is clean cluster storage.
     */
    private final boolean cleanClusterStorage;

    /**
     * Constructor.
     *
     * @param b matching context builder
     */
    public MatchingContext(MatchingContextBuilder b) {
        super(b);

        this.realTimeInput = b.matchingRealTimeInput;
        this.matchingSets = b.matchingSets;
        this.matchingRules = b.matchingRules;
        this.deletes = b.deletes;
        this.inserts = b.inserts;
        this.executionMode = b.executionMode;
        this.skipResultCalculation = b.skipResultCalculation;
        this.skipClusterUpdates = b.skipClusterUpdates;
        this.cleanClusterStorage = b.cleanClusterStorage;
    }

    /**
     * Gets real-time matching input.
     *
     * @return real-time input
     */
    public MatchingRealTimeInput getRealTimeInput() {
        return realTimeInput;
    }

    /**
     * Gets matching sets.
     *
     * @return matching sets
     */
    public List<MatchingRuleSetElement> getMatchingSets() {
        return ListUtils.emptyIfNull(matchingSets);
    }

    /**
     * Gets matching rules.
     *
     * @return matching rules
     */
    public Map<MatchingRuleSetElement, List<MatchingRuleElement>> getMatchingRules() {
        return MapUtils.emptyIfNull(matchingRules);
    }

    /**
     * Gets deletes.
     *
     * @return deletes
     */
    public Map<String, List<MatchingRecordKey>> getDeletes() {
        return MapUtils.emptyIfNull(deletes);
    }

    /**
     * Gets inserts.
     *
     * @return inserts
     */
    public Map<String, List<MatchingRecord>> getInserts() {
        return MapUtils.emptyIfNull(inserts);
    }

    /**
     * Gets execution mode.
     *
     * @return execution mode
     */
    public MatchingExecutionMode getExecutionMode() {
        return Objects.isNull(executionMode)
                ? MatchingExecutionMode.STANDARD
                : executionMode;
    }

    /**
     * Gets 'skipResultCalculation' flag.
     *
     * @return is skip result calculation
     */
    public boolean isSkipResultCalculation() {
        return skipResultCalculation;
    }

    /**
     * Gets 'skipClusterUpdates' flag.
     *
     * @return is skip cluster updates
     */
    public boolean isSkipClusterUpdates() {
        return skipClusterUpdates;
    }

    /**
     * Gets 'cleanClusterStorage' flag.
     *
     * @return is clean cluster storage
     */
    public boolean isCleanClusterStorage() {
        return cleanClusterStorage;
    }

    /**
     * Gets builder.
     *
     * @return the builder
     */
    public static MatchingContextBuilder builder() {
        return new MatchingContextBuilder();
    }

    /**
     * Builder class.
     */
    public static class MatchingContextBuilder extends AbstractMatchingContextBuilder<MatchingContextBuilder> {
        /**
         * Matching input to execution.
         */
        private MatchingRealTimeInput matchingRealTimeInput;
        /**
         * Matching sets to execution.
         */
        private List<MatchingRuleSetElement> matchingSets;
        /**
         * Matching rules to execution.
         */
        private Map<MatchingRuleSetElement, List<MatchingRuleElement>> matchingRules;
        /**
         * Matching execution mode.
         */
        private MatchingExecutionMode executionMode;
        /**
         * Matching records ids to delete.
         */
        private Map<String, List<MatchingRecordKey>> deletes;
        /**
         * Matching records to insert.
         */
        private Map<String, List<MatchingRecord>> inserts;
        /**
         * Is skip result calculation.
         */
        private boolean skipResultCalculation;
        /**
         * Is skip computing cluster updates.
         */
        private boolean skipClusterUpdates;
        /**
         * Is clean cluster storage.
         */
        private boolean cleanClusterStorage;

        /**
         * Adds keys to delete.
         *
         * @param matchingTableName the matching table name
         * @param keys              the matching record keys
         * @return self
         */
        public MatchingContextBuilder delete(String matchingTableName, MatchingRecordKey... keys) {
            if (ArrayUtils.isEmpty(keys)) {
                return self();
            }

            return delete(Map.of(matchingTableName, Arrays.asList(keys)));
        }

        /**
         * Adds keys to delete.
         *
         * @param matchingTableName the matching table name
         * @param keys              the matching record keys
         * @return self
         */
        public MatchingContextBuilder delete(String matchingTableName, List<MatchingRecordKey> keys) {

            if (CollectionUtils.isEmpty(keys) || StringUtils.isBlank(matchingTableName)) {
                return self();
            }

            return delete(Map.of(matchingTableName, keys));
        }

        /**
         * Adds keys to delete.
         *
         * @param keys the matching record keys
         * @return self
         */
        public MatchingContextBuilder delete(Map<String, List<MatchingRecordKey>> keys) {

            if (MapUtils.isEmpty(keys)) {
                return self();
            }

            if (Objects.isNull(deletes)) {
                deletes = new HashMap<>();
            }

            for (Map.Entry<String, List<MatchingRecordKey>> e : keys.entrySet()) {
                for (MatchingRecordKey key : e.getValue()) {
                    if (Objects.isNull(key) || key.isValidKey()) {
                        continue;
                    }

                    deletes.computeIfAbsent(e.getKey(), k -> new ArrayList<>()).add(key);
                }
            }

            return self();
        }

        /**
         * Adds objects to insert.
         *
         * @param matchingTableName the matching table name
         * @param records           matching records to insert
         * @return self
         */
        public MatchingContextBuilder insert(String matchingTableName, MatchingRecord... records) {
            if (ArrayUtils.isNotEmpty(records) && StringUtils.isNotBlank(matchingTableName)) {
                return insert(matchingTableName, Arrays.asList(records));
            }

            return self();
        }

        /**
         * Adds objects to index.
         *
         * @param matchingTableName the matching table name
         * @param records           matching records to insert
         * @return self
         */
        public MatchingContextBuilder insert(String matchingTableName, Collection<MatchingRecord> records) {

            if (StringUtils.isBlank(matchingTableName) || CollectionUtils.isEmpty(records)) {
                return self();
            }

            for (MatchingRecord matchingRecord : records) {
                if (Objects.isNull(matchingRecord) || !matchingRecord.isValid()) {
                    continue;
                }

                if (Objects.isNull(inserts)) {
                    inserts = new HashMap<>();
                }

                inserts.computeIfAbsent(matchingTableName, key -> new ArrayList<>()).add(matchingRecord);
            }

            return self();
        }

        /**
         * Adds objects to inserts.
         *
         * @param inserts the objects to insert
         * @return self
         */
        public MatchingContextBuilder insert(Map<String, List<MatchingRecord>> inserts) {

            if (MapUtils.isEmpty(inserts)) {
                return self();
            }

            if (Objects.isNull(this.inserts)) {
                this.inserts = new HashMap<>();
            }

            for (Map.Entry<String, List<MatchingRecord>> e : inserts.entrySet()) {
                for (MatchingRecord matchingRecord : e.getValue()) {
                    if (Objects.isNull(matchingRecord) || matchingRecord.isValid()) {
                        continue;
                    }

                    this.inserts.computeIfAbsent(e.getKey(), key -> new ArrayList<>()).add(matchingRecord);
                }
            }

            return self();
        }

        /**
         * Sets matching input to execution.
         *
         * @param matchingRealTimeInput the matching input
         * @return self
         */
        public MatchingContextBuilder matchingInput(MatchingRealTimeInput matchingRealTimeInput) {
            this.matchingRealTimeInput = matchingRealTimeInput;
            return self();
        }

        /**
         * Sets matching sets to execution.
         *
         * @param sets matching sets
         * @return self
         */
        public MatchingContextBuilder matchingSets(MatchingRuleSetElement... sets) {
            if (ArrayUtils.isEmpty(sets)) {
                return self();
            }

            return matchingSets(Arrays.asList(sets));
        }

        /**
         * Sets matching sets to execution.
         *
         * @param sets matching sets
         * @return self
         */
        public MatchingContextBuilder matchingSets(Collection<MatchingRuleSetElement> sets) {
            if (CollectionUtils.isEmpty(sets)) {
                return self();
            }

            if (Objects.isNull(this.matchingSets)) {
                this.matchingSets = new ArrayList<>();
            }

            this.matchingSets.addAll(sets);

            return self();
        }

        /**
         * Sets matching rules to execution.
         *
         * @param set  matching set
         * @param rule matching rule
         * @return self
         */
        public MatchingContextBuilder matchingRules(MatchingRuleSetElement set, MatchingRuleElement rule) {
            if (Objects.isNull(set) || Objects.isNull(rule)) {
                return self();
            }

            return matchingRules(set, Collections.singletonList(rule));
        }

        /**
         * Sets matching rules to execution.
         *
         * @param set   matching set
         * @param rules matching rules
         * @return self
         */
        public MatchingContextBuilder matchingRules(MatchingRuleSetElement set, Collection<MatchingRuleElement> rules) {
            if (Objects.isNull(set) || CollectionUtils.isEmpty(rules)) {
                return self();
            }

            if (Objects.isNull(this.matchingRules)) {
                this.matchingRules = new HashMap<>();
            }

            rules.forEach(rule -> {
                if (Objects.nonNull(rule)) {
                    this.matchingRules.computeIfAbsent(set, k -> new ArrayList<>())
                            .add(rule);
                }
            });

            return self();
        }

        /**
         * Sets matching execution mode.
         *
         * @param mode matching execution mode
         * @return self
         */
        public MatchingContextBuilder executionMode(MatchingExecutionMode mode) {
            this.executionMode = mode;

            return self();
        }

        /**
         * Sets 'skipResultCalculation' flag
         *
         * @param skipResultCalculation the 'skipResultCalculation' to set
         * @return self
         */
        public MatchingContextBuilder skipResultCalculation(boolean skipResultCalculation) {
            this.skipResultCalculation = skipResultCalculation;

            return self();
        }

        /**
         * Sets 'skipClusterUpdates' flag.
         *
         * @param skipClusterUpdates the 'skipClusterUpdates' to set
         * @return self
         */
        public MatchingContextBuilder skipClusterUpdates(boolean skipClusterUpdates) {
            this.skipClusterUpdates = skipClusterUpdates;
            return self();
        }

        /**
         * Sets 'cleanClusterStorage' flag.
         *
         * @param cleanClusterStorage the 'cleanClusterStorage' flag
         * @return self
         */
        public MatchingContextBuilder cleanClusterStorage(boolean cleanClusterStorage) {
            this.cleanClusterStorage = cleanClusterStorage;
            return self();
        }

        /**
         * {@inheritDoc}
         */
        public MatchingContext build() {
            return new MatchingContext(this);
        }
    }
}
