/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.exception;

import org.unidata.mdm.system.exception.DomainId;
import org.unidata.mdm.system.exception.ExceptionId;
import org.unidata.mdm.system.exception.PlatformValidationException;
import org.unidata.mdm.system.exception.ValidationResult;

import java.util.Collection;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class MatchingModelValidationException extends PlatformValidationException {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 5546259444568469563L;

    /**
     * This exception domain.
     */
    private static final DomainId MATCHING_MODEL_VALIDATION_EXCEPTION = () -> "MATCHING_MODEL_VALIDATION_EXCEPTION";

    /**
     * Constructor.
     *
     * @param message          the message
     * @param id               the id
     * @param validationResult validation result
     * @param args             the arguments
     */
    public MatchingModelValidationException(String message, ExceptionId id,
                                            Collection<ValidationResult> validationResult, Object... args) {
        super(message, id, validationResult, args);
    }

    /**
     * Constructor.
     *
     * @param message          the message
     * @param cause            the exception cause
     * @param id               exception id
     * @param validationResult validation elements
     * @param args             arguments
     */
    public MatchingModelValidationException(String message, Throwable cause, ExceptionId id,
                                            Collection<ValidationResult> validationResult, Object... args) {
        super(message, cause, id, validationResult, args);
    }

    /**
     * Constructor.
     *
     * @param message           the message
     * @param id                exception id
     * @param validationResults validation elements
     */
    public MatchingModelValidationException(String message, ExceptionId id,
                                            Collection<ValidationResult> validationResults) {
        super(message, id, validationResults);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DomainId getDomain() {
        return MATCHING_MODEL_VALIDATION_EXCEPTION;
    }
}
