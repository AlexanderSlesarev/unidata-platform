/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.algorithm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.core.type.model.source.AbstractCustomPropertiesHolder;

import javax.xml.datatype.XMLGregorianCalendar;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author Sergey Murskiy on 20.06.2021
 */
public class MatchingAlgorithmSource extends AbstractCustomPropertiesHolder<MatchingAlgorithmSource> {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -8234884446073656240L;

    @JacksonXmlProperty(isAttribute = true, localName = "name")
    private String name;

    @JacksonXmlProperty(isAttribute = true, localName = "displayName")
    private String displayName;

    @JacksonXmlProperty(isAttribute = true, localName = "description")
    private String description;

    @JacksonXmlProperty(isAttribute = true, localName = "matchingStorageId")
    private String matchingStorageId;

    @JacksonXmlProperty(isAttribute = true)
    private String libraryName;

    @JacksonXmlProperty(isAttribute = true)
    private String libraryVersion;

    @JacksonXmlProperty(isAttribute = true)
    private String javaClass;

    @JacksonXmlProperty(isAttribute = true)
    private OffsetDateTime createDate;

    @JacksonXmlProperty(isAttribute = true)
    private String createdBy;

    @JacksonXmlProperty(isAttribute = true)
    private OffsetDateTime updateDate;

    @JacksonXmlProperty(isAttribute = true)
    private String updatedBy;

    @JacksonXmlElementWrapper(localName = "parameters")
    @JacksonXmlProperty(localName = "parameter")
    private List<AlgorithmParamConfigurationSource> parameters;

    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore
    private transient String note;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore
    private transient boolean system;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore
    private transient boolean ready;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore
    private transient boolean configurable;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore
    private transient boolean editable;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore
    private transient Supplier<String> displayNameSupplier;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    @JsonIgnore
    private transient Supplier<String> descriptionSupplier;

    /**
     * Gets the value of the functionName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the functionName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setName(String value) {
        this.name = value;
    }

    public String getDisplayName() {
        return Objects.nonNull(displayNameSupplier) ? displayNameSupplier.get() : displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    /**
     * @param displayNameSupplier the displayNameSupplier to set
     */
    public void setDisplayName(Supplier<String> displayNameSupplier) {
        this.displayNameSupplier = displayNameSupplier;
    }

    /**
     * Gets the value of the description property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescription() {
        return Objects.nonNull(descriptionSupplier) ? descriptionSupplier.get() : description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * @param descriptionSupplier the descriptionSupplier to set
     */
    public void setDescription(Supplier<String> descriptionSupplier) {
        this.descriptionSupplier = descriptionSupplier;
    }

    /**
     * @return the matching storage
     */
    public String getMatchingStorageId() {
        return matchingStorageId;
    }

    /**
     * @param matchingStorageId the matching storage to set
     */
    public void setMatchingStorageId(String matchingStorageId) {
        this.matchingStorageId = matchingStorageId;
    }

    /**
     * @return the libraryName
     */
    public String getLibraryName() {
        return libraryName;
    }

    /**
     * @param libraryName the libraryName to set
     */
    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    /**
     * @return the libraryVersion
     */
    public String getLibraryVersion() {
        return libraryVersion;
    }

    /**
     * @param libraryVersion the libraryVersion to set
     */
    public void setLibraryVersion(String libraryVersion) {
        this.libraryVersion = libraryVersion;
    }

    /**
     * @return the java class
     */
    public String getJavaClass() {
        return javaClass;
    }

    /**
     * @param javaClass the java class to set
     */
    public void setJavaClass(String javaClass) {
        this.javaClass = javaClass;
    }

    /**
     * @return algorithm parameters
     */
    public List<AlgorithmParamConfigurationSource> getParameters() {
        if (Objects.isNull(parameters)) {
            parameters = new ArrayList<>();
        }

        return parameters;
    }

    /**
     * @param parameters algorithm parameters to set
     */
    public void setParameters(List<AlgorithmParamConfigurationSource> parameters) {
        this.parameters = parameters;
    }

    /**
     * Gets the value of the createdAt property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public OffsetDateTime getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createdAt property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setCreateDate(OffsetDateTime value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the createdBy property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the updatedAt property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public OffsetDateTime getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the value of the updatedAt property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setUpdateDate(OffsetDateTime value) {
        this.updateDate = value;
    }

    /**
     * Gets the value of the updatedBy property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the value of the updatedBy property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setUpdatedBy(String value) {
        this.updatedBy = value;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the system
     */
    public boolean isSystem() {
        return system;
    }

    /**
     * @param system the system to set
     */
    public void setSystem(boolean system) {
        this.system = system;
    }

    /**
     * @return the ready
     */
    public boolean isReady() {
        return ready;
    }

    /**
     * @param ready the ready to set
     */
    public void setReady(boolean ready) {
        this.ready = ready;
    }

    /**
     * @return the configurable
     */
    public boolean isConfigurable() {
        return configurable;
    }

    /**
     * @param configurable the configurable to set
     */
    public void setConfigurable(boolean configurable) {
        this.configurable = configurable;
    }

    /**
     * @return the editable
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * @param editable the editable to set
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public MatchingAlgorithmSource withName(String value) {
        setName(value);
        return self();
    }

    public MatchingAlgorithmSource withDisplayName(String value) {
        setDisplayName(value);
        return self();
    }

    public MatchingAlgorithmSource withDisplayName(Supplier<String> value) {
        setDisplayName(value);
        return self();
    }

    public MatchingAlgorithmSource withDescription(String value) {
        setDescription(value);
        return self();
    }

    public MatchingAlgorithmSource withDescription(Supplier<String> value) {
        setDescription(value);
        return self();
    }

    public MatchingAlgorithmSource withMatchingStorageId(String value) {
        setMatchingStorageId(value);
        return self();
    }

    public MatchingAlgorithmSource withLibraryName(String value) {
        setLibraryName(value);
        return self();
    }

    public MatchingAlgorithmSource withLibraryVersion(String value) {
        setLibraryVersion(value);
        return self();
    }

    public MatchingAlgorithmSource withJavaClass(String value) {
        setJavaClass(value);
        return self();
    }

    public MatchingAlgorithmSource withUpdateDate(OffsetDateTime value) {
        setUpdateDate(value);
        return self();
    }

    public MatchingAlgorithmSource withCreateDate(OffsetDateTime value) {
        setCreateDate(value);
        return self();
    }

    public MatchingAlgorithmSource withUpdatedBy(String value) {
        setUpdatedBy(value);
        return self();
    }

    public MatchingAlgorithmSource withCreatedBy(String value) {
        setCreatedBy(value);
        return self();
    }

    public MatchingAlgorithmSource withParameters(AlgorithmParamConfigurationSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withParameters(Arrays.asList(values));
        }

        return self();
    }

    public MatchingAlgorithmSource withParameters(Collection<AlgorithmParamConfigurationSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getParameters().addAll(values);
        }

        return self();
    }

    public MatchingAlgorithmSource withNote(String value) {
        setNote(value);
        return self();
    }

    public MatchingAlgorithmSource withSystem(boolean value) {
        setSystem(value);
        return self();
    }

    public MatchingAlgorithmSource withReady(boolean value) {
        setReady(value);
        return self();
    }

    public MatchingAlgorithmSource withConfigurable(boolean value) {
        setConfigurable(value);
        return self();
    }

    public MatchingAlgorithmSource withEditable(boolean value) {
        setEditable(value);
        return self();
    }
}
