/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.data.field;

import org.unidata.mdm.core.type.data.CharacterLargeValue;
import org.unidata.mdm.matching.core.type.data.MatchingDataType;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 16.08.2021
 */
public class ClobMatchingRecordFieldImpl extends AbstractMatchingRecordField<CharacterLargeValue> {
    /**
     * Special serialization constructor. Should not be used otherwise.
     */
    protected ClobMatchingRecordFieldImpl() {
        super();
    }

    /**
     * Constructor.
     *
     * @param name the name
     */
    public ClobMatchingRecordFieldImpl(String name) {
        super(name);
    }

    /**
     * Constructor.
     *
     * @param name  the name
     * @param value the value
     */
    public ClobMatchingRecordFieldImpl(String name, CharacterLargeValue value) {
        super(name, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingDataType getType() {
        return MatchingDataType.CLOB;
    }

    /**
     * Fluent part for compatibility.
     *
     * @param value the value
     * @return self
     */
    public ClobMatchingRecordFieldImpl withValue(CharacterLargeValue value) {
        setValue(value);
        return this;
    }

    /**
     * @return hash code
     */
    @Override
    public int hashCode() {
        CharacterLargeValue cv = getValue();
        return Objects.hash(MatchingDataType.CLOB,
                cv != null ? cv.getFileName() : null,
                cv != null ? cv.getSize() : null,
                cv != null ? cv.getMimeType() : null,
                cv != null ? cv.getId() : null,
                cv != null ? cv.getAcceptance() : null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof MatchingRecordField)) {
            return false;
        }

        MatchingRecordField<?> other = (MatchingRecordField<?>) obj;
        if (getType() != other.getType()) {
            return false;
        }

        @SuppressWarnings("unchecked")
        MatchingRecordField<CharacterLargeValue> target = (MatchingRecordField<CharacterLargeValue>) other;

        CharacterLargeValue cv1 = getValue();
        Object[] thisAttrs = {
                cv1 != null ? cv1.getFileName() : null,
                cv1 != null ? cv1.getSize() : null,
                cv1 != null ? cv1.getMimeType() : null,
                cv1 != null ? cv1.getId() : null,
                cv1 != null ? cv1.getAcceptance() : null
        };

        CharacterLargeValue cv2 = target.getValue();
        Object[] thatAttrs = {
                cv2 != null ? cv2.getFileName() : null,
                cv2 != null ? cv2.getSize() : null,
                cv2 != null ? cv2.getMimeType() : null,
                cv2 != null ? cv2.getId() : null,
                cv2 != null ? cv2.getAcceptance() : null
        };

        return Arrays.equals(thisAttrs, thatAttrs);
    }
}
