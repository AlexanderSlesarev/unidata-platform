/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.unidata.mdm.matching.core.dao.MatchingStorageDAO;
import org.unidata.mdm.matching.core.po.MatchingStoragePO;
import org.unidata.mdm.matching.core.type.storage.MatchingStorageStatus;
import org.unidata.mdm.system.dao.impl.BaseDAOImpl;

import javax.sql.DataSource;
import java.util.Collection;
import java.util.Properties;

/**
 * @author Sergey Murskiy on 07.09.2021
 */
@Repository
public class MatchingStorageDAOImpl extends BaseDAOImpl implements MatchingStorageDAO {

    /**
     * SQLs.
     */
    private final String fetchAllSQL;
    private final String saveSQL;

    /**
     * Default row mapper.
     */
    private static final RowMapper<MatchingStoragePO> DEFAULT_ROW_MAPPER = (rs, row) -> {

        MatchingStoragePO result = new MatchingStoragePO();
        result.setId(rs.getString(MatchingStoragePO.FIELD_ID));
        result.setStatus(MatchingStorageStatus.valueOf(rs.getString(MatchingStoragePO.FIELD_STATUS)));

        return result;
    };


    /**
     * Constructor.
     */
    @Autowired
    public MatchingStorageDAOImpl(@Qualifier("matchingDataSource") final DataSource dataSource,
                                  @Qualifier("matching-storage-sql") final Properties sql) {
        super(dataSource);

        fetchAllSQL = sql.getProperty("fetchAllSQL");
        saveSQL = sql.getProperty("saveSQL");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveMatchingStoragesInfo(Collection<MatchingStoragePO> storagesInfo) {
        getJdbcTemplate().batchUpdate(saveSQL, storagesInfo, storagesInfo.size(), (ps, po) -> {
            ps.setString(1, po.getId());
            ps.setString(2, po.getStatus().name());
            ps.setString(3, po.getStatus().name());
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MatchingStoragePO> fetchMatchingStoragesInfo() {
        return getJdbcTemplate().query(fetchAllSQL, DEFAULT_ROW_MAPPER);
    }
}
