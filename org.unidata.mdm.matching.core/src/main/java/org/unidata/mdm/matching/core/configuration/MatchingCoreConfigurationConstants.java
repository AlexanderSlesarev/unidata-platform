/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.configuration;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public final class MatchingCoreConfigurationConstants {
    /**
     * Constructor.
     */
    private MatchingCoreConfigurationConstants() {
        super();
    }

    /**
     * Schema name.
     */
    public static final String MATCHING_CORE_SCHEMA_NAME = "org_unidata_mdm_matching_core";
    /**
     * Change log table name.
     */
    public static final String MATCHING_CORE_LOG_NAME = "change_log";
    /**
     * Cluster 'ensure indexes' configuration action name.
     */
    public static final String CLUSTER_ENSURE_INDEXES_ACTION_NAME = "cluster-ensure-indexes";
    /**
     * Default cluster index name.
     */
    public static final String DEFAULT_CLUSTER_INDEX_NAME = "clusters";
}
