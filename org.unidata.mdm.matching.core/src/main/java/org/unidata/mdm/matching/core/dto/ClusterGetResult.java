/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dto;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.cluster.Cluster;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Sergey Murskiy on 20.07.2021
 */
public class ClusterGetResult {
    /**
     * Clusters.
     */
    private List<Cluster> clusters;

    /**
     * Constructor.
     *
     * @param clusters clusters to set
     */
    public ClusterGetResult(Collection<Cluster> clusters) {
        this.clusters = new ArrayList<>(clusters);
    }

    /**
     * Constructor.
     *
     * @param cluster cluster to set
     */
    public ClusterGetResult(Cluster cluster) {
        this.clusters = Collections.singletonList(cluster);
    }

    /**
     * Constructor.
     */
    public ClusterGetResult() {
    }

    /**
     * Gets clusters.
     *
     * @return clusters
     */
    public List<Cluster> getClusters() {
        if (CollectionUtils.isEmpty(clusters)) {
            return Collections.emptyList();
        }

        return clusters;
    }

    /**
     * Sets clusters.
     *
     * @param clusters clusters to set
     */
    public void setCluster(List<Cluster> clusters) {
        this.clusters = clusters;
    }

    /**
     * Is empty result.
     *
     * @return is empty result
     */
    public boolean isEmpty() {
        return CollectionUtils.isEmpty(clusters);
    }

    /**
     * Gets first value.
     *
     * @return cluster
     */
    public Cluster singleValue() {
        return CollectionUtils.isNotEmpty(clusters)
                ? clusters.get(0)
                : null;
    }
}
