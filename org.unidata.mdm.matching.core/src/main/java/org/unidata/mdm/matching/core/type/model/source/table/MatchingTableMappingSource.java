/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.table;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 15.06.2021
 */
public class MatchingTableMappingSource implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 5930241092924109110L;

    @JacksonXmlProperty(isAttribute = true, localName = "matchingTableName")
    private String matchingTableName;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "mapping")
    private List<MatchingColumnMappingSource> mappings;

    /**
     * Gets the matching table name.
     *
     * @return the matching table name
     */
    public String getMatchingTableName() {
        return matchingTableName;
    }

    /**
     * Sets the matching table name.
     *
     * @param matchingTableName the matching table name to set
     */
    public void setMatchingTableName(String matchingTableName) {
        this.matchingTableName = matchingTableName;
    }

    /**
     * Gets matching column mappings.
     *
     * @return matching column mappings
     */
    public List<MatchingColumnMappingSource> getMappings() {
        if (Objects.isNull(mappings)) {
            this.mappings = new ArrayList<>();
        }

        return mappings;
    }

    /**
     * Sets matching column mappings.
     *
     * @param mappings matching column mappings to set
     */
    public void setMappings(List<MatchingColumnMappingSource> mappings) {
        this.mappings = mappings;
    }

    /**
     * Matching table mapping with table name.
     *
     * @param matchingTableName the matching table name to set
     * @return self
     */
    public MatchingTableMappingSource withMatchingTableName(String matchingTableName) {
        setMatchingTableName(matchingTableName);
        return this;
    }

    /**
     * Matching table mapping with column mappings.
     *
     * @param mappings matching column mappings to set
     * @return self
     */
    public MatchingTableMappingSource withMappings(MatchingColumnMappingSource... mappings) {
        if (ArrayUtils.isNotEmpty(mappings)) {
            return withMappings(Arrays.asList(mappings));
        }

        return this;
    }

    /**
     * Matching table mapping with column mappings.
     *
     * @param mappings matching column mappings to set
     * @return self
     */
    public MatchingTableMappingSource withMappings(Collection<MatchingColumnMappingSource> mappings) {
        if (CollectionUtils.isNotEmpty(mappings)) {
            getMappings().addAll(mappings);
        }

        return this;
    }
}
