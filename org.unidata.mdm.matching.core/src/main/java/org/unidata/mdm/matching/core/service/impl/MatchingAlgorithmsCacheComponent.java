/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.context.UserLibraryGetContext;
import org.unidata.mdm.core.dto.UserLibraryResult;
import org.unidata.mdm.core.service.UserLibraryService;
import org.unidata.mdm.core.type.libraries.LibraryMimeType;
import org.unidata.mdm.core.type.libraries.UserLibrariesListener;
import org.unidata.mdm.matching.core.exception.MatchingExceptionIds;
import org.unidata.mdm.matching.core.service.impl.algorithm.JavaMatchingAlgorithmLibrary;
import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithm;
import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithmLibrary;
import org.unidata.mdm.matching.core.type.model.instance.MatchingAlgorithmElement;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.service.ModuleService;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Component
public class MatchingAlgorithmsCacheComponent implements UserLibrariesListener {
    /**
     * A ULS instance.
     */
    private final UserLibraryService userLibraryService;
    /**
     * A MS instance.
     */
    private final ModuleService moduleService;
    /**
     * Local libraries cache.
     */
    private final Map<String, MatchingAlgorithmLibrary> libraries = new ConcurrentHashMap<>();

    /**
     * Constructor.
     */
    @Autowired
    public MatchingAlgorithmsCacheComponent(
            final UserLibraryService userLibraryService,
            final ModuleService moduleService) {
        super();
        this.userLibraryService = userLibraryService;
        this.userLibraryService.addListener(this);
        this.moduleService = moduleService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void libraryUpserted(String storage, String name, String version) {
        // Just remove the library, if it exists,
        // causing it to be loaded once again on the next use.
        libraryRemoved(storage, name, version);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void libraryRemoved(String storage, String name, String version) {

        String id = MatchingAlgorithmLibrary.toName(storage, name, version);
        MatchingAlgorithmLibrary l = libraries.remove(id);

        if (Objects.nonNull(l)) {
            l.cleanup();
        }
    }

    /**
     * Find matching algorithm.
     *
     * @param storageId the storage id
     * @param el        the matching algorithm element
     * @return matching algorithm
     */
    @Nullable
    public Pair<MatchingAlgorithm, Exception> find(String storageId, MatchingAlgorithmElement el) {

        try {
            String id = MatchingAlgorithmLibrary.toName(storageId, el.getLibrary(), el.getVersion());
            MatchingAlgorithmLibrary cfl = libraries.computeIfAbsent(id, k -> load(storageId, el.getLibrary(),
                    el.getVersion()));

            return Pair.of(cfl.lookup(el), null);
        } catch (Exception exc) {
            return Pair.of(null, exc);
        }
    }

    /**
     * Find matching algorithms library.
     *
     * @param storageId the storage id
     * @param library   the library name
     * @param version   the library version
     * @return matching algorithms library
     */
    @Nullable
    public MatchingAlgorithmLibrary find(String storageId, String library, String version) {

        try {
            String id = MatchingAlgorithmLibrary.toName(storageId, library, version);
            return libraries.computeIfAbsent(id, k -> load(storageId, library, version));
        } catch (Exception exc) {
            return null;
        }
    }

    private MatchingAlgorithmLibrary load(String storageId, String libraryName, String libraryVersion) {

        UserLibraryResult ulr = userLibraryService.get(UserLibraryGetContext.builder()
                .storageId(storageId)
                .filename(libraryName)
                .version(libraryVersion)
                .withData(true)
                .build());

        if (Objects.nonNull(ulr) && ulr.getMimeType() == LibraryMimeType.JAR_FILE) {
            return new JavaMatchingAlgorithmLibrary(this.moduleService, ulr);
        }

        throw new PlatformFailureException("Library [{}:{}] not found.",
                MatchingExceptionIds.EX_MATCHING_LIBRARY_NOT_FOUND,
                libraryName, libraryVersion);
    }
}
