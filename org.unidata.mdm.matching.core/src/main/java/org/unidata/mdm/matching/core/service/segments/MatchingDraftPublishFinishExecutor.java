/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.matching.core.service.segments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.context.ModelChangeContext.ModelChangeType;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.service.ModelIdentitySupport;
import org.unidata.mdm.draft.context.DraftPublishContext;
import org.unidata.mdm.draft.dto.DraftPublishResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.matching.core.context.MatchingModelUpsertContext;
import org.unidata.mdm.matching.core.module.MatchingCoreModule;
import org.unidata.mdm.matching.core.type.draft.MatchingModelDraftConstants;
import org.unidata.mdm.matching.core.type.model.MatchingModel;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
@Component(MatchingDraftPublishFinishExecutor.SEGMENT_ID)
public class MatchingDraftPublishFinishExecutor extends Finish<DraftPublishContext, DraftPublishResult> implements ModelIdentitySupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingCoreModule.MODULE_ID + "[MATCHING_DRAFT_PUBLISH_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingCoreModule.MODULE_ID + ".matching.draft.publish.finish.description";

    /**
     * MMS instance.
     */
    @Autowired
    private MetaModelService metaModelService;

    /**
     * Constructor.
     */
    public MatchingDraftPublishFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftPublishResult.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DraftPublishResult finish(DraftPublishContext ctx) {
        DraftPublishResult result = new DraftPublishResult(publish(ctx));
        result.setDraft(ctx.currentDraft());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return DraftPublishContext.class.isAssignableFrom(start.getInputTypeClass());
    }

    protected boolean publish(DraftPublishContext ctx) {

        Draft draft = ctx.currentDraft();
        Edition edition = ctx.currentEdition();

        if (Objects.nonNull(edition) && Objects.nonNull(edition.getContent())) {

            String storageId = draft.getVariables().valueGet(MatchingModelDraftConstants.STORAGE_ID);
            Integer version = draft.getVariables().valueGet(MatchingModelDraftConstants.DRAFT_START_VERSION);

            MatchingModel result = edition.getContent();

            MatchingModelUpsertContext uCtx = MatchingModelUpsertContext.builder()
                .description(result.getDescription())
                .displayName(result.getDisplayName())
                .name(result.getName())
                .algorithmsUpdate(result.getAlgorithms())
                .matchingTablesUpdate(result.getMatchingTables())
                .rulesUpdate(result.getRules())
                .setsUpdate(result.getSets())
                .assignmentsUpdate(result.getAssignments())
                .upsertType(ModelChangeType.FULL)
                .storageId(storageId)
                .version(version + 1)
                .force(ctx.isForce())
                .waitForFinish(true)
                .postponeRefresh(true)
                .build();

            metaModelService.upsert(uCtx);

            ctx.afterTransaction(uCtx.afterTransaction());
        }

        return true;
    }
}
