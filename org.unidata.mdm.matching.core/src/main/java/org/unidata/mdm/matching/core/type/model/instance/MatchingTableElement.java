/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.instance;

import org.unidata.mdm.core.type.model.CreateUpdateElement;
import org.unidata.mdm.core.type.model.CustomPropertiesElement;
import org.unidata.mdm.core.type.model.IdentityElement;
import org.unidata.mdm.core.type.model.NamedDisplayableElement;

import java.util.Collection;

/**
 * @author Sergey Murskiy on 16.06.2021
 */
public interface MatchingTableElement extends NamedDisplayableElement, IdentityElement, CustomPropertiesElement, CreateUpdateElement {
    /**
     * Gets matching table columns.
     *
     * @return table columns
     */
    Collection<MatchingColumnElement> getColumns();

    /**
     * Gets matching column by id.
     *
     * @param id the matching column id
     * @return matching column element
     */
    MatchingColumnElement getColumn(String id);
}
