/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.data.field;

import org.unidata.mdm.core.type.data.BinaryLargeValue;
import org.unidata.mdm.core.type.data.CharacterLargeValue;
import org.unidata.mdm.core.type.data.MeasuredValue;
import org.unidata.mdm.core.type.data.SimpleAttribute;
import org.unidata.mdm.matching.core.type.data.MatchingDataType;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 04.08.2021
 */
public abstract class AbstractMatchingRecordField<T> implements MatchingRecordField<T> {
    /**
     * Field name
     */
    protected String name;
    /**
     * Value.
     */
    protected T value;

    /**
     * Special serialization constructor. Should not be used otherwise.
     */
    protected AbstractMatchingRecordField() {
        super();
    }

    /**
     * Constructor.
     *
     * @param name the field name
     */
    protected AbstractMatchingRecordField(String name) {
        this.name = name;
    }

    /**
     * Constructor.
     *
     * @param name  the field name
     * @param value the field value
     */
    protected AbstractMatchingRecordField(String name, T value) {
        this.name = name;
        this.value = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T getValue() {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setValue(T value) {
        this.value = value;
    }

    /**
     * @see Object#hashCode()
     * Not a good thing. Re-write this crap asap. Introduce solid value identity system instead.
     */
    @Override
    public int hashCode() {
        return Objects.hash(getType(), Objects.toString(getValue()));
    }

    /**
     * @see Object#equals(Object)
     * Not a good thing. Re-write this crap asap. Introduce solid value identity system instead.
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof MatchingRecordField)) {
            return false;
        }

        MatchingRecordField<?> other = (MatchingRecordField<?>) obj;
        if (getType() != other.getType()) {
            return false;
        }

        return Objects.equals(Objects.toString(getValue()), Objects.toString(other.getValue()));
    }

    /**
     * Creates matching record field.
     *
     * @param name      the field name
     * @param attribute the simple attribute
     * @return the field
     */
    public static MatchingRecordField<?> of(String name, SimpleAttribute<?> attribute) {
        MatchingDataType type = MatchingDataType.valueOf(attribute.getDataType().name());

        switch (type) {
            case STRING:
                return new StringMatchingRecordFieldImpl(name, attribute.castValue());
            case DICTIONARY:
                return new DictionaryMatchingRecordFieldImpl(name, attribute.castValue());
            case BLOB:
                return new BlobMatchingRecordFieldImpl(name, attribute.castValue());
            case BOOLEAN:
                return new BooleanMatchingRecordFieldImpl(name, attribute.castValue());
            case CLOB:
                return new ClobMatchingRecordFieldImpl(name, attribute.castValue());
            case DATE:
                return new DateMatchingRecordFieldImpl(name, attribute.castValue());
            case ENUM:
                return new EnumMatchingRecordFieldImpl(name, attribute.castValue());
            case INTEGER:
                return new IntegerMatchingRecordFieldImpl(name, attribute.castValue());
            case LINK:
                return new LinkMatchingRecordFieldImpl(name, attribute.castValue());
            case NUMBER:
                return new NumberMatchingRecordFieldImpl(name, attribute.castValue());
            case MEASURED:
                return new MeasureMatchingRecordFieldImpl(name, (MeasuredValue) attribute.castValue());
            case TIME:
                return new TimeMatchingRecordFieldImpl(name, attribute.castValue());
            case TIMESTAMP:
                return new TimestampMatchingRecordFieldImpl(name, attribute.castValue());
            default:
                return null;
        }
    }

    /**
     * Creates matching record field.
     *
     * @param type  the matching data type
     * @param name  the field name
     * @param value the value
     * @return the field
     */
    public static MatchingRecordField<?> of(MatchingDataType type, String name, Object value) {

        if (Objects.isNull(name) || Objects.isNull(type)) {
            return null;
        }

        switch (type) {
            case STRING:
                return new StringMatchingRecordFieldImpl(name, (String) value);
            case DICTIONARY:
                return new DictionaryMatchingRecordFieldImpl(name, (String) value);
            case BLOB:
                return new BlobMatchingRecordFieldImpl(name, (BinaryLargeValue) value);
            case BOOLEAN:
                return new BooleanMatchingRecordFieldImpl(name, (Boolean) value);
            case CLOB:
                return new ClobMatchingRecordFieldImpl(name, (CharacterLargeValue) value);
            case DATE:
                return new DateMatchingRecordFieldImpl(name, (LocalDate) value);
            case ENUM:
                return new EnumMatchingRecordFieldImpl(name, (String) value);
            case INTEGER:
                return new IntegerMatchingRecordFieldImpl(name, (Long) value);
            case LINK:
                return new LinkMatchingRecordFieldImpl(name, (String) value);
            case NUMBER:
                return new NumberMatchingRecordFieldImpl(name, (Double) value);
            case MEASURED:
                return new MeasureMatchingRecordFieldImpl(name, (MeasuredValue) value);
            case TIME:
                return new TimeMatchingRecordFieldImpl(name, (LocalTime) value);
            case TIMESTAMP:
                return new TimestampMatchingRecordFieldImpl(name, (LocalDateTime) value);
            default:
                return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "(name = " + getName() + ", value = " + value + ")";
    }
}
