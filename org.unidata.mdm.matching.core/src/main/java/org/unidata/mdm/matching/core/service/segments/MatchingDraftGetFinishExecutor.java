/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.matching.core.service.segments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.UPathService;
import org.unidata.mdm.draft.context.DraftGetContext;
import org.unidata.mdm.draft.dto.DraftGetResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.matching.core.module.MatchingCoreModule;
import org.unidata.mdm.matching.core.service.impl.MatchingAlgorithmsCacheComponent;
import org.unidata.mdm.matching.core.service.impl.MatchingModelComponent;
import org.unidata.mdm.matching.core.service.impl.instance.MatchingModelInstanceImpl;
import org.unidata.mdm.matching.core.type.model.MatchingModel;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
@Component(MatchingDraftGetFinishExecutor.SEGMENT_ID)
public class MatchingDraftGetFinishExecutor extends Finish<DraftGetContext, DraftGetResult> {

    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingCoreModule.MODULE_ID + "[MATCHING_DRAFT_GET_FINISH]";

    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingCoreModule.MODULE_ID + ".matching.get.finish.description";

    /**
     * UPath service.
     */
    @Autowired
    private UPathService upathService;
    /**
     * Cache component.
     */
    @Autowired
    private MatchingAlgorithmsCacheComponent matchingAlgorithmsCacheComponent;
    /**
     * The matching model component.
     */
    @Autowired
    private MatchingModelComponent matchingModelComponent;

    /**
     * Constructor.
     */
    public MatchingDraftGetFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftGetResult.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DraftGetResult finish(DraftGetContext ctx) {

        Draft draft = ctx.currentDraft();
        Edition current = ctx.currentEdition();

        MatchingModel m =  Objects.isNull(current)
                ? new MatchingModel().withVersion(0)
                : current.getContent();

        MatchingModelInstance i = new MatchingModelInstanceImpl(m, upathService, matchingAlgorithmsCacheComponent);

        DraftGetResult result = new DraftGetResult();
        result.setDraft(draft);
        result.setPayload(matchingModelComponent.disassemble(i, ctx.getPayload()));

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return DraftGetContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
