/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dto;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.storage.MatchingStorageInfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Sergey Murskiy on 09.09.2021
 */
public class MatchingStorageInfoGetResult {
    /**
     * Matching storages.
     */
    private List<MatchingStorageInfo> matchingStorages;

    /**
     * Constructor.
     *
     * @param matchingStorages matching storages to set
     */
    public MatchingStorageInfoGetResult(Collection<MatchingStorageInfo> matchingStorages) {
        this.matchingStorages = new ArrayList<>(matchingStorages);
    }

    /**
     * Constructor.
     *
     * @param matchingStorage matching storage to set
     */
    public MatchingStorageInfoGetResult(MatchingStorageInfo matchingStorage) {
        this.matchingStorages = Collections.singletonList(matchingStorage);
    }

    /**
     * Constructor.
     */
    public MatchingStorageInfoGetResult() {
    }

    /**
     * Gets matching storages.
     *
     * @return matching storages
     */
    public List<MatchingStorageInfo> getMatchingStorages() {
        if (CollectionUtils.isEmpty(matchingStorages)) {
            return Collections.emptyList();
        }

        return matchingStorages;
    }

    /**
     * Sets matching storages.
     *
     * @param matchingStorages matching storages to set
     */
    public void setCluster(List<MatchingStorageInfo> matchingStorages) {
        this.matchingStorages = matchingStorages;
    }

    /**
     * Is empty result.
     *
     * @return is empty result
     */
    public boolean isEmpty() {
        return CollectionUtils.isEmpty(matchingStorages);
    }

    /**
     * Gets first value.
     *
     * @return matching storage
     */
    public MatchingStorageInfo singleValue() {
        return CollectionUtils.isNotEmpty(matchingStorages)
                ? matchingStorages.get(0)
                : null;
    }
}
