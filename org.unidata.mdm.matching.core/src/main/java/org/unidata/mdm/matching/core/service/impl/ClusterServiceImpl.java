/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.unidata.mdm.matching.core.configuration.MatchingCoreConfigurationConstants;
import org.unidata.mdm.matching.core.context.ClusterDeleteContext;
import org.unidata.mdm.matching.core.context.ClusterGetContext;
import org.unidata.mdm.matching.core.context.ClusterSearchContext;
import org.unidata.mdm.matching.core.context.ClusterUpdateContext;
import org.unidata.mdm.matching.core.converter.ClusterConverter;
import org.unidata.mdm.matching.core.dto.ClusterGetResult;
import org.unidata.mdm.matching.core.service.ClusterService;
import org.unidata.mdm.matching.core.type.cluster.Cluster;
import org.unidata.mdm.matching.core.type.cluster.ClusterRecord;
import org.unidata.mdm.matching.core.type.cluster.ClusterSearchQuery;
import org.unidata.mdm.matching.core.type.search.ClusterChildIndexType;
import org.unidata.mdm.matching.core.type.search.ClusterHeadIndexType;
import org.unidata.mdm.matching.core.type.search.ClusterHeaderField;
import org.unidata.mdm.matching.core.type.search.ClusterIndexId;
import org.unidata.mdm.matching.core.type.search.ClusterRecordHeaderField;
import org.unidata.mdm.matching.core.type.search.ClusterRecordIndexId;
import org.unidata.mdm.search.configuration.SearchConfigurationConstants;
import org.unidata.mdm.search.context.ComplexSearchRequestContext;
import org.unidata.mdm.search.context.IndexRequestContext;
import org.unidata.mdm.search.context.NestedSearchRequestContext;
import org.unidata.mdm.search.context.SearchRequestContext;
import org.unidata.mdm.search.dto.ComplexSearchResultDTO;
import org.unidata.mdm.search.dto.SearchResultDTO;
import org.unidata.mdm.search.service.SearchService;
import org.unidata.mdm.search.type.form.FieldsGroup;
import org.unidata.mdm.search.type.form.FormField;
import org.unidata.mdm.search.type.id.ManagedIndexId;
import org.unidata.mdm.search.type.indexing.Indexing;
import org.unidata.mdm.search.type.query.SearchQuery;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.util.IdUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 20.07.2021
 */
@Service
public class ClusterServiceImpl implements ClusterService {

    /**
     * Maximum search window size, used in paginated requests.
     */
    @ConfigurationRef(SearchConfigurationConstants.PROPERTY_HITS_LIMIT)
    private ConfigurationValue<Long> maxWindowSize;

    /**
     * Search service.
     */
    @Autowired
    private SearchService searchService;
    /**
     * Cluster converter.
     */
    @Autowired
    private ClusterConverter clusterConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(ClusterUpdateContext ctx) {
        IndexRequestContext indexCtx = IndexRequestContext.builder()
                .entity(MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME)
                .delete(processClusterDeletes(ctx))
                .delete(processClusterRecordDeletes(ctx))
                .index(processClusterIndex(ctx))
                .index(processClusterRecordIndex(ctx))
                .build();

        deleteChildClusterRecords(ctx);
        searchService.process(indexCtx);
    }

    /**
     * {@inheritDoc}
     */
    public void delete(ClusterDeleteContext ctx) {
        if (Objects.isNull(ctx.getQuery())) {
            return;
        }

        SearchRequestContext clustersDeleteRequest =
                SearchRequestContext.builder(MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME)
                        .type(ClusterHeadIndexType.CLUSTER)
                        .query(SearchQuery.formQuery(createClusterFieldsGroup(ctx.getQuery())))
                        .build();

        SearchRequestContext clusterRecordsDeleteRequest =
                SearchRequestContext.builder(MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME)
                        .type(ClusterChildIndexType.CLUSTER_RECORD)
                        .nestedSearch(NestedSearchRequestContext.parent(clustersDeleteRequest).build())
                        .build();

        searchService.deleteFoundResult(clustersDeleteRequest);
        searchService.deleteFoundResult(clusterRecordsDeleteRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ClusterGetResult get(ClusterGetContext ctx) {
        ClusterSearchContext searchCtx = ClusterSearchContext.builder()
                .query(new ClusterSearchQuery()
                        .withClusterIds(ctx.getId()))
                .fetchClusterRecords(ctx.isFetchClusterRecords())
                .build();

        return search(searchCtx);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ClusterGetResult search(ClusterSearchContext ctx) {

        if (Objects.isNull(ctx.getQuery())) {
            return new ClusterGetResult();
        }

        ClusterSearchQuery query = ctx.getQuery();

        SearchRequestContext.SearchRequestContextBuilder main =
                SearchRequestContext.builder(MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME)
                        .returnFields(Arrays.stream(ClusterHeaderField.values())
                                .map(ClusterHeaderField::getName)
                                .collect(Collectors.toList()))
                        .type(ClusterHeadIndexType.CLUSTER)
                        .count(maxWindowSize.getValue().intValue())
                        .joinBy(ClusterHeaderField.FIELD_ID);

        FieldsGroup clusterFieldsGroup = createClusterFieldsGroup(query);

        if (clusterFieldsGroup.isEmpty()) {
            main.fetchAll(true);
        } else {
            main.query(SearchQuery.formQuery(clusterFieldsGroup));
        }

        List<SearchRequestContext> supplementaryContexts = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(query.getRecordsQueries())) {
            SearchRequestContext filterContext =
                    SearchRequestContext.builder(MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME)
                            .type(ClusterChildIndexType.CLUSTER_RECORD)
                            .query(SearchQuery.formQuery(createClusterRecordsFieldsGroup(query)))
                            .count(maxWindowSize.getValue().intValue())
                            .build();

            supplementaryContexts.add(filterContext);
        }

        if (ctx.isFetchClusterRecords()) {
            SearchRequestContext clusterRecordsContext =
                    SearchRequestContext.builder(MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME)
                            .type(ClusterChildIndexType.CLUSTER_RECORD)
                            .fetchAll(true)
                            .returnFields(Arrays.stream(ClusterRecordHeaderField.values())
                                    .map(ClusterRecordHeaderField::getName)
                                    .collect(Collectors.toList()))
                            .joinBy(ClusterRecordHeaderField.FIELD_CLUSTER_ID)
                            .count(maxWindowSize.getValue().intValue())
                            .build();

            supplementaryContexts.add(clusterRecordsContext);
        }

        ComplexSearchResultDTO search = searchService.search(ComplexSearchRequestContext.hierarchical(main.build(),
                supplementaryContexts));

        return extractSearch(search);
    }

    private ClusterGetResult extractSearch(ComplexSearchResultDTO searchResult) {
        SearchResultDTO main = searchResult.getMain();

        Map<String, Cluster> clustersResultMap = main.getHits().stream()
                .map(clusterConverter::clusterFromHit)
                .collect(Collectors.toMap(Cluster::getId, Function.identity()));

        searchResult.getSupplementary().forEach(supplementary -> supplementary.getHits().forEach(hit -> {
            String clusterId = hit.getFieldFirstValue(ClusterRecordHeaderField.FIELD_CLUSTER_ID.getName());

            clustersResultMap.get(clusterId).getClusterRecords().add(clusterConverter.clusterRecordFromHit(hit));
        }));

        return new ClusterGetResult(clustersResultMap.values());
    }

    private List<Indexing> processClusterIndex(ClusterUpdateContext ctx) {
        List<Indexing> result = new ArrayList<>();

        ctx.getClusterUpdate().forEach(cluster -> result.addAll(clusterConverter.toIndexing(cluster)));
        ctx.getClusterInsert().forEach(cluster -> {
            putClusterId(cluster);
            result.addAll(clusterConverter.toIndexing(cluster));
        });

        return result;
    }

    private List<ManagedIndexId> processClusterDeletes(ClusterUpdateContext ctx) {
        List<ManagedIndexId> result = new ArrayList<>(ctx.getClusterDelete().size() + ctx.getClusterUpdate().size());

        result.addAll(ctx.getClusterUpdate().stream()
                .map(cluster -> ClusterIndexId.of(cluster.getId()))
                .collect(Collectors.toList()));

        result.addAll(ctx.getClusterDelete().stream()
                .map(ClusterIndexId::of)
                .collect(Collectors.toList()));

        return result;
    }

    private List<Indexing> processClusterRecordIndex(ClusterUpdateContext ctx) {

        return ctx.getClusterRecordInsert().entrySet().stream()
                .map(e -> clusterConverter.toIndexing(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

    private List<ManagedIndexId> processClusterRecordDeletes(ClusterUpdateContext ctx) {

        return ctx.getClusterRecordDelete().entrySet().stream()
                .map(e -> {
                    ClusterRecord clusterRecord = e.getValue();

                    return ClusterRecordIndexId.of(e.getKey(), clusterRecord.getNamespace(), clusterRecord.getTypeName(),
                            clusterRecord.getSubjectId(), clusterRecord.getValidTo());
                })
                .collect(Collectors.toList());
    }

    private void deleteChildClusterRecords(ClusterUpdateContext ctx) {
        List<String> deletedCLusterIds = new ArrayList<>(ctx.getClusterDelete().size() + ctx.getClusterUpdate().size());

        deletedCLusterIds.addAll(ctx.getClusterDelete());

        List<String> updatedClustersWithRecords = ctx.getClusterUpdate().stream()
                .filter(cluster -> !cluster.getClusterRecords().isEmpty())
                .map(Cluster::getId)
                .collect(Collectors.toList());

        deletedCLusterIds.addAll(updatedClustersWithRecords);

        SearchRequestContext recordsSearchCtx = SearchRequestContext.builder(ClusterChildIndexType.CLUSTER_RECORD)
                .entity(MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME)
                .query(SearchQuery.formQuery(FieldsGroup.and(
                        FormField.exact(ClusterRecordHeaderField.FIELD_CLUSTER_ID, deletedCLusterIds)
                )))
                .build();

        searchService.deleteFoundResult(recordsSearchCtx);
    }

    private void putClusterId(Cluster cluster) {
        cluster.setId(IdUtils.v4String());
    }

    private FieldsGroup createClusterFieldsGroup(ClusterSearchQuery query) {
        FieldsGroup result = FieldsGroup.and();

        if (CollectionUtils.isNotEmpty(query.getClusterIds())) {
            result.add(FormField.exact(ClusterHeaderField.FIELD_ID, query.getClusterIds()));
        }

        if (CollectionUtils.isNotEmpty(query.getRuleNames())) {
            result.add(FormField.exact(ClusterHeaderField.FIELD_RULE_NAME, query.getRuleNames()));
        }

        if (CollectionUtils.isNotEmpty(query.getSetNames())) {
            result.add(FormField.exact(ClusterHeaderField.FIELD_SET_NAME, query.getSetNames()));
        }

        return result;
    }

    private FieldsGroup createClusterRecordsFieldsGroup(ClusterSearchQuery query) {
        FieldsGroup result = FieldsGroup.or();

        query.getRecordsQueries().forEach(q -> {
            FieldsGroup recordGroup = FieldsGroup.and();

            if (Objects.nonNull(q.getNamespace())) {
                recordGroup.add(FormField.exact(ClusterRecordHeaderField.FIELD_NAMESPACE, q.getNamespace()));
            }

            if (Objects.nonNull(q.getTypeName())) {
                recordGroup.add(FormField.exact(ClusterRecordHeaderField.FIELD_TYPE_NAME, q.getTypeName()));
            }

            if (Objects.nonNull(q.getSubjectId())) {
                recordGroup.add(FormField.exact(ClusterRecordHeaderField.FIELD_SUBJECT_ID, q.getSubjectId()));
            }

            if (Objects.nonNull(q.getAsOf())) {
                recordGroup.add(FieldsGroup.and(
                        FormField.range(ClusterRecordHeaderField.FIELD_VALID_FROM, null, q.getAsOf()),
                        FormField.range(ClusterRecordHeaderField.FIELD_VALID_TO, q.getAsOf(), null)));
            } else {
                if (Objects.nonNull(q.getValidFrom()) && Objects.nonNull(q.getValidTo())) {
                    recordGroup.add(FieldsGroup.and(
                            FormField.range(ClusterRecordHeaderField.FIELD_VALID_FROM, q.getValidFrom(), q.getValidFrom()),
                            FormField.range(ClusterRecordHeaderField.FIELD_VALID_TO, q.getValidTo(), q.getValidTo())));
                }
            }

            result.add(recordGroup);
        });

        return result;
    }
}
