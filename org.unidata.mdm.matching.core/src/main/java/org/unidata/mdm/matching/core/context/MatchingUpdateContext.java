/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.matching.core.type.data.MatchingRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 16.08.2021
 */
public class MatchingUpdateContext extends AbstractMatchingContext implements MatchingDeploymentHolderContext {

    /**
     * Matching records ids to delete.
     */
    private final Map<String, List<MatchingRecordKey>> deletes;
    /**
     * Matching records to insert.
     */
    private final Map<String, List<MatchingRecord>> inserts;

    /**
     * Constructor.
     *
     * @param b matching update context builder
     */
    public MatchingUpdateContext(MatchingUpdateContextBuilder b) {
        super(b);

        this.deletes = b.deletes;
        this.inserts = b.inserts;
    }

    /**
     * Gets deletes.
     *
     * @return deletes
     */
    public Map<String, List<MatchingRecordKey>> getDeletes() {
        return MapUtils.emptyIfNull(deletes);
    }

    /**
     * Gets inserts.
     *
     * @return inserts
     */
    public Map<String, List<MatchingRecord>> getInserts() {
        return MapUtils.emptyIfNull(inserts);
    }

    /**
     * Has deletes.
     *
     * @return has deletes
     */
    public boolean hasDeletes() {
        return MapUtils.isNotEmpty(deletes);
    }

    /**
     * Has inserts.
     *
     * @return has inserts
     */
    public boolean hasInserts() {
        return MapUtils.isNotEmpty(inserts);
    }

    /**
     * Tells, whether this builder has collected some updates.
     *
     * @return true if has some, false otherwise
     */
    public boolean hasChanges() {
        return MapUtils.isNotEmpty(deletes) || MapUtils.isNotEmpty(inserts);
    }

    /**
     * Gets builder.
     *
     * @return the builder
     */
    public static MatchingUpdateContextBuilder builder() {
        return new MatchingUpdateContextBuilder();
    }

    /**
     * Builder class.
     */
    public static class MatchingUpdateContextBuilder extends AbstractMatchingContextBuilder<MatchingUpdateContextBuilder> {

        /**
         * Matching records ids to delete.
         */
        private Map<String, List<MatchingRecordKey>> deletes;
        /**
         * Matching records to insert.
         */
        private Map<String, List<MatchingRecord>> inserts;

        /**
         * Adds keys to delete.
         *
         * @param matchingTableName the matching table name
         * @param keys              the matching record keys
         * @return self
         */
        public MatchingUpdateContextBuilder delete(String matchingTableName, MatchingRecordKey... keys) {
            if (ArrayUtils.isEmpty(keys)) {
                return self();
            }

            return delete(Map.of(matchingTableName, Arrays.asList(keys)));
        }

        /**
         * Adds keys to delete.
         *
         * @param matchingTableName the matching table name
         * @param keys              the matching record keys
         * @return self
         */
        public MatchingUpdateContextBuilder delete(String matchingTableName, List<MatchingRecordKey> keys) {

            if (CollectionUtils.isEmpty(keys) || StringUtils.isBlank(matchingTableName)) {
                return self();
            }

            return delete(Map.of(matchingTableName, keys));
        }

        /**
         * Adds keys to delete.
         *
         * @param keys the matching record keys
         * @return self
         */
        public MatchingUpdateContextBuilder delete(Map<String, List<MatchingRecordKey>> keys) {

            if (MapUtils.isEmpty(keys)) {
                return self();
            }

            if (Objects.isNull(deletes)) {
                deletes = new HashMap<>();
            }

            for (Map.Entry<String, List<MatchingRecordKey>> e : keys.entrySet()) {
                for (MatchingRecordKey key : e.getValue()) {
                    if (Objects.isNull(key) || key.isValidKey()) {
                        continue;
                    }

                    deletes.computeIfAbsent(e.getKey(), k -> new ArrayList<>()).add(key);
                }
            }

            return self();
        }

        /**
         * Adds objects to inserts.
         *
         * @param matchingTableName the matching table name
         * @param records           matching records to insert
         * @return self
         */
        public MatchingUpdateContextBuilder insert(String matchingTableName, MatchingRecord... records) {
            if (ArrayUtils.isNotEmpty(records) && StringUtils.isNotBlank(matchingTableName)) {
                return insert(matchingTableName, Arrays.asList(records));
            }

            return self();
        }

        /**
         * Adds objects to inserts.
         *
         * @param matchingTableName the matching table name
         * @param records           matching records to insert
         * @return self
         */
        public MatchingUpdateContextBuilder insert(String matchingTableName, List<MatchingRecord> records) {

            if (StringUtils.isBlank(matchingTableName) || CollectionUtils.isEmpty(records)) {
                return self();
            }

            return insert(Map.of(matchingTableName, records));
        }

        /**
         * Adds objects to inserts.
         *
         * @param inserts the objects to insert
         * @return self
         */
        public MatchingUpdateContextBuilder insert(Map<String, List<MatchingRecord>> inserts) {

            if (MapUtils.isEmpty(inserts)) {
                return self();
            }

            if (Objects.isNull(this.inserts)) {
                this.inserts = new HashMap<>();
            }

            for (Map.Entry<String, List<MatchingRecord>> e : inserts.entrySet()) {
                for (MatchingRecord matchingRecord : e.getValue()) {
                    if (Objects.isNull(matchingRecord) || matchingRecord.isValid()) {
                        continue;
                    }

                    this.inserts.computeIfAbsent(e.getKey(), key -> new ArrayList<>()).add(matchingRecord);
                }
            }

            return self();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public MatchingUpdateContext build() {
            return new MatchingUpdateContext(this);
        }
    }
}
