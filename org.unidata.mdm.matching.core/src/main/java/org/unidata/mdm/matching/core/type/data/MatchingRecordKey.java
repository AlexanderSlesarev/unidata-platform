/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.data;

import java.util.Date;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 16.08.2021
 */
public class MatchingRecordKey {
    /**
     * Namespace.
     */
    private String namespace;
    /**
     * Type name.
     */
    private String typeName;
    /**
     * Subject id.
     */
    private String subjectId;
    /**
     * Valid from.
     */
    private Date validFrom;
    /**
     * Valid to.
     */
    private Date validTo;

    /**
     * Creates matching record key.
     *
     * @param namespace the namespace
     * @param typeName  the type name
     * @param subjectId the subject id
     * @param validFrom the valid from
     * @param validTo   the valid to
     * @return matching record key
     */
    public static MatchingRecordKey of(String namespace, String typeName, String subjectId, Date validFrom,
                                       Date validTo) {
        MatchingRecordKey id = new MatchingRecordKey();

        id.setNamespace(namespace);
        id.setTypeName(typeName);
        id.setSubjectId(subjectId);
        id.setValidFrom(validFrom);
        id.setValidTo(validTo);

        return id;
    }

    /**
     * Gets namespace.
     *
     * @return the namespace
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Sets namespace.
     *
     * @param namespace the namespace to set
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /**
     * Gets type name.
     *
     * @return the type name
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets type name
     *
     * @param typeName the type name to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * Gets subject id.
     *
     * @return the subject id
     */
    public String getSubjectId() {
        return subjectId;
    }

    /**
     * Sets subject id.
     *
     * @param subjectId the subject id
     */
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * Gets valid from.
     *
     * @return the valid from
     */
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * Sets valid from.
     *
     * @param validFrom the valid from to set
     */
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * Gets valid to.
     *
     * @return the valid to
     */
    public Date getValidTo() {
        return validTo;
    }

    /**
     * Sets valid to.
     *
     * @param validTo the valid to to set
     */
    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    /**
     * Is valid key.
     *
     * @return is valid key
     */
    public boolean isValidKey() {
        return Objects.isNull(getNamespace()) || Objects.isNull(getTypeName()) || Objects.isNull(getSubjectId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MatchingRecordKey that = (MatchingRecordKey) o;

        return Objects.equals(namespace, that.namespace) &&
                Objects.equals(typeName, that.typeName) &&
                Objects.equals(subjectId, that.subjectId) &&
                Objects.equals(validFrom, that.validFrom) &&
                Objects.equals(validTo, that.validTo);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(namespace, typeName, subjectId, validFrom, validTo);
    }
}
