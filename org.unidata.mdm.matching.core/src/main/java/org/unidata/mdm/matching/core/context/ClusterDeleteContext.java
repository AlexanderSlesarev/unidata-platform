/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

import org.unidata.mdm.matching.core.type.cluster.ClusterSearchQuery;

/**
 * @author Sergey Murskiy on 06.09.2021
 */
public class ClusterDeleteContext {
    /**
     * Cluster search query.
     */
    private final ClusterSearchQuery query;

    /**
     * Constructor.
     *
     * @param b builder
     */
    public ClusterDeleteContext(ClusterDeleteContextBuilder b) {
        this.query = b.query;
    }

    /**
     * Gets cluster search query.
     *
     * @return cluster search query
     */
    public ClusterSearchQuery getQuery() {
        return query;
    }

    /**
     * Gets builder.
     *
     * @return builder
     */
    public static ClusterDeleteContextBuilder builder() {
        return new ClusterDeleteContextBuilder();
    }

    /**
     * Builder class.
     */
    public static class ClusterDeleteContextBuilder {
        /**
         * Cluster search query.
         */
        private ClusterSearchQuery query;

        /**
         * Sets search query.
         *
         * @param query search query
         * @return self
         */
        public ClusterDeleteContextBuilder query(ClusterSearchQuery query) {
            this.query = query;
            return this;
        }

        /**
         * Build.
         *
         * @return cluster delete context
         */
        public ClusterDeleteContext build() {
            return new ClusterDeleteContext(this);
        }
    }
}
