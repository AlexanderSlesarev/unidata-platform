/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.context.ModelChangeContext;
import org.unidata.mdm.core.context.ModelGetContext;
import org.unidata.mdm.core.context.ModelRefreshContext;
import org.unidata.mdm.core.context.ModelRemoveContext;
import org.unidata.mdm.core.context.ModelSourceContext;
import org.unidata.mdm.core.dto.ModelGetResult;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.service.UPathService;
import org.unidata.mdm.core.service.impl.AbstractModelComponent;
import org.unidata.mdm.core.type.model.ModelDescriptor;
import org.unidata.mdm.core.type.model.ModelImplementation;
import org.unidata.mdm.core.type.model.ModelSource;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.draft.context.DraftGetContext;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.dto.DraftGetResult;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.matching.core.configuration.MatchingDescriptors;
import org.unidata.mdm.matching.core.configuration.MatchingModelIds;
import org.unidata.mdm.matching.core.context.MatchingModelGetContext;
import org.unidata.mdm.matching.core.context.MatchingModelUpsertContext;
import org.unidata.mdm.matching.core.dao.MatchingModelDAO;
import org.unidata.mdm.matching.core.dto.AlgorithmsModelGetResult;
import org.unidata.mdm.matching.core.dto.AssignmentsModelGetResult;
import org.unidata.mdm.matching.core.dto.MatchingModelGetResult;
import org.unidata.mdm.matching.core.dto.MatchingTablesModelGetResult;
import org.unidata.mdm.matching.core.dto.RulesModelGetResult;
import org.unidata.mdm.matching.core.dto.SetsModelGetResult;
import org.unidata.mdm.matching.core.exception.MatchingExceptionIds;
import org.unidata.mdm.matching.core.exception.MatchingModelValidationException;
import org.unidata.mdm.matching.core.exception.MatchingRuntimeException;
import org.unidata.mdm.matching.core.po.MatchingModelPO;
import org.unidata.mdm.matching.core.serialization.MatchingSerializer;
import org.unidata.mdm.matching.core.service.impl.instance.MatchingAlgorithmImpl;
import org.unidata.mdm.matching.core.service.impl.instance.MatchingModelInstanceImpl;
import org.unidata.mdm.matching.core.service.impl.instance.MatchingRuleImpl;
import org.unidata.mdm.matching.core.service.impl.instance.MatchingRuleSetImpl;
import org.unidata.mdm.matching.core.service.impl.instance.MatchingTableImpl;
import org.unidata.mdm.matching.core.service.impl.instance.NamespaceAssignmentImpl;
import org.unidata.mdm.matching.core.type.draft.MatchingModelDraftConstants;
import org.unidata.mdm.matching.core.type.model.MatchingModel;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.matching.core.type.model.source.algorithm.MatchingAlgorithmSource;
import org.unidata.mdm.matching.core.type.model.source.assignment.NamespaceAssignmentSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSetSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableSource;
import org.unidata.mdm.matching.core.util.MatchingUtils;
import org.unidata.mdm.system.exception.ValidationResult;
import org.unidata.mdm.system.service.AfterModuleStartup;

/**
 * @author Sergey Murskiy on 14.06.2021
 */
@Component(MatchingModelIds.MATCHING)
public class MatchingModelComponent extends AbstractModelComponent implements ModelImplementation<MatchingModelInstance>, AfterModuleStartup {
    /**
     * The MMS.
     */
    private final MetaModelService metaModelService;
    /**
     * The draft service.
     */
    @Autowired
    private DraftService draftService;
    /**
     * UPath service.
     */
    @Autowired
    private UPathService upathService;
    /**
     * Cache component.
     */
    @Autowired
    private MatchingAlgorithmsCacheComponent matchingAlgorithmsCacheComponent;
    /**
     * Matching validation component.
     */
    @Autowired
    private MatchingModelValidationComponent matchingModelValidationComponent;
    /**
     * Matching DAO.
     */
    @Autowired
    private MatchingModelDAO matchingModelDAO;

    /**
     * Model instances, keyed by storage ID.
     */
    private final Map<String, MatchingModelInstance> instances = new ConcurrentHashMap<>(4);

    /**
     * Constructor.
     */
    public MatchingModelComponent(MetaModelService metaModelService) {
        super();
        this.metaModelService = metaModelService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterModuleStartup() {
        metaModelService.register(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ModelDescriptor<MatchingModelInstance> descriptor() {
        return MatchingDescriptors.MATCHING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingModelInstance instance(String storageId, String id) {
        return instances.computeIfAbsent(storageId, this::load);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ModelGetResult get(ModelGetContext ctx) {

        MatchingModelGetContext get = (MatchingModelGetContext) ctx;

        if (get.isDraftOperation()) {

            Long draftId = get.getDraftId();
            Long parentDraftId = get.getParentDraftId();

            DraftGetResult result = draftService.get(DraftGetContext.builder()
                    .draftId(draftId)
                    .parentDraftId(parentDraftId)
                    .payload(get)
                    .build());

            return result.hasPayload() ? result.narrow() : null;
        }

        return disassemble(instance(SecurityUtils.getStorageId(get), null), get);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void upsert(@NonNull ModelChangeContext ctx) {

        MatchingModelUpsertContext change = (MatchingModelUpsertContext) ctx;

        // 1. Process draft and return
        if (change.isDraftOperation()) {

            Long draftId = change.getDraftId();
            Long parentDraftId = change.getParentDraftId();

            draftService.upsert(DraftUpsertContext.builder()
                    .draftId(draftId)
                    .parentDraftId(parentDraftId)
                    .payload(change)
                    .build());

            return;
        }

        // 2. Run direct upsert
        MatchingModel target = assemble(change);

        List<ValidationResult> validations = new ArrayList<>(validate(target));

        if (CollectionUtils.isNotEmpty(validations)) {
            throw new MatchingModelValidationException("Cannot upsert matching model. Validation errors exist.",
                    MatchingExceptionIds.EX_MATCHING_UPSERT_MATCHING_MODEL_VALIDATION, validations);
        }

        put(SecurityUtils.getStorageId(change), target, change.force());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingModel assemble(@NonNull ModelChangeContext ctx) {

        MatchingModelUpsertContext change = (MatchingModelUpsertContext) ctx;
        String storageId = SecurityUtils.getStorageId(change);

        MatchingModelInstance current = instance(storageId, null);

        MatchingModel source;
        MatchingModel target;
        int next;

        // 1. Processing draft
        if (change.isDraftOperation()) {

            // Must be set!
            Draft draft = change.currentDraft();

            // 1.2. Load last edition
            Edition latest = null;
            if (draft.isExisting()) {
                latest = draftService.current(draft.getDraftId(), true);
            }

            // 1.3. Take data snapshot for new drafts, if no editions exist
            if (Objects.isNull(latest)) {
                source = current.toSource();
                next = current.getVersion() + 1;
                // 1.4. Use previous revision data otherwise
            } else {
                source = latest.getContent();
                next = draft.getVariables().<Integer>valueGet(MatchingModelDraftConstants.DRAFT_START_VERSION) + 1;
            }

        // 2. Direct upsert
        } else {
            source = change.getUpsertType() == ModelChangeContext.ModelChangeType.FULL ? null : current.toSource();
            next = Objects.nonNull(change.getVersion()) ? change.getVersion() : current.getVersion() + 1;
        }

        target = new MatchingModel()
                .withVersion(next)
                .withStorageId(storageId)
                .withCreateDate(OffsetDateTime.now())
                .withCreatedBy(SecurityUtils.getCurrentUserName());

        // 3. Process
        if (change.getUpsertType() == ModelChangeContext.ModelChangeType.FULL) {
            target
                    .withAlgorithms(change.getAlgorithmsUpdate())
                    .withMatchingTables(change.getMatchingTablesUpdate())
                    .withRules(change.getRulesUpdate())
                    .withSets(change.getSetsUpdate())
                    .withAssignments(change.getAssignmentsUpdate());
        } else if (Objects.nonNull(source)) {
            merge(target, source, change);
        }

        // 4. Info fields
        processInfoFields(target, source, change);

        // 5. Pre-check before validation to detect serious violations, rendering model unusable.
        Collection<ValidationResult> validations = matchingModelValidationComponent.precheck(target);
        if (CollectionUtils.isNotEmpty(validations)) {
            throw new MatchingModelValidationException("Cannot save matching model. Pre-check validation errors exist.",
                    MatchingExceptionIds.EX_MATCHING_UPSERT_MATCHING_MODEL_CONSISTENCY, validations);
        }

        return target;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<ValidationResult> validate(@NonNull ModelSource source) {
        return matchingModelValidationComponent.validate((MatchingModel) source);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<ValidationResult> allow(@NonNull ModelSourceContext<?> source) {
        return matchingModelValidationComponent.allow(source);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh(ModelRefreshContext refresh) {
        instances.computeIfPresent(SecurityUtils.getStorageId(refresh), (k, v) -> load(k));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(ModelRemoveContext remove) {
        // Not supported
    }

    /**
     * Puts a model to storage.
     *
     * @param storageId the target storage id
     * @param src       the model source
     * @param force     force to latest version upon failure
     */
    public void put(String storageId, MatchingModel source, boolean force) {

        Objects.requireNonNull(storageId, "Storage id must not be null.");

        // 1. Lock revisions
        matchingModelDAO.lock();

        // 2. Do stuff
        MatchingModelPO po = new MatchingModelPO();
        po.setCreatedBy(SecurityUtils.getCurrentUserName());
        po.setStorageId(storageId);
        po.setRevision(source.getVersion());
        po.setContent(MatchingSerializer.modelToCompressedXml(source));

        // 3. Get the latest revision and compare with supplied
        int latest = matchingModelDAO.latest(storageId);
        int next = latest + 1;

        // 4. If force is not specified just fail the upsert. Attempt to save otherwise.
        if (po.getRevision() != next) {

            if (force) {

                source.withVersion(next);

                po.setRevision(next);
                po.setContent(MatchingSerializer.modelToCompressedXml(source));

            } else {

                throw new MatchingRuntimeException(
                        "Cannot save matching model due to concurrent modifications. Revisions conflict [expected next {}].",
                        MatchingExceptionIds.EX_MATCHING_UPSERT_MATCHING_MODEL_REVISION_EXISTS,
                        next);
            }
        }

        matchingModelDAO.save(po);
    }

    /**
     * Disassemble matching model.
     *
     * @param i matching model instance
     * @param ctx matching model get context
     * @return matching model get result
     */
    public MatchingModelGetResult disassemble(MatchingModelInstance i, MatchingModelGetContext ctx) {

        MatchingModelGetResult result = new MatchingModelGetResult();

        processAlgorithms(i, ctx, result);
        processMatchingTables(i, ctx, result);
        processRules(i, ctx, result);
        processSets(i, ctx, result);
        processAssignments(i, ctx, result);
        processInfoFields(i, ctx, result);

        return result;
    }

    private void merge(MatchingModel target, MatchingModel source, MatchingModelUpsertContext change) {

        // 1. Source
        Map<String, MatchingAlgorithmSource> algorithmsMerge = source.getAlgorithms().stream()
                .collect(Collectors.toMap(MatchingAlgorithmSource::getName, Function.identity()));

        Map<String, MatchingTableSource> matchingTablesMerge = source.getMatchingTables().stream()
                .collect(Collectors.toMap(MatchingTableSource::getName, Function.identity()));

        Map<String, MatchingRuleSource> rulesMerge = source.getRules().stream()
                .collect(Collectors.toMap(MatchingRuleSource::getName, Function.identity()));

        Map<String, MatchingRuleSetSource> setsMerge = source.getSets().stream()
                .collect(Collectors.toMap(MatchingRuleSetSource::getName, Function.identity()));

        Map<String, NamespaceAssignmentSource> assignmentsMerge = source.getAssignments().stream()
                .collect(Collectors.toMap(NamespaceAssignmentSource::getAssignmentKey, Function.identity()));

        // 2. Updates
        if (change.hasAlgorithmsUpdate()) {
            change.getAlgorithmsUpdate().forEach(f -> algorithmsMerge.put(f.getName(), f));
        }

        if (change.hasMatchingTablesUpdate()) {
            change.getMatchingTablesUpdate().forEach(t -> matchingTablesMerge.put(t.getName(), t));
        }

        if (change.hasRulesUpdate()) {
            change.getRulesUpdate().forEach(r -> rulesMerge.put(r.getName(), r));
        }

        if (change.hasSetsUpdate()) {
            change.getSetsUpdate().forEach(s -> setsMerge.put(s.getName(), s));
        }

        if (change.hasAssignmentsUpdate()) {
            change.getAssignmentsUpdate().forEach(a -> assignmentsMerge.put(a.getAssignmentKey(), a));
        }

        // 3. Deletes
        if (change.hasAlgorithmsDelete()) {
            change.getAlgorithmsDelete().forEach(algorithmsMerge::remove);
        }

        if (change.hasMatchingTablesDelete()) {
            change.getMatchingTablesDelete().forEach(matchingTablesMerge::remove);
        }

        if (change.hasRulesDelete()) {
            change.getRulesDelete().forEach(rulesMerge::remove);
        }

        if (change.hasSetsDelete()) {
            change.getSetsDelete().forEach(setsMerge::remove);
        }

        if (change.hasAssignmentsDelete()) {
            change.getAssignmentsDelete().forEach(assignmentsMerge::remove);
        }

        // 4. Set
        target
                .withAlgorithms(algorithmsMerge.values())
                .withMatchingTables(matchingTablesMerge.values())
                .withRules(rulesMerge.values())
                .withSets(setsMerge.values())
                .withAssignments(assignmentsMerge.values());
    }

    private MatchingModelInstance load(String storageId) {

        MatchingModelPO po = matchingModelDAO.current(storageId);
        if (Objects.isNull(po) || ArrayUtils.isEmpty(po.getContent())) {
            // Default instance
            return new MatchingModelInstanceImpl(
                    MatchingUtils.defaultModel(storageId),
                    upathService,
                    matchingAlgorithmsCacheComponent);
        }

        return new MatchingModelInstanceImpl(
                MatchingSerializer.modelFromCompressedXml(po.getContent()),
                upathService,
                matchingAlgorithmsCacheComponent);
    }

    private void processAlgorithms(MatchingModelInstance i, MatchingModelGetContext ctx, MatchingModelGetResult dto) {

        if (!ctx.isAllAlgorithms() && CollectionUtils.isEmpty(ctx.getAlgorithmIds())) {
            return;
        }

        List<MatchingAlgorithmSource> algorithms;

        if (ctx.isAllAlgorithms()) {
            algorithms = i.getAlgorithms().stream()
                    .map(el -> ((MatchingAlgorithmImpl) el).getSource())
                    .collect(Collectors.toList());
        } else {
            algorithms = ctx.getAlgorithmIds().stream()
                    .map(i::getAlgorithm)
                    .filter(Objects::nonNull)
                    .map(el -> ((MatchingAlgorithmImpl) el).getSource())
                    .collect(Collectors.toList());
        }

        dto.setAlgorithms(new AlgorithmsModelGetResult(algorithms));
    }

    private void processMatchingTables(MatchingModelInstance i, MatchingModelGetContext ctx,
                                       MatchingModelGetResult dto) {

        if (!ctx.isAllMatchingTables() && CollectionUtils.isEmpty(ctx.getMatchingTableIds())) {
            return;
        }

        List<MatchingTableSource> tables;

        if (ctx.isAllMatchingTables()) {
            tables = i.getTables().stream()
                    .map(el -> ((MatchingTableImpl) el).getSource())
                    .collect(Collectors.toList());
        } else {
            tables = ctx.getMatchingTableIds().stream()
                    .map(i::getTable)
                    .filter(Objects::nonNull)
                    .map(el -> ((MatchingTableImpl) el).getSource())
                    .collect(Collectors.toList());
        }

        dto.setMatchingTables(new MatchingTablesModelGetResult(tables));
    }

    private void processRules(MatchingModelInstance i, MatchingModelGetContext ctx, MatchingModelGetResult dto) {

        if (!ctx.isAllMatchingRules() && CollectionUtils.isEmpty(ctx.getMatchingRuleIds())) {
            return;
        }

        List<MatchingRuleSource> rules;

        if (ctx.isAllMatchingRules()) {
            rules = i.getRules().stream()
                    .map(el -> ((MatchingRuleImpl) el).getSource())
                    .collect(Collectors.toList());
        } else {
            rules = ctx.getMatchingRuleIds().stream()
                    .map(i::getRule)
                    .filter(Objects::nonNull)
                    .map(el -> ((MatchingRuleImpl) el).getSource())
                    .collect(Collectors.toList());
        }

        dto.setRules(new RulesModelGetResult(rules));
    }

    private void processSets(MatchingModelInstance i, MatchingModelGetContext ctx, MatchingModelGetResult dto) {

        if (!ctx.isAllMatchingRuleSets() && CollectionUtils.isEmpty(ctx.getMatchingRuleSetIds())) {
            return;
        }

        List<MatchingRuleSetSource> sets;

        if (ctx.isAllMatchingRuleSets()) {
            sets = i.getSets().stream()
                    .map(el -> ((MatchingRuleSetImpl) el).getSource())
                    .collect(Collectors.toList());
        } else {
            sets = ctx.getMatchingRuleSetIds().stream()
                    .map(i::getSet)
                    .filter(Objects::nonNull)
                    .map(el -> ((MatchingRuleSetImpl) el).getSource())
                    .collect(Collectors.toList());
        }

        dto.setSets(new SetsModelGetResult(sets));
    }

    private void processAssignments(MatchingModelInstance i, MatchingModelGetContext ctx, MatchingModelGetResult dto) {

        if (!ctx.isAllAssignments() && CollectionUtils.isEmpty(ctx.getAssignmentByKeys())) {
            return;
        }

        List<NamespaceAssignmentSource> assignments;

        if (ctx.isAllAssignments()) {
            assignments = i.getAssignments().stream()
                    .map(el -> ((NamespaceAssignmentImpl) el).getSource())
                    .collect(Collectors.toList());
        } else {
            assignments = ctx.getAssignmentByKeys().stream()
                    .map(i::getAssignment)
                    .filter(Objects::nonNull)
                    .map(el -> ((NamespaceAssignmentImpl) el).getSource())
                    .collect(Collectors.toList());
        }

        dto.setAssignments(new AssignmentsModelGetResult(assignments));
    }
}
