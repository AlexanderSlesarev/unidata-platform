/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.algorithm;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.unidata.mdm.core.dto.UserLibraryResult;
import org.unidata.mdm.core.type.libraries.LibraryMimeType;
import org.unidata.mdm.matching.core.type.model.instance.MatchingAlgorithmElement;

import java.util.Collection;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
public abstract class MatchingAlgorithmLibrary {
    /**
     * Default library version.
     */
    public static final String DEFAULT_LIBRARY_VERSION = "v1.0.0";
    /**
     * Type of the library.
     */
    protected final LibraryMimeType type;
    /**
     * This library (and classloader's) name.
     */
    protected final String name;

    /**
     * Constructor.
     */
    protected MatchingAlgorithmLibrary(UserLibraryResult ulr) {
        super();

        Objects.requireNonNull(ulr.getMimeType(), "MIME type cannot be null.");

        this.type = ulr.getMimeType();
        this.name = toName(ulr.getStorageId(), ulr.getFilename(), ulr.getVersion());
    }

    /**
     * @return the type
     */
    @NonNull
    public LibraryMimeType getType() {
        return type;
    }

    /**
     * Gets this library identity name.
     *
     * @return name
     */
    @NonNull
    public String getName() {
        return name;
    }

    /**
     * Performs lookup of matching algorithm implementation for the given element.
     *
     * @param el the element
     * @return implementation or null
     */
    @Nullable
    public abstract MatchingAlgorithm lookup(MatchingAlgorithmElement el);

    /**
     * Returns suggestions for class/script names, that can be used in matching algorithm definitions.
     *
     * @return suggestions for class/script names, that can be used in matching algorithm definitions.
     */
    @NonNull
    public abstract Collection<String> suggest();

    /**
     * Cleanup library.
     */
    public abstract void cleanup();

    /**
     * Gets name.
     *
     * @param ulr user library
     * @return the name
     */
    public static String toName(UserLibraryResult ulr) {
        return toName(ulr.getStorageId(), ulr.getFilename(), ulr.getVersion());
    }

    /**
     * Gets name.
     *
     * @param storageId the storage id
     * @param fileName  the file name
     * @param version   the version
     * @return the name
     */
    public static String toName(String storageId, String fileName, String version) {
        return StringUtils.trimToEmpty(storageId) + ':' +
                StringUtils.trimToEmpty(fileName) + ':' + StringUtils.trimToEmpty(version);
    }
}
