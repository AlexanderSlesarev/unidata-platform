/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.exception;

import org.unidata.mdm.system.exception.DomainId;
import org.unidata.mdm.system.exception.ExceptionId;
import org.unidata.mdm.system.exception.PlatformRuntimeException;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class MatchingRuntimeException extends PlatformRuntimeException {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 5758985735488537389L;

    /**
     * Domain.
     */
    private static final DomainId MATCHING_EXCEPTION = () -> "MATCHING_RUNTIME_EXCEPTION";

    /**
     * @param message the message
     * @param id      the id
     */
    public MatchingRuntimeException(String message, ExceptionId id, Object... args) {
        super(message, id, args);
    }

    /**
     * @param message the message
     * @param cause   the cause
     * @param id      the id
     */
    public MatchingRuntimeException(String message, Throwable cause, ExceptionId id, Object... args) {
        super(message, cause, id, args);
    }

    /**
     * @param cause the cause
     * @param id    the id
     */
    public MatchingRuntimeException(Throwable cause, ExceptionId id, Object... args) {
        super(cause, id, args);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DomainId getDomain() {
        return MATCHING_EXCEPTION;
    }
}
