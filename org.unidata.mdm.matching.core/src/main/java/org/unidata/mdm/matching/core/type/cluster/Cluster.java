/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.cluster;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Sergey Murskiy on 26.06.2021
 */
public class Cluster {
    /**
     * Cluster id.
     */
    private String id;
    /**
     * Matching set name.
     */
    private String setName;
    /**
     * Matching set display name.
     */
    private String setDisplayName;
    /**
     * Matching rule name.
     */
    private String ruleName;
    /**
     * Matching rule display name.
     */
    private String ruleDisplayName;
    /**
     * Cluster owner (needed for record linkage).
     */
    private ClusterRecord owner;
    /**
     * Matching date.
     */
    private Date matchingDate;
    /**
     * Cluster records.
     */
    private Set<ClusterRecord> clusterRecords;
    /**
     * Records count.
     */
    private int recordsCount;
    /**
     * Additional parameters.
     */
    private Map<String, Object> additionalParameters;

    /**
     * Gets cluster id.
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets cluster id.
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets matching set name.
     * @return the matching set name
     */
    public String getSetName() {
        return setName;
    }

    /**
     * Sets matching set name.
     *
     * @param setName the matching set name to set
     */
    public void setSetName(String setName) {
        this.setName = setName;
    }

    /**
     * Gets set display name.
     *
     * @return set display name
     */
    public String getSetDisplayName() {
        return setDisplayName;
    }

    /**
     * Sets set display name.
     *
     * @param setDisplayName set display name to set
     */
    public void setSetDisplayName(String setDisplayName) {
        this.setDisplayName = setDisplayName;
    }

    /**
     * Gets matching rule name.
     *
     * @return the matching rule name
     */
    public String getRuleName() {
        return ruleName;
    }

    /**
     * Sets matching rule name.
     *
     * @param ruleName the matching rule name to set
     */
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    /**
     * Gets matching rule display name.
     *
     * @return the matching rule display name
     */
    public String getRuleDisplayName() {
        return ruleDisplayName;
    }

    /**
     * Sets matching rule display name.
     *
     * @param ruleDisplayName the matching rule display name to set
     */
    public void setRuleDisplayName(String ruleDisplayName) {
        this.ruleDisplayName = ruleDisplayName;
    }

    /**
     * Gets owner.
     *
     * @return owner record
     */
    public ClusterRecord getOwner() {
        return owner;
    }

    /**
     * Sets owner.
     *
     * @param owner owner record to set
     */
    public void setOwner(ClusterRecord owner) {
        this.owner = owner;
    }

    /**
     * Gets matching date.
     *
     * @return the matching date
     */
    public Date getMatchingDate() {
        return matchingDate;
    }

    /**
     * Sets matching date.
     *
     * @param matchingDate the matching date to set
     */
    public void setMatchingDate(Date matchingDate) {
        this.matchingDate = matchingDate;
    }

    /**
     * Gets cluster records.
     *
     * @return cluster records
     */
    public Set<ClusterRecord> getClusterRecords() {
        if (Objects.isNull(clusterRecords)) {
            clusterRecords = new HashSet<>();
        }

        return clusterRecords;
    }

    /**
     * Sets cluster records.
     *
     * @param clusterRecords cluster records to set
     */
    public void setClusterRecords(Set<ClusterRecord> clusterRecords) {
        this.clusterRecords = clusterRecords;
    }

    /**
     * Gets cluster records count.
     *
     * @return records count
     */
    public int getRecordsCount() {
        return recordsCount;
    }

    /**
     * Sets cluster records count.
     *
     * @param recordsCount records count to set
     */
    public void setRecordsCount(int recordsCount) {
        this.recordsCount = recordsCount;
    }

    /**
     * Gets additional parameters.
     *
     * @return additional parameters
     */
    public Map<String, Object> getAdditionalParameters() {
        return additionalParameters;
    }

    /**
     * Sets additional parameters.
     *
     * @param additionalParameters additional parameters to set
     */
    public void setAdditionalParameters(Map<String, Object> additionalParameters) {
        this.additionalParameters = additionalParameters;
    }
}
