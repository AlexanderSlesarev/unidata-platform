/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.search;

import org.springframework.lang.NonNull;
import org.unidata.mdm.search.type.HierarchicalIndexType;
import org.unidata.mdm.search.type.HierarchicalTopIndexType;
import org.unidata.mdm.search.type.IndexField;
import org.unidata.mdm.search.type.IndexType;
import org.unidata.mdm.system.type.support.IdentityHashSet;

import java.util.Set;

/**
 * @author Sergey Murskiy on 01.07.2021
 */
public enum ClusterHeadIndexType implements ClusterIndexType, HierarchicalTopIndexType {
    /**
     * Cluster type - the top type.
     */
    CLUSTER("cluster");

    /**
     * Container for the children.
     */
    private static final Set<HierarchicalIndexType> CHILDREN = new IdentityHashSet<>();
    /**
     * Name of type
     */
    private final String type;

    /**
     * Constructor.
     *
     * @param type the name of the type
     */
    ClusterHeadIndexType(String type) {
        this.type = type;
    }
    /**
     * @return name of type
     */
    @NonNull
    @Override
    public String getName() {
        return type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRelated(IndexType searchType) {
        return searchType instanceof ClusterIndexType;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTopType() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public HierarchicalTopIndexType getTopType() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public IndexField getJoinField() {
        return ClusterHeaderField.FIELD_JOIN;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addChild(HierarchicalIndexType type) {
        CHILDREN.add(type);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<HierarchicalIndexType> getChildren() {
        return CHILDREN;
    }
}
