/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.search;

import org.springframework.lang.NonNull;
import org.unidata.mdm.search.type.HierarchicalTopIndexType;
import org.unidata.mdm.search.type.IndexType;

/**
 * @author Sergey Murskiy on 01.07.2021
 */
public enum ClusterChildIndexType implements ClusterIndexType {
    /**
     * Cluster record index type.
     */
    CLUSTER_RECORD("record");

    /**
     * Type.
     */
    private final String type;

    /**
     * Constructor.
     *
     * @param type the name of the type
     */
    ClusterChildIndexType(String type) {
        this.type = type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public HierarchicalTopIndexType getTopType() {
        return ClusterHeadIndexType.CLUSTER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTopType() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public String getName() {
        return type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRelated(IndexType searchType) {
        return searchType instanceof ClusterIndexType;
    }
}
