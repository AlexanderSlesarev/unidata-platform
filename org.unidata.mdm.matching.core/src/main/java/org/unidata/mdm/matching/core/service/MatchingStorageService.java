/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service;

import org.unidata.mdm.matching.core.context.MatchingExecutionContext;
import org.unidata.mdm.matching.core.context.MatchingUpdateContext;
import org.unidata.mdm.matching.core.dto.MatchingExecutionResult;
import org.unidata.mdm.matching.core.dto.MatchingStorageInfoGetResult;
import org.unidata.mdm.matching.core.type.storage.MatchingStorage;

import java.util.Collection;

/**
 * @author Sergey Murskiy on 07.09.2021
 */
public interface MatchingStorageService {

    /**
     * Register matching storage in matching storage service.
     *
     * @param storage matching storage
     */
    void register(MatchingStorage storage);

    /**
     * Gets all registered matching storages.
     *
     * @return matching storages
     */
    Collection<MatchingStorage> getMatchingStorages();

    /**
     * Gets registered matching storage by id.
     *
     * @param matchingStorageId the matching storage id
     * @return matching storage
     */
    MatchingStorage getMatchingStorage(String matchingStorageId);

    /**
     * Gets matching storages info.
     *
     * @return matching storages info
     */
    MatchingStorageInfoGetResult getMatchingStoragesInfo();

    /**
     * Updates storage matching records.
     *
     * @param ctx matching update context
     */
    void update(MatchingUpdateContext ctx);

    /**
     * Execute matching rule.
     *
     * @param ctx matching execution context
     * @return matching execution result
     */
    MatchingExecutionResult match(MatchingExecutionContext ctx);
}
