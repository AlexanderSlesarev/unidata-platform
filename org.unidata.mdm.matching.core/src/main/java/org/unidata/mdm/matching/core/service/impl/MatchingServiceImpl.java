/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.unidata.mdm.matching.core.context.MatchingContext;
import org.unidata.mdm.matching.core.context.MatchingExecutionContext;
import org.unidata.mdm.matching.core.context.MatchingUpdateContext;
import org.unidata.mdm.matching.core.dto.MatchingExecutionResult;
import org.unidata.mdm.matching.core.dto.MatchingResult;
import org.unidata.mdm.matching.core.dto.MatchingResult.RuleExecutionResult;
import org.unidata.mdm.matching.core.service.ClusterService;
import org.unidata.mdm.matching.core.service.MatchingService;
import org.unidata.mdm.matching.core.service.MatchingStorageService;
import org.unidata.mdm.matching.core.type.data.MatchingRecord;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 08.08.2021
 */
@Service
public class MatchingServiceImpl implements MatchingService {

    /**
     * Matching storage service.
     */
    @Autowired
    private MatchingStorageService matchingStorageService;
    /**
     * Cluster service.
     */
    @Autowired
    private ClusterService clusterService;

    /**
     * {@inheritDoc}
     */
    public void update(MatchingContext ctx) {
        matchingStorageService.update(MatchingUpdateContext.builder()
                .delete(ctx.getDeletes())
                .insert(ctx.getInserts())
                .build());
    }

    /**
     * {@inheritDoc}
     */
    public Map<MatchingRecordKey, MatchingResult> updateWithResult(MatchingContext ctx) {
        // 1. Update matching records.
        update(ctx);

        // 2. Execute matching.
        return match(ctx);
    }

    /**
     * {@inheritDoc}
     */
    public Map<MatchingRecordKey, MatchingResult> match(MatchingContext ctx) {

        if (CollectionUtils.isEmpty(ctx.getMatchingSets()) && MapUtils.isEmpty(ctx.getMatchingRules())
                && ctx.getRealTimeInput().isEmpty()) {

            return Collections.emptyMap();
        }

        switch (ctx.getExecutionMode()) {
            case ONLINE:
                return executeOnlineMode(ctx);
            case STANDARD:
            default:
                return executeStandardMode(ctx);
        }
    }

    private Map<MatchingRecordKey, MatchingResult> executeOnlineMode(MatchingContext ctx) {

        Map<MatchingRecordKey, MatchingResult> result = new HashMap<>();

        // 0. Insert matching records for processing.
        persistOnlineMatchingRecords(ctx);

        // 1. Create matching plan.
        MatchingPlan matchingPlan = new MatchingPlan(ctx);

        // 2. Execute matching.
        matchingPlan.getExecutions().forEach(execution -> {
            MatchingRuleSetElement set = execution.getKey();
            MatchingRuleMappingElement ruleMapping = execution.getValue();

            MatchingExecutionResult executionResult = matchingStorageService.match(MatchingExecutionContext.builder()
                    .realTimeInput(ctx.getRealTimeInput())
                    .set(set)
                    .skipClusterUpdates(true)
                    .rule(ruleMapping)
                    .build());

            extractMatchingResult(set, ruleMapping.getRule(), executionResult, result);
        });

        // 3. Remove processed matching records.
        removeProcessedMatchingRecords(ctx);

        // 4. Return result.
        return result;
    }

    private Map<MatchingRecordKey, MatchingResult> executeStandardMode(MatchingContext ctx) {

        Map<MatchingRecordKey, MatchingResult> result = new HashMap<>();

        // 1. Create matching plan.
        MatchingPlan matchingPlan = new MatchingPlan(ctx);

        // 2. Execute matching.
        matchingPlan.getExecutions().forEach(execution -> {
            MatchingRuleSetElement set = execution.getKey();
            MatchingRuleMappingElement ruleMapping = execution.getValue();

            // 2.1. Execute matching rule.
            MatchingExecutionResult executionResult = matchingStorageService.match(MatchingExecutionContext.builder()
                    .realTimeInput(ctx.getRealTimeInput())
                    .set(set)
                    .rule(ruleMapping)
                    .skipClusterUpdates(ctx.isSkipClusterUpdates())
                    .skipResultCalculation(ctx.isSkipResultCalculation())
                    .cleanClusterStorage(ctx.isCleanClusterStorage())
                    .build());

            // 2.2. Collect results.
            if (!ctx.isSkipResultCalculation()) {
                extractMatchingResult(set, ruleMapping.getRule(), executionResult, result);
            }

            // 2.3. Process clusters updates.
            if (!ctx.isSkipClusterUpdates() && Objects.nonNull(executionResult.getClusterUpdate())) {
                clusterService.update(executionResult.getClusterUpdate());
            }
        });

        // 3. Return result.
        return result;
    }

    private void persistOnlineMatchingRecords(MatchingContext ctx) {

        if (MapUtils.isNotEmpty(ctx.getInserts())) {
            MatchingUpdateContext updateContext = MatchingUpdateContext.builder()
                    .insert(ctx.getInserts())
                    .build();

            matchingStorageService.update(updateContext);
        }
    }

    private void removeProcessedMatchingRecords(MatchingContext ctx) {

        Map<String, List<MatchingRecordKey>> deletes = new HashMap<>();

        ctx.getInserts().forEach((matchingTableName, records) -> {
            List<MatchingRecordKey> keys = records.stream()
                    .map(MatchingRecord::getKey)
                    .collect(Collectors.toList());

            deletes.put(matchingTableName, keys);
        });

        matchingStorageService.update(MatchingUpdateContext.builder()
                .delete(deletes)
                .build());
    }

    private void extractMatchingResult(MatchingRuleSetElement set, MatchingRuleElement rule,
                                       MatchingExecutionResult executionResult,
                                       Map<MatchingRecordKey, MatchingResult> results) {

        executionResult.getResult().forEach((key, cluster) -> {
            RuleExecutionResult ruleExecutionResult = new RuleExecutionResult(set, rule);
            ruleExecutionResult.cluster(cluster);

            results.computeIfAbsent(key, k -> new MatchingResult())
                    .add(ruleExecutionResult);
        });
    }

    private static class MatchingPlan {

        /**
         * Matching executions.
         */
        private final List<Pair<MatchingRuleSetElement, MatchingRuleMappingElement>> executions;

        /**
         * Constructor.
         *
         * @param ctx matching context
         */
        private MatchingPlan(MatchingContext ctx) {

            List<MatchingRuleSetElement> matchingSets = ctx.getMatchingSets();
            Map<MatchingRuleSetElement, List<MatchingRuleElement>> matchingRules = ctx.getMatchingRules();

            if (CollectionUtils.isEmpty(matchingSets) && MapUtils.isEmpty(matchingRules)) {
                executions = Collections.emptyList();
            } else {
                executions = new ArrayList<>();

                if (MapUtils.isNotEmpty(ctx.getMatchingRules())) {

                    ctx.getMatchingRules().forEach((set, rules) ->
                            rules.forEach(rule -> executions.add(Pair.of(set, set.getMapping(rule.getName())))));

                } else if (CollectionUtils.isNotEmpty(ctx.getMatchingSets())) {

                    ctx.getMatchingSets().forEach(set ->
                            set.getMappings().forEach(ruleMapping -> executions.add(Pair.of(set, ruleMapping))));
                }
            }
        }

        private List<Pair<MatchingRuleSetElement, MatchingRuleMappingElement>> getExecutions() {
            return Objects.isNull(executions)
                    ? Collections.emptyList()
                    : executions;
        }
    }
}
