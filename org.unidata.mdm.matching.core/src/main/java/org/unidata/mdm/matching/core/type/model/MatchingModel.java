/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.core.type.model.ModelSource;
import org.unidata.mdm.core.type.model.source.AbstractModelSource;
import org.unidata.mdm.matching.core.configuration.MatchingModelIds;
import org.unidata.mdm.matching.core.type.model.source.algorithm.MatchingAlgorithmSource;
import org.unidata.mdm.matching.core.type.model.source.assignment.NamespaceAssignmentSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSetSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableSource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 12.06.2021
 */
@JacksonXmlRootElement(localName = "matching", namespace = MatchingModel.NAMESPACE)
public class MatchingModel extends AbstractModelSource<MatchingModel> implements ModelSource, Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -260452637436119410L;

    /**
     * The NS.
     */
    public static final String NAMESPACE = "http://matching.mdm.unidata.org/";

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "algorithms")
    private List<MatchingAlgorithmSource> algorithms;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "matchingTable")
    private List<MatchingTableSource> matchingTables;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "rule")
    private List<MatchingRuleSource> rules;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "set")
    private List<MatchingRuleSetSource> sets;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "assignment")
    private List<NamespaceAssignmentSource> assignments;

    /**
     * {@inheritDoc}
     */
    @JacksonXmlProperty(isAttribute = true, localName = "instanceId")
    @Override
    public String getInstanceId() {
        return MatchingModelIds.DEFAULT_MODEL_INSTANCE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @JacksonXmlProperty(isAttribute = true, localName = "typeId")
    @Override
    public String getTypeId() {
        return MatchingModelIds.MATCHING;
    }

    /**
     * Gets matching algorithms.
     *
     * @return matching algorithms
     */
    public List<MatchingAlgorithmSource> getAlgorithms() {
        if (Objects.isNull(algorithms)) {
            algorithms = new ArrayList<>();
        }

        return algorithms;
    }

    /**
     * Sets matching algorithms.
     *
     * @param algorithms matching algorithms to set
     */
    public void setAlgorithms(List<MatchingAlgorithmSource> algorithms) {
        this.algorithms = algorithms;
    }

    /**
     * Gets matching tables.
     *
     * @return matching tables
     */
    public List<MatchingTableSource> getMatchingTables() {
        if (Objects.isNull(matchingTables)) {
            matchingTables = new ArrayList<>();
        }

        return matchingTables;
    }

    /**
     * Sets matching tables.
     *
     * @param matchingTables matching tables to set
     */
    public void setMatchingTables(List<MatchingTableSource> matchingTables) {
        this.matchingTables = matchingTables;
    }

    /**
     * Gets matching rules.
     *
     * @return matching rules
     */
    public List<MatchingRuleSource> getRules() {
        if (Objects.isNull(rules)) {
            rules = new ArrayList<>();
        }

        return rules;
    }

    /**
     * Sets matching rules.
     *
     * @param rules matching rules to set
     */
    public void setRules(List<MatchingRuleSource> rules) {
        this.rules = rules;
    }

    /**
     * Gets matching rule sets.
     *
     * @return matching rule sets
     */
    public List<MatchingRuleSetSource> getSets() {
        if (Objects.isNull(sets)) {
            sets = new ArrayList<>();
        }

        return sets;
    }

    /**
     * Sets matching rule sets.
     *
     * @param sets matching rule sets to set
     */
    public void setSets(List<MatchingRuleSetSource> sets) {
        this.sets = sets;
    }

    /**
     * Gets assignments.
     *
     * @return assignments
     */
    public List<NamespaceAssignmentSource> getAssignments() {
        if (Objects.isNull(assignments)) {
            assignments = new ArrayList<>();
        }

        return assignments;
    }

    /**
     * Sets assignments.
     *
     * @param assignments assignments to set
     */
    public void setAssignments(List<NamespaceAssignmentSource> assignments) {
        this.assignments = assignments;
    }

    /**
     * Matching model with matching algorithms.
     *
     * @param values matching algorithms to set
     * @return self
     */
    public MatchingModel withAlgorithms(MatchingAlgorithmSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withAlgorithms(Arrays.asList(values));
        }

        return this;
    }

    /**
     * Matching model with matching algorithms.
     *
     * @param values matching algorithms to set
     * @return self
     */
    public MatchingModel withAlgorithms(Collection<MatchingAlgorithmSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getAlgorithms().addAll(values);
        }

        return this;
    }

    /**
     * Matching model with matching tables.
     *
     * @param values matching tables to set
     * @return self
     */
    public MatchingModel withMatchingTables(MatchingTableSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withMatchingTables(Arrays.asList(values));
        }

        return this;
    }

    /**
     * Matching model with matching tables.
     *
     * @param values matching tables to set
     * @return self
     */
    public MatchingModel withMatchingTables(Collection<MatchingTableSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getMatchingTables().addAll(values);
        }

        return this;
    }

    /**
     * Matching model with matching rules.
     *
     * @param values matching rules to set
     * @return self
     */
    public MatchingModel withRules(MatchingRuleSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withRules(Arrays.asList(values));
        }

        return this;
    }

    /**
     * Matching model with matching rules.
     *
     * @param values matching rules to set
     * @return self
     */
    public MatchingModel withRules(Collection<MatchingRuleSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getRules().addAll(values);
        }

        return this;
    }

    /**
     * Matching model with matching rule sets.
     *
     * @param values matching rule sets to set
     * @return self
     */
    public MatchingModel withSets(MatchingRuleSetSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withSets(Arrays.asList(values));
        }

        return this;
    }

    /**
     * Matching model with matching rule sets.
     *
     * @param values matching rule sets to set
     * @return self
     */
    public MatchingModel withSets(Collection<MatchingRuleSetSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getSets().addAll(values);
        }

        return this;
    }

    /**
     * Matching model with assignments.
     *
     * @param values assignments to set
     * @return self
     */
    public MatchingModel withAssignments(NamespaceAssignmentSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withAssignments(Arrays.asList(values));
        }

        return this;
    }

    /**
     * Matching model with assignments.
     *
     * @param values assignments to set
     * @return self
     */
    public MatchingModel withAssignments(Collection<NamespaceAssignmentSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getAssignments().addAll(values);
        }

        return this;
    }
}
