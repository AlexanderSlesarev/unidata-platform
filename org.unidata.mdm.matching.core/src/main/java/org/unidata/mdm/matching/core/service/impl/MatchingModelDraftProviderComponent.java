/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.draft.type.DraftOperation;
import org.unidata.mdm.draft.type.DraftProvider;
import org.unidata.mdm.matching.core.serialization.MatchingSerializer;
import org.unidata.mdm.matching.core.service.segments.MatchingDraftGetStartExecutor;
import org.unidata.mdm.matching.core.service.segments.MatchingDraftPublishStartExecutor;
import org.unidata.mdm.matching.core.service.segments.MatchingDraftUpsertStartExecutor;
import org.unidata.mdm.matching.core.type.model.MatchingModel;
import org.unidata.mdm.system.service.TextService;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
@Component
public class MatchingModelDraftProviderComponent implements DraftProvider<MatchingModel> {
    /**
     * This draft provider id.
     */
    private static final String ID = "matching-model";
    /**
     * This draft provider description.
     */
    private static final String DESCRIPTION = "app.matching.draft.model.provider.description";

    /**
     * The TS.
     */
    @Autowired
    private TextService textService;

    /**
     * Constructor.
     * @param draftService the draft service
     */
    @Autowired
    public MatchingModelDraftProviderComponent(DraftService draftService) {
        super();
        draftService.register(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public String getId() {
        return ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public String getDescription() {
        return textService.getText(DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @NonNull
    public String getPipelineId(DraftOperation operation) {

        switch (operation) {
            case GET_DATA:
                return MatchingDraftGetStartExecutor.SEGMENT_ID;
            case PUBLISH_DATA:
                return MatchingDraftPublishStartExecutor.SEGMENT_ID;
            case UPSERT_DATA:
                return MatchingDraftUpsertStartExecutor.SEGMENT_ID;
            default:
                return StringUtils.EMPTY;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingModel fromBytes(byte[] data) {
        return MatchingSerializer.modelFromCompressedXml(data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] toBytes(MatchingModel data) {
        return MatchingSerializer.modelToCompressedXml(data);
    }
}
