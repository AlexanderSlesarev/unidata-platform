/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl.instance;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.core.type.model.instance.AbstractNamedDisplayableCustomPropertiesImpl;
import org.unidata.mdm.core.util.AttributeUtils;
import org.unidata.mdm.matching.core.service.impl.MatchingAlgorithmsCacheComponent;
import org.unidata.mdm.matching.core.service.impl.algorithm.AbstractSystemMatchingAlgorithm;
import org.unidata.mdm.matching.core.type.algorithm.AlgorithmParamConfiguration;
import org.unidata.mdm.matching.core.type.algorithm.AlgorithmParameterType;
import org.unidata.mdm.matching.core.type.algorithm.EnumValue;
import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithm;
import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithmConfiguration;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmParamConfigurationElement;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmParameterElement;
import org.unidata.mdm.matching.core.type.model.instance.EnumValueElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingAlgorithmElement;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmParamConfigurationSource;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmParameterSource;
import org.unidata.mdm.matching.core.type.model.source.algorithm.EnumValueSource;
import org.unidata.mdm.matching.core.type.model.source.algorithm.MatchingAlgorithmSource;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.BooleanParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.DateParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.EnumParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.IntegerParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.NumberParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.StringParameterValue;
import org.unidata.mdm.system.util.TextUtils;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class MatchingAlgorithmImpl extends AbstractNamedDisplayableCustomPropertiesImpl
        implements MatchingAlgorithmElement {

    /**
     * Source.
     */
    private final MatchingAlgorithmSource source;
    /**
     * Matching algorithm instance.
     */
    private MatchingAlgorithm algorithm;
    /**
     * Algorithm parameters.
     */
    private final Map<String, AlgorithmParamConfigurationElement> parameters;

    /**
     * Constructor.
     *
     * @param source matching algorithm source
     */
    public MatchingAlgorithmImpl(MatchingAlgorithmSource source) {
        super(source.getName(), source.getDisplayName(), source.getDescription(), source.getCustomProperties());
        this.source = source;

        this.parameters = source.getParameters().stream()
                .map(AlgorithmParamConfigurationImpl::new)
                .collect(Collectors.toMap(AlgorithmParamConfigurationImpl::getId, Function.identity()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMatchingStorageId() {
        return source.getMatchingStorageId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJavaClass() {
        return source.getJavaClass();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingAlgorithm getAlgorithm() {
        return algorithm;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<AlgorithmParamConfigurationElement> getParameters() {
        return parameters.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AlgorithmParamConfigurationElement getParameter(String name) {
        return parameters.get(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSystem() {
        return source.isSystem();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isReady() {
        return source.isReady();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isConfigurable() {
        return source.isConfigurable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEditable() {
        return source.isEditable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getNote() {
        return source.getNote();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getCreateDate() {
        return source.getCreateDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreatedBy() {
        return source.getCreatedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getUpdateDate() {
        return source.getUpdateDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUpdatedBy() {
        return source.getUpdatedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLibrary() {
        return source.getLibraryName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion() {
        return source.getLibraryVersion();
    }

    /**
     * Gets source.
     *
     * @return the source
     */
    public MatchingAlgorithmSource getSource() {
        return source;
    }

    /*
     * Sets the matching algorithm implementation and calculates state.
     */
    public void implement(String storageId, MatchingAlgorithmsCacheComponent cache) {

        Pair<MatchingAlgorithm, Exception> result = cache.find(storageId, this);

        if (Objects.isNull(result)) {
            return;
        }

        MatchingAlgorithm matchingAlgorithm = result.getKey();
        Exception exception = result.getValue();
        this.algorithm = matchingAlgorithm;

        // Collect flags
        boolean system = false;
        boolean configurable = true;
        boolean editable = true;
        boolean ready = Objects.nonNull(matchingAlgorithm);

        if (ready) {

            MatchingAlgorithmConfiguration configuration = matchingAlgorithm.configure();

            system = matchingAlgorithm instanceof AbstractSystemMatchingAlgorithm;
            editable = !system;
            configurable = Objects.isNull(configuration);

            // Add port definitions
            if (!configurable) {

                this.parameters.clear();
                this.parameters.putAll(configuration.getParameters().stream()
                        .map(AlgorithmParamConfigurationImpl::new)
                        .collect(Collectors.toMap(AlgorithmParamConfigurationElement::getId, Function.identity())));

                // Update source
                source.getParameters().clear();
                source
                        .withParameters(paramsToParams(configuration.getParameters()));

                if (!editable) {

                    AbstractSystemMatchingAlgorithm asma
                            = (AbstractSystemMatchingAlgorithm) matchingAlgorithm;

                    source
                            .withDisplayName(asma::getDisplayName)
                            .withDescription(asma::getDescription)
                            .withMatchingStorageId(asma.getMatchingStorageId());
                }
            }
        }

        if (Objects.nonNull(exception)) {
            source.setNote(TextUtils.getText(exception));
        }

        source
                .withReady(ready)
                .withSystem(system)
                .withConfigurable(configurable)
                .withEditable(editable);
    }

    private static Collection<AlgorithmParamConfigurationSource> paramsToParams(Collection<AlgorithmParamConfiguration<?>> source) {

        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        return source.stream()
                .map(src -> new AlgorithmParamConfigurationSource()
                        .withName(src.getName())
                        .withDisplayName(src.getDisplayName())
                        .withDescription(src.getDescription())
                        .withType(src.getType())
                        .withDefaultValue(AlgorithmParamConfigurationImpl.toSourceParameter(src.getName(), src.getType(), src.getDefaultValue()))
                        .withEnumValues(src.getEnumValues().stream()
                                .map(e -> new EnumValueSource()
                                        .withName(e.getName())
                                        .withDisplayName(e::getDisplayName)).collect(Collectors.toList()))
                        .withRequired(src.isRequired()))
                .collect(Collectors.toList());
    }

    private static class AlgorithmParamConfigurationImpl implements AlgorithmParamConfigurationElement {
        /**
         * Source.
         */
        private final AlgorithmParamConfigurationSource source;
        /**
         * Config.
         */
        private final AlgorithmParamConfiguration<?> config;
        /**
         * Default value.
         */
        private final AlgorithmParameterElement defaultValue;
        /**
         * Enum values.
         */
        private final List<EnumValueElement> enumValues;


        /**
         * Constructor.
         *
         * @param source the source
         */
        public AlgorithmParamConfigurationImpl(AlgorithmParamConfigurationSource source) {
            this.source = source;
            this.config = null;

            this.defaultValue = new AlgorithmParameterImpl(source.getDefaultValue());
            this.enumValues = source.getEnumValues().stream()
                    .map(EnumValueImpl::new)
                    .collect(Collectors.toList());
        }

        /**
         * Constructor.
         *
         * @param config the configuration
         */
        public AlgorithmParamConfigurationImpl(AlgorithmParamConfiguration<?> config) {
            this.config = config;
            this.source = null;

            this.defaultValue = new AlgorithmParameterImpl(toSourceParameter(config.getName(), config.getType(), config.getDefaultValue()));
            this.enumValues = config.getEnumValues().stream()
                    .map(EnumValueImpl::new)
                    .collect(Collectors.toList());
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public AlgorithmParameterType getType() {
            return isAutoConfig() ? config.getType() : source.getType();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public AlgorithmParameterElement getDefaultValue() {
            return defaultValue;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<EnumValueElement> getEnumValues() {
            return enumValues;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isRequired() {
            return isAutoConfig() ? config.isRequired() : source.isRequired();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getId() {
            return getName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getName() {
            return isAutoConfig() ? config.getName() : source.getName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getDisplayName() {
            return isAutoConfig() ? config.getDisplayName().get() : source.getDisplayName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getDescription() {
            return isAutoConfig() ? config.getDescription().get() : source.getDescription();
        }

        private boolean isAutoConfig() {
            return Objects.nonNull(config);
        }

        private static AlgorithmParameterSource toSourceParameter(String name, AlgorithmParameterType type, Object value) {
            AlgorithmParameterSource source = new AlgorithmParameterSource()
                    .withName(name)
                    .withType(type);

            switch (type) {
                case DATE:
                    return source.withValue(new DateParameterValue().withValue(AttributeUtils.toDate(value)));
                case ENUM:
                    return source.withValue(new EnumParameterValue().withValue(AttributeUtils.toString(value)));
                case NUMBER:
                    return source.withValue(new NumberParameterValue().withValue(AttributeUtils.toDouble(value)));
                case STRING:
                    return source.withValue(new StringParameterValue().withValue(AttributeUtils.toString(value)));
                case BOOLEAN:
                    return source.withValue(new BooleanParameterValue().withValue(AttributeUtils.toBoolean(value)));
                case INTEGER:
                    return source.withValue(new IntegerParameterValue().withValue(AttributeUtils.toLong(value)));
                default:
                    return null;
            }
        }
    }

    private static class EnumValueImpl implements EnumValueElement {
        /**
         * Source.
         */
        private final EnumValueSource source;
        /**
         * Auto config.
         */
        private final EnumValue config;

        /**
         * Constructor.
         *
         * @param source source
         */
        public EnumValueImpl(EnumValueSource source) {
            this.source = source;
            this.config = null;
        }

        /**
         * Constructor.
         *
         * @param config config
         */
        public EnumValueImpl(EnumValue config) {
            this.source = null;
            this.config = config;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getName() {
            return isAutoConfig() ? config.getName() : source.getName();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getDisplayName() {
            return isAutoConfig() ? config.getDisplayName() : source.getDisplayName();
        }

        private boolean isAutoConfig() {
            return Objects.nonNull(config);
        }
    }
}
