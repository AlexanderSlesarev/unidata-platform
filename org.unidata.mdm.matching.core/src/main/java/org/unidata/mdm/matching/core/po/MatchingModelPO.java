/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.po;

import java.util.Date;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class MatchingModelPO {
    /**
     * Table name.
     */
    public static final String TABLE_NAME = "matching_model";
    /**
     * Storage ID field.
     */
    public static final String FIELD_STORAGE_ID = "storage_id";
    /**
     * Revision.
     */
    public static final String FIELD_REVISION = "revision";
    /**
     * General description.
     */
    public static final String FIELD_DESCRIPTION = "description";
    /**
     * Operation id.
     */
    public static final String FIELD_OPERATION_ID = "operation_id";
    /**
     * Data.
     */
    public static final String FIELD_CONTENT = "content";
    /**
     * Create date.
     */
    public static final String FIELD_CREATE_DATE = "create_date";
    /**
     * Created by.
     */
    public static final String FIELD_CREATED_BY = "created_by";
    /**
     * The storage id.
     */
    private String storageId;
    /**
     * Revision (is also the table PK).
     * Assigned manually causing CV for duplicates.
     */
    private int revision;
    /**
     * General description.
     */
    private String description;
    /**
     * Operation id.
     */
    private String operationId;
    /**
     * Data.
     */
    private byte[] content;
    /**
     * Creator's user name.
     */
    private String createdBy;
    /**
     * Create date.
     */
    private Date createDate;

    /**
     * Constructor.
     */
    public MatchingModelPO() {
        super();
    }

    /**
     * @return the storageId
     */
    public String getStorageId() {
        return storageId;
    }

    /**
     * @param storageId the storageId to set
     */
    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }

    /**
     * @return the revision
     */
    public int getRevision() {
        return revision;
    }

    /**
     * @param revision the revision to set
     */
    public void setRevision(int revision) {
        this.revision = revision;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the operationId
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * @param operationId the operationId to set
     */
    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    /**
     * @return the content
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * @param data the content to set
     */
    public void setContent(byte[] data) {
        this.content = data;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
