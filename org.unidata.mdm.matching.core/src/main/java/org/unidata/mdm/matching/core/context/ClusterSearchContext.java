/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

import org.unidata.mdm.matching.core.type.cluster.ClusterSearchQuery;

/**
 * @author Sergey Murskiy on 06.09.2021
 */
public class ClusterSearchContext {
    /**
     * Cluster search query.
     */
    private final ClusterSearchQuery query;
    /**
     * Fetch cluster records.
     */
    private final boolean fetchClusterRecords;

    /**
     * Constructor.
     *
     * @param b builder
     */
    public ClusterSearchContext(ClusterSearchContextBuilder b) {
        this.query = b.query;
        this.fetchClusterRecords = b.fetchClusterRecords;
    }

    /**
     * Gets cluster search query.
     *
     * @return cluster search query
     */
    public ClusterSearchQuery getQuery() {
        return query;
    }

    /**
     * Is fetch cluster records.
     *
     * @return is fetch cluster records
     */
    public boolean isFetchClusterRecords() {
        return fetchClusterRecords;
    }

    /**
     * Gets builder.
     *
     * @return builder
     */
    public static ClusterSearchContextBuilder builder() {
        return new ClusterSearchContextBuilder();
    }

    /**
     * Builder class.
     */
    public static class ClusterSearchContextBuilder {
        /**
         * Cluster search query.
         */
        private ClusterSearchQuery query;
        /**
         * Fetch cluster records.
         */
        private boolean fetchClusterRecords;

        /**
         * Sets search query.
         *
         * @param query search query
         * @return self
         */
        public ClusterSearchContextBuilder query(ClusterSearchQuery query) {
            this.query = query;
            return this;
        }

        /**
         * Sets fetchClusterRecords.
         *
         * @param fetchClusterRecords the 'fetchClusterRecords' to set
         * @return self
         */
        public ClusterSearchContextBuilder fetchClusterRecords(boolean fetchClusterRecords) {
            this.fetchClusterRecords = fetchClusterRecords;
            return this;
        }

        /**
         * Build.
         *
         * @return cluster search context
         */
        public ClusterSearchContext build() {
            return new ClusterSearchContext(this);
        }
    }
}
