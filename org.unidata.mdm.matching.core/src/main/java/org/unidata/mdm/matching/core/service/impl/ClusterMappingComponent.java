/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.matching.core.configuration.MatchingCoreConfigurationConstants;
import org.unidata.mdm.matching.core.type.search.ClusterChildIndexType;
import org.unidata.mdm.matching.core.type.search.ClusterHeadIndexType;
import org.unidata.mdm.matching.core.type.search.ClusterHeaderField;
import org.unidata.mdm.matching.core.type.search.ClusterRecordHeaderField;
import org.unidata.mdm.search.context.MappingRequestContext;
import org.unidata.mdm.search.service.SearchService;
import org.unidata.mdm.search.type.mapping.Mapping;
import org.unidata.mdm.search.type.mapping.impl.CompositeMappingField;
import org.unidata.mdm.search.type.mapping.impl.LongMappingField;
import org.unidata.mdm.search.type.mapping.impl.StringMappingField;
import org.unidata.mdm.search.type.mapping.impl.TimestampMappingField;
import org.unidata.mdm.search.util.SearchUtils;
import org.unidata.mdm.system.service.AfterModuleStartup;
import org.unidata.mdm.system.service.AfterPlatformStartup;
import org.unidata.mdm.system.service.ConfigurationActionService;
import org.unidata.mdm.system.type.action.ConfigurationAction;

/**
 * @author Sergey Murskiy on 01.07.2021
 */
@Component
public class ClusterMappingComponent implements AfterPlatformStartup, AfterModuleStartup {
    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ClusterMappingComponent.class);
    /**
     * Config action.
     */
    private final ConfigurationAction ensureMappingAction = new ConfigurationAction() {
        /**
         * {@inheritDoc}
         */
        @Override
        public int getTimes() {
            return 1;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @NonNull
        public String getId() {
            return MatchingCoreConfigurationConstants.CLUSTER_ENSURE_INDEXES_ACTION_NAME;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean execute() {

            try {
                appendMapping();
            } catch (Exception e) {
                LOGGER.warn("Cannot ensure cluster index. Exception caught.", e);
                return false;
            }

            return true;
        }

        private void appendMapping() {

            final Mapping clusterMapping = new Mapping(ClusterHeadIndexType.CLUSTER)
                    .withFields(
                            new StringMappingField(ClusterHeaderField.FIELD_ID.getName())
                                    .withDocValue(true),
                            new StringMappingField(ClusterHeaderField.FIELD_RULE_NAME.getName())
                                    .withDocValue(true),
                            new StringMappingField(ClusterHeaderField.FIELD_SET_NAME.getName())
                                    .withDocValue(true),
                            new StringMappingField(ClusterHeaderField.FIELD_SET_DISPLAY_NAME.getName())
                                    .withDocValue(true),
                            new StringMappingField(ClusterHeaderField.FIELD_RULE_DISPLAY_NAME.getName())
                                    .withDocValue(true),
                            new StringMappingField(ClusterHeaderField.FIELD_OWNER_NAMESPACE.getName())
                                    .withDocValue(true),
                            new StringMappingField(ClusterHeaderField.FIELD_OWNER_TYPE_NAME.getName())
                                    .withDocValue(true),
                            new StringMappingField(ClusterHeaderField.FIELD_OWNER_SUBJECT_ID.getName())
                                    .withDocValue(true),
                            new TimestampMappingField(ClusterHeaderField.FIELD_OWNER_VALID_FROM.getName())
                                    .withFormat(SearchUtils.DEFAULT_TIMESTAMP_TARGET_FORMAT)
                                    .withDocValue(true),
                            new TimestampMappingField(ClusterHeaderField.FIELD_OWNER_VALID_TO.getName())
                                    .withFormat(SearchUtils.DEFAULT_TIMESTAMP_TARGET_FORMAT)
                                    .withDocValue(true),
                            new LongMappingField(ClusterHeaderField.FIELD_RECORDS_COUNT.getName())
                                    .withDocValue(true),
                            new TimestampMappingField(ClusterHeaderField.FIELD_MATCHING_DATE.getName()),
                            new CompositeMappingField(ClusterHeaderField.FIELD_ADDITIONAL_PARAMETERS.getName())
                                    .withNested(true)
                                    .withFields(
                                            new StringMappingField(ClusterHeaderField.FIELD_ADDITIONAL_PARAMETER_NAME.getName())
                                                    .withDocValue(true),
                                            new StringMappingField(ClusterHeaderField.FIELD_ADDITIONAL_PARAMETER_VALUE.getName())
                                                    .withDocValue(true)
                                    )
                    );

            final Mapping clusterRecordMapping = new Mapping(ClusterChildIndexType.CLUSTER_RECORD)
                    .withFields(
                            new StringMappingField(ClusterRecordHeaderField.FIELD_SUBJECT_ID.getName())
                                    .withDocValue(true),
                            new StringMappingField(ClusterRecordHeaderField.FIELD_NAMESPACE.getName())
                                    .withDocValue(true),
                            new StringMappingField(ClusterRecordHeaderField.FIELD_TYPE_NAME.getName())
                                    .withDocValue(true),
                            new TimestampMappingField(ClusterRecordHeaderField.FIELD_VALID_FROM.getName())
                                    .withFormat(SearchUtils.DEFAULT_TIMESTAMP_TARGET_FORMAT),
                            new TimestampMappingField(ClusterRecordHeaderField.FIELD_VALID_TO.getName())
                                    .withFormat(SearchUtils.DEFAULT_TIMESTAMP_TARGET_FORMAT),
                            new StringMappingField(ClusterRecordHeaderField.FIELD_CLUSTER_ID.getName())
                                    .withDocValue(true),
                            new LongMappingField(ClusterRecordHeaderField.FIELD_MATCHING_RATE.getName())
                    );

            MappingRequestContext mCtx = MappingRequestContext.builder()
                    .entity(MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME)
                    .storageId(SecurityUtils.getCurrentUserStorageId())
                    .mappings(clusterMapping, clusterRecordMapping)
                    .build();

            searchService.process(mCtx);
        }
    };

    /**
     * Configuration action service.
     */
    @Autowired
    private ConfigurationActionService configurationActionService;
    /**
     * The search service.
     */
    @Autowired
    private SearchService searchService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterModuleStartup() {
        ClusterHeadIndexType.CLUSTER.addChild(ClusterChildIndexType.CLUSTER_RECORD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPlatformStartup() {
        configurationActionService.execute(ensureMappingAction);
    }
}
