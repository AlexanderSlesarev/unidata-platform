/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.unidata.mdm.core.configuration.CoreConfigurationConstants;
import org.unidata.mdm.core.context.UserLibraryUpsertContext;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.service.UserLibraryService;
import org.unidata.mdm.core.type.libraries.LibraryMimeType;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.matching.core.configuration.MatchingDescriptors;
import org.unidata.mdm.matching.core.context.MatchingExecutionContext;
import org.unidata.mdm.matching.core.context.MatchingModelUpsertContext;
import org.unidata.mdm.matching.core.context.MatchingUpdateContext;
import org.unidata.mdm.matching.core.dao.MatchingStorageDAO;
import org.unidata.mdm.matching.core.dto.MatchingExecutionResult;
import org.unidata.mdm.matching.core.dto.MatchingStorageInfoGetResult;
import org.unidata.mdm.matching.core.exception.MatchingExceptionIds;
import org.unidata.mdm.matching.core.exception.MatchingRuntimeException;
import org.unidata.mdm.matching.core.po.MatchingStoragePO;
import org.unidata.mdm.matching.core.service.MatchingStorageService;
import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithmLibrary;
import org.unidata.mdm.matching.core.type.model.instance.MatchingDeploymentElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.matching.core.type.model.source.algorithm.MatchingAlgorithmSource;
import org.unidata.mdm.matching.core.type.storage.MatchingStorage;
import org.unidata.mdm.matching.core.type.storage.MatchingStorageDescriptor;
import org.unidata.mdm.matching.core.type.storage.MatchingStorageInfo;
import org.unidata.mdm.matching.core.type.storage.MatchingStorageStatus;
import org.unidata.mdm.system.service.AfterPlatformStartup;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 09.08.2021
 */
@Service
public class MatchingStorageServiceImpl implements MatchingStorageService, AfterPlatformStartup {

    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MatchingStorageServiceImpl.class);

    /**
     * Meta model service.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * User library service.
     */
    @Autowired
    private UserLibraryService userLibraryService;
    /**
     * Matching storage DAO.
     */
    @Autowired
    private MatchingStorageDAO matchingStorageDAO;

    /**
     * Register matching storages.
     */
    private final Map<String, MatchingStorage> matchingStorages = new HashMap<>();

    /**
     * {@inheritDoc}
     */
    public void register(MatchingStorage storage) {
        matchingStorages.put(storage.descriptor().getStorageId(), storage);
    }

    /**
     * {@inheritDoc}
     */
    public Collection<MatchingStorage> getMatchingStorages() {
        return matchingStorages.values();
    }

    /**
     * {@inheritDoc}
     */
    public MatchingStorage getMatchingStorage(String id) {
        return MapUtils.isEmpty(matchingStorages)
                ? null
                : matchingStorages.get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingStorageInfoGetResult getMatchingStoragesInfo() {
        List<MatchingStorageInfo> infos = matchingStorages.values().stream()
                .map(matchingStorage -> {
                    MatchingStorageDescriptor descriptor = matchingStorage.descriptor();

                    MatchingStorageInfo info = new MatchingStorageInfo();
                    info.setName(descriptor.getStorageId());
                    info.setDisplayName(descriptor.getDisplayName());
                    info.setDescription(descriptor.getDescription());

                    return info;
                })
                .collect(Collectors.toList());

        return new MatchingStorageInfoGetResult(infos);
    }

    /**
     * {@inheritDoc}
     */
    public void update(MatchingUpdateContext ctx) {

        Map<String, MatchingUpdateContext.MatchingUpdateContextBuilder> target = new HashMap<>();
        MatchingModelInstance instance = metaModelService.instance(MatchingDescriptors.MATCHING);

        // 1. Process deletes
        ctx.getDeletes().forEach((table, ids) -> {
            Map<String, MatchingDeploymentElement> deployments = instance.getTableDeployments(table);

            deployments.forEach((storageId, deployment) ->
                    target.computeIfAbsent(deployment.getMatchingStorageId(), k -> MatchingUpdateContext.builder())
                            .delete(table, ids));
        });

        // 2. Process inserts.
        ctx.getInserts().forEach((table, records) -> {
            Map<String, MatchingDeploymentElement> deployments = instance.getTableDeployments(table);

            deployments.forEach((storageId, deployment) ->
                    target.computeIfAbsent(storageId, k -> MatchingUpdateContext.builder())
                            .insert(table, records));
        });

        // 3. Apply changes.
        target.forEach((storageId, targetCtx) -> {
            Map<String, MatchingDeploymentElement> deployments = instance.getStorageDeployments(storageId);

            MatchingUpdateContext updateContext = targetCtx.build();
            updateContext.deployments(deployments);

            getMatchingStorage(storageId).update(updateContext);
        });
    }

    /**
     * {@inheritDoc}
     */
    public MatchingExecutionResult match(MatchingExecutionContext ctx) {

        MatchingModelInstance instance = metaModelService.instance(MatchingDescriptors.MATCHING);
        String matchingStorageId = ctx.getSet().getMatchingStorageId();

        Map<String, MatchingDeploymentElement> deployments = instance.getStorageDeployments(matchingStorageId);
        ctx.deployments(deployments);

        return getMatchingStorage(matchingStorageId).match(ctx);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPlatformStartup() {
        Map<String, MatchingStoragePO> loadedStorages = matchingStorageDAO.fetchMatchingStoragesInfo().stream()
                .collect(Collectors.toMap(MatchingStoragePO::getId, Function.identity()));

        List<MatchingStoragePO> updates = new ArrayList<>();
        for (MatchingStorage matchingStorage : matchingStorages.values()) {
            MatchingStorageDescriptor descriptor = matchingStorage.descriptor();

            if (loadedStorages.containsKey(descriptor.getStorageId())) {
                registerListeners(descriptor);
                continue;
            }

            MatchingStoragePO info = new MatchingStoragePO();
            info.setId(descriptor.getStorageId());

            try {
                LOGGER.info("Installing matching storage {}", descriptor.getStorageId());
                registerListeners(descriptor);
                registerAlgorithms(descriptor);
                LOGGER.info("Matching storage {} was installing", descriptor.getStorageId());

                info.setStatus(MatchingStorageStatus.LOADED);
            } catch (Exception e) {
                info.setStatus(MatchingStorageStatus.LOAD_FAILED);
                LOGGER.error("Installation of matching storage {} failed", descriptor.getStorageId(), e);
            }

            updates.add(info);
        }

        matchingStorageDAO.saveMatchingStoragesInfo(updates);
    }

    private void registerListeners(MatchingStorageDescriptor descriptor) {
        descriptor.getModelRefreshListeners().forEach(listener -> metaModelService.listener(listener));
    }

    private void registerAlgorithms(MatchingStorageDescriptor descriptor) {

        String libraryName = StringUtils.join(descriptor.getStorageId(), "-algorithms.jar");

        Manifest mf = new Manifest();
        mf.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
        mf.getMainAttributes().put(new Attributes.Name(CoreConfigurationConstants.CORE_LIBRARIES_VERSION_PROPERTY), "1.0.0");

        byte[] buf = new byte[8096];
        int read;
        ClassLoader ccl = Thread.currentThread().getContextClassLoader();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (JarOutputStream jos = new JarOutputStream(baos, mf)) {

            for (String name : descriptor.getMatchingAlgorithms()) {

                String path = name.replace('.', '/') + ".class";
                JarEntry je = new JarEntry(path);
                jos.putNextEntry(je);

                InputStream is = ccl.getResourceAsStream(path);
                while ((read = is.read(buf, 0, buf.length)) != -1) {
                    jos.write(buf, 0, read);
                }
            }
        } catch (Exception e) {
            throw new MatchingRuntimeException("Cannot install algorithms.", e,
                    MatchingExceptionIds.EX_MATCHING_CANNOT_INSTALL_MATCHING_ALGORITHMS);
        }

        byte[] payload = baos.toByteArray();
        if (ArrayUtils.isNotEmpty(payload)) {

            ByteArrayInputStream bais = new ByteArrayInputStream(payload);
            UserLibraryUpsertContext ctx = UserLibraryUpsertContext.builder()
                    .filename(libraryName)
                    .version(MatchingAlgorithmLibrary.DEFAULT_LIBRARY_VERSION)
                    .mimeType(LibraryMimeType.JAR_FILE.getCode())
                    .input(bais)
                    .force(true)
                    .editable(false)
                    .build();

            userLibraryService.upsert(ctx);

            String user = SecurityUtils.getCurrentUserName();
            OffsetDateTime now = OffsetDateTime.now();

            List<MatchingAlgorithmSource> sources = new ArrayList<>();
            for (String name : descriptor.getMatchingAlgorithms()) {

                MatchingAlgorithmSource source = new MatchingAlgorithmSource()
                        .withCreateDate(now)
                        .withCreatedBy(user)
                        .withLibraryName(libraryName)
                        .withLibraryVersion(MatchingAlgorithmLibrary.DEFAULT_LIBRARY_VERSION)
                        .withJavaClass(name)
                        .withMatchingStorageId(descriptor.getStorageId())
                        .withName(name.substring(name.lastIndexOf('.') + 1));

                sources.add(source);
            }

            MatchingModelUpsertContext uCtx = MatchingModelUpsertContext.builder()
                    .algorithmsUpdate(sources)
                    .force(true)
                    .build();

            metaModelService.upsert(uCtx);
        }
    }
}
