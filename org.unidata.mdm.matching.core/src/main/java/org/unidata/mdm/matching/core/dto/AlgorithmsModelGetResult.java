/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dto;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.model.source.algorithm.MatchingAlgorithmSource;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class AlgorithmsModelGetResult {
    /**
     * Algorithms, if asked as standalone.
     */
    private List<MatchingAlgorithmSource> algorithms;

    /**
     * Constructor.
     *
     * @param algorithms matching algorithms
     */
    public AlgorithmsModelGetResult(List<MatchingAlgorithmSource> algorithms) {
        super();
        this.algorithms = algorithms;
    }

    /**
     * Constructor.
     *
     * @param algorithm matching algorithm
     */
    public AlgorithmsModelGetResult(MatchingAlgorithmSource algorithm) {
        super();
        this.algorithms = Collections.singletonList(algorithm);
    }

    /**
     * @return the algorithms
     */
    public List<MatchingAlgorithmSource> getAlgorithms() {
        return Objects.isNull(algorithms) ? Collections.emptyList() : algorithms;
    }

    /**
     * @param algorithms the algorithms to set
     */
    public void setAlgorithms(List<MatchingAlgorithmSource> algorithms) {
        this.algorithms = algorithms;
    }

    /**
     * Gets first value.
     *
     * @return algorithm
     */
    public MatchingAlgorithmSource singleValue() {
        return CollectionUtils.isNotEmpty(algorithms)
                ? algorithms.get(0)
                : null;
    }
}
