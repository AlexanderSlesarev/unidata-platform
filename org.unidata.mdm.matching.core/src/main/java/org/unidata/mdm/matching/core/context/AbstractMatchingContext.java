/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

import org.springframework.lang.NonNull;
import org.unidata.mdm.system.context.StorageCapableContext;
import org.unidata.mdm.system.context.StorageId;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * @author Sergey Murskiy on 10.08.2021
 */
public abstract class AbstractMatchingContext implements StorageCapableContext {
    /**
     * Simple storage.
     * This map is intentionally made to work with object pointers.
     * See children for examples.
     */
    protected final Map<StorageId, Object> systemStorage = new IdentityHashMap<>();

    /**
     * Constructor.
     *
     * @param b the builder
     */
    protected AbstractMatchingContext(AbstractMatchingContextBuilder<?> b) {
        super();
    }

    /**
     * Puts an object to temp storage.
     *
     * @param id id
     * @param t  object
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T, R extends StorageCapableContext> R putToStorage(@NonNull StorageId id, T t) {
        systemStorage.put(id, t);
        return (R) this;
    }
    /**
     * Gets an object from temp storage.
     *
     * @param id id
     * @return object
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T getFromStorage(@NonNull StorageId id) {
        return (T) systemStorage.get(id);
    }

    /**
     * Abstract matching context builder.
     *
     * @param <X>
     */
    public abstract static class AbstractMatchingContextBuilder<X extends AbstractMatchingContextBuilder<X>> {

        /**
         * Gets self.
         *
         * @return self
         */
        @SuppressWarnings("unchecked")
        protected X self() {
            return (X) this;
        }

        /**
         * Build context.
         *
         * @return matching context
         */
        public abstract AbstractMatchingContext build();
    }
}
