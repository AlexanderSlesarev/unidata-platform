package org.unidata.mdm.matching.core.type.storage;

/**
 * @author Sergey Murskiy on 07.09.2021
 */
public enum MatchingStorageStatus {
    LOADED,
    LOAD_FAILED
}
