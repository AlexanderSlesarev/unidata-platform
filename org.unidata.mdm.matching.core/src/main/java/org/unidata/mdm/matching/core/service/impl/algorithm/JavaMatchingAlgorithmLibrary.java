/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl.algorithm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.unidata.mdm.core.dto.UserLibraryResult;
import org.unidata.mdm.matching.core.exception.MatchingExceptionIds;
import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithm;
import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithmLibrary;
import org.unidata.mdm.matching.core.type.model.instance.MatchingAlgorithmElement;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.service.ModuleService;
import org.unidata.mdm.system.util.JarUtils;

import java.io.ByteArrayInputStream;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarInputStream;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
public class JavaMatchingAlgorithmLibrary extends MatchingAlgorithmLibrary {
    /**
     * This class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JavaMatchingAlgorithmLibrary.class);
    /**
     * This class loader.
     */
    private final JarUtils.SingleJarClassLoader classLoader;
    /**
     * The MS instance
     */
    private final ModuleService moduleService;
    /**
     * Beans.
     */
    private final Map<String, MatchingAlgorithm> beans = new ConcurrentHashMap<>();

    /**
     * Constructor.
     *
     * @param moduleService module service
     * @param ulr           user library
     */
    public JavaMatchingAlgorithmLibrary(ModuleService moduleService, UserLibraryResult ulr) {

        super(ulr);
        this.moduleService = moduleService;

        try (JarInputStream jis = new JarInputStream(new ByteArrayInputStream(ulr.getPayload()))) {
            this.classLoader = new JarUtils.SingleJarClassLoader(this.name,
                    Thread.currentThread().getContextClassLoader(), jis);
        } catch (Exception e) {
            throw new PlatformFailureException("Exception caught while creating Java library instance.",
                    e, MatchingExceptionIds.EX_MATCHING_LIBRARY_CANNOT_BE_CREATED);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingAlgorithm lookup(MatchingAlgorithmElement el) {
        String algorithmName = el.getId();
        String className = el.getJavaClass();

        return lookup(algorithmName, className);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanup() {

        beans.forEach((name, bean) -> {
            try {
                // Call @PreDestroy, destroy() etc.
                moduleService.releaseOrphanBean(bean);
            } catch (Exception e) {
                LOGGER.error("Exception caught while passivating a Java Matching Algorithm [{}].", name, e);
            }
        });

        beans.clear();
        classLoader.cleanup();
    }

    /**
     * {@inheritDoc}
     */
    @NonNull
    @Override
    public Collection<String> suggest() {
        return classLoader.filterAssignableNames(MatchingAlgorithm.class);
    }

    /**
     * Gets algorithm implementation by algorithm name and class.
     *
     * @param algorithmName the algorithm name
     * @param className     the class name
     * @return matching algorithm
     */
    public MatchingAlgorithm lookup(String algorithmName, String className) {
        return beans.computeIfAbsent(algorithmName, key -> wireAlgorithm(key, className));
    }

    /**
     * Lazy creates algorithm implementation.
     *
     * @param algorithmName the algorithm name
     * @param className     the class name
     * @return algorithm or NIL (not found/unreachable) indicator
     */
    private MatchingAlgorithm wireAlgorithm(String algorithmName, String className) {

        try {

            Class<?> klass = Class.forName(className, true, this.classLoader);

            if (!MatchingAlgorithm.class.isAssignableFrom(klass)) {
                throw new IllegalArgumentException("The class "
                        + klass.getName()
                        + " is NOT an instance of MatchingAlgorithm interface!");
            }

            Object o = klass.getConstructor().newInstance();
            return moduleService.initOrphanBean(algorithmName, (MatchingAlgorithm) o);

        } catch (Exception e) {
            throw new PlatformFailureException("Cannot create Java matching algorithm instance for [{}].", e,
                    MatchingExceptionIds.EX_MATCHING_ALGORITHM_CANNOT_BE_CREATED, algorithmName);
        }
    }
}
