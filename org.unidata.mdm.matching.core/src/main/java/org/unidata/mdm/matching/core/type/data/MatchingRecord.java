/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.data;

import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.matching.core.type.data.field.MatchingRecordField;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 04.08.2021
 */
public class MatchingRecord {
    /**
     * Matching record key.
     */
    private MatchingRecordKey key;
    /**
     * Matching record fields.
     */
    private Map<String, MatchingRecordField<?>> fields;
    /**
     * Child records (map namespace -> records).
     */
    private Map<String, List<MatchingRecord>> child;

    /**
     * Gets matching record key.
     *
     * @return matching record key
     */
    public MatchingRecordKey getKey() {
        return key;
    }

    /**
     * Sets matching record key.
     *
     * @param key matching record key
     */
    public void setKey(MatchingRecordKey key) {
        this.key = key;
    }

    /**
     * Gets matching record field.
     *
     * @return matching record fields
     */
    public Map<String, MatchingRecordField<?>> getFields() {
        return fields;
    }

    /**
     * Gets matching record field by name.
     *
     * @param name the name
     * @return matching record field
     */
    public MatchingRecordField<?> getField(String name) {
        return fields.get(name);
    }

    /**
     * Sets matching record fields
     *
     * @param fields matching record fields to set
     */
    public void setFields(Map<String, MatchingRecordField<?>> fields) {
        this.fields = fields;
    }

    /**
     * Gets child matching records.
     *
     * @return child matching records
     */
    public Map<String, List<MatchingRecord>> getChild() {
        return child;
    }

    /**
     * Sets child matching records.
     *
     * @param child child matching records to set (map namespace -> child records)
     */
    public void setChild(Map<String, List<MatchingRecord>> child) {
        this.child = child;
    }

    /**
     * Has child.
     *
     * @return true, if record has child
     */
    public boolean hasChild() {
        return MapUtils.isNotEmpty(child);
    }

    /**
     * Is valid.
     *
     * @return true, if record is valid
     */
    public boolean isValid() {
        return Objects.nonNull(key) && key.isValidKey();
    }
}
