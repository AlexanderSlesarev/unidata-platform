/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.instance;

import org.unidata.mdm.core.type.model.IdentityElement;
import org.unidata.mdm.matching.core.type.algorithm.AlgorithmParameterType;

/**
 * @author Sergey Murskiy on 20.06.2021
 */
public interface AlgorithmParameterElement extends IdentityElement {
    /**
     * Gets the parameter name.
     *
     * @return the name
     */
    String getName();

    /**
     * Gets the single value parameter type.
     *
     * @return type
     */
    AlgorithmParameterType getType();

    /**
     * Gets the value of the parameter.
     *
     * @param <V> the value type
     * @return value
     */
    <V> V getValue();
}
