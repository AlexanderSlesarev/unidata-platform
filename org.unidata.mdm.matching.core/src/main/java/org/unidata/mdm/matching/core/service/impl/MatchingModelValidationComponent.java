/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.context.ModelSourceContext;
import org.unidata.mdm.core.exception.UPathException;
import org.unidata.mdm.core.service.CustomPropertiesSupport;
import org.unidata.mdm.core.service.UPathService;
import org.unidata.mdm.core.type.upath.UPath;
import org.unidata.mdm.matching.core.service.MatchingStorageService;
import org.unidata.mdm.matching.core.service.impl.instance.MatchingAlgorithmImpl;
import org.unidata.mdm.matching.core.type.model.MatchingModel;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmMappingSource;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmSettingsSource;
import org.unidata.mdm.matching.core.type.model.source.algorithm.MatchingAlgorithmSource;
import org.unidata.mdm.matching.core.type.model.source.assignment.NamespaceAssignmentSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleMappingSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSetSource;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingColumnMappingSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingColumnSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableMappingSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableSource;
import org.unidata.mdm.matching.core.type.storage.MatchingStorageInfo;
import org.unidata.mdm.system.exception.ValidationResult;
import org.unidata.mdm.system.util.NameSpaceUtils;
import org.unidata.mdm.system.util.TextUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
@Component
public class MatchingModelValidationComponent implements CustomPropertiesSupport {
    /**
     * Default validation pattern for names in model.
     */
    public static final Pattern DEFAULT_MODEL_NAME_PATTERN = Pattern.compile("^[a-z][a-z0-9_-]*$", Pattern.CASE_INSENSITIVE);

    // MATCHING ALGORITHM VALIDATION CONSTANTS.
    /**
     * The Constant VALIDATE_ALGORITHM_HAS_MISSING_MATCHING_STORAGE_ID.
     */
    private static final String VALIDATE_ALGORITHM_HAS_MISSING_MATCHING_STORAGE_ID = "app.meta.matching.algorithm.has.missing.matching.storage.id";
    /**
     * The Constant VALIDATE_ALGORITHM_MATCHING_STORAGE_NOT_FOUND.
     */
    private static final String VALIDATE_ALGORITHM_MATCHING_STORAGE_NOT_FOUND = "app.meta.matching.algorithm.matching.storage.not.found";
    /**
     * The Constant VALIDATE_ALGORITHM_NOT_READY_FOR_OPERATION.
     */
    private static final String VALIDATE_ALGORITHM_NOT_READY_FOR_OPERATION = "app.meta.matching.algorithm.not.ready.for.operation";
    /**
     * The Constant VALIDATE_ALGORITHM_HAS_FAILURE_NOTES.
     */
    private static final String VALIDATE_ALGORITHM_HAS_FAILURE_NOTES = "app.meta.matching.algorithm.has.failure.notes";
    /**
     * The Constant VALIDATE_ALGORITHM_CAN_NOT_BE_INSTANTIATED.
     */
    private static final String VALIDATE_ALGORITHM_CAN_NOT_BE_INSTANTIATED = "app.meta.matching.algorithm.can.not.be.instantiated";

    // END OF ALGORITHM VALIDATION CONSTANTS.
    // --------------------------------------------------
    // MATCHING RULE VALIDATION CONSTANTS.
    /**
     * The Constant VALIDATE_RULE_HAS_MISSING_MATCHING_STORAGE_ID.
     */
    private static final String VALIDATE_RULE_HAS_MISSING_MATCHING_STORAGE_ID = "app.meta.matching.rule.has.missing.matching.storage.id";
    /**
     * The Constant VALIDATE_RULE_MATCHING_STORAGE_NOT_FOUND.
     */
    private static final String VALIDATE_RULE_MATCHING_STORAGE_NOT_FOUND = "app.meta.matching.rule.matching.storage.not.found";
    /**
     * The Constant VALIDATE_RULE_ALGORITHM_ABSENT.
     */
    private static final String VALIDATE_RULE_ALGORITHM_ABSENT = "app.meta.matching.rule.algorithm.not.found";
    /**
     * The Constant VALIDATE_RULE_INVALID_ALGORITHM_STORAGE.
     */
    private static final String VALIDATE_RULE_INVALID_ALGORITHM_STORAGE = "app.meta.matching.rule.invalid.algorithm.storage";

    // END OF RULE VALIDATION CONSTANTS.
    // --------------------------------------------------
    // SET VALIDATIONS CONSTANTS.
    /**
     * The Constant VALIDATE_SET_HAS_MISSING_MATCHING_STORAGE_ID.
     */
    private static final String VALIDATE_SET_HAS_MISSING_MATCHING_STORAGE_ID = "app.meta.matching.set.has.missing.matching.storage.id";
    /**
     * The Constant VALIDATE_SET_MATCHING_STORAGE_NOT_FOUND.
     */
    private static final String VALIDATE_SET_MATCHING_STORAGE_NOT_FOUND = "app.meta.matching.set.matching.storage.not.found";
    /**
     * The Constant VALIDATE_SET_HAS_MISSING_MATCHING_TABLE_NAME.
     */
    private static final String VALIDATE_SET_HAS_MISSING_MATCHING_TABLE_NAME = "app.meta.matching.set.has.missing.matching.table.name";
    /**
     * The Constant VALIDATE_SET_MATCHING_TABLE_NOT_FOUND.
     */
    private static final String VALIDATE_SET_MATCHING_TABLE_NOT_FOUND = "app.meta.matching.set.matching.table.not.found";
    /**
     * The Constant VALIDATE_SET_RULE_MAPPING_RULE_NOT_FOUND.
     */
    private static final String VALIDATE_SET_RULE_MAPPING_RULE_NOT_FOUND = "app.meta.matching.set.rule.mapping.rule.not.found";
    /**
     * The Constant VALIDATE_SET_RULE_MAPPING_SETTINGS_IS_MISSING.
     */
    private static final String VALIDATE_SET_RULE_MAPPING_SETTINGS_IS_MISSING = "app.meta.matching.set.rule.mapping.settings.is.missing";
    /**
     * The Constant VALIDATE_SET_RULE_MAPPING_COLUMN_IS_MISSING.
     */
    private static final String VALIDATE_SET_RULE_MAPPING_COLUMN_IS_MISSING = "app.meta.matching.set.rule.mapping.column.is.missing";
    /**
     * The Constant VALIDATE_SET_RULE_MAPPING_SETTINGS_NOT_EXIST.
     */
    private static final String VALIDATE_SET_RULE_MAPPING_SETTINGS_NOT_EXIST = "app.meta.matching.set.rule.mapping.settings.not.exist";
    /**
     * The Constant VALIDATE_SET_RULE_MAPPING_COLUMN_NOT_EXIST.
     */
    private static final String VALIDATE_SET_RULE_MAPPING_COLUMN_NOT_EXIST = "app.meta.matching.set.rule.mapping.column.not.exist";

    // END OF SET VALIDATION CONSTANTS.
    // --------------------------------------------------
    // MATCHING ASSIGNMENT VALIDATION CONSTANTS.
    /**
     * The Constant VALIDATE_ASSIGNMENT_NAMESPACE_NOT_FOUND.
     */
    private static final String VALIDATE_ASSIGNMENT_NAMESPACE_NOT_FOUND = "app.meta.matching.assignment.namespace.not.found";
    /**
     * The Constant VALIDATE_ASSIGNMENT_SETS_NOT_FOUND.
     */
    private static final String VALIDATE_ASSIGNMENT_SETS_NOT_FOUND = "app.meta.matching.assignment.sets.not.found";
    /**
     * The Constant VALIDATE_ASSIGNMENT_TABLES_NOT_FOUND.
     */
    private static final String VALIDATE_ASSIGNMENT_TABLES_NOT_FOUND = "app.meta.matching.assignment.tables.not.found";
    /**
     * The Constant VALIDATE_ASSIGNMENT_TABLE_MAPPING_COLUMN_NOT_EXIST.
     */
    private static final String VALIDATE_ASSIGNMENT_TABLE_MAPPING_COLUMN_NOT_EXIST = "app.meta.matching.assignment.table.mapping.column.not.exist";
    /**
     * The Constant VALIDATE_ASSIGNMENT_TABLE_MAPPING_PATH_INVALID.
     */
    private static final String VALIDATE_ASSIGNMENT_TABLE_MAPPING_PATH_INVALID = "app.meta.matching.assignment.table.mapping.path.invalid";
    /**
     * The Constant VALIDATE_ASSIGNMENT_TABLE_MAPPING_PATH_FAILED.
     */
    private static final String VALIDATE_ASSIGNMENT_TABLE_MAPPING_PATH_FAILED = "app.meta.matching.assignment.table.mapping.path.failed";

    /**
     * @author Mikhail Mikhailov on Apr 6, 2021
     * Objects, that can be checked in similar fashion.
     */
    private enum NamedValidatedObject {
        /**
         * Algorithms.
         */
        ALGORITHM(
                "app.meta.matching.algorithm.name.empty",
                "app.meta.matching.algorithm.name.invalid",
                "app.meta.matching.algorithm.name.too.long",
                "app.meta.matching.algorithm.duplicate.names"),
        /**
         * Tables.
         */
        TABLE(
                "app.meta.matching.table.name.empty",
                "app.meta.matching.table.name.invalid",
                "app.meta.matching.table.name.too.long",
                "app.meta.matching.table.duplicate.names"),
        /**
         * COLUMNS.
         */
        COLUMN(
                "app.meta.matching.column.name.empty",
                "app.meta.matching.column.name.invalid",
                "app.meta.matching.column.name.too.long",
                "app.meta.matching.column.duplicate.names"),
        /**
         * Matching rules.
         */
        RULE(
                "app.meta.matching.rule.name.empty",
                "app.meta.matching.rule.name.invalid",
                "app.meta.matching.rule.name.too.long",
                "app.meta.matching.rule.duplicate.names"),
        /**
         * Sets.
         */
        SET(
                "app.meta.matching.set.name.empty",
                "app.meta.matching.set.name.invalid",
                "app.meta.matching.set.name.too.long",
                "app.meta.matching.set.duplicate.names");

        NamedValidatedObject(String nameEmpty, String nameInvalid, String nameTooLong, String nameDuplicate) {
            this.nameDuplicate = nameDuplicate;
            this.nameEmpty = nameEmpty;
            this.nameInvalid = nameInvalid;
            this.nameTooLong = nameTooLong;
        }

        private final String nameEmpty;

        private final String nameInvalid;

        private final String nameTooLong;

        private final String nameDuplicate;

        /**
         * @return the nameEmpty
         */
        public String getNameEmpty() {
            return nameEmpty;
        }

        /**
         * @return the nameInvalid
         */
        public String getNameInvalid() {
            return nameInvalid;
        }

        /**
         * @return the nameTooLong
         */
        public String getNameTooLong() {
            return nameTooLong;
        }

        /**
         * @return the nameDuplicate
         */
        public String getNameDuplicate() {
            return nameDuplicate;
        }

        public String getObjectTypeTitle() {

            switch (this) {
                case ALGORITHM:
                    return "Matching algorithm";
                case RULE:
                    return "Rule";
                case SET:
                    return "Set";
                case TABLE:
                    return "Matching table";
                case COLUMN:
                    return "Matching column";
            }

            return StringUtils.EMPTY;
        }
    }

    /**
     * The cache component.
     */
    @Autowired
    private MatchingAlgorithmsCacheComponent matchingAlgorithmsCacheComponent;
    /**
     * UPath service.
     */
    @Autowired
    private UPathService upathService;
    /**
     * Matching storage service.
     */
    @Autowired
    private MatchingStorageService matchingStorageService;


    public Collection<ValidationResult> allow(ModelSourceContext<?> change) {
        return Collections.emptyList();
    }

    public Collection<ValidationResult> precheck(MatchingModel model) {

        List<ValidationResult> errors = new ArrayList<>();
        MatchingValidationState state = new MatchingValidationState(model);

        // 1. Algorithms
        validateAlgorithms(errors, model, state);

        // 2. Matching tables
        validateMatchingTables(errors, model, state);

        // 3. Rules
        validateRules(errors, model, state);

        // 4. Sets
        validateSets(errors, model, state);

        // 5. Validate set assignments
        validateAssignments(errors, model, state);

        return errors;
    }

    public Collection<ValidationResult> validate(MatchingModel model) {
        return Collections.emptyList();
    }

    private void validateAlgorithms(List<ValidationResult> errors, MatchingModel model, MatchingValidationState state) {

        List<MatchingAlgorithmSource> allAlgorithms = model.getAlgorithms();

        if (CollectionUtils.isEmpty(allAlgorithms)) {
            return;
        }

        // 1. Validate basic stuff - element names conformance, custom properties, name uniqueness, etc.
        errors.addAll(validateNames(allAlgorithms.stream()
                .map(MatchingAlgorithmSource::getName)
                .collect(Collectors.toList()), NamedValidatedObject.ALGORITHM));

        for (MatchingAlgorithmSource algorithmSource : allAlgorithms) {

            if (StringUtils.isBlank(algorithmSource.getName())) {
                continue;
            }

            if (Objects.isNull(algorithmSource.getMatchingStorageId())) {

                errors.add(new ValidationResult("Matching algorithm [{}] has missing matching storage id.",
                        VALIDATE_ALGORITHM_HAS_MISSING_MATCHING_STORAGE_ID, algorithmSource.getName()));
            }

            if (!state.getMatchingStorageNames().contains(algorithmSource.getMatchingStorageId())) {

                errors.add(new ValidationResult("Matching storage [{}] from algorithm definition [{}] not found.",
                        VALIDATE_ALGORITHM_MATCHING_STORAGE_NOT_FOUND,
                        algorithmSource.getMatchingStorageId(),
                        algorithmSource.getName()));
            }

            // 1.1. Custom props
            errors.addAll(validateCustomProperties(algorithmSource.getName(), algorithmSource.getCustomProperties()));

            // 1.2. Check operation ability.
            MatchingAlgorithmImpl impl = getAlgorithmImpl(model, algorithmSource);

            if (Objects.nonNull(impl)) {

                if (!impl.isReady()) {
                    errors.add(new ValidationResult("Matching algorithm [{}] is not ready for operation.",
                            VALIDATE_ALGORITHM_NOT_READY_FOR_OPERATION, impl.getName()));
                }

                if (StringUtils.isNotBlank(impl.getNote())) {
                    errors.add(new ValidationResult("Matching algorithm [{}] has additional failure notes [{}].",
                            VALIDATE_ALGORITHM_HAS_FAILURE_NOTES,
                            impl.getName(), impl.getNote()));
                }

                state.getAlgorithmImplMap().put(impl.getName(), impl);
            } else {

                errors.add(new ValidationResult("Matching algorithm [{}] can not be instantiated.",
                        VALIDATE_ALGORITHM_CAN_NOT_BE_INSTANTIATED,
                        algorithmSource.getName()));
            }
        }
    }

    private void validateMatchingTables(List<ValidationResult> errors, MatchingModel model, MatchingValidationState state) {

        List<MatchingTableSource> allTables = model.getMatchingTables();

        if (CollectionUtils.isEmpty(allTables)) {
            return;
        }

        // 1. Validate basic stuff - element names conformance, custom properties, name uniqueness, etc.
        errors.addAll(validateNames(allTables.stream()
                .map(MatchingTableSource::getName)
                .collect(Collectors.toList()), NamedValidatedObject.TABLE));

        for (MatchingTableSource tableSource : allTables) {

            errors.addAll(validateNames(tableSource.getColumns().stream()
                    .map(MatchingColumnSource::getName)
                    .collect(Collectors.toList()), NamedValidatedObject.COLUMN));
        }
    }

    private void validateRules(List<ValidationResult> errors, MatchingModel model, MatchingValidationState state) {

        List<MatchingRuleSource> allRules = model.getRules();

        if (CollectionUtils.isEmpty(allRules)) {
            return;
        }

        // 1. Basic stuff
        errors.addAll(validateNames(allRules.stream()
                .map(MatchingRuleSource::getName)
                .collect(Collectors.toList()), NamedValidatedObject.RULE));

        for (MatchingRuleSource rule : allRules) {
            validateRule(errors, rule, state);
        }
    }

    private void validateRule(List<ValidationResult> errors, MatchingRuleSource rule, MatchingValidationState state) {

        // 1.1. Custom props.
        errors.addAll(validateCustomProperties(rule.getName(), rule.getCustomProperties()));

        if (Objects.isNull(rule.getMatchingStorageId())) {

            errors.add(new ValidationResult("Matching rule [{}] has missing matching storage id.",
                    VALIDATE_RULE_HAS_MISSING_MATCHING_STORAGE_ID, rule.getName()));
        }

        if (!state.getMatchingStorageNames().contains(rule.getMatchingStorageId())) {

            errors.add(new ValidationResult("Matching storage [{}] from rule definition [{}] not found.",
                    VALIDATE_RULE_MATCHING_STORAGE_NOT_FOUND,
                    rule.getMatchingStorageId(),
                    rule.getName()));
        }

        // 1.2. Algorithm reference
        for (AlgorithmSettingsSource settings : rule.getAlgorithms()) {

            if (!state.getAlgorithmNames().contains(settings.getAlgorithmName())) {

                errors.add(new ValidationResult("Matching algorithm [{}] from rule definition [{}] not found.",
                        VALIDATE_RULE_ALGORITHM_ABSENT, settings.getAlgorithmName(), rule.getName()));
            }

            MatchingAlgorithmImpl algorithmImpl = state.getAlgorithmImplMap().get(settings.getAlgorithmName());

            if (Objects.isNull(algorithmImpl)) {
                continue;
            }

            if (!StringUtils.equals(rule.getMatchingStorageId(), algorithmImpl.getMatchingStorageId())) {

                errors.add(new ValidationResult("Matching algorithm [{}] with matching storage id [{}] " +
                        "cannot be used in rule [{}] with matching storage id [{}].",
                        VALIDATE_RULE_INVALID_ALGORITHM_STORAGE, algorithmImpl.getName(), algorithmImpl.getMatchingStorageId(),
                        rule.getName(),
                        rule.getMatchingStorageId()));
            }
        }
    }

    private void validateSets(List<ValidationResult> errors, MatchingModel model, MatchingValidationState state) {

        List<MatchingRuleSetSource> allSets = model.getSets();

        if (CollectionUtils.isEmpty(allSets)) {
            return;
        }

        // 3. Sets
        errors.addAll(validateNames(allSets.stream()
                .map(MatchingRuleSetSource::getName)
                .collect(Collectors.toList()), NamedValidatedObject.SET));

        for (MatchingRuleSetSource source : allSets) {
            validateSet(errors, source, state);
        }
    }

    private void validateSet(List<ValidationResult> errors, MatchingRuleSetSource setSource, MatchingValidationState state) {

        if (Objects.isNull(setSource.getMatchingStorageId())) {

            errors.add(new ValidationResult("Matching set [{}] has missing matching storage id.",
                    VALIDATE_SET_HAS_MISSING_MATCHING_STORAGE_ID, setSource.getName()));
        }

        if (!state.getMatchingStorageNames().contains(setSource.getMatchingStorageId())) {

            errors.add(new ValidationResult("Matching storage [{}] from set definition [{}] not found.",
                    VALIDATE_SET_MATCHING_STORAGE_NOT_FOUND,
                    setSource.getMatchingStorageId(),
                    setSource.getName()));
        }

        if (Objects.isNull(setSource.getMatchingTable())) {

            errors.add(new ValidationResult("Matching set [{}] has missing matching table name.",
                    VALIDATE_SET_HAS_MISSING_MATCHING_TABLE_NAME, setSource.getName()));
        }

        if (!state.getTableNames().contains(setSource.getMatchingTable())) {

            errors.add(new ValidationResult("Matching table [{}] from set definition [{}] not found.",
                    VALIDATE_SET_MATCHING_TABLE_NOT_FOUND,
                    setSource.getMatchingTable(),
                    setSource.getName()));
        }

        for (MatchingRuleMappingSource mappingSource : setSource.getMappings()) {

            validateRuleMapping(errors, setSource, mappingSource, state);
        }
    }

    private void validateRuleMapping(List<ValidationResult> errors, MatchingRuleSetSource setSource,
                                     MatchingRuleMappingSource mappingSource, MatchingValidationState state) {

        // A non existing rule referenced.
        if (!state.getRuleNames().contains(mappingSource.getRuleName())) {

            errors.add(new ValidationResult("Matching rule mapping in set [{}] references a non-existing rule [{}].",
                    VALIDATE_SET_RULE_MAPPING_RULE_NOT_FOUND, setSource.getName(), mappingSource.getRuleName()));

            return;
        }

        Set<String> algorithmsSettingsIds = state.getAlgorithmsSettingsIds(mappingSource.getRuleName());
        Set<String> columns = state.getColumnNames(setSource.getMatchingTable());

        for (AlgorithmMappingSource algMapping : mappingSource.getMappings()) {

            if (Objects.isNull(algMapping.getSettingsId())) {

                errors.add(new ValidationResult(
                        "Rule mapping in set [{}] for rule [{}] has missing algorithm settings id.",
                        VALIDATE_SET_RULE_MAPPING_SETTINGS_IS_MISSING, setSource.getName(), mappingSource.getRuleName()));
            }

            if (Objects.isNull(algMapping.getColumnName())) {

                errors.add(new ValidationResult(
                        "Rule mapping in set [{}] for rule [{}] has missing matching column.",
                        VALIDATE_SET_RULE_MAPPING_COLUMN_IS_MISSING, setSource.getName(), mappingSource.getRuleName()));
            }

            if (!algorithmsSettingsIds.contains(algMapping.getSettingsId())) {

                String[] splitId = StringUtils.split(algMapping.getSettingsId(), "_");
                errors.add(new ValidationResult(
                        "Rule mapping in set [{}] for rule [{}] refers to a non-existent algorithm settings [{}] with order [{}].",
                        VALIDATE_SET_RULE_MAPPING_SETTINGS_NOT_EXIST, setSource.getName(), mappingSource.getRuleName(),
                        Objects.requireNonNullElse(splitId[1], splitId[0]),
                        Objects.requireNonNullElse(splitId[2], StringUtils.EMPTY)));
            }

            if (!columns.contains(algMapping.getColumnName())) {

                errors.add(new ValidationResult(
                        "Rule mapping in set [{}] for rule [{}] refers to a non-existent column [{}] of matching table [{}].",
                        VALIDATE_SET_RULE_MAPPING_COLUMN_NOT_EXIST, setSource.getName(), mappingSource.getRuleName(), algMapping.getColumnName(),
                        setSource.getMatchingTable()));
            }
        }
    }

    private void validateAssignments(List<ValidationResult> errors, MatchingModel model, MatchingValidationState state) {

        List<NamespaceAssignmentSource> assignments = model.getAssignments();

        if (CollectionUtils.isEmpty(assignments)) {
            return;
        }

        // 2. Check namespaces for existent
        errors.addAll(assignments.stream()
                .map(NamespaceAssignmentSource::getNameSpace)
                .filter(ns -> NameSpaceUtils.find(ns) == null)
                .map(ns -> new ValidationResult("Assignment incorrect. Namespace [{}] not found.", VALIDATE_ASSIGNMENT_NAMESPACE_NOT_FOUND, ns))
                .collect(Collectors.toList()));

        // 3. Find mapping sets, missing in the model
        errors.addAll(assignments.stream()
                .map(ns -> ns.getSets().stream()
                        .distinct()
                        .filter(set -> !state.getSetNames().contains(set))
                        .collect(Collectors.toList()))
                .filter(CollectionUtils::isNotEmpty)
                .map(ns -> new ValidationResult("Assignment incorrect. Mapping sets [{}] not found.", VALIDATE_ASSIGNMENT_SETS_NOT_FOUND, ns))
                .collect(Collectors.toList()));

        errors.addAll(assignments.stream()
                .map(ns -> ns.getMatchingTables().stream()
                        .distinct()
                        .filter(table -> !state.getTableNames().contains(table))
                        .collect(Collectors.toList()))
                .filter(CollectionUtils::isNotEmpty)
                .map(ns -> new ValidationResult("Assignment incorrect. Matching tables [{}] not found.", VALIDATE_ASSIGNMENT_TABLES_NOT_FOUND, ns))
                .collect(Collectors.toList()));

        assignments.forEach(assignment -> {

        });
    }

    private void validateTableMapping(List<ValidationResult> errors, NamespaceAssignmentSource assignmentSource,
                                      MatchingTableMappingSource mappingSource, MatchingValidationState state) {

        if (!state.getTableNames().contains(mappingSource.getMatchingTableName())) {
            return;
        }

        Set<String> columnNames = state.getColumnNames(mappingSource.getMatchingTableName());

        for (MatchingColumnMappingSource mapping : mappingSource.getMappings()) {

            if (!columnNames.contains(mapping.getColumnName())) {

                errors.add(new ValidationResult(
                        "Column mapping refers to a non-existent column [{}] of matching table [{}].",
                        VALIDATE_ASSIGNMENT_TABLE_MAPPING_COLUMN_NOT_EXIST, mapping.getColumnName(),
                        mappingSource.getMatchingTableName()));
            }

            validatePath(errors, mappingSource, mapping.getPath());
        }
    }

    private void validatePath(List<ValidationResult> errors, MatchingTableMappingSource mss, String path) {

        if (Objects.nonNull(path)) {

            try {

                UPath upath = upathService.upathCreate(path);
                String upathResult = upath.toUPath();
                boolean isValid = StringUtils.equals(StringUtils.trim(path), StringUtils.trim(upathResult));
                if (!isValid) {
                    errors.add(new ValidationResult("Column mapping for matching table [{}] contains invalid UPath expression [{}], which differs from parsed result [{}].",
                            VALIDATE_ASSIGNMENT_TABLE_MAPPING_PATH_INVALID, mss.getMatchingTableName(), path, upathResult));
                } else {

                    if (!upath.getTail().isCollecting()) {
                        errors.add(new ValidationResult("Column mapping for matching table [{}] contains invalid UPath expression [{}]. "
                                + "Last element of the port mapping must be a collecting element.",
                                VALIDATE_ASSIGNMENT_TABLE_MAPPING_PATH_INVALID, mss.getMatchingTableName(), path));
                    }
                }

            } catch (UPathException e) {
                errors.add(new ValidationResult(
                        "Column mapping for matching table [{}] contains UPath expression, which didn't pass validation. Exception caught [{}].",
                        VALIDATE_ASSIGNMENT_TABLE_MAPPING_PATH_FAILED, mss.getMatchingTableName(), TextUtils.getText(e.getId().code(), e.getArgs())));
            }
        }
    }

    private Collection<ValidationResult> validateNames(Collection<String> names, NamedValidatedObject nvo) {

        Map<String, Integer> cardinality = new HashMap<>();
        List<ValidationResult> errors = new ArrayList<>();
        for (String en : names) {

            // 1. Name
            if (StringUtils.isBlank(en)) {
                String message = nvo.getObjectTypeTitle() + " name is blank. Model object names must not be blank.";
                errors.add(new ValidationResult(message, nvo.getNameEmpty(), nvo.getObjectTypeTitle()));
            } else {

                // 2. Shape
                if (!DEFAULT_MODEL_NAME_PATTERN.matcher(en).matches()) {
                    String message = nvo.getObjectTypeTitle() + " name [{}] is invalid. The name contains invalid characters.";
                    errors.add(new ValidationResult(message, nvo.getNameInvalid(), en));
                }

                // 3. Name length
                if(en.length() > 255) {
                    String message = nvo.getObjectTypeTitle() + " name too long. Name [{}], max length [{}]";
                    errors.add(new ValidationResult(message, nvo.getNameTooLong(), en, 255));
                }

                // 4. Uniqueness
                cardinality.compute(en, (k, v) -> v == null ? Integer.valueOf(1) : Integer.valueOf(v + 1));
            }
        }

        Set<String> duplicates = cardinality.entrySet().stream()
                .filter(entry -> entry.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());

        if (CollectionUtils.isNotEmpty(duplicates)) {
            String message = "Duplicate " + nvo.getObjectTypeTitle() + " name(s) [{}]";
            errors.add(new ValidationResult(message, nvo.getNameDuplicate(), String.join(", ", duplicates)));
        }

        return errors;
    }

    private MatchingAlgorithmImpl getAlgorithmImpl(MatchingModel model, MatchingAlgorithmSource source) {

        MatchingAlgorithmImpl impl = new MatchingAlgorithmImpl(source);
        impl.implement(model.getStorageId(), matchingAlgorithmsCacheComponent);

        return impl;
    }

    private class MatchingValidationState {
        private final Set<String> matchingStorageNames;
        private final Set<String> algorithmNames;
        private final Set<String> tableNames;
        private final Map<String, Set<String>> columnNamesMap;
        private final Set<String> ruleNames;
        private final Map<String, Set<String>> algorithmsSettingsIdsMap;
        private final Set<String> setNames;

        private Map<String, MatchingAlgorithmImpl> algorithmImplMap;

        /**
         * Constructor.
         *
         * @param model matching model
         */
        public MatchingValidationState(MatchingModel model) {

            this.matchingStorageNames = matchingStorageService.getMatchingStoragesInfo().getMatchingStorages()
                    .stream().map(MatchingStorageInfo::getName)
                    .collect(Collectors.toSet());

            this.algorithmNames = model.getAlgorithms().stream()
                    .map(MatchingAlgorithmSource::getName)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            this.algorithmImplMap = new HashMap<>(model.getAlgorithms().size());

            this.tableNames = new HashSet<>(model.getMatchingTables().size());
            this.columnNamesMap = new HashMap<>(model.getMatchingTables().size());

            for (MatchingTableSource tableSource : model.getMatchingTables()) {

                if (Objects.isNull(tableSource.getName())) {
                    continue;
                }

                this.tableNames.add(tableSource.getName());

                Set<String> columnNames = tableSource.getColumns().stream()
                        .map(MatchingColumnSource::getName)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet());

                this.columnNamesMap.put(tableSource.getName(), columnNames);
            }

            this.ruleNames = new HashSet<>(model.getRules().size());
            this.algorithmsSettingsIdsMap = new HashMap<>(model.getRules().size());

            for (MatchingRuleSource ruleSource : model.getRules()) {

                if (Objects.isNull(ruleSource.getName())) {
                    continue;
                }

                this.ruleNames.add(ruleSource.getName());
                this.algorithmsSettingsIdsMap.put(ruleSource.getName(), getAlgorithmsSettingsIds(ruleSource));
            }

            this.setNames = model.getSets().stream()
                    .map(MatchingRuleSetSource::getName)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
        }

        public Set<String> getMatchingStorageNames() {
            return matchingStorageNames;
        }

        public Set<String> getAlgorithmNames() {
            return algorithmNames;
        }

        public Set<String> getTableNames() {
            return tableNames;
        }

        public Set<String> getColumnNames(String tableName) {
            return columnNamesMap.getOrDefault(tableName, Collections.emptySet());
        }

        public Set<String> getRuleNames() {
            return ruleNames;
        }

        public Set<String> getSetNames() {
            return setNames;
        }

        public Map<String, MatchingAlgorithmImpl> getAlgorithmImplMap() {
            return algorithmImplMap;
        }

        public Set<String> getAlgorithmsSettingsIds(String ruleName) {
            return algorithmsSettingsIdsMap.getOrDefault(ruleName, Collections.emptySet());
        }

        private Set<String> getAlgorithmsSettingsIds(MatchingRuleSource ruleSource) {

            if (Objects.isNull(ruleSource)) {
                return Collections.emptySet();
            }

            Set<String> settingsIds = new HashSet<>();
            int order = 0;

            for (AlgorithmSettingsSource settings: ruleSource.getAlgorithms()) {
                String[] parts = new String[] {
                        ruleSource.getName(), settings.getAlgorithmName(), String.valueOf(order++)
                };

                settingsIds.add(StringUtils.join(parts, "_"));
            }

            return settingsIds;
        }
    }
}
