/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.instance;

import org.unidata.mdm.core.type.model.CreateUpdateElement;
import org.unidata.mdm.core.type.model.CustomPropertiesElement;
import org.unidata.mdm.core.type.model.IdentityElement;
import org.unidata.mdm.core.type.model.LibrarySourceElement;
import org.unidata.mdm.core.type.model.NamedDisplayableElement;
import org.unidata.mdm.matching.core.type.algorithm.MatchingAlgorithm;

import java.util.Collection;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public interface MatchingAlgorithmElement extends IdentityElement, NamedDisplayableElement,
        CustomPropertiesElement, CreateUpdateElement, LibrarySourceElement, MatchingStorageSourceElement {

    /**
     * Gets java class.
     *
     * @return the java class
     */
    String getJavaClass();

    /**
     * Gets matching algorithm instance.
     *
     * @return matching algorithm
     */
    MatchingAlgorithm getAlgorithm();

    /**
     * Gets parameters collection.
     *
     * @return parameters collection
     */
    Collection<AlgorithmParamConfigurationElement> getParameters();

    /**
     * Gets parameter by name.
     *
     * @param name the parameter name
     * @return algorithm parameter
     */
    AlgorithmParamConfigurationElement getParameter(String name);

    /**
     * Returns true, if this matching algorithm is a system one.
     *
     * @return true, if this matching algorithm is a system one.
     */
    boolean isSystem();

    /**
     * Returns true, if this matching algorithm is ready for execution.
     *
     * @return true, if this matching algorithm is ready for execution.
     */
    boolean isReady();

    /**
     * Returns true, if this matching algorithm parameters can be externally configured.
     *
     * @return true, if this matching algorithm parameters can be externally configured.
     */
    boolean isConfigurable();

    /**
     * Returns true, if this matching algorithm properties, such as name, display name, description and custom
     * properties, can be externally configured.
     *
     * @return true, if this matching algorithm properties, such as name, display name, description and custom
     * properties, can be externally configured
     */
    boolean isEditable();

    /**
     * Gets state note (if the matching algorithm can not be initialized this field will contain the reason).
     * Returns an empty string, if all right.
     *
     * @return state note
     */
    String getNote();
}
