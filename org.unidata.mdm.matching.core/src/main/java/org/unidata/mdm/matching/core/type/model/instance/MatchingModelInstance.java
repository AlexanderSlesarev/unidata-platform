/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.instance;

import org.unidata.mdm.core.type.model.ModelInstance;
import org.unidata.mdm.matching.core.type.model.MatchingModel;
import org.unidata.mdm.system.type.namespace.NameSpace;

import java.util.Collection;
import java.util.Map;

/**
 * @author Sergey Murskiy on 19.06.2021
 */
public interface MatchingModelInstance extends ModelInstance<MatchingModel> {
    /**
     * Gets all defined matching algorithms.
     *
     * @return matching algorithm elements
     */
    Collection<MatchingAlgorithmElement> getAlgorithms();

    /**
     * Gets matching algorithm by id.
     *
     * @param id the matching algorithm id
     * @return matching algorithm element
     */
    MatchingAlgorithmElement getAlgorithm(String id);

    /**
     * Gets all defined matching tables.
     *
     * @return matching table elements
     */
    Collection<MatchingTableElement> getTables();

    /**
     * Gets matching table element by id.
     *
     * @return matching table element
     */
    MatchingTableElement getTable(String id);

    /**
     * Gets all defined matching rules.
     *
     * @return matching rule elements
     */
    Collection<MatchingRuleElement> getRules();

    /**
     * Gets matching rule by id.
     *
     * @param id the matching rule id
     * @return matching rule element
     */
    MatchingRuleElement getRule(String id);

    /**
     * Gets all defined matching rule sets.
     *
     * @return matching rule set elements
     */
    Collection<MatchingRuleSetElement> getSets();

    /**
     * Gets matching rule set by id.
     *
     * @param id the matching rule set id
     * @return matching rule set element
     */
    MatchingRuleSetElement getSet(String id);

    /**
     * Gets namespace assignment element, if the given assignment key has an association (links).
     *
     * @param assignmentKey the assignment key as string
     * @return namespace link element or null
     */
    NamespaceAssignmentElement getAssignment(String assignmentKey);

    /**
     * Gets all namespace assignment elements.
     *
     * @return namespace assignment elements
     */
    Collection<NamespaceAssignmentElement> getAssignments();

    /**
     * Returns true, if the given namespace has entity to matching rule sets associations (assignments).
     *
     * @param ns the namespace
     * @return true, if the given namespace has entity to matching rule sets associations (assignments)
     */
    boolean hasAssignment(NameSpace ns, String typeName);

    /**
     * Returns true, if the given namespace has entity to matching rule sets associations (assignments).
     *
     * @param ns the namespace
     * @return true, if the given namespace has entity to matching rule sets associations (assignments)
     */
    boolean hasAssignment(String ns, String typeName);

    /**
     * Returns true, if the given assignment key has entity to matching rule sets associations (assignments).
     *
     * @param assignmentKey the assignment key
     * @return true, if the given assignment key has entity to matching rule sets associations (assignments)
     */
    boolean hasAssignment(String assignmentKey);

    /**
     * Gets matching deployments for matching storage.
     *
     * @param matchingStorageId the matching storage id
     * @return deployments
     */
    Map<String, MatchingDeploymentElement> getStorageDeployments(String matchingStorageId);

    /**
     * Gets matching deployments for matching table.
     *
     * @param matchingTableName the matching table name
     * @return deployments
     */
    Map<String, MatchingDeploymentElement> getTableDeployments(String matchingTableName);

    /**
     * Gets matching deployment.
     *
     * @param matchingTableName the matching table name
     * @param matchingStorageId the matching storage id
     * @return matching deployment
     */
    MatchingDeploymentElement getDeployment(String matchingTableName, String matchingStorageId);
}
