/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dto;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSource;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class RulesModelGetResult {
    /**
     * Matching rules, if asked as standalone.
     */
    private List<MatchingRuleSource> rules;

    /**
     * Constructor.
     *
     * @param rules matching rules
     */
    public RulesModelGetResult(List<MatchingRuleSource> rules) {
        super();
        this.rules = rules;
    }

    /**
     * Constructor.
     *
     * @param rule matching rule
     */
    public RulesModelGetResult(MatchingRuleSource rule) {
        super();
        this.rules = Collections.singletonList(rule);
    }

    /**
     * @return the rules
     */
    public List<MatchingRuleSource> getRules() {
        return Objects.isNull(rules) ? Collections.emptyList() : rules;
    }

    /**
     * @param rules the rules to set
     */
    public void setRules(List<MatchingRuleSource> rules) {
        this.rules = rules;
    }

    /**
     * Gets first value.
     *
     * @return algorithm
     */
    public MatchingRuleSource singleValue() {
        return CollectionUtils.isNotEmpty(rules)
                ? rules.get(0)
                : null;
    }
}
