/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.algorithm;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;

/**
 * @author Sergey Murskiy on 14.06.2021
 */
public class AlgorithmMappingSource implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 6433132660784508811L;

    @JacksonXmlProperty(isAttribute = true, localName = "settingsId")
    private String settingsId;

    @JacksonXmlProperty(isAttribute = true, localName = "columnName")
    private String columnName;


    /**
     * Gets the algorithm settings id.
     *
     * @return the algorithm settings id
     */
    public String getSettingsId() {
        return settingsId;
    }

    /**
     * Sets the algorithm settings id.
     *
     * @param settingsId the algorithm settings id to set
     */
    public void setSettingsId(String settingsId) {
        this.settingsId = settingsId;
    }

    /**
     * Gets the matching column.
     *
     * @return the matching column
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Sets the matching column.
     *
     * @param columnName the matching column to set
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    /**
     * Mapping with algorithm settings id.
     *
     * @param value the algorithm settings id
     * @return self
     */
    public AlgorithmMappingSource withAlgorithmSettingsId(String value) {
        setSettingsId(value);
        return this;
    }

    /**
     * Mapping with matching column.
     *
     * @param columnName the matching column
     * @return self
     */
    public AlgorithmMappingSource withColumnName(String columnName) {
        setColumnName(columnName);
        return this;
    }
}
