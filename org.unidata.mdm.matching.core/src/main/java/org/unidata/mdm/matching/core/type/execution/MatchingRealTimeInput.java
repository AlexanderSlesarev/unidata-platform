/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.execution;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.matching.core.type.data.MatchingRecordKey;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Sergey Murskiy on 08.08.2021
 */
public class MatchingRealTimeInput {
    /**
     * Records keys for matching execution.
     */
    private List<MatchingRecordKey> processedKeys = new ArrayList<>();
    /**
     * Matching record keys for exclude from clusters.
     */
    private List<MatchingRecordKey> excludedKeys = new ArrayList<>();

    /**
     * Gets record keys for matching executions.
     *
     * @return records keys.
     */
    public List<MatchingRecordKey> getProcessedKeys() {
        return processedKeys;
    }

    /**
     * Sets record keys for matching execution.
     *
     * @param processedKeys records keys
     */
    public void setProcessedKeys(Collection<MatchingRecordKey> processedKeys) {
        this.processedKeys = new ArrayList<>(processedKeys);
    }

    /**
     * Gets record keys for exclude from clusters.
     *
     * @return records keys
     */
    public List<MatchingRecordKey> getExcludedKeys() {
        return excludedKeys;
    }

    /**
     * Sets records keys for exclude from clusters.
     *
     * @param excludedKeys matching record keys
     */
    public void setExcludedKeys(Collection<MatchingRecordKey> excludedKeys) {
        this.excludedKeys = new ArrayList<>(excludedKeys);
    }

    /**
     * Gets is real time matching input empty.
     *
     * @return is real time matching input empty
     */
    public boolean isEmpty() {
        return CollectionUtils.isEmpty(processedKeys) && CollectionUtils.isEmpty(excludedKeys);
    }

    /**
     * Append input.
     *
     * @param input input
     */
    public void append(MatchingRealTimeInput input) {
        excludedKeys.addAll(input.getExcludedKeys());
        processedKeys.addAll(input.getProcessedKeys());
    }
}
