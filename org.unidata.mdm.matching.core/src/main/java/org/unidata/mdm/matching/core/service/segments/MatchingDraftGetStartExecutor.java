/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.matching.core.service.segments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.unidata.mdm.draft.context.DraftGetContext;
import org.unidata.mdm.draft.dto.DraftGetResult;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.matching.core.module.MatchingCoreModule;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
@Component(MatchingDraftGetStartExecutor.SEGMENT_ID)
public class MatchingDraftGetStartExecutor extends Start<DraftGetContext, DraftGetResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MatchingCoreModule.MODULE_ID + "[MATCHING_DRAFT_GET_START]";

    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MatchingCoreModule.MODULE_ID + ".matching.get.start.description";

    /**
     * The DS.
     */
    @Autowired
    private DraftService draftService;

    /**
     * Constructor.
     */
    public MatchingDraftGetStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftGetContext.class, DraftGetResult.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(@NonNull DraftGetContext ctx) {
        // NOOP. Start does nothing here.
        setup(ctx);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String subject(DraftGetContext ctx) {
        // No subject for this type of pipelines
        // This may be storage id in the future
        setup(ctx);
        return null;
    }

    protected void setup(DraftGetContext ctx) {

        if (ctx.setUp()) {
            return;
        }

        Draft draft = ctx.currentDraft();
        ctx.currentEdition(draftService.current(draft.getDraftId(), true));

        ctx.setUp(true);
    }
}
