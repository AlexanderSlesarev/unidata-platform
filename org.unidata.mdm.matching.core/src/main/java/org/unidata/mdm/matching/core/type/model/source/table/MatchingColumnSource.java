/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.table;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.matching.core.type.data.MatchingDataType;

import java.io.Serializable;

/**
 * @author Sergey Murskiy on 14.06.2021
 */
public class MatchingColumnSource implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -1081285246366999098L;

    @JacksonXmlProperty(isAttribute = true, localName = "name")
    private String name;

    @JacksonXmlProperty(isAttribute = true, localName = "displayName")
    private String displayName;

    @JacksonXmlProperty(isAttribute = true, localName = "type")
    private MatchingDataType type;

    /**
     * Gets the column name.
     *
     * @return the column name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the column name.
     *
     * @param name the column name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the display name.
     *
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the display name.
     *
     * @param displayName the display name to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Gets the column type.
     *
     * @return the column type
     */
    public MatchingDataType getType() {
        return type;
    }

    /**
     * Sets the column type.
     *
     * @param type the column type to set
     */
    public void setType(MatchingDataType type) {
        this.type = type;
    }

    /**
     * Matching column with name.
     *
     * @param name the column name to set
     * @return self
     */
    public MatchingColumnSource withName(String name) {
        setName(name);
        return this;
    }

    /**
     * Matching column with display name.
     *
     * @param displayName the column display name to set
     * @return self
     */
    public MatchingColumnSource withDisplayName(String displayName) {
        setDisplayName(displayName);
        return this;
    }

    /**
     * Matching column with type.
     *
     * @param type the column type to set
     * @return self
     */
    public MatchingColumnSource withType(MatchingDataType type) {
        setType(type);
        return this;
    }
}
