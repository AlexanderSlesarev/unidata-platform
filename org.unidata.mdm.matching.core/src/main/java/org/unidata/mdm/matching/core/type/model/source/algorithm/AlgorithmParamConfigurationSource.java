/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.algorithm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.matching.core.type.algorithm.AlgorithmParameterType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
public class AlgorithmParamConfigurationSource implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 7074622553681079321L;

    @JacksonXmlProperty(isAttribute = true)
    private String name;

    @JacksonXmlProperty(isAttribute = true)
    private String displayName;

    @JacksonXmlProperty(isAttribute = true)
    private String description;

    @JacksonXmlProperty(isAttribute = true)
    private boolean required;

    @JacksonXmlProperty(isAttribute = true)
    private AlgorithmParameterType type;

    @JacksonXmlProperty(localName = "defaultValue")
    private AlgorithmParameterSource defaultValue;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "enumValues")
    private List<EnumValueSource> enumValues;

    // Not serializable section
    @JsonIgnore
    protected transient Supplier<String> displayNameSupplier;

    @JsonIgnore
    protected transient Supplier<String> descriptionSupplier;

    /**
     * Gets the value of the name property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return Objects.nonNull(displayNameSupplier) ? displayNameSupplier.get() : displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @param displayNameSupplier the displayNameSupplier to set
     */
    public void setDisplayName(Supplier<String> displayNameSupplier) {
        this.displayNameSupplier = displayNameSupplier;
    }

    /**
     * Gets the value of the description property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescription() {
        return Objects.nonNull(descriptionSupplier) ? descriptionSupplier.get() : description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * @param descriptionSupplier the descriptionSupplier to set
     */
    public void setDescription(Supplier<String> descriptionSupplier) {
        this.descriptionSupplier = descriptionSupplier;
    }

    /**
     * Gets the value of the required property.
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * Sets the value of the required property.
     */
    public void setRequired(boolean value) {
        this.required = value;
    }

    /**
     * Gets parameter type.
     *
     * @return the type
     */
    public AlgorithmParameterType getType() {
        return type;
    }

    /**
     * Sets parameter type.
     *
     * @param type the type to set
     */
    public void setType(AlgorithmParameterType type) {
        this.type = type;
    }

    /**
     * Gets param default value.
     *
     * @return the default value
     */
    public AlgorithmParameterSource getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets param default value.
     *
     * @param defaultValue the default value
     */
    public void setDefaultValue(AlgorithmParameterSource defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Gets enum values.
     *
     * @return enum values
     */
    public List<EnumValueSource> getEnumValues() {
        if (Objects.isNull(enumValues)) {
            enumValues = new ArrayList<>();
        }

        return enumValues;
    }

    /**
     * Sets enum values
     *
     * @param enumValues the enum values to set
     */
    public void setEnumValues(List<EnumValueSource> enumValues) {
        this.enumValues = enumValues;
    }

    /**
     * Algorithm parameter with name.
     *
     * @param value the name
     * @return self
     */
    public AlgorithmParamConfigurationSource withName(String value) {
        setName(value);
        return self();
    }

    /**
     * Algorithm parameter with display name.
     *
     * @param value the display name
     * @return self
     */
    public AlgorithmParamConfigurationSource withDisplayName(String value) {
        setDisplayName(value);
        return self();
    }

    /**
     * Algorithm parameter with display name.
     *
     * @param value the display name
     * @return self
     */
    public AlgorithmParamConfigurationSource withDisplayName(Supplier<String> value) {
        setDisplayName(value);
        return self();
    }

    /**
     * Algorithm parameter with description.
     *
     * @param value the description
     * @return self
     */
    public AlgorithmParamConfigurationSource withDescription(String value) {
        setDescription(value);
        return self();
    }

    /**
     * Algorithm parameter with description.
     *
     * @param value the description
     * @return self
     */
    public AlgorithmParamConfigurationSource withDescription(Supplier<String> value) {
        setDescription(value);
        return self();
    }

    /**
     * Algorithm parameter with required property.
     *
     * @param value the required property
     * @return self
     */
    public AlgorithmParamConfigurationSource withRequired(boolean value) {
        setRequired(value);
        return self();
    }

    /**
     * Algorithm parameter with type.
     *
     * @param value the parameter type
     * @return self
     */
    public AlgorithmParamConfigurationSource withType(AlgorithmParameterType value) {
        setType(value);
        return self();
    }

    /**
     * Algorithm parameter with default value.
     *
     * @param value the default value
     * @return self
     */
    public AlgorithmParamConfigurationSource withDefaultValue(AlgorithmParameterSource value) {
        setDefaultValue(value);
        return self();
    }

    /**
     * Algorithm parameter with enum values.
     *
     * @param values the enum values to set
     * @return self
     */
    public AlgorithmParamConfigurationSource withEnumValues(EnumValueSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withEnumValues(Arrays.asList(values));
        }

        return self();
    }

    /**
     * Algorithm parameter with enum values.
     *
     * @param values the enum values to set
     * @return self
     */
    public AlgorithmParamConfigurationSource withEnumValues(Collection<EnumValueSource> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getEnumValues().addAll(values);
        }

        return self();
    }

    private AlgorithmParamConfigurationSource self() {
        return this;
    }
}
