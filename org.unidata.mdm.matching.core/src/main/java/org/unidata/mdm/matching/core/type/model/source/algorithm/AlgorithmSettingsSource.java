/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.type.model.source.algorithm;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 12.06.2021
 */
public class AlgorithmSettingsSource implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -2785583720789867890L;

    @JacksonXmlProperty(isAttribute = true, localName = "id")
    private String id;

    @JacksonXmlProperty(isAttribute = true, localName = "algorithmName")
    private String algorithmName;

    @JacksonXmlElementWrapper(localName = "parameters")
    @JacksonXmlProperty(localName = "parameter")
    private List<AlgorithmParameterSource> parameters;

    /**
     * Gets settings id.
     *
     * @return settings id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets settings id.
     *
     * @param id the settings id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the algorithm name.
     *
     * @return the algorithm name
     */
    public String getAlgorithmName() {
        return algorithmName;
    }

    /**
     * Sets the algorithm name.
     *
     * @param algorithmName the algorithm name to set
     */
    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    /**
     * Gets algorithm parameters.
     *
     * @return algorithm parameters
     */
    public List<AlgorithmParameterSource> getParameters() {
        if (Objects.isNull(parameters)) {
            parameters = new ArrayList<>();
        }

        return parameters;
    }

    /**
     * Sets algorithm parameters.
     *
     * @param parameters algorithm parameters to set
     */
    public void setParameters(List<AlgorithmParameterSource> parameters) {
        this.parameters = parameters;
    }

    /**
     * Algorithm configuration with settings id.
     *
     * @param id the settings id to set
     * @return self
     */
    public AlgorithmSettingsSource withId(String id) {
        setId(id);
        return this;
    }

    /**
     * Algorithm configuration with algorithm name.
     *
     * @param algorithmName the algorithm name to set
     * @return self
     */
    public AlgorithmSettingsSource withAlgorithmName(String algorithmName) {
        setAlgorithmName(algorithmName);
        return this;
    }

    /**
     * Algorithm configuration with algorithm parameters
     *
     * @param values algorithm parameters
     * @return self
     */
    public AlgorithmSettingsSource withParameters(AlgorithmParameterSource... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withParameters(Arrays.asList(values));
        }

        return this;
    }

    /**
     * Algorithm configuration with algorithm parameters
     *
     * @param values algorithm parameters
     * @return self
     */
    public AlgorithmSettingsSource withParameters(Collection<AlgorithmParameterSource> values) {
        if(CollectionUtils.isNotEmpty(values)) {
            getParameters().addAll(values);
        }

        return this;
    }
}
