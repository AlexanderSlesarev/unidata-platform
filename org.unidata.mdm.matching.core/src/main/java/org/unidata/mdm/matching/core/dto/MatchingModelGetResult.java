/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dto;

import org.unidata.mdm.core.dto.AbstractModelResult;
import org.unidata.mdm.draft.type.DraftPayloadResponse;
import org.unidata.mdm.matching.core.configuration.MatchingModelIds;

/**
 * @author Sergey Murskiy on 14.06.2021
 */
public class MatchingModelGetResult extends AbstractModelResult implements DraftPayloadResponse {
    /**
     * Algorithms.
     */
    private AlgorithmsModelGetResult algorithms;
    /**
     * Matching tables.
     */
    private MatchingTablesModelGetResult matchingTables;
    /**
     * Rules.
     */
    private RulesModelGetResult rules;
    /**
     * Sets.
     */
    private SetsModelGetResult sets;
    /**
     * Assignments.
     */
    private AssignmentsModelGetResult assignments;

    /**
     * Constructor.
     */
    public MatchingModelGetResult() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return MatchingModelIds.DEFAULT_MODEL_INSTANCE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return MatchingModelIds.MATCHING;
    }

    /**
     * @return the algorithms
     */
    public AlgorithmsModelGetResult getAlgorithms() {
        return algorithms;
    }

    /**
     * @param algorithms the algorithms to set
     */
    public void setAlgorithms(AlgorithmsModelGetResult algorithms) {
        this.algorithms = algorithms;
    }

    /**
     * @return matching tables
     */
    public MatchingTablesModelGetResult getMatchingTables() {
        return matchingTables;
    }

    /**
     * @param matchingTables the matching tables to set
     */
    public void setMatchingTables(MatchingTablesModelGetResult matchingTables) {
        this.matchingTables = matchingTables;
    }

    /**
     * @return the rules
     */
    public RulesModelGetResult getRules() {
        return rules;
    }

    /**
     * @param rules the rules to set
     */
    public void setRules(RulesModelGetResult rules) {
        this.rules = rules;
    }

    /**
     * @return the sets
     */
    public SetsModelGetResult getSets() {
        return sets;
    }

    /**
     * @param sets the sets to set
     */
    public void setSets(SetsModelGetResult sets) {
        this.sets = sets;
    }

    /**
     * @return the assignments
     */
    public AssignmentsModelGetResult getAssignments() {
        return assignments;
    }

    /**
     * @param assignments the assignments to set
     */
    public void setAssignments(AssignmentsModelGetResult assignments) {
        this.assignments = assignments;
    }
}
