/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.context;

import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.matching.core.type.model.instance.MatchingDeploymentElement;
import org.unidata.mdm.system.context.StorageCapableContext;
import org.unidata.mdm.system.context.StorageId;

import java.util.Map;

/**
 * @author Sergey Murskiy on 18.08.2021
 */
public interface MatchingDeploymentHolderContext extends StorageCapableContext {

    /**
     * Matching storage deployments.
     */
    StorageId SID_MATCHING_STORAGE_DEPLOYMENTS = new StorageId("MATCHING_STORAGE_DEPLOYMENTS");

    /**
     * Puts matching deployments to storage.
     *
     * @param deployments matching deployments to set
     */
    default void deployments(Map<String, MatchingDeploymentElement> deployments) {
        putToStorage(SID_MATCHING_STORAGE_DEPLOYMENTS, deployments);
    }

    /**
     * Gets matching deployments from storage.
     *
     * @return matching storage deployments
     */
    default Map<String, MatchingDeploymentElement> deployments() {
        return getFromStorage(SID_MATCHING_STORAGE_DEPLOYMENTS);
    }

    /**
     * Gets matching table deployment.
     *
     * @param matchingTableName the matching table name
     * @return matching deployment
     */
    default MatchingDeploymentElement deployment(String matchingTableName) {
        Map<String, MatchingDeploymentElement> deployments = deployments();

        return MapUtils.isNotEmpty(deployments)
                ? deployments.get(matchingTableName)
                : null;
    }
}
