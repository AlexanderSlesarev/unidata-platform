/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dao.impl;

import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.unidata.mdm.core.dao.impl.BoundedDAOImpl;
import org.unidata.mdm.matching.core.dao.MatchingModelDAO;
import org.unidata.mdm.matching.core.po.MatchingModelPO;

/**
 * @author Sergey Murskiy on 22.06.2021
 */
@Repository
public class MatchingModelDAOImpl extends BoundedDAOImpl implements MatchingModelDAO {
    /**
     * Default row mapper.
     */
    private static final RowMapper<MatchingModelPO> DEFAULT_ROW_MAPPER = (rs, row) -> {

        MatchingModelPO result = new MatchingModelPO();
        result.setStorageId(rs.getString(MatchingModelPO.FIELD_STORAGE_ID));
        result.setRevision(rs.getInt(MatchingModelPO.FIELD_REVISION));
        result.setOperationId(rs.getString(MatchingModelPO.FIELD_OPERATION_ID));
        result.setDescription(rs.getString(MatchingModelPO.FIELD_DESCRIPTION));
        result.setCreatedBy(rs.getString(MatchingModelPO.FIELD_CREATED_BY));
        result.setCreateDate(rs.getTimestamp(MatchingModelPO.FIELD_CREATE_DATE));
        result.setContent(rs.getBytes(MatchingModelPO.FIELD_CONTENT));

        return result;
    };

    /**
     * Queries.
     */
    private final String lastSQL;
    private final String currentSQL;
    private final String previousSQL;
    private final String loadSQL;
    private final String saveSQL;
    private final String removeSQL;

    /**
     * Constructor.
     */
    @Autowired
    public MatchingModelDAOImpl(
            @Qualifier("matchingDataSource") final DataSource dataSource,
            @Qualifier("matching-model-sql") final Properties sql) {
        super(dataSource, MatchingModelPO.TABLE_NAME);
        lastSQL = sql.getProperty("lastSQL");
        currentSQL = sql.getProperty("currentSQL");
        previousSQL = sql.getProperty("previousSQL");
        loadSQL = sql.getProperty("loadSQL");
        saveSQL = sql.getProperty("saveSQL");
        removeSQL = sql.getProperty("removeSQL");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int latest(String storageId) {
        return getJdbcTemplate().query(lastSQL, rs -> rs.next() ? rs.getInt(MatchingModelPO.FIELD_REVISION) : 0, storageId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingModelPO current(String storageId) {
        return getJdbcTemplate().query(currentSQL, rs -> rs.next() ? DEFAULT_ROW_MAPPER.mapRow(rs, 0) : null, storageId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingModelPO previous(String storageId, int revision) {
        return getJdbcTemplate().query(previousSQL, rs -> rs.next() ? DEFAULT_ROW_MAPPER.mapRow(rs, 0) : null, storageId, revision);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MatchingModelPO> load(String storageId, int from, int count, boolean withData) {
        return getJdbcTemplate().query(loadSQL, DEFAULT_ROW_MAPPER, withData, storageId, from, count);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int save(MatchingModelPO po) {
        return getJdbcTemplate().queryForObject(saveSQL, Integer.class,
                po.getStorageId(),
                po.getRevision(),
                po.getDescription(),
                po.getOperationId(),
                po.getContent(),
                po.getCreatedBy());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(String storageId, int revision) {
        getJdbcTemplate().update(removeSQL, storageId, revision);
    }
}
