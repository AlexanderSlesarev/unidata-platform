/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.service.impl.instance;

import org.unidata.mdm.core.service.CustomPropertiesSupport;
import org.unidata.mdm.core.service.UPathService;
import org.unidata.mdm.core.type.model.instance.AbstractModelInstanceImpl;
import org.unidata.mdm.matching.core.configuration.MatchingModelIds;
import org.unidata.mdm.matching.core.service.impl.MatchingAlgorithmsCacheComponent;
import org.unidata.mdm.matching.core.type.model.MatchingModel;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.AlgorithmSettingsElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingAlgorithmElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingColumnElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingDeploymentElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingModelInstance;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleMappingElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingRuleSetElement;
import org.unidata.mdm.matching.core.type.model.instance.MatchingTableElement;
import org.unidata.mdm.matching.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.system.type.namespace.NameSpace;
import org.unidata.mdm.system.util.NameSpaceUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 17.06.2021
 */
public class MatchingModelInstanceImpl extends AbstractModelInstanceImpl<MatchingModel>
        implements MatchingModelInstance, CustomPropertiesSupport {

    /**
     * Matching algorithms.
     */
    private final Map<String, MatchingAlgorithmElement> algorithms;
    /**
     * Matching tables.
     */
    private final Map<String, MatchingTableElement> matchingTables;
    /**
     * Matching rules.
     */
    private final Map<String, MatchingRuleElement> rules;
    /**
     * Matching rule sets.
     */
    private final Map<String, MatchingRuleSetElement> sets;
    /**
     * Assignments by assignment key.
     */
    private final Map<String, NamespaceAssignmentElement> assignments;

    private final Map<String, Map<String, MatchingDeploymentElement>> tableDeployments;
    private final Map<String, Map<String, MatchingDeploymentElement>> storageDeployments;

    /**
     * Constructor.
     *
     * @param model matching model source
     * @param ups   upath service
     */
    public MatchingModelInstanceImpl(MatchingModel model, UPathService ups,
                                     MatchingAlgorithmsCacheComponent cacheComponent) {
        super(model);

        algorithms = model.getAlgorithms().stream()
                .map(MatchingAlgorithmImpl::new)
                .peek(i -> i.implement(getStorageId(), cacheComponent))
                .collect(Collectors.toMap(MatchingAlgorithmElement::getId, Function.identity()));

        matchingTables = model.getMatchingTables().stream()
                .map(MatchingTableImpl::new)
                .collect(Collectors.toMap(MatchingTableElement::getId, Function.identity()));

        rules = model.getRules().stream()
                .map(s -> new MatchingRuleImpl(s, this))
                .collect(Collectors.toMap(MatchingRuleElement::getId, Function.identity()));

        sets = model.getSets().stream()
                .map(s -> new MatchingRuleSetImpl(s, this))
                .collect(Collectors.toMap(MatchingRuleSetElement::getId, Function.identity()));

        assignments = model.getAssignments().stream()
                .map(source -> new NamespaceAssignmentImpl(this, source, ups))
                .collect(Collectors.toMap(NamespaceAssignmentElement::getId, Function.identity()));

        tableDeployments = new HashMap<>();
        storageDeployments = new HashMap<>();

        Map<MatchingTableElement, Map<String, List<MatchingRuleSetElement>>> grouping = getAssignments().stream()
                .flatMap(assignment -> assignment.getAssignedSets().stream())
                .distinct()
                .collect(Collectors.groupingBy(MatchingRuleSetElement::getMatchingTable,
                        Collectors.groupingBy(MatchingRuleSetElement::getMatchingStorageId)));

        grouping.forEach((table, tableAssignments) -> tableAssignments.forEach((storage, assignedSets) -> {
            Map<MatchingColumnElement, List<AlgorithmSettingsElement>> columnAssignments = new HashMap<>();

            for (MatchingRuleSetElement set : assignedSets) {
                for (MatchingRuleMappingElement ruleMapping : set.getMappings()) {
                    for (AlgorithmMappingElement algorithmMapping : ruleMapping.getMappings()) {
                        columnAssignments.computeIfAbsent(algorithmMapping.getColumn(), k -> new ArrayList<>())
                                .add(algorithmMapping.getAlgorithmSettings());
                    }
                }
            }

            MatchingDeploymentImpl matchingDeployment = new MatchingDeploymentImpl(storage, table, columnAssignments);

            tableDeployments.computeIfAbsent(matchingDeployment.getMatchingTable().getName(), k -> new HashMap<>())
                    .put(storage, matchingDeployment);
            storageDeployments.computeIfAbsent(matchingDeployment.getMatchingStorageId(), k -> new HashMap<>())
                    .put(matchingDeployment.getMatchingTableName(), matchingDeployment);
        }));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MatchingAlgorithmElement> getAlgorithms() {
        return algorithms.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingAlgorithmElement getAlgorithm(String id) {
        return algorithms.get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MatchingTableElement> getTables() {
        return matchingTables.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingTableElement getTable(String id) {
        return matchingTables.get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MatchingRuleElement> getRules() {
        return rules.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingRuleElement getRule(String id) {
        return rules.get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MatchingRuleSetElement> getSets() {
        return sets.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingRuleSetElement getSet(String id) {
        return sets.get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<NamespaceAssignmentElement> getAssignments() {
        return assignments.values();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NamespaceAssignmentElement getAssignment(String assignmentKey) {
        return assignments.get(assignmentKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasAssignment(NameSpace ns, String typeName) {
        return Objects.nonNull(ns) && assignments.containsKey(NameSpaceUtils.join(ns, typeName));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasAssignment(String ns, String typeName) {
        return assignments.containsKey(NameSpaceUtils.join(ns, typeName));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasAssignment(String assignmentKey) {
        return assignments.containsKey(assignmentKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return MatchingModelIds.DEFAULT_MODEL_INSTANCE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return MatchingModelIds.MATCHING;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, MatchingDeploymentElement> getTableDeployments(String matchingTableName) {
        return Objects.isNull(tableDeployments.get(matchingTableName))
                ? Collections.emptyMap()
                : tableDeployments.get(matchingTableName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, MatchingDeploymentElement> getStorageDeployments(String matchingStorageId) {
        return Objects.isNull(storageDeployments.get(matchingStorageId))
                ? Collections.emptyMap()
                : storageDeployments.get(matchingStorageId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingDeploymentElement getDeployment(String matchingTableName, String matchingStorageId) {
        return tableDeployments.getOrDefault(matchingTableName, Collections.emptyMap()).get(matchingStorageId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MatchingModel toSource() {

        return new MatchingModel()
                .withDescription(getDescription())
                .withDisplayName(getDisplayName())
                .withName(getName())
                .withStorageId(getStorageId())
                .withCreateDate(getCreateDate())
                .withCreatedBy(getCreatedBy())
                .withVersion(getVersion())
                .withCustomProperties(getSourceCustomProperties())
                .withAlgorithms(algorithms.values().stream()
                        .map(MatchingAlgorithmImpl.class::cast)
                        .map(MatchingAlgorithmImpl::getSource)
                        .collect(Collectors.toList()))
                .withMatchingTables(matchingTables.values().stream()
                        .map(MatchingTableImpl.class::cast)
                        .map(MatchingTableImpl::getSource)
                        .collect(Collectors.toList()))
                .withRules(rules.values().stream()
                        .map(MatchingRuleImpl.class::cast)
                        .map(MatchingRuleImpl::getSource)
                        .collect(Collectors.toList()))
                .withSets(sets.values().stream()
                        .map(MatchingRuleSetImpl.class::cast)
                        .map(MatchingRuleSetImpl::getSource)
                        .collect(Collectors.toList()))
                .withAssignments(assignments.values().stream()
                        .map(NamespaceAssignmentImpl.class::cast)
                        .map(NamespaceAssignmentImpl::getSource)
                        .collect(Collectors.toList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return algorithms.isEmpty()
                && matchingTables.isEmpty()
                && rules.isEmpty()
                && sets.isEmpty()
                && assignments.isEmpty();
    }
}
