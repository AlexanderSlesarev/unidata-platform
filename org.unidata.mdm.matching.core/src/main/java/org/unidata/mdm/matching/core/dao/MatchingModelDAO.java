/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.matching.core.dao;

import java.util.List;

import org.unidata.mdm.core.dao.BoundedDAO;
import org.unidata.mdm.matching.core.po.MatchingModelPO;

/**
 * Matching model DAO.
 *
 * @author Sergey Murskiy on 22.06.2021
 */
public interface MatchingModelDAO extends BoundedDAO {
    /**
     * Gets the latest revision of the model.
     *
     * @param storageId the storage id
     * @return revision
     */
    int latest(String storageId);

    /**
     * Loads current version of matching model set for a storage id.
     *
     * @param storageId the storage id
     * @return matching model object or null, if there are no saved enumerations.
     */
    MatchingModelPO current(String storageId);

    /**
     * Loads current version of the model.
     *
     * @param storageId the storage id
     * @param revision the revision we're interested in
     * @return model object or null, if there are no saved models.
     */
    MatchingModelPO previous(String storageId, int revision);
    /**
     * Loads several matching model versions sorted by revision / create date.
     *
     * @param storageId the storage id
     * @param from start from
     * @param count return count
     * @param withData load 'content' field or not
     * @return collection of matching model objects
     */
    List<MatchingModelPO> load(String storageId, int from, int count, boolean withData);

    /**
     * Saves a new version of matching model.
     *
     * @param po the PO
     * @return revision put
     */
    int save(MatchingModelPO po);

    /**
     * Deletes a matching model revision.
     * @param storageId the storage id
     * @param revision the revision
     */
    void remove(String storageId, int revision);
}
