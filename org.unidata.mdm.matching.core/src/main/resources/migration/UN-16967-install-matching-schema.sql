create table matching_model (
    storage_id varchar(128) not null,
    revision integer not null,
    description text,
    operation_id varchar(128),
    content bytea not null,
    created_by varchar(256) not null,
    create_date timestamptz not null default current_timestamp,
    constraint pk_matching_model_storage_id_revision primary key(storage_id, revision)
);
