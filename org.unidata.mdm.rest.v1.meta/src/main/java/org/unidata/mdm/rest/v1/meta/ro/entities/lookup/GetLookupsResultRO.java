package org.unidata.mdm.rest.v1.meta.ro.entities.lookup;

import java.util.List;

import org.unidata.mdm.rest.v1.meta.ro.entities.LookupEntityRO;

/**
 * Get lookups result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class GetLookupsResultRO {

    private List<LookupEntityRO> lookupEntities;

    public List<LookupEntityRO> getLookupEntities() {
        return lookupEntities;
    }

    public void setLookupEntities(List<LookupEntityRO> lookupEntities) {
        this.lookupEntities = lookupEntities;
    }
}
