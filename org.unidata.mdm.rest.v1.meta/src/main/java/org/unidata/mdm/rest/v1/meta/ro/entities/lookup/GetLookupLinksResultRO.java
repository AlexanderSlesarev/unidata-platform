package org.unidata.mdm.rest.v1.meta.ro.entities.lookup;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.meta.ro.references.ReferenceInfoRO;

/**
 * Get lookup links result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class GetLookupLinksResultRO extends DetailedOutputRO {

    private List<ReferenceInfoRO> links;

    public List<ReferenceInfoRO> getLinks() {
        return links;
    }

    public void setLinks(List<ReferenceInfoRO> links) {
        this.links = links;
    }
}
