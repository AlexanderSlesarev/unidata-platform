/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.meta.ro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov on Dec 18, 2020
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpsertGenericModelInfoRequestRO {

    private Long draftId;
    private Long parentDraftId;
    private String name;
    private String displayName;
    private String description;

    /**
     * Constructor.
     */
    public UpsertGenericModelInfoRequestRO() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the draftId
     */
    public Long getDraftId() {
        return draftId;
    }

    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

    /**
     * @return the parentDraftId
     */
    public Long getParentDraftId() {
        return parentDraftId;
    }

    /**
     * @param parentDraftId the parentDraftId to set
     */
    public void setParentDraftId(Long parentDraftId) {
        this.parentDraftId = parentDraftId;
    }
}
