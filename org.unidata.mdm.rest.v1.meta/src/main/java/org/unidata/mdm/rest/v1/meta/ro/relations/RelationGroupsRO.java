/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro.relations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * The Class presents information about group, place on UI and presented relations.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RelationGroupsRO {

    /**
     * list of grouped relations
     */
    private List<String> relations = new ArrayList<>();
    /**
     * the column number
     */
    private int column;
    /**
     * the row number
     */
    private int row;
    /**
     * relation type
     */
    private RelationTypeRO relType;
    /**
     * the title
     */
    private String title;

    public List<String> getRelations() {
        return relations;
    }

    public void setRelations(List<String> relations) {
        this.relations = relations;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    /**
     * relation type
     */
    @JsonGetter
    public String getRelType() {
        return relType.value();
    }

    @JsonIgnore
    public void setRelType(RelationTypeRO relType) {
        this.relType = relType;
    }

    @JsonSetter
    public void setRelType(String relType) {
        this.relType = RelationTypeRO.fromValue(relType);
    }

    public RelationGroupsRO withRelations(Collection<String> values) {
        if (values != null) {
            getRelations().addAll(values);
        }
        return this;
    }

    public RelationGroupsRO withRow(int value) {
        setRow(value);
        return this;
    }

    public RelationGroupsRO withColumn(int value) {
        setColumn(value);
        return this;
    }

    public RelationGroupsRO withTitle(String value) {
        setTitle(value);
        return this;
    }

    public RelationGroupsRO withRelType(RelationTypeRO relType) {
        setRelType(relType);
        return this;
    }

}
