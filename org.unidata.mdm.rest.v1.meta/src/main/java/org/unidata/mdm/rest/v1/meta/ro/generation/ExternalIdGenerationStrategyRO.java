/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro.generation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov
 * Base class for gen. type REST descriptors.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "@type")
@JsonSubTypes({
    @Type(value = RandomExternalIdGenerationStrategyRO.class, name = "RANDOM"),
    @Type(value = ConcatExternalIdGenerationStrategyRO.class, name = "CONCAT"),
    @Type(value = SequenceExternalIdGenerationStrategyRO.class, name = "SEQUENCE"),
    @Type(value = CustomExternalIdGenerationStrategyRO.class, name = "CUSTOM")
})
@Schema(oneOf = {RandomExternalIdGenerationStrategyRO.class,
    ConcatExternalIdGenerationStrategyRO.class,
    SequenceExternalIdGenerationStrategyRO.class,
    CustomExternalIdGenerationStrategyRO.class})
public abstract class ExternalIdGenerationStrategyRO {
    /**
     * Constructor.
     */
    public ExternalIdGenerationStrategyRO() {
        super();
    }
    /**
     * @return the strategyType
     */
    public abstract ExternalIdGenerationTypeRO getStrategyType();
}
