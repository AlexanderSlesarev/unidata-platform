package org.unidata.mdm.rest.v1.meta.ro.groups;

/**
 * Upsert entity group result
 *
 * @author Alexandr Serov
 * @since 27.11.2020
 **/
public class UpsertEntityGroupRequestRO {

    private FilledEntityGroupMappingRO group;
    private Long draftId;

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

    public FilledEntityGroupMappingRO getGroup() {
        return group;
    }

    public void setGroup(FilledEntityGroupMappingRO group) {
        this.group = group;
    }
}
