/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The Enum MetaTypeRO.
 * @author ilya.bykov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public enum MetaTypeRO {
	/** The entity. */
	ENTITY,

	/** The lookup. */
	LOOKUP,

	/** The enum. */
	ENUM,

	/** The classifier. */
	CLASSIFIER,

	/** The measure. */
	MEASURE,

	/** The custom cf. */
	CUSTOM_CF,

	/** The composite cf. */
	COMPOSITE_CF,

	/** The match rule. */
	MATCH_RULE,

	/** The merge rule. */
	MERGE_RULE,

	/** The source system. */
	SOURCE_SYSTEM,
	/** The zip. */
	ZIP,

	/** The relation. */
	RELATION,
	
	/** The nested entity. */
	NESTED_ENTITY,
	
	/** The groups. */
	GROUPS;
	/**
	 * From value.
	 *
	 * @param v
	 *            the v
	 * @return the meta type RO
	 */
	@JsonCreator
	public static MetaTypeRO fromValue(String v) {

		return MetaTypeRO.valueOf(v);
	}
}
