package org.unidata.mdm.rest.v1.meta.ro.entities.register;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Upsert register result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class UpsertRegisterResultRO extends DetailedOutputRO {

    private Long draftId;
    // TODO: coming soon, wait response from upsert
//    private RegisterEntityRO registerEntity;

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

}
