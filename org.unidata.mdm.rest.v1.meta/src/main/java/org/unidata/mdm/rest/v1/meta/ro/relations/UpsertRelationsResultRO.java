package org.unidata.mdm.rest.v1.meta.ro.relations;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Upsert Relations Result
 *
 * @author Alexandr Serov
 * @since 25.11.2020
 **/
public class UpsertRelationsResultRO extends DetailedOutputRO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
