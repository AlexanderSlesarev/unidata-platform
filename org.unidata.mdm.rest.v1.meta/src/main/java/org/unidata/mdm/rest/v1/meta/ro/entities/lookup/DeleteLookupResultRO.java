package org.unidata.mdm.rest.v1.meta.ro.entities.lookup;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Delete lookup entity reqsult
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class DeleteLookupResultRO extends DetailedOutputRO {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
