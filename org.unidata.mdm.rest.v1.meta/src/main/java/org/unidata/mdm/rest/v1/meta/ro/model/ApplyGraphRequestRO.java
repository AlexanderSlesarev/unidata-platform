package org.unidata.mdm.rest.v1.meta.ro.model;

import org.unidata.mdm.rest.v1.meta.ro.graph.MetaGraphRO;

/**
 * Apply Graph Request
 *
 * @author Alexandr Serov
 * @since 28.11.2020
 **/
public class ApplyGraphRequestRO {

    private MetaGraphRO metaGraph;

    public MetaGraphRO getMetaGraph() {
        return metaGraph;
    }

    public void setMetaGraph(MetaGraphRO metaGraph) {
        this.metaGraph = metaGraph;
    }
}
