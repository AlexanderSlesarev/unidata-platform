package org.unidata.mdm.rest.v1.meta.ro.model;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Export model result
 *
 * @author Alexandr Serov
 * @since 28.11.2020
 **/
public class ExportModelResultRO extends DetailedOutputRO {

    private String storageId;

    public String getStorageId() {
        return storageId;
    }

    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }
}
