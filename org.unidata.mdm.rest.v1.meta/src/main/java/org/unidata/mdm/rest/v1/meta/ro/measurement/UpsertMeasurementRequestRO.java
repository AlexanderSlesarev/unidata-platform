package org.unidata.mdm.rest.v1.meta.ro.measurement;

/**
 * Upsert Measurement Request
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class UpsertMeasurementRequestRO {

    private MeasurementCategoryRO measurement;

    public MeasurementCategoryRO getMeasurement() {
        return measurement;
    }

    public void setMeasurement(MeasurementCategoryRO measurement) {
        this.measurement = measurement;
    }
}
