package org.unidata.mdm.rest.v1.meta.service.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.unidata.mdm.meta.type.event.MetaModelImportEvent;
import org.unidata.mdm.system.type.event.Event;
import org.unidata.mdm.system.type.event.EventReceiver;

@Service
public class ImportMetaModelWebSocketService implements EventReceiver {

    private static final Logger LOG = LoggerFactory.getLogger(ImportMetaModelWebSocketService.class);

    @Override
    public void receive(Event event) {
        if (event.isLocal() && event instanceof MetaModelImportEvent) {
            MetaModelImportEvent modelImportEvent = (MetaModelImportEvent) event;
            ImportMetaModelWebSocketEndpoint.MetaModelMessage msg = new ImportMetaModelWebSocketEndpoint.MetaModelMessage();
            msg.setType(modelImportEvent.getMetaModelImportStatus());
            try {
                ImportMetaModelWebSocketEndpoint.broadcastMetaModelMessage(msg);
            } catch (Exception ex) {
                LOG.error("Broadcast event error", ex);
            }
        }
    }

}
