package org.unidata.mdm.rest.v1.meta.ro.entities.nested;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.meta.ro.entities.NestedEntityRO;

/**
 * Get result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class GetNestedEntityResultRO extends DetailedOutputRO {

    private NestedEntityRO nestedEntity;

    public NestedEntityRO getNestedEntity() {
        return nestedEntity;
    }

    public void setNestedEntity(NestedEntityRO nestedEntity) {
        this.nestedEntity = nestedEntity;
    }

}
