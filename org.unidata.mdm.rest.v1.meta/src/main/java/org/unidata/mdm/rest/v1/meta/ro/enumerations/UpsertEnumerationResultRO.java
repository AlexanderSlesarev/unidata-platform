package org.unidata.mdm.rest.v1.meta.ro.enumerations;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Upsert enumeration result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class UpsertEnumerationResultRO extends DetailedOutputRO {

    private String name;
    // TODO: coming soon, wait response from upsert
//    private EnumerationDefinitionRO enumeration;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
