package org.unidata.mdm.rest.v1.meta.ro.relations;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.meta.ro.entities.RegisterEntityRO;

/**
 * Get Related Entities Result
 *
 * @author Alexandr Serov
 * @since 25.11.2020
 **/
public class GetRelatedEntitiesResultRO extends DetailedOutputRO {

    private List<RegisterEntityRO> entities;

    public List<RegisterEntityRO> getEntities() {
        return entities;
    }

    public void setEntities(List<RegisterEntityRO> entities) {
        this.entities = entities;
    }
}
