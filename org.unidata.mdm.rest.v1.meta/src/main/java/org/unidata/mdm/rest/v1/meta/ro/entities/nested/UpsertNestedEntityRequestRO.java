package org.unidata.mdm.rest.v1.meta.ro.entities.nested;

import org.unidata.mdm.rest.v1.meta.ro.entities.NestedEntityRO;

/**
 * Upsert nested result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class UpsertNestedEntityRequestRO {

    private NestedEntityRO nestedEntity;
    private Long draftId;

    public NestedEntityRO getNestedEntity() {
        return nestedEntity;
    }

    public void setNestedEntity(NestedEntityRO nestedEntity) {
        this.nestedEntity = nestedEntity;
    }

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}
