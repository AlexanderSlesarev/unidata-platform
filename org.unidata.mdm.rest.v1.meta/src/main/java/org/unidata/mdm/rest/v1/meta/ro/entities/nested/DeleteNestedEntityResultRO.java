package org.unidata.mdm.rest.v1.meta.ro.entities.nested;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Delete nested entity result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class DeleteNestedEntityResultRO extends DetailedOutputRO {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
