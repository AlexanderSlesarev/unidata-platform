package org.unidata.mdm.rest.v1.meta.ro.enumerations;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get enumeration result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class GetEnumerationResultRO extends DetailedOutputRO {

    private EnumerationDefinitionRO enumeration;

    public EnumerationDefinitionRO getEnumeration() {
        return enumeration;
    }

    public void setEnumeration(EnumerationDefinitionRO enumeration) {
        this.enumeration = enumeration;
    }

}
