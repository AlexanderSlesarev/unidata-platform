package org.unidata.mdm.rest.v1.meta.ro.measurement;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Upsert Measurement Result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class UpsertMeasurementResultRO extends DetailedOutputRO {

    private String name;
    // TODO: coming soon, wait response from upsert
//    private MeasurementCategoryRO measurement;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
