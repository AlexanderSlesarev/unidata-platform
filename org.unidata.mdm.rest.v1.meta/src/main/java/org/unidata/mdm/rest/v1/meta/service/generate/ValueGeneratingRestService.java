/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.rest.v1.meta.service.generate;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.meta.service.impl.AttributeValueGeneratorCacheComponent;
import org.unidata.mdm.meta.service.impl.EntityValueGeneratorCacheComponent;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.v1.meta.ro.generate.GeneratorValueTypeRO;
import org.unidata.mdm.rest.v1.meta.ro.generate.SuggestCustomGenerationStrategyRO;

/**
 * @author Dmitriy Bobrov on Oct 26, 2021
 */
@Path(ValueGeneratingRestService.SERVICE_PATH)
@Consumes({"application/json"})
@Produces({"application/json"})
public class ValueGeneratingRestService extends AbstractRestService {
    /**
     * This service path.
     */
    public static final String SERVICE_PATH = "value-generating";
    public static final String SERVICE_TAG = "value-generating";

    @Autowired
    private AttributeValueGeneratorCacheComponent attributeValueGeneratorCacheComponent;
    @Autowired
    private EntityValueGeneratorCacheComponent entityValueGeneratorCacheComponent;

    @GET
    @Path("/suggest")
    @Operation(
        description = "Suggests value generator implementation from a given library.",
        method = HttpMethod.GET,
        tags = SERVICE_TAG,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = SuggestCustomGenerationStrategyRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response suggest(
        @Parameter(required = true, description = "Library name.") @QueryParam("library") String library,
        @Parameter(required = true, description = "Library version.") @QueryParam("version") String version,
        @Parameter(required = true, description = "Interface type") @QueryParam("type") GeneratorValueTypeRO type) {

        Collection<String> suggest;

        switch (type) {
            case ATTRIBUTE:
                suggest = attributeValueGeneratorCacheComponent.find(SecurityUtils.getCurrentUserStorageId(),
                    library, version).suggest();
                break;
            case EXTERNAL_ID:
                suggest = entityValueGeneratorCacheComponent.find(SecurityUtils.getCurrentUserStorageId(),
                    library, version).suggest();
                break;
            default:
                suggest = Collections.emptyList();
                break;
        }

        return ok(new SuggestCustomGenerationStrategyRO(new ArrayList<>(suggest)));
    }
}
