package org.unidata.mdm.rest.v1.meta.ro.entities.nested;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.meta.ro.entities.NestedEntityRO;

/**
 * Get entities result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class GetNestedEntitiesResultRO extends DetailedOutputRO {

    private List<NestedEntityRO> nestedEntities;

    public List<NestedEntityRO> getNestedEntities() {
        return nestedEntities;
    }

    public void setNestedEntities(List<NestedEntityRO> nestedEntities) {
        this.nestedEntities = nestedEntities;
    }
}
