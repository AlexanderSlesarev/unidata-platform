package org.unidata.mdm.rest.v1.meta.ro.model;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Storages
 *
 * @author Alexandr Serov
 * @since 28.11.2020
 **/
public class GetStorageIdsResultRO extends DetailedOutputRO {

    private List<String> storageIds;

    public List<String> getStorageIds() {
        return storageIds;
    }

    public void setStorageIds(List<String> storageIds) {
        this.storageIds = storageIds;
    }
}
