package org.unidata.mdm.rest.v1.meta.ro.model;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.meta.ro.graph.MetaGraphRO;

/**
 * Upload Model Result
 *
 * @author Alexandr Serov
 * @since 28.11.2020
 **/
public class UploadModelResultRO extends DetailedOutputRO {

    private MetaGraphRO modelGraph;

    public void setModelGraph(MetaGraphRO modelGraph) {
        this.modelGraph = modelGraph;
    }

    public MetaGraphRO getModelGraph() {
        return modelGraph;
    }
}
