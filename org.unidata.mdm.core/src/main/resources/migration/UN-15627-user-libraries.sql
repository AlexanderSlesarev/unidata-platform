-- source systems
create table libraries (
    storage_id varchar(128) not null,
    name text,
    version varchar(128) not null,
    mime_type text,
    description text,
    content bytea not null,
    content_size bigint, 
    created_by varchar(256) not null,
    create_date timestamptz not null default current_timestamp,
    constraint pk_libraries_storage_id_revision primary key(storage_id, name, version)
);

create index if not exists ix_libraries_create_date on libraries (create_date);
create index if not exists ix_libraries_mime_type on libraries (mime_type);

