create table storage (
    id varchar(128) not null primary key,
    description text not null,
    create_date timestamp not null default current_timestamp,
    update_date timestamp,
    created_by text not null,
    updated_by text
);

insert into storage (id, description, created_by) 
values ('default', 'Default Unidata storage ID.', 'migrate');
