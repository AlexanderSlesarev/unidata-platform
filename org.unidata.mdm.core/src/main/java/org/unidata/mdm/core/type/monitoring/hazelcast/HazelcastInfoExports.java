/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.core.type.monitoring.hazelcast;

import com.hazelcast.core.HazelcastInstance;
import io.prometheus.client.Collector;
import io.prometheus.client.GaugeMetricFamily;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HazelcastInfoExports extends Collector {

    private final HazelcastInstance hazelcastInstance;

    public HazelcastInfoExports(final HazelcastInstance hazelcastInstance) {
        this.hazelcastInstance = hazelcastInstance;
    }

    @Override
    public List<MetricFamilySamples> collect() {
        final List<MetricFamilySamples> metricFamilySamples = new ArrayList<>();

        final GaugeMetricFamily hazelcastInfo = new GaugeMetricFamily(
                "hazelcast_info",
                "Labeled Hazelcast info",
                Collections.singletonList("name")
        );

        hazelcastInfo.addMetric(
                Collections.singletonList(
                        hazelcastInstance.getName()
                ),
                1L
        );

        metricFamilySamples.add(hazelcastInfo);

        return metricFamilySamples;
    }
}
