/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.core.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.unidata.mdm.core.exception.CoreExceptionIds;
import org.unidata.mdm.core.service.UPathService;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.Attribute.AttributeType;
import org.unidata.mdm.core.type.data.ComplexAttribute;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.impl.ComplexAttributeImpl;
import org.unidata.mdm.core.type.data.impl.SerializableDataRecord;
import org.unidata.mdm.core.type.upath.UPath;
import org.unidata.mdm.core.type.upath.UPathApplicationMode;
import org.unidata.mdm.core.type.upath.UPathConstants;
import org.unidata.mdm.core.type.upath.UPathElement;
import org.unidata.mdm.core.type.upath.UPathElementType;
import org.unidata.mdm.core.type.upath.UPathExecutionContext;
import org.unidata.mdm.core.type.upath.UPathIncompletePath;
import org.unidata.mdm.core.type.upath.UPathResult;
import org.unidata.mdm.system.exception.PlatformFailureException;

/**
 * @author Mikhail Mikhailov
 * UPath bits implementation.
 * FIXME: Rewrite navigation! Move parse process to UPath class! Kill expressions cache (move compiled expression to UPath class).
 */
@Service
public class UPathServiceImpl implements UPathService {
    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UPathServiceImpl.class);
    /**
     * Constructor.
     */
    public UPathServiceImpl() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public UPath upathCreate(String path) {
        return UPath.of(path);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public UPathResult upathResult(String path, DataRecord data) {
        UPath upath = UPath.of(path);
        return upathGet(upath, data, UPathApplicationMode.MODE_ALL);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public UPathResult upathGet(UPath upath, DataRecord data, UPathExecutionContext context, UPathApplicationMode mode) {

        if (context == UPathExecutionContext.FULL_TREE) {
            return upathFullTreeGetImpl(upath, data, mode);
        } else if (context == UPathExecutionContext.SUB_TREE) {
            return upathSubTreeGetImpl(upath, data, mode);
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public UPathResult upathGet(UPath upath, DataRecord data, UPathApplicationMode mode) {
        return upathGet(upath, data, UPathExecutionContext.FULL_TREE, mode);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public UPathResult upathGet(UPath upath, DataRecord data) {
        return upathGet(upath, data, UPathApplicationMode.MODE_ALL);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean upathSet(UPath upath, DataRecord data, Attribute target) {
        return upathSet(upath, data, target, UPathApplicationMode.MODE_ALL);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean upathSet(UPath upath, DataRecord data, Attribute target, UPathApplicationMode mode) {
        return upathSet(upath, data, target, UPathExecutionContext.FULL_TREE, mode);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean upathSet(UPath upath, DataRecord data, Attribute target, UPathExecutionContext context,
            UPathApplicationMode mode) {

        if (context == UPathExecutionContext.FULL_TREE) {
            return upathFullTreeSetImpl(upath, data, target, mode);
        } else if (context == UPathExecutionContext.SUB_TREE) {
            return upathSubTreeSetImpl(upath, data, target, mode);
        }

        return false;
    }

    private UPathResult upathSubTreeGetImpl(UPath upath, DataRecord data, UPathApplicationMode mode) {

        UPathResult result = new UPathResult(mode);

        // 1. Return immediately if this upath is empty or no input exist
        if (Objects.isNull(data) || upath.getElements().isEmpty()) {
            return result;
        }

        List<Pair<String, DataRecord>> segmentsChain = toSegmentsChain(data);

        // 2. Check root path and return immediately
        if (upath.isRoot()) {
            result.getAttributes().add(ComplexAttributeImpl.ofUnattended(UPathConstants.UPATH_ROOT_NAME, segmentsChain.get(0).getRight()));
            return result;
        }

        // 3. Check number of segments and proceed with diff
        int segmentsDiff = upath.getNumberOfSegments() - (segmentsChain.size() - 1);
        if (segmentsDiff > 0) {

            // 3.1. Childern, possibly filtered
            UPath subPath = upath.getSubSegmentsUPath(segmentsChain.size() - 1);
            return upathFullTreeGetImpl(subPath, data, mode);
        } else {

            // 3.2. Ancestors path processing
            List<UPathElement> upathSegments = upath.getSegments();
            for (int i = 0; i < upathSegments.size(); i++) {

                UPathElement element = upathSegments.get(i);
                Pair<String, DataRecord> source = segmentsChain.get(i);

                // 3.2.1 Check attribute exists to ensure path validity
                if (i < (upathSegments.size() - 1)) {

                    Attribute selection = source.getRight().getAttribute(element.getElement());
                    if (Objects.isNull(selection) || selection.getAttributeType() != AttributeType.COMPLEX) {

                        // Path lost. Return.
                        return result;
                    }

                    continue;
                }

                // 3.2.2 Last segment
                Attribute selection = source.getRight().getAttribute(element.getElement());
                if (Objects.nonNull(selection)) {

                    // 3.2.1.1 Complex attribute
                    if (selection.getAttributeType() == AttributeType.COMPLEX && segmentsChain.size() > (i + 1)) {
                        source = segmentsChain.get(i + 1);
                        result.getAttributes().add(ComplexAttributeImpl.ofUnattended(source.getLeft(), source.getRight()));
                    // 3.2.1.2 Simple, code, array
                    } else {
                        result.getAttributes().add(selection);
                    }
                }
            }
        }

        return result;
    }

    private UPathResult upathFullTreeGetImpl(UPath upath, DataRecord data, UPathApplicationMode mode) {

        UPathResult result = new UPathResult(mode);

        // Return immediately if this upath is empty or no input exist
        if (Objects.isNull(data) || upath.getElements().isEmpty()) {
            return result;
        }

        List<Attribute> collected = new ArrayList<>();
        List<Attribute> packaged = new ArrayList<>();

        // Add the first one for iteration.
        packaged.add(ComplexAttributeImpl.ofUnattended(UPathConstants.UPATH_ROOT_NAME, data));

        for (int i = 0; i < upath.getElements().size(); i++) {

            // Nothing filtered.
            if (packaged.isEmpty()) {
                break;
            }

            UPathElement element = upath.getElements().get(i);
            boolean isComplex = false;
            boolean isTerminating = i == (upath.getElements().size() - 1);

            // For each complex attribute
            for (ListIterator<Attribute> ci = packaged.listIterator(); ci.hasNext(); ) {

                ComplexAttribute holder = (ComplexAttribute) ci.next();
                for (Iterator<DataRecord> li = holder.iterator(); li.hasNext(); ) {

                    // Filtering
                    DataRecord dr = li.next();
                    if (element.isFiltering()) {

                        if (!element.getFilter().matches(dr)) {
                            li.remove();
                        }

                        continue;
                    }

                    // Collecting
                    Attribute attr = dr.getAttribute(element.getElement());
                    if (Objects.nonNull(attr)) {

                        isComplex = attr.getAttributeType() == AttributeType.COMPLEX;
                        if (!isTerminating && !isComplex) {
                            final String message = "Attribute selected for an intermediate path element [{}] is not a complex attribute.";
                            LOGGER.warn(message, element.getElement());
                            throw new PlatformFailureException(message,
                                    CoreExceptionIds.EX_UPATH_NOT_A_COMPLEX_ATTRIBUTE_FOR_INTERMEDIATE_PATH_ELEMENT,
                                    element.getElement());
                        }

                        // UN-9738 handle complex attributes with no records
                        if (isComplex && !isTerminating) {
                            ComplexAttribute ca = attr.narrow();
                            if (ca.isEmpty() && mode == UPathApplicationMode.MODE_ALL_WITH_INCOMPLETE) {
                                result.getIncomplete().add(new UPathIncompletePath(dr, element));
                                // Prevent collecting of empty attribute
                                continue;
                            }
                        }

                        collected.add(attr);
                    } else {
                        // Collect incomplete element, if requested
                        if (mode == UPathApplicationMode.MODE_ALL_WITH_INCOMPLETE) {
                            result.getIncomplete().add(new UPathIncompletePath(dr, element));
                        }
                    }
                }

                // Remove attribute, if empty
                if (holder.isEmpty()) {
                    ci.remove();
                }
            }

            if (element.isCollecting()) {

                packaged.clear();
                if (isComplex) {
                    for (Attribute attr : collected) {
                        packaged.add(ComplexAttributeImpl.ofUnattended(attr.getName(), ((ComplexAttribute) attr).toCollection()));
                    }

                    collected.clear();
                }
            }
        }

        // Return only one value.
        // Wait until this is reached, because of the case with filtered and of path, denoting complex attributes
        if (mode == UPathApplicationMode.MODE_ONCE) {

            if (CollectionUtils.isNotEmpty(collected)) {
                result.getAttributes().add(collected.get(0));
            }

            if (CollectionUtils.isNotEmpty(packaged)) {
                result.getAttributes().add(packaged.get(0));
            }
        } else {

            result.getAttributes().addAll(collected);
            result.getAttributes().addAll(packaged);
        }

        return result;
    }
    /**
     * Sets the attribute to target record.
     * @param upath UPath to process
     * @param data the record to manipulate
     * @param target target attribute to set
     * @param mode application mode
     * @return modified record
     */
    private boolean upathSubTreeSetImpl(UPath upath, DataRecord data, Attribute target, UPathApplicationMode mode) {

        // 1. Return immediately if this upath is empty, denotes root (what is not allowed) or record is null
        if (upath.getElements().isEmpty() || upath.isRoot() || Objects.isNull(data)) {
            return false;
        }

        // 2. Collect segments
        List<Pair<String, DataRecord>> segmentsChain = toSegmentsChain(data);

        // 3. Check number of segments and proceed with diff
        int segmentsDiff = upath.getNumberOfSegments() - (segmentsChain.size() - 1);
        if (segmentsDiff > 0) {

            // 3.1. Childern, possibly filtered
            UPath subPath = upath.getSubSegmentsUPath(segmentsChain.size() - 1);
            return upathFullTreeSetImpl(subPath, data, target, mode);
        } else {

            // 3.2. Check, we're generally able to set with params
            checkGeneralSetAbility(upath.getElements().get(upath.getElements().size() - 1), target);

            // 3.3. Ancestors path processing
            List<UPathElement> upathSegments = upath.getSegments();
            for (int i = 0; i < upathSegments.size(); i++) {

                UPathElement element = upathSegments.get(i);
                Pair<String, DataRecord> source = segmentsChain.get(i);

                // 3.2.1 Check attribute exists to ensure path validity
                if (i < (upathSegments.size() - 1)) {

                    Attribute selection = source.getRight().getAttribute(element.getElement());
                    if (Objects.isNull(selection) || selection.getAttributeType() != AttributeType.COMPLEX) {

                        // Path lost. Return.
                        return false;
                    }

                    continue;
                }

                // 3.2.2 Last segment. Put.
                source.getRight().addAttribute(target);
                return true;
            }
        }

        return false;
    }
    /**
     * Sets the attribute to target record.
     * @param upath UPath to process
     * @param source the record to manipulate
     * @param target target attribute to set
     * @param mode application mode
     * @return modified record
     */
    private boolean upathFullTreeSetImpl(UPath upath, DataRecord source, Attribute target, UPathApplicationMode mode) {

        // Return immediately if this upath is empty
        if (upath.getElements().isEmpty() || Objects.isNull(source)) {
            return false;
        }

        // Check, we're generally able to set with params
        checkGeneralSetAbility(upath.getElements().get(upath.getElements().size() - 1), target);

        // Think about whether such behaviour is really desired.
        DataRecord data = Objects.isNull(source) ? SerializableDataRecord.of() : source;

        List<Attribute> collected = new ArrayList<>(16);
        List<Attribute> packaged = new ArrayList<>(8);

        // Add the first one for iteration.
        packaged.add(ComplexAttributeImpl.ofUnattended("ROOT", data));

        boolean hadApplications = false;
        for (int i = 0; i < upath.getElements().size(); i++) {

            // Nothing filtered.
            if (packaged.isEmpty()) {
                break;
            }

            UPathElement element = upath.getElements().get(i);
            boolean isComplex = false;
            boolean isTerminating = i == (upath.getElements().size() - 1);

            // For each complex attribute
            for (ListIterator<Attribute> ci = packaged.listIterator(); ci.hasNext(); ) {

                ComplexAttribute holder = (ComplexAttribute) ci.next();
                for (Iterator<DataRecord> li = holder.iterator(); li.hasNext(); ) {

                    // Filtering
                    DataRecord dr = li.next();
                    if (element.isFiltering()) {

                        if (!element.getFilter().matches(dr)) {
                            li.remove();
                        }

                        continue;
                    }

                    // Set
                    if (isTerminating) {

                        dr.addAttribute(target);

                        // And finish
                        if (mode == UPathApplicationMode.MODE_ALL || mode == UPathApplicationMode.MODE_ALL_WITH_INCOMPLETE) {
                            hadApplications = true;
                            continue;
                        } else if (mode == UPathApplicationMode.MODE_ONCE) {
                            return true;
                        }
                    }

                    // Collecting
                    Attribute attr = dr.getAttribute(element.getElement());
                    if (Objects.nonNull(attr)) {

                        isComplex = attr.getAttributeType() == AttributeType.COMPLEX;
                        if (!isComplex) {
                            final String message = "Attribute selected for an intermediate path element [{}] is not a complex attribute.";
                            LOGGER.warn(message, element.getElement());
                            throw new PlatformFailureException(message, CoreExceptionIds.EX_UPATH_INVALID_SET_NOT_A_COMPLEX_FOR_INTERMEDIATE, element.getElement());
                        }

                        collected.add(attr);
                    }
                }

                // Remove attribute, if empty
                if (holder.isEmpty()) {
                    ci.remove();
                }
            }

            if (element.isCollecting()) {

                packaged.clear();
                if (isComplex) {
                    for (Attribute attr : collected) {
                        packaged.add(ComplexAttributeImpl.ofUnattended(attr.getName(), ((ComplexAttribute) attr).toCollection()));
                    }

                    collected.clear();
                }
            }
        }

        return hadApplications;
    }
    /**
     * Builds segments chain from bottom to top.
     * @param last the lowest end of the hierarсhie
     * @return chain
     */
    private List<Pair<String, DataRecord>> toSegmentsChain(DataRecord last) {

        if (Objects.isNull(last)) {
            return Collections.emptyList();
        }

        List<Pair<String, DataRecord>> chain = new ArrayList<>();
        DataRecord backpointer = last;
        while (!backpointer.isTopLevel()) {
            chain.add(0, Pair.of(backpointer.getHolderAttribute().getName(), backpointer));
            backpointer = backpointer.getParentRecord();
        }

        chain.add(0, Pair.of(UPathConstants.UPATH_ROOT_NAME, backpointer));
        return chain;
    }
    /**
     * Checks the general ability to set the target attribute to this UPath.
     * @param last last UPath element
     * @param target the target attribute
     */
    private void checkGeneralSetAbility(UPathElement last, Attribute target) {

        if (last.getType() != UPathElementType.COLLECTOR) {
            final String message = "Invalid input. UPath for set operations must end with collecting element. Element '{}' is not a collecting one.";
            LOGGER.warn(message, last.getElement());
            throw new PlatformFailureException(message, CoreExceptionIds.EX_UPATH_INVALID_SET_WRONG_END_ELEMENT, last.getElement());
        }

        if (!StringUtils.equals(last.getElement(), target.getName())) {
            final String message = "Invalid input. Attribute '{}' and last UPath element '{}' have different names.";
            LOGGER.warn(message, target.getName(), last.getElement());
            throw new PlatformFailureException(message, CoreExceptionIds.EX_UPATH_INVALID_SET_WRONG_ATTRIBUTE_NAME, target.getName(), last.getElement());
        }
    }
}
