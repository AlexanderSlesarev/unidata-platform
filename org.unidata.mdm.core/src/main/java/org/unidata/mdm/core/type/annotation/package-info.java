/**
 * UD 'core' annotations.
 * @author Mikhail Mikhailov on Jan 17, 2020
 */
package org.unidata.mdm.core.type.annotation;