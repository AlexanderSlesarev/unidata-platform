/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.core.dto;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.core.context.DisplayNameResolutionContext;
import org.unidata.mdm.core.type.display.DisplayableField;
import org.unidata.mdm.system.type.namespace.NameSpace;

/**
 * @author Dmitriy Bobrov on Oct 29, 2021
 */
public class DisplayNameResolutionResult {
    /**
     * Namespace
     */
    private final NameSpace nameSpace;
    /**
     * The type name.
     */
    private final String typeName;
    /**
     * subject
     */
    private final Object subject;
    /**
     * qualification
     */
    private final String qualifier;
    /**
     * Object is in deleted state mark.
     */
    private final boolean deleted;
    /**
     * Object is in inactive state mark.
     */
    private final boolean inactive;
    /**
     * validFrom
     */
    private final Date validFrom;
    /**
     * validTo
     */
    private final Date validTo;
    /**
     * Display names as fields.
     */
    private Map<String, DisplayableField> fields;
    /**
     * The final joined value.
     */
    private String displayValue;
    /**
     * The target object ID.
     */
    private String id;
    /**
     * Constructor.
     * @param upath the original upath element (used in request)
     * @param names display names
     */
    public DisplayNameResolutionResult(DisplayNameResolutionContext input) {
        this.nameSpace = input.getNameSpace();
        this.typeName = input.getTypeName();
        this.subject = input.getSubject();
        this.qualifier = input.getQualifier();
        this.deleted = input.isDeleted();
        this.inactive = input.isInactive();
        this.validFrom = input.getValidFrom();
        this.validTo = input.getValidTo();

        if (CollectionUtils.isNotEmpty(input.getFields())) {

            fields = new HashMap<>();
            for (String f : input.getFields()) {
                fields.put(f, null);
            }
        }
    }

    public boolean hasFields() {
        return MapUtils.isNotEmpty(fields);
    }
    /**
     * Gets field names.
     * @return names
     */
    public Set<String> getFieldNames() {

        if (Objects.isNull(fields)) {
            return Collections.emptySet();
        }

        return fields.keySet();
    }
    /**
     * @return names
     */
    public Collection<DisplayableField> getFields() {

        if (Objects.isNull(fields)) {
            return Collections.emptyList();
        }

        return fields.values();
    }

    public void addField(DisplayableField f) {

        if (Objects.nonNull(f)) {

            if (Objects.isNull(fields)) {
                fields = new HashMap<>();
            }

            fields.put(f.getName(), f);
        }
    }
    /**
     * @param names the names to set
     */
    public void addFields(Collection<DisplayableField> fields) {
        if (CollectionUtils.isNotEmpty(fields)) {
            for (DisplayableField df : fields) {
                addField(df);
            }
        }
    }
    /**
     * @return the upath
     */
    public NameSpace getNameSpace() {
        return nameSpace;
    }
    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }
    /**
     * @return the subject
     */
    public Object getSubject() {
        return subject;
    }
    /**
     * @return the qualifier
     */
    public String getQualifier() {
        return qualifier;
    }
    /**
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }
    /**
     * @return the inactive
     */
    public boolean isInactive() {
        return inactive;
    }
    /**
     * @return the validFrom
     */
    public Date getValidFrom() {
        return validFrom;
    }
    /**
     * @return the validTo
     */
    public Date getValidTo() {
        return validTo;
    }
    /**
     * @return the displayValue
     */
    public String getDisplayValue() {
        return displayValue;
    }
    /**
     * @param displayValue the displayValue to set
     */
    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
}
