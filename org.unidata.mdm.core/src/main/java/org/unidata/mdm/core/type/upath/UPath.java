/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.core.type.upath;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;
import org.mvel2.ParserContext;
import org.mvel2.compiler.CompiledExpression;
import org.mvel2.compiler.ExpressionCompiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.core.exception.CoreExceptionIds;
import org.unidata.mdm.core.service.impl.upath.CollectorElementImpl;
import org.unidata.mdm.core.service.impl.upath.ExpressionElementImpl;
import org.unidata.mdm.core.service.impl.upath.PredicateElementImpl;
import org.unidata.mdm.core.service.impl.upath.SubscriptElementImpl;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.ArrayAttribute.ArrayDataType;
import org.unidata.mdm.core.type.data.Attribute.AttributeType;
import org.unidata.mdm.core.type.data.CodeAttribute.CodeDataType;
import org.unidata.mdm.core.type.data.SimpleAttribute.SimpleDataType;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.type.namespace.NameSpace;
import org.unidata.mdm.system.util.NameSpaceUtils;

/**
 * @author Mikhail Mikhailov
 * UPath object.
 */
public class UPath {
    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UPath.class);
    /**
     * MVEL evaluation context.
     */
    private static final ParserContext EVALUATION_CONTEXT = ParserContext.create()
            .withInput(UPathConstants.UPATH_RECORD_NAME, DataRecord.class)
            .withImport(AttributeType.class)
            .withImport(SimpleDataType.class)
            .withImport(CodeDataType.class)
            .withImport(ArrayDataType.class)
            .withImport(LocalDate.class)
            .withImport(LocalDateTime.class)
            .withImport(LocalTime.class);
    /**
     * Optional namespace.
     */
    private final String nameSpace;
    /**
     * Optional type name.
     */
    private final String typeName;
    /**
     * Optional subject (typically data ID).
     */
    private final String subject;
    /**
     * Joined prefix key (namespace:typename).
     */
    private final String qualifierKey;
    /**
     * Elements.
     */
    private final List<UPathElement> elements = new ArrayList<>();
    /**
     * Constructor.
     */
    public UPath() {
        this(null, null, null);
    }
    /**
     * Constructor.
     * @param nameSpace optional namespace
     * @param typeName optional type name
     * @param subject optional subject (typically some data ID)
     */
    public UPath(String nameSpace, String typeName, String subject) {
        super();
        this.nameSpace = nameSpace;
        this.typeName = typeName;
        this.subject = subject;
        this.qualifierKey = NameSpaceUtils.join(nameSpace, typeName);
    }
    /**
     * @return the nameSpace
     */
    public String getNameSpace() {
        return nameSpace;
    }
    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }
    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }
    /**
     * @return the qualifierKey
     */
    public String getQualifierKey() {
        return qualifierKey;
    }
    /**
     * @return the elements
     */
    public List<UPathElement> getElements() {
        return elements;
    }
    /**
     * @return the segments
     */
    public List<UPathElement> getSegments() {
        List<UPathElement> view = new ArrayList<>(elements.size());
        for (int i = 0; i < elements.size(); i++) {

            UPathElement e = elements.get(i);
            if (e.getType() != UPathElementType.COLLECTOR) {
                continue;
            }

            view.add(e);
        }

        return view;
    }
    /**
     * Gets the size (i. e. number of path segments) of this UPath.
     * @return number of segments
     */
    public int getNumberOfSegments() {

        int result = 0;
        for (int i = 0; i < elements.size(); i++) {

            UPathElement e = elements.get(i);
            if (e.getType() != UPathElementType.COLLECTOR) {
                continue;
            }

            result++;
        }

        return result;
    }
    /**
     * Gets the last {@link UPathElement} or null, if empty.
     * @return last element
     */
    public UPathElement getTail() {
        return elements.isEmpty() ? null : elements.get(elements.size() - 1);
    }
    /**
     * Gets the sub segments UPath starting from segment index 'from'.
     * @param from the segment index to start
     * @return UPath
     */
    public UPath getSubSegmentsUPath(int from) {

        if (from < 0) {
            throw new ArrayIndexOutOfBoundsException("'From' segments index for UPath subtraction out of bounds.");
        }

        int done = 0;
        for (int i = 0; i < elements.size(); i++) {

            UPathElement e = elements.get(i);
            if (e.getType() != UPathElementType.COLLECTOR) {
                continue;
            }

            if (done++ == from) {
                UPath newPath = new UPath(this.nameSpace, this.typeName, null);
                newPath.elements.addAll(this.elements.subList(i, this.elements.size()));
                return newPath;
            }
        }

        throw new ArrayIndexOutOfBoundsException("'From' segments index for UPath subtraction out of bounds.");
    }
    /**
     * Tells whether this upath denotes the root element.
     * @return true, if so, false otherwise
     */
    public boolean isRoot() {
        return elements.size() == 1 && UPathConstants.UPATH_ROOT_NAME.equals(elements.get(0).getElement());
    }
    /**
     * Gets canonical meta model path.
     * @return meta model path
     */
    public String toPath() {

        if (elements.isEmpty()) {
            return StringUtils.EMPTY;
        }

        StringBuilder pb = new StringBuilder();
        for (int i = 0; i < elements.size(); i++) {

            UPathElement e = elements.get(i);
            if (e.getType() != UPathElementType.COLLECTOR) {
                continue;
            }

            pb.append(i > 0 ? '.' : StringUtils.EMPTY)
              .append(e.getElement());
        }

        return pb.toString();
    }
    /**
     * Gets UPath path.
     * @return UPath path
     */
    public String toUPath() {

        StringBuilder pb = new StringBuilder();
        if (StringUtils.isNotBlank(nameSpace)) {
            pb
                .append(nameSpace)
                .append(NameSpace.NAMESPACE_SEPARATOR);
        }

        if (StringUtils.isNoneBlank(typeName)) {
            pb
                .append(typeName)
                .append(NameSpace.NAMESPACE_SEPARATOR);
        }

        if (StringUtils.isNotBlank(subject)) {
            pb
                .append(subject)
                .append(NameSpace.NAMESPACE_SEPARATOR);
        }

        for (int i = 0; i < elements.size(); i++) {

            UPathElement e = elements.get(i);
            if (e.getType() == UPathElementType.COLLECTOR) {
                pb.append(i > 0 ? '.' : StringUtils.EMPTY);
            }

            pb.append(e.getElement());
        }

        return pb.toString();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return toUPath();
    }
    /**
     * Creates an empty UPath object, that can be populated with segments.
     * @return a new empty UPath object
     */
    public static UPath of() {
        return of(null, null, null);
    }
    /**
     * Creates a new UPath object completely from the given spec.
     * @param spec the spec to use
     * @return a new empty UPath object
     * @throws PlatformFailureException if spec is blank or has no elements
     */
    public static UPath of(String spec) {
        return spec(spec);
    }
    /**
     * Creates a new UPath object, initialized with optional namespace, optional type name and optional subject (all can be null).
     * @param ns the name space
     * @param tn the type name
     * @param subj the subject
     * @return a new UPath object
     */
    public static UPath of(String ns, String tn, String subj) {
        return new UPath(ns, tn, subj);
    }
    /**
     * Creates a new UPath object, initialized with optional namespace, optional type name and optional subject (all can be null) and supplied path.
     * @param ns the name space
     * @param tn the type name
     * @param subj the subject
     * @param spec the path spec
     * @return a new UPath object
     * @throws PlatformFailureException if spec is blank or has no elements
     */
    public static UPath of(String ns, String tn, String subj, String spec) {

        final StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(ns)) {
            sb
                .append(ns)
                .append(NameSpace.NAMESPACE_SEPARATOR);
        }

        if (StringUtils.isNoneBlank(tn)) {
            sb
                .append(tn)
                .append(NameSpace.NAMESPACE_SEPARATOR);
        }

        if (StringUtils.isNotBlank(subj)) {
            sb
                .append(subj)
                .append(NameSpace.NAMESPACE_SEPARATOR);
        }

        if (Objects.nonNull(spec)) {
            sb.append(spec);
        }

        return of(sb.toString());
    }
    /**
     * Checks and sets namespace and type name if possible.
     * @param element the element being processed
     */
    private static UPath init(String element) {

        int count = 0;
        int prev = 0;
        int pos;

        String nameSpace = null;
        String typeName = null;
        String subject = null;
        while ((pos = element.indexOf(':', prev)) != -1) {

            if (count > 3) {
                final String message = "Malformed UPath expression [{}]. Malformed intro (namespace:typename:subject:).";
                LOGGER.warn(message, element);
                throw new PlatformFailureException(message, CoreExceptionIds.EX_UPATH_INVALID_INPUT_MORE_THEN_THREE_COLONS, element);
            }

            if ((pos - prev) > 0) {

                String part = element.substring(prev, pos);
                switch (count) {
                case 0:
                    nameSpace = part;
                    break;
                case 1:
                    typeName = part;
                    break;
                case 2:
                    subject = part;
                    break;
                default:
                    break;
                }
            }

            prev = (pos + 1);
            count++;
        }

        return new UPath(nameSpace, typeName, subject);
    }
    /**
     * MMI: Taken from apache-commons StringUtils and modified for our needs.
     *
     * Performs the logic for the {@code split} and
     * {@code splitPreserveAllTokens} methods that do not return a
     * maximum array length.
     *
     * @param str  the String to parse, may not be {@code null}
     * @param separatorChar the separate character
     * @param preserveAllTokens if {@code true}, adjacent separators are
     * treated as empty token separators; if {@code false}, adjacent
     * separators are treated as one separator.
     * @return an array of parsed Strings, {@code null} if null String input
     */
    private static List<String> split(@Nonnull final String str) {

        final int len = str.length();
        if (len == 0) {
            return Collections.emptyList();
        }

        final List<String> list = new ArrayList<>();

        int i = 0;
        int start = 0;
        int expr = 0;
        char current;
        while (i < len) {

            current = str.charAt(i);
            if (expr == 0 && current == UPathConstants.UPATH_SEPARATOR_CHAR) {

                if (i > start) {

                    // UD, honor escape sym followed by path separator,
                    // which may be part of expression
                    if (str.charAt(i - 1) == UPathConstants.UPATH_ESCAPE_CHAR) {
                        i++;
                        continue;
                    }

                    list.add(str.substring(start, i));
                }

                // Move to next position,
                // but not increase counter (done below)
                start = i + 1;
            } else if (current == UPathConstants.UPATH_EXPRESSION_START) {
                expr++;
            } else if (current == UPathConstants.UPATH_EXPRESSION_END) {
                expr--;
            }

            i++;
        }

        if (i > start) {
            list.add(str.substring(start, i));
        }

        return list;
    }
    /**
     * Check root record special notation.
     * @param upath the {@link UPath} currently being built
     * @param element the element being processed
     * @return true, if element has root special notation, false otherwise
     */
    private static boolean root(UPath upath, String element) {

        int start = element.indexOf(UPathConstants.UPATH_EXPRESSION_START);
        if (start == -1) {
            return false;
        }

        int end = element.indexOf(UPathConstants.UPATH_EXPRESSION_END, start);
        if (end == -1) {
            final String message = "Invalid input. Root record expression incorrect [{}].";
            LOGGER.warn(message, element);
            throw new PlatformFailureException(message, CoreExceptionIds.EX_UPATH_INVALID_ROOT_EXPRESSION, element);
        }

        boolean isUnfilteredRoot = end - start == 1;
        if (isUnfilteredRoot) {
            upath.getElements().add(new PredicateElementImpl(UPathConstants.UPATH_ROOT_NAME, DataRecord::isTopLevel));
            return true;
        } else {
            return expression(upath, element);
        }
    }
    /**
     * Check subscript (record ordinal).
     * @param upath the {@link UPath} currently being built
     * @param element the element being processed
     * @return true, if element has subscript filtering, false otherwise
     */
    private static boolean subscript(UPath upath, String element) {

        int start = element.indexOf(UPathConstants.UPATH_SUBSCRIPT_START);
        if (start == -1) {
            return false;
        }

        int end = element.indexOf(UPathConstants.UPATH_SUBSCRIPT_END, start);
        if (end == -1) {
            final String message = "Invalid input. Subscript expression incorrect [{}].";
            LOGGER.warn(message, element);
            throw new PlatformFailureException(message, CoreExceptionIds.EX_UPATH_INVALID_SUBSCRIPT_EXPRESSION, element);
        }

        final String subscriptAsString = element.substring(start + 1, end);
        final int ordinal = Integer.parseUnsignedInt(subscriptAsString);
        final String subpath = element.substring(0, start);

        upath.getElements().add(new CollectorElementImpl(subpath));
        upath.getElements().add(new SubscriptElementImpl(element.substring(start, (end + 1)), ordinal));

        return true;
    }
    /**
     * Checks the supplied expression.
     * @param upath current upath
     * @param element the element name
     * @return true, if the element has been processed, false otherwise
     */
    private static boolean expression(UPath upath, String element) {

        int start = element.indexOf(UPathConstants.UPATH_EXPRESSION_START);
        if (start == -1) {
            return false;
        }

        String name = element.substring(0, start);

        int end = element.length() - 1;
        if (element.charAt(end) != UPathConstants.UPATH_EXPRESSION_END) {
            final String message = "Malformed UPath expression [{}].";
            LOGGER.warn(message, element);
            throw new PlatformFailureException(message, CoreExceptionIds.EX_UPATH_MALFORMED_EXPRESSION, element);
        }

        String value = StringUtils.trimToNull(element.substring(start + 1, end));

        // name == "", but the rest is ok - this is a root expression
        // omit complex attribute part
        boolean hasPath = StringUtils.isNotBlank(name);
        if (hasPath) {
            upath.getElements().add(new CollectorElementImpl(name));
        }

        boolean hasExpression = StringUtils.isNotBlank(value);
        if (hasExpression) {

            ExpressionCompiler ec = new ExpressionCompiler(value, EVALUATION_CONTEXT);
            CompiledExpression ce = ec.compile();

            upath.getElements().add(new ExpressionElementImpl(value, ce));
        }

        return hasPath || hasExpression;
    }
    /**
     * Builds UPath from spec.
     * @param spec the spec
     * @return UPath
     */
    private static UPath spec(String spec) {

        if (StringUtils.isBlank(spec)) {
            final String message = "Invalid input. Path is blank.";
            LOGGER.warn(message);
            throw new PlatformFailureException(message, CoreExceptionIds.EX_UPATH_INVALID_INPUT_PATH_IS_BLANK);
        }

        UPath upath = null;

        List<String> tokens = split(spec);
        if (tokens.isEmpty()) {
            final String message = "Invalid input. Path [{}] was split to zero elements.";
            LOGGER.warn(message, spec);
            throw new PlatformFailureException(message, CoreExceptionIds.EX_UPATH_INVALID_INPUT_SPLIT_TO_ZERO_ELEMENTS, spec);
        }

        for (int i = 0; i < tokens.size(); i++) {

            String token = tokens.get(i);

            if (i == 0) {

                upath = init(token);
                if (Objects.nonNull(upath.getNameSpace())
                 || Objects.nonNull(upath.getTypeName())
                 || Objects.nonNull(upath.getSubject())) {
                    token = token.substring(token.lastIndexOf(':') + 1);
                }
            }

            // 1. Subscript or expression.
            if ((i == 0 && root(upath, token))
              || subscript(upath, token)
              || expression(upath, token)) {
                continue;
            }

            // 2. Simple collecting
            upath.getElements().add(new CollectorElementImpl(token));
        }

        return upath;
    }
}