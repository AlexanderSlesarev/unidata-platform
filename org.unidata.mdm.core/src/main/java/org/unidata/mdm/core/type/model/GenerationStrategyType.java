package org.unidata.mdm.core.type.model;

public enum GenerationStrategyType {
    RANDOM,
    CONCAT,
    SEQUENCE,
    CUSTOM;
}