/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.core.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.unidata.mdm.core.context.DisplayNameResolutionContext;
import org.unidata.mdm.core.dto.DisplayNameResolutionResult;
import org.unidata.mdm.core.exception.CoreExceptionIds;
import org.unidata.mdm.core.service.DisplayNameService;
import org.unidata.mdm.core.type.display.DisplayNameResolver;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.type.namespace.NameSpace;

/**
 * @author Dmitriy Bobrov on Oct 29, 2021
 */
@Service
public class DisplayNameServiceImpl implements DisplayNameService {
    /**
     * Display name resolver implementors
     */
    private final Map<NameSpace, DisplayNameResolver> implementors = new HashMap<>();
    /**
     * {@inheritDoc}
     */
    @Override
    public DisplayNameResolutionResult resolve(DisplayNameResolutionContext ctx) {

        if (Objects.isNull(ctx)) {
            return null;
        }

        DisplayNameResolver resolver = implementors.get(ctx.getNameSpace());
        ensureResolver(resolver, ctx.getNameSpace());

        return resolver.resolve(ctx);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public List<DisplayNameResolutionResult> resolve(List<DisplayNameResolutionContext> input) {

        if (CollectionUtils.isEmpty(input)) {
            return Collections.emptyList();
        }

        List<DisplayNameResolutionResult> result = new ArrayList<>(input.size());
        input.stream()
            .collect(Collectors.groupingBy(DisplayNameResolutionContext::getNameSpace))
            .forEach((ns, values) -> {

                DisplayNameResolver resolver = implementors.get(ns);
                ensureResolver(resolver, ns);

                result.addAll(resolver.resolve(values));
            });

        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void register(DisplayNameResolver resolver) {
        Objects.requireNonNull(resolver, "Resolver implementor must not be null");
        Objects.requireNonNull(resolver.getNameSpace(), "Resolver implementor's nameSpace must not be null");
        implementors.put(resolver.getNameSpace(), resolver);
    }

    private void ensureResolver(DisplayNameResolver resolver, NameSpace ns) {
        if (Objects.isNull(resolver)) {
            throw new PlatformFailureException("Display name resolver for the name space [{}] not found.",
                    CoreExceptionIds.EX_CORE_DISPLAY_NAME_RESOLVER_NOT_FOUND, ns);
        }
    }
}
