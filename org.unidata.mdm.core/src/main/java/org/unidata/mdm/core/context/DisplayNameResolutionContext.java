/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.core.context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.system.type.namespace.NameSpace;

/**
 * @author Dmitriy Bobrov on Oct 29, 2021
 */
public class DisplayNameResolutionContext implements ValidityRangeContext{
    /**
     * Namespace
     */
    private final NameSpace nameSpace;
    /**
     * The type name.
     */
    private final String typeName;
    /**
     * subject
     */
    private final Serializable subject;
    /**
     * qualification
     */
    private final String qualifier;
    /**
     * Object is in deleted state mark.
     */
    private final boolean deleted;
    /**
     * Object is in inactive state mark.
     */
    private final boolean inactive;
    /**
     * validFrom
     */
    private final Date validFrom;
    /**
     * validTo
     */
    private final Date validTo;
    /**
     * Override fields
     */
    private final List<String> fields;
    /**
     * Constructor.
     */
    private DisplayNameResolutionContext(DisplayNameResolutionContextBuilder builder) {
        super();
        this.nameSpace = builder.nameSpace;
        this.typeName = builder.typeName;
        this.subject = builder.subject;
        this.qualifier = builder.qualifier;
        this.deleted = builder.deleted;
        this.inactive = builder.inactive;
        this.validFrom = builder.validFrom;
        this.validTo = builder.validTo;
        this.fields = builder.fields;
    }

    public NameSpace getNameSpace() {
        return nameSpace;
    }
    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }

    public Serializable getSubject() {
        return subject;
    }

    public String getQualifier() {
        return qualifier;
    }

    @Override
    public Date getValidFrom() {
        return validFrom;
    }

    @Override
    public Date getValidTo() {
        return validTo;
    }
    /**
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }
    /**
     * @return the inactive
     */
    public boolean isInactive() {
        return inactive;
    }

    /**
     * @return the fields
     */
    public List<String> getFields() {
        return fields;
    }

    public static DisplayNameResolutionContextBuilder builder() {
        return new DisplayNameResolutionContextBuilder();
    }

    public static class DisplayNameResolutionContextBuilder {
        /**
         * Namespace
         */
        private NameSpace nameSpace;
        /**
         * The type name.
         */
        private String typeName;
        /**
         * subject
         */
        private Serializable subject;
        /**
         * qualification
         */
        private String qualifier;
        /**
         * Object is in deleted state mark.
         */
        private boolean deleted;
        /**
         * Object is in inactive state mark.
         */
        private boolean inactive;
        /**
         * validFrom
         */
        private Date validFrom;
        /**
         * validTo
         */
        private Date validTo;
        /**
         * Override fields
         */
        private List<String> fields;
        /**
         * Constructor.
         */
        public DisplayNameResolutionContextBuilder() {
            super();
        }
        /**
         * Sets namespace.
         * @param nameSpace the nameSpace
         * @return self
         */
        public DisplayNameResolutionContextBuilder nameSpace(NameSpace nameSpace) {
            this.nameSpace = nameSpace;
            return this;
        }
        /**
         * Sets typeName.
         * @param typeName the typeName
         * @return self
         */
        public DisplayNameResolutionContextBuilder typeName(String typeName) {
            this.typeName = typeName;
            return this;
        }
        /**
         * Sets etalonId
         * @param subject the etalonId
         * @return self
         */
        public DisplayNameResolutionContextBuilder subject(Serializable subject) {
            this.subject = subject;
            return this;
        }
        /**
         * Sets qualification.
         * @param qualifier the qualifier
         * @return self
         */
        public DisplayNameResolutionContextBuilder qualifier(String qualifier) {
            this.qualifier = qualifier;
            return this;
        }
        /**
         * Sets deleted
         * @param deleted the deleted state
         * @return self
         */
        public DisplayNameResolutionContextBuilder deleted(boolean deleted) {
            this.deleted = deleted;
            return this;
        }
        /**
         * Sets inactive.
         * @param inactive the inactive
         * @return self
         */
        public DisplayNameResolutionContextBuilder inactive(boolean inactive) {
            this.inactive = inactive;
            return this;
        }
        /**
         * Sets validFrom
         * @param validFrom the validFrom
         * @return self
         */
        public DisplayNameResolutionContextBuilder validFrom(Date validFrom) {
            this.validFrom = validFrom;
            return this;
        }
        /**
         * Sets validTo
         * @param validTo the validTo
         * @return self
         */
        public DisplayNameResolutionContextBuilder validTo(Date validTo) {
            this.validTo = validTo;
            return this;
        }
        /**
         * Sets fields
         * @param fields the fields
         * @return self
         */
        public DisplayNameResolutionContextBuilder fields(String... fields) {
            if (ArrayUtils.isNotEmpty(fields)) {
                return fields(Arrays.asList(fields));
            }
            return this;
        }
        /**
         * Sets fields
         * @param fields the fields
         * @return self
         */
        public DisplayNameResolutionContextBuilder fields(Collection<String> fields) {
            if (CollectionUtils.isNotEmpty(fields)) {

                if (Objects.isNull(this.fields)) {
                    this.fields = new ArrayList<>();
                }

                this.fields.addAll(fields);
            }
            return this;
        }

        public DisplayNameResolutionContext build() {

            Objects.requireNonNull(this.nameSpace, "Name space must not be null.");
            Objects.requireNonNull(this.typeName, "Type name must not be null.");
            Objects.requireNonNull(this.subject, "Subject must not be null.");

            return new DisplayNameResolutionContext(this);
        }
    }
}
