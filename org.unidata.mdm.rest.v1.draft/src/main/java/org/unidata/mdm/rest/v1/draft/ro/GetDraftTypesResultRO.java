package org.unidata.mdm.rest.v1.draft.ro;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get draft types response object
 *
 * @author Alexandr Serov
 * @since 01.12.2020
 **/
public class GetDraftTypesResultRO extends DetailedOutputRO {

    private List<DraftProviderInfoRO> types;

    public GetDraftTypesResultRO(List<DraftProviderInfoRO> types) {
        super();
        setTypes(types);
    }

    public List<DraftProviderInfoRO> getTypes() {
        return types;
    }

    public void setTypes(List<DraftProviderInfoRO> types) {
        this.types = types;
    }
}
