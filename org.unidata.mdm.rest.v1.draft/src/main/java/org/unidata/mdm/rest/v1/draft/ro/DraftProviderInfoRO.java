/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.draft.ro;

import java.util.Collection;

import org.unidata.mdm.draft.dto.DraftProviderInfo;

/**
 * @author Mikhail Mikhailov on Apr 23, 2021
 * Simple draft provider info container.
 */
public class DraftProviderInfoRO {
    /**
     * Provider id.
     */
    private String id;
    /**
     * Description.
     */
    private String description;
    /**
     * Exported tags.
     */
    private Collection<String> tags;
    /**
     * Constructor.
     */
    public DraftProviderInfoRO(DraftProviderInfo dpi) {
        super();
        setId(dpi.getId());
        setDescription(dpi.getDescription());
        setTags(dpi.getTags());
    }
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the tags
     */
    public Collection<String> getTags() {
        return tags;
    }
    /**
     * @param tags the tags to set
     */
    public void setTags(Collection<String> tags) {
        this.tags = tags;
    }
}
