/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.draft.migration;

import org.unidata.mdm.system.migration.Migrations;
import org.unidata.mdm.system.util.ResourceUtils;

import nl.myndocs.database.migrator.MigrationScript;

/**
 * storage migrations to install security meta + admin login + resource
 *
 *
 * @author maria.chistyakova
 */
public final class DraftMigrations {

    private static final MigrationScript[] MIGRATIONS = {
        Migrations.of("UN-14594-initial-draft-schema", "mikhail.mikhailov",
                ResourceUtils.asString("classpath:/migration/UN-14594-initial-draft-schema.sql")),
        Migrations.of("UN-16483-publication-order", "mikhail.mikhailov",
                "create type publication_order as enum ('CHILDREN_LAST', 'CHILDREN_FIRST')",
                "alter table drafts add column if not exists publication_order publication_order default 'CHILDREN_LAST'::publication_order")
    };
    /**
     * Constructor.
     */
    private DraftMigrations() {
        super();
    }
    /**
     * Makes SONAR happy.
     *
     * @return migrations
     */
    public static MigrationScript[] migrations() {
        return MIGRATIONS;
    }
}
