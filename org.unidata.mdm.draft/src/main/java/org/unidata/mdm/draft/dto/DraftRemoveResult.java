package org.unidata.mdm.draft.dto;

/**
 * @author Mikhail Mikhailov on Sep 17, 2020
 */
public class DraftRemoveResult {
    /**
     * Count of removed objects.
     */
    private final int count;
    /**
     * Constructor.
     */
    public DraftRemoveResult(int count) {
        super();
        this.count = count;
    }
    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }
}
