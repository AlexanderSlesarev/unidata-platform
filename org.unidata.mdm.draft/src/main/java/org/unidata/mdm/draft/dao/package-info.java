/**
 * @author Mikhail Mikhailov on Sep 9, 2020
 * DAO support for draft functionality.
 */
package org.unidata.mdm.draft.dao;