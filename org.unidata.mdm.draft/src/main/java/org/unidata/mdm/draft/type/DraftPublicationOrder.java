/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.type;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Mikhail Mikhailov on Apr 30, 2021
 * Publication order, in what a draft wants to be published.
 */
public enum DraftPublicationOrder {
    /**
     * Children must be published after this draft is published.
     */
    CHILDREN_LAST,
    /**
     * Children must be published before this draft is published.
     */
    CHILDREN_FIRST;
    /**
     * From string value.
     * @param v the value
     * @return publication mode
     */
    public static DraftPublicationOrder fromString(String v) {

        if (StringUtils.isBlank(v)) {
            return CHILDREN_LAST;
        }

        for (DraftPublicationOrder m : values()) {
            if (StringUtils.equals(v, m.name())) {
                return m;
            }
        }

        throw new IllegalArgumentException("Unknown value passed to DraftPublicationMode : " + v);
    }
}
