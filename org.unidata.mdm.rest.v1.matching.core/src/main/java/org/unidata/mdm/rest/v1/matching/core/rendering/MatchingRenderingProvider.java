/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.rendering;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestInputRenderingAction;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestOutputRenderingAction;
import org.unidata.mdm.system.type.rendering.InputFragmentRenderer;
import org.unidata.mdm.system.type.rendering.InputRenderingAction;
import org.unidata.mdm.system.type.rendering.OutputFragmentRenderer;
import org.unidata.mdm.system.type.rendering.OutputRenderingAction;
import org.unidata.mdm.system.type.rendering.RenderingProvider;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Sergey Murskiy on 01.07.2021
 */
@Component
public class MatchingRenderingProvider implements RenderingProvider {
    /**
     * Metamodel service.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Display name resolver.
     */
    @Autowired
    private ClusterDisplayNameResolver displayNameResolver;

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<InputFragmentRenderer> get(@NonNull InputRenderingAction action) {
        if (SearchRestInputRenderingAction.SIMPLE_SEARCH_INPUT == action) {
            return Collections.singletonList(new SearchClusterInputRequestRenderer());
        }

        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<OutputFragmentRenderer> get(@NonNull OutputRenderingAction action) {
        if (SearchRestOutputRenderingAction.SIMPLE_SEARCH_OUTPUT == action) {
            return List.of(new SearchClusterResponseRenderer(displayNameResolver), new SearchClusterOutputFinalizer());
        }

        return Collections.emptyList();
    }
}
