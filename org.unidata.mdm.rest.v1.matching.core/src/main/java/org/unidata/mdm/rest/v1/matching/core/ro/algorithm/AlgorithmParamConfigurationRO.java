/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro.algorithm;

import org.unidata.mdm.rest.v1.matching.core.ro.AlgorithmParameterType;

import java.util.List;

/**
 * @author Sergey Murskiy on 10.06.2021
 */
public class AlgorithmParamConfigurationRO {
    /**
     * Parameter name.
     */
    private String name;
    /**
     * Display name.
     */
    private String displayName;
    /**
     * Description.
     */
    private String description;
    /**
     * Parameter type.
     */
    private AlgorithmParameterType type;
    /**
     * Default value.
     */
    private AlgorithmParameterRO defaultValue;
    /**
     * Enum values.
     */
    private List<EnumValueRO> enumValues;
    /**
     * Is parameter required;
     */
    private boolean isRequired;

    /**
     * @return parameter name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name parameter name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName display name to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return parameter type
     */
    public AlgorithmParameterType getType() {
        return type;
    }

    /**
     * @param type parameter type to set
     */
    public void setType(AlgorithmParameterType type) {
        this.type = type;
    }

    /**
     * @return default value
     */
    public AlgorithmParameterRO getDefaultValue() {
        return defaultValue;
    }

    /**
     * @param defaultValue default value to set
     */
    public void setDefaultValue(AlgorithmParameterRO defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * @return enum values
     */
    public List<EnumValueRO> getEnumValues() {
        return enumValues;
    }

    /**
     * @param enumValues enum values to set
     */
    public void setEnumValues(List<EnumValueRO> enumValues) {
        this.enumValues = enumValues;
    }

    /**
     * @return is required
     */
    public boolean isRequired() {
        return isRequired;
    }

    /**
     * @param required is required to set
     */
    public void setRequired(boolean required) {
        isRequired = required;
    }
}
