/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.matching.core.context.MatchingModelGetContext;
import org.unidata.mdm.matching.core.dto.AlgorithmsModelGetResult;
import org.unidata.mdm.matching.core.dto.MatchingModelGetResult;
import org.unidata.mdm.matching.core.dto.MatchingStorageInfoGetResult;
import org.unidata.mdm.matching.core.service.MatchingStorageService;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.v1.matching.core.converter.MatchingAlgorithmConverter;
import org.unidata.mdm.rest.v1.matching.core.converter.MatchingStorageConverter;
import org.unidata.mdm.rest.v1.matching.core.ro.GetMatchingAlgorithmResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.GetMatchingAlgorithmsResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.GetMatchingStoragesResultRO;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Path(MatchingAlgorithmRestService.SERVICE_PATH)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MatchingAlgorithmRestService extends AbstractRestService {
    /**
     * Service path.
     */
    public static final String SERVICE_PATH = "algorithms";
    /**
     * Tag.
     */
    public static final String SERVICE_TAG = "algorithms";

    /**
     * The MMS.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Matching storage service.
     */
    @Autowired
    private MatchingStorageService matchingStorageService;
    /**
     * Converter.
     */
    @Autowired
    private MatchingAlgorithmConverter algorithmConverter;
    /**
     * Matching storage converter.
     */
    @Autowired
    private MatchingStorageConverter matchingStorageConverter;

    @GET
    @Operation(
            description = "Get all defined matching algorithms.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingAlgorithmsResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingAlgorithmsResultRO getAll(@QueryParam("draftId") @DefaultValue("0") Long draftId) {
        MatchingModelGetResult modelGetResult = metaModelService.get(MatchingModelGetContext.builder()
                .allAlgorithms(true)
                .draftId(draftId)
                .build());

        return new GetMatchingAlgorithmsResultRO(Objects.isNull(modelGetResult.getAlgorithms())
                ? Collections.emptyList()
                : algorithmConverter.to(modelGetResult.getAlgorithms().getAlgorithms()));
    }

    @GET
    @Path("{id}")
    @Operation(
            description = "Get defined matching algorithm by id.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The matching algorithm id (name). Required.", in = ParameterIn.PATH, name = "id"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingAlgorithmResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingAlgorithmResultRO getById(@PathParam("id") String id,
                                                @QueryParam("draftId") @DefaultValue("0") Long draftId) {

        MatchingModelGetResult modelGetResult = metaModelService.get(MatchingModelGetContext.builder()
                .algorithmIds(Collections.singletonList(id))
                .draftId(draftId)
                .build());

        GetMatchingAlgorithmResultRO result = new GetMatchingAlgorithmResultRO();

        AlgorithmsModelGetResult amr = modelGetResult.getAlgorithms();
        if (CollectionUtils.isNotEmpty(amr.getAlgorithms()) && amr.getAlgorithms().size() == 1) {
            result.setAlgorithm(algorithmConverter.to(amr.singleValue()));
        }

        return result;
    }

    @GET
    @Path("storages")
    @Operation(
            description = "Get all defined matching storages.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingStoragesResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingStoragesResultRO getStorages() {
        MatchingStorageInfoGetResult storages =
                matchingStorageService.getMatchingStoragesInfo();

        return new GetMatchingStoragesResultRO(matchingStorageConverter.to(storages.getMatchingStorages()));
    }
}
