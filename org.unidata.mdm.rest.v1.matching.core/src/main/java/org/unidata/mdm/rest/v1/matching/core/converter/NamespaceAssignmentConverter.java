/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.converter;

import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.type.model.source.assignment.NamespaceAssignmentSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingColumnMappingSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableMappingSource;
import org.unidata.mdm.rest.v1.matching.core.ro.assignment.NamespaceAssignmentRO;
import org.unidata.mdm.rest.v1.matching.core.ro.table.MatchingColumnMappingRO;
import org.unidata.mdm.rest.v1.matching.core.ro.table.MatchingTableMappingRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Component
public class NamespaceAssignmentConverter extends Converter<NamespaceAssignmentSource, NamespaceAssignmentRO> {
    /**
     * Matching table mapping converter.
     */
    private static final MatchingTableMappingConverter MATCHING_TABLE_MAPPING_CONVERTER = new MatchingTableMappingConverter();

    /**
     * Constructor.
     */
    public NamespaceAssignmentConverter() {
        this.to = this::convert;
        this.from = this::convert;
    }

    private NamespaceAssignmentRO convert(NamespaceAssignmentSource source) {
        NamespaceAssignmentRO ro = new NamespaceAssignmentRO();

        ro.setNamespace(source.getNameSpace());
        ro.setTypeName(source.getTypeName());
        ro.setAssignmentKey(source.getAssignmentKey());
        ro.setMatchingTables(source.getMatchingTables());
        ro.setSets(source.getSets());
        ro.setMappings(MATCHING_TABLE_MAPPING_CONVERTER.to(source.getMappings()));

        return ro;
    }

    private NamespaceAssignmentSource convert(NamespaceAssignmentRO source) {
        return new NamespaceAssignmentSource()
                .withNameSpace(source.getNamespace())
                .withTypeName(source.getTypeName())
                .withMatchingTables(source.getMatchingTables())
                .withSets(source.getSets())
                .withMappings(MATCHING_TABLE_MAPPING_CONVERTER.from(source.getMappings()));
    }

    private static class MatchingTableMappingConverter extends Converter<MatchingTableMappingSource, MatchingTableMappingRO> {
        /**
         * Matching column mapping converter.
         */
        private static final MatchingColumnMappingConverter MATCHING_COLUMN_MAPPING_CONVERTER = new MatchingColumnMappingConverter();

        /**
         * Constructor.
         */
        public MatchingTableMappingConverter() {
            super(MatchingTableMappingConverter::convert, MatchingTableMappingConverter::convert);
        }

        private static MatchingTableMappingRO convert(MatchingTableMappingSource source) {
            MatchingTableMappingRO ro = new MatchingTableMappingRO();

            ro.setMatchingTableName(source.getMatchingTableName());
            ro.setMappings(MATCHING_COLUMN_MAPPING_CONVERTER.to(source.getMappings()));

            return ro;
        }

        private static MatchingTableMappingSource convert(MatchingTableMappingRO source) {
            return new MatchingTableMappingSource()
                    .withMatchingTableName(source.getMatchingTableName())
                    .withMappings(MATCHING_COLUMN_MAPPING_CONVERTER.from(source.getMappings()));
        }
    }

    private static class MatchingColumnMappingConverter extends Converter<MatchingColumnMappingSource, MatchingColumnMappingRO> {
        /**
         * Constructor.
         */
        public MatchingColumnMappingConverter() {
            super(MatchingColumnMappingConverter::convert, MatchingColumnMappingConverter::convert);
        }

        private static MatchingColumnMappingRO convert(MatchingColumnMappingSource source) {
            MatchingColumnMappingRO ro = new MatchingColumnMappingRO();

            ro.setPath(source.getPath());
            ro.setColumnName(source.getColumnName());

            return ro;
        }

        private static MatchingColumnMappingSource convert(MatchingColumnMappingRO source) {
            return new MatchingColumnMappingSource()
                    .withColumnName(source.getColumnName())
                    .withPath(source.getPath());
        }
    }
}
