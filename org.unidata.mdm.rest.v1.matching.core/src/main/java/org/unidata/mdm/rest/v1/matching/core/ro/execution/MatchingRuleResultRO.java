/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro.execution;

import org.unidata.mdm.rest.v1.matching.core.ro.cluster.ClusterRO;

/**
 * @author Sergey Murskiy on 22.08.2021
 */
public class MatchingRuleResultRO {
    /**
     * Rule name.
     */
    private String ruleName;
    /**
     * Rule display name.
     */
    private String ruleDisplayName;
    /**
     * Skip indicator.
     */
    private boolean skipped;
    /**
     * The cluster.
     */
    private ClusterRO cluster;

    /**
     * Constructor.
     */
    public MatchingRuleResultRO() {
        super();
    }

    /**
     * Gets the rule name.
     *
     * @return the ruleName
     */
    public String getRuleName() {
        return ruleName;
    }
    /**
     * Sets the rule name.
     *
     * @param ruleName the ruleName to set
     */
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
    /**
     * @return the ruleDisplayName
     */
    public String getRuleDisplayName() {
        return ruleDisplayName;
    }
    /**
     * @param ruleDisplayName the ruleDisplayName to set
     */
    public void setRuleDisplayName(String ruleDisplayName) {
        this.ruleDisplayName = ruleDisplayName;
    }
    /**
     * @return the skipped
     */
    public boolean isSkipped() {
        return skipped;
    }
    /**
     * @param skipped the skipped to set
     */
    public void setSkipped(boolean skipped) {
        this.skipped = skipped;
    }
    /**
     * @return the cluster
     */
    public ClusterRO getCluster() {
        return cluster;
    }
    /**
     * @param cluster the cluster to set
     */
    public void setCluster(ClusterRO cluster) {
        this.cluster = cluster;
    }
}
