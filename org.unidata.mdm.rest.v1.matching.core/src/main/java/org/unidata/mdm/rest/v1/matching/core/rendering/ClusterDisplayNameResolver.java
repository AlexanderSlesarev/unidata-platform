/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.rendering;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.context.DisplayNameResolutionContext;
import org.unidata.mdm.core.dto.DisplayNameResolutionResult;
import org.unidata.mdm.core.service.DisplayNameService;
import org.unidata.mdm.matching.core.type.cluster.ClusterRecord;
import org.unidata.mdm.matching.core.type.search.ClusterHeaderField;
import org.unidata.mdm.rest.v1.matching.core.module.MatchingCoreRestModule;
import org.unidata.mdm.search.dto.SearchResultHitDTO;
import org.unidata.mdm.search.util.SearchUtils;
import org.unidata.mdm.system.service.TextService;
import org.unidata.mdm.system.util.NameSpaceUtils;

import java.util.Date;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.11.2021
 */
@Component
public class ClusterDisplayNameResolver {
    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ClusterDisplayNameResolver.class);
    /**
     * Display name not available message.
     */
    private static final String DN_NOT_AVAILABLE = MatchingCoreRestModule.MODULE_ID + ".record.displayName.not.available";
    /**
     * Display name service.
     */
    @Autowired
    private DisplayNameService displayNameService;
    /**
     * Test service.
     */
    @Autowired
    private TextService textService;

    /**
     * Resolve cluster record display name.
     *
     * @param clusterRecord cluster record to resolve
     * @return display name
     */
    public String resolve(ClusterRecord clusterRecord) {
        return resolve(clusterRecord.getNamespace(), clusterRecord.getTypeName(), clusterRecord.getSubjectId(),
                clusterRecord.getValidFrom(), clusterRecord.getValidTo());
    }

    /**
     * Resolve cluster owner display name.
     *
     * @param hit cluster result search hit
     * @return owner display name
     */
    public String resolve(SearchResultHitDTO hit) {
        String namespace = hit.getFieldFirstValue(ClusterHeaderField.FIELD_OWNER_NAMESPACE.getName());
        String typeName = hit.getFieldFirstValue(ClusterHeaderField.FIELD_OWNER_TYPE_NAME.getName());
        String subjectId = hit.getFieldFirstValue(ClusterHeaderField.FIELD_OWNER_SUBJECT_ID.getName());
        Date validFrom =
                SearchUtils.parseFromIndex(hit.getFieldFirstValue(ClusterHeaderField.FIELD_OWNER_VALID_FROM.getName()));
        Date validTo =
                SearchUtils.parseFromIndex(hit.getFieldFirstValue(ClusterHeaderField.FIELD_OWNER_VALID_TO.getName()));

        return resolve(namespace, typeName, subjectId, validFrom, validTo);
    }

    private String resolve(String nameSpace, String typeName, String subjectId, Date validFrom, Date validTo) {

        String displayName = null;
        try {
            DisplayNameResolutionResult resolveResult =
                    displayNameService.resolve(DisplayNameResolutionContext.builder()
                            .nameSpace(NameSpaceUtils.find(nameSpace))
                            .typeName(typeName)
                            .subject(subjectId)
                            .validFrom(validFrom)
                            .validTo(validTo)
                            .inactive(false)
                            .deleted(false)
                            .build());

            if (Objects.nonNull(resolveResult)) {
                displayName = resolveResult.getDisplayValue();
            }
        } catch (Exception e) {
            final String message = "Unable to get display name for cluster record with namespace [{}], " +
                    "typeName [{}], subjectId [{}], validFrom [{}] and validTo [{}]";
            LOGGER.warn(message, nameSpace, typeName, subjectId, validFrom, validTo);
        }

        if (Objects.isNull(displayName)) {
            displayName = textService.getText(DN_NOT_AVAILABLE);
        }

        return displayName;
    }
}
