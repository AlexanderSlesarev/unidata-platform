/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.matching.core.context.MatchingModelGetContext;
import org.unidata.mdm.matching.core.context.MatchingModelUpsertContext;
import org.unidata.mdm.matching.core.dto.MatchingModelGetResult;
import org.unidata.mdm.matching.core.dto.MatchingTablesModelGetResult;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableSource;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.v1.matching.core.converter.MatchingTableConverter;
import org.unidata.mdm.rest.v1.matching.core.ro.DeleteMatchingTableResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.GetMatchingTableResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.GetMatchingTablesResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.UpsertMatchingTableRequestRO;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Path(MatchingTableRestService.SERVICE_PATH)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MatchingTableRestService extends AbstractRestService {
    /**
     * Service path.
     */
    public static final String SERVICE_PATH = "matching-tables";
    /**
     * Tag.
     */
    public static final String SERVICE_TAG = "matching-tables";

    /**
     * Matching table converter.
     */
    @Autowired
    private MatchingTableConverter matchingTableConverter;

    /**
     * The MMS.
     */
    @Autowired
    private MetaModelService metaModelService;

    @GET
    @Operation(
            description = "Get all defined matching tables.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema =
                    @Schema(implementation = GetMatchingTablesResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class))
                            , responseCode = "500")
            }
    )
    public GetMatchingTablesResultRO getAll(@QueryParam("draftId") @DefaultValue("0") Long draftId) {
        MatchingModelGetResult modelGetResult = metaModelService.get(MatchingModelGetContext.builder()
                .allMatchingTables(true)
                .draftId(draftId)
                .build());

        return new GetMatchingTablesResultRO(Objects.isNull(modelGetResult.getMatchingTables())
                ? Collections.emptyList()
                : matchingTableConverter.to(modelGetResult.getMatchingTables().getMatchingTables()));
    }

    @GET
    @Path("{id}")
    @Operation(
            description = "Get defined matching table by id.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The matching table id (name). Required.", in = ParameterIn.PATH, name =
                            "id"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content =
                    @Content(schema = @Schema(implementation = GetMatchingTableResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class))
                            , responseCode = "500")
            }
    )
    public GetMatchingTableResultRO getById(@PathParam("id") String id,
                                            @QueryParam("draftId") @DefaultValue("0") Long draftId) {

        MatchingModelGetResult modelGetResult = metaModelService.get(MatchingModelGetContext.builder()
                .matchingTableIds(Collections.singletonList(id))
                .draftId(draftId)
                .build());

        GetMatchingTableResultRO result = new GetMatchingTableResultRO();

        MatchingTablesModelGetResult tablesResult = modelGetResult.getMatchingTables();
        if (CollectionUtils.isNotEmpty(tablesResult.getMatchingTables()) && tablesResult.getMatchingTables().size() == 1) {
            result.setMatchingTable(matchingTableConverter.to(tablesResult.singleValue()));
        }

        return result;
    }

    @POST
    @Operation(
            description = "Create a matching table definition.",
            method = HttpMethod.POST,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            requestBody = @RequestBody(
                    content = @Content(schema = @Schema(implementation = UpsertMatchingTableRequestRO.class)),
                    description = "Matching table definition."),
            responses = {
                    @ApiResponse(content =
                    @Content(schema = @Schema(implementation = GetMatchingTableResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class))
                            , responseCode = "500")
            }
    )
    public GetMatchingTableResultRO create(@QueryParam("draftId") @DefaultValue("0") Long draftId,
                                           UpsertMatchingTableRequestRO request) {

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .matchingTablesUpdate(matchingTableConverter.from(request.getMatchingTable()))
                .draftId(draftId)
                .waitForFinish(true)
                .build());

        return getById(request.getMatchingTable().getName(), draftId);
    }

    @PUT
    @Path("{id}")
    @Operation(
            description = "Update a matching table definition.",
            method = HttpMethod.POST,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The matching table id (name). Required.", in = ParameterIn.PATH, name =
                            "id"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            requestBody = @RequestBody(
                    content = @Content(schema = @Schema(implementation = UpsertMatchingTableRequestRO.class)),
                    description = "Matching table definition."),
            responses = {
                    @ApiResponse(content =
                    @Content(schema = @Schema(implementation = GetMatchingTableResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class))
                            , responseCode = "500")
            }
    )
    public GetMatchingTableResultRO upsert(@PathParam("id") String id,
                                           @QueryParam("draftId") @DefaultValue("0") Long draftId,
                                           UpsertMatchingTableRequestRO request) {

        MatchingTableSource source = matchingTableConverter.from(request.getMatchingTable());

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .matchingTablesUpdate(source)
                .matchingTablesDelete(!StringUtils.equals(id, source.getName()) ? id : null)
                .draftId(draftId)
                .waitForFinish(true)
                .build());

        return getById(source.getName(), draftId);
    }

    @DELETE
    @Path("{id}")
    @Operation(
            description = "Delete an existing matching table definition.",
            method = HttpMethod.POST,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The matching table id (name). Required.", in = ParameterIn.PATH, name =
                            "id"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation =
                            DeleteMatchingTableResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class))
                            , responseCode = "500")
            }
    )
    public DeleteMatchingTableResultRO delete(@PathParam("id") String id,
                                              @QueryParam("draftId") @DefaultValue("0") Long draftId) {

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .matchingTablesDelete(id)
                .draftId(draftId)
                .build());

        return new DeleteMatchingTableResultRO();
    }
}
