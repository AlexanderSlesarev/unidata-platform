/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.matching.core.context.ClusterGetContext;
import org.unidata.mdm.matching.core.dto.ClusterGetResult;
import org.unidata.mdm.matching.core.service.ClusterService;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.v1.matching.core.converter.ClusterConverter;
import org.unidata.mdm.rest.v1.matching.core.ro.GetClusterResultRO;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 02.07.2021
 */
@Path(ClusterRestService.SERVICE_PATH)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ClusterRestService extends AbstractRestService {
    /**
     * Service path.
     */
    public static final String SERVICE_PATH = "clusters";
    /**
     * Tag.
     */
    public static final String SERVICE_TAG = "clusters";

    /**
     * Cluster service.
     */
    @Autowired
    private ClusterService clusterService;
    /**
     * Cluster converter.
     */
    @Autowired
    private ClusterConverter clusterConverter;

    @GET
    @Path("{id}")
    @Operation(
            description = "Get cluster by id.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The cluster id. Required.", in = ParameterIn.PATH, name = "id")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetClusterResultRO.class)),
                            responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class))
                            , responseCode = "500")
            }
    )
    public GetClusterResultRO getById(@PathParam("id") String id) {
        ClusterGetResult clusterGetResult = clusterService.get(ClusterGetContext.builder()
                .id(id)
                .fetchClusterRecords(true)
                .build());

        if (Objects.isNull(clusterGetResult.singleValue())) {
            return new GetClusterResultRO();
        }

        GetClusterResultRO result = new GetClusterResultRO();
        result.setCluster(clusterConverter.to(clusterGetResult.singleValue()));

        return result;
    }
}
