/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro.algorithm;

import java.util.List;

/**
 * @author Sergey Murskiy on 10.06.2021
 */
public class AlgorithmSettingsRO {
    /**
     * Settings id.
     */
    private String id;
    /**
     * Algorithm name.
     */
    private String algorithmName;
    /**
     * Algorithm parameters.
     */
    private List<AlgorithmParameterRO> parameters;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return algorithm name
     */
    public String getAlgorithmName() {
        return algorithmName;
    }

    /**
     * @param algorithmName algorithm name to set
     */
    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    /**
     * @return algorithm parameters
     */
    public List<AlgorithmParameterRO> getParameters() {
        return parameters;
    }

    /**
     * @param parameters algorithm parameters to set
     */
    public void setParameters(List<AlgorithmParameterRO> parameters) {
        this.parameters = parameters;
    }
}
