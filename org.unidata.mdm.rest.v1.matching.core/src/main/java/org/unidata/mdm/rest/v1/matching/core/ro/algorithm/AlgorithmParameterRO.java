/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro.algorithm;

import org.unidata.mdm.rest.v1.matching.core.ro.AlgorithmParameterType;

/**
 * @author Sergey Murskiy on 10.06.2021
 */
public class AlgorithmParameterRO {
    /**
     * Parameter name.
     */
    private String name;
    /**
     * Type.
     */
    private AlgorithmParameterType type;
    /**
     * Value.
     */
    private Object value;

    /**
     * @return parameter name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name parameter name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public AlgorithmParameterType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(AlgorithmParameterType type) {
        this.type = type;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Object value) {
        this.value = value;
    }
}
