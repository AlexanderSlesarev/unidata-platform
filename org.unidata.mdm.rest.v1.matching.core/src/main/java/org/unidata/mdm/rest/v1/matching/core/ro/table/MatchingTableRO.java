/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.matching.core.ro.table;

import org.unidata.mdm.rest.core.ro.CustomPropertyRO;
import org.unidata.mdm.rest.v1.matching.core.ro.AbstractMatchingElementRO;

import java.util.List;

/**
 * @author Sergey Murskiy on 08.06.2021
 */
public class MatchingTableRO extends AbstractMatchingElementRO {
    /**
     * Table columns.
     */
    private List<MatchingColumnRO> columns;
    /**
     * Table custom properties.
     */
    private List<CustomPropertyRO> customProperties;

    /**
     * @return table columns
     */
    public List<MatchingColumnRO> getColumns() {
        return columns;
    }

    /**
     * @param columns table columns to set
     */
    public void setColumns(List<MatchingColumnRO> columns) {
        this.columns = columns;
    }

    /**
     * @return table custom properties
     */
    public List<CustomPropertyRO> getCustomProperties() {
        return customProperties;
    }

    /**
     * @param customProperties table custom properties to set
     */
    public void setCustomProperties(List<CustomPropertyRO> customProperties) {
        this.customProperties = customProperties;
    }
}
