/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Sergey Murskiy on 10.06.2021
 */
public enum AlgorithmParameterType {
    /**
     * String type.
     */
    STRING("String"),
    /**
     * Integer type.
     */
    INTEGER("Integer"),
    /**
     * Number type.
     */
    NUMBER("Number"),
    /**
     * Boolean type.
     */
    BOOLEAN("Boolean"),
    /**
     * Date type.
     */
    DATE("Date"),
    /**
     * Enum type
     */
    ENUM("Enum");

    /**
     * Value.
     */
    private String value;

    /**
     * Constructor.
     *
     * @param value the value
     */
    AlgorithmParameterType(String value) {
        this.value = value;
    }

    /**
     * @return the value
     */
    @JsonValue
    public String getValue() {
        return value;
    }

    /**
     * From value.
     *
     * @param v the value
     * @return the algorithm parameter type
     */
    @JsonCreator
    public static AlgorithmParameterType fromValue(String v) {
        for (AlgorithmParameterType c : AlgorithmParameterType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
