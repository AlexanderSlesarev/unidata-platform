/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro.cluster;

import java.util.Date;

/**
 * @author Sergey Murskiy on 02.07.2021
 */
public class ClusterRecordRO {
    /**
     * Namespace.
     */
    private String namespace;
    /**
     * Type name.
     */
    private String typeName;
    /**
     * Matching record id.
     */
    private String subjectId;
    /**
     * Valid from.
     */
    private Date validFrom;
    /**
     * Valid to.
     */
    private Date validTo;
    /**
     * Matching rate. Always 100% for exact rules.
     */
    private int matchingRate;
    /**
     * Display name.
     */
    private String displayName;

    /**
     * Gets namespace.
     *
     * @return the namespace
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Sets namespace.
     *
     * @param namespace the namespace to set
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /**
     * Gets type name.
     *
     * @return the type name
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets type name.
     *
     * @param typeName the type name to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * Gets subject id.
     *
     * @return the subject id
     */
    public String getSubjectId() {
        return subjectId;
    }

    /**
     * Sets subject id.
     *
     * @param subjectId the subject id to set
     */
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * Gets valid from.
     *
     * @return the valid from
     */
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * Sets valid from.
     *
     * @param validFrom the valid from to set
     */
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * Gets valid to.
     *
     * @return the valid to
     */
    public Date getValidTo() {
        return validTo;
    }

    /**
     * Sets valid to.
     *
     * @param validTo the valid to to set
     */
    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    /**
     * Gets matching rate.
     *
     * @return the matching rate
     */
    public int getMatchingRate() {
        return matchingRate;
    }

    /**
     * Sets matching rate.
     *
     * @param matchingRate the matching rate to set
     */
    public void setMatchingRate(int matchingRate) {
        this.matchingRate = matchingRate;
    }

    /**
     * Gets display name.
     *
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets display name.
     *
     * @param displayName the display name to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
