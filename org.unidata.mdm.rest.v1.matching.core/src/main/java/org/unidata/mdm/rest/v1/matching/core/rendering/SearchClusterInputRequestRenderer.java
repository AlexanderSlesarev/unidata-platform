/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.rendering;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.unidata.mdm.matching.core.configuration.MatchingCoreConfigurationConstants;
import org.unidata.mdm.matching.core.type.search.ClusterChildIndexType;
import org.unidata.mdm.matching.core.type.search.ClusterHeadIndexType;
import org.unidata.mdm.matching.core.type.search.ClusterHeaderField;
import org.unidata.mdm.matching.core.type.search.ClusterRecordHeaderField;
import org.unidata.mdm.rest.system.service.TypedRestInputRenderer;
import org.unidata.mdm.rest.v1.matching.core.module.MatchingCoreRestModule;
import org.unidata.mdm.rest.v1.matching.core.ro.cluster.ComplexSearchClusterQueryRO;
import org.unidata.mdm.rest.v1.matching.core.ro.cluster.SearchClusterType;
import org.unidata.mdm.rest.v1.matching.core.ro.cluster.SimpleSearchClusterQueryRO;
import org.unidata.mdm.rest.v1.search.converter.SearchFieldsConverter;
import org.unidata.mdm.rest.v1.search.ro.SearchRequestRO;
import org.unidata.mdm.rest.v1.search.ro.SearchSortFieldRO;
import org.unidata.mdm.search.context.ComplexSearchRequestContext;
import org.unidata.mdm.search.context.SearchRequestContext;
import org.unidata.mdm.search.exception.SearchExceptionIds;
import org.unidata.mdm.search.type.IndexField;
import org.unidata.mdm.search.type.IndexType;
import org.unidata.mdm.search.type.form.FieldsGroup;
import org.unidata.mdm.search.type.query.SearchQuery;
import org.unidata.mdm.search.type.sort.SortField;
import org.unidata.mdm.system.context.InputCollector;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.type.rendering.FieldDef;
import org.unidata.mdm.system.type.rendering.FragmentDef;
import org.unidata.mdm.system.type.rendering.MapInputSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 01.07.2021
 */
public class SearchClusterInputRequestRenderer extends TypedRestInputRenderer<SearchRequestRO> {
    /**
     * Search type mapping.
     */
    private static final Map<SearchClusterType, IndexType> TYPE_MAPPING = Map.of(
            SearchClusterType.CLUSTER, ClusterHeadIndexType.CLUSTER,
            SearchClusterType.RECORD, ClusterChildIndexType.CLUSTER_RECORD
    );
    /**
     * Cluster owner fields.
     */
    private static final Set<String> CLUSTER_OWNER_FIELDS = Set.of(
            ClusterHeaderField.FIELD_OWNER_NAMESPACE.getName(),
            ClusterHeaderField.FIELD_OWNER_TYPE_NAME.getName(),
            ClusterHeaderField.FIELD_OWNER_SUBJECT_ID.getName(),
            ClusterHeaderField.FIELD_OWNER_VALID_FROM.getName(),
            ClusterHeaderField.FIELD_OWNER_VALID_TO.getName()
    );

    /**
     * Cluster system fields.
     */
    private final Map<String, ClusterHeaderField> clusterHeaderFieldsByName;
    /**
     * Record system fields.
     */
    private final Map<String, ClusterRecordHeaderField> recordHeaderFieldsByName;

    /**
     * Constructor.
     */
    public SearchClusterInputRequestRenderer() {
        super(SearchRequestRO.class, Collections.singletonList(FieldDef.fieldDef(MatchingCoreRestModule.MODULE_ID,
                ComplexSearchClusterQueryRO.class)));

        clusterHeaderFieldsByName = Arrays.stream(ClusterHeaderField.values())
                .collect(Collectors.toMap(IndexField::getPath, Function.identity()));
        recordHeaderFieldsByName = Arrays.stream(ClusterRecordHeaderField.values())
                .collect(Collectors.toMap(IndexField::getPath, Function.identity()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void renderFields(FragmentDef fragmentDef, InputCollector collector, SearchRequestRO source,
                                MapInputSource fieldValues) {
        ComplexSearchClusterQueryRO searchClusterQuery = fieldValues.get(MatchingCoreRestModule.MODULE_ID,
                ComplexSearchClusterQueryRO.class);

        if (Objects.nonNull(searchClusterQuery) && collector instanceof ComplexSearchRequestContext.ComplexSearchRequestContextBuilder) {
            ComplexSearchRequestContext.ComplexSearchRequestContextBuilder context = (ComplexSearchRequestContext.ComplexSearchRequestContextBuilder) collector;
            SearchClusterType searchClusterType = searchClusterQuery.getSearchClusterType();
            List<SimpleSearchClusterQueryRO> supplementary = ObjectUtils.defaultIfNull(searchClusterQuery.getSupplementary(), Collections.emptyList());

            if (Objects.nonNull(searchClusterType)) {
                context.main(renderSimpleSearchInput(SearchRequestContext
                        .builder(MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME), searchClusterQuery)
                        .build());
            }

            for (SimpleSearchClusterQueryRO sub : supplementary) {
                SearchClusterType subType = sub.getSearchClusterType();
                if (subType != null) {
                    SearchRequestContext.SearchRequestContextBuilder subCtx = SearchRequestContext
                            .builder(MatchingCoreConfigurationConstants.DEFAULT_CLUSTER_INDEX_NAME);
                    context.supplementary(renderSimpleSearchInput(subCtx, sub).build());
                }
            }
        }
    }

    private SearchRequestContext.SearchRequestContextBuilder renderSimpleSearchInput(SearchRequestContext.SearchRequestContextBuilder srcb,
                                                                                     SimpleSearchClusterQueryRO source) {
        SearchClusterType type = source.getSearchClusterType();
        IndexType indexType = indexTypeByDataType(type);

        srcb.type(indexType)
                .count(source.getCount())
                .page(source.getPage() > 0 ? source.getPage() - 1 : source.getPage())
                .totalCount(source.isTotalCount())
                .countOnly(source.isCountOnly())
                .fetchAll(source.isFetchAll())
                .skipEtalonId(true)
                .sayt(source.isSayt());

        Map<String, IndexField> fieldSpace = indexType == ClusterChildIndexType.CLUSTER_RECORD
                ? new HashMap<>(recordHeaderFieldsByName)
                : new HashMap<>(clusterHeaderFieldsByName);

        srcb.query(renderFormFields(source, fieldSpace));
        srcb.query(renderFormGroups(source, fieldSpace));
        srcb.returnFields(new ArrayList<>(renderReturnFields(source, fieldSpace)));
        srcb.sorting(renderSortFields(source, indexType, fieldSpace));
        srcb.joinBy(indexType == ClusterHeadIndexType.CLUSTER ? ClusterHeaderField.FIELD_ID : ClusterRecordHeaderField.FIELD_CLUSTER_ID);

        return srcb;
    }

    private SearchQuery renderFormFields(SimpleSearchClusterQueryRO source, Map<String, IndexField> fieldSpace) {
        Objects.requireNonNull(source, "Source can't be null");
        Objects.requireNonNull(fieldSpace, "FieldSpace can't be null");

        List<FieldsGroup> groups = Optional.ofNullable(source.getFormFields())
                .map(ff -> SearchFieldsConverter.convertFormFields(ff, fieldSpace))
                .orElseGet(Collections::emptyList);

        return CollectionUtils.isNotEmpty(groups) ? SearchQuery.formQuery(groups) : null;
    }

    private SearchQuery renderFormGroups(SimpleSearchClusterQueryRO source, Map<String, IndexField> fieldSpace) {
        Objects.requireNonNull(source, "Source can't be null");
        Objects.requireNonNull(fieldSpace, "FieldSpace can't be null");

        List<FieldsGroup> groups = Optional.ofNullable(source.getFormGroups()).map(fg -> fg.stream()
                .map(g -> SearchFieldsConverter.convertFormFieldsGroup(g, fieldSpace))
                .collect(Collectors.toList()))
                .orElseGet(Collections::emptyList);

        return CollectionUtils.isNotEmpty(groups) ? SearchQuery.formQuery(groups) : null;
    }

    private Set<String> renderReturnFields(SimpleSearchClusterQueryRO source, Map<String, IndexField> fieldSpace) {
        if (source.isReturnAllFields()) {
            return fieldSpace.keySet();
        }

        Set<String> returnFields = new HashSet<>();

        if (CollectionUtils.isNotEmpty(source.getReturnFields())) {
            returnFields.addAll(source.getReturnFields());
        }

        if (source.getSearchClusterType() == SearchClusterType.CLUSTER) {
            returnFields.addAll(CLUSTER_OWNER_FIELDS);
        }

        return returnFields;
    }

    private Collection<SortField> renderSortFields(SimpleSearchClusterQueryRO source, IndexType target, Map<String,
            IndexField> fieldSpace) {
        Collection<SearchSortFieldRO> fields = source.getSortFields();
        List<SortField> result = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(fields)) {
            for (SearchSortFieldRO sortFieldRO : fields) {
                IndexField sortRef = fieldSpace.get(sortFieldRO.getField());
                if (Objects.nonNull(sortRef)) {
                    result.add(SortField.of(sortRef, SortField.SortOrder.valueOf(sortFieldRO.getOrder())));
                }
            }
        } else if (ClusterHeadIndexType.CLUSTER == target) {
            result.add(SortField.of(ClusterHeaderField.FIELD_MATCHING_DATE, SortField.SortOrder.DESC));
        } else if (ClusterChildIndexType.CLUSTER_RECORD == target) {
            result.add(SortField.of(ClusterRecordHeaderField.FIELD_MATCHING_RATE, SortField.SortOrder.DESC));
        }

        return result;
    }

    private IndexType indexTypeByDataType(SearchClusterType type) {
        Objects.requireNonNull(type, "ClusterType can't be null");
        IndexType indexType = TYPE_MAPPING.get(type);

        if (Objects.isNull(indexType)) {
            throw new PlatformFailureException(
                    "Unsupported cluster type:" + type, SearchExceptionIds.EX_SEARCH_MAPPING_TYPE_UNKNOWN
            );
        }

        return indexType;
    }
}
