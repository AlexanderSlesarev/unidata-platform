/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro.algorithm;

import org.unidata.mdm.rest.core.ro.CustomPropertyRO;
import org.unidata.mdm.rest.v1.matching.core.ro.AbstractMatchingElementRO;

import java.util.List;

/**
 * @author Sergey Murskiy on 10.06.2021
 */
public class MatchingAlgorithmRO extends AbstractMatchingElementRO {
    /**
     * Matching storage id.
     */
    private String matchingStorageId;
    /**
     * Java class.
     */
    private String javaClass;
    /**
     * Library name.
     */
    private String libraryName;
    /**
     * Library version.
     */
    private String libraryVersion;
    /**
     * Algorithm parameters.
     */
    private List<AlgorithmParamConfigurationRO> parameters;
    /**
     * Current algorithm state indicator/annotation.
     */
    private String note;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    private boolean system;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    private boolean ready;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    private boolean configurable;
    /*
     * This field is for disassembling only.
     * Not marshalled to DB.
     */
    private boolean editable;
    /**
     * Algorithm custom properties.
     */
    private List<CustomPropertyRO> customProperties;

    /**
     * @return matching storage id
     */
    public String getMatchingStorageId() {
        return matchingStorageId;
    }

    /**
     * @param matchingStorageId matching storage id to set
     */
    public void setMatchingStorageId(String matchingStorageId) {
        this.matchingStorageId = matchingStorageId;
    }

    /**
     * @return java class
     */
    public String getJavaClass() {
        return javaClass;
    }

    /**
     * @param javaClass java class to set
     */
    public void setJavaClass(String javaClass) {
        this.javaClass = javaClass;
    }

    /**
     * @return library name
     */
    public String getLibraryName() {
        return libraryName;
    }

    /**
     * @param libraryName library name to set
     */
    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    /**
     * @return library version
     */
    public String getLibraryVersion() {
        return libraryVersion;
    }

    /**
     * @param libraryVersion library version to set
     */
    public void setLibraryVersion(String libraryVersion) {
        this.libraryVersion = libraryVersion;
    }

    /**
     * @return algorithm parameters
     */
    public List<AlgorithmParamConfigurationRO> getParameters() {
        return parameters;
    }

    /**
     * @param parameters aplgorithm parameters to set
     */
    public void setParameters(List<AlgorithmParamConfigurationRO> parameters) {
        this.parameters = parameters;
    }

    /**
     * @return note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return is system
     */
    public boolean isSystem() {
        return system;
    }

    /**
     * @param system is system to set
     */
    public void setSystem(boolean system) {
        this.system = system;
    }

    /**
     * @return is ready
     */
    public boolean isReady() {
        return ready;
    }

    /**
     * @param ready is ready to set
     */
    public void setReady(boolean ready) {
        this.ready = ready;
    }

    /**
     * @return is configurable
     */
    public boolean isConfigurable() {
        return configurable;
    }

    /**
     * @param configurable is configurable to set
     */
    public void setConfigurable(boolean configurable) {
        this.configurable = configurable;
    }

    /**
     * @return is editable
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * @param editable is editable to set
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    /**
     * @return rule custom properties
     */
    public List<CustomPropertyRO> getCustomProperties() {
        return customProperties;
    }

    /**
     * @param customProperties rule custom properties to set
     */
    public void setCustomProperties(List<CustomPropertyRO> customProperties) {
        this.customProperties = customProperties;
    }
}
