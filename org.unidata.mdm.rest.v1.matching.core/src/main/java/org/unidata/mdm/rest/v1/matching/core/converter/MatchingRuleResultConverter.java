package org.unidata.mdm.rest.v1.matching.core.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.matching.core.dto.MatchingResult.RuleExecutionResult;
import org.unidata.mdm.rest.v1.matching.core.ro.execution.MatchingRuleResultRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Sergey Murskiy on 22.08.2021
 */
@Component
public class MatchingRuleResultConverter extends Converter<RuleExecutionResult, MatchingRuleResultRO> {

    /**
     * Cluster converter.
     */
    @Autowired
    private ClusterConverter clusterConverter;

    /**
     * Constructor.
     */
    public MatchingRuleResultConverter() {
        this.to = this::convert;
    }

    private MatchingRuleResultRO convert(RuleExecutionResult source) {
        MatchingRuleResultRO target = new MatchingRuleResultRO();

        target.setSkipped(source.isSkipped());
        target.setRuleName(source.getRule().getName());
        target.setRuleDisplayName(source.getRule().getDisplayName());
        target.setCluster(clusterConverter.to(source.getCluster()));

        return target;
    }
}
