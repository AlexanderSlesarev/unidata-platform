/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.converter;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.util.AttributeUtils;
import org.unidata.mdm.matching.core.type.model.source.algorithm.AlgorithmParameterSource;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.AlgorithmParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.BooleanParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.DateParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.EnumParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.IntegerParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.NumberParameterValue;
import org.unidata.mdm.matching.core.type.model.source.algorithm.parameter.StringParameterValue;
import org.unidata.mdm.rest.v1.matching.core.ro.AlgorithmParameterType;
import org.unidata.mdm.rest.v1.matching.core.ro.algorithm.AlgorithmParameterRO;
import org.unidata.mdm.system.convert.Converter;

import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Component
public class AlgorithmParameterConverter extends Converter<AlgorithmParameterSource, AlgorithmParameterRO> {

    /**
     * Constructor.
     */
    public AlgorithmParameterConverter() {
        this.to = this::convert;
        this.from = this::convert;
    }

    private AlgorithmParameterRO convert(AlgorithmParameterSource source) {
        AlgorithmParameterRO ro = new AlgorithmParameterRO();

        ro.setName(source.getName());
        ro.setType(AlgorithmParameterType.fromValue(source.getType().getValue()));

        AlgorithmParameterValue<?> value = source.getValue();

        if (Objects.nonNull(value)) {
            ro.setValue(value.getValue());
        }

        return ro;
    }

    private AlgorithmParameterSource convert(AlgorithmParameterRO ro) {
        AlgorithmParameterSource source = new AlgorithmParameterSource()
                .withName(ro.getName())
                .withType(org.unidata.mdm.matching.core.type.algorithm.AlgorithmParameterType.fromValue(ro.getType().getValue()));

        switch (ro.getType()) {
            case DATE:
                return source.withValue(new DateParameterValue().withValue(AttributeUtils.toDate(ro.getValue())));
            case ENUM:
                return source.withValue(new EnumParameterValue().withValue(AttributeUtils.toString(ro.getValue())));
            case NUMBER:
                return source.withValue(new NumberParameterValue().withValue(AttributeUtils.toDouble(ro.getValue())));
            case STRING:
                return source.withValue(new StringParameterValue().withValue(AttributeUtils.toString(ro.getValue())));
            case BOOLEAN:
                return source.withValue(new BooleanParameterValue().withValue(AttributeUtils.toBoolean(ro.getValue())));
            case INTEGER:
                return source.withValue(new IntegerParameterValue().withValue(AttributeUtils.toLong(ro.getValue())));
            default:
                return null;
        }
    }
}
