/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.matching.core.ro.table;

import org.unidata.mdm.rest.v1.matching.core.ro.MatchingDataType;

/**
 *
 * @author Sergey Murskiy on 08.06.2021
 */
public class MatchingColumnRO {
    /**
     * Column name.
     */
    private String name;
    /**
     * Column display name.
     */
    private String displayName;
    /**
     * Column type.
     */
    private MatchingDataType type;

    /**
     * @return column name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name column name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the display name to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return column type
     */
    public MatchingDataType getType() {
        return type;
    }

    /**
     * @param type column type to set
     */
    public void setType(MatchingDataType type) {
        this.type = type;
    }
}
