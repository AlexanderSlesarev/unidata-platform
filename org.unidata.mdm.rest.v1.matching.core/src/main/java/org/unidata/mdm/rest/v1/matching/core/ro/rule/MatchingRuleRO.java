/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro.rule;

import org.unidata.mdm.rest.core.ro.CustomPropertyRO;
import org.unidata.mdm.rest.v1.matching.core.ro.AbstractMatchingElementRO;
import org.unidata.mdm.rest.v1.matching.core.ro.algorithm.AlgorithmSettingsRO;

import java.util.List;

/**
 * @author Sergey Murskiy on 10.06.2021
 */
public class MatchingRuleRO extends AbstractMatchingElementRO {
    /**
     * Matching storage id.
     */
    private String matchingStorageId;
    /**
     * Rule algorithms.
     */
    private List<AlgorithmSettingsRO> algorithms;
    /**
     * Rule custom properties.
     */
    private List<CustomPropertyRO> customProperties;

    /**
     * @return matching storage id
     */
    public String getMatchingStorageId() {
        return matchingStorageId;
    }

    /**
     * @param matchingStorageId matching storage id to set
     */
    public void setMatchingStorageId(String matchingStorageId) {
        this.matchingStorageId = matchingStorageId;
    }

    /**
     * @return rule algorithms
     */
    public List<AlgorithmSettingsRO> getAlgorithms() {
        return algorithms;
    }

    /**
     * @param algorithms rule algorithms to set
     */
    public void setAlgorithms(List<AlgorithmSettingsRO> algorithms) {
        this.algorithms = algorithms;
    }

    /**
     * @return rule custom properties
     */
    public List<CustomPropertyRO> getCustomProperties() {
        return customProperties;
    }

    /**
     * @param customProperties rule custom properties to set
     */
    public void setCustomProperties(List<CustomPropertyRO> customProperties) {
        this.customProperties = customProperties;
    }
}
