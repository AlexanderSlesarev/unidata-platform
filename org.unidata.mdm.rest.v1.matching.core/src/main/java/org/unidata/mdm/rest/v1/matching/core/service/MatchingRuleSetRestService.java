/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.matching.core.context.MatchingModelGetContext;
import org.unidata.mdm.matching.core.context.MatchingModelUpsertContext;
import org.unidata.mdm.matching.core.dto.MatchingModelGetResult;
import org.unidata.mdm.matching.core.dto.SetsModelGetResult;
import org.unidata.mdm.matching.core.type.model.source.rule.MatchingRuleSetSource;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.v1.matching.core.converter.MatchingRuleSetConverter;
import org.unidata.mdm.rest.v1.matching.core.ro.DeleteMatchingSetResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.GetMatchingSetResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.GetMatchingSetsResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.UpsertMatchingSetRequestRO;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Path(MatchingRuleSetRestService.SERVICE_PATH)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MatchingRuleSetRestService extends AbstractRestService {
    /**
     * Service path.
     */
    public static final String SERVICE_PATH = "sets";
    /**
     * Tag.
     */
    public static final String SERVICE_TAG = "sets";
    /**
     * Matching rule set converter.
     */
    @Autowired
    private MatchingRuleSetConverter matchingRuleSetConverter;

    /**
     * The MMS.
     */
    @Autowired
    private MetaModelService metaModelService;

    @GET
    @Operation(
            description = "Get all defined matching rule sets.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingSetsResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingSetsResultRO getAll(@QueryParam("draftId") @DefaultValue("0") Long draftId) {

        MatchingModelGetResult getResult = metaModelService.get(MatchingModelGetContext.builder()
                .allMatchingRuleSets(true)
                .draftId(draftId)
                .build());

        return new GetMatchingSetsResultRO(Objects.isNull(getResult.getSets())
                ? Collections.emptyList()
                : matchingRuleSetConverter.to(getResult.getSets().getSets()));
    }

    @GET
    @Path("{id}")
    @Operation(
            description = "Get a matching rule set definition by id.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The matching rule set id (name). Required.", in = ParameterIn.PATH, name = "id"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingSetResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingSetResultRO getById(@PathParam("id") String id,
                                          @QueryParam("draftId") @DefaultValue("0") Long draftId) {

        MatchingModelGetResult modelGetResult = metaModelService.get(MatchingModelGetContext.builder()
                .matchingRuleSetIds(Collections.singletonList(id))
                .draftId(draftId)
                .build());

        GetMatchingSetResultRO result = new GetMatchingSetResultRO();

        SetsModelGetResult setsResult = modelGetResult.getSets();
        if (CollectionUtils.isNotEmpty(setsResult.getSets()) && setsResult.getSets().size() == 1) {
            result.setSet(matchingRuleSetConverter.to(setsResult.singleValue()));
        }

        return result;
    }

    @POST
    @Operation(
            description = "Create a matching rule set definition.",
            method = HttpMethod.POST,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            requestBody = @RequestBody(
                    content = @Content(schema = @Schema(implementation = UpsertMatchingSetRequestRO.class)),
                    description = "Matching rule set definition."),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingSetResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingSetResultRO create(@QueryParam("draftId") @DefaultValue("0") Long draftId,
                                         UpsertMatchingSetRequestRO request) {

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .setsUpdate(matchingRuleSetConverter.from(request.getSet()))
                .draftId(draftId)
                .waitForFinish(true)
                .build());

        return getById(request.getSet().getName(), draftId);
    }

    @PUT
    @Path("{id}")
    @Operation(
            description = "Update a matching rule set definition.",
            method = HttpMethod.POST,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The matching rule set id. Required.", in = ParameterIn.PATH, name = "id"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            requestBody = @RequestBody(
                    content = @Content(schema = @Schema(implementation = UpsertMatchingSetRequestRO.class)),
                    description = "Matching rule set definition."),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetMatchingSetResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetMatchingSetResultRO upsert(@PathParam("id") String id,
                                         @QueryParam("draftId") @DefaultValue("0") Long draftId,
                                         UpsertMatchingSetRequestRO request) {

        MatchingRuleSetSource source = matchingRuleSetConverter.from(request.getSet());

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .setsUpdate(source)
                .setsDelete(!StringUtils.equals(id, source.getName()) ? id : null)
                .draftId(draftId)
                .waitForFinish(true)
                .build());

        return getById(source.getName(), draftId);
    }

    @DELETE
    @Path("{id}")
    @Operation(
            description = "Delete an existing matching rule set definition by id.",
            method = HttpMethod.DELETE,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The matching rule set id. Required.", in = ParameterIn.PATH, name = "id"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DeleteMatchingSetResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public DeleteMatchingSetResultRO delete(@PathParam("id") String id,
                                            @QueryParam("draftId") @DefaultValue("0") Long draftId) {

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .setsDelete(id)
                .draftId(draftId)
                .build());

        return new DeleteMatchingSetResultRO();
    }
}
