/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.matching.core.ro.table.MatchingTableRO;

import java.util.List;

/**
 * @author Sergey Murskiy on 08.06.2021
 */
public class GetMatchingTablesResultRO extends DetailedOutputRO {
    /**
     * Matching tables.
     */
    private List<MatchingTableRO> matchingTables;

    /**
     * Constructor.
     */
    public GetMatchingTablesResultRO() {
        super();
    }

    /**
     * Constructor.
     *
     * @param matchingTables matching tables to set
     */
    public GetMatchingTablesResultRO(List<MatchingTableRO> matchingTables) {
        this.matchingTables = matchingTables;
    }

    /**
     * @return matching tables
     */
    public List<MatchingTableRO> getMatchingTables() {
        return matchingTables;
    }

    /**
     * @param matchingTables matching tables to set
     */
    public void setMatchingTables(List<MatchingTableRO> matchingTables) {
        this.matchingTables = matchingTables;
    }
}
