/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingColumnSource;
import org.unidata.mdm.matching.core.type.model.source.table.MatchingTableSource;
import org.unidata.mdm.rest.core.converter.CustomPropertiesConverter;
import org.unidata.mdm.rest.v1.matching.core.ro.MatchingDataType;
import org.unidata.mdm.rest.v1.matching.core.ro.table.MatchingColumnRO;
import org.unidata.mdm.rest.v1.matching.core.ro.table.MatchingTableRO;
import org.unidata.mdm.system.convert.Converter;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Component
public class MatchingTableConverter extends Converter<MatchingTableSource, MatchingTableRO> {
    /**
     * Matching column converter.
     */
    private static final MatchingColumnConverter COLUMN_CONVERTER = new MatchingColumnConverter();

    /**
     * Constructor.
     */
    public MatchingTableConverter() {
        this.to = this::convert;
        this.from = this::convert;
    }

    private MatchingTableRO convert(MatchingTableSource source) {
        MatchingTableRO ro = new MatchingTableRO();

        ro.setName(source.getName());
        ro.setDisplayName(source.getDisplayName());
        ro.setDescription(source.getDescription());
        ro.setColumns(COLUMN_CONVERTER.to(source.getColumns()));
        ro.setCustomProperties(CustomPropertiesConverter.to(source.getCustomProperties()));
        ro.setCreateDate(source.getCreateDate());
        ro.setCreatedBy(source.getCreatedBy());
        ro.setUpdateDate(source.getUpdateDate());
        ro.setUpdatedBy(source.getUpdatedBy());

        return ro;
    }

    private MatchingTableSource convert(MatchingTableRO source) {
        String user = SecurityUtils.getCurrentUserName();
        OffsetDateTime now = OffsetDateTime.now();

        return new MatchingTableSource()
                .withName(source.getName())
                .withDisplayName(source.getDisplayName())
                .withDescription(source.getDescription())
                .withColumns(COLUMN_CONVERTER.from(source.getColumns()))
                .withCustomProperties(CustomPropertiesConverter.from(source.getCustomProperties()))
                .withCreateDate(Objects.isNull(source.getCreateDate()) ? now : source.getCreateDate())
                .withCreatedBy(StringUtils.isBlank(source.getCreatedBy()) ? user : source.getCreatedBy())
                .withUpdateDate(Objects.isNull(source.getCreateDate()) ? null : now)
                .withUpdatedBy(StringUtils.isBlank(source.getCreatedBy()) ? null : user);
    }

    private static class MatchingColumnConverter extends Converter<MatchingColumnSource, MatchingColumnRO> {

        /**
         * Constructor.
         */
        public MatchingColumnConverter() {
            super(MatchingColumnConverter::convert, MatchingColumnConverter::convert);
        }

        private static MatchingColumnRO convert(MatchingColumnSource source) {
            MatchingColumnRO ro = new MatchingColumnRO();

            ro.setName(source.getName());
            ro.setDisplayName(source.getDisplayName());
            ro.setType(MatchingDataType.fromValue(source.getType().getValue()));

            return ro;
        }

        private static MatchingColumnSource convert(MatchingColumnRO source) {
            return new MatchingColumnSource()
                    .withName(source.getName())
                    .withDisplayName(source.getDisplayName())
                    .withType(org.unidata.mdm.matching.core.type.data.MatchingDataType.fromValue(source.getType().getValue()));
        }
    }
}
