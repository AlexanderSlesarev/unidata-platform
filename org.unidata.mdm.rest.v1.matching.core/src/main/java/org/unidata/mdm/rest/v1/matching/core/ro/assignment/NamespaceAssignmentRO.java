/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro.assignment;

import org.unidata.mdm.rest.v1.matching.core.ro.table.MatchingTableMappingRO;

import java.util.List;

/**
 * @author Sergey Murskiy on 10.06.2021
 */
public class NamespaceAssignmentRO {
    /**
     * Namespace.
     */
    private String namespace;
    /**
     * Type name.
     */
    private String typeName;
    /**
     * Assignment key.
     */
    private String assignmentKey;
    /**
     * Matching rule sets.
     */
    private List<String> sets;
    /**
     * Matching tables.
     */
    private List<String> matchingTables;
    /**
     * Matching table mappings.
     */
    private List<MatchingTableMappingRO> mappings;

    /**
     * @return namespace
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * @param namespace namespace to set
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /**
     * @return type name
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param typeName type name to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * @return assignment key
     */
    public String getAssignmentKey() {
        return assignmentKey;
    }

    /**
     * @param assignmentKey assignment key to set
     */
    public void setAssignmentKey(String assignmentKey) {
        this.assignmentKey = assignmentKey;
    }

    /**
     * @return matching rule sets
     */
    public List<String> getSets() {
        return sets;
    }

    /**
     * @param sets matching rule sets to set
     */
    public void setSets(List<String> sets) {
        this.sets = sets;
    }

    /**
     * @return matching tables
     */
    public List<String> getMatchingTables() {
        return matchingTables;
    }

    /**
     * @param matchingTables matching tables to set
     */
    public void setMatchingTables(List<String> matchingTables) {
        this.matchingTables = matchingTables;
    }

    /**
     * @return matching tables mappings
     */
    public List<MatchingTableMappingRO> getMappings() {
        return mappings;
    }

    /**
     * @param mappings matching tables mappings to set
     */
    public void setMappings(List<MatchingTableMappingRO> mappings) {
        this.mappings = mappings;
    }
}
