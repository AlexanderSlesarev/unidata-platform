/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.ro.cluster;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Sergey Murskiy on 02.07.2021
 */
public class ClusterRO {
    /**
     * Cluster id.
     */
    private String id;
    /**
     * Matching set name.
     */
    private String setName;
    /**
     * Matching set display name/
     */
    private String setDisplayName;
    /**
     * Matching rule name.
     */
    private String ruleName;
    /**
     * Matching rule display name.
     */
    private String ruleDisplayName;
    /**
     * Cluster owner.
     */
    private ClusterRecordRO owner;
    /**
     * Matching date.
     */
    private Date matchingDate;
    /**
     * Cluster records.
     */
    private List<ClusterRecordRO> clusterRecords;
    /**
     * Records count.
     */
    private int recordsCount;
    /**
     * Additional parameters.
     */
    private Map<String, Object> additionalParameters;

    /**
     * Gets cluster id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets cluster id.
     *
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets set name.
     *
     * @return the set name
     */
    public String getSetName() {
        return setName;
    }

    /**
     * Sets set name.
     *
     * @param setName the set name to set
     */
    public void setSetName(String setName) {
        this.setName = setName;
    }

    /**
     * Gets set display name.
     *
     * @return the set display name
     */
    public String getSetDisplayName() {
        return setDisplayName;
    }

    /**
     * Sets set display name.
     *
     * @param setDisplayName the set display name to set
     */
    public void setSetDisplayName(String setDisplayName) {
        this.setDisplayName = setDisplayName;
    }

    /**
     * Gets rule name.
     *
     * @return the rule name
     */
    public String getRuleName() {
        return ruleName;
    }

    /**
     * Sets rule name.
     *
     * @param ruleName the rule name to set
     */
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    /**
     * Gets rule display name.
     *
     * @return the rule display name
     */
    public String getRuleDisplayName() {
        return ruleDisplayName;
    }

    /**
     * Sets rule display name.
     *
     * @param ruleDisplayName the rule display name to set
     */
    public void setRuleDisplayName(String ruleDisplayName) {
        this.ruleDisplayName = ruleDisplayName;
    }

    /**
     * Gets cluster owner.
     *
     * @return owner
     */
    public ClusterRecordRO getOwner() {
        return owner;
    }

    /**
     * Sets cluster owner.
     *
     * @param owner the owner to set
     */
    public void setOwner(ClusterRecordRO owner) {
        this.owner = owner;
    }

    /**
     * Gets matching date.
     *
     * @return the matching date
     */
    public Date getMatchingDate() {
        return matchingDate;
    }

    /**
     * Sets matching date.
     *
     * @param matchingDate the matching date to set
     */
    public void setMatchingDate(Date matchingDate) {
        this.matchingDate = matchingDate;
    }

    /**
     * Gets cluster records.
     *
     * @return cluster records
     */
    public List<ClusterRecordRO> getClusterRecords() {
        return clusterRecords;
    }

    /**
     * Sets cluster records.
     *
     * @param clusterRecords cluster records to set
     */
    public void setClusterRecords(List<ClusterRecordRO> clusterRecords) {
        this.clusterRecords = clusterRecords;
    }

    /**
     * Gets records count.
     *
     * @return records count
     */
    public int getRecordsCount() {
        return recordsCount;
    }

    /**
     * Sets records count.
     *
     * @param recordsCount records count to set
     */
    public void setRecordsCount(int recordsCount) {
        this.recordsCount = recordsCount;
    }

    /**
     * Gets additional parameters.
     *
     * @return additional parameters
     */
    public Map<String, Object> getAdditionalParameters() {
        return additionalParameters;
    }

    /**
     * Sets additional parameters.
     *
     * @param additionalParameters additional parameters to set
     */
    public void setAdditionalParameters(Map<String, Object> additionalParameters) {
        this.additionalParameters = additionalParameters;
    }
}
