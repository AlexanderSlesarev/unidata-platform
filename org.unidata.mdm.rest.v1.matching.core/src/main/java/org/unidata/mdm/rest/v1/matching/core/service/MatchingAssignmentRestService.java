/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.matching.core.service;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.matching.core.context.MatchingModelGetContext;
import org.unidata.mdm.matching.core.context.MatchingModelUpsertContext;
import org.unidata.mdm.matching.core.dto.AssignmentsModelGetResult;
import org.unidata.mdm.matching.core.dto.MatchingModelGetResult;
import org.unidata.mdm.matching.core.type.model.source.assignment.NamespaceAssignmentSource;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.v1.matching.core.converter.NamespaceAssignmentConverter;
import org.unidata.mdm.rest.v1.matching.core.ro.DeleteNamespaceAssignmentResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.GetNamespaceAssignmentResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.GetNamespaceAssignmentsResultRO;
import org.unidata.mdm.rest.v1.matching.core.ro.UpsertNamespaceAssignmentRequestRO;
import org.unidata.mdm.rest.v1.matching.core.ro.assignment.NamespaceAssignmentRO;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Sergey Murskiy on 23.06.2021
 */
@Path(MatchingAssignmentRestService.SERVICE_PATH)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MatchingAssignmentRestService extends AbstractRestService {
    /**
     * Service path.
     */
    public static final String SERVICE_PATH = "assignments";
    /**
     * Tag.
     */
    public static final String SERVICE_TAG = "assignments";

    /**
     * Assignment converter.
     */
    @Autowired
    private NamespaceAssignmentConverter assignmentConverter;
    /**
     * Meta model service.
     */
    @Autowired
    private MetaModelService metaModelService;

    @GET
    @Operation(
            description = "Get all defined namespace assignments.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetNamespaceAssignmentsResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetNamespaceAssignmentsResultRO getAll(@QueryParam("draftId") @DefaultValue("0") Long draftId) {

        MatchingModelGetResult getModelResult = metaModelService.get(MatchingModelGetContext.builder()
                .allAssignments(true)
                .draftId(draftId)
                .build());

        return new GetNamespaceAssignmentsResultRO(Objects.isNull(getModelResult.getAssignments())
                ? Collections.emptyMap()
                : getModelResult.getAssignments().getAssignments().stream()
                    .map(assignmentConverter::to)
                    .collect(Collectors.groupingBy(NamespaceAssignmentRO::getAssignmentKey)));
    }

    @GET
    @Path("{key}")
    @Operation(
            description = "Get a namespace assignment by assignment key.",
            method = HttpMethod.GET,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The assignment key. Required.", in = ParameterIn.PATH, name = "key"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetNamespaceAssignmentsResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetNamespaceAssignmentResultRO getByKey(@PathParam("key") String key,
                                                   @QueryParam("draftId") @DefaultValue("0") Long draftId) {

        MatchingModelGetResult getModelResult = metaModelService.get(MatchingModelGetContext.builder()
                .assignmentByKeys(Collections.singletonList(key))
                .draftId(draftId)
                .build());

        GetNamespaceAssignmentResultRO result = new GetNamespaceAssignmentResultRO();

        AssignmentsModelGetResult amr = getModelResult.getAssignments();
        if (CollectionUtils.isNotEmpty(amr.getAssignments()) && amr.getAssignments().size() == 1) {
            result.setAssignment(assignmentConverter.to(amr.singleValue()));
        }

        return result;
    }

    @POST
    @Operation(
            description = "Create a namespace assignment definition.",
            method = HttpMethod.POST,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            requestBody = @RequestBody(
                    content = @Content(schema = @Schema(implementation = UpsertNamespaceAssignmentRequestRO.class)),
                    description = "Namespace assignment definition."),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetNamespaceAssignmentResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetNamespaceAssignmentResultRO upsert(@QueryParam("draftId") @DefaultValue("0") Long draftId,
                                                 UpsertNamespaceAssignmentRequestRO request) {

        NamespaceAssignmentSource source = assignmentConverter.from(request.getAssignment());

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .assignmentsUpdate(source)
                .draftId(draftId)
                .waitForFinish(true)
                .build());

        return getByKey(source.getAssignmentKey(), draftId);
    }

    @PUT
    @Path("{key}")
    @Operation(
            description = "Update a namespace assignment definition.",
            method = HttpMethod.POST,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The namespace assignment key. Required.", in = ParameterIn.PATH, name = "key"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            requestBody = @RequestBody(
                    content = @Content(schema = @Schema(implementation = UpsertNamespaceAssignmentRequestRO.class)),
                    description = "Namespace assignment definition."),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = GetNamespaceAssignmentResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public GetNamespaceAssignmentResultRO update(@PathParam("key") String assignmentKey,
                                                 @QueryParam("draftId") @DefaultValue("0") Long draftId,
                                                 UpsertNamespaceAssignmentRequestRO request) {

        NamespaceAssignmentSource source = assignmentConverter.from(request.getAssignment());

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .assignmentsUpdate(source)
                .assignmentsDelete(!StringUtils.equals(assignmentKey, source.getAssignmentKey()) ? assignmentKey : null)
                .draftId(draftId)
                .waitForFinish(true)
                .build());

        return getByKey(source.getAssignmentKey(), draftId);
    }

    @DELETE
    @Path("{key}")
    @Operation(
            description = "Delete an existing namespace assignment definition by assignment key.",
            method = HttpMethod.DELETE,
            tags = SERVICE_TAG,
            parameters = {
                    @Parameter(description = "The namespace assignment key. Required.", in = ParameterIn.PATH, name = "key"),
                    @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY, name = "draftId")
            },
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DeleteNamespaceAssignmentResultRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
            }
    )
    public DeleteNamespaceAssignmentResultRO delete(@PathParam("key") String key,
                                                    @QueryParam("draftId") @DefaultValue("0") Long draftId) {

        metaModelService.upsert(MatchingModelUpsertContext.builder()
                .assignmentsDelete(key)
                .draftId(draftId)
                .build());

        return new DeleteNamespaceAssignmentResultRO();
    }
}
