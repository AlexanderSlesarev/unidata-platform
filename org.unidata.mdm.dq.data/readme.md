# DQ Data Module

# backend.properties:
org.unidata.mdm.dq.data.datasource.username=postgres
org.unidata.mdm.dq.data.datasource.password=postgres
org.unidata.mdm.dq.data.datasource.url=jdbc:postgresql://localhost:5435/ud_520?currentSchema=com_unidata_mdm_dq_data&reWriteBatchedInserts=true&ApplicationName=Unidata-DataQuality
org.unidata.mdm.dq.data.datasource.driverClassName=org.postgresql.Driver
org.unidata.mdm.dq.data.datasource.initialSize=10
org.unidata.mdm.dq.data.datasource.maxActive=10
org.unidata.mdm.dq.data.datasource.maxIdle=10
org.unidata.mdm.dq.data.datasource.minIdle=10
org.unidata.mdm.dq.data.datasource.minEvictableIdleTimeMillis=60000
org.unidata.mdm.dq.data.datasource.timeBetweenEvictionRunsMillis=30000
org.unidata.mdm.dq.data.datasource.removeAbandoned=true
org.unidata.mdm.dq.data.datasource.removeAbandonedTimeout=360
org.unidata.mdm.dq.data.datasource.jdbcInterceptors=ResetAbandonedTimer
org.unidata.mdm.dq.data.datasource.logAbandoned=true
org.unidata.mdm.dq.data.datasource.suspectTimeout=60
org.unidata.mdm.dq.data.datasource.testOnBorrow=true
org.unidata.mdm.dq.data.datasource.validationQuery=SELECT 1
org.unidata.mdm.dq.data.datasource.validationInterval=30000
org.unidata.mdm.dq.data.datasource.type=javax.sql.DataSource

# 1.0 Upsert Data Quality with model upsert.
# 1.1 modify MODEL_UPSERT_START pipeline, add segment:
    {
	  "segmentType":"CONNECTOR",
	  "id":"org.unidata.mdm.dq.data[DATA_QUALITY_UPSERT_CONNECTOR]"
	}
# 1.2 add new pipeline DATA_QUALITY_UPSERT_START:

    INSERT INTO org_unidata_mdm_system.pipelines_info 
    (start_id, subject, content) 
    VALUES ('org.unidata.mdm.dq.data[DATA_QUALITY_UPSERT_START]',
     '',
      '{"startId":"org.unidata.mdm.dq.data[DATA_QUALITY_UPSERT_START]",
      "subjectId":"",
      "description":"data quality upsert",
      "segments":[{"segmentType":"START","id":"org.unidata.mdm.dq.data[DATA_QUALITY_UPSERT_START]"}, {"segmentType":"FINISH","id":"org.unidata.mdm.dq.data[DATA_QUALITY_UPSERT_FINISH]"}]}'
      );
# 2.0 Get Data Quality from model.      
# 2.1 modify MODEL_GET_START pipeline, add segment:
    {"segmentType":"CONNECTOR","id":"org.unidata.mdm.dq.data[ENTITY_DATA_QUALITY_GET_CONNECTOR]"}

# 3.0 Publish Data Quality draft model.      
# 3.1 modify MODEL_PUBLISH_START pipeline, add segment:
    {"segmentType":"POINT","id":"org.unidata.mdm.dq.data[PUBLISH_META_DATA_QUALITY_POINT]"}

# 4.0 Upsert record with Data Quality.
# 4.1 modify RECORD_UPSERT_START pipeline, insert dq point after RECORD_UPSERT_VALIDATE:
    {
      "segmentType": "POINT",
      "id": "org.unidata.mdm.dq.data[ORGIN_RECORD_APPLY_DATA_QUALITY]"
    }
# 4.2 modify RECORD_UPSERT_START pipeline, insert dq point after RECORD_UPSERT_INDEXING:
    {
      "segmentType": "POINT",
      "id": "org.unidata.mdm.dq.data[ETALON_RECORD_APPLY_DATA_QUALITY]"
    }