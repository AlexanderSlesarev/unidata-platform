/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.data.service.segments.records.upsert;

import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.calculables.ModificationBox;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.DataShift;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.type.calculables.impl.DataRecordHolder;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.data.OriginRecordInfoSection;
import org.unidata.mdm.data.type.data.UpsertAction;
import org.unidata.mdm.data.type.data.impl.OriginRecordImpl;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.dq.core.context.DataQualityContext;
import org.unidata.mdm.dq.core.dto.DataQualityResult;
import org.unidata.mdm.dq.core.dto.DataQualityResult.RuleExecutionResult;
import org.unidata.mdm.dq.core.dto.DataQualityResult.SetExecutionResult;
import org.unidata.mdm.dq.core.type.model.instance.MappingSetElement;
import org.unidata.mdm.dq.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.dq.core.type.model.instance.QualityRuleElement;
import org.unidata.mdm.dq.core.type.rule.SeverityIndicator;
import org.unidata.mdm.dq.data.exception.DataQualityDataExceptionIds;
import org.unidata.mdm.dq.data.module.DataQualityDataModule;
import org.unidata.mdm.system.exception.PlatformValidationException;
import org.unidata.mdm.system.exception.ValidationResult;

/**
 * @author Mikhail Mikhailov on Nov 7, 2021
 * Implements 'old' origns check behavior.
 */
@Component(RecordUpsertQualityOriginExecutor.SEGMENT_ID)
public class RecordUpsertQualityOriginExecutor extends RecordUpsertQualityPhaseExecutor {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataQualityDataModule.MODULE_ID + "[RECORD_UPSERT_QUALITY_ORIGIN]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataQualityDataModule.MODULE_ID + ".record.upsert.quality.origin.description";
    /**
     * This point's phase name.
     */
    private static final String PHASE_NAME = "ORIGIN";
    /**
     * MS validation errors tcode.
     */
    private static final String MAPPING_SET_VALIDATION_ERRORS = DataQualityDataModule.MODULE_ID + ".mapping.set.validation.errors";
    /**
     * Rule validation errors tcode.
     */
    private static final String RULE_VALIDATION_ERRORS = DataQualityDataModule.MODULE_ID + ".rule.validation.errors";
    /**
     * Constructor.
     */
    public RecordUpsertQualityOriginExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(UpsertRequestContext ctx) {

        if (ctx.upsertAction() == UpsertAction.NO_ACTION) {
            return;
        }

        RecordKeys keys = ctx.currentTimeline().getKeys();
        ModificationBox<OriginRecord> box = ctx.modificationBox();
        if (Objects.isNull(box) || box.count(keys.getOriginKey().toBoxKey()) == 0) {
            return;
        }

        NamespaceAssignmentElement nae = selectByNamespace(keys.getEntityName());
        if (Objects.isNull(nae) || !nae.isAssigned(keys.getEntityName(), PHASE_NAME)) {
            return;
        }

        DataRecord input = box.peek(keys.getOriginKey().toBoxKey()).getValue();
        DataQualityResult result = dataQualityService.apply(DataQualityContext.builder()
                .payload(ctx)
                .mappings(nae.getAssigned(keys.getEntityName(), PHASE_NAME))
                .sourceSystem(keys.getOriginKey().getSourceSystem())
                .input(nae.getNameSpace(), keys.getEntityName(), keys.getOriginKey().getId(), input)
                .build());

        process(nae, result, keys, box);
    }

    private void process(NamespaceAssignmentElement nae, DataQualityResult result, RecordKeys keys, ModificationBox<OriginRecord> box) {

        // 1. If result has validation errors - throw and stop pipeline
        if (!result.isValid()) {
            processValidations(result);
        }

        // 2. Apply enrichments, if generated
        if (result.isEnriched()) {
            processEnrichments(nae, result, keys, box);
        }
    }

    private void processEnrichments(NamespaceAssignmentElement nae, DataQualityResult result, RecordKeys keys, ModificationBox<OriginRecord> box) {

        DataRecord enrichment = result.getOutput()
                .getAsIdentity(nae.getNameSpace(), keys.getEntityName())
                .get(keys.getOriginKey().getId());

        if (Objects.isNull(enrichment)) {
            return;
        }

        OriginRecord origin = box.peek(keys.getOriginKey().toBoxKey()).getValue();
        box.push(new DataRecordHolder(new OriginRecordImpl()
                .withDataRecord(enrichment)
                .withInfoSection(OriginRecordInfoSection.of(origin.getInfoSection())
                        .withShift(DataShift.REVISED))));
    }

    private void processValidations(DataQualityResult result) {

        List<ValidationResult> top = new ArrayList<>(8);
        for (Entry<MappingSetElement, SetExecutionResult> sr : result.getResults().entrySet()) {

            List<ValidationResult> rules = new ArrayList<>();
            for(Entry<QualityRuleElement, List<RuleExecutionResult>> rr : sr.getValue().getResult().entrySet()) {

                for (RuleExecutionResult rer : rr.getValue()) {

                    if (!rer.isValid()) {
                        rules.addAll(rer.getErrors().stream()
                            .filter(dqe -> dqe.getSeverity() == SeverityIndicator.RED)
                            .map(dqe -> new ValidationResult("Rule {} - function {}, category {}, score {}, message {}.",
                                    RULE_VALIDATION_ERRORS,
                                    rer.getRule().getDisplayName(),
                                    dqe.getFunctionName(),
                                    dqe.getCategory(),
                                    dqe.getScore(),
                                    dqe.getMessage()))
                            .collect(Collectors.toList()));
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(rules)) {
                top.add(new ValidationResult("Rule mapping set {} discovered critical arrors.", rules,
                        MAPPING_SET_VALIDATION_ERRORS,
                        sr.getKey().getDisplayName()));
            }
        }

        if (CollectionUtils.isNotEmpty(top)) {
            throw new PlatformValidationException("Data quality ORIGIN phase - upsert rejected",
                    DataQualityDataExceptionIds.EX_DQ_DATA_ORIGINS_PHASE_FAILED, top);
        }
    }
}
