/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.data.service.impl.function.data;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.model.LookupElement;
import org.unidata.mdm.dq.core.context.CleanseFunctionContext;
import org.unidata.mdm.dq.core.dto.CleanseFunctionResult;
import org.unidata.mdm.dq.core.exception.CleanseFunctionExecutionException;
import org.unidata.mdm.dq.core.service.impl.function.system.AbstractSystemCleanseFunction;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionConfiguration;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionExecutionScope;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionInputParam;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionOutputParam;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortFilteringMode;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortInputType;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionPortValueType;
import org.unidata.mdm.dq.core.type.constant.CleanseConstants;
import org.unidata.mdm.dq.core.type.io.DataQualitySpot;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.search.EntityIndexType;
import org.unidata.mdm.meta.type.search.RecordHeaderField;
import org.unidata.mdm.search.context.SearchRequestContext;
import org.unidata.mdm.search.dto.SearchResultDTO;
import org.unidata.mdm.search.dto.SearchResultHitDTO;
import org.unidata.mdm.search.service.SearchService;
import org.unidata.mdm.search.type.form.FieldsGroup;
import org.unidata.mdm.search.type.form.FormField;
import org.unidata.mdm.search.type.query.SearchQuery;
import org.unidata.mdm.system.util.TextUtils;

public class CheckRecordLinks extends AbstractSystemCleanseFunction {
    /**
     * Display name code.
     */
    private static final String FUNCTION_DISPLAY_NAME = "app.dq.functions.data.check.link.display.name";
    /**
     * Description code.
     */
    private static final String FUNCTION_DESCRIPTION = "app.dq.functions.data.check.link.decsription";
    /**
     * IP1 name code.
     */
    private static final String INPUT_PORT_1_NAME = "app.dq.functions.data.check.link.input.port1.name";
    /**
     * IP1 description code.
     */
    private static final String INPUT_PORT_1_DESCRIPTION = "app.dq.functions.data.check.link.input.port1.decsription";
    /**
     * IP2 name code.
     */
    private static final String INPUT_PORT_2_NAME = "app.dq.functions.data.check.link.input.port2.name";
    /**
     * IP2 description code.
     */
    private static final String INPUT_PORT_2_DESCRIPTION = "app.dq.functions.data.check.link.input.port2.decsription";
    /**
     * OP1 name code.
     */
    private static final String OUTPUT_PORT_1_NAME = "app.dq.functions.data.check.link.output.port1.name";
    /**
     * OP1 description code.
     */
    private static final String OUTPUT_PORT_1_DESCRIPTION = "app.dq.functions.data.check.link.output.port1.decsription";
    /**
     * OP2 name code.
     */
    private static final String OUTPUT_PORT_2_NAME = "app.dq.functions.data.check.link.output.port2.name";
    /**
     * OP2 description code.
     */
    private static final String OUTPUT_PORT_2_DESCRIPTION = "app.dq.functions.data.check.link.output.port2.decsription";
    /**
     * This function configuration.
     */
    private static final CleanseFunctionConfiguration CONFIGURATION
        = CleanseFunctionConfiguration.configuration()
            .supports(CleanseFunctionExecutionScope.GLOBAL, CleanseFunctionExecutionScope.LOCAL)
            .input(CleanseFunctionConfiguration.port()
                    .name(CleanseConstants.INPUT_PORT_1)
                    .displayName(() -> TextUtils.getText(INPUT_PORT_1_NAME))
                    .description(() -> TextUtils.getText(INPUT_PORT_1_DESCRIPTION))
                    .filteringMode(CleanseFunctionPortFilteringMode.MODE_ALL)
                    .inputTypes(CleanseFunctionPortInputType.SIMPLE, CleanseFunctionPortInputType.ARRAY, CleanseFunctionPortInputType.CODE)
                    .valueTypes(CleanseFunctionPortValueType.STRING, CleanseFunctionPortValueType.INTEGER)
                    .required(false)
                    .build())
            .input(CleanseFunctionConfiguration.port()
                    .name(CleanseConstants.INPUT_PORT_2)
                    .displayName(() -> TextUtils.getText(INPUT_PORT_2_NAME))
                    .description(() -> TextUtils.getText(INPUT_PORT_2_DESCRIPTION))
                    .filteringMode(CleanseFunctionPortFilteringMode.MODE_ALL)
                    .inputTypes(CleanseFunctionPortInputType.SIMPLE)
                    .valueTypes(CleanseFunctionPortValueType.STRING)
                    .required(true)
                    .build())
            .output(CleanseFunctionConfiguration.port()
                    .name(CleanseConstants.OUTPUT_PORT_1)
                    .displayName(() -> TextUtils.getText(OUTPUT_PORT_1_NAME))
                    .description(() -> TextUtils.getText(OUTPUT_PORT_1_DESCRIPTION))
                    .filteringMode(CleanseFunctionPortFilteringMode.MODE_ALL)
                    .inputTypes(CleanseFunctionPortInputType.SIMPLE)
                    .valueTypes(CleanseFunctionPortValueType.BOOLEAN)
                    .required(true)
                    .build())
            .output(CleanseFunctionConfiguration.port()
                    .name(CleanseConstants.OUTPUT_PORT_2)
                    .displayName(() -> TextUtils.getText(OUTPUT_PORT_2_NAME))
                    .description(() -> TextUtils.getText(OUTPUT_PORT_2_DESCRIPTION))
                    .filteringMode(CleanseFunctionPortFilteringMode.MODE_ALL)
                    .inputTypes(CleanseFunctionPortInputType.SIMPLE)
                    .valueTypes(CleanseFunctionPortValueType.STRING)
                    .required(true)
                    .build())
            .build();
    /**
     * Search service.
     */
    @Autowired
    private SearchService searchService;
    /**
     * Model Service
     */
    @Autowired
    private MetaModelService modelService;
    /**
     * Instantiates a new cleanse function abstract.
     *
     * @param clazz cleanse function class.
     */
    public CheckRecordLinks() {
        super("CheckRecordLinks", () -> TextUtils.getText(FUNCTION_DISPLAY_NAME), () -> TextUtils.getText(FUNCTION_DESCRIPTION));
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public CleanseFunctionConfiguration configure() {
        return CONFIGURATION;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public CleanseFunctionResult execute(CleanseFunctionContext ctx) {

        CleanseFunctionResult result = new CleanseFunctionResult();

        CleanseFunctionInputParam param1 = ctx.getInputParam(CleanseConstants.INPUT_PORT_1);
        CleanseFunctionInputParam param2 = ctx.getInputParam(CleanseConstants.INPUT_PORT_2);

        // 1. Invalid input - lookup entity name is null
        // 2. Fail on wrong configuration. Lookup entity not found by name.
        LookupElement lookupElement = null;
        if (Objects.isNull(param2) || param2.isEmpty() || StringUtils.isBlank(param2.toSingletonValue())) {
            throw new CleanseFunctionExecutionException(ctx.getFunctionName(),
                    TextUtils.getText("app.cleanse.validation.invalid.parameters"));
        } else {

            lookupElement = modelService.instance(Descriptors.DATA).getLookup(param2.toSingletonValue());
            if (lookupElement == null) {
                throw new CleanseFunctionExecutionException(ctx.getFunctionName(),
                        TextUtils.getText("app.cleanse.validation.lookupEntity.notExist", param2.toSingletonValue()));
            }
        }

        // 3. Extract values, exit on empty
        Map<Object, List<Attribute>> check = extractAndMapAttributes(param1);
        if (check.isEmpty()) {
            result.putOutputParam(CleanseFunctionOutputParam.of(CleanseConstants.OUTPUT_PORT_1, Boolean.TRUE));
            result.putOutputParam(CleanseFunctionOutputParam.of(CleanseConstants.OUTPUT_PORT_2, StringUtils.EMPTY));
            return result;
        }

        // 4. Collect current values and compare them with input. Report missing.
        Set<Object> current = collectCurrentState(lookupElement, check);
        Pair<Set<Object>, Set<Attribute>> failed = collectMissingValues(check, current);

        // 5. Stop on missing values in links
        if (CollectionUtils.isNotEmpty(failed.getKey())) {
            result.putOutputParam(CleanseFunctionOutputParam.of(CleanseConstants.OUTPUT_PORT_1, Boolean.FALSE));
            result.putOutputParam(CleanseFunctionOutputParam.of(CleanseConstants.OUTPUT_PORT_2, TextUtils.getText("app.cleanse.validation.lookupEntityRecord.notExist", failed.getKey().toString())));
            result.addSpots(createPathCollection(param1, failed.getValue()));
            return result;
        }

        result.putOutputParam(CleanseFunctionOutputParam.of(CleanseConstants.OUTPUT_PORT_1, Boolean.TRUE));
        result.putOutputParam(CleanseFunctionOutputParam.of(CleanseConstants.OUTPUT_PORT_2, StringUtils.EMPTY));
        return result;
    }
    /**
     * Creates search request and executes it, returning preprocessed response.
     * @param lookupEntityDef lookup entity definition
     * @param values the values map
     * @return search result as multimap
     */
    private Set<Object> collectCurrentState(LookupElement lookupEntityDef, Map<Object, List<Attribute>> values) {

        // 1. Request
        SearchRequestContext ctx = SearchRequestContext.builder(EntityIndexType.RECORD, lookupEntityDef.getName())
                .query(SearchQuery.formQuery(FieldsGroup.and(
                        FormField.exact(RecordHeaderField.FIELD_DELETED, Boolean.FALSE),
                        FormField.exact(RecordHeaderField.FIELD_INACTIVE, Boolean.FALSE),
                        FormField.exact(lookupEntityDef.getCodeAttribute().getIndexed(), values.keySet()))))
                .returnFields(lookupEntityDef.getCodeAttribute().getName())
                .page(0)
                .count(Integer.MAX_VALUE)
                .totalCount(true)
                .build();

        // 2. Execute
        SearchResultDTO searchResult = searchService.search(ctx);

        // 3. Post-process
        if (CollectionUtils.isEmpty(searchResult.getHits())) {
            return Collections.emptySet();
        }

        Set<Object> current = new HashSet<>();
        for (SearchResultHitDTO hit : searchResult.getHits()) {

            List<Object> codeValues = hit.getFieldValues(lookupEntityDef.getCodeAttribute().getName());
            if (CollectionUtils.isNotEmpty(codeValues)) {
                codeValues.forEach(codeValue -> current.add(codeValue instanceof Number
                            ? ((Number) codeValue).longValue()
                            : codeValue.toString()));
            }
        }

        return current;
    }
    /**
     * Gets the not presented values.
     *
     * @param check the values to check
     * @param current the current state
     * @return the not present values
     */
    private Pair<Set<Object>, Set<Attribute>> collectMissingValues(
            Map<Object, List<Attribute>> check, Set<Object> current) {

        Set<Object> missingValues = new HashSet<>();
        Set<Attribute> failedAttributes = new HashSet<>();
        for (Entry<Object, List<Attribute>> checkEntry : check.entrySet()) {
            if (!current.contains(checkEntry.getKey())) {
                missingValues.add(checkEntry.getKey());
                failedAttributes.addAll(checkEntry.getValue());
            }
        }

        return Pair.of(missingValues, failedAttributes);
    }
    /**
     * Collects paths of attributes.
     * @param input the input param
     * @param attributes the attributes collection
     * @return paths collection
     */
    private Collection<DataQualitySpot> createPathCollection(CleanseFunctionInputParam input, Collection<Attribute> attributes) {
        return CollectionUtils.isEmpty(attributes)
                ? Collections.emptyList()
                : attributes.stream()
                    .map(attr -> new DataQualitySpot(input, attr.toLocalPath(), attr))
                    .collect(Collectors.toList());
    }
}
