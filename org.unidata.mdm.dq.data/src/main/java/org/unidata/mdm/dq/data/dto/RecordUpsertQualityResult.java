/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.data.dto;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.dq.core.dto.DataQualityResult;
import org.unidata.mdm.system.dto.ExecutionResult;
import org.unidata.mdm.system.type.pipeline.PipelineOutput;
import org.unidata.mdm.system.type.pipeline.fragment.FragmentId;
import org.unidata.mdm.system.type.pipeline.fragment.OutputFragment;

/**
 * @author Alexey Tsarapkin
 * Collects RECORDS DQ results, keyed by time intervals, that produced those results.
 * On GET operation only errors and spots are returned (spots do not contain attribute values).
 */
public class RecordUpsertQualityResult implements OutputFragment<RecordUpsertQualityResult>, PipelineOutput, ExecutionResult {
    /**
     * Fragment id.
     */
    public static final FragmentId<RecordUpsertQualityResult> ID = new FragmentId<>("UPSERT_DATA_QUALITY_RECORDS_RESULT");
    /**
     * Execution result by rules.
     */
    private Map<TimeInterval<OriginRecord>, DataQualityResult> payload;
    /**
     * Constructor.
     */
    public RecordUpsertQualityResult() {
        super();
    }
    /**
     * Constructor.
     * @param payload the results
     */
    public RecordUpsertQualityResult(Map<TimeInterval<OriginRecord>, DataQualityResult> payload) {
        this();
        this.payload = payload;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FragmentId<RecordUpsertQualityResult> fragmentId() {
        return ID;
    }
    /**
     * Returns true, if has some payload.
     * @return true, if has some payload
     */
    public boolean hasPayload() {
        return MapUtils.isEmpty(payload);
    }
    /**
     * Gets rules excution state.
     * @return rules excution state
     */
    public Map<TimeInterval<OriginRecord>, DataQualityResult> getPayload() {
        return Objects.isNull(payload) ? Collections.emptyMap() : payload;
    }
    /**
     * Sets DQ rules execution results.
     * @param payload  the results
     */
    public void setPayload(Map<TimeInterval<OriginRecord>, DataQualityResult> payload) {
        this.payload = payload;
    }
}
