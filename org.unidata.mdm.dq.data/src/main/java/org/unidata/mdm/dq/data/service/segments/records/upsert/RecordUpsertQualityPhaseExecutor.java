/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.data.service.segments.records.upsert;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.data.configuration.DataNamespace;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.dq.core.configuration.DataQualityDescriptors;
import org.unidata.mdm.dq.core.service.DataQualityService;
import org.unidata.mdm.dq.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.system.type.namespace.NameSpace;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Mikhail Mikhailov on Nov 7, 2021
 * Base for 'old' origns/etalons check behavior.
 */
public abstract class RecordUpsertQualityPhaseExecutor extends Point<UpsertRequestContext> {
    /**
     * The MMS instance.
     */
    @Autowired
    protected MetaModelService metaModelService;
    /**
     * DQS.
     */
    @Autowired
    protected DataQualityService dataQualityService;
    /**
     * Constructor.
     * @param id
     * @param description
     */
    protected RecordUpsertQualityPhaseExecutor(String id, String description) {
        super(id, description);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(UpsertRequestContext ctx) {
        // To be overridden by subclasses
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return UpsertRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
    /*
     * Gets the namespace assignment by entity name, if it is defined.
     */
    protected NamespaceAssignmentElement selectByNamespace(String name) {

        EntityElement el = metaModelService.instance(Descriptors.DATA)
                .getElement(name);

        NameSpace target = null;
        if (el.isLookup()) {
            target = DataNamespace.LOOKUP;
        } else if (el.isRegister()) {
            target = DataNamespace.REGISTER;
        }

        return metaModelService.instance(DataQualityDescriptors.DQ)
            .getAssignment(target);
    }
}
