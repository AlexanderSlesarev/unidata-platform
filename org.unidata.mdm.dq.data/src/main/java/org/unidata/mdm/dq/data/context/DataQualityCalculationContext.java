/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.data.context;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.unidata.mdm.core.context.ValidityRangeContext;
import org.unidata.mdm.core.type.calculables.Calculable;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.data.context.BatchAwareContext;
import org.unidata.mdm.data.context.OperationTypeContext;
import org.unidata.mdm.data.context.ReadWriteDataContext;
import org.unidata.mdm.data.context.ReadWriteTimelineContext;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.dq.core.dto.DataQualityResult;
import org.unidata.mdm.system.context.StorageId;

/**
 * @author Mikhail Mikhailov on Apr 9, 2021
 * Basic data quality context.
 */
public interface DataQualityCalculationContext<T extends Calculable>
    extends
        ReadWriteDataContext<T>,
        ReadWriteTimelineContext<T>,
        BatchAwareContext,
        OperationTypeContext,
        ValidityRangeContext {
    /**
     * Collected changes.
     * Map<TimeInterval<OriginRecord>, DataQualityResult> collected
     */
    StorageId SID_QUALITY_CALCULATION_RESULT = new StorageId("QUALITY_CALCULATION_RESULT");
    /**
     * Get collected changes.
     * @return collected changes
     */
    default Map<TimeInterval<OriginRecord>, DataQualityResult> qualityResult() {
        Map<TimeInterval<OriginRecord>, DataQualityResult> result = getFromStorage(SID_QUALITY_CALCULATION_RESULT);
        return Objects.isNull(result) ? Collections.emptyMap() : result;
    }
    /**
     * Put collected changes.
     * @param qr collected changes
     */
    default void qualityResult(Map<TimeInterval<OriginRecord>, DataQualityResult> rq) {
        putToStorage(SID_QUALITY_CALCULATION_RESULT, rq);
    }
}
