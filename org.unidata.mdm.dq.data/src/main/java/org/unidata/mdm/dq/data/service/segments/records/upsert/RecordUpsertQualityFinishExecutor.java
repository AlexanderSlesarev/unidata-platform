/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.dq.data.service.segments.records.upsert;

import java.util.Map;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.dq.core.dto.DataQualityResult;
import org.unidata.mdm.dq.data.context.RecordQualityContext;
import org.unidata.mdm.dq.data.dto.RecordUpsertQualityResult;
import org.unidata.mdm.dq.data.module.DataQualityDataModule;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Mikhail Mikhailov on Nov 8, 2019
 */
@Component(RecordUpsertQualityFinishExecutor.SEGMENT_ID)
public class RecordUpsertQualityFinishExecutor extends Finish<RecordQualityContext, RecordUpsertQualityResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataQualityDataModule.MODULE_ID + "[RECORD_UPSERT_QUALITY_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataQualityDataModule.MODULE_ID + ".record.upsert.quality.finish.description";
    /**
     * Constructor.
     * @param id
     * @param description
     * @param outputTypeClass
     */
    public RecordUpsertQualityFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, RecordUpsertQualityResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public RecordUpsertQualityResult finish(RecordQualityContext ctx) {
        Map<TimeInterval<OriginRecord>, DataQualityResult> collected = ctx.qualityResult();
        return new RecordUpsertQualityResult(collected);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RecordQualityContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
