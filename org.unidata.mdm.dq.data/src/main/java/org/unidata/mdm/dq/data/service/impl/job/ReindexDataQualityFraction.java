/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.data.service.impl.job;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.service.job.step.ItemWriteListenerAdapter;
import org.unidata.mdm.core.type.annotation.JobRef;
import org.unidata.mdm.core.type.job.JobFraction;
import org.unidata.mdm.data.configuration.DataNamespace;
import org.unidata.mdm.dq.core.configuration.DataQualityDescriptors;
import org.unidata.mdm.dq.core.type.model.instance.DataQualityInstance;
import org.unidata.mdm.dq.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.dq.data.module.DataQualityDataModule;
import org.unidata.mdm.dq.data.service.impl.DataQualityDataMappingComponent;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.instance.DataModelInstance;
import org.unidata.mdm.system.service.TextService;

/**
 * @author Mikhail Mikhailov on Jul 13, 2021
 */
@JobRef("reindexDataJob")
@Component("reindexQualityDataFraction")
public class ReindexDataQualityFraction extends JobFraction {
    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReindexDataQualityFraction.class);
    /**
     * Relations mapping listener.
     */
    private final ItemWriteListener<String> qualityMappingListener = new ItemWriteListenerAdapter<String>() {
        /**
         * {@inheritDoc}
         */
        @Override
        public void afterWrite(List<? extends String> items) {

            // Update mappings
            DataModelInstance di = metaModelService.instance(Descriptors.DATA);
            DataQualityInstance qi = metaModelService.instance(DataQualityDescriptors.DQ);
            if (Objects.isNull(di) || Objects.isNull(qi)) {
                return;
            }

            for (String entityName : items) {

                LOGGER.info("Processing quality mapping for {}.", entityName);

                NamespaceAssignmentElement assignment = null;
                if (di.isRegister(entityName)) {
                    assignment = qi.getAssignment(DataNamespace.REGISTER);
                } else if (di.isLookup(entityName)) {
                    assignment = qi.getAssignment(DataNamespace.LOOKUP);
                }

                if (Objects.nonNull(assignment) && assignment.isAssigned(entityName)) {
                    dataQualityMappingComponent.appendMapping(entityName);
                }
            }
        }
    };
    /**
     * IWL.
     */
    private Map<String, ItemWriteListener<?>> itemWriteListeners = Map.of("reindexDataJobMappingStep", qualityMappingListener);
    /**
     * This fraction ID.
     */
    private static final String ID = "reindex-quality-data-fraction";
    /**
     * This fraction display name.
     */
    private static final String DISPLAY_NAME = DataQualityDataModule.MODULE_ID + ".reindex.quality.data.name";
    /**
     * This fraction description.
     */
    private static final String DESCRIPTION = DataQualityDataModule.MODULE_ID + ".reindex.quality.data.description";
    /**
     * The MMS.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Model mapping component.
     */
    @Autowired
    private DataQualityDataMappingComponent dataQualityMappingComponent;
    /**
     * The TS.
     */
    @Autowired
    private TextService textService;
    /**
     * Constructor.
     * @param id
     * @param parameters
     */
    public ReindexDataQualityFraction() {
        super(ID, Collections.emptyList());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName() {
        return textService.getText(DISPLAY_NAME);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return textService.getText(DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getOrder() {
        return 20;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ItemWriteListener<?> itemWriteListener(String stepName) {
        return itemWriteListeners.get(stepName);
    }
}
