/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.data.service.segments.records.upsert;

import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.service.impl.RecordComposerComponent;
import org.unidata.mdm.data.type.apply.RecordUpsertChangeSet;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.data.UpsertAction;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.dq.core.context.DataQualityContext;
import org.unidata.mdm.dq.core.dto.DataQualityResult;
import org.unidata.mdm.dq.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.dq.data.module.DataQualityDataModule;
import org.unidata.mdm.search.configuration.SearchConfigurationConstants;
import org.unidata.mdm.search.type.id.AbstractManagedIndexId;
import org.unidata.mdm.system.service.IdentityService;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;

/**
 * @author Mikhail Mikhailov on Nov 7, 2021
 * Implements 'old' etalons check behavior.
 */
@Component(RecordUpsertQualityEtalonExecutor.SEGMENT_ID)
public class RecordUpsertQualityEtalonExecutor extends RecordUpsertQualityPhaseExecutor implements RecordQualityProcessingSupport<UpsertRequestContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataQualityDataModule.MODULE_ID + "[RECORD_UPSERT_QUALITY_ETALON]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataQualityDataModule.MODULE_ID + ".record.upsert.quality.etalon.description";
    /**
     * This point's phase name.
     */
    private static final String PHASE_NAME = "ETALON";
    /**
     * PC.
     */
    @Autowired
    private IdentityService identityService;
    /**
     * RCC.
     */
    @Autowired
    private RecordComposerComponent recordComposerComponent;
    /**
     * Delay for async audit operations.
     */
    @ConfigurationRef(SearchConfigurationConstants.PROPERTY_REFRESH_IMMEDIATE)
    private ConfigurationValue<Boolean> refreshImmediate;
    /**
     * Constructor.
     */
    public RecordUpsertQualityEtalonExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(UpsertRequestContext ctx) {

        if (ctx.upsertAction() == UpsertAction.NO_ACTION) {
            return;
        }

        Timeline<OriginRecord> next = ctx.nextTimeline();
        if (Objects.isNull(next) || next.isEmpty()) {
            return;
        }

        RecordKeys keys = next.getKeys();
        NamespaceAssignmentElement nae = selectByNamespace(keys.getEntityName());
        if (Objects.isNull(nae) || !nae.isAssigned(keys.getEntityName(), PHASE_NAME)) {
            return;
        }

        RecordUpsertChangeSet cs = ctx.changeSet();
        List<TimeInterval<OriginRecord>> selection = next.selectBy(ctx.getValidFrom(), ctx.getValidTo());
        for (TimeInterval<OriginRecord> ti : selection) {

            EtalonRecord er = ti.getCalculationResult();
            if (Objects.nonNull(er)) {

                // We use index period id here for record identification purposes,
                // but this can be anything else.
                String periodIdAsString = AbstractManagedIndexId.periodIdValToString(ti.getPeriodId());

                DataQualityResult r = dataQualityService.apply(DataQualityContext.builder()
                        .payload(ctx)
                        .mappings(nae.getAssigned(keys.getEntityName(), PHASE_NAME))
                        .sourceSystem(keys.getOriginKey().getSourceSystem())
                        .input(nae.getNameSpace(), er.getInfoSection().getEntityName(), periodIdAsString, er)
                        .build());

                if (r.isValid() && !r.isEnriched()) {
                    continue;
                }

                processIndex(ctx, r, keys, periodIdAsString, cs);
                processData(ctx, r, keys, periodIdAsString, ti, nae);
            }
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MetaModelService metaModelService() {
        return metaModelService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public IdentityService identityService() {
        return identityService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public RecordComposerComponent recordComposerComponent() {
        return recordComposerComponent;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean refreshImmediate() {
        return refreshImmediate.getValue();
    }
}
