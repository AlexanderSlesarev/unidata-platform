/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.data.module;


import java.util.Collection;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.dq.data.configuration.DataQualityDataConfiguration;
import org.unidata.mdm.system.type.module.AbstractModule;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.pipeline.Segment;

/**
 * @author Alexey Tsarapkin
 */
public class DataQualityDataModule extends AbstractModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataQualityDataModule.class);

    public static final String MODULE_ID = "org.unidata.mdm.dq.data";

    private static final Set<Dependency> DEPENDENCIES = Set.of(
            new Dependency("org.unidata.mdm.data", "6.0"),
            new Dependency("org.unidata.mdm.dq.core", "6.0"));

    @Autowired
    private DataQualityDataConfiguration configuration;

    public DataQualityDataModule() {
        super();
    }

    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }



    @Override
    public String getId() {
        return MODULE_ID;
    }

    @Override
    public String getVersion() {
        return "6.0";
    }

    @Override
    public String getName() {
        return "DQ Data";
    }

    @Override
    public String getDescription() {
        return "DQ Data module";
    }

    @Override
    public void install() {
        LOGGER.info("Install");
    }

    @Override
    public void uninstall() {
        LOGGER.info("Uninstall");
    }

    @Override
    public void start() {
        LOGGER.info("Start");
        addSegments(configuration.getBeansOfType(Segment.class).values());
    }

    @Override
    public void stop() {
        LOGGER.info("Stop");
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getResourceBundleBasenames() {
        return new String[] { "quality_data" };
    }
}
