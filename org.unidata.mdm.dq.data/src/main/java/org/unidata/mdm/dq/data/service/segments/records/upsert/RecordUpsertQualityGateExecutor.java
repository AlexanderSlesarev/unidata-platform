/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.data.service.segments.records.upsert;

import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.service.impl.RecordComposerComponent;
import org.unidata.mdm.data.type.apply.RecordUpsertChangeSet;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.dq.core.dto.DataQualityResult;
import org.unidata.mdm.dq.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.dq.data.context.RecordQualityContext;
import org.unidata.mdm.dq.data.module.DataQualityDataModule;
import org.unidata.mdm.search.configuration.SearchConfigurationConstants;
import org.unidata.mdm.search.type.id.AbstractManagedIndexId;
import org.unidata.mdm.system.service.IdentityService;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Mikhail Mikhailov on Apr 9, 2021<br>
 * DQ processing gate.<br>
 * This class is a working example for decision point.<br>
 * In such a point you can decide what to do with the result (examine and throw or save).<br>
 */
@Component(RecordUpsertQualityGateExecutor.SEGMENT_ID)
public class RecordUpsertQualityGateExecutor extends Point<RecordQualityContext> implements RecordQualityProcessingSupport<RecordQualityContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataQualityDataModule.MODULE_ID + "[RECORD_UPSERT_QUALITY_GATE]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataQualityDataModule.MODULE_ID + ".record.upsert.quality.gate.description";
    /**
     * PC.
     */
    @Autowired
    private IdentityService identityService;
    /**
     * RCC.
     */
    @Autowired
    private RecordComposerComponent recordComposerComponent;
    /**
     * The MMS instance.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Delay for async audit operations.
     */
    @ConfigurationRef(SearchConfigurationConstants.PROPERTY_REFRESH_IMMEDIATE)
    private ConfigurationValue<Boolean> refreshImmediate;
    /**
     * Constructor.
     */
    public RecordUpsertQualityGateExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RecordQualityContext ctx) {

        Map<TimeInterval<OriginRecord>, DataQualityResult> collected = ctx.qualityResult();
        if (MapUtils.isNotEmpty(collected)) {

            Timeline<OriginRecord> next = ctx.nextTimeline();
            RecordKeys keys = next.getKeys();
            RecordUpsertChangeSet cs = ctx.changeSet();
            NamespaceAssignmentElement nae = ctx.getAssignment();

            for (Entry<TimeInterval<OriginRecord>, DataQualityResult> re : collected.entrySet()) {

                DataQualityResult r = re.getValue();
                if (r.isValid() && !r.isEnriched()) {
                    continue;
                }

                // This is expected to be live interval from the 'next' timeline,
                // so all the operations are performed on it directly.
                TimeInterval<OriginRecord> ti = re.getKey();

                String periodIdAsString = AbstractManagedIndexId.periodIdValToString(ti.getPeriodId());

                processIndex(ctx, r, keys, periodIdAsString, cs);
                processData(ctx, r, keys, periodIdAsString, ti, nae);
            }
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RecordQualityContext.class.isAssignableFrom(start.getInputTypeClass());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MetaModelService metaModelService() {
        return metaModelService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public IdentityService identityService() {
        return identityService;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public RecordComposerComponent recordComposerComponent() {
        return recordComposerComponent;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean refreshImmediate() {
        return refreshImmediate.getValue();
    }
}
