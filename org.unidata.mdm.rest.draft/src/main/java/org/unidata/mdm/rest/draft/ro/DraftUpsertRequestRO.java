package org.unidata.mdm.rest.draft.ro;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DraftUpsertRequestRO extends AbstractDraftRequestRO {

    private String description;
    private Map<String, String> parameters;

    public String getDescription() {
        return description;
    }

    public void setDescription(String displayName) {
        this.description = displayName;
    }

    /**
     * @return the parameters
     */
    public Map<String, String> getParameters() {
        return Objects.isNull(parameters) ? Collections.emptyMap() : parameters;
    }

    /**
     * @param parameters the parameters to set
     */
    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
}
