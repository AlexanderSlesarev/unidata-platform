package org.unidata.mdm.search.type;

import java.util.Set;

import javax.annotation.Nonnull;

/**
 * The top index type, defining join field
 * @author Mikhail Mikhailov on Mar 27, 2020
 */
public interface HierarchicalTopIndexType extends HierarchicalIndexType {
    /**
     * Gets the field, used to join parent <-> child queries.
     * @return the join field
     */
    @Nonnull
    IndexField getJoinField();
    /**
     * Adds a child type to the hierarchie.
     * @param type the type to add
     */
    void addChild(HierarchicalIndexType type);
    /**
     * Gets registered children types.
     * @return children types
     */
    Set<HierarchicalIndexType> getChildren();
}
