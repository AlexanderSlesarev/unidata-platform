/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */

package org.unidata.mdm.search.dto;

import org.unidata.mdm.system.dto.OutputContainer;

/**
 * The specifically typed search output container.
 * @author Mikhail Mikhailov on Mar 5, 2020
 */
public interface SearchOutputContainer extends OutputContainer {
    /**
     * Type of the container.
     */
    enum SearchOutputContainerType {
        SIMPLE,
        COMPLEX
    }
    /**
     * Gets the precise type of the container.
     * @return type
     */
    SearchOutputContainerType getContainerType();
}
