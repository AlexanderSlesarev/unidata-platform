package org.unidata.mdm.search.type.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.search.type.IndexField;

/**
 * The (multi)match FTS query.
 * @author Mikhail Mikhailov on Feb 20, 2020
 */
public class MatchSearchQuery implements SearchQuery {
    /**
     * The text.
     */
    private final String text;
    /**
     * Search with regard to NL morphology.
     */
    private final boolean morhological;
    /**
     * Fields involved.
     */
    private final List<IndexField> fields;
    /**
     * Constructor.
     * @param text
     * @param fields
     */
    protected MatchSearchQuery(String text, List<IndexField> fields) {
        this(text, false, fields);
    }
    /**
     * Constructor.
     * @param text the query
     * @param morphological the morphology feature
     * @param fields search fields
     */
    protected MatchSearchQuery(String text, boolean morphological, List<IndexField> fields) {
        super();
        this.text = text;
        this.morhological = morphological;
        this.fields = Objects.isNull(fields) ? new ArrayList<>(4) : new ArrayList<>(fields);
    }
    /**
     * Constructor.
     * @param text
     * @param fields
     */
    protected MatchSearchQuery(String text, IndexField... fields) {
        this(text, false, fields);
    }
    /**
     * Constructor.
     * @param text the query
     * @param morphological the morphology feature
     * @param fields search fields
     */
    protected MatchSearchQuery(String text, boolean morphological, IndexField... fields) {
        super();
        this.text = text;
        this.morhological = morphological;
        this.fields = new ArrayList<>(ArrayUtils.getLength(fields));

        if (ArrayUtils.isNotEmpty(fields)) {

            for (int i = 0; i < fields.length; i++) {

                if (Objects.isNull(fields[i])) {
                    continue;
                }

                this.fields.add(fields[i]);
            }
        }
    }
    /**
     * @return the text
     */
    public String getText() {
        return text;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public SearchQueryType getType() {
        return SearchQueryType.MATCH;
    }
    /**
     * @return the fields
     */
    public List<IndexField> getFields() {
        return fields;
    }
    /**
     * @return the morhological
     */
    public boolean isMorhological() {
        return morhological;
    }
}
