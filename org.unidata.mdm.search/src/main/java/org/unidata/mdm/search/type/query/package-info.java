/**
 * Query types:
 * - FORM
 * - QSTRING
 * for now.
 * @author Mikhail Mikhailov on Feb 20, 2020
 */
package org.unidata.mdm.search.type.query;