/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.type.id;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.system.util.TimeBoundaryUtils;

/**
 * @author Mikhail Mikhailov
 * Common part of managed index id.
 */
public abstract class AbstractManagedIndexId implements ManagedIndexId {
    /**
     * Separator for period id fields (hyphen).
     */
    protected static final String PERIOD_ID_SEPARATOR = "-";
    /**
     * Entity name.
     */
    protected String entityName;
    /**
     * Generated index id.
     */
    protected String indexId;
    /**
     * Routing.
     */
    protected String routing;
    /**
     * Constructor.
     */
    protected AbstractManagedIndexId() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getIndexId() {
        return indexId;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getRouting() {
        return routing;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getEntityName() {
        return entityName;
    }

    /**
     * Ids for indexed objects.
     *
     * @param parts prefix parts (etalon id, classifier/relation name etc.)
     * @return id string
     */
    protected static String periodId(String... parts) {
        return StringUtils.join(parts, PERIOD_ID_SEPARATOR);
    }
    /**
     * Returns period id as string.
     *
     * @param val period id value
     * @return
     */
    public static String periodIdValToString(long val) {

        String valString = Long.toUnsignedString(val);
        StringBuilder sb = new StringBuilder().append(Long.signum(val) == -1 ? "0" : "1");
        for (int i = valString.length(); i < 19; i++) {
            sb.append("0");
        }

        return sb
                .append(valString)
                .toString();
    }
    /**
     * Returns period id as string.
     *
     * @param d the date to turn into period id
     * @return period id as string
     */
    public static String periodIdValToString(Date d) {
        return periodIdValToString(TimeBoundaryUtils.toUpperBound(d));
    }
}
